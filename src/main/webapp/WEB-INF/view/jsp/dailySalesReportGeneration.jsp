<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!-- <link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/buttons/1.5.1/css/buttons.bootstrap.min.css">
<!-- <script
	src="https://code.jquery.com/jquery-3.3.1.js"></script> -->
<script
	src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script
	src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
<script
	src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script
	src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.bootstrap.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script
	src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script
	src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>

	




<style>
html, body {
	background-color: #fff;
}
</style>
<script type="text/javascript">
	
</script>
<div class="container-fluid container-fluid-441222">
	<div class="row row-without-margin">
		<h1 class="h1-report">Daily Sales Report</h1>
		<ol class="breadcrumb">
			<li><i class="fa fa-bar-chart"></i>&nbsp;Report Management</li>
			<li class="active">Daily Sales Report</li>
		</ol>
	</div>
	<hr>
	<div class="row row-without-margin">
		<form:form action="dsReport" method="post" commandName="dsrParams"
			cssClass="form-incline">
			<div class='row'>

				<div class='col-sm-2'>
					<div class='form-group'>
						<label>From Date</label>
						<form:input path="fromDate" id="fromDate"
							class="date-picker form-control glyphicon glyphicon-calendar" />
					</div>
				</div>
				<div class='col-sm-2'>
					<div class='form-group'>
						<label>TO Date</label>
						<form:input path="toDate" id="toDate"
							class="date-picker form-control glyphicon glyphicon-calendar" />
					</div>
				</div>

				<div class='col-sm-2'>
					<div class='form-group'>
						<label style="display: block; color: #f5f5f5">. </label> <input
							type="submit" id="datesearch"
							class="btn btn-success form-control" name="datesearch"
							value="Search" />
					</div>
				</div>

			</div>
		</form:form>
	</div>
	<hr>
		<div class="row row-without-margin">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<h3 style="margin-top: 0px; margin-bottom: 20px;">Search Result</h3>
		</div>
	</div>
	<div class="row row-without-margin">
		<div class="container-fluid table-responsive">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="dataTable_wrapper">
					<table class="table table-striped table-hover display"
						id="report">
						<thead style="background-color: #222534; color: #fff;">
							<tr>

								<!-- hamid -->
								<th style="display: none;">SALES ORDER NO</th>
								<!-- 0 -->
								<th>MBL/MAWB</th>
								<th style="display: none;">GHA</th>
								<th style="display: none;">BOOKED</th>
								<th>ORIGIN</th>
								<th>DESTINATION</th>
								<th style="display: none;">PIECES</th>
								<th style="display: none;">GROSS WEIGHT</th>
								<th style="display: none;">CHARGEABLE WEIGHT</th>
								<th>CBM</th>
								<th style="display: none;">TYPES OF SHIPMENT</th>
								<!-- 10 -->
								<th>ETA</th>
								<th>ETD</th>
								<th style="display: none;">DIFF ETA & ATA</th>
								<th style="display: none;">CARGO HANDOVER DATE</th>
								<th style="display: none;">PRE-ALERT DATE</th>
								<th style="display: none;">ATA AIPORT</th>
								<th style="display: none;">WH DTAE</th>
								<th style="display: none;">TYPE OF DELIVERY</th>
								<th style="display: none;">P/U TIME</th>
								<th style="display: none;">SLOT REQUEST DATE</th>
								<!-- 20 -->
								<th style="display: none;">SLOT RECEIVED DATE</th>
								<th style="display: none;">CARGO STATUS</th>
								<th style="display: none;">EXCEPTION</th>
								<th style="display: none;">REMARKS</th>
								<th style="display: none;">WEEKEND HOURS</th>
								<th style="display: none;">PRE-NOTICE REQ H&M</th>
								<th style="display: none;">DLV. AFTER ARRIVAL DTAE</th>
								<th style="display: none;">ARR. TO DLV.- WEEKEND/SLOT REQ TIME/TRUCKING TIME</th>
								<th style="display: none;">ARR. TO TRUCK DEP.</th>
								<th style="display: none;">TRUCK TYPE</th>
								<!-- 30 -->
								<th style="display: none;">TRUCK BOOKING DATE</th>
								<th style="display: none;">TRUCK CANCELLATION DATE</th>
								<th style="display: none;">LICENSE PLATE</th>
								<th style="display: none;">DRIVER DETAILS</th>
								<th style="display: none;">GHA TO SOV ATA</th>
								<th style="display: none;">GHA TO SOV ATD</th>
								<!-- 36 -->


							</tr>
						</thead>
						<tbody>

							<c:forEach items="${searchList}" var="i">
								<tr>
									<td>${i.bl_no}</td>
									<td style="display: none;">${i.bl_date}</td>
									<td style="display: none;">${i.shipper_name}</td>
									<td>${i.buyer_name}</td>
									<td style="display: none;"></td>
									<td style="display: none;">${i.comm_invoice_no}</td>
									<td style="display: none;">${i.lc_tt_po_no}</td>
									<td style="display: none;">${i.freight_mode}</td>
									<td style="display: none;">${i.po_no}</td>
									<td>${i.material_desc}</td>
									<!-- 10 -->
									<td style="display: none;">${i.style_no}</td>
									<td style="display: none;"></td>
									<td style="display: none;">${i.article_no}</td>
									<td style="display: none;">${i.sku_no}</td>
									<td style="display: none;">${i.size1}</td>
									<td style="display: none;">${i.color}</td>
									<td style="display: none;">${i.indent_no}</td>
									<td style="display: none;">${i.tot_qty}</td>
									<td style="display: none;">${i.gross_wt}</td>
									<td style="display: none;">${i.net_wt}</td>
									<!-- 20 -->
									<td style="display: none;">${i.tot_volume}</td>
									<td style="display: none;">${i.tot_pcs}</td>
									<td style="display: none;">${i.length}</td>
									<td style="display: none;">${i.width}</td>
									<td style="display: none;">${i.height}</td>
									<td style="display: none;"></td>
									<td style="display: none;"></td>
									<td style="display: none;"></td>
									<td style="display: none;">${i.chdt}</td>
									<td style="display: none;"></td>
									<!-- 30 -->
									<td style="display: none;">${i.gr_date}</td>
									<td style="display: none;"></td>
									<td style="display: none;"></td>
									<td style="display: none;"></td>
									<td style="display: none;">${i.carrier_name}</td>
									<td style="display: none;">${i.pol_name}</td>
									<td style="display: none;">${i.fvslName}</td>
									<td></td>
									<td><fmt:formatDate pattern="dd-MMM-yy" type="date"
											value="${i.fvsletd}" /></td>
									<td>${i.tship_port_code}</td>
									<!-- 40 -->
								</tr>
							</c:forEach>


						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	
</div>
<jsp:include page="footer_v2.jsp"></jsp:include>
<script>
	$(document).ready(function() {
		$("#toDate").datepicker({
			autoclose : true,
			dateFormat : "dd-mm-yy"
		});
		$("#fromDate").datepicker({
			autoclose : true,
			dateFormat : "dd-mm-yy"
		});	
		    var table = $('#report').DataTable( {
		        lengthChange: false,
		        buttons: [ 'excel']
		    } );
		 
		    table.buttons().container()
		        .appendTo( '#report_wrapper .col-sm-6:eq(0)' );
	});
</script>
