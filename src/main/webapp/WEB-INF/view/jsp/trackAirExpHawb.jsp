<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script>
	$(document)
			.ready(
					function() {
						var $overlay = $.LoadingOverlay("show");
						var status = "${airExportHAWBData.myshipmentTrackHeader[0].status}";
						var mawbIssueDate = "${airExportHAWBData.myshipmentTrackHeader[0].mbl_dt}";
						console.log("mawb " + mawbIssueDate);
						console.log("status" + status);

						var statusDate = "${airExportHAWBData.myshipmentTrackHeader[0].eta_dt}";
						var year = statusDate.substring(0, 4);
						var month = statusDate.substring(4, 6);
						var date = statusDate.substring(6);
						var totalDate = year + "-" + month + "-" + date;
						console.log("mawb " + totalDate);
						console.log("status" + status);
						var today = new Date();
						var eta = new Date(totalDate);
						console.log("ETA " + eta);
						console.log("Today " + today);

						if (eta < today) {
							document.getElementById("order-booked").className = "active";
							document.getElementById("gr-done").className = "active";
							document.getElementById("booked-in-airlines").className = "active";
							document.getElementById("shipment-done").className = "active";
							document.getElementById("arrival").className = "active";
						}

						if (status === "Order Booked") {
							document.getElementById("order-booked").className = "active";
						} else if (status === "GR Done") {
							document.getElementById("order-booked").className = "active";
							document.getElementById("gr-done").className = "active";
						} else if (status === "Shipment Done") {
							document.getElementById("order-booked").className = "active";
							document.getElementById("gr-done").className = "active";
							document.getElementById("booked-in-airlines").className = "active";
							document.getElementById("shipment-done").className = "active";
						} else if (mawbIssueDate !== "") {
							document.getElementById("order-booked").className = "active";
							document.getElementById("gr-done").className = "active";
							document.getElementById("booked-in-airlines").className = "active";
						}
						$('.collapse')
								.on(
										'shown.bs.collapse',
										function() {
											$(this)
													.parent()
													.find(
															".glyphicon-plus-sign")
													.removeClass(
															"glyphicon-plus-sign")
													.addClass(
															"glyphicon-minus-sign");
										})
								.on(
										'hidden.bs.collapse',
										function() {
											$(this)
													.parent()
													.find(
															".glyphicon-minus-sign")
													.removeClass(
															"glyphicon-minus-sign")
													.addClass(
															"glyphicon-plus-sign");
										});
						$.LoadingOverlay("hide");
					});
</script>


<script type="text/javascript">
	function convertDateTime_temp(date, time) {
		if (typeof (date) == 'undefined')
			return null;
		if (typeof (time) == 'undefined')
			return null;
		console.log(date.getFullYear());
		console.log(date.getMonth());
		console.log(date.getDate());
		var d = new Date(date.getFullYear()
				+ '-'
				+ (date.getMonth() + 1)
				+ '-'
				+ (date.getDate().toString().length == 1 ? '0' + date.getDate()
						: date.getDate()) + 'T' + time + 'Z');
		console.log(d);
		return d;
		//return new Date(date.getFullYear() + '-' + (date.getMonth()+1) + '-' + date.getDate() + 'T' + time + 'Z');
	}
	function convertJsonArrayDateToJSDate(json) {

		json.filter(function(e) {
			var keys = Object.keys(e);
			for (var i = 0; i < keys.length; i++) {
				if (typeof (e[keys[i]]['iLocalMillis']) != "undefined") {
					if (keys[i] == 'dtDepGtSchDate') {
						var tempDate = new Date(
								parseInt(e[keys[i]]['iLocalMillis']));
						e[keys[i]] = convertDateTime_temp(tempDate,
								e['vcDepGtSchTime']);

					} else if (keys[i] == 'dtDepGtActDate') {
						var tempDate = new Date(
								parseInt(e[keys[i]]['iLocalMillis']));
						e[keys[i]] = convertDateTime_temp(tempDate,
								e['vcDepGtActTime']);

					} else if (keys[i] == 'dtDepGtEstDate') {
						var tempDate = new Date(
								parseInt(e[keys[i]]['iLocalMillis']));
						e[keys[i]] = convertDateTime_temp(tempDate,
								e['vcDepGtEstTime']);

					} else if (keys[i] == 'dtArrGtSchDate') {
						var tempDate = new Date(
								parseInt(e[keys[i]]['iLocalMillis']));
						e[keys[i]] = convertDateTime_temp(tempDate,
								e['vcArrGtSchTime']);

					} else if (keys[i] == 'dtArrGtActDate') {
						var tempDate = new Date(
								parseInt(e[keys[i]]['iLocalMillis']));
						e[keys[i]] = convertDateTime_temp(tempDate,
								e['vcArrGtActTime']);

					} else if (keys[i] == 'dtArrGtEstDate') {
						var tempDate = new Date(
								parseInt(e[keys[i]]['iLocalMillis']));
						e[keys[i]] = convertDateTime_temp(tempDate,
								e['vcArrGtEstTime']);

					} else {
						e[keys[i]] = new Date(
								parseInt(e[keys[i]]['iLocalMillis']));
					}
				}
			}
		});
	}

	var flightInfoJson = [];
	var currentFlightJson = [];

	var flightModel = '${oagResponseDetails}';

	if (flightModel != '') {
		flightInfoJson = JSON.parse(flightModel);
		convertJsonArrayDateToJSDate(flightInfoJson['oagResponse']);
	}

	var currentFlight = '${currentFlight}';

	if (currentFlight != '') {
		tempObject = JSON.parse(currentFlight);
		currentFlightJson.push(tempObject);
		convertJsonArrayDateToJSDate(currentFlightJson);
	}

	$(document).ready(function() {

	});
	var flightLat = '${flight.flightLat}';
	var flightLong = '${flight.flightLong}';
	var depLat = '${flight.depLat}';
	var depLong = '${flight.depLong}';
	var arrLat = '${flight.arrLat}';
	var arrLong = '${flight.arrLong}';
	var dep = '${flight.depCity}';
	var arr = '${flight.arrCity}';
	var flight = '${flight.flightNo}';
	var depCode = '${flight.depCode}';
	var arrCode = '${flight.arrCode}';
	var status = '${flight.flightStatus}';
</script>




<!-- <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.4/dist/leaflet.css"
integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
crossorigin=""/>

<script src="https://unpkg.com/leaflet@1.3.4/dist/leaflet.js"
integrity="sha512-nMMmRyTVoLYqjP9hrbed9S+FzjZHW5gY1TWCHA5ckwXZBadntCNs8kEqAWdrb9O7rxbCaA4lKTIWjDXZxflOcA=="
crossorigin=""></script> -->

<script
	src="${pageContext.request.contextPath}/resources/ammap/ammap/ammap.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/ammap/ammap/maps/js/worldLow.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/ammap/ammap/themes/light.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/ammap/ammap/plugins/export/export.min.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/dashboardMap.css"
	type="text/css" media="all" />

<script
	src="${pageContext.request.contextPath}/resources/js/airportcordinates.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/oagMap.js"></script>


<style>
/* Set the size of the div element that contains the map */
#map {
	height: 500px; /* The height is 400 pixels */
	width: 100%; /* The width is the width of the web page */
}

.container {
	width: 1325px !important;
}

.flightPageDataTableContainer table th, table td {
	text-align: center;
}
</style>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/oag.css"
	type="text/css" media="all" />

<style>
.table>thead>tr>td, .table>tbody>tr>td, .table>thead>tr>th, .table>tbody>tr>th
	{
	border: none;
}

/* 	.panel {
            border-color: #FFF;
        } */
html, body {
	height: 100% !important;
	background-color: #FFF;
}

img {
	/* max-width:100%;
        max-height:100%; */
	border: 0;
	margin-top: 0;
}

h4 {
	/*   padding-right: 24px;
        padding-bottom: 5px; */
	padding: 15px;
	font-size: 14px;
}
</style>

<!-- Theme style -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/newtimeline.css" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/myshipment_v2/oag-style.css" />


<section>
	<div class="container-fluid">
		<div class="page-header"
			style="margin: 10px 0 10px; padding-bottom: 0px; border-bottom: 0px;">
			<!-- <h1>Sea Export HBL Tracking Details</h1> -->
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-offset-1 col-md-5">
						<h2>Air Export HAWB Tracking Details</h2>
					</div>
					<%-- 							<div class="col-md-4 sea-export-3">
                                                    <h4 style="float: right;">${airExportHAWBData.myshipmentTrackHeader[0].sales_org_desc}</h4>
                                                </div>
                                                <div class="col-md-2 sea-export-3">
                                                    <h4>
                                                        <img
                                                            src="${pageContext.request.contextPath}/resources/images/${airExportHAWBData.myshipmentTrackHeader[0].comp_code}.jpg"
                                                            alt="Company Logo" style="width: 90px;" />
                                                    </h4>
                                                </div> --%>

				</div>
			</div>
		</div>
	</div>
</section>

<section class="milestone-track-section-254196">
	<div class="container">
		<div class="panel-group">
			<div class="panel panel-default"
				style="border-bottom: 3px solid #007AA7; border-bottom: 2px solid #007AA7; margin-bottom: 0px; border-radius: 0px;">
				<div class="panel-heading-217226">
					<div class="row">
						<div class="col-md-12" style="text-align: center">
							<div class="col-md-4">
								<div style="padding: 5px 0px 5px 0px;">HAWB NO. :
									${airExportHAWBData.myshipmentTrackHeader[0].bl_no }</div>
							</div>
							<div class="col-md-4">
								<%--  < div style="padding: 5px 0px 5px 0px;">STATUS :
                                    ${airExportHAWBData.myshipmentTrackHeader[0].status}</div> --%>
							</div>
							<div class="col-md-4">
								<div style="padding: 5px 0px 5px 0px;">
									ETA :
									<fmt:parseDate
										value="${airExportHAWBData.myshipmentTrackHeader[0].eta_dt}"
										var="eta_date" pattern="yyyyMMdd" />
									<fmt:formatDate pattern="dd-MM-yyyy" type="date"
										value="${eta_date}" />
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- <div class="panel-heading" style="background-color: #00ADF1; color: #FFF;margin-bottom: 0px;border-radius: 0px;">
            </div> -->
				<div class="panel-body">
					<ul class="progressbar customUl">
						<li id="order-booked" class="active">
							<div class="timestamp-194858">
								<span> <c:choose>
										<c:when
											test="${empty airExportHAWBData.myshipmentTrackHeader[0].bl_bt}">
                                        N/A
                                    </c:when>
										<c:otherwise>
											<fmt:parseDate
												value="${airExportHAWBData.myshipmentTrackHeader[0].bl_bt}"
												var="bl_date" pattern="yyyyMMdd" />
											<fmt:formatDate pattern="dd-MM-yyyy" type="date"
												value="${bl_date}" />
										</c:otherwise>
									</c:choose>
								</span>
							</div>
							<div class="status-662147">
								<!-- Prints the order details bl_no, tot_pcs (Total pieces), por (port name) if status matches-->
								<span> <c:choose>
										<c:when
											test="${!empty airExportHAWBData.myshipmentTrackHeader[0].bl_bt || airExportHAWBData.myshipmentTrackHeader[0].status =='Order Booked'}">
											<h4>Order
												${airExportHAWBData.myshipmentTrackHeader[0].bl_no} Booked</h4>
										</c:when>
										<c:otherwise>
											<h4>Order
												${airExportHAWBData.myshipmentTrackHeader[0].bl_no} is not Booked yet</h4>
										</c:otherwise>
									</c:choose>
								</span>
								<!-- <h4>Order ${airExportHAWBData.myshipmentTrackHeader[0].bl_no} Booked</h4> -->
							</div>
						</li>
						<li id="gr-done">
							<div class="timestamp-194858">
								<span> <c:choose>
										<c:when
											test="${empty airExportHAWBData.myshipmentTrackHeader[0].gr_dt}">
                                        N/A
                                    </c:when>
										<c:otherwise>
											<fmt:parseDate
												value="${airExportHAWBData.myshipmentTrackHeader[0].gr_dt}"
												var="gr_date" pattern="yyyyMMdd" />
											<fmt:formatDate pattern="dd-MM-yyyy" type="date"
												value="${gr_date}" />
										</c:otherwise>
									</c:choose>
								</span>
							</div>
							<div class="status-662147">
								<!-- Prints the order details bl_no, tot_pcs (Total pieces), por (port name) if status matches-->
								<span> <c:choose>
										<c:when
											test="${!empty airExportHAWBData.myshipmentTrackHeader[0].gr_dt || airExportHAWBData.myshipmentTrackHeader[0].status =='GR Done'}">
											<h4>
												Order ${airExportHAWBData.myshipmentTrackHeader[0].bl_no}, <strong>
													${airExportHAWBData.myshipmentTrackHeader[0].tot_pcs}</strong>
												pieces Received at
												${airExportHAWBData.myshipmentTrackHeader[0].por}
											</h4>
										</c:when>
										<c:otherwise>
											<h4>
												Order ${airExportHAWBData.myshipmentTrackHeader[0].bl_no}, <strong>
													0</strong>
												pieces Received at
												${airExportHAWBData.myshipmentTrackHeader[0].por}
											</h4>
										</c:otherwise>
									</c:choose>
								</span>
								<!-- <h4>Order ${airExportHAWBData.myshipmentTrackHeader[0].bl_no}, <strong> ${airExportHAWBData.myshipmentTrackHeader[0].tot_pcs}</strong> pieces Received at ${airExportHAWBData.myshipmentTrackHeader[0].por}</h4> -->
							</div>
						</li>

						<li id="booked-in-airlines">
							<div class="timestamp-194858">
								<span> <c:choose>
										<c:when
											test="${empty airExportHAWBData.myshipmentTrackHeader[0].mbl_dt}">
                                        N/A
                                    </c:when>
										<c:otherwise>
											<fmt:parseDate
												value="${airExportHAWBData.myshipmentTrackHeader[0].mbl_dt}"
												var="gr_date" pattern="yyyyMMdd" />
											<fmt:formatDate pattern="dd-MM-yyyy" type="date"
												value="${gr_date}" />
										</c:otherwise>
									</c:choose>
								</span>
							</div>
							<div class="status-662147">
								<!-- Prints the order details bl_no, tot_pcs (Total pieces), por (port name) if status matches-->
								<span> <c:choose>
										<c:when
											test="${!empty airExportHAWBData.myshipmentTrackHeader[0].mbl_dt || airExportHAWBData.myshipmentTrackHeader[0].mbl_dt != ''}">
											<h4>
												Order ${airExportHAWBData.myshipmentTrackHeader[0].bl_no}, <strong>
													${airExportHAWBData.myshipmentTrackHeader[0].tot_pcs}</strong>
												pieces Booked with<br> <strong>${airports}</strong>
											</h4>
										</c:when>
										<c:otherwise>
											<h4>Order ${airExportHAWBData.myshipmentTrackHeader[0].bl_no}, <strong>
													0</strong>
												pieces Booked with<br> <strong>${airports}</strong></h4>
										</c:otherwise>
									</c:choose>
								</span>
								<!-- <h4>Order ${airExportHAWBData.myshipmentTrackHeader[0].bl_no}, <strong> ${airExportHAWBData.myshipmentTrackHeader[0].tot_pcs}</strong> pieces Booked with<br> <strong>${airports}</strong></h4> -->
							</div>
						</li>
						<li id="shipment-done">
							<div class="timestamp-194858">
								<span> <c:choose>
										<c:when
											test="${empty airExportHAWBData.myshipmentTrackHeader[0].stuff_dt}">
                                        N/A
                                    </c:when>
										<c:otherwise>
											<fmt:parseDate
												value="${airExportHAWBData.myshipmentTrackHeader[0].stuff_dt}"
												var="stuff_date" pattern="yyyyMMdd" />
											<fmt:formatDate pattern="dd-MM-yyyy" type="date"
												value="${stuff_date}" />
										</c:otherwise>
									</c:choose>
								</span>
							</div>
							<div class="status-662147">
								<!-- <h4>Shipment Done</h4> -->
								<!-- Prints the order details bl_no, tot_pcs (Total pieces), por (port name) if status matches-->
								<span> <c:choose>
										<c:when
											test="${!empty airExportHAWBData.myshipmentTrackHeader[0].stuff_dt || airExportHAWBData.myshipmentTrackHeader[0].status =='Shipment Done'}">
											<h4>
												Order ${airExportHAWBData.myshipmentTrackHeader[0].bl_no}, <strong>
													${airExportHAWBData.myshipmentTrackHeader[0].tot_pcs}</strong>
												pieces Shipped
											</h4>
										</c:when>
										<c:otherwise>
											<h4>Order ${airExportHAWBData.myshipmentTrackHeader[0].bl_no}, <strong>
													0</strong>
												pieces Shipped</h4>
										</c:otherwise>
									</c:choose>
								</span>
								<!-- <h4>
									Order ${airExportHAWBData.myshipmentTrackHeader[0].bl_no}, <strong>
										${airExportHAWBData.myshipmentTrackHeader[0].tot_pcs}</strong> pieces
									Shipped
								</h4> -->
							</div>
						</li>

						<li id="arrival">
							<div class="timestamp-194858">
								<span> <c:choose>
										<c:when
											test="${empty airExportHAWBData.myshipmentTrackHeader[0].eta_dt}">
                                        N/A
                                    </c:when>
										<c:otherwise>
											<fmt:parseDate
												value="${airExportHAWBData.myshipmentTrackHeader[0].eta_dt}"
												var="eta_date" pattern="yyyyMMdd" />
											<fmt:formatDate pattern="dd-MM-yyyy" type="date"
												value="${eta_date}" />
										</c:otherwise>
									</c:choose>
								</span>
							</div>
							<div class="status-662147">
								<!-- Prints the order details bl_no, tot_pcs (Total pieces), por (port name) if status matches-->
								<span> <c:choose>
										<c:when
											test="${!empty airExportHAWBData.myshipmentTrackHeader[0].eta_dt}">
											<h4>
												Order ${airExportHAWBData.myshipmentTrackHeader[0].bl_no}, <strong>
													${airExportHAWBData.myshipmentTrackHeader[0].tot_pcs}</strong>
												pieces Arrived at AirPort of Discharge
											</h4>
										</c:when>
										<c:otherwise>
											<h4>Order ${airExportHAWBData.myshipmentTrackHeader[0].bl_no}, <strong>
													0</strong>
												pieces Arrived at AirPort of Discharge</h4>
										</c:otherwise>
									</c:choose>
								</span>
								<!-- <h4>
									Order ${airExportHAWBData.myshipmentTrackHeader[0].bl_no}, <strong>
										${airExportHAWBData.myshipmentTrackHeader[0].tot_pcs}</strong> pieces
									Arrived at AirPort of Discharge
								</h4> -->
							</div>
						</li>

						<li id="delivery">
							<div class="timestamp-194858">
								<span> <%-- <c:choose>
                                    <c:when
                                            test="${empty airExportHAWBData.myshipmentTrackHeader[0].eta_dt}">
                                        N/A
                                    </c:when>
                                    <c:otherwise>
                                        <fmt:parseDate
                                                value="${airExportHAWBData.myshipmentTrackHeader[0].eta_dt}"
                                                var="eta_date" pattern="yyyyMMdd" />
                                        <fmt:formatDate pattern="dd-MM-yyyy" type="date"
                                                        value="${eta_date}" />
                                    </c:otherwise>
                                </c:choose> --%> N/A
								</span>
							</div>
							<div class="status-662147">
								<!-- Prints the order details bl_no, tot_pcs (Total pieces), por (port name) if status matches-->
								<!--  <span> <c:choose>
										<c:when
											test="${airExportHAWBData.myshipmentTrackHeader[0].status =='Shipment Done'}">
											<h4>
												Order ${airExportHAWBData.myshipmentTrackHeader[0].bl_no}, <strong>
													${airExportHAWBData.myshipmentTrackHeader[0].tot_pcs}</strong>
												Arrived at place of delivery
											</h4>
										</c:when>
										<c:otherwise>
											<h4>Pending</h4>
										</c:otherwise>
									</c:choose>
								</span>-->
								<h4>
									Order ${airExportHAWBData.myshipmentTrackHeader[0].bl_no}, <strong>
										0</strong> Arrived
									at place of delivery
								</h4>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- hamid collapse toggle -->
<div class="container">
	<ul class="nav nav-tabs nav-justified nav-book-update" id=trackTab>
		<li id="general"><a data-toggle="tab" href="#generalinfo">Shipment
				Information</a></li>
		<li id="flightwise" class="active"><a data-toggle="tab"
			href="#flightinfo">Flight Track Information</a></li>

	</ul>
	<div class="tab-content">
		<div id="generalinfo" class="tab-pane fade">
			<div class="panel-group" id="accordion">
				<div class="panel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="" data-toggle="collapse" data-parent="#accordion"
								href="#collapseOne"> <span>CONSIGNMENT DETAILS</span> <!-- <span class="glyphicon glyphicon-minus-sign" style="float: right;"></span> -->

							</a>
						</h4>
					</div>
					<div id="collapseOne" class="panel-collapse collapse in">
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-hover table-inverse">

									<tbody>
										<tr>
											<th scope="row">HAWB No.</th>
											<td>${airExportHAWBData.myshipmentTrackHeader[0].bl_no }</td>
											<th scope="row">Commercial Invoice No.</th>
											<td>${airExportHAWBData.myshipmentTrackHeader[0].comm_inv }</td>
										</tr>
										<tr>
											<th scope="row">HAWB Date</th>
											<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${airExportHAWBData.myshipmentTrackHeader[0].bl_bt_openTracking }" /></td> --%>
											<%-- <td>${airExportHAWBData.myshipmentTrackHeader[0].bl_bt }</td>  --%>
											<fmt:parseDate
												value="${airExportHAWBData.myshipmentTrackHeader[0].bl_bt}"
												var="bl_date" pattern="yyyyMMdd" />
											<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
													value="${bl_date}" /></td>

											<th scope="row">Commercial Invoice Date</th>
											<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${airExportHAWBData.myshipmentTrackHeader[0].comm_inv_dt_openTracking }" /></td> --%>

											<%-- <td>${airExportHAWBData.myshipmentTrackHeader[0].comm_inv_dt }</td> --%>
											<fmt:parseDate
												value="${airExportHAWBData.myshipmentTrackHeader[0].comm_inv_dt}"
												var="comm_inv_date" pattern="yyyyMMdd" />
											<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
													value="${comm_inv_date}" /></td>
										</tr>
										<tr>
											<th scope="row">Shipper Name</th>
											<td>${airExportHAWBData.myshipmentTrackHeader[0].shipper }</td>
											<th scope="row">Place of Receipt</th>
											<td>${airExportHAWBData.myshipmentTrackHeader[0].por }</td>
										</tr>
										<tr>
											<th scope="row">Consignee</th>
											<td>${airExportHAWBData.myshipmentTrackHeader[0].buyer }</td>
											<th scope="row">Cargo Received Date</th>
											<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${airExportHAWBData.myshipmentTrackHeader[0].gr_dt_openTracking }" /></td> --%>
											<%-- <td>${airExportHAWBData.myshipmentTrackHeader[0].gr_dt }</td> --%>
											<fmt:parseDate
												value="${airExportHAWBData.myshipmentTrackHeader[0].gr_dt}"
												var="gr_date" pattern="yyyyMMdd" />
											<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
													value="${gr_date}" /></td>
										</tr>
										<tr>
											<th scope="row">Port Of Loading</th>
											<td>${airExportHAWBData.myshipmentTrackHeader[0].pol }</td>
											<th scope="row">MAWB Number</th>
											<td>${airExportHAWBData.myshipmentTrackHeader[0].mbl_no }</td>
										</tr>
										<tr>
											<th scope="row">Port of Discharge</th>
											<td>${airExportHAWBData.myshipmentTrackHeader[0].pod }</td>
											<th scope="row">Airline</th>
											<td>${airExportHAWBData.myshipmentTrackHeader[0].carrier }</td>
										</tr>
										<tr>
											<th scope="row">Place of Discharge</th>
											<td>${airExportHAWBData.myshipmentTrackHeader[0].plod }</td>
											<th scope="row">MAWB Issue Date</th>
											<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${airExportHAWBData.myshipmentTrackHeader[0].stuff_dt_openTracking }" /></td> --%>

											<%-- <td>${airExportHAWBData.myshipmentTrackHeader[0].stuff_dt }</td> --%>
											<fmt:parseDate
												value="${airExportHAWBData.myshipmentTrackHeader[0].mbl_dt}"
												var="stuff_date" pattern="yyyyMMdd" />
											<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
													value="${stuff_date}" /></td>

										</tr>
										<tr>
											<th scope="row">TOS</th>
											<td>${airExportHAWBData.myshipmentTrackHeader[0].frt_mode_ds }</td>
											<th scope="row">ETD</th>
											<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${airExportHAWBData.myshipmentTrackHeader[0].dep_dt_openTracking }" /></td> --%>

											<%-- <td>${airExportHAWBData.myshipmentTrackHeader[0].dep_dt }</td> --%>
											<fmt:parseDate
												value="${airExportHAWBData.myshipmentTrackHeader[0].dep_dt}"
												var="load_date" pattern="yyyyMMdd" />
											<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
													value="${load_date}" /></td>

										</tr>
										<tr>
											<th scope="row">Total Quantity</th>
											<td>${airExportHAWBData.myshipmentTrackHeader[0].tot_qty}</td>
											<fmt:parseNumber var="totqty" integerOnly="true"
												type="number"
												value="${airExportHAWBData.myshipmentTrackHeader[0].tot_qty}" />
											<%-- <td>${totqty}</td> --%>

											<th scope="row">ETA</th>
											<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${airExportHAWBData.myshipmentTrackHeader[0].eta_dt_openTracking }" /></td> --%>
											<%-- <td>${airExportHAWBData.myshipmentTrackHeader[0].eta_dt }</td> --%>
											<fmt:parseDate
												value="${airExportHAWBData.myshipmentTrackHeader[0].eta_dt}"
												var="eta_date" pattern="yyyyMMdd" />
											<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
													value="${eta_date}" /></td>
										</tr>
										<tr>
											<th scope="row">Total Volume (CBM)</th>
											<td>${airExportHAWBData.myshipmentTrackHeader[0].volume }</td>
											<th scope="row">Destination Agent</th>
											<td>${airExportHAWBData.myshipmentTrackHeader[0].agent }</td>
										</tr>
										<tr>
											<th scope="row">Total Gross Weight</th>
											<td>${airExportHAWBData.myshipmentTrackHeader[0].grs_wt }</td>
											<th scope="row">Chargeable Weight</th>
											<td>${airExportHAWBData.myshipmentTrackHeader[0].crg_wt }</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="panel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse"
								data-parent="#accordion" href="#collapseTwo"> <span>ITEM
									DETAILS</span> <!-- <span class="glyphicon glyphicon-plus-sign" style="float: right;"></span> -->

							</a>
						</h4>
					</div>
					<div id="collapseTwo" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table ">

									<thead>
										<tr>
											<td><strong>Sl</strong></td>
											<td><strong>PO No.</strong></td>
											<td><strong>Commodity</strong></td>
											<td><strong>Style</strong></td>
											<td><strong>Size</strong></td>
											<td><strong>SKU</strong></td>
											<td><strong>Article No.</strong></td>
											<td><strong>Color</strong></td>
											<td><strong>Pieces</strong></td>
											<td><strong>Carton</strong></td>
											<td><strong>Vol.(CBM)</strong></td>
											<td><strong>Gross Wt.</strong></td>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${airExportHAWBData.myshipmentTrackItem}"
											var="item" varStatus="loopCounter">
											<tr>
												<td scope="row">${loopCounter.count}</td>
												<td>${item.po_no}</td>
												<td>${item.commodity}</td>
												<td>${item.style}</td>
												<td>${item.size}</td>
												<td>${item.sku}</td>
												<td>${item.art_no}</td>
												<td>${item.color}</td>
												<%-- <td>${item.item_pcs}</td> --%>
												<fmt:parseNumber var="itmpcs" integerOnly="true"
													type="number" value="${item.item_pcs}" />
												<td>${itmpcs}</td>
												<%-- <td>${item.item_qty}</td> --%>
												<fmt:parseNumber var="itmqty" integerOnly="true"
													type="number" value="${item.item_qty}" />
												<td>${itmqty}</td>
												<td>${item.item_vol}</td>
												<td>${item.item_grwt}</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<div class="panel ">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse"
								data-parent="#accordion" href="#collapseThree"> <span>FLIGHT
									SCHEDULE</span> <!-- <span class="glyphicon glyphicon-plus-sign" style="float: right;"></span> -->

							</a>
						</h4>
					</div>
					<div id="collapseThree" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table ">

									<thead>
										<tr>
											<td><strong>Leg</strong></td>
											<!-- <th>Vessel Name</th> -->
											<!-- <th>Vessel Type</th> -->
											<td><strong>Flight No.</strong></td>
											<td><strong>POL</strong></td>
											<td><strong>POD</strong></td>
											<td><strong>ETD</strong></td>
											<td><strong>ETA</strong></td>
										</tr>
									</thead>
									<tbody>
										<c:forEach
											items="${airExportHAWBData.myshipmentTrackSchedule}"
											var="vSchedule" varStatus="loopCounter">
											<tr>
												<td scope="row">${loopCounter.count}</td>
												<%-- <td>${vSchedule.car_name}</td>
                            <td>${vSchedule.car_type}</td> --%>
												<td>${vSchedule.car_no}</td>
												<td>${vSchedule.pol}</td>
												<td>${vSchedule.pod}</td>
												<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${vSchedule.etd_openTracking}" /></td> --%>
												<%-- <td>${vSchedule.etd}</td> --%>
												<fmt:parseDate value="${vSchedule.etd}" var="etd_date"
													pattern="yyyyMMdd" />
												<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
														value="${etd_date}" /></td>
												<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${vSchedule.eta_openTracking}" /></td> --%>
												<%-- <td>${vSchedule.eta}</td> --%>
												<fmt:parseDate value="${vSchedule.eta}" var="eta_date"
													pattern="yyyyMMdd" />
												<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
														value="${eta_date}" /></td>
												<!--  <td>${vSchedule.etd}</td>
      <td>${vSchedule.eta}</td>-->
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div id="flightinfo" class="tab-pane fade in active">
			<div class=row style="font-family: 'Quicksand', sans-serif;">
				<div class=col-md-8 id="mapBody">

					<div class="row" style="display: block;">
						<div class="col-md-12">
							<div class="flightPageContainer" style="margin: 15px;">
								<div class="flightPageSummaryMap">
									<div class="flightPageSummaryBlock " id="flightPageTourStep1">
										<!-- Airports -->
										<div class="flightPageSummaryAirports  ">
											<div class="flightPageSummaryOrigin">
												<span class="flightPageAirportGate">From</span> <span
													class="flightPageSummaryAirportCode"> <span
													class="displayFlexElementContainer"
													id="legOneDepartureAirportCode"> </span>
												</span> <span class="flightPageSummaryCity"
													id="legOneDepartureAirlineName"> </span>
											</div>
											<div class="flightPageSummaryDestination">
												<span class="flightPageAirportGate">To</span> <span
													class="flightPageSummaryAirportCode"> <span
													class="displayFlexElementContainer"
													id="legOneArrivalAirportCode"> </span>
												</span> <span class="flightPageSummaryCity"> <span
													class="destinationCity" id="legOneArrivalAirlineName">
												</span>
												</span>
											</div>
										</div>
										<div class="flightPageAirportGates ">
											<div class="flightPageSummaryOrigin">
												<span class="flightPageAirportGate">airline</span> <span
													class="flightPageSummaryAirportLink"
													id="legOneDepartureAirportName"> </span> <span
													class="flightPageSummaryDepartureDay"
													id="legOneDepartureDate"></span> <span
													class="flightPageSummaryDeparture" id="legOneDepartureTime">

												</span>
											</div>
											<div class="flightPageSummaryDestination ">
												<span class="flightPageAirportGate"> <span
													class="displayFlexElementContainer">airline</span>
												</span> <span class="flightPageSummaryAirportLink"
													id="legOneArrivalAirportName"> </span> <span
													class="flightPageSummaryArrivalDay" id="legOneArrivalDate"></span>
												<span class="flightPageSummaryArrival"
													id="legOneArrivalTime"> </span>
											</div>
										</div>
										<!--<div class="flightPageSummaryTimes">
                                            <div class="flightPageSummaryOrigin">
                                                <span class="flightPageSummaryDepartureDay" id="legOneDepartureDate"></span>
                                                <span class="flightPageSummaryDeparture">
                                                <em>
                                                08:25AM +03
                                                </em>
                                                <div class="flightPageOriginDelayStatus">
                                                   <span class="flightPageDepartureDelayStatus normal">(on time)</span>
                                                </div>
                                             </span>
                                            </div>
                                            <div class="flightPageSummaryDestination">
                                                <span class="flightPageSummaryArrivalDay" id="legOneArrivalDate"></span>
                                                <span class="flightPageSummaryArrival" id="legOneArrivalTime">
                                                <em>
                                                11:27AM EDT
                                                </em>
                                                <div class="flightPageDestinationDelayStatus">
                                                   <span class="flightPageArrivalDelayStatus normal">(on time)</span>
                                                </div>
                                             </span>
                                            </div>
                                        </div>-->
										<!-- Flight progress -->
										<!--<div class="flightPageProgressContainer ">
                                            <div class="flightPageProgressTrack Scheduled"><span
                                                    class="flightPageProgressTrackMarker">&nbsp;</span><span
                                                    class="flightPageProgressTrackCompleted" style="width:0%"></span>
                                            </div>
                                            <div class="flightPageProgress">
                                                <span class="flightPageProgressPlaceholder"></span>
                                                <span class="flightPageProgressTotal">
                                                 <strong>10h 3m</strong>
                                                 total travel time
                                                 </span>
                                                <span class="flightPageProgressPlaceholder"></span>
                                            </div>
                                            <div class="flightPageProgressDistance">
                                                <span class="flightPageProgressPlaceholder"></span>
                                                <span class="flightPageProgressPlaceholder"></span>
                                            </div>
                                        </div>-->
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="pad">
								<div id="map"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4">
					<div class="flightPageContainer">
						<!-- Airlines Information -->

						<div class="flightPageDataTableContainer">
							<div data-template="live/flight/airline">
								<!-- Airline information -->
								<h3 class="flightPageDataTableHeading flightPageAirlineDetails">Airline
									information</h3>
								<div class="flightPageDataTable flightPageAirlineDetails">
									<!-- Airline -->
									<div class="flightPageDataRow">
										<div class="flightPageDataLabel">Airline</div>
										<div class="flightPageData">
											<span class="flightPageAirlineCallsign"
												id="flightAirlineName"></span>
										</div>
									</div>
								</div>
							</div>
							<div data-template="live/flight/detailOther">
								<!-- Aircraft information -->
								<h3 class="flightPageDataTableHeading">Recent Flight
									Information</h3>
								<div class="flightPageDataTable">
									<!-- Tail number -->
									<!-- Registration -->
									<!-- Aircraft type -->
									<div class="flightPageDataRow">
										<div class="flightPageDataLabel" style="width: 40%;">Flight
											No</div>
										<div class="flightPageData" id="recentFlightNumber"></div>
									</div>
									<div class="flightPageDataRow">
										<div class="flightPageDataLabel" style="width: 40%;">Flight
											Status</div>
										<div class="flightPageData" id="recentFlightStatus"
											style="font-weight: bolder;"></div>
									</div>
									<div class="flightPageDataRow" style="width: 40%;">
										<div class="flightPageDataLabel">Remaining Time</div>
										<div class="flightPageData" id="recentFlightRemaining">

										</div>
									</div>
								</div>
							</div>

						</div>
						<hr>
						<div class="flightPageDataTableContainer">
							<!-- Departure times -->
							<h3 class="flightPageDataTableHeading">Departure Times</h3>
							<div class="flightPageDataTable">
								<div class="flightPageDataTimesParent">
									<div class="flightPageDataTimesChild">
										<div class="flightPageDataActualTimeHeading">
											<div>Gate Departure</div>
										</div>
										<div class="flightPageDataActualTimeText">
											<div id="gateDeparture">
												<span class="time">${flight.depGTTime}</span><span
													class="gmt"></span>
											</div>
										</div>
										<div class="flightPageDataAncillaryText">
											<div>
												<div id="gateDepartureScheduled">
													<span class="time"></span><span class="gmt"></span>
												</div>
											</div>
										</div>
									</div>
									<div class="flightPageDataTimesChild">
										<div class="flightPageDataActualTimeHeading">
											<div>Takeoff</div>
										</div>
										<div class="flightPageDataActualTimeText">
											<div id="takeoffDeparture">
												<span class="time">${flight.depRTTime}</span><span
													class="gmt"></span>
											</div>
										</div>
										<div class="flightPageDataAncillaryText">
											<div>
												<div id="takeoffDepartureScheduled">
													<span class="time"></span><span class="gmt"></span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="flightPageDataAncillaryTextContainer">
									<div class="flightPageDataAncillaryText">
										<div>
											Airport Name: <span id="depAirpotName"> </span>
										</div>
									</div>
									<div class="flightPageDataAncillaryText">
										<div>
											Average Delay: <span> 20-40 minutes </span>
										</div>
									</div>
								</div>
							</div>
							<!-- Arrival times -->
							<h3 class="flightPageDataTableHeading">Arrival Times</h3>
							<div class="flightPageDataTable">
								<div class="flightPageDataTimesParent">
									<div class="flightPageDataTimesChild">
										<div class="flightPageDataActualTimeHeading">
											<div>Landing</div>
										</div>
										<div class="flightPageDataActualTimeText">
											<div id="landingArrival">
												<span class="time"></span><span class="gmt"></span>
											</div>
										</div>
										<div class="flightPageDataAncillaryText">
											<div>
												<div id="landingArrivalScheduled">
													<span class="time"></span><span class="gmt"></span>
												</div>
											</div>
										</div>
									</div>
									<div class="flightPageDataTimesChild">
										<div class="flightPageDataActualTimeHeading">
											<div>Gate Arrival</div>
										</div>
										<div class="flightPageDataActualTimeText">
											<div id="gateArrival">
												<span class="time">${flight.arrGTTime}</span><span
													class="gmt"></span>
											</div>
										</div>
										<div class="flightPageDataAncillaryText">
											<div>
												<div id="gateArrivalScheduled">
													<span class="time">${flight.arrRTTime}</span><span
														class="gmt"></span>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="flightPageDataAncillaryTextContainer">
									<div class="flightPageDataAncillaryText">
										<div>
											Airport Name: <span id="arrAirpotName"> </span>
										</div>
									</div>
									<div class="flightPageDataAncillaryText">
										<div>
											Average Delay: <span> 20-40 minutes </span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
				<div class=col-md-4 style="display: none;">
					<h3 class="h3-report" style="font-weight: 200;">Flight Details</h3>
					<%-- <h4 class="h4-report" style="font-weight:200;">Flight No:${airExportHAWBData.inAirFlightDetails.vcFlightNo}<span style="font-size:12px;Color:gray;">/${airExportHAWBData.inAirFlightDetails.vcAirlineName}</span></h4> --%>
					<table class="table table-bodered">
						<tbody>
							<tr>
								<th colspan="1">Flight No:</th>
								<td colspan="3"><b>${flight.flightNo}</b><span
									style="font-size: 12px; Color: gray;">/${flight.airlines}</span></td>
							</tr>
							<tr>
								<th>Flight Status:</th>
								<td>${flight.flightStatus}</td>
								<th>Remaining Time:</th>
								<c:if test="${ not empty flight.remainingTime}">
									<td>${flight.remainingTime}</td>
								</c:if>
								<c:if test="${empty flight.remainingTime}">
									<td>Unknown</td>
								</c:if>


							</tr>
						</tbody>
					</table>
					<!-- <hr/> -->
					<div class="box">
						<div class="box-header with-border">Departure Times</div>
						<div class="box-body">
							<table class="table table-bodered">
								<tbody>
									<tr>
										<th>Airport:</th>
										<c:if test="${ not empty flight.depAirport}">
											<td>${flight.depAirport}(${flight.depCode}),${flight.depCity}</td>
										</c:if>
										<c:if test="${empty flight.depAirport}">
											<td></td>
										</c:if>
									</tr>
									<tr>
										<th>Gate Departure:</th>
										<c:if test="${not empty flight.depGTDate}">
											<td>${flight.depGTDate},<b>${flight.depGTTime}</b> (UTC
												+ 00)
											</td>
										</c:if>
										<c:if test="${empty flight.depGTDate}">
											<td></td>
										</c:if>
									</tr>
									<tr>
										<th>Takeoff Time:</th>
										<c:if test="${not empty flight.depRTDate}">
											<td>${flight.depRTDate},<b>${flight.depRTTime}</b> (UTC
												+ 00)
											</td>
										</c:if>
										<c:if test="${empty flight.depRTDate}">
											<td></td>
										</c:if>
									</tr>
								</tbody>

							</table>
						</div>
					</div>
					<!-- <hr/> -->
					<div class="box">
						<div class="box-header with-border">Arrival Times</div>
						<div class="box-body">
							<table class="table table-bodered">
								<tbody>
									<tr>
										<th>Airport:</th>
										<c:if test="${not empty flight.arrAirport}">
											<td>${flight.arrAirport}(${flight.arrCode}),${flight.arrCity}</td>
										</c:if>
										<c:if test="${empty flight.arrAirport}">
											<td></td>
										</c:if>
									</tr>
									<tr>
										<th>Landing Time:</th>
										<c:if test="${not empty flight.arrRTDate}">
											<td>${flight.arrRTDate},<b>${flight.arrRTTime}</b> (UTC
												+ 00)
											</td>
										</c:if>
										<c:if test="${empty flight.arrRTDate}">
											<td></td>
										</c:if>
									</tr>
									<tr>
										<th>Gate Arrival:</th>
										<c:if test="${not empty flight.arrGTDate}">
											<td>${flight.arrGTDate},<b>${flight.arrGTTime}</b> (UTC
												+ 00)
											</td>
										</c:if>
										<c:if test="${empty flight.arrGTDate}">
											<td></td>
										</c:if>
									</tr>

								</tbody>

							</table>
						</div>
					</div>
					<div class="box">
						<div class="box-header with-border">Airline & Aircraft
							Information</div>
						<div class="box-body">
							<table class="table table-bodered">
								<tbody>
									<tr>
										<th>Airline Name:</th>
										<c:if test="${not empty flight.airlines}">
											<td>${flight.airlines}</td>
										</c:if>
										<c:if test="${empty flight.airlines}">
											<td></td>
										</c:if>
									</tr>
									<tr>
										<th>Aircraft Type:</th>
										<c:if test="${not empty flight.aircraft}">
											<td>${flight.aircraft}</td>
										</c:if>
										<c:if test="${empty flight.aircraft}">
											<td></td>
										</c:if>
									</tr>
									<tr>
										<th>Tail Number:</th>
										<c:if test="${not empty flight.tailNumber}">
											<td>${flight.tailNumber}</td>
										</c:if>
										<c:if test="${empty flight.tailNumber}">
											<td></td>
										</c:if>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<span style="color: grey; text-align: right;"><i>**Last
							Updated at ${flight.updateTime}</i></span>
				</div>
			</div>
			<div class=row>
				<div class=col-md-12 style="padding: 0px 20px 0px 20px">
					<div class="flightPageContainer">
						<div class="flightPageDataTableContainer">
							<!-- Departure times -->
							<h3 class="flightPageDataTableHeading">Activity Logs</h3>
							<div class="flightPageDataTable">
								<table class="table table-bodered">
									<thead>
										<tr>
											<th>Leg no</th>
											<th>Flight No</th>
											<th>Flight Status</th>
											<th>Departure</th>
											<th>Arrival</th>
											<th>Take Off</th>
											<th>Landing</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${airExportHAWBData.flightDetails}"
											var="vSchedule" varStatus="loopCounter">
											<tr>
												<td>${vSchedule.leg}</td>
												<td>${vSchedule.vcFlightNo}</td>
												<td><c:choose>
														<c:when test="${vSchedule.vcFlightStatus == 'INGATE'}">
                                                        Landed
                                                    </c:when>
														<c:when test="${vSchedule.vcFlightStatus == 'SCHEDULED'}">
                                                        Scheduled to Fly
                                                    </c:when>
														<c:when test="${vSchedule.vcFlightStatus == 'PROPOSED'}">
                                                        Scheduled to Fly
                                                    </c:when>
														<c:when test="${vSchedule.vcFlightStatus == 'INAIR'}">
                                                        Flying
                                                    </c:when>
														<c:when test="${vSchedule.vcFlightStatus == 'OUTGATE'}">
                                                        Ready to Fly
                                                    </c:when>
														<c:when test="${vSchedule.vcFlightStatus == 'LANDED'}">
                                                        Landed
                                                    </c:when>
														<c:when test="${vSchedule.vcFlightStatus == 'CANCELLED'}">
                                                        Flight Cancelled
                                                    </c:when>
														<c:when test="${vSchedule.vcFlightStatus == 'DELAYED'}">
                                                        Flight Delayed
                                                    </c:when>
														<c:otherwise>
                                                        ${vSchedule.vcFlightStatus}
                                                    </c:otherwise>
													</c:choose></td>
												<td>${vSchedule.vcDepAirportName}</td>
												<td>${vSchedule.vcArrAirportName}</td>
												<td>${vSchedule.dtDepGtActDate}
													${vSchedule.vcDepGtActTime}</td>
												<td>${vSchedule.dtArrGtActDate}
													${vSchedule.vcArrGtActTime}</td>
											</tr>
										</c:forEach>

									</tbody>


								</table>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
		<br> <br>
	</div>
</div>
		<script async defer
			src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCoxe6JBxhTnpG_LlHQAHHImMnqn3Sx4hk&callback=initMap">
			
		</script>
		<script>
			$('a[data-toggle="tab"]').on('show.bs.tab', function(e) {
				localStorage.setItem('activeTab', $(e.target).attr('href'));
			});
			/* var activeTab = localStorage.getItem('activeTab');
			if (activeTab) {
			    $('#trackTab a[href="' + activeTab + '"]').tab('show');
			} */
		</script>


		<br> <br>
		<%@include file="footer_v2.jsp"%>