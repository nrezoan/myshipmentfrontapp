<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<script
	src="${pageContext.request.contextPath}/resources/bootstrap-select/js/bootstrap-select.min.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bootstrap-select/css/bootstrap-select.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/dsrForm.css">
<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/timeline.css" />
<head>
<style>
html, body {
	background-color: #FFF;
}
.select2{
    height: 33px;
    width: 100% !important;
    padding-left: 10px;
}
</style>
</head>
<section class="main-body">
	<div class="container-fluid">
		<div class="nav-tabs-custom-949974">
			<section class="content-header"
				style="display: display; margin-bottom: 10px">
				<legend>HAWB Information</legend>
			</section>
		</div>
		<div class="row">
			<div class="col-md-12">
				<span class="bl-information-header">B/L Information</span>
				<hr>
			</div>
			<div class="col-md-12">
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">HBL/HAWB No.</label>
						</div>
						<div class="col-md-12">
							<span>${airExportHAWBData.myshipmentTrackHeader[0].bl_no} </span>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">MBL/MAWB No.</label>
						</div>
						<div class="col-md-12">
							<span>${airExportHAWBData.myshipmentTrackHeader[0].mbl_no }</span>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">Commercial Invoice No</label>
						</div>
						<div class="col-md-12">
							<span>${airExportHAWBData.myshipmentTrackHeader[0].comm_inv}</span>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">Commercial Invoice Date</label>
						</div>
						<div class="col-md-12">
							<span><fmt:parseDate
									value="${airExportHAWBData.myshipmentTrackHeader[0].comm_inv_dt}"
									var="inv_date" pattern="yyyyMMdd" /> <fmt:formatDate
									pattern="dd-MM-yyyy" type="date" value="${inv_date}" /></span>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<br />
			</div>
			<div class="col-md-12">
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">Total Gross Weight</label>
						</div>
						<div class="col-md-12">
							<span>${airExportHAWBData.myshipmentTrackHeader[0].grs_wt }</span>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">Total Chargeable Weight</label>
						</div>
						<div class="col-md-12">
							<span>${airExportHAWBData.myshipmentTrackHeader[0].crg_wt }</span>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">Total Volume</label>
						</div>
						<div class="col-md-12">
							<span>${airExportHAWBData.myshipmentTrackHeader[0].volume }</span>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">Cargo Hand-over Date</label>
						</div>
						<div class="col-md-12">
							<span><fmt:parseDate
									value="${airExportHAWBData.myshipmentTrackHeader[0].gr_dt}"
									var="gr_date" pattern="yyyyMMdd" /> <fmt:formatDate
									pattern="dd-MM-yyyy" type="date" value="${gr_date}" /></span>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<br />
			</div>
			<div class="col-md-12">
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">Port of Loading</label>
						</div>
						<div class="col-md-12">
							<span>${airExportHAWBData.myshipmentTrackHeader[0].pol}</span>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">Port of Discharge</label>
						</div>
						<div class="col-md-12">
							<span>${airExportHAWBData.myshipmentTrackHeader[0].pod }</span>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">Place of Delivery</label>
						</div>
						<div class="col-md-12">
							<span>${airExportHAWBData.myshipmentTrackHeader[0].plod }</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<%-- 	<div class="container-fluid">
		<div class="panel-group" id="accordion">
			<div class="panel">
				<div class="panel-heading">
					<h4 class="panel-title">
						<a class="" data-toggle="collapse" data-parent="#accordion"
							href="#collapseOne"> <span>ITEM DETAILS</span> <!-- <span class="glyphicon glyphicon-minus-sign" style="float: right;"></span> -->
						</a>
					</h4>
				</div>
				<div id="collapseOne" class="panel-collapse collapse in">
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-hover table-inverse">
								<thead>
									<tr>
										<th>Sl</th>
										<th>PO No.</th>
										<th>Commodity</th>
										<th>Style</th>
										<th>Size</th>
										<th>SKU</th>
										<th>Article No.</th>
										<th>Color</th>
										<th>Pieces</th>
										<th>Carton</th>
										<th>Vol.(CBM)</th>
										<th>Weight</th>
									</tr>
								</thead>
								<tbody>
								<tbody>
									<c:forEach items="${airExportHAWBData.myshipmentTrackItem}"
										var="item" varStatus="loopCounter">
										<tr>
											<td scope="row">${loopCounter.count}</td>
											<td>${item.po_no}</td>
											<td>${item.commodity}</td>
											<td>${item.style}</td>
											<td>${item.size}</td>
											<td>${item.sku}</td>
											<td>${item.art_no}</td>
											<td>${item.color}</td>
											<td>${item.item_pcs}</td>
											<fmt:parseNumber var="itmpcs" integerOnly="true"
												type="number" value="${item.item_pcs}" />
											<td>${itmpcs}</td>
											<td>${item.item_qty}</td>
											<fmt:parseNumber var="itmqty" integerOnly="true"
												type="number" value="${item.item_qty}" />
											<td>${itmqty}</td>
											<td>${item.item_vol}</td>
											<td>${item.item_grwt}</td>
										</tr>
									</c:forEach>
								</tbody>
								</tbody>

							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> --%>
	<div class="row">
		<div class="col-md-12">
			<br />
		</div>
	</div>
	<form:form action="dsrFormDetails" method="post"
			commandName="airExportHAWBData" id="airDSRForm">
		<div class="row">
			<div class="col-md-12">
				<span class="bl-information-header">Shipment Information</span>
				<hr>
			</div>

			<div class="col-md-12">
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">HBL/HAWB No 2</label>
						</div>
						<div class="col-md-12">
							<form:input type="text" path="hawb_2" 
								class="form-control color-change"  />
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">MBL/MAWB No 2</label>
						</div>
						<div class="col-md-12">
							<form:input type="text" path="mawb_2" 
								class="form-control color-change"  />
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">Invoice No 2</label>
						</div>
						<div class="col-md-12">
							<form:input type="text" path="rsas_invoice" 
								class="form-control color-change"  />
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">Shipment Mode</label>
						</div>
						<div class="col-md-12">
							<form:input type="text" path="shipment_mode" 
								class="form-control color-change"  />
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-12">
				<br />
			</div>
			<div class="col-md-12">
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">Cargo Status</label>
						</div>
						<div class="col-md-12">
							<form:select class="select2 predictable-select" path="cargo_status">
								<option selected value="pending">Pending</option>
								<option value="dispatched">Dispatched</option>

							</form:select>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">ETD</label>
						</div>
						<div class="col-md-12">
							<form:input type="text" path="etd_updated" 
								class="form-control date-picker"  />
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">ETA</label>
						</div>
						<div class="col-md-12">
							<form:input type="text" path="eta_updated" 
								class="form-control date-picker"  />
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">ETA By Airlines</label>
						</div>
						<div class="col-md-12">
							<form:input type="text" path="eta_airlines" 
								class="form-control date-picker"  />
						</div>
					</div>
				</div>
			
			</div>
			<div class="col-md-12">
				<br />
			</div>
			<div class="col-md-12">
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">ATD ex Origin</label>
						</div>
						<div class="col-md-12">
							<form:input type="text" path="atd_origin" 
								class="form-control date-picker"  />
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">ATA ex Transhipment</label>
						</div>
						<div class="col-md-12">
							<form:input type="text" path="ata_trship" 
								class="form-control date-picker"  />
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">ATD ex Transhipment</label>
						</div>
						<div class="col-md-12">
							<form:input type="text" path="atd_trship" 
								class="form-control date-picker"  />
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">ATA</label>
						</div>
						<div class="col-md-12">
							<form:input type="text" path="ata_updated" 
								class="form-control date-picker"  />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<br />
		</div>
		<div class="row">
			<div class="col-md-12">
				<span class="bl-information-header">Item Information</span>
				<hr>
			</div>
			<div class="col-md-12">
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">Stillage/Box-Type</label>
						</div>
						<div class="col-md-12">
							<form:input type="text" path="stillage" 
								class="form-control color-change"  />
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">Part Reference</label>
						</div>
						<div class="col-md-12">
							<form:input type="text" path="part_reference" 
								class="form-control color-change"  />
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-12">
			<br />
		</div>
		<div class="row">
			<div class="col-md-12">
				<span class="bl-information-header">Other Information</span>
				<hr>
			</div>
			<div class="col-md-12">
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">Pre-alert Date</label>
						</div>
						<div class="col-md-12">
							<form:input type="text" path="pre_Alert_dt" 
								class="form-control date-picker"  />
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">Die-after Date</label>
						</div>
						<div class="col-md-12">
							<form:input type="text" path="die_after_dt" 
								class="form-control date-picker"  />
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">Scroll Date</label>
						</div>
						<div class="col-md-12">
							<form:input type="text" path="scroll_dt" 
								class="form-control date-picker"  />
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">Post shipment DOC Dispatch Date</label>
						</div>
						<div class="col-md-12">
							<form:input type="text" path="doc_dispatch_dt" 
								class="form-control date-picker"  />
						</div>
					</div>
				</div>
				
			</div>
			<div class="col-md-12">
				<br />
		 	</div>
		 	<div class="col-md-12">
		 		<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">SPC Date</label>
						</div>
						<div class="col-md-12">
							<form:input type="text" path="spc_dt" 
								class="form-control date-picker"  />
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">DOC Date</label>
						</div>
						<div class="col-md-12">
							<form:input type="text" path="doc_dt" 
								class="form-control date-picker"  />
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">Cost-Center</label>
						</div>
						<div class="col-md-12">
							<form:input type="text" path="costCenter" 
								class="form-control color-change"  />
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">Air freight Cost</label>
						</div>
						<div class="col-md-12">
							<form:input type="text" path="airFreight_cost" 
								class="form-control color-change"  />
						</div>
					</div>
				</div>
		 	</div>
		 	<div class="col-md-12">
				<br />
		 	</div>
		 	<div class="col-md-12">
		 		<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">DAS No</label>
						</div>
						<div class="col-md-12">
							<form:input type="text" path="das" 
								class="form-control color-change"  />
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">Lead Time</label>
						</div>
						<div class="col-md-12">
							<form:input type="text" path="lead_time" 
								class="form-control color-change"  />
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">Dwell Time-Target</label>
						</div>
						<div class="col-md-12">
							<form:input type="text" path="dwell_target" 
								class="form-control color-change"  />
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class='form-group'>
						<div class="col-md-12">
							<label class="text-center">Reasons For Air-Shipment</label>
						</div>
						<div class="col-md-12">
							<form:textarea id="reason_for_shipment" path="reason_for_shipment" style=""
								class="textarea" tabindex="36" maxlength="500"
								autocapitalize="sentences" autocorrect="on"></form:textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row"
			style="float: right; margin-right: 2%; margin-top: 2%;">
			<!-- 	<button type="button" id="submitBtn" class="btn btn-primary">Save</button> -->
			<input type="submit" id="dsrSave" class="btn btn-primary submit"
				value="Submit"
				style="background-color: #337ab7; height: 30px; margin-right: 5px;">
		</div>
	</form:form>
</section>
<script>
	$(document).ready(function() {
		$(".date-picker").datepicker({
			dateFormat : "dd-mm-yy"
		})
	});
</script>
<%@include file="footer_v2.jsp"%>