<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE-edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<title>Sea Export HBL Tracking Details</title>
<!-- Bootstrap CSS -->
<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.5/css/bootstrap.min.css"> -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/myshipment_v2/header-footer.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/myshipment_v2/credentials.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/myshipment_v2/responsive.css">


<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css">
<%-- <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/myshipment_v2/animate/animate.css"> --%>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/sea-export.css" />
<link
	href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,600,700'
	rel='stylesheet' type='text/css'>
<!-- <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"	rel="stylesheet" type="text/css" /> -->
<link
	href="${pageContext.request.contextPath}/resources/myshipment_landing/css/footer-before-login.css"
	rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/timeline.css" />
<script
	src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.7.2/modernizr.js"
	type="text/javascript"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script>
	$(document)
			.ready(
					function() {
						var status = "${seaExportHBLData.myshipmentTrackHeader[0].status}";
						if (status === "Order Booked") {
							document.getElementById("order-booked").className = "li-order-booked-003439 complete";
						} else if (status === "GR Done") {
							document.getElementById("order-booked").className = "li-order-booked-003439 complete";
							document.getElementById("gr-done").className = "li-gr-done-137915 complete";
						} else if (status === "Stuffing Done") {
							document.getElementById("order-booked").className = "li-order-booked-003439 complete";
							document.getElementById("gr-done").className = "li-gr-done-137915 complete";
							document.getElementById("stuffing-done").className = "li-stuffing-done-870074 complete";
						}
						$('.collapse')
								.on(
										'shown.bs.collapse',
										function() {
											$(this)
													.parent()
													.find(
															".glyphicon-plus-sign")
													.removeClass(
															"glyphicon-plus-sign")
													.addClass(
															"glyphicon-minus-sign");
										})
								.on(
										'hidden.bs.collapse',
										function() {
											$(this)
													.parent()
													.find(
															".glyphicon-minus-sign")
													.removeClass(
															"glyphicon-minus-sign")
													.addClass(
															"glyphicon-plus-sign");
										});
					});
</script>

<!-- hamid styles -->
<style>
.table>thead>tr>td, .table>tbody>tr>td, .table>thead>tr>th, .table>tbody>tr>th
	{
	border: none;
}

.panel {
	border-color: #FFF;
}

.table>thead>tr>td, .table>tbody>tr>td, .table>thead>tr>th, .table>tbody>tr>th
	{
	border: none;
}
/* 	.panel {
    	border-color: #FFF;
    } */
html {
	height: 100% !important;
	background-color: #FFF;
}

img {
	/* max-width:100%;
	max-height:100%; */
	border: 0;
	margin-top: 0;
}

h4 {
	padding: 0px;
	font-size: 16px;
}
</style>


</head>

<body>
	<div>
		<nav class="navbar credentials-nav-356369">
			<div class="row row-margin-unset-all">
				<div class="col-md-12">
					<div class="col-md-3">

						<div class=""></div>
						<div class="">
							<a class="navbar-brand-295154" href="/"> <img
								class="nav-img-447749"
								src="${pageContext.request.contextPath}/resources/img/logo-xs.png" /></a>
						</div>
						<div class=""></div>

					</div>
					<div class="col-md-6"></div>
					<div class="col-md-3">
						<div id="navigation">
							<ul class="nav navbar-nav" id="top-nav">
								<li class="nav-item"><a class="navbar-brand-455723" href="/MyShipmentFrontApp/">
										HOME
								</a></li>
								<%-- <li class="nav-item"><a class="nav-link" href="${pageContext.request.contextPath}/signin"> <img src="${pageContext.request.contextPath}/resources/images/home-1.png" title="Back to Home">
										LOGIN
								</a></li> --%>
							</ul>
						</div>
					</div>
					<div class=""></div>

					<%-- 					<a class="navbar-brand" href="#"><img
						src="${pageContext.request.contextPath}/resources/images/logo-xs.png" /></a> --%>

				</div>
			</div>
		</nav>
		<!--HEADER-->


		<!-- CREDENTIALS -->
		<!--         <section id="credential" style="height:auto;">
            <div class="container text-xs-center">
                <div class="row">
                    <div class="col-lg-12">
                        <h2>credentials</h2>
                        <div class="content-title-underline-body"></div>
                        <h5>Forgot Username/Password? <br> <br>Fill-up the details below to receive the credentials on your registered email-id</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="col-lg-4"></div>
                        <div class="col-lg-4">
                            <form>
                                <fieldset class="form-group">
                                    <input type="text" class="form-control form-control-sm myName" id="name" placeholder="Customer ID...">
                                </fieldset>
                                <fieldset class="form-group">
                                    <input type="email" class="form-control form-control-sm myEmail" id="email" placeholder="E-mail...">
                                </fieldset>
                                <button type="submit" class="btn btn-info btn-block">Submit</button>
                            </form>
                        </div>
                        <div class="col-lg-4"></div>
                    </div>
                </div>
            </div>
        </section> -->
		<!-- END CONTACT -->

		<section>
			<div class="container" id="track-ship-header">
				<div class="page-header"
					style="margin: 10px 0 10px; padding-bottom: 0px;">
					<!-- <h1>Sea Export HBL Tracking Details</h1> -->
					<div class="row" style="margin-bottom: 10px;">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="col-md-6 col-sm-12 col-xs-12">
								<h2>Sea Export HBL Tracking</h2>
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12">
								<div id="company-name">
									<h4>${seaExportHBLData.myshipmentTrackHeader[0].sales_org_desc}</h4>
								</div>
							</div>
							<div class="col-md-2 col-sm-12 col-xs-12">
								<img id="img-comp-logo"
									src="${pageContext.request.contextPath}/resources/images/${seaExportHBLData.myshipmentTrackHeader[0].comp_code}.jpg"
									alt="Company Logo" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section class="milestone-track-section-254196">
			<div class="container" id="track-ship-summary">
				<div class="panel-group">
					<div class="panel panel-default"
						style="border-bottom: 3px solid #007AA7; border-bottom: 2px solid #007AA7; margin-bottom: 0px; border-radius: 0px;">
						<%-- <div   style="font-weight: bold;">
							<div class="row">
								<div class="col-md-12 col-xs-12 col-sm-12">
									<div class="col-md-4 col-xs-12 col-sm-12">
										<h5>HBL NO. :
											${seaExportHBLData.myshipmentTrackHeader[0].bl_no }</h5>
									</div>
									<div class="col-md-4">
										<h5>STATUS :
											${seaExportHBLData.myshipmentTrackHeader[0].status}</h5>
									</div>
									<div class="col-md-4">
										<h5>
											ETA :
											<fmt:parseDate
												value="${seaExportHBLData.myshipmentTrackHeader[0].eta_dt}"
												var="eta_date" pattern="yyyyMMdd" />
											<fmt:formatDate pattern="dd-MM-yyyy" type="date"
												value="${eta_date}" />
										</h5>
									</div>
								</div>
							</div>
						</div> --%>


						<div class="panel-heading-217226" style="font-weight: bold;">
							<div class="row">
								<div class="col-md-12">
									<div class="col-md-4">
										<div style="padding: 0px 0px 5px 0px;">HBL NO. :
											${seaExportHBLData.myshipmentTrackHeader[0].bl_no }</div>
									</div>
									<div class="col-md-4">
										<div style="padding: 0px 0px 5px 0px;">STATUS :
											${seaExportHBLData.myshipmentTrackHeader[0].status}</div>
									</div>
									<div class="col-md-4">
										<div style="padding: 0px 0px 5px 0px;">
											ETA :
											<c:choose>
												<c:when
													test="${empty seaExportHBLData.myshipmentTrackHeader[0].eta_dt}">
        			N/A
    			</c:when>
												<c:otherwise>
													<fmt:parseDate
														value="${seaExportHBLData.myshipmentTrackHeader[0].eta_dt}"
														var="eta_date" pattern="yyyyMMdd" />
													<fmt:formatDate pattern="dd-MM-yyyy" type="date"
														value="${eta_date}" />
												</c:otherwise>
											</c:choose>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- <div class="panel-heading" style="background-color: #00ADF1; color: #FFF;margin-bottom: 0px;border-radius: 0px;">						
					</div> -->
						<%-- <div class="panel-body panel-body-422432">
							<ul class="timeline-591271" id="timeline-591271">

								<li id="order-booked" class="li-order-booked-003439">
									<div class="timestamp-194858" style="margin-bottom: 15px;">
										<span> <c:choose>
												<c:when
													test="${empty seaExportHBLData.myshipmentTrackHeader[0].bl_bt}">
        			N/A
    			</c:when>
												<c:otherwise>
													<fmt:parseDate
														value="${seaExportHBLData.myshipmentTrackHeader[0].bl_bt}"
														var="bl_date" pattern="yyyyMMdd" />
													<fmt:formatDate pattern="dd-MM-yyyy" type="date"
														value="${bl_date}" />
												</c:otherwise>
											</c:choose>
										</span>
									</div>
									<div class="status-662147">
										<h4>Order Booked</h4>
									</div>
								</li>
								<li id="gr-done" class="li-gr-done-137915 ">
									<div class="timestamp-194858" style="margin-bottom: 15px;">
										<span> <c:choose>
												<c:when
													test="${empty seaExportHBLData.myshipmentTrackHeader[0].gr_dt}">
        			N/A
    			</c:when>
												<c:otherwise>
													<fmt:parseDate
														value="${seaExportHBLData.myshipmentTrackHeader[0].gr_dt}"
														var="gr_date" pattern="yyyyMMdd" />
													<fmt:formatDate pattern="dd-MM-yyyy" type="date"
														value="${gr_date}" />
												</c:otherwise>
											</c:choose>
										</span>
									</div>
									<div class="status-662147">
										<h4>Goods Received</h4>
									</div>
								</li>
								<li id="stuffing-done" class="li-stuffing-done-870074 ">
									<div class="timestamp-194858" style="margin-bottom: 15px;">
										<span> <c:choose>
												<c:when
													test="${empty seaExportHBLData.myshipmentTrackHeader[0].stuff_dt}">
        			N/A
    			</c:when>
												<c:otherwise>
													<fmt:parseDate
														value="${seaExportHBLData.myshipmentTrackHeader[0].stuff_dt}"
														var="stuff_date" pattern="yyyyMMdd" />
													<fmt:formatDate pattern="dd-MM-yyyy" type="date"
														value="${stuff_date}" />
												</c:otherwise>
											</c:choose>
										</span>
									</div>
									<div class="status-662147">
										<h4>Stuffing Done</h4>
									</div>
								</li>

							</ul>
						</div> --%>

						<div class="panel-body">
							<ul class="timeline-591271" id="timeline-591271">

								<li id="order-booked" class="li-order-booked-003439">
									<div class="timestamp-194858">
										<h4 style="margin-top: 20px; margin-bottom: 0px;">
											<span><b>Order Booked</b></span>
										</h4>
										<div class="image-68">
											<img
												src="${pageContext.request.contextPath}/resources/images/orderBooking.png" style="width:75px;height:75px;"
												title="ORDER BOOKED">
										</div>

									</div>
									<div class="status-662147">
										<h4>
											<span> <c:choose>
													<c:when
														test="${empty seaExportHBLData.myshipmentTrackHeader[0].bl_bt}">
        			N/A
    			</c:when>
													<c:otherwise>
														<fmt:parseDate
															value="${seaExportHBLData.myshipmentTrackHeader[0].bl_bt}"
															var="bl_date" pattern="yyyyMMdd" />
														<fmt:formatDate pattern="dd-MM-yyyy" type="date"
															value="${bl_date}" />
													</c:otherwise>
												</c:choose>
											</span>
										</h4>
									</div>
								</li>
								<li id="gr-done" class="li-gr-done-137915 ">
									<div class="timestamp-194858">
										<h4 style="margin-top: 20px; margin-bottom: 0px;">
											<span><b>Goods Received</b></span>
										</h4>
										<div class="image-68">
											<c:choose>
												<c:when
													test="${seaExportHBLData.myshipmentTrackHeader[0].status=='GR Done'}">
													<img
														src="${pageContext.request.contextPath}/resources/images/goodshandover.png" style="width:75px;height:75px;"
														title="GR DONE">
												</c:when>
												<c:when
													test="${seaExportHBLData.myshipmentTrackHeader[0].status=='Stuffing Done'}">
													<img
														src="${pageContext.request.contextPath}/resources/images/goodshandover.png" style="width:75px;height:75px;"
														title="GR DONE">
												</c:when>
												<c:otherwise>
													<img
														src="${pageContext.request.contextPath}/resources/images/goodshandover-2.png" style="width:75px;height:75px;"
														title="GR DONE">
												</c:otherwise>
											</c:choose>
										</div>
									</div>
									<div class="status-662147">
										<h4>
											<span><c:choose>
													<c:when
														test="${empty seaExportHBLData.myshipmentTrackHeader[0].gr_dt}">
        			N/A
    			</c:when>
													<c:otherwise>
														<fmt:parseDate
															value="${seaExportHBLData.myshipmentTrackHeader[0].gr_dt}"
															var="gr_date" pattern="yyyyMMdd" />
														<fmt:formatDate pattern="dd-MM-yyyy" type="date"
															value="${gr_date}" />
													</c:otherwise>
												</c:choose> </span>
										</h4>
									</div>
								</li>
								<li id="stuffing-done" class="li-stuffing-done-870074 ">
									<div class="timestamp-194858">
										<h4 style="margin-top: 20px; margin-bottom: 0px;">
											<span><b>Stuffing Done</b></span>
										</h4>
										<div class="image-68">
											<c:choose>
												<c:when
													test="${seaExportHBLData.myshipmentTrackHeader[0].status=='Stuffing Done'}">
													<img
														src="${pageContext.request.contextPath}/resources/images/stuffing.png" style="width:75px;height:75px;"
														title="STUFFING DONE">
												</c:when>
												<c:otherwise>
													<img
														src="${pageContext.request.contextPath}/resources/images/stuffing-2.png" style="width:75px;height:75px;"
														title="STUFFING DONE">
												</c:otherwise>
											</c:choose>
										</div>
									</div>
									<div class="status-662147">

										<h4>
											<span> <c:choose>
													<c:when
														test="${empty seaExportHBLData.myshipmentTrackHeader[0].stuff_dt}">
        			N/A
    			</c:when>
													<c:otherwise>
														<fmt:parseDate
															value="${seaExportHBLData.myshipmentTrackHeader[0].stuff_dt}"
															var="stuff_date" pattern="yyyyMMdd" />
														<fmt:formatDate pattern="dd-MM-yyyy" type="date"
															value="${stuff_date}" />
													</c:otherwise>
												</c:choose>
											</span>
										</h4>
									</div>
								</li>

							</ul>
						</div>

						<div class="panel-footer-217226" style="font-weight: bold;">
							<div class="row">
								<div class="col-md-12" style="text-align: center;">
									<div class="col-md-5">
										<div style="padding: 5px 0px 0px 0px; text-align: center;">Shipper
											: ${seaExportHBLData.myshipmentTrackHeader[0].shipper }</div>
									</div>
									<div class="col-md-2">
										<div style="padding: 0px 0px 0px 0px; text-align: center;">
										</div>
									</div>
									<div class="col-md-5">
										<div style="padding: 5px 15px 5px 0px; text-align: center">Consignee
											: ${seaExportHBLData.myshipmentTrackHeader[0].buyer }</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</section>
		<!-- hamid collapse toggle -->
		<div class="container">
			<div class="panel-group" id="accordion">
				<div class="panel">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="" data-toggle="collapse" data-parent="#accordion"
								href="#collapseOne"> <span>CONSIGNMENT DETAILS</span> <!-- <span class="glyphicon glyphicon-minus-sign" style="float: right;"></span> -->
							</a>
						</h4>
					</div>
					<div id="collapseOne" class="panel-collapse collapse in">
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table table-hover table-inverse">

									<tbody>
										<tr>
											<th scope="row">HBL No.</th>
											<td>${seaExportHBLData.myshipmentTrackHeader[0].bl_no }</td>
											<th scope="row">Commercial Invoice No.</th>
											<%-- <td>${seaExportHBLData.myshipmentTrackHeader[0].comm_inv }</td> --%>
											<td>-</td>
										</tr>
										<tr>
											<th scope="row">HBL Date</th>
											<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${seaExportHBLData.myshipmentTrackHeader[0].bl_bt_openTracking }" /></td> --%>
											<%-- <td>${seaExportHBLData.myshipmentTrackHeader[0].bl_bt }</td>  --%>
											<fmt:parseDate
												value="${seaExportHBLData.myshipmentTrackHeader[0].bl_bt}"
												var="bl_date" pattern="yyyyMMdd" />
											<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
													value="${bl_date}" /></td>

											<th scope="row">Commercial Invoice Date</th>
											<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${seaExportHBLData.myshipmentTrackHeader[0].comm_inv_dt_openTracking }" /></td> --%>

											<%-- <td>${seaExportHBLData.myshipmentTrackHeader[0].comm_inv_dt }</td> --%>
											<!-- following codes commented -->
											<%-- <fmt:parseDate
												value="${seaExportHBLData.myshipmentTrackHeader[0].comm_inv_dt}"
												var="comm_inv_date" pattern="yyyyMMdd" />
											<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
													value="${comm_inv_date}" /></td> --%>
											<td>-</td>
										</tr>
										<tr>
											<th scope="row">Place of Receipt</th>
											<%-- <td>${seaExportHBLData.myshipmentTrackHeader[0].por }</td> --%>
											<td>-</td>	
											<th scope="row">Cargo Received Date</th>
											<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${seaExportHBLData.myshipmentTrackHeader[0].gr_dt_openTracking }" /></td> --%>
											<%-- <td>${seaExportHBLData.myshipmentTrackHeader[0].gr_dt }</td> --%>
											<%-- <fmt:parseDate
												value="${seaExportHBLData.myshipmentTrackHeader[0].gr_dt}"
												var="gr_date" pattern="yyyyMMdd" />
											<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
													value="${gr_date}" /></td> --%>
											<td>-</td>
										</tr>
										<tr>
											<th scope="row">Port Of Loading</th>
											<td>${seaExportHBLData.myshipmentTrackHeader[0].pol }</td>
											<th scope="row">MBL Number</th>
											<td>${seaExportHBLData.myshipmentTrackHeader[0].mbl_no }</td>
										</tr>
										<tr>
											<th scope="row">Port of Discharge</th>
											<td>${seaExportHBLData.myshipmentTrackHeader[0].pod }</td>
											<th scope="row">Shipping Line</th>
											<%-- <td>${seaExportHBLData.myshipmentTrackHeader[0].carrier }</td> --%>
											<td>-</td>
										</tr>
										<tr>
											<th scope="row">Place of Discharge</th>
											<td>${seaExportHBLData.myshipmentTrackHeader[0].plod }</td>
											<th scope="row">Stuffing Date</th>
											<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${seaExportHBLData.myshipmentTrackHeader[0].stuff_dt_openTracking }" /></td> --%>

											<%-- <td>${seaExportHBLData.myshipmentTrackHeader[0].stuff_dt }</td> --%>
											<fmt:parseDate
												value="${seaExportHBLData.myshipmentTrackHeader[0].stuff_dt}"
												var="stuff_date" pattern="yyyyMMdd" />
											<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
													value="${stuff_date}" /></td>
										</tr>
										<tr>
											<th scope="row">TOS</th>
											<td>${seaExportHBLData.myshipmentTrackHeader[0].frt_mode_ds }</td>
											<th scope="row">Shipped on Board</th>
											<%-- <td>${seaExportHBLData.myshipmentTrackHeader[0].dep_dt_openTracking }</td> --%>
											<%-- <td>${seaExportHBLData.myshipmentTrackHeader[0].dep_dt }</td> --%>
											<fmt:parseDate
												value="${seaExportHBLData.myshipmentTrackHeader[0].load_dt}"
												var="dep_date" pattern="yyyyMMdd" />
											<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
													value="${dep_date}" /></td>
										</tr>
										<tr>
											<th scope="row">Total Quantity</th>
											<%-- <td>${seaExportHBLData.myshipmentTrackHeader[0].tot_qty}</td> --%>
											<fmt:parseNumber var="totqty" integerOnly="true"
												type="number"
												value="${seaExportHBLData.myshipmentTrackHeader[0].tot_qty}" />
											<td>${totqty}</td>

											<th scope="row">ETD</th>
											<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${seaExportHBLData.myshipmentTrackHeader[0].dep_dt_openTracking }" /></td> --%>

											<%-- <td>${seaExportHBLData.myshipmentTrackHeader[0].dep_dt }</td> --%>
											<%-- value="${seaExportHBLData.myshipmentTrackHeader[0].load_dt}" --%>
											<fmt:parseDate
												value="${seaExportHBLData.myshipmentTrackHeader[0].dep_dt}"
												var="load_date" pattern="yyyyMMdd" />
											<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
													value="${load_date}" /></td>
										</tr>
										<tr>
											<th scope="row">Total Volume (CBM)</th>
											<td>${seaExportHBLData.myshipmentTrackHeader[0].volume }</td>
											<th scope="row">ETA</th>
											<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${seaExportHBLData.myshipmentTrackHeader[0].eta_dt_openTracking }" /></td> --%>
											<%-- <td>${seaExportHBLData.myshipmentTrackHeader[0].eta_dt }</td> --%>
											<fmt:parseDate
												value="${seaExportHBLData.myshipmentTrackHeader[0].eta_dt}"
												var="eta_date" pattern="yyyyMMdd" />
											<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
													value="${eta_date}" /></td>
										</tr>
										<tr>
											<th scope="row">Total Gross Weight</th>
											<td>${seaExportHBLData.myshipmentTrackHeader[0].grs_wt }</td>
											<th scope="row">Destination Agent</th>
											<%-- <td>${seaExportHBLData.myshipmentTrackHeader[0].agent }</td> --%>
											<td>-</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<%-- 				<div class="panel">
					<div class="panel-heading">
					<h4 class="panel-title">
						<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
							<span>ITEM DETAILS</span>
							<!-- <span class="glyphicon glyphicon-plus-sign" style="float: right;"></span> -->							
						</a>
					</h4>
					</div>
					<div id="collapseTwo" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table">

									<thead>
										<tr>
											<th>Sl</th>
											<th>PO No.</th>
											<th>Commodity</th>
											<th>Style</th>
											<th>Size</th>
											<th>SKU</th>
											<th>Article No.</th>
											<th>Color</th>
											<th>Pieces</th>
											<th>Carton</th>
											<th>Vol.(CBM)</th>
											<th>Weight</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${seaExportHBLData.myshipmentTrackItem}"
											var="item" varStatus="loopCounter">
											<tr>
												<td scope="row">${loopCounter.count}</td>
												<td>${item.po_no}</td>
												<td>${item.commodity}</td>
												<td>${item.style}</td>
												<td>${item.size}</td>
												<td>${item.sku}</td>
												<td>${item.art_no}</td>
												<td>${item.color}</td>
												<td>${item.item_pcs}</td>
												<fmt:parseNumber var="itmpcs" integerOnly="true"
													type="number" value="${item.item_pcs}" />
												<td>${itmpcs}</td>
												<td>${item.item_qty}</td>
												<fmt:parseNumber var="itmqty" integerOnly="true"
													type="number" value="${item.item_qty}" />
												<td>${itmqty}</td>
												<td>${item.item_vol}</td>
												<td>${item.item_grwt}</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div> --%>
				<div class="panel ">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a class="collapsed" data-toggle="collapse"
								data-parent="#accordion" href="#collapseThree"> <span>VESSEL
									SCHEDULE</span> <!-- <span class="glyphicon glyphicon-plus-sign" style="float: right;"></span> -->

							</a>
						</h4>
					</div>
					<div id="collapseThree" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table">

									<thead>
										<tr>
											<th>Leg</th>
											<th>Vessel Name</th>
											<!-- <th>Vessel Type</th> -->
											<th>Voyage</th>
											<th>POL</th>
											<th>POD</th>
											<th>ETD</th>
											<th>ETA</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach items="${seaExportHBLData.myshipmentTrackSchedule}"
											var="vSchedule" varStatus="loopCounter">
											<tr>
												<td scope="row">${loopCounter.count}</td>
												<td>${vSchedule.car_name}</td>
												<%-- <td>${vSchedule.car_type}</td> --%>
												<td>${vSchedule.car_no}</td>
												<td>${vSchedule.pol}</td>
												<td>${vSchedule.pod}</td>
												<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${vSchedule.etd_openTracking}" /></td> --%>
												<%-- <td>${vSchedule.etd}</td> --%>
												<fmt:parseDate value="${vSchedule.etd}" var="etd_date"
													pattern="yyyyMMdd" />
												<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
														value="${etd_date}" /></td>
												<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${vSchedule.eta_openTracking}" /></td> --%>
												<%-- <td>${vSchedule.eta}</td> --%>
												<fmt:parseDate value="${vSchedule.eta}" var="eta_date"
													pattern="yyyyMMdd" />
												<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
														value="${eta_date}" /></td>
												<!--  <td>${vSchedule.etd}</td>
      <td>${vSchedule.eta}</td>-->
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
				<%-- 				<div class="panel ">
					<div class="panel-heading">
					<h4 class="panel-title">
						<a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" >
							
								<span>CONTAINER DETAILS</span> 
								<!-- <span class="glyphicon glyphicon-plus-sign" style="float: right;"></span> -->
							
						</a></h4>
					</div>
					<div id="collapseFour" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="table-responsive">
								<table class="table ">

									<thead>
										<tr class="">
											<th>Sl</th>
											<th>Container No</th>
											<th>Seal No</th>
											<th>Container Size</th>
											<th>Mode</th>
											<th>PO No.</th>
											<th>Style</th>
											<th>Pckd Carton</th>
											<th>Packd Vol</th>
											<th>Packd Wt.</th>
										</tr>
									</thead>
									<tbody>
										<c:forEach
											items="${seaExportHBLData.myshipmentTrackContainer}"
											var="container" varStatus="loopCounter">
											<tr>
												<td scope="row">${loopCounter.count}</td>
												<td>${container.cont_no}</td>
												<td>${container.seal_no}</td>
												<td>${container.cont_size}</td>
												<td>${container.cont_mode}</td>
												<td>${container.po_no}</td>
												<td>${container.style}</td>
												<td>${container.pacd_qty}</td>
												<fmt:parseNumber var="pcdqty" integerOnly="true"
													type="number" value="${container.pacd_qty}" />
												<td>${pcdqty}</td>
												<td>${container.pacd_vol}</td>
												<td>${container.pacd_wt}</td>
											</tr>
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div> --%>
			</div>
		</div>

		<!-- hamid edit ends -->

		<!-- FOOTER -->
		<%-- 		<section id="footer-673168" class=""	>
			<div class="container wow fadeInUp" data-wow-delay="0.4s"
				data-wow-duration="1.5s">
				<div class="row footer-top">
					<div class="col-md-4 footer-follow">
						<h5>Follow Us :</h5>
						<ul class="list-inline">
							<li class="list-inline-item"><a style="color: #FFF"
								href="https://www.facebook.com/mghgroupglobal" target="_blank"><i
									class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li class="list-inline-item"><a style="color: #FFF"
								href="https://www.linkedin.com/company/mghgroup" target="_blank"><i
									class="fa fa-linkedin" aria-hidden="true"></i></a></li>
						</ul>
					</div>
					<div class="col-md-4 footer-copy">
						<p class="text-xs-center">
							&copy; myshipment.com
							<script type="text/javascript">
                                var today = new Date()
                                var year = today.getFullYear()
                                document.write(year)

                            </script>
						</p>
					</div>
					<div class="col-md-4 text-xs-center footer-app">
						<a
							href="https://itunes.apple.com/za/app/myshipment-supplier/id1168561725?mt=8"
							target="_blank"><img
							src="${pageContext.request.contextPath}/resources/images/Android-App-Store-AF.png"
							class="img-fluid" title="Get On App Store"></a> <a
							href="https://play.google.com/store/apps/details?id=com.mgh.shipment"
							target="_blank"><img
							src="${pageContext.request.contextPath}/resources/images/Android-App-Store-GF.png"
							class="img-fluid" title="Get On Google Play"></a>
					</div>
					<div id="footer">
						<div class="copy">
							<p>
								&copy;
								<script type="text/javascript">
									var today = new Date()
									var year = today.getFullYear()
									document.write(year)
								</script>
								my<span class="shipment-footer-panel">shipment</span> | All
								Rights Reserved
							</p>
						</div>
					</div>
				</div>
			</div>
		</section> --%>

		<!-- END FOOTER -->
		<div id="footer">
			<div class="copy">
				<p>
					&copy;
					<script type="text/javascript">
						var today = new Date()
						var year = today.getFullYear()
						document.write(year)
					</script>
					my<span class="shipment-footer-panel">shipment</span> | All Rights
					Reserved
				</p>
			</div>
		</div>

		<!-- FOOTER -->
		<section id="footer"
			style="background-color: #252525; display: none;<%-- background: url('${pageContext.request.contextPath}/resources/img/topheaderBG.jpg') --%> center center no-repeat;">
			<div class="container wow animated fadeInUp" data-wow-delay="0.4s"
				data-wow-duration="1.5s">
				<div class="row footer-top">
					<div class="col-sm-4 footer-follow">
						<h5>Follow Us :</h5>
						<ul class="list-inline">
							<li class="list-inline-item"><a
								href="https://www.facebook.com/mghgroupglobal" target="_blank"><i
									class="fa fa-facebook" aria-hidden="true"></i></a></li>
							<li class="list-inline-item"><a
								href="https://www.linkedin.com/company/mghgroup" target="_blank"><i
									class="fa fa-linkedin" aria-hidden="true"></i></a></li>
						</ul>
					</div>
					<div class="col-sm-3 footer-copy">
						<p class="">
							&copy; myshipment.com
							<script type="text/javascript">
								var today = new Date()
								var year = today.getFullYear()
								document.write(year)
							</script>
						</p>
					</div>
					<div class="col-sm-5 text-xs-center footer-app">
						<a
							href="https://itunes.apple.com/za/app/myshipment-supplier/id1168561725?mt=8"
							target="_blank"><img
							src="${pageContext.request.contextPath}/resources/img/Android-App-Store-AF.png"
							class="img-fluid" title="Get On App Store"></a> <a
							href="https://play.google.com/store/apps/details?id=com.mgh.shipment"
							target="_blank"><img
							src="${pageContext.request.contextPath}/resources/img/Android-App-Store-GF.png"
							class="img-fluid" title="Get On Google Play"></a>
					</div>
				</div>
			</div>
		</section>
	</div>

</body>