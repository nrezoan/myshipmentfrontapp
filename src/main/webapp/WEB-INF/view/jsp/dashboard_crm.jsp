<!-- header -->
<%@include file="header_v2.jsp"%>
<!-- navbar -->
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!-- Theme style -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

<script type="text/javascript">
	var loginDTO = loginDetails;
	var dashboardInfoBox = ${dashboardInfoBox};

	var hblWisePreAlert = ${preAlertHblDetailsByBuyerList};
	var mblWisePreAlert = ${mblWiseHbl};
</script>



<div class="entire-content-c">
	<!-- <br><br><br> -->

	<input id="dashboard_from_date" type="hidden"
		class="date-picker glyphicon glyphicon-calendar dashboard-ship-frm-date"
		style="width: 160px;" /> <input id="dashboard_to_date" type="hidden"
		class="date-picker glyphicon glyphicon-calendar dashboard-ship-to-date"
		style="width: 160px;" />

	<section class="content" style="padding: 30px;">

		<div class="row dashboard-box-menu-row-gap">
			<div class="col-md-2 col-sm-6 col-xs-12">
				<div class="info-box wow animated zoomIn" data-wow-delay="0.3s"
					id="total-bookings">
					<span class="info-box-icon total-bookings-box"><i
						class="fa fa-envelope"></i></span>
					<div class="info-box-content">
						<span class="info-box-text" id="totalBookings-title"></span> <span
							class="dashboard-info-box-number" id="totalBooking-number"></span>
						<span class="progress-description" id="totalBooking-dateRange"></span>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-sm-6 col-xs-12">
				<div class="info-box wow animated zoomIn" data-wow-delay="0.3s"
					id="open-bookings">
					<span class="info-box-icon open-bookings-box"><i
						class="fa fa-envelope-open"></i></span>
					<div class="info-box-content">
						<span class="info-box-text" id="openBookings-title"></span> <span
							class="dashboard-info-box-number" id="openBookings-number"></span>
						<span class="progress-description" id="openBookings-dateRange"></span>
					</div>
				</div>
			</div>
			<div class="clearfix visible-sm-block"></div>
			<div class="col-md-2 col-sm-6 col-xs-12">
				<div class="info-box wow animated zoomIn" data-wow-delay="0.3s"
					id="goods-hand-over">
					<span class="info-box-icon goods-handover-box"><i
						class="fa fa-handshake-o"></i></span>

					<div class="info-box-content">
						<span class="info-box-text" id="goodsHandOver-title"></span> <span
							class="dashboard-info-box-number" id="goodsHandOver-number"></span>
						<span class="progress-description" id="goodsHandOver-dateRange"></span>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-sm-6 col-xs-12">
				<div class="info-box wow animated zoomIn" data-wow-delay="0.3s"
					id="stuffing-done">
					<span class="info-box-icon stuffing-done-box"><i
						class="fa fa-archive"></i></span>

					<div class="info-box-content">
						<span class="info-box-text" id="stuffingDone-title"></span> <span
							class="dashboard-info-box-number" id="stuffingDone-number"></span>
						<span class="progress-description" id="stuffingDone-dateRange"></span>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-sm-6 col-xs-12">
				<div class="info-box wow animated zoomIn" data-wow-delay="0.3s"
					id="in-transit">
					<span class="info-box-icon in-transit-box"><i
						id="inTransit-image" class=""></i></span>
					<div class="info-box-content">
						<span class="info-box-text" id="inTransit-title"></span> <span
							class="dashboard-info-box-number" id="inTransit-number"></span> <span
							class="progress-description" id="inTransit-dateRange"></span>
					</div>
				</div>
			</div>
			<div class="col-md-2 col-sm-6 col-xs-12">
				<div class="info-box wow animated zoomIn" data-wow-delay="0.3s"
					id="arrived">
					<span class="info-box-icon arrived-box"><i
						class="fa fa-check"></i></span>

					<div class="info-box-content">

						<span class="info-box-text" id="arrived-title"></span> <span
							class="dashboard-info-box-number" id="arrived-number"></span> <span
							class="progress-description" id="arrived-dateRange"></span>
					</div>
				</div>
			</div>
		</div>

		<div class="row">

			<div class="col-md-6">
				<div class="box box-myshipment">
					<div class="box-header">
						<h3 class="box-title">Pre-Alert Management</h3>
					</div>
					<div class="box-body no-padding">
						<table class="table table-striped" id="tbl-pre-alert">
							<thead>
								<tr>
									<th class="text-center">Pending Invoice/PackList</th>
									<th class="text-center">Pending MBL/MAWB</th>
									<th class="text-center">Pre-Alerts Generated</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th class="text-center" style="font-size: xx-large;"><a
										href="preAlertManagement?status=pending" id="pendingIPLNumber"
										style="color: #dd4b39 !important;"></a></th>
									<th class="text-center" style="font-size: xx-large;"><a
										href="preAlertBLwise?status=PENDING" id="pendingMBLNumber"
										style="color: #dd4b39 !important;"></a></th>
									<th class="text-center" style="font-size: xx-large;"><a
										href="preAlertBLwise?status=COMPLETED"
										id="pendingPreAlertNumber" style="color: #dd4b39 !important;"></a>
									</th>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
			<div class="col-md-6"></div>

		</div>
	</section>
</div>

<script
	src="${pageContext.request.contextPath}/resources/js/dashboard_crm.js"
	type="text/javascript"></script>

<!-- footer -->
<%@include file="footer_v2.jsp"%>