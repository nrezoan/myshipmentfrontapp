<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<jsp:include page="header_v2.jsp"></jsp:include>
<jsp:include page="navbar.jsp"></jsp:include>
<script src="${pageContext.request.contextPath}/resources/js/jszip.js"></script>
<script src="${pageContext.request.contextPath}/resources/js/xlsx.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/js/directBooking.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/bootstrap-select/js/bootstrap-select.min.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/js/bootstrap.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/js/DocParserBooking.js"></script>
<%-- <link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bootstrap-select/css/bootstrap-select.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/bootstrap.css"> --%>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/table-card.css">


<script type="text/javascript">
	var warehouseList = [];
	var whList = [];
	var planningList = [];
	var planning;
	var wh;
</script>
<c:forEach items="${whCodeLst}" var="wareHouse">
	<script type="text/javascript">
		wh = {
			pm : "${wareHouse.pm}",
			countryCode : "${wareHouse.countryCode}",
			whCode : "${wareHouse.whCode}"
		};
		warehouseList.push(wh);
		/* console.log(warehouseList); */
	</script>
</c:forEach>
<c:forEach items="${pmList}" var="p">
	<script type="text/javascript">
		planning = {
			pm : "${p}",
		};
		planningList.push(planning);
		/* console.log(planningList); */
	</script>
</c:forEach>
<style>
html {
	background-color: #fff;
	margin-bottom: 30px;
}

.input-group-addon {
	border: unset !important;
}

.nav-tabs.nav-justified>.active>a, .nav-tabs.nav-justified>.active>a:focus,
	.nav-tabs.nav-justified>.active>a:hover {
	background-color: #007aa7;
	color: #fff;
}

.tab-direct-booking-well {
	background-color: #fff !important;
}

.ul-unset-margin {
	margin-left: unset !important;
}
</style>

<!-- Nazib Testing code Starts Here -->
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
	aria-labelledby="exampleModalLabel" aria-hidden="true"
	style="padding: 0px">
	<div class="modal-dialog width-rectifier" role="document">
		<div class="modal-content">
			<div class="modal-body style-rectifier">
				<ul class="nav nav-tabs nav-justified nav_hov ul-unset-margin"
					role="tablist">
					<li role="presentation" class="active"><a href="#home"
						aria-controls="home" role="tab" data-toggle="tab">Item Detail</a></li>
				</ul>

				<!-- Tab panes -->
				<input type="hidden" name="app-id" id="app-id"
					value="${approvedPurchaseOrderId }">
				<div class="tab-content card card-rectifier">

					<div role="tabpanel" class="tab-pane active" id="home">

						<section class="">
							<div class="container-fluid ">
								<div id="itemdetail">
									<div id="first-tab">
										<div class="row first-tab-clone tab-direct-booking-well">
											<!-- Starting of first column -->
											<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
												<input type="hidden" name="itemId" id="itemId">
												<div class="forms">
													<div class="form-group">
														<div class="input-group2">
															<div class="row row-margin-unset">
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
																	<div class="input-group-addon">
																		<span>PO No:</span><span class="required-alone">*</span>
																	</div>
																</div>
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">

																	<input type="text" name="text" class="form-control"
																		id="PONumber" />

																</div>
															</div>
														</div>
													</div>
												</div>

												<div class="forms">
													<div class="form-group">
														<div class="input-group1">
															<div class="row row-margin-unset">
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
																	<div class="input-group-addon">
																		<span>Total Gross Weight:</span><span
																			class="required-alone">*</span>

																	</div>
																</div>
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">


																	<input type="text" name="text" class="form-control"
																		required id="GrossWeight"
																		onkeypress="return validateDecimalDataTypeStrict(this,event);" />


																</div>
															</div>
														</div>
													</div>
												</div>

												<div class="forms">
													<div class="form-group">
														<div class="input-group1">
															<div class="row row-margin-unset">
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
																	<div class="input-group-addon">
																		<span>Carton Length:</span><span
																			class="required-alone">*</span>
																	</div>
																</div>
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">

																	<input type="text" name="text" class="form-control"
																		id="CartonLength"
																		onkeypress="return validateDecimalDataTypeStrict(this,event);"
																		onchange="return calculateCBM();" />

																</div>
															</div>
														</div>
													</div>
												</div>

												<div class="forms">
													<div class="form-group">
														<div class="input-group1">
															<div class="row row-margin-unset">
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
																	<div class="input-group-addon">
																		<span>Carton Height:</span><span
																			class="required-alone">*</span>

																	</div>
																</div>
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">

																	<input type="text" name="text" class="form-control"
																		id="CartonHeight"
																		onkeypress="return validateDecimalDataTypeStrict(this,event);"
																		onchange="return calculateCBM();" />


																	<!-- <input type="text" name="text" class="form-control" id="orderItem_car_number" onkeyup="calculateCBM();" onkeypress="return validateDataType(this,event);"/> -->
																</div>
															</div>
														</div>
													</div>
												</div>

												<div class="forms">
													<div class="form-group">
														<div class="input-group1">
															<div class="row row-margin-unset">
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
																	<div class="input-group-addon">
																		<span>Carton Width:</span><span class="required-alone">*</span>
																	</div>
																</div>
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">


																	<input type="text" name="text" class="form-control"
																		id="CartonWidth"
																		onkeypress="return validateDecimalDataTypeStrict(this,event);"
																		onchange="return calculateCBM();" />


																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="forms">
													<div class="form-group">
														<div class="input-group1">
															<div class="row row-margin-unset">
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6"">
																	<div class="input-group-addon">
																		<span>Ref.#4/Department:</span>
																		<c:if test="${ not empty pmList}">
																			<span class="required-alone">*</span>
																		</c:if>
																	</div>
																</div>
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
																	<input type="text" name="text" class="form-control"
																		id="Line_ReferenceNo5" />
																</div>
															</div>
														</div>
													</div>
												</div>

											</div>


											<!-- End of first column -->

											<!-- Start of Second column -->
											<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">

												<div class="forms">
													<div class="form-group">
														<div class="input-group1">
															<div class="row">
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
																	<div class="input-group-addon">
																		<span>Total Pieces:</span><span class="required-alone">*</span>

																	</div>
																</div>
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">

																	<input type="text" name="text" class="form-control"
																		title="Only Numbers Allowed!"
																		onkeypress="return validateDataType(this,event);"
																		id="TotalPcs" />

																</div>
															</div>
														</div>
													</div>
												</div>

												<div class="forms">
													<div class="form-group">
														<div class="input-group2">
															<div class="row">
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
																	<div class="input-group-addon">
																		<span>Carton Quantity:</span><span
																			class="required-alone">*</span>
																	</div>
																</div>
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">


																	<input type="text" name="text" class="form-control"
																		id="TotalCarton" data-toggle="tooltip"
																		title="Only Numbers Allowed!"
																		onkeypress="return validateDataType(this,event);"
																		onchange="return calculateCBM();" />


																</div>
															</div>
														</div>
													</div>
												</div>

												<div class="forms">
													<div class="form-group">
														<div class="input-group1">
															<div class="row">
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
																	<div class="input-group-addon">
																		<span>Carton Unit:</span><span class="required-alone">*</span>
																	</div>
																</div>
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
																	<select class="form-control drop"
																		id="CartonMeasurementUnit"
																		onchange="return calculateCBM();">
																		<!-- 														onchange="return calculateCBM();" onfocus="return checkMandatoryForCBM()" > -->
																		<option value="">Select</option>
																		<option value="IN">IN</option>
																		<option selected value="CM">CM</option>

																	</select>
																</div>
															</div>
														</div>
													</div>
												</div>



												<div class="forms">
													<div class="form-group">
														<div class="input-group1">
															<div class="row">
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
																	<div class="input-group-addon">
																		<span>Total CBM:</span><span class="required-alone">*</span>

																	</div>
																</div>
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">

																	<input type="text" name="text"
																		class="form-control auto-complete" id="TotalCBM"
																		onkeypress="return validateDecimalDataTypeStrict(this,event);" />

																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="forms">
													<div class="form-group">
														<div class="input-group1">
															<div class="row">
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
																	<div class="input-group-addon">
																		<span>HS Code:</span><span class="required-alone">*</span>
																	</div>
																</div>
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">

																	<input type="text" name="text" class="form-control"
																		id="HSCode" />

																</div>
															</div>
														</div>
													</div>
												</div>


											</div>
											<!-- End of second column -->



											<!-- Start of Third column -->
											<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">


												<div class="forms">
													<div class="form-group">
														<div class="input-group2">
															<div class="row">
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
																	<div class="input-group-addon">
																		<span>Unit:</span>
																	</div>
																</div>
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
																	<select class="form-control drop select2"
																		data-live-search="true" id="ItemUoM" style="">
																		<option value="">Please Select</option>
																		<option value="KG" selected>Kilogram</option>
																		<!-- <option value="MOK">Mol/kilogram</option>
														<option value="KGM">Kilogram/Mol</option>
														<option value="MOL">Mol</option>
														<option value="MON">Months</option>
														<option value="EA">Each</option>-->
																		<!-- <option value="FT2">Square foot</option>
														<option value="FT3">Cubic foot</option>
														<option value="KGS">Kilogram/second</option>
														<option value="KGV">Kilogram/cubic meter</option>
														<option value="TM3">1/cubic meter</option> -->
																		<option value="DZ">Dozen</option>
																		<option value="JAR">JAR</option>
																		<option value="DM">Decimeter</option>
																		<option value="PWC">PLYWOOD CASE</option>
																		<option value="?GQ">Microgram/cubic meter</option>
																		<option value="?GL">Microgram/liter</option>
																		<option value="MPB">Mass parts per billion</option>
																		<option value="DR">Drum</option>
																		<option value="MPA">Megapascal</option>
																		<option value="KGF">Kilogram/Square meter</option>
																		<option value="ROL">Role</option>
																		<option value="&quot;3">Cubic inch</option>
																		<option value="&quot;2">Square inch</option>
																		<option value="KGK">Kilogram/Kilogram</option>
																		<option value="&quot;">Inch</option>
																		<option value="MNM">Millinewton/meter</option>
																		<option value="%">Percentage</option>
																		<option value="EU">Enzyme Units</option>
																		<option value="KVA">Kilovoltampere</option>
																		<option value="V%">Percent volume</option>
																		<option value="C3S">Cubic centimeter/second</option>
																		<option value="D">Days</option>
																		<option value="ACR">Acre</option>
																		<option value="KWH">Kilowatt hours</option>
																		<option value="F">Farad</option>
																		<option value="G">Gram</option>
																		<option value="MMO">Millimol</option>
																		<option value="BDR">Board Drums</option>
																		<option value="MMH">Millimeter/hour</option>
																		<option value="A">Ampere</option>
																		<option value="MMK">Millimol/kilogram</option>
																		<option value="L">Liter</option>
																		<option value="M">Meter</option>
																		<option value="N">Newton</option>
																		<option value="QML">Kilomol</option>
																		<option value="MMG">Millimol/gram</option>
																		<option value="H">Hour</option>
																		<option value="MMA">Millimeter/year</option>
																		<option value="J">Joule</option>
																		<option value="K">Kelvin</option>
																		<option value="W">Watt</option>
																		<option value="V">Volt</option>
																		<option value="A/V">Siemens per meter</option>
																		<option value="FT">Foot</option>
																		<option value="P">Points</option>
																		<option value="S">Second</option>
																		<option value="MMS">Millimeter/second</option>
																		<option value="MLK">Milliliter/cubic meter</option>
																		<option value="R-U">Nanofarad</option>
																		<option value="MLI">Milliliter act. ingr.</option>
																		<option value="EML">Enzyme Units / Milliliter</option>
																		<option value="HA">Hectare</option>
																		<option value="BUN">BUNDLES</option>
																		<option value="WK">Weeks</option>
																		<option value="LHK">Liter per 100 km</option>
																		<option value="SKD">SKIDS</option>
																		<option value="GM">Gram/Mol</option>
																		<option value="GJ">Gigajoule</option>
																		<option value="MM3">Cubic millimeter</option>
																		<option value="MM2">Square millimeter</option>
																		<option value="NAM">Nanometer</option>
																		<option value="RF">Millifarad</option>
																		<option value="M-2">1 / square meter</option>
																		<option value="VPB">Volume parts per billion</option>
																		<option value="GHG">Gram/hectogram</option>
																		<option value="%O">Per mille</option>
																		<option value="PKG">Package</option>
																		<option value="VPM">Volume parts per million</option>
																		<option value="QT">Quart, US liquid</option>
																		<option value="VPT">Volume parts per trillion</option>
																		<option value="STR">String</option>
																		<option value="AU">Activity unit</option>
																		<option value="PT">Pint, US liquid</option>
																		<option value="KJM">Kilojoule/Mol</option>
																		<option value="JMO">Joule/Mol</option>
																		<option value="KJK">Kilojoule/kilogram</option>
																		<option value="PS">Picosecond</option>
																		<option value="LMS">Liter/Molsecond</option>
																		<option value="BT">Bottle</option>
																		<option value="NMM">Newton/Square millimeter</option>
																		<option value="TO">Tonne</option>
																		<option value="PMI">1/minute</option>
																		<option value="MIJ">Millijoule</option>
																		<option value="TS">Thousands</option>
																		<option value="KIK">kg act.ingrd. / kg</option>
																		<option value="ST">items</option>
																		<option value="SET">SETS</option>
																		<option value="MIS">Microsecond</option>
																		<option value="MIN">Minute</option>
																		<option value="LMI">Liter/Minute</option>
																		<option value="UNI">Unit</option>
																		<option value="TES">Tesla</option>
																		<option value="?C">Degrees Celsius</option>
																		<option value="KHZ">Kilohertz</option>
																		<option value="CV">Case</option>
																		<option value="RLS">ROLLS</option>
																		<option value="?F">Fahrenheit</option>
																		<option value="GKG">Gram/kilogram</option>
																		<option value="000">Meter/Minute</option>
																		<option value="MI2">Square mile</option>
																		<option value="JKK">Spec. Heat Capacity</option>
																		<option value="CD">Candela</option>
																		<option value="MHZ">Megahertz</option>
																		<option value="JKG">Joule/Kilogram</option>
																		<option value="CM">Centimeter</option>
																		<option value="OHM">Ohm</option>
																		<option value="MHV">Megavolt</option>
																		<option value="CL">Centiliter</option>
																		<option value="GPH">Gallons per hour (US)</option>
																		<option value="KOH">Kiloohm</option>
																		<option value="TC3">1/cubic centimeter</option>
																		<option value="M3">Cubic meter</option>
																		<option value="M2">Square meter</option>
																		<option value="MG">Milligram</option>
																		<option value="COI">COIL</option>
																		<option value="ONE">One</option>
																		<option value="DAY">Days</option>
																		<option value="ML">Milliliter</option>
																		<option value="MI">Mile</option>
																		<option value="IDR">Iron Drum</option>
																		<option value="GOH">Gigaohm</option>
																		<option value="MA">Milliampere</option>
																		<option value="KPA">Kilopascal</option>
																		<option value="MV">Millivolt</option>
																		<option value="MGW">Megawatt</option>
																		<option value="MW">Milliwatt</option>
																		<option value="M/M">Mol per cubic meter</option>
																		<option value="MWH">Megawatt hour</option>
																		<option value="M/L">Mol per liter</option>
																		<option value="MN">Meganewton</option>
																		<option value="MGQ">Milligram/cubic meter</option>
																		<option value="MM">Millimeter</option>
																		<option value="MGO">Megohm</option>
																		<option value="M/H">Meter/Hour</option>
																		<option value="COL">COLLI</option>
																		<option value="MGL">Milligram/liter</option>
																		<option value="MGK">Milligram/kilogram</option>
																		<option value="CON">Container</option>
																		<option value="VAM">Voltampere</option>
																		<option value="VAL">value-only material</option>
																		<option value="NI">Kilonewton</option>
																		<option value="MGG">Milligram/gram</option>
																		<option value="MGE">Milligram/Square
																			centimeter</option>
																		<option value="NM">Newton/meter</option>
																		<option value="NA">Nanoampere</option>
																		<option value="M/S">Meter/second</option>
																		<option value="V%O">Permille volume</option>
																		<option value="KMH">Kilometer/hour</option>
																		<option value="CM2">Square centimeter</option>
																		<option value="M2S">Square meter/second</option>
																		<option value="PAL">Pallet</option>
																		<option value="BLK">BULK</option>
																		<option value="MVA">Megavoltampere</option>
																		<option value="NS">Nanosecond</option>
																		<option value="KMN">Kelvin/Minute</option>
																		<option value="PAA">Pair</option>
																		<option value="NO">NOS</option>
																		<option value="KMK">Cubic meter/Cubic meter</option>
																		<option value="PPT">Parts per trillion</option>
																		<option value="PAC">Pack</option>
																		<option value="OM">Spec. Elec. Resistance</option>
																		<option value="TRS">TRUSSES</option>
																		<option value="CMH">Centimeter/hour</option>
																		<option value="YD2">Square Yard</option>
																		<option value="PPM">Parts per million</option>
																		<option value="KMS">Kelvin/Second</option>
																		<option value="PPB">Parts per billion</option>
																		<option value="YD3">Cubic yard</option>
																		<option value="WDP">Wooden Pallet</option>
																		<option value="CMS">Centimeter/second</option>
																		<option value="?A">Microampere</option>
																		<option value="OZ">Ounce</option>
																		<option value="MEJ">Megajoule</option>
																		<option value="WDC">Wooden Case</option>
																		<option value="LPH">Liter per hour</option>
																		<option value="WDB">WOODEN BOX</option>
																		<option value="KM2">Square kilometer</option>
																		<option value="GM2">Gram/square meter</option>
																		<option value="GM3">Gram/Cubic meter</option>
																		<option value="?M">Micrometer</option>
																		<option value="?L">Microliter</option>
																		<option value="GLI">Gram/liter</option>
																		<option value="WCR">WOODEN CRATE</option>
																		<option value="PA">Pascal</option>
																		<option value="?F">Microfarad</option>
																		<option value="RHO">Gram/cubic centimeter</option>
																		<option value="WMK">Heat Conductivity</option>
																		<option value="CRT">Crate</option>
																		<option value="OCM">Spec. Elec. Resistance</option>
																		<option value="HL">Hectoliter</option>
																		<option value="DEG">Degree</option>
																		<option value="PRS">Number of Persons</option>
																		<option value="HR">Hours</option>
																		<option value="FOZ">Fluid Ounce US</option>
																		<option value="MTE">Millitesla</option>
																		<option value="HZ">Hertz (1/second)</option>
																		<option value="PRC">Group proportion</option>
																		<option value="MBA">Millibar</option>
																		<option value="CAR">Carton</option>
																		<option value="KBK">Kilobecquerel/kilogram</option>
																		<option value="HPA">Hectopascal</option>
																		<option value="IB">Pikofarad</option>
																		<option value="CAN">Canister</option>
																		<option value="BOX">BOXES</option>
																		<option value="GAU">Gram Gold</option>
																		<option value="M3H">Cubic meter/Hour</option>
																		<option value="PBG">POLY BAG</option>
																		<option value="M3S">Cubic meter/second</option>
																		<option value="GAL">US gallon</option>
																		<option value="MSC">Microsiemens per
																			centimeter</option>
																		<option value="YD">Yards</option>
																		<option value="FDR">FIBER DRUM</option>
																		<option value="MSE">Millisecond</option>
																		<option value="GAI">Gram act. ingrd.</option>
																		<option value="WKY">Evaporation Rate</option>
																		<option value="PAS">Pascal second</option>
																		<option value="GRO">Gross</option>
																		<option value="MBZ">Meterbar/second</option>
																		<option value="KD3">Kilogram/cubic decimeter</option>
																		<option value="22S">Square millimeter/second</option>
																		<option value="MS2">Meter/second squared</option>
																		<option value="G/L">gram act.ingrd / liter</option>
																		<option value="YR">Years</option>
																		<option value="CCM">Cubic centimeter</option>
																		<option value="KA">Kiloampere</option>
																		<option value="M%O">Permille mass</option>
																		<option value="BQK">Becquerel/kilogram</option>
																		<option value="REL">REEL</option>
																		<option value="KJ">Kilojoule</option>
																		<option value="CD3">Cubic decimeter</option>
																		<option value="KG">Kilogram</option>
																		<option value="MPZ">Meterpascal/second</option>
																		<option value="BAL">BALES</option>
																		<option value="MPS">Millipascal seconds</option>
																		<option value="MPT">Mass parts per trillion</option>
																		<option value="KM">Kilometer</option>
																		<option value="BAG">Bag</option>
																		<option value="KW">Kilowatt</option>
																		<option value="KT">Kilotonne</option>
																		<option value="KV">Kilovolt</option>
																		<option value="BRL">Barrel</option>
																		<option value="MPG">Miles per gallon (US)</option>
																		<option value="LB">US pound</option>
																		<option value="GPM">Gallons per mile (US)</option>
																		<option value="PCS">Pieces</option>
																		<option value="KAI">Kilogram act. ingrd.</option>
																		<option value="JCN">JERRICANS</option>
																		<option value="MPL">Millimol per liter</option>
																		<option value="BAR">bar</option>
																		<option value="M%">Percent mass</option>
																		<option value="TOM">Ton/Cubic meter</option>
																		<option value="MPM">Mass parts per million</option>
																		<option value="TON">US ton</option>
																	</select>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="forms">
													<div class="form-group">
														<div class="input-group1">
															<div class="row">
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
																	<div class="input-group-addon">
																		<span>Style No./PMA-PMC:</span>
																		<c:if test="${ not empty pmList}">
																			<span class="required-alone">*</span>
																		</c:if>
																	</div>
																</div>
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">


																	<!-- 	<input type="text" name="text" class="form-control"
														id="Style" /> -->
																	<c:choose>
																		<c:when test="${ not empty pmList}">
																			<select name="text"
																				class="form-control auto-complete select2"
																				data-live-search="true" id="Style"
																				onchange="sortWHCodeList();">
																				<option value="">Please Select</option>
																				<%
																					try {
																				%>
																				<c:forEach var="pm" items="${pmList}">
																					<option value="${pm}">${pm}</option>
																				</c:forEach>
																				<%
																					} catch (Exception e) {
																								e.printStackTrace();
																							}
																				%>
																			</select>
																		</c:when>
																		<c:otherwise>
																			<input type="text" name="text" class="form-control"
																				id="Style" />
																		</c:otherwise>
																	</c:choose>

																	<!-- <input type="text" name="text" class="form-control" id="orderItem_styleNo"/> -->
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="forms">
													<div class="form-group">
														<div class="input-group1">
															<div class="row">
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
																	<div class="input-group-addon">
																		<span>Ref. No/WH Code:</span>
																		<c:if test="${ not empty pmList}">
																			<span class="required-alone">*</span>
																		</c:if>
																	</div>
																</div>
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
																	<!-- <input type="text" name="text" class="form-control"
														id="ReferenceNo1" /> -->
																	<c:choose>
																		<c:when test="${ not empty pmList}">
																			<select name="text"
																				class="form-control auto-complete select2"
																				data-live-search="true" id="ReferenceNo1">
																				<option value="">Please Select</option>
																				<%-- 														<%
															try {
														%>
														<c:forEach var="w" items="${whList}">
															<option value="${w.whCode}">${w.whCode}</option>
														</c:forEach>												
														<%
															} catch (Exception e) {
																e.printStackTrace();
															}
														%> --%>
																			</select>
																		</c:when>
																		<c:otherwise>
																			<input type="text" name="text" class="form-control"
																				id="ReferenceNo1" />
																		</c:otherwise>
																	</c:choose>
																</div>
															</div>
														</div>
													</div>
												</div>


												<div class="forms">
													<div class="form-group">
														<div class="input-group1">
															<div class="row">
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
																	<div class="input-group-addon">
																		<span>Size No:</span>
																	</div>
																</div>
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
																	<input type="text" name="text" class="form-control"
																		id="Size" />
																	<!-- onkeypress="return validateDataType(this,event);" /> -->
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
												<div class="forms">
													<div class="form-group">
														<div class="input-group2">
															<div class="row">
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
																	<div class="input-group-addon">
																		<span>Prod. Code/SKU/Season:</span>
																		<c:if test="${ not empty pmList}">
																			<span class="required-alone">*</span>
																		</c:if>
																	</div>
																</div>
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
																	<input type="text" name="text" class="form-control"
																		id="ProductCode" />

																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="forms">
													<div class="form-group">
														<div class="input-group1">
															<div class="row">
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
																	<div class="input-group-addon">
																		<span>Proj./Art./Del. No:</span>
																		<c:if test="${ not empty pmList}">
																			<span class="required-alone">*</span>
																		</c:if>
																	</div>
																</div>
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
																	<!-- <input type="text" name="text"
														class="form-control auto-complete" id="ProjectNo" /> -->
																	<c:choose>
																		<c:when test="${ not empty pmList}">
																			<select name="text"
																				class="form-control auto-complete select2"
																				data-live-search="true" id="ProjectNo">
																				<option value="1">1</option>
																				<option value="2">2</option>
																			</select>
																		</c:when>
																		<c:otherwise>
																			<input type="text" name="text"
																				class="form-control auto-complete" id="ProjectNo" />
																		</c:otherwise>
																	</c:choose>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="forms">
													<div class="form-group">
														<div class="input-group1">
															<div class="row">
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
																	<div class="input-group-addon">
																		<span>Color:</span>
																	</div>
																</div>
																<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
																	<input type="text" name="text" class="form-control"
																		id="Color" />

																	<!-- <input type="text" name="text" class="form-control" id="orderItem_color"/> -->
																</div>
															</div>
														</div>
													</div>
													<div class="forms">
														<div class="form-group">
															<div class="input-group1">
																<div class="row">
																	<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
																		<div class="input-group-addon">
																			<span>Total Net Weight:</span>
																		</div>
																	</div>
																	<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">


																		<input type="text" name="text" class="form-control"
																			id="NetWeight"
																			onkeypress="return validateDecimalDataTypeStrict(this,event);" />

																		<!-- <input type="text" name="text" class="form-control" id="orderItem_netWeight" onkeypress="return validateDataType(this,event);"/> -->
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>

											</div>

											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
												<div class="forms">
													<div class="form-group">
														<div class="input-group1">
															<div class="row">
																<div class="col-md-3 col-lg-3 col-sm-6 col-xs-6">
																	<div class="input-group-addon">
																		<span>Commodity:</span><span class="required-alone">*</span>
																	</div>
																</div>
																<div class="col-md-9 col-lg-9 col-sm-6 col-xs-6">
																	<select name="text"
																		class="form-control auto-complete select2 selection-form-rectifier"
																		data-live-search="true" id="MaterialNo"
																		style="width: 100% !important;">
																		<option value="">--Please Select--</option>
																		<%
																			try {
																		%>
																		<c:forEach var="materials" items="${materials}">
																			<c:choose>
																				<c:when
																					test="${materials.value == '000000000001000106'}">
																					<!-- This line is written to set A75 as the material value for RMG -->
																					<option value="A75" selected>${materials.label}</option>
																				</c:when>
																				<c:otherwise>
																					<option value="${materials.value }">${materials.label}</option>
																				</c:otherwise>
																			</c:choose>
																			<%-- <option value="${materials.value }" >${materials.label}</option> --%>
																		</c:forEach>

																		<%-- 													<c:forEach var="materials" items="${materials }">
														<c:choose>
															<c:when test="${materials.value == ${directBookingParams.  }">
																<option value="${materials.value }" selected>${materials.label}</option>
															</c:when>
															<c:otherwise>
																<option value="${materials.value }">${materials.label}</option>
															</c:otherwise>
														</c:choose>
														<option value="${materials.value }" >${materials.label}</option>
														</c:forEach> --%>
																		<%
																			} catch (Exception e) {
																				e.printStackTrace();
																			}
																		%>
																	</select>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>

											<!-- This div is for buttons -->
											<div class="col-xs-6 col-md-4" style="float: right;">
												<div style="float: right;">
													<button type="submit" data-type="horizontal"
														class="btn btn-primary btn_addmore" id="add-item"
														name="singlebutton"
														style="border-radius: 5px; height: 35px;"
														onclick="createBookingObj();" data-dismiss="modal">Confirm</button>
													<button type="submit" data-type="horizontal"
														class="btn btn-danger btn_addmore" data-dismiss="modal">Cancel</button>
												</div>
											</div>

											<!-- Button Areas Ends -->

										</div>
									</div>
								</div>
							</div>


						</section>



					</div>

				</div>





			</div>
		</div>
	</div>
</div>
<!-- Nazib Testing codde Ends Here -->





<!-- nazib Code starts testing here -->

<div id="lineItemVal" hidden>${lineItemJson}</div>
<div class="row row-margin-unset card">
	<h3>Line Items Informations From Uploaded Invoice</h3>
	<p class="lineItemTextFormatting">
		<img src="/MyShipmentFrontApp/resources/img/iconExclam.png"
			width="25px" height="25px">&nbsp; &nbsp; &nbsp; Please Click On
		The Action Button "Preview" To Confirm And Finalize The Entered Data
	</p>
	<table style="" class="table">
		<thead style="">
			<tr>
				<th>Item No</th>
				<th>PO No</th>
				<th>HS Code</th>
				<th>Carton Qty</th>
				<th>Total Pcs</th>
				<th>Gross Weight</th>
				<th>Length</th>
				<th>Width</th>
				<th>Height</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="lineItem_table">

		</tbody>
	</table>
</div>

<!-- nazib Code ends testing here -->

<!-- This Below portion  is for the final block of list element after pressing add as item item is being included over here -->
<div class="row row-margin-unset card">
	<h3>Finalized Line Items</h3>

	<p class="lineItemTextFormatting">
		<img src="/MyShipmentFrontApp/resources/img/iconExclam.png"
			width="25px" height="25px">&nbsp; &nbsp; &nbsp; Click on Submit
		to Proceed
	</p>
	<table style="" class="table">
		<thead style="">
			<tr>
				<th>Item No</th>
				<th>PO No</th>
				<th>Material</th>
				<th>Colour</th>
				<th>Product Code/SKU</th>
				<th>HS Code</th>
				<th>Carton Qty</th>
				<th>Total Pcs</th>
				<th>Net Weight</th>
				<th>Gross Weight</th>
				<th>CBM</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="booking_table">


		</tbody>
	</table>
</div>
<div class="row" style="float: right; margin-right: 2%;">
	<button type="submit" class="btn btn-primary btn_addmore"
		id="btn-submit" name="singlebutton"
		style="border-radius: 5px; height: 35px; padding-top: 8px;">Submit</button>
	<button type="button" class="btn btn-danger btn_addmore"
		name="singlebutton"
		style="border-radius: 5px; height: 35px; padding-top: 8px;">Cancel</button>
</div>
<div style="clear: both;"></div>

<script>
	$(function() {

		//Initialize Select2 Elements
		$('.select2').select2();
	});
	$(".tr_clone_add").click(function() {

		rowNum++;
		html = "";
		html += '<tr>';
		html += '<td><div style="width:6em"><input onchange="updateWidth(this)" type="text" class="form-control" size="80" value="" id="serial-no_' + rowNum + '"/></div></td>';
		html += '<td><div style="width:6em"><input onchange="updateWidth(this)" type="text" class="form-control" size="80" value="" id="no-of-cartoon_' + rowNum + '"/></div></td>';
		html += '<td><div style="width:3em"><input onchange="updateWidth(this)" type="text" class="form-control" size="80" value="" id="color_' + rowNum + '"/></div></td>';
		html += '<td><div style="width:3em"><input onchange="updateWidth(this)"  type="text" class="form-control" size="80" value="" id="size[1]_' + rowNum + '"/></div></td>';
		html += '<td><div style="width:3em"><input onchange="updateWidth(this)" type="text" class="form-control" size="80" value="" id="size[2]_' + rowNum + '"/></div></td>';
		html += '<td><div style="width:3em"><input onchange="updateWidth(this)" type="text" class="form-control" size="80" value="" id="size[3]_' + rowNum + '"/></div></td>';
		html += '<td><div style="width:3em"><input onchange="updateWidth(this)" type="text" class="form-control" size="80" value="" id="size[4]_' + rowNum + '"/></div></td>';
		html += '<td><div style="width:3em"><input onchange="updateWidth(this)" type="text" class="form-control" size="80" value="" id="size[5]_' + rowNum + '"/></div></td>';
		html += '<td><div style="width:3em"><input onchange="updateWidth(this)" type="text" class="form-control" size="80" value="" id="size[6]_' + rowNum + '"/></div></td>';
		html += '<td><div style="width:3em"><input onchange="updateWidth(this)" type="text" class="form-control" size="80" value="" id="size[7]_' + rowNum + '"/></div></td>';
		html += '<td><div style="width:3em"><input onchange="updateWidth(this)" type="text" class="form-control" size="80" value="" id="size[8]_' + rowNum + '"/></div></td>';
		html += '<td><div style="width:3em"><input onchange="updateWidth(this)" type="text" class="form-control" size="80" value="" id="size[9]_' + rowNum + '"/></div></td>';
		html += '<td><div style="width:3em"><input onchange="updateWidth(this)" type="text" class="form-control" size="80" value="" id="size[10]_' + rowNum + '"/></div></td>';
		html += '<td><div style="width:3em"><input onchange="updateWidth(this)" type="text" class="form-control" size="80" value="" id="size[11]_' + rowNum + '"/></div></td>';
		html += '<td><div style="width:3em"><input onchange="updateWidth(this)" type="text" class="form-control" size="80" value="" id="size[12]_' + rowNum + '"/></div></td>';
		html += '<td><div style="width:3em"><input onchange="updateWidth(this)" type="text" class="form-control" size="80" value="" id="size[13]_' + rowNum + '"/></div></td>';
		html += '<td><div style="width: 3em"><input onchange="updateWidth(this)" type="text" class="form-control" value="" id="pcs-crtn_' + rowNum + '" /></div></td>';
		html += '<td><div style="width: 3em"><input onchange="updateWidth(this)" type="text"class="form-control" value="" id="total-pcs_' + rowNum + '" /></div></td>';
		html += '<td><div style="width: 3em"><input onchange="updateWidth(this)" type="text" class="form-control" value="" id="remarks_' + rowNum + '" /></div></td>';
		html += '<td><div style="width:5em"><button  type="submit" data-type="horizontal" name="singlebutton" class="btn btn-danger center-block delete pull-right">Remove</button></div></td>';
		html += '</tr>';

		$("#table-data").append(html);

	});
	function updateWidth(textbox) {

		$(textbox).parent().css("width", (textbox.value.length + 2) + "em");
	}

	$(document).ready(function() {

		$(document).on("click", ".delete", function(e) {

			$(this).parent().parent().parent().remove();
			rowNum--;
		});
		/* int intRow=$("#orderItem_commodity option[value='000000000001000106']").val();
		 $("#orderItem_commodity").val(intRow);
		 $("#orderItem_commodity").selectpicker("refresh"); */

	});
</script>
<jsp:include page="footer_v2.jsp"></jsp:include>


