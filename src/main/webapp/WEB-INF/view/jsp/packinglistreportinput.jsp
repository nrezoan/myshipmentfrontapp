		<%@include file="header.jsp" %>
		<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		
		<script type="text/javascript">
	
		</script>
		
		<section class="main-body">
		<div class="well assign-po-search">
	<form:form  action="getPackingListReport" method="post" commandName="packingListReportParam">		
      <fieldset>
          <legend>Packing List Report</legend>
                <div class='row' >
                    
                   <div class='col-sm-2'>    
                        <div class='form-group'>
                        <label>HBL No</label>                        
                        <form:input path="hblNo" id="number" cssClass="form-control"/>
                        </div>
                   </div>
                   
				   <div class='col-sm-2'>
                  <div class='form-group'>
                   <label style="display:block;color:#f5f5f5">. </label> <!-- added for alignment purpose, dont delete -->       
                   <input type="submit" id="btn-appr-po-search"class="btn btn-success btn-lg form-control" value="Submit"/>
                 </div>
                 </div>  
                 </div>
             
</fieldset>
</form:form>
		</div>
		</section>
		
	<!-- ----------------------------- -->		
		<%@include file="footer.jsp" %>