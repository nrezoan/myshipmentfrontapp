<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>MyShipment</title>
        <meta name="description" content="HTML5 template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- favicon
		============================================ -->		
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
		
		<!-- Google Fonts
		============================================ -->		
       <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900,300' rel='stylesheet' type='text/css'>
	   
		<!-- Bootstrap CSS
		============================================ -->		
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
		<!-- Bootstrap CSS
		============================================ -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css">
		<!-- normalize CSS
        ============================================ -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/normalize.css">
        <!-- main CSS
        ============================================ -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
		<!-- style CSS
		============================================ -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
         <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/graph.css">
		<!-- modernizr JS
		============================================ -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/test.css">		
        <script src="${pageContext.request.contextPath}/resources/js/vendor/modernizr-2.8.3.min.js"></script>
		
    </head>
<body>

	<!-- Navigation -->
	<nav class="navbar navbar-inverse navbar-fixed-top header-area"
		role="navigation">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<spring:url value="/test"/>"> <img style="max-width:24%;margin-top: 4.5%;margin-left: -4%;" src="${pageContext.request.contextPath}/resources/images/logo.png" style="width: 70%;margin-top: 3%;"></a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
				<!-- <li><a href="about.html">About</a></li>  -->
				<!-- <li><a href="services.html">Services</a></li>  -->
				<li><a href="${pageContext.request.contextPath}/signin">Login   <i class="fa fa-user"></i></a></li>

			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container --> </nav>

	<!-- Header Carousel -->


	<!-- Page Content -->
	<div class="container">
		<br>
		<br> <br>
		<br>
		<!-- Marketing Icons Section -->
		<div class="well assign-po-search">
		<form:form action="${pageContext.request.contextPath}/recoverPassword" method="post"
			commandName="editparam">
			<fieldset>
			 <legend style="color: green; margin-top: 2px;font-size: 140%; text-align:center">${message}</legend>
				<div class='row'>
					<div class="col-md-4"></div>
					<div class="col-md-4 well">
						<div class="col-md-12">
							<legend>Recover Your Password</legend>
						</div>
							<div class='col-md-12'>
							<div class='form-group'>
								<label>User ID</label>
								<form:input path="customerNo" id="mail" cssClass="form-control" />
									
							</div>
						</div>
						<div class='col-md-12'>
							<div class='form-group'>
								<label>Mail Address</label>
								<form:input path="verifyMail" id="mail" cssClass="form-control" />
									
							</div>
						</div>
					
						<div class='col-md-12'>
							<div class='form-group'>
								<label style="display: block; color: #f5f5f5">. </label>
								<!-- added for alignment purpose, dont delete -->
								<input type="submit" id="btn-appr-po-search"
									class="btn btn-success btn-lg form-control" value="Submit"
									onclick="return Validate()" />
							</div>
						</div>
					</div>
					<div class="col-md-4"></div>








				</div>


			</fieldset>
		</form:form>
	</div>
		
		<!-- /.row -->


		<!-- Call to Action Section -->



	</div>
	<!-- Footer -->
	<section class="footer">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				<p class="footer-para">Copyright &copy MyShipment.com</p>

			</div>
			<!-- 
			<div class="col-lg-3 col-md-3 footer-img">
				<img src="img/logo.png" alt="" class="" />
			</div>
			<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 footer_nav">
				<ul class="table-h-nav-footer">
					<li><a href="">Booking</a></li>
					<li><a href="">booking template</a></li>
					<li><a href="">Mis</a></li>
					<li><a href="">frequently used form</a></li>
					<li><a href="">other reports</a></li>

					<li><a href="">Update</a></li>
				</ul>
			</div> -->
		</div>
	</div>
	</section>


    <!-- /.container -->

   <!-- jquery
		============================================ -->		
        <script src="/MyShipmentFrontApp/resources/js/vendor/jquery-1.11.3.min.js"></script>
		<!-- bootstrap JS
		============================================ -->		
        <script src="/MyShipmentFrontApp/resources/js/bootstrap.min.js"></script>
		<!-- plugins JS
		============================================ -->		
        <script src="/MyShipmentFrontApp/resources/js/plugins.js"></script>
		<!-- main JS

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 2500 //changes the speed
    });
    </script>

</body>

</html>