<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<section class="main-body">
	<div class="container-fluid table-responsive">
		<div class="row table_header">

			<h3>Sea Export Invoice List</h3>

		</div>
		<div class="row col-xs-6">
			<table class="table table-striped">
				<thead style="">
					<tr>
						<th>Invoice Number</th>
						<th>HBL Number</th>
						<th>Type</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<tr class="">
						<c:forEach items="${invoiceListJsonData.invoice}" var="i">
							<tr>
								<td class="invoice">${i.vbeln}</td>
								<td>${i.hbl}</td>
								<c:if test="${i.knumv =='ZSCM'}">
									<td>Invoice</td>
								</c:if>
								<c:if test="${i.knumv !='ZSCM'}">
									<td>Debit Note</td>
								</c:if>
								<td>
									<form
										action="${pageContext.request.contextPath}/getSeaExportInvoiceDetail"
										method="get">
										<input type="button" id="reportId" value="Print"
											class="btn btn-primary btn-md" />
									</form>
								</td>
							</tr>
						</c:forEach>
					</tr>

				</tbody>
			</table>
			<c:if test="${invoiceListJsonData ==null }">
				<div
					style="height: 50px; width: 200px; color: red; margin-left: 43%; padding-top: 10px;">No
					Record found!</div>
			</c:if>
		</div>

	</div>

</section>

<script type="text/javascript">

$(document).ready(function() {
	console.log("in");
	var myContextPath = "${pageContext.request.contextPath}"
	console.log(myContextPath);
 	$("#reportId").click(function(){	
 	    var invoice = $(this).closest("tr").find(".invoice").text();
 	    console.log(flightNo);
 	   $.ajax({
 			url: myContextPath + '/getSeaExportInvoiceDetail',
 			data: {
 				invoiceNo:invoice
 			},
 			type: "GET",
 			dataType: "json",
 			success:function(data){
 			console.log(data);	
 			},
 			error:function(jqXHR, textStatus, errorThrown)
 			{
 				
 			}
 		});
	})  

	
});




</script>

<!-- ----------------------------- -->
<%@include file="footer_v2_fixed.jsp"%>