<%@include file="header.jsp" %>       
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>	
<style>
.error {
	color: #ff0000;
	font-style: italic;
	font-weight: bold;
	text-align: center;
}
</style>
		
        <div class="col-xs-12" style="height:30px;"></div>
        <div class="container">
            
  <ul class="nav nav-justified">    
    <li><a style="text-transform:uppercase;background-color:#fff;">UPDATE PO</a></li>
  </ul>

  <div class="tab-content">
    <div id="#menu1" class="tab-pane fade in active">
    <form:form  action="updateThePo" method="post" commandName="segPurchaseOrder">
      <section>
		<div class="container-fluid">
			<div class="row line_item_text">
			<div class="error">
			<c:if test="${not empty error}">
  				 Error: ${error}
			</c:if>
			</div>
				<!-- <h3>Edit Data</h3> -->
			</div>
                <div class='row input_area' >
                    
                   <div class='col-sm-4'>    
                        <div class='form-group'>
                        <label>PO Number</label>
                        <form:input path="vc_po_no" cssClass="form-control" id="user_title" readonly="true" style="padding-left: 0px !important;"/>                        
                        </div>
                   </div>
                   
                   <div class='col-sm-4'>
                       <div class='form-group'>
                       <label>Division</label>
                       <form:input path="vc_division" cssClass="form-control" style="padding-left: 0px !important;"/>                       
                   </div>
                  </div>
                  
                   <div class='col-sm-4'>
                       <div class='form-group'>
                       <label>Product Number</label>
                       <form:input path="vc_product_no" cssClass="form-control" style="padding-left: 0px !important;"/>                       
                   </div>
                  </div>
                  <div class="col-xs-12" style="height:20px;"></div>
                  <div class='col-sm-4'>
                  <div class='form-group'>
                   <label>Style No</label>
                   <form:input path="vc_style_no" cssClass="form-control" style="padding-left: 0px !important;"/>                   
                 </div>
                 </div>
                    <div class='col-sm-4'>
                  <div class='form-group'>
                   <label>Sku-No</label>
                   <form:input path="vc_sku_no" cssClass="form-control" style="padding-left: 0px !important;"/>                  
                 </div>
                 </div>
                    <div class='col-sm-4'>
                  <div class='form-group'>
                   <label>Color</label>
                   <form:input path="vc_color" cssClass="form-control" style="padding-left: 0px !important;"/>                  
                 </div>
                 </div>
                 <div class="col-xs-12" style="height:20px;"></div>
                    <div class='col-sm-4'>
                  <div class='form-group'>
                   <label>Size</label>
                   <form:input path="vc_size" cssClass="form-control" style="padding-left: 0px !important;"/>                  
                 </div>
                 </div><br>
                    <div class='col-sm-4'>
                  <div class='form-group'>
                   <label>Total Pieces</label>
                   <form:input path="vc_tot_pcs" cssClass="form-control" style="padding-left: 0px !important;"/>                  
                 </div>
                 </div>
                  <div class='col-sm-4'>
                    <div class='form-group'>
                      <label>HS Code</label>
                        <form:input path="vc_hs_code" cssClass="form-control" style="padding-left: 0px !important;"/>                  
                 </div>
                 </div>
                 <div class="col-xs-12" style="height:20px;"></div>
                 <div class='col-sm-4'>
                  <div class='form-group'>
                   <label>No Pcs Ctns</label>  
                    <form:input path="nu_no_pcs_ctns" cssClass="form-control"  style="padding-left: 0px !important;"/>                                                
                 </div>
                 </div>
                 <div class='col-sm-4'>
                    <div class='form-group'>
                      <label>Article No</label>
                        <form:input path="vc_article_no" cssClass="form-control" style="padding-left: 0px !important;"/>                  
                 </div>
                 </div>
                 <div class='col-sm-4'>
                    <div class='form-group'>
                      <label>POL</label>
                        <form:input path="vc_pol" cssClass="form-control" style="padding-left: 0px !important;"/>                  
                 </div>
                 </div>
                 <div class="col-xs-12" style="height:20px;"></div>
                 <div class='col-sm-4'>
                    <div class='form-group'>
                      <label>POD</label>
                        <form:input path="vc_pod" cssClass="form-control" style="padding-left: 0px !important;"/>                  
                 </div>
                 </div>
                 <div class='col-sm-4'>
                    <div class='form-group'>
                      <label>Commodity</label>
                        <form:input path="vc_commodity" cssClass="form-control" style="padding-left: 0px !important;"/>                  
                 </div>
                 </div>
                <!--  <div class='col-sm-4'>
                    <div class='form-group'>
                      <label>ETD</label>
                        <form:input path="dt_etd" cssClass="form-control" style="padding-left: 0px !important;"/>                  
                 </div>
                 </div> -->
                 <div class='col-sm-4'>
                    <div class='form-group'>
                      <label>Quantity</label>
                        <form:input path="vc_quan" cssClass="form-control" style="padding-left: 0px !important;"/>                  
                 </div>
                 </div>
                 <div class="col-xs-12" style="height:20px;"></div>
                 <div class='col-sm-4'>
                    <div class='form-group'>
                      <label>Length</label>
                        <form:input path="nu_length" cssClass="form-control" style="padding-left: 0px !important;"/>                  
                 </div>
                 </div>
                 <div class='col-sm-4'>
                    <div class='form-group'>
                      <label>Width</label>
                        <form:input path="nu_width" cssClass="form-control" style="padding-left: 0px !important;"/>                  
                 </div>
                 </div>
                 <div class='col-sm-4'>
                    <div class='form-group'>
                      <label>Height</label>
                        <form:input path="nu_height" cssClass="form-control" style="padding-left: 0px !important;"/>                  
                 </div>
                 </div>
                 <div class="col-xs-12" style="height:20px;"></div>
                 <div class='col-sm-4'>
                    <div class='form-group'>
                      <label>CBM Sea</label>
                        <form:input path="vc_cbm_sea" cssClass="form-control" style="padding-left: 0px !important;"/>                  
                 </div>
                 </div>
                 <div class='col-sm-4'>
                    <div class='form-group'>
                      <label>Volume</label>
                        <form:input path="vc_volume" cssClass="form-control" style="padding-left: 0px !important;"/>                  
                 </div>
                 </div>
                 <div class='col-sm-4'>
                    <div class='form-group'>
                      <label>Gross Weight</label>
                        <form:input path="vc_gr_wt" cssClass="form-control" style="padding-left: 0px !important;"/>                  
                 </div>
                 </div>
                 <div class="col-xs-12" style="height:20px;"></div>
                 <div class='col-sm-4'>
                    <div class='form-group'>
                      <label>Net Weight</label>
                        <form:input path="vc_nt_wt" cssClass="form-control" style="padding-left: 0px !important;"/>                  
                 </div>
                 </div>
                 <div class='col-sm-4'>
                    <div class='form-group'>
                      <label>Ref Field1</label>
                        <form:input path="vc_ref_field1" cssClass="form-control" style="padding-left: 0px !important;"/>                  
                 </div>
                 </div>
                 <div class='col-sm-4'>
                    <div class='form-group'>
                      <label>Ref Field2</label>
                        <form:input path="vc_ref_field2" cssClass="form-control" style="padding-left: 0px !important;"/>                  
                 </div>
                 </div>
                 <div class="col-xs-12" style="height:20px;"></div>
                 <div class='col-sm-4'>
                    <div class='form-group'>
                      <label>Ref Field3</label>
                        <form:input path="vc_ref_field3" cssClass="form-control" style="padding-left: 0px !important;"/>                  
                 </div>
                 </div>
                 <div class='col-sm-4'>
                    <div class='form-group'>
                      <label>Ref Field4</label>
                        <form:input path="vc_ref_field4" cssClass="form-control" style="padding-left: 0px !important;"/>                  
                 </div>
                 </div>
                 <div class='col-sm-4'>
                    <div class='form-group'>
                      <label>Ref Field5</label>
                        <form:input path="vc_ref_field5" cssClass="form-control" style="padding-left: 0px !important;"/>   
                        <form:hidden path="seg_id"/>     
                    	<form:hidden path="paramPoNo"/>
                    	<form:hidden path="paramFromDate"/>
                    	<form:hidden path="paramToDate"/>
                 </div>
                 </div>
                 <div class="col-xs-12" style="height:20px;"></div>
                <!--  <div class='col-sm-4'>
                    <div class='form-group'>
                      <label>Unit</label>
                        <form:input path="unit" cssClass="form-control" />                  
                 </div>
                 </div> -->
                 <!-- <div class='col-sm-4'>
                    <div class='form-group'>
                      <label>Currency Unit</label>
                        <form:input path="currencyUnit" cssClass="form-control" />                  
                 </div>
                 </div> -->
                </div>
                </div> 
			<div class="row input_area">
				<button id="line_item_button" type="submit" name="singlebutton" class="btn btn-success btn-lg center-block">Submit</button>
			</div> 
</section>
       
       
</form:form>
      </div>
    </div>
        </div>
		
	<!-- ----------------------------- -->		
		<%@include file="footer.jsp" %>
