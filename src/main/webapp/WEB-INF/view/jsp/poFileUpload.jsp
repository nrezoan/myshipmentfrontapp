<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<jsp:include page="header_v2.jsp"></jsp:include>
	<jsp:include page="navbar.jsp"></jsp:include>
	<script src="${pageContext.request.contextPath}/resources/js/jszip.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/xlsx.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/poFileUpload.js"></script>
	<script src="${pageContext.request.contextPath}/resources/bootstrap-select/js/bootstrap-select.min.js"></script>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bootstrap-select/css/bootstrap-select.min.css">

	<script type="text/javascript">

	</script>
	<style>
		html {
			background-color: #fff;
			margin-bottom: 30px;
		}

		.input-group-addon {
			border: unset !important;
		}

		.nav-tabs.nav-justified>.active>a,
		.nav-tabs.nav-justified>.active>a:focus,
		.nav-tabs.nav-justified>.active>a:hover {
			background-color: #007aa7;
			color: #fff;
		}

		.tab-direct-booking-well {
			background-color: #fff !important;
		}

		.ul-unset-margin {
			margin-left: unset !important;
		}
	</style>
	<div class="container-fluid container-fluid-441222">
		<div class="row row-without-margin">
			<h1 class="h1-report">PO Upload</h1>
			<ol class="breadcrumb">
				<li><i class="fa fa-upload"></i>&nbsp;Purchase Orders</li>
				<li class="active">PO Upload</li>
			</ol>
		</div>
		<div class="row row-without-margin">
			<div class="col-md-10">
				<fieldset>
					<legend>PO Upload Information</legend>

					<div class="col-md-3">
						<div class="col-md-12">
							<div class="form-inline">
								<div class="form-group">
									<div class="input-group">
										<input type="radio" name="tempType" value="buyerSpecific" onchange="downloadGeneralTemplate()" checked> Buyer Wise
										Template<br> <input type="radio" name="tempType" value="general" onchange="downloadGeneralTemplate()"> General
										Template<br>
									</div>
								</div>
							</div>
						</div>
						<br>
						<div class="col-md-12">
							<div style="display: none">
								<span style="display: inline;"> <a class="btn btn-null" href="/MyShipmentFrontApp/download/generalTemplateExcel/General-Template.xlsx">
										<i class="fa fa-download"></i> Download General Template </a>
								</span>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-inline">
							<div class="form-group">
								<div class="input-group">
									<form id="upload-file-form">
										<input id="upload-file-input" class="file" type="file" name="uploadfile" accept="*" /> <input type="text" class="form-control"
										 disabled placeholder="Upload" style="height: 35px; text-align: center;"> <span class="input-group-btn">
											<button class="browse btn btn-primary" type="button" style="height: 35px;">
												<i class="glyphicon glyphicon-search"></i> Browse
											</button>
										</span>
									</form>
								</div>


							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group">
							<div class="col-md-5">
								<div style="text-align: right">
									<span>Date Format</span>
								</div>
							</div>
							<div class="col-md-7">
								<select class="form-control drop" id="dateformat">
									<option selected value="">Please Select</option>
									<option value="DD-MM-YYYY">DD-MM-YYYY</option>
									<option value="MM-DD-YYYY">MM-DD-YYYY</option>
									<option value="DD-MMM-YYYY">DD-MMM-YYYY</option>
									<option value="YYYY-MM-DD">YYYY-MM-DD</option>
									<option value="YYYY-DD-MM">YYYY-DD-MM</option>
									<option value="DD/MM/YYYY">DD/MM/YYYY</option>
									<option value="MM/DD/YYYY">MM/DD/YYYY</option>
									<option value="DD/MMM/YYYY">DD/MMM/YYYY</option>
									<option value="YYYY/MM/DD">YYYY/MM/DD</option>
									<option value="DD.MM.YYYY">DD.MM.YYYY</option>
									<option value="MM.DD.YYYY">MM.DD.YYYY</option>
									<option value="DD.MMM.YYYY">DD.MMM.YYYY</option>
									<option value="YYYY.MM.DD">YYYY.MM.DD</option>
									<option value="DDMMYYYY">DDMMYYYY</option>
								</select>
							</div>

						</div>
					</div>
					<div class="col-md-1">
						<!-- <input type="submit" class="btn btn-success" value="Upload"
						style="" id="btn-upload" onclick="showConfirmationBox()"> -->
						<a class="btn btn-success" id="btn-upload" onclick="showConfirmationBox()"><i class="fa fa-upload"></i> Upload</a>
					</div>

				</fieldset>
			</div>

			<div id="addNewTemp" class="col-md-2">
				<a href="sendTemplate"><button class="btn btn-null" type="button" style="height: 35px;">
						<i class="glyphicon glyphicon-plus"></i> ADD NEW TEMPLATE
					</button> </a>
			</div>

			<div id="showDown" class="col-md-2" style="display:none">
					<span style="display: inline;"> <a class="btn btn-danger" href="/MyShipmentFrontApp/download/generalTemplateExcel/General-Template.xlsx">
						<i class="fa fa-download"></i> Download General Template </a>
				</span>
				</div>

		</div>

	</div>

	<div class="modal fade" id="fileUploadSuccessModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Buyer Template UpdatePO Excel File Upload
					</h4>
				</div>
				<div class="modal-body" style="text-align: center;">
					<p id="msgBody">Data upload Successfully!!!</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="fileUploadErrorModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">PO Excel File Upload</h4>
				</div>
				<div class="modal-body" style="text-align: center;">
					<p id="msgBody2">Data Upload Unsuccessful!!!</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="poUploadConfirmation" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Buyer Template Update</h4>
				</div>
				<div class="modal-body" style="text-align: center;">
					<p id=confirmationBody>
						Do you want to upload file for <strong style="color: red">"${buyer}"
						</strong>
					</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal" onclick="uploadFile()">Yes</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
				</div>
			</div>
		</div>
	</div>
	<jsp:include page="footer_v2.jsp"></jsp:include>