
<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style>
	html{
		background-color: #fff;
	}
</style>
<script type="text/javascript">
	
</script>

<section class="content-header" style="display: block;">
	<h1>Booking Update</h1>
	<ol class="breadcrumb" style="position: unset; float: none;">
		<li><i class="fa fa-dashboard">&nbsp;</i>Booking</li>
		<li class="active">Booking Update</li>
	</ol>

</section>
<hr>

<form:form action="${pageContext.request.contextPath}/bookingDisplayData"
	modelAttribute="salesOrderParams" method="POST">
	<section class="main-body">
		<div class="well-white-269490">

			<fieldset>
				
				<div class='row'>

					<div class='col-sm-2'>
						<div class='form-group'>
							<label>HBL Number</label>
							<form:input class="form-control" path="blno" placeholder="EX: TML123456"></form:input>
						</div>
					</div>


					<div class='col-sm-2'>
						<div class='form-group'>
							<label style="display: block; color: #f5f5f5">. </label>
							<!-- added for alignment purpose, dont delete -->
							<input type="submit" id="btn-appr-po-search"
								class="btn btn-success form-control" value="Search" />
						</div>
					</div>
				</div>
				<div>
					<span style="color: red;">${displayError}</span>
				</div>

			</fieldset>

		</div>


	</section>
</form:form>
<!-- ----------------------------- -->
<%@include file="footer_v2_fixed.jsp"%>