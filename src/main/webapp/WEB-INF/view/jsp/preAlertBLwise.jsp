<jsp:include page="header_v2.jsp"></jsp:include>
<jsp:include page="navbar.jsp"></jsp:include>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!-- DataTables -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">


<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/pre-alert-management.css"></link>

<script src="${pageContext.request.contextPath}/resources/js/pre-alert-management.js"></script>

<style>
	#preAlertTable thead {
		background-color: #d8d6d2;
	}
	.btn-null {
		background-color: #d8d6d2;
		color: #252525;
		font-weight: 600;
	}
</style>

<div class="container-fluid container-fluid-441222">
	<div class="row row-without-margin">
		<h1 class="h1-report">Pre-Alert Management</h1>
		<ol class="breadcrumb">
			<li><i class="fa fa-exclamation-triangle active"></i>&nbsp;Pre-Alert Management</li>
		</ol>
	</div>
	<hr>
	
	<div class="row row-without-margin">
		<form class="form-incline">
			<div class="col-md-3 col-sm-12 col-xs-12">
				<div class='form-group'>
					<label class="text-center">MBL/MAWB Number</label>
					<input id="hblNumberSearchInput" class="form-control" />
				</div>
			</div>
			<div class="col-md-2 col-sm-12 col-xs-12">				
				<label style="display: block; color: transparent">.</label>
				<!-- added for alignment purpose, dont delete -->
				<button type="submit" onclick="return searchHBLpreAlertDoc();" class="btn btn-primary btn-block">Search</button>
			</div>
			<div class="col-md-4">				
				
			</div>
			<div class="col-md-1 col-sm-4 col-xs-4">				
				<label style="display: block; color: transparent">.</label>
				<!-- added for alignment purpose, dont delete -->
				<a href="preAlertManagement?status=all" class="btn btn-null">Search All</a>
			</div>
			<div class="col-md-1 col-sm-4 col-xs-4">				
				<label style="display: block; color: transparent">.</label>
				<!-- added for alignment purpose, dont delete -->
				<a href="preAlertManagement?status=pending" class="btn btn-null" style="margin-left: -3%;">Pending</a>
			</div>
			<div class="col-md-1 col-sm-4 col-xs-4">				
				<label style="display: block; color: transparent">.</label>
				<!-- added for alignment purpose, dont delete -->
				<a href="preAlertManagement?status=completed" class="btn btn-null" style="margin-left: -21%;">Completed</a>
			</div>
		</form>
	</div>
	
	<hr>

	<div class="row row-no-margin-331932" style="padding-top: 10px;">
		<div class="col-xs-12 col-md-12">
			<div class="table-responsive" id="headingTwo">
				<table class="table table-hover" id="preAlertTable">
					<thead>
						<tr>
							<th style="text-align: center;">HBL/HAWB No.</th>
							<th style="text-align: center;">Consignee</th>
							<th style="text-align: center;">Notify Party</th>
							<th style="text-align: center;">Shipment Status</th>
							<th style="text-align: center;">Status</th>
							<th style="text-align: center;">Action</th>
						</tr>
					</thead>
					<tbody id="valueTableBody" style="text-align: center;">
						<%-- <c:choose>
							<c:when test="${fn:length(preAlertHblDetailsBySupplierList) gt 0}">
								<c:if test="${not empty preAlertHblDetailsBySupplierList}">
									<c:forEach items="${preAlertHblDetailsBySupplierList}" var="shipment"
										varStatus="index">
										<tr>
											<td style="text-align: center;">${shipment.trackHeaderBean.bl_no}</td>
											<td style="text-align: center;">${shipment.trackHeaderBean.buyer}</td>
											<td style="text-align: center;">${shipment.trackHeaderBean.agent}</td>
											<td style="text-align: center;">In Transit</td>

											<td style="text-align: center;">Incomplete</td>
											<td style="text-align: center; width: 5%;"><a
												data-toggle="modal" data-target="#updateFormModal"
												class="button btn btn-warning btn-sm"
												onclick="setUploadFilesValue('${shipment.trackHeaderBean.bl_no}', '${shipment.trackHeaderBean.buyer}', '${shipment.trackHeaderBean.agent}', '${shipment.preAlertHbl.isPreAlertDoc}')"><i
													class="fa fa-upload" aria-hidden="true"></i>&nbsp;Upload</a></td>
										</tr>
									</c:forEach>
								</c:if>
							</c:when>
							<c:otherwise>
								<td class="text-center" colspan="6"><a class="not-found">No
										HBL/HAWB Found</a></td>
							</c:otherwise>
						</c:choose> --%>
					</tbody>

				</table>
			</div>
		</div>
	</div>

</div>

<jsp:include page="preAlertUploadModal.jsp"></jsp:include>

<script>

	/* $(function() {
		$('#preAlertTable').DataTable();
	}); */
	
	var preAlertTableList = ${preAlertHblDetailsBySupplierListJSON};
	var preAlertManagement = true;
	
	$(document).ready(function() {
		generatePreAlertTable(preAlertTableList);
		$('#preAlertTable').DataTable();
	});
	
	
</script>

<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>


<jsp:include page="footer_v2.jsp"></jsp:include>