<%@page import="java.util.Date"%>
<%@page import="com.myshipment.util.DateUtil"%>
<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!-- DataTables -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<style>
	html {
		background-color: #eaeaea;
	}
	.ship_desti {
		margin-left: 1%;
	}
	th {
		text-align: center;
	}
	.main-body {
		background-color: #eaeaea;
	}
	.commercial h3 {
	    background-color: #222534;
	    padding-top: 7px;
	    font-weight: 400;
	    height: 40px;
	    margin-top: 10px;
	    color: #fff;
	}
</style>

<section class="main-body">

	<div class="container-fluid">
		<div class="row-without-margin" style="width: 101%;">
			<%-- <div class="col-md-12">
				<h3 class="text-center">${name }</h3>
			</div> --%>
			
			<div class="col-sm-12">
				<div class="commercial">
					<h3 class="text-center">${portTitle } : ${port }</h3>
				</div>
			</div>
			
		</div>
		<!--/row-->
		<!-- <hr> -->
		<div class="row" style="margin-left: 0px;">


			<div class="col-md-12 table-responsive">
				<table class="table table-bordered table-striped" id="shipmentTable">
					<thead style="background-color: #222534; color: #fff;">
						<tr id="shipmentHeader">
							<th>BL. No</th>
							<th>BL. Date</th>
							<!-- <th>Booking Date</th> -->
							<th>${accountGroup }</th>
							<th>${portTable }</th>
							<th>Gross Weight</th>
							<th>Charge Weight</th>
							<th>Total Volume</th>
							<th>GR Date</th>
							<th>Shipment Date</th>


						</tr>
					</thead>
					<tbody>
						<c:forEach items="${portWiseShipmentList}"
							var="sodetails">
							<tr class="text-center">
								<td>
									<a href="getTrackingInfoFrmUrl?searchString=${sodetails.bl_no }">${sodetails.bl_no }</a>
								</td>
								<td>
									<c:set var="bl_date" value="${sodetails.bl_bt }"/>
									
									<% 
									if(pageContext.getAttribute("bl_date") != null && !pageContext.getAttribute("bl_date").equals("")) {
										Date blDate = DateUtil.stringToDateTime((String) pageContext.getAttribute("bl_date") + "000000");
										request.setAttribute("blDate", blDate); 
									}
									%>
									<fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${blDate}" />
								</td>
								<%-- <td>${sodetails.so_dt }</td> --%>
								<%-- <td>${sodetails.buyer}</td> --%>
								<td>
									<c:choose>
										<c:when test="${accountGroup == 'Supplier'}">
											${sodetails.shipper }
										</c:when>
										<c:when test="${accountGroup == 'Buyer'}">
											${sodetails.buyer }
										</c:when>
									</c:choose>
								</td>
								<td>
									<c:choose>
										<c:when test="${accountGroup == 'Supplier'}">
											${sodetails.pod }
										</c:when>
										<c:when test="${accountGroup == 'Buyer'}">
											${sodetails.pol }
										</c:when>
									</c:choose>
								</td>
								<td>${sodetails.grs_wt }</td>
								<td>${sodetails.crg_wt }</td>
								<td>${sodetails.volume }</td>
								<td>
									<c:set var="gr_dt" value="${sodetails.gr_dt }"/>
									<% 
										if(pageContext.getAttribute("gr_dt") != null && !pageContext.getAttribute("gr_dt").equals("")) {
											Date grDate = DateUtil.stringToDateTime((String) pageContext.getAttribute("gr_dt") + "000000");
											request.setAttribute("grDate", grDate);
										}else {
											request.setAttribute("grDate", "");
										}
										 
									%>
									<fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${grDate}" />
								</td>
								<td>
									<c:set var="dep_dt" value="${sodetails.dep_dt }"/>
									
									<% 
										if(pageContext.getAttribute("dep_dt") != null && !pageContext.getAttribute("dep_dt").equals("")) {
											Date depDate = DateUtil.stringToDateTime((String) pageContext.getAttribute("dep_dt") + "000000");
											request.setAttribute("depDate", depDate);
										}else {
											request.setAttribute("depDate", "");
										}
										 
									%>
									<fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${depDate}" />
								</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>

<script>

	$(function() {
		$('#shipmentTable').DataTable();
	});
	
	
</script>

<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

<%@include file="footer_v2_fixed.jsp"%>