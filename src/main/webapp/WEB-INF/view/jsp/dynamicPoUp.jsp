<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
	<jsp:include page="header_v2.jsp"></jsp:include>
	<jsp:include page="navbar.jsp"></jsp:include>
	<script src="${pageContext.request.contextPath}/resources/js/jszip.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/xlsx.js"></script>
	<script src="${pageContext.request.contextPath}/resources/js/templateMapping.js"></script>
	<script src="${pageContext.request.contextPath}/resources/bootstrap-select/js/bootstrap-select.min.js"></script>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bootstrap-select/css/bootstrap-select.min.css">
	<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/myshipment_v2/update-order-booking.css">


	<script type="text/javascript">
		var x = false;
	</script>
	<style>
		html {
			background-color: #fff;
			margin-bottom: 30px;
		}

		.input-group-addon {
			border: unset !important;
		}

		.nav-tabs.nav-justified>.active>a,
		.nav-tabs.nav-justified>.active>a:focus,
		.nav-tabs.nav-justified>.active>a:hover {
			background-color: #007aa7;
			color: #fff;
		}

		.tab-direct-booking-well {
			background-color: #fff !important;
		}

		.ul-unset-margin {
			margin-left: unset !important;
		}

		.map-header {
			font-size: 16px;
			padding-left: 5px;
			font-weight: bold;
		}
	</style>

	<!-- Tab panes -->

	<div class="container-fluid container-fluid-441222">
		<div class="row row-without-margin">
			<h1 class="h1-report">Template Mapping</h1>
			<ol class="breadcrumb">
				<li><i class="fa fa-upload"></i>&nbsp;PO UPLOAD</li>
				<li class="active">Template Mapping</li>
			</ol>
		</div>
		<div class="row row-without-margin">
			<div class="col-xs-6 col-md-4" style="padding-left: 20px;">
				<div class="form-inline">
					<div class="form-group">
						<div class="input-group">

							<input type="file" name="" class="file" id="uploadXL">
							<input type="text" class="form-control" disabled placeholder="Upload" style="height: 35px; text-align: center;">
							<span class="input-group-btn">
								<button class="browse btn btn-primary" type="button" style="height: 35px;">
									<i class="glyphicon glyphicon-search"></i> Browse
								</button>
							</span>
						</div>

						<input type="submit" class="btn btn-success" value="Upload" id="btn-upload" onclick="check()" style="height: 35px">
					</div>
				</div>
			</div>
		</div>
		<br/>


		<%-- <c:if test="${x == 'true'}"> --%>

			<div id="hideDiv" class="row row-without-margin" style="display: none">
				<div class="row row-without-margin">
					<!-- 	<div class="col-md-12">
			<span class="map-header">Please Map Template Fields</span>
			<hr>
		</div> -->
					<div class="row row-margin-unset tab_text" style="background-color: unset;">
						<p>Map Purchase Order Fields</p>
						<hr>
					</div>
					<div class="col-md-12">
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="po_number">PO Number</label>
								<select id="po_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="shipper">Shipper</label>
								<select id="shipper_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group'>
								<label class="text-center" id="total_gw">Total Gross Weight</label>
								<select id="total_gw_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group'>
								<label class="text-center" id="carton_length">Carton Length</label>
								<select id="carton_length_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
					</div>
					<br>
					<div class="col-md-12 booking-update-padding-column">
						<div class="col-md-3">
							<div class='form-group'>
								<label class="text-center" id="carton_height">Carton Height</label>
								<select id="carton_height_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="carton_width">Carton Width</label>
								<select id="carton_width_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="total_pieces">Total Pieces</label>
								<select id="total_pieces_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center">Carton Quantity</label>
								<select id="carton_quantity_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-12 booking-update-padding-column">

						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="carton_unit">Carton Unit</label>
								<select id="carton_unit_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>

						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="total_cbm">Total CBM</label>
								<select id="total_cbm_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="hs_code">HS Code</label>
								<select id="hs_code_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="commodity">Commodity</label>
								<select id="commodity_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="row row-without-margin">
					<!-- shipper -->
					<div class="col-md-12 booking-update-padding-column">

						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="style_no">Style No</label>
								<select id="style_no_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="wh_code">Warehouse Code</label>
								<select id="wh_code_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="size_no">Size No</label>
								<select id="size_no_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="sku">SKU</label>
								<select id="sku_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-12 booking-update-padding-column">

						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center">Article No</label>
								<select id="article_no_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center">Color</label>
								<select id="color_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center">Total Net Weight</label>
								<select id="total_nw_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="pieces_ctn">Pieces Carton</label>
								<select id="pieces_ctn_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-12 booking-update-padding-column">

						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="carton_sr_no">Carton Serial No</label>
								<select id="carton_sr_no_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center">Reference 1</label>
								<select id="reference_1_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="reference_2">Reference 2</label>
								<select id="reference_2_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="reference_3">Reference 3</label>
								<select id="reference_3_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<br/>
					</div>
					<div class="row row-margin-unset tab_text" style="background-color: unset;">
						<p>Map Commercial Invoice/Route Fields</p>
						<hr>
					</div>
					<div class="col-md-12 booking-update-padding-column">
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="comm_inv">Commercial Invoice</label>
								<select id="comm_inv_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="comm_inv_date">Commercial Invoice Date</label>
								<select id="comm_inv_date_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="terms_of_shipment">Terms of Shipment</label>
								<select id="terms_of_shipment_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="freight_mode">Freight Mode</label>
								<select id="freight_mode_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>

					</div>
					<!-- notify party -->
					<div class="col-md-12 booking-update-padding-column">

						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="division">Division</label>
								<select id="division_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="pol">Port of Loading</label>
								<select id="pol_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="pod">Port of Delivery</label>
								<select id="pod_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="place_of_delivery">Place of Delivery</label>
								<select id="place_of_delivery_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
					</div>
					<div class="col-md-12 booking-update-padding-column">

						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="place_of_delivery_address">Place of Delivery Address</label>
								<select id="place_of_delivery_address_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="cargo_handover_date">Cargo Handover Date</label>
								<select id="cargo_handover_date_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row row-margin-unset tab_text" style="background-color: unset;">
						<p>Feeder/Flight 1 information Fields</p>
						<hr>
					</div>
					<div class="col-md-12 booking-update-padding-column">
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="fvsl">Feeder Vessel Name</label>
								<select id="fvsl_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="voyage1">Voyage No/Flight No</label>
								<select id="voyage1_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="etd">Departure Date</label>
								<select id="etd_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="transhipment1eta">Arrival Date</label>
								<select id="transhipment1eta_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						
					<!-- 	<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="mvsl3">Mother Vessel 3 Name</label>
								<select id="mvsl3_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div> -->

					</div>
					<div class="row row-margin-unset tab_text" style="background-color: unset;">
						<p>Mother Vessel 1/Flight 2 information Fields</p>
						<hr>
					</div>
					<div class="col-md-12 booking-update-padding-column">
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="mvsl1">Mother Vessel 1 Name</label>
								<select id="mvsl1_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="voyage2">Voyage No/Flight No</label>
								<select id="voyage2_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="transhipment1etd">Departure Date</label>
								<select id="transhipment1etd_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="transhipment2eta">Arrival Date</label>
								<select id="transhipment2eta_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						
						

					</div>
					<div class="row row-margin-unset tab_text" style="background-color: unset;">
						<p>Mother Vessel 2/Flight 3 information Fields</p>
						<hr>
					</div>
					<div class="col-md-12 booking-update-padding-column">
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="mvsl2">Mother Vessel 2 Name</label>
								<select id="mvsl2_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="voyage3">Voyage No/Flight No</label>
								<select id="voyage3_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="transhipment2etd">Departure Date</label>
								<select id="transhipment2etd_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						

					</div>
					<div class="row row-margin-unset tab_text" style="background-color: unset;">
						<p>Arrival Information</p>
						<hr>
					</div>
					<div class="col-md-12 booking-update-padding-column">	
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="atd">Actual Departure Date</label>
								<select id="atd_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>					
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="eta">Expected Arrival Date</label>
								<select id="eta_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div class='form-group required'>
								<label class="text-center" id="ata">Actual Arrival Date</label>
								<select id="ata_options" class="form-control drop">
									<option value="">Please Select</option>
								</select>
							</div>
						</div>

					</div>
					
				</div>

				<div class="row" style="text-align: center; padding-top:15px;padding-bottom:15px;">
					<!-- <button type="submit" class="btn btn-primary btn_addmore" id="btn-submit" onclick="buildJson()" name="singlebutton" style="border-radius: 5px; height: 35px; padding-top: 8px;">Save
						Template
					</button> -->
					<button type="submit" class="btn btn-primary btn_addmore" id="btn-submit" onclick="openConfimationBox()" name="singlebutton" style="border-radius: 5px; height: 35px; padding-top: 8px;">Save
							Template
						</button>
					<button type="button" class="btn btn-danger btn_addmore" name="singlebutton" style="border-radius: 5px; height: 35px; width:130px; padding-top: 8px;">Cancel</button>
				</div>
			</div>
			<%-- </c:if> --%>
	</div>
	<div class="modal fade" id="templateExistsModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Error in saving Template</h4>
				</div>
				<div class="modal-body" style="text-align:center;">
					<p id=msgBody>Template for this Buyer already exists!!!</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" data-dismiss="modal" onclick="rebuildJson()">Update Template</button>
					<button type="button" class="btn btn-danger" data-dismiss="modal">Cancel</button>
				</div>
			</div>

		</div>
	</div>
	<div class="modal fade" id="templateSaveSuccessModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Buyer Template Update</h4>
				</div>
				<div class="modal-body" style="text-align:center;">
					<p id=savemsg>Template Updated Successfully!!!</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
	<div class="modal fade" id="templateUpdateErrorModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Buyer Template Update</h4>
				</div>
				<div class="modal-body" style="text-align:center;">
					<p id=msgBody>Template Update Unsuccessful!!!</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="templateUploadConfirmation" role="dialog">
			<div class="modal-dialog">
	
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Buyer Template Update</h4>
					</div>
					<div class="modal-body" style="text-align:center;">
						<p id=confirmationBody>Do you want to upload template for  <strong style="color: red">"${buyer}" </strong></p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal" onclick="buildJson()">Yes</button>
						<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
					</div>
				</div>
			</div>
		</div>
	<jsp:include page="footer_v2.jsp"></jsp:include>