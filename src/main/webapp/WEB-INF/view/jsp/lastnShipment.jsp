<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<script type="text/javascript">
</script>
<style>
html, body {
	background-color: #fff;
	/* margin-bottom: 25px; */
}

.table-borderless>tbody>tr>td, .table-borderless>tbody>tr>th,
	.table-borderless>tfoot>tr>td, .table-borderless>tfoot>tr>th,
	.table-borderless>thead>tr>td, .table-borderless>thead>tr>th {
	border: none;
}

.table-borderless {
	margin-bottom: 0px !important;
}
</style>

<div class="container-fluid container-fluid-441222">
	<div class="row row-without-margin">
		<h1 class="h1-report">Last N Shipment</h1>
		<ol class="breadcrumb">
			<li><i class="fa fa-bar-chart"></i>&nbsp;Report Management</li>
			<li class="active">Last N Shipment</li>
		</ol>
	</div>
	<hr>
	<div class="row row-without-margin">
		<form:form action="lastNShipmentData" method="post"
			commandName="lastNShipmentParams">
			<div class="col-md-2 col-sm-12 col-xs-12">
				<div class='form-group'>
					<label>Number Of Shipments</label>
					<form:input path="number" id="number" cssClass="form-control"
						placeholder="ex: 10" required="true" />
				</div>
			</div>
			<div class="col-md-2 col-sm-6 col-xs-6">
				<label style="display: block; color: transparent">.</label>
				<!-- added for alignment purpose, dont delete -->
				<input type="submit" id="btn-appr-po-search"
					class="btn btn-success form-control" value="Submit" />
			</div>
		</form:form>
	</div>
	<br>

<!-- 	<div class="row row-without-margin table_header">
		<div class="col-lg-11 col-md-11 col-sm-10 col-xs-9">
			<span class="glyphicon glyphicon-triangle-bottom"></span>
			<h3>Search Result</h3>
		</div>
	</div> -->
	<div class="row row-without-margin">
		<table class="table table-hover" id="lastNshipmentTable">
			<thead style="background-color: #252525; color: #fff;">
				<tr>
					<th>HBL NO</th>
					<th>HBL Date</th>
					<th>Comm. Inv</th>
					<th>Fvsl Name</th>
					<th>Fvsl ETD</th>
					<th>Fvsl ETA</th>
					<th>Mvsl Name</th>
					<th>Mvsl ETD</th>
					<th>Mvsl ETA</th>
					<th>POL</th>
					<th>POD</th>

				</tr>
			</thead>
			<tbody>
				<c:set var="user" value="${lastNShipmentParams.kunnr}" />
				<c:set var="userinit" value="${fn:substring(user, 0, 3)}" />
				<%-- 					<c:out value="${userinit}"></c:out> --%>
				<c:forEach items="${itHeaderLst}" var="i">
					<tr>
						<%-- 							<c:out value="${lastNShipmentParams.spart}"></c:out>							 --%>
						<c:choose>
							<c:when test="${lastNShipmentParams.spart == 'AR'}">
								<c:choose>
									<c:when test="${userinit eq '100'}">
										<td style="cursor: pointer"><a data-toggle="modal"
											data-target="#airModalShipper" data-id="${i.zzhblhawbno }">${i.zzhblhawbno }</a></td>
									</c:when>
									<c:when test="${userinit eq '101' || userinit eq '102'}">
										<td style="cursor: pointer"><a data-toggle="modal"
											data-target="#airModalBuyer" data-id="${i.zzhblhawbno }">${i.zzhblhawbno }</a></td>
									</c:when>
									<c:when test="${userinit eq '104' || userinit eq '106'}">
										<td style="cursor: pointer"><a data-toggle="modal"
											data-target="#airModalAgent" data-id="${i.zzhblhawbno }">${i.zzhblhawbno }</a></td>
									</c:when>
								</c:choose>
							</c:when>
							<c:when test="${lastNShipmentParams.spart == 'SE'}">
								<c:choose>
									<c:when test="${userinit eq '100'}">
										<td style="cursor: pointer"><a data-toggle="modal"
											data-target="#shipmentModalShipper"
											data-id="${i.zzhblhawbno }">${i.zzhblhawbno }</a></td>
									</c:when>
									<c:when test="${userinit eq '101' || userinit eq '102'}">
										<td style="cursor: pointer"><a data-toggle="modal"
											data-target="#shipmentModalBuyer" data-id="${i.zzhblhawbno }">${i.zzhblhawbno }</a></td>
									</c:when>
									<c:when test="${userinit eq '104' || userinit eq '106'}">
										<td style="cursor: pointer"><a data-toggle="modal"
											data-target="#shipmentModalAgent" data-id="${i.zzhblhawbno }">${i.zzhblhawbno }</a></td>
									</c:when>
								</c:choose>
							</c:when>
						</c:choose>
						<%-- 							<td style="cursor:pointer"><a data-toggle="modal" data-target="#shipmentModal" data-id="${i.zzhblhawbno }" >${i.zzhblhawbno }</a></td> --%>
						<td><fmt:formatDate type="date" value="${i.zzhblhawbdt }" /></td>
						<td>${i.zzcomminvno }</td>
						<td>${i.zzairlinename1 }</td>
						<td><fmt:formatDate type="date" value="${i.fvetdport }" /></td>
						<td><fmt:formatDate type="date" value="${i.fvetaport }" /></td>
						<td>${i.zzairlinename2 }</td>
						<td><fmt:formatDate type="date" value="${i.mvetdport }" /></td>
						<td><fmt:formatDate type="date" value="${i.mvetaport }" /></td>
						<td>${i.zzportofloading }</td>
						<td>${i.zzportofdest }</td>

					</tr>

				</c:forEach>
			</tbody>
		</table>

	</div>
	<!-- Modal -->
	<!-- 		<div class="container"> -->
	<!-- 			<div class="well assign-po-search"> -->
	<div class="modal fade" id="shipmentModalShipper" role="dialog">
		<div class="modal-dialog" style="width: 600px;">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header"
					style="background-color: #222534; color: #fff;">
					<button type="button" class="close" data-dismiss="modal"
						style="color: lightgray;">&times;</button>
					<h4 class="modal-title">Quick Access</h4>
				</div>
				<div class="modal-body" id="shipmentDetails">
					<!-- 								<p>Some text in the modal.</p> -->
					<!-- 								<a href=getTrackingInfoFrmUrl?searchString=${i.zzhblhawbno }>HB/L Details</a> -->
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>
	<div class="modal fade" id="shipmentModalBuyer" role="dialog">
		<div class="modal-dialog" style="width: 600px;">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header"
					style="background-color: #252525; color: #fff;">
					<button type="button" class="close" data-dismiss="modal"
						style="color: lightgray;">&times;</button>
					<h4 class="modal-title">Quick Access</h4>
				</div>
				<div class="modal-body" id="shipmentDetailsB">
					<!-- 								<p>Some text in the modal.</p> -->
					<!-- 								<a href=getTrackingInfoFrmUrl?searchString=${i.zzhblhawbno }>HB/L Details</a> -->
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>
	<div class="modal fade" id="shipmentModalAgent" role="dialog">
		<div class="modal-dialog" style="width: 600px;">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header"
					style="background-color: #252525; color: #fff;">
					<button type="button" class="close" data-dismiss="modal"
						style="color: lightgray;">&times;</button>
					<h4 class="modal-title">Quick Access</h4>
				</div>
				<div class="modal-body" id="shipmentDetailsA">
					<!-- 								<p>Some text in the modal.</p> -->
					<!-- 								<a href=getTrackingInfoFrmUrl?searchString=${i.zzhblhawbno }>HB/L Details</a> -->
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>
	<div class="modal fade" id="airModalShipper" role="dialog">
		<div class="modal-dialog" style="width: 600px;">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header"
					style="background-color: #252525; color: #fff;">
					<button type="button" class="close" data-dismiss="modal"
						style="color: lightgray;">&times;</button>
					<!-- 								<h4 class="modal-title">Reports</h4> -->
					<h4 class="modal-title">Quick Access</h4>
				</div>
				<div class="modal-body" id="airShipmentDetails">
					<!-- 								<p>Some text in the modal.</p> -->
					<!-- 								<a href=getTrackingInfoFrmUrl?searchString=${i.zzhblhawbno }>HB/L Details</a> -->
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>
	<div class="modal fade" id="airModalBuyer" role="dialog">
		<div class="modal-dialog" style="width: 600px;">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header"
					style="background-color: #252525; color: #fff;">
					<button type="button" class="close" data-dismiss="modal"
						style="color: lightgray;">&times;</button>
					<!-- 								<h4 class="modal-title">Reports</h4> -->
					<h4 class="modal-title">Quick Access</h4>
				</div>
				<div class="modal-body" id="airShipmentDetailsB">
					<!-- 								<p>Some text in the modal.</p> -->
					<!-- 								<a href=getTrackingInfoFrmUrl?searchString=${i.zzhblhawbno }>HB/L Details</a> -->
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>
	<div class="modal fade" id="airModalAgent" role="dialog">
		<div class="modal-dialog" style="width: 600px;">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header"
					style="background-color: #222534; color: #fff;">
					<button type="button" class="close" data-dismiss="modal"
						style="color: lightgray;">&times;</button>
					<!-- 								<h4 class="modal-title">Reports</h4> -->
					<h4 class="modal-title">Quick Access</h4>
				</div>
				<div class="modal-body" id="airShipmentDetailsA">
					<!-- 								<p>Some text in the modal.</p> -->
					<!-- 								<a href=getTrackingInfoFrmUrl?searchString=${i.zzhblhawbno }>HB/L Details</a> -->
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>
</div>

<%@include file="footer_v2_fixed.jsp"%>
<script>
    $(document).ready(function() {
        $('#shipmentModalShipper').on('show.bs.modal', function(event) {

            var getId = $(event.relatedTarget).data('id');
            //$("#shipmentDetails").html('Selected: ' + getId);
            $("#shipmentDetails").html(            
            '<table class="table table-borderless">'+
            	'<tr>'+
  					'<td><a href=shippingOrderFromUrl?searchString='+getId+'>Shipping Order</a></td>'+
  				'</tr>'+
      			'<tr>'+
					'<td><a target="_blank" href=getSeaBillOfLandingFromUrl?searchString='+getId+'>Non-Nego Bill Of Lading</a></td>'+
				'</tr>'+
          		'<tr>'+
  					'<td><a href=cargoAcknowledgementFromUrl?searchString='+getId+'>Cargo Acknowledgement Certificate</a></td>'+
  				'</tr>'+
              	'<tr>'+
          			'<td><a href=stuffingAdviceFrmUrl?searchString='+getId+'>Stuffing Advice</a></td>'+
          		'</tr>'+
              	'<tr>'+
          			'<td><a href=getSeaExportInvoiceListFromUrl?searchString='+getId+'>Freight Invoice</a></td>'+
          		'</tr>'+
  				'<tr>'+
              		'<td><a href=getTrackingInfoFrmUrl?searchString='+getId+'>Shipment Details</a></td>'+              		
            	'</tr>'+              	
          	'</table>'
          	);            

        });        
        $('#shipmentModalBuyer').on('show.bs.modal', function(event) {

            var getId = $(event.relatedTarget).data('id');
            //$("#shipmentDetails").html('Selected: ' + getId);
            $("#shipmentDetailsB").html(            
            '<table class="table table-borderless">'+
      			'<tr>'+
					'<td><a target="_blank" href=getSeaBillOfLandingFromUrl?searchString='+getId+'>Non-Nego Bill Of Lading</a></td>'+
				'</tr>'+
          		'<tr>'+
  					'<td><a href=cargoAcknowledgementFromUrl?searchString='+getId+'>Cargo Acknowledgement Certificate</a></td>'+
  				'</tr>'+
              	'<tr>'+
          			'<td><a href=stuffingAdviceFrmUrl?searchString='+getId+'>Stuffing Advice</a></td>'+
          		'</tr>'+
  				'<tr>'+
              		'<td><a href=getTrackingInfoFrmUrl?searchString='+getId+'>Shipment Details</a></td>'+              		
            	'</tr>'+              	
          	'</table>'
          	);            

        });        
        $('#shipmentModalAgent').on('show.bs.modal', function(event) {

            var getId = $(event.relatedTarget).data('id');
            //$("#shipmentDetails").html('Selected: ' + getId);
            $("#shipmentDetailsA").html(            
            '<table class="table table-borderless">'+
          		'<tr>'+
  					'<td><a href=cargoAcknowledgementFromUrl?searchString='+getId+'>Cargo Acknowledgement Certificate</a></td>'+
  				'</tr>'+
              	'<tr>'+
          			'<td><a href=stuffingAdviceFrmUrl?searchString='+getId+'>Stuffing Advice</a></td>'+
          		'</tr>'+
  				'<tr>'+
              		'<td><a href=getTrackingInfoFrmUrl?searchString='+getId+'>Shipment Details</a></td>'+              		
            	'</tr>'+              	
          	'</table>'
          	);            

        });
        $('#airModalShipper').on('show.bs.modal', function(event) {

            var getId = $(event.relatedTarget).data('id');
            //$("#shipmentDetails").html('Selected: ' + getId);
            $("#airShipmentDetails").html(            
            '<table class="table table-borderless">'+            	
				'<tr>'+
					'<td><a target="_blank" href=getAirBillOfLandingFromUrl?searchString='+getId+'>Non-Nego Bill Of Lading</a></td>'+
				'</tr>'+
  				'<tr>'+
					'<td><a href=cargoAcknowledgementFromUrl?searchString='+getId+'>Cargo Acknowledgement Certificate</a></td>'+
				'</tr>'+
      			'<tr>'+
  					'<td><a href=getAirExportInvoiceListFromUrl?searchString='+getId+'>Freight Invoice</a></td>'+
  				'</tr>'+
/* 				'<tr>'+
      				'<td><a href=getTrackingInfoFrmUrl?searchString='+getId+'>Shipment Details</a></td>'+              		
    			'</tr>'+   */          	
          	'</table>'
          	);            

        });        
        $('#airModalBuyer').on('show.bs.modal', function(event) {

            var getId = $(event.relatedTarget).data('id');
            //$("#shipmentDetails").html('Selected: ' + getId);
            $("#airShipmentDetailsB").html(            
            '<table class="table table-borderless">'+            	
				'<tr>'+
					'<td><a target="_blank" href=getAirBillOfLandingFromUrl?searchString='+getId+'>Non-Nego Bill Of Lading</a></td>'+
				'</tr>'+
  				'<tr>'+
					'<td><a href=cargoAcknowledgementFromUrl?searchString='+getId+'>Cargo Acknowledgement Certificate</a></td>'+
				'</tr>'+
/* 				'<tr>'+
      				'<td><a href=getTrackingInfoFrmUrl?searchString='+getId+'>Shipment Details</a></td>'+              		
    			'</tr>'+   */          	
          	'</table>'
          	);            

        });        
        $('#airModalAgent').on('show.bs.modal', function(event) {

            var getId = $(event.relatedTarget).data('id');
            //$("#shipmentDetails").html('Selected: ' + getId);
            $("#airShipmentDetailsA").html(            
            '<table class="table table-borderless">'+				
  				'<tr>'+
					'<td><a href=cargoAcknowledgementFromUrl?searchString='+getId+'>Cargo Acknowledgement Certificate</a></td>'+
				'</tr>'+
/* 				'<tr>'+
      				'<td><a href=getTrackingInfoFrmUrl?searchString='+getId+'>Shipment Details</a></td>'+              		
    			'</tr>'+   */          	
          	'</table>'
          	);            

        });

    }); 
    $(function() {
		$('#lastNshipmentTable').DataTable();
	});
    /* $('#lastNshipmentTable').DataTable({
    	
    	responsive : true,
    	dom : 'Bfrtip',
    	buttons : [ 'csv', 'excel' ]

    }); */
</script>

<script
	src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
