
<%@include file="header.jsp" %>       

        <!--END OF HEADER-->
        
        
        
        <section id="bill-of-landing">
          <section>
            <div class="container-fluid">
             <div class="row">
                <div class="input-group-addon commercial">
                    <h4 style="text-align:left;">Tracking</h4>
				</div>
             </div>
            </div>
        </section>
        
        
        <div class="col-xs-12" style="height:20px;"></div>
            
<div class="container" style="width:80%; margin: 0 20% 0 12%;">       
<!-- Nav tabs -->
<ul class="tracking-items">
    <li id="booking-conf" class="stateC">
        <a class="tracking-item active">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Booking Confirm</a>
    </li>
    <li id="goods-rec" class="stateA">
        <a class="tracking-item">Goods Receive</a>
    </li>
    <li id="stuffing-done" class="stateB">
        <a class="tracking-item">Stuffing Done</a>
    </li>
    <li id="shipped-on" class="stateB">
        <a class="tracking-item">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Shipped Onboard</a>
    </li>
    <li id="delivered" class="stateB">
        <a class="tracking-item">Delivered</a>
    </li>
   <!--  <li id="invoice-do" class="stateB">
        <a class="tracking-item">Invoice Done</a>
    </li> -->
</ul>



      <div style="clear:both;"></div>

      <!--<div class="tab-pane" id="panel1" role="tabpanel">
        <p style="font-size:18px;padding-left:10px;"></p>
        <br>
      <section>
            <div class="container" style="margin-top:50px;">
            
            <table class="table commercial-table container-table2">
             <thead>
               <tr>
                   <td>Order No</td>
                   <td>Style No</td>
                   <td>Colour</td>
                   <td>Container No</td>
                   <td>Size</td>
                   <td>Seal</td>
                   <td>Stuffing Date</td>
                   <td>Mode</td>
                   <td>PCS</td>
                   <td>CBM</td>
                   <td>CTNS</td>   
               </tr>
            </thead>
               
               <tbody>  
                   <tr>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                   </tr>
               </tbody>
            </table>
            </div> 
        </section>  
    </div>-->
</div> 
</section>

        <section>
            <div class="container" style="width:75%; margin: 0 auto;">
              <div class="col-xs-12" style="height:40px;"></div>
              <div class="row">          
            <div class="col-md-12">
            <table class="table commercial-table package-detail" style="margin-top:-10px;">
             <thead>
               <tr>
                   <td colspan="3" style="font-size:14px;">Package Details</td>
               </tr>
            </thead>
               
               <tbody>
                   
                   <tr>
                     <td>Shipped To :<p style="font-weight:normal;">JMD Pacific Square, 5th Floor,MGH Group, Gurgaon</p></td>
                       <td>Data1 :<p style="font-weight:normal;"></p></td>
                     <td>Data2 :<p style="font-weight:normal;"></p></td>
                   </tr>
                   <tr>
                     <td>Total Weight :<p style="font-weight:normal;"></p></td>
                     <td>Data3 :<p style="font-weight:normal;"></p></td>
                     <td>Data4 :<p style="font-weight:normal;"></p></td>
                   </tr>
                   <tr>
                     <td>Service :<p style="font-weight:normal;"></p></td>
                     <td>GM :<p style="font-weight:normal;"></p></td>
                     <td>Data9 :<p style="font-weight:normal;"></p></td>
                   </tr>
                   
                   <tr>
                     <td>Data5 :<p style="font-weight:normal;"></p></td>
                     <td>Data6 :<p style="font-weight:normal;"></p></td>
                     <td>Data10 :<p style="font-weight:normal;"></p></td>
                   </tr>
                   
                   <tr>
                     <td>Data7 :<p style="font-weight:normal;"></p></td>
                     <td>Data8 :<p style="font-weight:normal;"></p></td>
                     <td>Data9 :<p style="font-weight:normal;"></p></td>
                   </tr>
               </tbody>
            </table>
            </div>
            </div>
        </div>
        </section>

<script type="text/javascript">
	

	
	$(document).ready(function() {
		//var trackingObject = "${trackingDto.zzmblmawbno}";
		var bookingConfirmed = "${trackingStatusParams.bookingDate}";
		var goodsReceived = "${trackingStatusParams.goodsReceivedDate}";
		var stuffingDone = "${trackingStatusParams.stuffingDate}";
		var shippedOnboard = "${trackingStatusParams.shippedOnboardDate}";
		var delivered = "${trackingStatusParams.deliveredDate}";

		
		/*if (bookingConfirmed != null) {			
			$('#booking-conf').removeClass('stateC').addClass('stateC');
		}*/
		if (bookingConfirmed == null || bookingConfirmed == '') {
			$('#booking-conf').removeClass('stateC').addClass('stateA');
		}
		if (goodsReceived != null && goodsReceived != '') {
			$('#goods-rec').removeClass('stateA').addClass('stateC');
		}
		if (goodsReceived == null || goodsReceived == '') {
			if (bookingConfirmed != null && bookingConfirmed != '') {
				//$('#goods-rec').removeClass('stateA').addClass('stateC');
			}
			if (bookingConfirmed == null || bookingConfirmed == '') {
				$('#goods-rec').removeClass('stateA').addClass('stateB');
			}

		}
		//Stuffing done
		if (stuffingDone != null && stuffingDone != '') {
			
			$('#stuffing-done').removeClass('stateB').addClass('stateC');
		}
		if (stuffingDone == null || stuffingDone == '') {
			
			if (goodsReceived != null && goodsReceived != '') {
				$('#stuffing-done').removeClass('stateB').addClass('stateA');
			}
			if (goodsReceived == null || goodsReceived == '') {
				//$('#stuffing-done').removeClass('stateB').addClass('stateB');
			}

		}

		//Shipped Onboard
		if (shippedOnboard != null && shippedOnboard != '') {
			//alert("in sh")
			$('#shipped-on').removeClass('stateB').addClass('stateC');
		}
		if (shippedOnboard == null || shippedOnboard == '') {
			if (stuffingDone != null && stuffingDone != '') {
				$('#shipped-on').removeClass('stateB').addClass('stateA');
			}
			if (stuffingDone == null || stuffingDone == '') {
				//$('#stuffing-done').removeClass('stateB').addClass('stateB');
			}

		}

		//Delivered
		if (delivered != null && delivered != '') {
			$('#delivered').removeClass('stateB').addClass('stateC');
		}
		if (delivered == null || delivered == '') {
			if (shippedOnboard != null && shippedOnboard != '') {
				$('#delivered').removeClass('stateB').addClass('stateA');
			}
			if (shippedOnboard == null || shippedOnboard == '') {
				//$('#stuffing-done').removeClass('stateB').addClass('stateB');
			}

		}

	});
</script> 
 <%@include file="footer.jsp" %>