<jsp:include page="header.jsp"></jsp:include>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
		<section class="main-body">
		<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
    <div class="col-sm-6">
    	<!--  <h4 class="ship_desti" style="margin-top: 10px;">Supplier Wise Shipment</h4> -->
    </div>
    	  <!--  <div class='col-sm-3 dash-ship-frm-div' style="">
                       
                       <label class="dash-ship-frm-lbl">From Date</label>-->
                       <input id="dashboard_from_date" type="hidden" class="date-picker glyphicon glyphicon-calendar dashboard-ship-frm-date" style="width: 160px;"/>
                  
         <!-- </div>-->
           <!-- <div class='col-sm-3 dash-ship-to-div' style="">
                 
                   <label class="dash-ship-to-lbl">To Date</label>-->
                   <input id="dashboard_to_date" type="hidden" class="date-picker glyphicon glyphicon-calendar dashboard-ship-to-date" style="width: 160px;"/>
                   <!--  <span style="display: inline;"><a href="${pageContext.request.contextPath}/download/excel/dashboard/dashboard-excel(Buyer).xlsx" title="Export Dashboard data to excel" alt="Export Dashboard data to excel"><img class="excel-export" src="${pageContext.request.contextPath}/resources/images/excel.png"/></a> </span>--> 
          <!-- </div> -->
    </div>
  </div><!--/row-->
  
  <div class="row">
    
   
    <div class="col-md-6">
    <div class="panel panel-default" style="margin-left:10px">
    <div class="panel-heading" style="background-color: #222534;color: white;font-weight: bold;">Exceptions</div>
  <div class="panel-body">
        <!--  <div id="bar-example" style="height: 250px;"></div>-->
       <table width="100%" align="center" style="margin-left:0px">
       <tr>
       	<td ><a href="getExceptionReportByStatus?status=DELAYED"><button type="button" class="btn btn-danger btn-circle btn-xl " style="margin-left:0px;margin-top:0px;"><i class="glyphicon glyphicon-hourglass"></i>  </button></a>  <strong style="font-size:15px">  	<a href="getExceptionReportByStatus?status=DELAYED">    Delays - ${eb.delayed}</a> </strong></td>
       	<td><a href="getExceptionReportByStatus?status=ON-TIME"><button type="button" class="btn btn-success btn-circle btn-xl" style="margin-left:0px;margin-top:0px;"><i class="glyphicon glyphicon-ok"></i></button></a>  <strong style="font-size:15px">   <a href="getExceptionReportByStatus?status=ON-TIME">     On Time  - ${eb.ontime}</a></strong></td>
       	<td><a href="getExceptionReportByStatus?status=ADVANCE"><button type="button" class="btn btn-primary btn-circle btn-xl" style="margin-left:0px;margin-top:10px;margin-bottom:10px;"><i class="glyphicon glyphicon-time"></i></button></a> <strong style="font-size:15px">   <a href="getExceptionReportByStatus?status=ADVANCE">    Early Arrivals - ${eb.advance}</a></strong></td>
      
       </tr>
       <tr>
       	</tr>
       
       </table>
        
      
     </div>
     
     
	</div>
</div>   
    
	 <div class="col-md-6" style="margin-top: ;">
        <div class="panel panel-default" >
         <div class="panel-heading" style="background-color: #222534;color: white;font-weight: bold;">Quick Stats</div>
  <div class="panel-body" >
		<div class="col-lg-4 col-md-6 btn-primary" style="border-radius: 25px;margin-top:8px;margin-left:17px;">
    <div class=" panel-green rectangle ">
        <div class="panel-heading">
            <div class="row">
                
                <div class="col-xs-9 text-center">
                    <a href="#total-shipment" class="link-color"><div class="huge" style="font-size:30px;font-align:center;" id="total-shipment"></div></a>
                    <div>
					<a href="#total-shipment" class="link-color">Total Shipment</a>
					</div>
                </div>
            </div>
        </div>
      
    </div>
</div>
<div class="col-lg-4 col-md-6 btn-success" style="border-radius: 25px;margin-top:8px;">
    <div class=" panel-green rectangle ">
        <div class="panel-heading">
            <div class="row rectangle ">
                
                <div class="col-xs-9 text-center">
                    <a href="#total-cbm" class="link-color"><div class="huge" style="font-size:30px;font-align:center;" id="total-cbm"></div></a>
                    <div>
					<a href="#total-cbm" class="link-color">Total CBM</a>
					</div>
                </div>
            </div>
        </div>
      
    </div>
</div>
<div class="col-lg-4 col-md-6 btn-warning" style="border-radius: 25px;margin-top:8px;">
    <div class=" panel-green rectangle">
        <div class="panel-heading">
            <div class="row">
                
                <div class="col-xs-9 text-center">
                    <a href="#total-gwt" class="link-color"><div class="huge" style="font-size:30px;font-align:center;" id="total-gwt"></div></a>
                    <div>
					<a href="#total-gwt" class="link-color">Total GWT (KG)</a>
					</div>
                </div>
            </div>
        </div>
      
    </div>
</div>
</div></div>
</div>
    </div>
  </div>
</div>



<!-- Recent Shipment -->
<!--  
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
    	<h4 class="ship_desti" style="font-weight:bold;">Recent Shipments</h4>  
    </div>
  </div><!--/row-->
  <!--  
  <div class="row">
    
   <div class="table-responsive">
    <div class="col-md-12">
      <table class="table table-striped" id="tbl-recent-shipment">
		  <thead style="">
      <tr style="background-color:#222534;color:#fff">
        <th>HBL NO</th>
        <th>Comm. Inv</th>
        <th>POL</th>
		<th>POD</th>
		<th>Fvsl Name</th>
		<th>Fvsl ETD</th>
		<th>Fvsl ETA</th>
		<th>Mvsl Name</th>
		<th>Mvsl ETD</th>
		<th>Mvsl ETA</th>
	  </tr>
    </thead>
    <tbody>
      
    </tbody>
  </table>
    </div>
    </div>
	 <div class="col-md-4">
        
	
<!-- <div id="chartContainer" style="width: 100%; height: 300px"></div> -->
  <!--   </div>
  </div>
</div>
-->

<!-- Top 5 Shipments -->

<div class="container-fluid">
  <div class="row">
  <div class="col-md-6">
    <div class="panel panel-default" style="margin-left:10px">
    <div class="panel-heading" style="background-color: #222534;color: white;font-weight: bold;">Top 5 Origins</div>
  <div class="panel-body">
    <div class="col-sm-12">
    	<table class="table table-striped" id="tbl-top-shipment">
		  <thead style="">
      <tr style="background-color:#222534;font-weight:bold;color:#fff">
        <th>Port of Origin</th>
        <th>Total Shipment</th>
        <th>Total CBM</th>
		<th>Total Gross Weight</th>
		
      </tr>
    </thead>
    <tbody>
      
    </tbody>
  </table>
    </div>
    
    </div>
    </div>
    </div>
    
    <div class="col-md-6">
    <div class="panel panel-default" style="margin-left:">
    <div class="panel-heading" style="background-color: #222534;color: white;font-weight: bold;">Recent Shipments</div>
  <div class="panel-body">
    <div class="col-sm-12">
    	<div class="table-responsive">
    <div class="col-md-12">
      <table class="table table-striped" id="tbl-recent-shipment">
		  <thead style="">
      <tr style="background-color:#222534;color:#fff">
        <th>HBL NO</th>
        <th>MBL NO</th>
        <th>POL</th>
		<th>POD</th>
	  </tr>
    </thead>
    <tbody>
      
    </tbody>
  </table>
    </div>
    </div>
    </div>
    
    </div>
    </div>
    </div>
  </div><!--/row-->
 
  <!--  <div class="row">
    
   
    
	 <div class="col-md-4">
        
	
<div id="chartContainer" style="width: 100%; height: 300px"></div>
    </div>
  </div>-->
</div>








		</section>
		<jsp:include page="footer.jsp"></jsp:include>
	<%-- 	<script type="text/javascript" src="${pageContext.request.contextPath}\resources\js\canvasjsmin.js"></script> --%>
		<script type="text/javascript" src="${pageContext.request.contextPath}\resources\js\dashboardbuyer.js"></script>
		<script>
		var loginDto=${loginDto};
		var supplierWiseShipSummaryBuyer = ${suppWiseShipSummaryJson};
		var topFiveShipment = ${topFiveShipBuyerJson};
		var lastNShipments = ${lastNShipmentsJsonOutputData};
	 $(document).ready( 
			 
                function () {
                	$.LoadingOverlay("show");
                	var loginDTO=loginDto;
                	if(loginDTO!=undefined)
                	{
                	  $("#dashboard_to_date").datepicker({ dateFormat: "dd-mm-yy"}).datepicker("setDate",loginDTO.dashBoardToDate);
                	  $("#dashboard_from_date").datepicker({ dateFormat: "dd-mm-yy"}).datepicker("setDate",loginDTO.dashBoardFromDate);
                	}
                else
                	{
                	 $("#dashboard_to_date").datepicker({ dateFormat: "dd-mm-yy"}).datepicker("setDate", new Date());
               		 var currentDate=new Date();
               		 currentDate.setMonth(currentDate.getMonth()-2);
               		 $("#dashboard_from_date").datepicker({ dateFormat: "dd-mm-yy"}).datepicker("setDate", currentDate);
                	}
                	var fromDate=$("#dashboard_from_date").val();
                	var toDate=$("#dashboard_to_date").val();
                	
					 drawChart(loginDTO.dashBoardFromDate,loginDTO.dashBoardToDate);
                    $("#dashboard_from_date").change(function(){
                    	var fromDate=$("#dashboard_from_date").val();
                    	var toDate=$("#dashboard_to_date").val();
                    
                    	drawChart(fromDate,toDate);
                    })
                     $("#dashboard_to_date").change(function(){
                    	var fromDate=$("#dashboard_from_date").val();
                    	var toDate=$("#dashboard_to_date").val();
                    
                    	drawChart(fromDate,toDate);
                    })
                    
                    $('a[href="#total-shipment"]').click(function(){
                    	var fromDate=$("#dashboard_from_date").val();
                    	var toDate=$("#dashboard_to_date").val();
                    	var url=myContextPath+'/suppwiseshipdetails?fromDate='+fromDate+'&toDate='+toDate;
                    	window.location.assign(url);
                    })
                     $('a[href="#total-cbm"]').click(function(){
                    	var fromDate=$("#dashboard_from_date").val();
                    	var toDate=$("#dashboard_to_date").val();
                    	var url=myContextPath+'/suppwisecbmdetails?fromDate='+fromDate+'&toDate='+toDate;
                    	window.location.assign(url);
                    })
                    $('a[href="#total-gwt"]').click(function(){
                    	var fromDate=$("#dashboard_from_date").val();
                    	var toDate=$("#dashboard_to_date").val();
                    	var url=myContextPath+'/suppwisegwtdetails?fromDate='+fromDate+'&toDate='+toDate;
                    	window.location.assign(url);
                    })
                   /*  $('a[href="#pod"]').click(function(){
                    	var fromDate=$("#from_date").val();
                    	var toDate=$("#to_date").val();
                    	var pod=this;
                    	var url=myContextPath+'/suppwiseshipdetailsbypoo?fromDate='+fromDate+'&toDate='+toDate;
                    	window.location.assign(url);
                    }) */
                    $.LoadingOverlay("hide");  
                   
                }
                
            );	
			</script>	
