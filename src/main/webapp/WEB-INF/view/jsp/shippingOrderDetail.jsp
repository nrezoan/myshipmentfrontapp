<%@ include file="header_v2.jsp"%>
<%@ include file="navbar.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<style>
html, body {
	background-color: #fff;
}
</style>
<section id="shipping_order" style="background-color:#fff;">
<div class="container-fluid container-fluid-441222">
	<div class="row row-without-margin">
		<h1 class="h1-report">Shipping Order</h1>
		<ol class="breadcrumb">
			<li><i class="fa fa-bar-chart"></i>&nbsp;Report Management</li>
			<li class="active">Shipping Order</li>
		</ol>
	</div>
	<c:if test="${shippingOrderJson.itShipperList.size() == 0 }">
		<div class="row row-without-margin">		
			<div class="alert alert-danger">
  				<strong>No Record found!</strong> Please Enter Valid HBL/HAWB No.
			</div>		
		</div>
	</c:if>
	<c:if test="${shippingOrderJson.itShipperList.size() > 0 }">
		<div class="row row-without-margin" style="display: none;">
			<div class="col-md-6 col-sm-6 col-xs-6">
				<img src="${pageContext.request.contextPath}/resources/images/${shippingOrderJson.itVbak[0].comp_code}.jpg"
						alt="Company Logo" class="img-responsive" style="width: 75px;">
			</div>			
			<div class="col-md-6 col-sm-6 col-xs-6">
				<%-- <img src="data:image/png;base64,${barCodeImage.base64Image}" alt="barcode" class="img-responsive" style="float: right;"/>	 --%>			
				
			</div>		
		</div>
		<div class="row row-without-margin">
			<div class="col-md-8 col-sm-12 col-xs-12">
				<h4 class="h4-report">${shippingOrderJson.itAddList[0].name1}</h4>
				<p>${shippingOrderJson.itAddList[0].name2}</p>
				<p>${shippingOrderJson.itAddList[0].name3}</p>
				<p>${shippingOrderJson.itAddList[0].name4}</p>
				<p>${shippingOrderJson.itAddList[0].telNumber}</p>
			</div>
			<div class="col-md-4 col-sm-6 col-xs-6">
				<%-- <img src="data:image/png;base64,${barCodeImage.base64Image}" alt="barcode" class="img-responsive" style="float: right;"/> --%>
				<p style="text-align:right;"><strong>
					<c:if test="${shippingOrderJson.itVbak[0].orderType== 'ZMSE'}">SEA BOOKING</c:if>
					<c:if test="${shippingOrderJson.itVbak[0].orderType== 'ZSCM'}">SEA BOOKING</c:if>
					<c:if test="${shippingOrderJson.itVbak[0].orderType== 'ZBKT'}">SEA BOOKING</c:if>
					<c:if test="${shippingOrderJson.itVbak[0].orderType== 'ZPSE'}">SEA BOOKING</c:if>
					<c:if test="${shippingOrderJson.itVbak[0].orderType== 'ZMSA'}">SEA-AIR BOOKING</c:if>
					<c:if test="${shippingOrderJson.itVbak[0].orderType== 'ZSSA'}">SEA-AIR BOOKING</c:if>
					<c:if test="${shippingOrderJson.itVbak[0].orderType== 'ZBSA'}">SEA-AIR BOOKING</c:if>
					<c:if test="${shippingOrderJson.itVbak[0].orderType== 'ZMAE'}">AIR BOOKING</c:if>
					<c:if test="${shippingOrderJson.itVbak[0].orderType== 'ZBKA'}">AIR BOOKING</c:if>
					<c:if test="${shippingOrderJson.itVbak[0].orderType== 'ZSCA'}">AIR BOOKING</c:if>
					<c:if test="${shippingOrderJson.itVbak[0].orderType== 'ZMAS'}">AIR-SEA BOOKING</c:if>
					<c:if test="${shippingOrderJson.itVbak[0].orderType== 'ZSAS'}">AIR-SEA BOOKING</c:if>
					<c:if test="${shippingOrderJson.itVbak[0].orderType== 'ZBAS'}">AIR-SEA BOOKING</c:if>	
				</strong></p>
			</div>
			<div class="col-md-2 col-sm-6 col-xs-6" style="display:none">
				<p class="p-below-barcode"><strong>SO No. ${shippingOrderJson.itVbak[0].vbeln}</strong></p>
				<p class="p-below-barcode"><strong>HBL No. ${shippingOrderJson.itVbak[0].bstnk}</strong></p>
			</div>	
		</div>
		<hr>
		<div class="row row-without-margin">
			<div class="col-md-3 col-sm-6 col-xs-6">
				<p><strong>Shipper:</strong></p>
				<p><strong>Cnf Agent:</strong></p>
				<p><strong>Notify:</strong></p>
				<p><strong>Place of Receipt:</strong></p>
				<p><strong>Destination:</strong></p>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6">
				<p><strong>${shippingOrderJson.itShipperList[0].name1}</strong></p>
				<p><strong>
					<c:if test="${shippingOrderJson.itAgentList.size() > 0}">
							${shippingOrderJson.itAgentList[0].name1}
					</c:if>
					<c:if test="${shippingOrderJson.itAgentList.size() == 0}">
							N/A
					</c:if>
					</strong>
				</p>
				<p><strong>
					<c:if test="${shippingOrderJson.itPickList.size() > 0}">
							${shippingOrderJson.itPickList[0].name1}
					</c:if>
					<c:if test="${shippingOrderJson.itPickList.size() == 0}">
							N/A
					</c:if>
					</strong>
				</p>
				<p><strong>${shippingOrderJson.itPortLoad}</strong></p>
				<p><strong>${shippingOrderJson.itPortDest}</strong></p>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6">
				<p><strong>Phone:</strong></p>
				<p><strong>Phone:</strong></p>
				<p><strong>Phone:</strong></p>
				<p><strong>Exp. Delv.of Cargo On:</strong></p>
				<p><strong>HBL No:</strong></p>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6">
				<p><strong>
					<c:if test="${shippingOrderJson.itShipperList[0].telNumber!=''}">
						${shippingOrderJson.itShipperList[0].telNumber}
					</c:if>
					<c:if test="${shippingOrderJson.itShipperList[0].telNumber== ''}">
						N/A
					</c:if>
					</strong>
				</p>
				<p><strong>
					<c:if test="${shippingOrderJson.itAgentList.size() > 0}">
						<c:if test="${shippingOrderJson.itAgentList[0].telNumber!=''}">
							${shippingOrderJson.itAgentList[0].telNumber}
						</c:if>
						<c:if test="${shippingOrderJson.itAgentList[0].telNumber== ''}">
							N/A
						</c:if>
					</c:if>
					<c:if test="${shippingOrderJson.itAgentList.size() == 0}">
							N/A
					</c:if></strong>
				</p>
				<p><strong>
					<c:if test="${shippingOrderJson.itPickList.size() > 0}">
						<c:if test="${shippingOrderJson.itPickList[0].telNumber!=''}">
								${shippingOrderJson.itPickList[0].telNumber}
						</c:if>
						<c:if test="${shippingOrderJson.itPickList[0].telNumber== ''}">
							N/A
						</c:if>
						</c:if>
					<c:if test="${shippingOrderJson.itPickList.size() == 0}">
							N/A
						</c:if>
					</strong>	
				</p>
				<p><strong>
					<fmt:formatDate pattern="dd-MM-yyyy" type="date"
						value="${shippingOrderJson.itVbak[0].zzCargoHandOverDt}" />
					</strong>	
				</p>
				<p><strong>${shippingOrderJson.itVbak[0].bstnk}</strong></p>
			</div>			
		</div>
		<hr>
		<div class="row row-without-margin">
			<div class="table-responsive">
				<table class="table">
					<thead>
						<tr>
							<th>PO No.</th>
							<th>Commodity</th>
							<th>Style No</th>
							<th>SKU</th>
							<th>Carton Qty</th>
							<th>Total Pcs</th>
							<th>Net Wt</th>
							<th>Gross Wt</th>
							<th>Total CBM</th>
						</tr>
					</thead>
					<tbody>
						<c:forEach items="${shippingOrderJson.itVbap}" var="itVbap">
							<tr>
								<td>${itVbap.zzPoNumber}</td>
								<td>${itVbap.arktx}</td>
								<td>${itVbap.zzStyleNo}</td>
								<td>${itVbap.zzSkuNo}</td>
								<fmt:parseNumber var="qty_int" integerOnly="true" type="number"
									value="${itVbap.zzQuantity}" />
								<td>${qty_int}</td>
								<fmt:parseNumber var="pcs_int" integerOnly="true" type="number"
									value="${itVbap.zzTotalNoPcs}" />
								<td>${pcs_int}</td>
								<fmt:formatNumber var="netwt" type="number"
									maxFractionDigits="2" value="${itVbap.zzNtWt}" />
								<td>${netwt}</td>
								<fmt:formatNumber var="grwt" type="number" maxFractionDigits="2"
									value="${itVbap.zzGrWt}" />
								<td>${grwt}</td>
								<td>${itVbap.zzVolume}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
		<br>
		<br>
		<div class="row row-without-margin">
			<div class="col-md-9 col-sm-6 col-xs-6">
				<h4 class="h4-report">To be Filled in by office :</h4>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6">
				<h4 class="h4-report">Signature of Shipper/C &amp; F Agent</h4>
			</div>
		</div>	
		<br>
		<div class="row row-without-margin">
			<div class="col-md-3 col-sm-6 col-xs-6">
				<p>Vessel / Voyage</p>
				<p>Exp. Rot. No.</p>
				<p>Proj. Mother Vsl</p>
				<p>Custom Officer</p>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6">
				<p>${shippingOrderJson.itVbak[0].zzAirlineName1}-
					${shippingOrderJson.itVbak[0].zzAirLineNo1}</p>
				<p>${shippingOrderJson.itVbak[0].zzRotation1}-</p>
				<p>${shippingOrderJson.itVbak[0].zzAirlinerName2}-
					${shippingOrderJson.itVbak[0].zzAirlineNo2}</p>
				<p>-</p>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6">
				<p>Address</p>
				<p>Name of Stevedore</p>
				<p>Carrier/CFS</p>
				<p style="display: none">HBL No.</p>
			</div>
			<div class="col-md-3 col-sm-6 col-xs-6">
				<p>-</p>
				<p>-</p>
				<p>${shippingOrderJson.itSlineList[0].name1}
					${shippingOrderJson.itVbak[0].zzPlaceOfReceipt}</p>
				<p style="display: none">${shippingOrderJson.itVbak[0].bstnk}</p>
			</div>
		</div>
		<div class="row row-without-margin">
			<form:form action="shippingOrder" method="post" commandName="req"
				cssClass="form-incline" target="_blank">
				<div class="col-md-10 col-sm-12 col-xs-12">
					<div class='form-group' style="display: none">
						<form:input path="req1" id="number" cssClass="form-control"
							required="true" />
					</div>
				</div>
				<div class="col-md-2 col-sm-12 col-xs-12">
					<div class='form-group'>
						<label style="display: block; color: #f5f5f5">. </label>
						<!-- added for alignment purpose, dont delete -->
						<!-- <input type="submit" name="download" id="download"
							class="btn btn-primary form-control" value="Download" /> -->
						 <button type="button" class="btn btn-default no-print" style="background-color:#5cb85c;" id="print" onclick="printContent('shipping_order')">Print</button>
					</div>
				</div>
			</form:form>
		</div>
	</c:if>
</div>
</section>
<!-- jquery
		============================================ -->
<script src="js/vendor/jquery-1.11.3.min.js"></script>
<!-- bootstrap JS
		============================================ -->
<script src="js/bootstrap.min.js"></script>
<!-- plugins JS
		============================================ -->
<script src="js/plugins.js"></script>
<!-- main JS
		============================================ -->

<script src="js/canvasjsmin.js"></script>
<script src="js/main.js"></script>

<script>
	function printContent(el) {
		var restorepage = $('body').html();
		var printcontent = $('#' + el).clone();
		$('body').empty().html(printcontent);
		window.print();
		$('body').html(restorepage);
	}
</script>

<%@include file="footer_v2.jsp"%>
