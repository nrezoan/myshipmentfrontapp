<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Myshipment</title>
        <meta name="description" content="HTML5 template">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<!-- favicon
		============================================ -->		
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
		
		<!-- Google Fonts
		============================================ -->		
       <link href='https://fonts.googleapis.com/css?family=Lato:400,700,900,300' rel='stylesheet' type='text/css'>
	   
		<!-- Bootstrap CSS
		============================================ -->		
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
		<!-- Bootstrap CSS
		============================================ -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css">
		<!-- normalize CSS
        ============================================ -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/normalize.css">
        <!-- main CSS
        ============================================ -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
		<!-- style CSS
		============================================ -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
         <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/graph.css">
		<!-- modernizr JS
		============================================ -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/test.css">		
        <script src="${pageContext.request.contextPath}/resources/js/vendor/modernizr-2.8.3.min.js"></script>
		
    </head>
<body>

	<!-- Navigation -->
	<nav class="navbar navbar-inverse navbar-fixed-top header-area"
		role="navigation">
	<div class="container">
		<!-- Brand and toggle get grouped for better mobile display -->
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<spring:url value="/test"/>"> <img style="max-width:24%;margin-top: 4.5%;margin-left: -4%;" src="${pageContext.request.contextPath}/resources/images/logo.png" style="width: 70%;margin-top: 3%;"></a>
		</div>
		<!-- Collect the nav links, forms, and other content for toggling -->
		<div class="collapse navbar-collapse"
			id="bs-example-navbar-collapse-1">
			<ul class="nav navbar-nav navbar-right">
				<!-- <li><a href="about.html">About</a></li>  -->
				<!-- <li><a href="services.html">Services</a></li>  -->
				<li><a href="${pageContext.request.contextPath}/signin">Login   <i class="fa fa-user"></i></a></li>

			</ul>
		</div>
		<!-- /.navbar-collapse -->
	</div>
	<!-- /.container --> </nav>

	<!-- Header Carousel -->


	<!-- Page Content -->
	<div class="container">
		<br>
		<br> <br>
		<br>
		<!-- Marketing Icons Section -->
		<div class="row">
			<div class="col-md-3">
				<div class="panel panel-default"
					style="background-color: #555; margin-top: 10%;">

                    <div class="panel-body">
                    <div class="logo">Welcome</div>
                       <!-- Main Form -->
                        <div class="login-form-1">
                            <!-- <form id="login-form" class="text-left">   -->
                            <form:form id="login-form" class="text-left" action="${pageContext.request.contextPath}/selectedorg" modelAttribute="loginMdl" method="post">
                                <div class="login-form-main-message"></div>
                                <div class="main-login-form">
                                    <div class="login-group">
                                    <label for="username" class="errorMsg" style="color:red;">${error}</label>
                                        <div class="form-group">
                                            <!-- <label for="lg_username" class="sr-only">Username</label> -->
                                            <label for="lg_username" class="sr-only">Customer ID</label>
                                            <!--
                                            <input type="text" class="form-control" id="lg_username" name="lg_username" placeholder="username"> -->
                                            <input type="text" class="form-control" id="customerCode" name="customerCode" value="" required="required" title="Please enter your customer id" placeholder="Customer Id" maxlength="10">
                                        </div>
                                        <div class="form-group">
                                            <label for="lg_password" class="sr-only">Password</label>
                                            <!-- <input type="password" class="form-control" id="lg_password" name="lg_password" placeholder="password"> -->
                                            <input type="password" class="form-control" id="password" name="password" value="" required="required" title="Please enter your password" placeholder="Password">
                                        </div>                                  
                                        <div class="form-group login-group-checkbox" style="color: white;">
                                            <!-- <input type="checkbox" id="lg_remember" name="lg_remember">
                                            <label for="lg_remember">remember me -->
                                            <input type="checkbox" name="remember" id="remember" value="on"> Stay signed in    
                                            
                                        </div>

                                        

                                    </div>
                                    <!--<button type="submit" class="login-button"><i class="fa fa-chevron-right"></i></button>-->
                                </div>
                               <!-- <div class="etc-login-form">
                                    <p>forgot your password? <a href="#">click here</a></p>
                                    <!--<p>new user? <a href="#">create new account</a></p>-->
                               <!-- </div>-->
								<div class="row">
                            <div class="col-md-4"></div>
                            <div class="col-md-4"></div>
                            <div class="col-md-4">
                                <button type="submit" id="trackButton" name="trackButton" class="btn btn-primary">Login</button>
                            </div>
                          </div>
                            </form:form>
                        </div>
                        <!-- end:Main Form -->
                    </div>
				</div>
			</div>
			<div class="col-md-6">
                <!--<img src="img/track.jpg">-->
				<header id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1" class=""></li>
            <!--  <li data-target="#myCarousel" data-slide-to="2" class=""></li> -->
            <!-- <li data-target="#myCarousel" data-slide-to="3" class=""></li>  -->
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" style="margin-top: 5%;">
            <div class="item active">
                <div class="fill"><img style="height: 216px;" src="${pageContext.request.contextPath}/resources/images/track1new.png"></div>
                <div class="carousel-caption">
                    <!--<h2 style="color:#286090">Caption 1</h2>-->
                </div>
            </div>
            <!--
            <div class="item">
                <div class="fill"><img style="height: 216px;" src="${pageContext.request.contextPath}/resources/images/track2new.png"></div>
                <div class="carousel-caption">
                    <!--<h2 style="color:#286090">Caption 2</h2>--> <!-- 
                </div>
            </div>
            -->
            <div class="item">
                <div class="fill"><img style="height: 216px;" src="${pageContext.request.contextPath}/resources/images/track3new.png"></div>
                <div class="carousel-caption">
                    <!--<h2 style="color:#286090">Caption 3</h2>-->
                </div>
            </div>
            <!-- 
            <div class="item">
                <div class="fill"><img style="height: 216px;" src="${pageContext.request.contextPath}/resources/images/track4new.png"></div>
                <div class="carousel-caption">
                    <!--<h2 style="color:#286090">Caption 3</h2>--> <!--
                </div>
            </div>
             -->
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>
    </header>
            </div>
			<div class="col-md-3">
				<div class="panel panel-default"
					style="background-color: #555; margin-top: 10%; color: white">

					<div class="panel-body">
						<h4><i class="fa fa-fw fa-ship"></i> Track a Shipment</h4>

                        <!-- adding new form element -->
                        <form:form action="${pageContext.request.contextPath}/getCommonTrackingDetailInfo" method="post" commandName="trackingRequestParam" >

						<div class="form-group">
							<div class="row">
								<div class="col-md-12" style="margin-top: 8%;">
									<select class="form-control" name="action" id="action">
    									<option value="01">SEA EXPORT HBL</option>
									    <option value="02">SEA IMPORT HBL</option>
									    <option value="03">AIR EXPORT HAWB</option>
									    <option value="04">AIR IMPORT HAWB</option>
  									</select>			
									<input type="text" class="form-control"  title="Please enter your HBL Number" name="blNo" placeholder="HBL Number" required="true">
														
								</div>
								<!-- <strong></br> </strong> -->
								<div class="col-md-4"></div>
								<div class="col-md-4"></div>
								<div class="col-md-4">
									<button type="submit" id="trackButton" name="trackButton" class="btn btn-primary">Track
                                </button>
								</div>
								<!-- <strong></br> </strong> -->

							</div>
							
						</div>
						<br>
						<!-- Button -->
						<!-- 
						<div class="form-group">
							<div class="row">
								<div class="col-md-4"></div>
								<div class="col-md-4"></div>
								<div class="col-md-4">
									<button type="submit" id="trackButton" name="trackButton" class="btn btn-primary">Track
                                </button>
								</div>
							</div>
						</div> -->
						
						</form:form>

					</div>



				</div>
			</div>
			<div class="col-xs-12" style="height: 15px;"></div>
			
			<div class="col-md-6">
				<div class="panel panel-default">
					<div class="panel-body">
						<h4>
							<i class="fa fa-fw fa-check"></i> Quick Access
						</h4>
						<div class="list-group list-cust" style="margin-bottom: 5px;">
							<a href="${pageContext.request.contextPath}/orderheader" class="list-group-item active"> Ship Quick & Simple with an Account <i class="fa fa-fw fa-check-square" style="float: right;margin-top: 3px;"></i></a>
							<a href="${pageContext.request.contextPath}/getPoTrackingPage" class="list-group-item text-primary">Track PO <i class="fa fa-fw fa-map-marker" style="float: right;margin-top: 3px;"></i></a>
							<a href="${pageContext.request.contextPath}/getSeaBillOfLandingPage" class="list-group-item">Get Non-Nego B/L <i class="fa fa-fw fa-file" style="float: right;margin-top: 3px;"></i></a>
							<a href="${pageContext.request.contextPath}/getExceptionReportPage" class="list-group-item">Exception Report <i class="fa fa-fw fa-bar-chart" style="float: right;margin-top: 3px;"></i></a>

						</div>
					</div>
				</div>
			</div>
			<!--div class="col-md-3">
                
            </div-->
			
			<!-- 
			<div class="col-md-4">
				<div class="panel panel-default">

					<div class="panel-body">
						<h4>
							<i class="fa fa-fw fa-compass"></i> Get a Quick Rate</h4>
						<div class="form-group">
							<div class="row">
								<div class="col-md-12">
									<label>From</label>
									<input type="text" class="form-control" title="Enter a location" name="from" placeholder="Enter a location" required="true">
									<label>To</label>
									<input type="text" class="form-control" title="Enter a location" name="to" placeholder="Enter a location" required="true">
								</div>

							</div>
						</div>
						<br>
						
						<div class="form-group">
							<div class="row">
								<div class="col-md-4"></div>
								<div class="col-md-4"></div>
								<div class="col-md-4">
									<button type="submit" id="trackButton" name="trackButton"
										class="btn btn-primary">Submit</button>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
			-->
			
			
			<div class="col-md-6">
				<div class="panel panel-default">

					<div class="panel-body">
						<h4>
							<i class="glyphicon glyphicon-book"></i> News
						</h4>

						<div class="list-group list-cust">
							<a href="#" class="list-group-item"> <strong>MyShipment launches a brand new version</strong> </a>
							<a href="#" class="list-group-item text-primary">Bangladesh to benefit from $360m logistics investment</a>
							<a href="#" class="list-group-item">Bangladesh leaps 21 places in World Bank's Logistics Performance Index</a>							

						</div>

						<!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit.
							Itaque, optio corporis quae nulla aspernatur in alias at numquam
							rerum ea excepturi expedita tenetur assumenda voluptatibus
							eveniet incidunt dicta nostrum quod?</p>
							-->
						<!-- <a href="#" class="btn btn-default" style="margin-top: 10px;">Learn More</a>  -->
					</div>
				</div>
			</div>
		</div>
		<!-- /.row -->


		<!-- Call to Action Section -->



	</div>
	<!-- Footer -->
	<section class="footer">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
				<p class="footer-para">Copyright &copy MyShipment.com</p>

			</div>
			<!-- 
			<div class="col-lg-3 col-md-3 footer-img">
				<img src="img/logo.png" alt="" class="" />
			</div>
			<div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 footer_nav">
				<ul class="table-h-nav-footer">
					<li><a href="">Booking</a></li>
					<li><a href="">booking template</a></li>
					<li><a href="">Mis</a></li>
					<li><a href="">frequently used form</a></li>
					<li><a href="">other reports</a></li>

					<li><a href="">Update</a></li>
				</ul>
			</div> -->
		</div>
	</div>
	</section>


    <!-- /.container -->

   <!-- jquery
		============================================ -->		
        <script src="/MyShipmentFrontApp/resources/js/vendor/jquery-1.11.3.min.js"></script>
		<!-- bootstrap JS
		============================================ -->		
        <script src="/MyShipmentFrontApp/resources/js/bootstrap.min.js"></script>
		<!-- plugins JS
		============================================ -->		
        <script src="/MyShipmentFrontApp/resources/js/plugins.js"></script>
		<!-- main JS

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 2500 //changes the speed
    });
    </script>

</body>

</html>