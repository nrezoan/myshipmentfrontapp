<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script>
	$(document)
			.ready(
					function() {
						var status = "${seaExportHBLData.myshipmentTrackHeader[0].status}";
						var mawbIssueDate = "${seaExportHBLData.myshipmentTrackHeader[0].bl_bt}}";
						var statusDate = "${seaExportHBLData.myshipmentTrackHeader[0].eta_dt}";

						var year = statusDate.substring(0, 4);
						var month = statusDate.substring(4, 6);
						var date = statusDate.substring(6);

						var totalDate = year + "-" + month + "-" + date;

						var today = new Date();
						var eta = new Date(totalDate);

						if (eta < today) {
							console.log("in comparison");
							document.getElementById("order-booked").className = "active";
							document.getElementById("gr-done").className = "active";
							document.getElementById("stuffing-done").className = "active";
							document.getElementById("order-shipped").className = "active";
							document.getElementById("arrival").className = "active";
						}

						if (status === "Order Booked") {
							document.getElementById("order-booked").className = "active";
						} else if (status === "GR Done") {
							document.getElementById("order-booked").className = "active";
							document.getElementById("gr-done").className = "active";
						} else if (status === "Shipment Done") {
							document.getElementById("order-booked").className = "active";
							document.getElementById("gr-done").className = "active";
							document.getElementById("stuffing-done").className = "active";
							document.getElementById("order-shipped").className = "active";
						} else if (status === "Stuffing Done") {
							document.getElementById("order-booked").className = "active";
							document.getElementById("gr-done").className = "active";
							document.getElementById("stuffing-done").className = "active";
						}
						$('.collapse')
								.on(
										'shown.bs.collapse',
										function() {
											$(this)
													.parent()
													.find(
															".glyphicon-plus-sign")
													.removeClass(
															"glyphicon-plus-sign")
													.addClass(
															"glyphicon-minus-sign");
										})
								.on(
										'hidden.bs.collapse',
										function() {
											$(this)
													.parent()
													.find(
															".glyphicon-minus-sign")
													.removeClass(
															"glyphicon-minus-sign")
													.addClass(
															"glyphicon-plus-sign");
										});
					});
</script>
<style>
.table>thead>tr>td, .table>tbody>tr>td, .table>thead>tr>th, .table>tbody>tr>th
	{
	border: none;
}
/* 	.panel {
    	border-color: #FFF;
    } */
html {
	height: 100% !important;
	background-color: #FFF;
}

h4 {
	padding: 15px;
	font-size: 14px;
}

.container {
	width: 1325px !important;
}
</style>

<!-- Theme style -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/newtimeline.css" />

<section>
	<div class="container-fluid">
		<div class="page-header"
			style="margin: 10px 0 10px; padding-bottom: 0px; border-bottom: 0px;">
			<!-- <h1>Sea Export HBL Tracking Details</h1> -->
			<div class="row">
				<div class="col-md-12">
					<div class="col-md-offset-1 col-md-5">
						<h2>Sea Export HBL Tracking Details</h2>
					</div>
					<%-- <div class="col-md-4 sea-export-3">
								<h4 style="float: right;">${seaExportHBLData.myshipmentTrackHeader[0].sales_org_desc}</h4>
							</div>
							<div class="col-md-2 ">
								<h4>
									<img
										src="${pageContext.request.contextPath}/resources/images/${seaExportHBLData.myshipmentTrackHeader[0].comp_code}.jpg"
										alt="Company Logo" style="width: 90px;" />
								</h4>
							</div> --%>

				</div>
			</div>
		</div>
	</div>
</section>

<section class="milestone-track-section-254196">
	<div class="container">
		<div class="panel-group">
			<div class="panel panel-default"
				style="border-bottom: 3px solid #007AA7; border-bottom: 2px solid #007AA7; margin-bottom: 0px; border-radius: 0px;">
				<div class="panel-heading-217226">
					<div class="row">
						<div class="col-md-12" style="text-align: center;">
							<div class="col-md-4">
								<div style="padding: 5px 0px 5px 0px;">HBL NO. :
									${seaExportHBLData.myshipmentTrackHeader[0].bl_no }</div>
							</div>
							<div class="col-md-4">
								<%-- <div style="padding: 5px 0px 5px 0px;">STATUS :
											${seaExportHBLData.myshipmentTrackHeader[0].status}</div> --%>
							</div>
							<div class="col-md-4">
								<div style="padding: 5px 0px 5px 0px;">
									ETA :
									<c:choose>
										<c:when
											test="${empty seaExportHBLData.myshipmentTrackHeader[0].eta_dt}">
        			N/A
    			</c:when>
										<c:otherwise>
											<fmt:parseDate
												value="${seaExportHBLData.myshipmentTrackHeader[0].eta_dt}"
												var="eta_date" pattern="yyyyMMdd" />
											<fmt:formatDate pattern="dd-MM-yyyy" type="date"
												value="${eta_date}" />
										</c:otherwise>
									</c:choose>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- <div class="panel-heading" style="background-color: #00ADF1; color: #FFF;margin-bottom: 0px;border-radius: 0px;">						
					</div> -->
				<div class="panel-body">
					<ul class="progressbar customUl">
						<li id="order-booked">
							<div class="timestamp-194858">
								<span> <c:choose>
										<c:when
											test="${empty seaExportHBLData.myshipmentTrackHeader[0].bl_bt}">
        											N/A
    									</c:when>
										<c:otherwise>
											<fmt:parseDate
												value="${seaExportHBLData.myshipmentTrackHeader[0].bl_bt}"
												var="bl_date" pattern="yyyyMMdd" />
											<fmt:formatDate pattern="dd-MM-yyyy" type="date"
												value="${bl_date}" />
										</c:otherwise>
									</c:choose>
								</span>
							</div>
							<div class="status-662147">
								<!-- Prints the order details bl_no, tot_pcs (Total pieces), por (port name) if status matches-->
								<span> <c:choose>
										<c:when
											test="${seaExportHBLData.myshipmentTrackHeader[0].status == 'Order Booked' || !empty seaExportHBLData.myshipmentTrackHeader[0].bl_bt}">
											<h4>Order
												${seaExportHBLData.myshipmentTrackHeader[0].bl_no } Booked</h4>
										</c:when>
										<c:otherwise>
											<h4>Order
												${seaExportHBLData.myshipmentTrackHeader[0].bl_no } Not
												Booked yet.</h4>
										</c:otherwise>
									</c:choose>
								</span>
								<!-- <h4>Order ${seaExportHBLData.myshipmentTrackHeader[0].bl_no } Booked</h4> -->
							</div>
						</li>
						<li id="gr-done">
							<div class="timestamp-194858">
								<span> <c:choose>
										<c:when
											test="${empty seaExportHBLData.myshipmentTrackHeader[0].gr_dt}">
        									N/A
    									</c:when>
										<c:otherwise>
											<fmt:parseDate
												value="${seaExportHBLData.myshipmentTrackHeader[0].gr_dt}"
												var="gr_date" pattern="yyyyMMdd" />
											<fmt:formatDate pattern="dd-MM-yyyy" type="date"
												value="${gr_date}" />
										</c:otherwise>
									</c:choose>
								</span>
							</div>
							<div class="status-662147">
								<!-- Prints the order details bl_no, tot_pcs (Total pieces), por (port name) if status matches-->
								<span> <c:choose>
										<c:when
											test="${!empty seaExportHBLData.myshipmentTrackHeader[0].gr_dt || seaExportHBLData.myshipmentTrackHeader[0].status =='GR Done'}">
											<h4>
												Order ${seaExportHBLData.myshipmentTrackHeader[0].bl_no}, <strong>
													${seaExportHBLData.myshipmentTrackHeader[0].tot_pcs}</strong> pieces
												Received at ${seaExportHBLData.myshipmentTrackHeader[0].por}
											</h4>
										</c:when>
										<c:otherwise>
											<h4>
												Order ${seaExportHBLData.myshipmentTrackHeader[0].bl_no}, <strong>
													0</strong> pieces Received at
												${seaExportHBLData.myshipmentTrackHeader[0].por}
											</h4>
										</c:otherwise>
									</c:choose>
								</span>

							</div>
						</li>
						<li id="stuffing-done">
							<div class="timestamp-194858">
								<span> <c:choose>
										<c:when
											test="${empty seaExportHBLData.myshipmentTrackHeader[0].stuff_dt}">
        									N/A
    									</c:when>
										<c:otherwise>
											<fmt:parseDate
												value="${seaExportHBLData.myshipmentTrackHeader[0].stuff_dt}"
												var="stuff_date" pattern="yyyyMMdd" />
											<fmt:formatDate pattern="dd-MM-yyyy" type="date"
												value="${stuff_date}" />
										</c:otherwise>
									</c:choose>
								</span>
							</div>
							<div class="status-662147">
								<!-- Prints the order details bl_no, tot_pcs (Total pieces), por (port name) if status matches-->
								<span> <c:choose>
										<c:when
											test="${!empty seaExportHBLData.myshipmentTrackHeader[0].stuff_dt || seaExportHBLData.myshipmentTrackHeader[0].status =='Stuffing Done'}">
											<h4>
												Order ${seaExportHBLData.myshipmentTrackHeader[0].bl_no}, <strong>
													${seaExportHBLData.myshipmentTrackHeader[0].tot_pcs}</strong> pieces
												Stuffed in container
											</h4>
										</c:when>
										<c:otherwise>
											<h4>
												Order ${seaExportHBLData.myshipmentTrackHeader[0].bl_no}, <strong>
													0</strong> pieces Stuffed in container
											</h4>
										</c:otherwise>
									</c:choose>
								</span>

							</div>
						</li>

						<li id="order-shipped">
							<div class="timestamp-194858">
								<span> <c:choose>
										<c:when
											test="${empty seaExportHBLData.myshipmentTrackHeader[0].dep_dt}">
        									N/A
    									</c:when>
										<c:otherwise>
											<fmt:parseDate
												value="${seaExportHBLData.myshipmentTrackHeader[0].dep_dt}"
												var="stuff_date" pattern="yyyyMMdd" />
											<fmt:formatDate pattern="dd-MM-yyyy" type="date"
												value="${stuff_date}" />
										</c:otherwise>
									</c:choose>
								</span>
							</div>
							<div class="status-662147">
								<!-- Prints the order details bl_no, tot_pcs (Total pieces), por (port name) if status matches-->
								<span> <c:choose>
										<c:when
											test="${!empty seaExportHBLData.myshipmentTrackHeader[0].dep_dt || seaExportHBLData.myshipmentTrackHeader[0].status == 'Shipment Done'}">
											<h4>Order
												${seaExportHBLData.myshipmentTrackHeader[0].bl_no} Shipped</h4>
										</c:when>
										<c:otherwise>
											<h4>Order
												${seaExportHBLData.myshipmentTrackHeader[0].bl_no} Waiting
												to be Shipped</h4>
										</c:otherwise>
									</c:choose>
								</span>

							</div>
						</li>

						<li id="arrival">
							<div class="timestamp-194858">
								<span> <c:choose>
										<c:when
											test="${empty seaExportHBLData.myshipmentTrackHeader[0].eta_dt}">
        									N/A
    									</c:when>
										<c:otherwise>
											<fmt:parseDate
												value="${seaExportHBLData.myshipmentTrackHeader[0].eta_dt}"
												var="stuff_date" pattern="yyyyMMdd" />
											<fmt:formatDate pattern="dd-MM-yyyy" type="date"
												value="${stuff_date}" />
										</c:otherwise>
									</c:choose>
								</span>
							</div>
							<div class="status-662147">
								<!-- Prints the order details bl_no, tot_pcs (Total pieces), por (port name) if status matches-->
								<c:choose>
									<c:when
										test="${!empty seaExportHBLData.myshipmentTrackHeader[0].eta_dt }">
										<h4>
											Order Arrived
											${seaExportHBLData.myshipmentTrackHeader[0].bl_no}, <strong>
												${seaExportHBLData.myshipmentTrackHeader[0].tot_pcs}</strong> pieces
											at Port of Discharge
										</h4>
									</c:when>
									<c:otherwise>
										<h4>
											Order Arrived
											${seaExportHBLData.myshipmentTrackHeader[0].bl_no}, <strong>
												1000</strong> pieces at Port of Discharge
										</h4>
									</c:otherwise>
								</c:choose>
							</div>
						</li>
						<li id="delivered">
							<div class="timestamp-194858">
								<span> <%-- <c:choose>
												<c:when
													test="${empty seaExportHBLData.myshipmentTrackHeader[0].stuff_dt}">
        											N/A
    											</c:when>
												<c:otherwise>
													<fmt:parseDate
														value="${seaExportHBLData.myshipmentTrackHeader[0].stuff_dt}"
														var="stuff_date" pattern="yyyyMMdd" />
													<fmt:formatDate pattern="dd-MM-yyyy" type="date"
														value="${stuff_date}" />
												</c:otherwise>
											</c:choose> --%> N/A
								</span>
							</div>
							<div class="status-662147">
								<h4>
									Order ${seaExportHBLData.myshipmentTrackHeader[0].bl_no}, <strong>
										0</strong> Arrived at place of delivery
								</h4>
								<!-- Prints the order details bl_no, tot_pcs (Total pieces) if status matches-->
								<!--  <span><c:choose>
									<c:when
										test="${seaExportHBLData.myshipmentTrackHeader[0].status == 'Order Booked' || seaExportHBLData.myshipmentTrackHeader[0].status =='GR Done' || seaExportHBLData.myshipmentTrackHeader[0].status =='Shipment Done'}">
										<h4>
											Order ${seaExportHBLData.myshipmentTrackHeader[0].bl_no}, <strong>
												${seaExportHBLData.myshipmentTrackHeader[0].tot_pcs}</strong> Arrived
											at place of delivery
										</h4>
									</c:when>
									<c:otherwise>
										<h4>Order ${seaExportHBLData.myshipmentTrackHeader[0].bl_no}, <strong>
												0</strong> Arrived
											at place of delivery</h4>
									</c:otherwise>
								</c:choose>
								</span>-->
							</div>
						</li>

					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- hamid collapse toggle -->
<div class="container">
	<div class="panel-group" id="accordion">
		<div class="panel">
			<div class="panel-heading">
				<h4 class="panel-title">
					<a class="" data-toggle="collapse" data-parent="#accordion"
						href="#collapseOne"> <span>CONSIGNMENT DETAILS</span> <!-- <span class="glyphicon glyphicon-minus-sign" style="float: right;"></span> -->
					</a>
				</h4>
			</div>
			<div id="collapseOne" class="panel-collapse collapse in">
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-hover table-inverse">

							<tbody>
								<tr>
									<th scope="row">HBL No.</th>
									<td>${seaExportHBLData.myshipmentTrackHeader[0].bl_no }</td>
									<th scope="row">Commercial Invoice No.</th>
									<td>${seaExportHBLData.myshipmentTrackHeader[0].comm_inv }</td>
								</tr>
								<tr>
									<th scope="row">HBL Date</th>
									<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${seaExportHBLData.myshipmentTrackHeader[0].bl_bt_openTracking }" /></td> --%>
									<%-- <td>${seaExportHBLData.myshipmentTrackHeader[0].bl_bt }</td>  --%>
									<fmt:parseDate
										value="${seaExportHBLData.myshipmentTrackHeader[0].bl_bt}"
										var="bl_date" pattern="yyyyMMdd" />
									<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
											value="${bl_date}" /></td>

									<th scope="row">Commercial Invoice Date</th>
									<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${seaExportHBLData.myshipmentTrackHeader[0].comm_inv_dt_openTracking }" /></td> --%>

									<%-- <td>${seaExportHBLData.myshipmentTrackHeader[0].comm_inv_dt }</td> --%>
									<fmt:parseDate
										value="${seaExportHBLData.myshipmentTrackHeader[0].comm_inv_dt}"
										var="comm_inv_date" pattern="yyyyMMdd" />
									<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
											value="${comm_inv_date}" /></td>
								</tr>
								<tr>
									<th scope="row">Shipper Name</th>
									<td>${seaExportHBLData.myshipmentTrackHeader[0].shipper }</td>
									<th scope="row">Place of Receipt</th>
									<td>${seaExportHBLData.myshipmentTrackHeader[0].por }</td>
								</tr>
								<tr>
									<th scope="row">Consignee Name</th>
									<td>${seaExportHBLData.myshipmentTrackHeader[0].buyer }</td>
									<th scope="row">Cargo Received Date</th>
									<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${seaExportHBLData.myshipmentTrackHeader[0].gr_dt_openTracking }" /></td> --%>
									<%-- <td>${seaExportHBLData.myshipmentTrackHeader[0].gr_dt }</td> --%>
									<fmt:parseDate
										value="${seaExportHBLData.myshipmentTrackHeader[0].gr_dt}"
										var="gr_date" pattern="yyyyMMdd" />
									<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
											value="${gr_date}" /></td>
								</tr>
								<tr>
									<th scope="row">Port Of Loading</th>
									<td>${seaExportHBLData.myshipmentTrackHeader[0].pol }</td>
									<th scope="row">MBL Number</th>
									<td>${seaExportHBLData.myshipmentTrackHeader[0].mbl_no }</td>
								</tr>
								<tr>
									<th scope="row">Port of Discharge</th>
									<td>${seaExportHBLData.myshipmentTrackHeader[0].pod }</td>
									<th scope="row">Shipping Line</th>
									<td>${seaExportHBLData.myshipmentTrackHeader[0].carrier }</td>
								</tr>
								<tr>
									<th scope="row">Place of Discharge</th>
									<td>${seaExportHBLData.myshipmentTrackHeader[0].plod }</td>
									<th scope="row">Stuffing Date</th>
									<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${seaExportHBLData.myshipmentTrackHeader[0].stuff_dt_openTracking }" /></td> --%>

									<%-- <td>${seaExportHBLData.myshipmentTrackHeader[0].stuff_dt }</td> --%>
									<fmt:parseDate
										value="${seaExportHBLData.myshipmentTrackHeader[0].stuff_dt}"
										var="stuff_date" pattern="yyyyMMdd" />
									<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
											value="${stuff_date}" /></td>
								</tr>
								<tr>
									<th scope="row">TOS</th>
									<td>${seaExportHBLData.myshipmentTrackHeader[0].frt_mode_ds }</td>
									<th scope="row">Shipped on Board</th>
									<%-- <td>${seaExportHBLData.myshipmentTrackHeader[0].dep_dt_openTracking }</td> --%>
									<%-- <td>${seaExportHBLData.myshipmentTrackHeader[0].dep_dt }</td> --%>
									<fmt:parseDate
										value="${seaExportHBLData.myshipmentTrackHeader[0].load_dt}"
										var="dep_date" pattern="yyyyMMdd" />
									<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
											value="${dep_date}" /></td>
								</tr>
								<tr>
									<th scope="row">Total Quantity</th>
									<%-- <td>${seaExportHBLData.myshipmentTrackHeader[0].tot_qty}</td> --%>
									<fmt:parseNumber var="totqty" integerOnly="true" type="number"
										value="${seaExportHBLData.myshipmentTrackHeader[0].tot_qty}" />
									<td>${totqty}</td>

									<th scope="row">ETD</th>
									<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${seaExportHBLData.myshipmentTrackHeader[0].dep_dt_openTracking }" /></td> --%>

									<%-- <td>${seaExportHBLData.myshipmentTrackHeader[0].dep_dt }</td> --%>
									<fmt:parseDate
										value="${seaExportHBLData.myshipmentTrackHeader[0].dep_dt}"
										var="load_date" pattern="yyyyMMdd" />
									<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
											value="${load_date}" /></td>
								</tr>
								<tr>
									<th scope="row">Total Volume (CBM)</th>
									<td>${seaExportHBLData.myshipmentTrackHeader[0].volume }</td>
									<th scope="row">ETA</th>
									<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${seaExportHBLData.myshipmentTrackHeader[0].eta_dt_openTracking }" /></td> --%>
									<%-- <td>${seaExportHBLData.myshipmentTrackHeader[0].eta_dt }</td> --%>
									<fmt:parseDate
										value="${seaExportHBLData.myshipmentTrackHeader[0].eta_dt}"
										var="eta_date" pattern="yyyyMMdd" />
									<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
											value="${eta_date}" /></td>
								</tr>
								<tr>
									<th scope="row">Total Gross Weight</th>
									<td>${seaExportHBLData.myshipmentTrackHeader[0].grs_wt }</td>
									<th scope="row">Destination Agent</th>
									<td>${seaExportHBLData.myshipmentTrackHeader[0].agent }</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="panel">
			<div class="panel-heading">
				<h4 class="panel-title">
					<a class="collapsed" data-toggle="collapse"
						data-parent="#accordion" href="#collapseTwo"> <span>ITEM
							DETAILS</span> <!-- <span class="glyphicon glyphicon-plus-sign" style="float: right;"></span> -->
					</a>
				</h4>
			</div>
			<div id="collapseTwo" class="panel-collapse collapse">
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-hover">

							<thead>
								<tr>
									<th>Sl</th>
									<th>PO No.</th>
									<th>Commodity</th>
									<th>Style</th>
									<th>Size</th>
									<th>SKU</th>
									<th>Article No.</th>
									<th>Color</th>
									<th>Pieces</th>
									<th>Carton</th>
									<th>Vol.(CBM)</th>
									<th>Weight</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${seaExportHBLData.myshipmentTrackItem}"
									var="item" varStatus="loopCounter">
									<tr>
										<td scope="row">${loopCounter.count}</td>
										<td>${item.po_no}</td>
										<td>${item.commodity}</td>
										<td>${item.style}</td>
										<td>${item.size}</td>
										<td>${item.sku}</td>
										<td>${item.art_no}</td>
										<td>${item.color}</td>
										<%-- <td>${item.item_pcs}</td> --%>
										<fmt:parseNumber var="itmpcs" integerOnly="true" type="number"
											value="${item.item_pcs}" />
										<td>${itmpcs}</td>
										<%-- <td>${item.item_qty}</td> --%>
										<fmt:parseNumber var="itmqty" integerOnly="true" type="number"
											value="${item.item_qty}" />
										<td>${itmqty}</td>
										<td>${item.item_vol}</td>
										<td>${item.item_grwt}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="panel ">
			<div class="panel-heading">
				<h4 class="panel-title">
					<a class="collapsed" data-toggle="collapse"
						data-parent="#accordion" href="#collapseThree"> <span>VESSEL
							SCHEDULE</span> <!-- <span class="glyphicon glyphicon-plus-sign" style="float: right;"></span> -->

					</a>
				</h4>
			</div>
			<div id="collapseThree" class="panel-collapse collapse">
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-hover">

							<thead>
								<tr>
									<th>Leg</th>
									<th>Vessel Name</th>
									<!-- <th>Vessel Type</th> -->
									<th>Voyage</th>
									<th>POL</th>
									<th>POD</th>
									<th>ETD</th>
									<th>ETA</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${seaExportHBLData.myshipmentTrackSchedule}"
									var="vSchedule" varStatus="loopCounter">
									<tr>
										<td scope="row">${loopCounter.count}</td>
										<td>${vSchedule.car_name}</td>
										<%-- <td>${vSchedule.car_type}</td> --%>
										<td>${vSchedule.car_no}</td>
										<td>${vSchedule.pol}</td>
										<td>${vSchedule.pod}</td>
										<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${vSchedule.etd_openTracking}" /></td> --%>
										<%-- <td>${vSchedule.etd}</td> --%>
										<fmt:parseDate value="${vSchedule.etd}" var="etd_date"
											pattern="yyyyMMdd" />
										<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
												value="${etd_date}" /></td>
										<%-- <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${vSchedule.eta_openTracking}" /></td> --%>
										<%-- <td>${vSchedule.eta}</td> --%>
										<fmt:parseDate value="${vSchedule.eta}" var="eta_date"
											pattern="yyyyMMdd" />
										<td><fmt:formatDate pattern="dd-MM-yyyy" type="date"
												value="${eta_date}" /></td>
										<!--  <td>${vSchedule.etd}</td>
      <td>${vSchedule.eta}</td>-->
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="panel ">
			<div class="panel-heading">
				<h4 class="panel-title">
					<a class="collapsed" data-toggle="collapse"
						data-parent="#accordion" href="#collapseFour"> <span>CONTAINER
							DETAILS</span> <!-- <span class="glyphicon glyphicon-plus-sign" style="float: right;"></span> -->

					</a>
				</h4>
			</div>
			<div id="collapseFour" class="panel-collapse collapse">
				<div class="panel-body">
					<div class="table-responsive">
						<table class="table table-hover">

							<thead>
								<tr class="">
									<th>Sl</th>
									<th>Container No</th>
									<th>Seal No</th>
									<th>Container Size</th>
									<th>Mode</th>
									<th>PO No.</th>
									<th>Style</th>
									<th>Pckd Carton</th>
									<th>Packd Vol</th>
									<th>Packd Wt.</th>
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${seaExportHBLData.myshipmentTrackContainer}"
									var="container" varStatus="loopCounter">
									<tr>
										<td scope="row">${loopCounter.count}</td>
										<td>${container.cont_no}</td>
										<td>${container.seal_no}</td>
										<td>${container.cont_size}</td>
										<td>${container.cont_mode}</td>
										<td>${container.po_no}</td>
										<td>${container.style}</td>
										<%-- <td>${container.pacd_qty}</td> --%>
										<fmt:parseNumber var="pcdqty" integerOnly="true" type="number"
											value="${container.pacd_qty}" />
										<td>${pcdqty}</td>
										<td>${container.pacd_vol}</td>
										<td>${container.pacd_wt}</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- hamid edit ends -->


<%@include file="footer_v2.jsp"%>