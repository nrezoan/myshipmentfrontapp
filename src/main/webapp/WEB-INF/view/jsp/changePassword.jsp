<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<script type="text/javascript">
	
</script>

<!-- Theme style -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.css">
<!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">



<div class="entire-content-c">
	<!-- <br><br><br> -->

	<input id="dashboard_from_date" type="hidden"
		class="date-picker glyphicon glyphicon-calendar dashboard-ship-frm-date"
		style="width: 160px;" /> <input id="dashboard_to_date" type="hidden"
		class="date-picker glyphicon glyphicon-calendar dashboard-ship-to-date"
		style="width: 160px;" />

	<div class="content" style="background-color: #eaeaea; display: block;">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<!-- <h1>Change Password</h1> -->
			<!-- 			<ol class="breadcrumb" style="position: unset; float: none;">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li class="active">Dashboard</li>
			</ol> -->
		</section>
		<!-- Main content -->
		<section class="content">
			<!-- Info boxes -->
			<!-- /.row -->
			<!-- Main row -->
			<div class="row">
				<!-- Left col -->
				<div class="col-md-12">
					<!-- MAP & BOX PANE -->
					<div class="box box-myshipment">
						<div class="box-header with-border" style="text-align: center;">
							<h1 class="box-title" style="font-size: 26px; font-family: 'Quicksand', sans-serif; padding-bottom: 0px !important;"
								>Change Password
							</h1>
						</div>
						<!-- /.box-header -->
						<div class="box-body no-padding" style="display: block;">
							<div class="row">
								<div class="col-md-12 col-sm-12">
									<div class="pad">
										<form:form action="setNewPassword" method="post"
											commandName="editparam">
											<fieldset>
												<legend style="color: #00a65a; margin-top: 2px; /* font-size: 140%; */ text-align: center;border-bottom: 0px solid;">
														${message}
												</legend>
												<div class='row'>
													<div class="col-md-4"></div>
													<div class="col-md-4">
														<!-- 														<div class="col-md-12">
															<legend>Password Change</legend>
														</div> -->
														<div class='col-md-12'>
															<div class='form-group'>
																<label>Old Password</label>
																<form:input path="password" id="pass"
																	cssClass="form-control" required="required"
																	type="password" />
															</div>
														</div>
														<div class='col-md-12'>
															<div class='form-group'>
																<label>New Password</label>
																<form:input path="newPassword" id="pass1"
																	required="required" cssClass="form-control"
																	type="password" />
															</div>
														</div>
														<div class='col-md-12'>
															<div class='form-group'>
																<label>Verify Password</label>
																<form:input path="verifyPassword" id="pass2"
																	required="required" cssClass="form-control"
																	type="password" />
															</div>
														</div>
														<div class='col-md-12'>
															<div class='form-group'>
																<label style="display: block; color: #f5f5f5">.
																</label>
																<!-- added for alignment purpose, dont delete -->
																<input type="submit" id="btn-appr-po-search"
																	class="btn btn-success form-control" value="SUBMIT"
																	onclick="return Validate()" />
															</div>
														</div>
													</div>
													<div class="col-md-4"></div>
												</div>
											</fieldset>
										</form:form>
									</div>
								</div>
							</div>
							<!-- /.row -->
						</div>
						<!-- /.box-body -->
					</div>
				</div>
			</div>
		</section>
	</div>
</div>


<!-- ----------------------------- -->
<%@include file="footer_v2_fixed.jsp"%>


<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/passwordChange.js"></script>