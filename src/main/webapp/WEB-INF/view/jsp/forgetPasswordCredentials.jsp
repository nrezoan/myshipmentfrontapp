<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE-edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>myshipment</title>
    
    <!-- google fonts -->
    <link href="https://fonts.googleapis.com/css?family=Alegreya+Sans:300,400,700" rel="stylesheet">
    
    

    <!-- bootstrap CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/myshipment_v2/bootstrap/bootstrap.min.css">

    <!-- animate CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/myshipment_v2/animate/animate.css">

    <!-- style CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/myshipment_v2/styles.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/myshipment_v2/credentials.css">

    <!-- responsive CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/myshipment_v2/responsive.css">
    
    <link
	href="${pageContext.request.contextPath}/resources/myshipment_landing/css/footer-before-login.css"
	rel="stylesheet" type="text/css" media="all" />
</head>

<style>
	form .error {
		color: #ff0000;
		font-weight: unset;
		display: unset;
	}
</style>

<body>
    <div class="entire-content-c">
        <nav class="navbar navbar-fixed-top credentials-nav">
            <a class="navbar-brand" href="${pageContext.request.contextPath}/"><img src="${pageContext.request.contextPath}/resources/img/logo-xs.png" /></a>
            <div class="" id="navigation">
                <ul class="nav navbar-nav" id="top-nav-1">
                    <li>
                        <a href="${pageContext.request.contextPath}/"><img src="${pageContext.request.contextPath}/resources/img/home-1.png" title="Back to Home"></a>
                    </li>
                    <li></li>
                </ul>
            </div>
        </nav>
        <!--HEADER-->


        <!-- CREDENTIALS -->
        <section id="credential">
            <div class="container credential-content">
                <div class="row">
                    <div class="col-md-12">
                        <h2>credentials</h2>
                        <div class="content-title-underline-body"></div>
                        <h5>Forgot Username/Password? <br> <br>Fill-up the details below to receive the credentials on your registered email-id</h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-4"></div>
                        <div class="col-md-4">
                            <form:form action="${pageContext.request.contextPath}/recoverPassword" name="forgetPassword" method="post" commandName="editparam">
								<legend style="color: green; margin-top: 2px;font-size: 140%; text-align:center">${message}</legend>
                                <fieldset class="form-group">
                                    <form:input path="customerNo" type="text" cssClass="form-control form-control-sm myName" id="customerId" name="customerId" required="required" placeholder="Customer ID..." />
                                </fieldset>
                                <fieldset class="form-group">
                                    <form:input path="verifyMail" type="email" cssClass="form-control form-control-sm myEmail" id="email" name="email" required="required" placeholder="E-mail..." />
                                </fieldset>
                                <button type="submit" class="btn btn-info btn-block">Submit</button>
                            </form:form>
                        </div>
                        <div class="col-md-4"></div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END CREDENTIALS -->
		<div id="footer">
			<div class="copy">
				<p>
					&copy; <script type="text/javascript">
	                                var today = new Date()
	                                var year = today.getFullYear()
	                                document.write(year)
	
	                            </script> my<span class="shipment-footer-panel">shipment</span> | All
					Rights Reserved
				</p>
			</div>
		</div>

         <!-- FOOTER -->
<%--         <section id="footer" style="background-color: #252525; display: none;background: url('${pageContext.request.contextPath}/resources/img/topheaderBG.jpg') center center no-repeat;">
            <div class="container wow animated fadeInUp" data-wow-delay="0.4s" data-wow-duration="1.5s">
                <div class="row footer-top">
                    <div class="col-sm-4 footer-follow">
                        <h5>Follow Us :</h5>
                        <ul class="list-inline">
                            <li class="list-inline-item"><a href="https://www.facebook.com/mghgroupglobal" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <li class="list-inline-item"><a href="https://www.linkedin.com/company/mghgroup" target="_blank"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                    <div class="col-sm-3 footer-copy">
                        <p class="">
                            &copy; myshipment.com
                            <script type="text/javascript">
                                var today = new Date()
                                var year = today.getFullYear()
                                document.write(year)

                            </script>
                        </p>
                    </div>
                    <div class="col-sm-5 text-xs-center footer-app">
                        <a href="https://itunes.apple.com/za/app/myshipment-supplier/id1168561725?mt=8" target="_blank"><img src="${pageContext.request.contextPath}/resources/img/Android-App-Store-AF.png" class="img-fluid" title="Get On App Store"></a>
                        <a href="https://play.google.com/store/apps/details?id=com.mgh.shipment" target="_blank"><img src="${pageContext.request.contextPath}/resources/img/Android-App-Store-GF.png" class="img-fluid" title="Get On Google Play"></a>
                    </div>
                </div>
            </div>
        </section> --%>

        <!-- END FOOTER -->

        <!-- jQuery -->
        <script src="${pageContext.request.contextPath}/resources/js/myshipment_v2/jquery.js"></script>
        
        <!-- jQuery Validation -->
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js">a</script>

        <!-- bootstrap JS -->
        <script src="${pageContext.request.contextPath}/resources/js/myshipment_v2/bootstrap.min.js"></script>
     
        <script src="${pageContext.request.contextPath}/resources/js/myshipment_v2/wow.min.js"></script>
        
        <!--fontawesome script-->
        <script src="https://use.fontawesome.com/fcba9d8315.js"></script>
        
        <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/forgetPasswordCredential.js"></script>
        
        <script>
            $(document).ready(function() {
                $(".btn").click(function() {
                    var xxx = $(".myName").val();
                    var xxy = $(".myEmail").val();
                    if (xxx == '' || xxy == '') {
                        //alert('Input cannot be left blank');
                    } else {
                        //alert("An email has been sent to you with your new account information");
                    }
                });
            });

        </script>
    </div>
</body>
</html>
