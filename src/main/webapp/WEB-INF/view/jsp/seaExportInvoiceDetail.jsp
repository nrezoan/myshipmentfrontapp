<%@include file="header.jsp" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<section class="main-body">
		
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
    	<h3 class="ship_desti">${sexInvoiceJsonData.outputName}</h3>
       		<div class="pull-right"><input type="button" class="btn btn-info" 
       			style="margin-top: 0%; font-weight: bolder; border: 1px solid; padding: 11px 31px;
    					border-radius: 2px solid; border-radius: 6px; color: black; background-color: white;" 
    					value="Print Invoice" onclick="printContent('print-con')" /></div>
    </div>
  </div><!--/row-->
  <hr>
  
  
  <div id="print-con">
<!--   <div class="row nopadding"> -->
  <div class="row">
	  <div class="col-xs-12">
	  <div class="col-xs-6">
	  <h4>TO</h4>
	  <h5>${sexInvoiceJsonData.payer.name1}(${sexInvoiceJsonData.payerNo}/${sexInvoiceJsonData.companyCode})</h5>
	  <h6>${sexInvoiceJsonData.payer.name2},${sexInvoiceJsonData.payer.name3}</h6>
	  <h6>${sexInvoiceJsonData.payer.city1}</h6>
	  <h6>${sexInvoiceJsonData.payerCountry}</h6>
	  
	  
	  </div>
	  <div class="col-xs-3">
	  <img src="${pageContext.request.contextPath}/resources/images/${sexInvoiceJsonData.vkOrg}.jpg" alt="" class=""/>
	  
	  </div>
	  <div class="col-xs-3 pull-right-temp">
	  <h4>Invoice No:<strong> ${sexInvoiceJsonData.billNo}</strong></h4>
	  <h6>Date: <strong><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${sexInvoiceJsonData.billDt}" /> </strong></h6>
	  <h6>So No:<strong> ${sexInvoiceJsonData.soNo}</strong></h6>
	  <h6>DOC. Currency:<strong> ${sexInvoiceJsonData.currency}</strong></h6>
	  </div>
	  </div>
  </div>
	</br>
	<div class="row">
	<div class="col-xs-12">
		
			<div class="col-xs-3  shipper_info">
				<h6>Shipper:<strong> ${sexInvoiceJsonData.shipper}</strong></h6>
				<h6>Consignee:<strong> ${sexInvoiceJsonData.consignee}</strong></h6>
				<h6>Destination Agent:<strong> ${sexInvoiceJsonData.destAgent}</strong></h6>
				<h6>Payment Mode:<strong> ${sexInvoiceJsonData.paymentMode}</strong></h6>
				
			</div>
			<div class="col-xs-3  shipper_info">
				<h6>Commercial Invoice No.: <strong> ${sexInvoiceJsonData.commInv}</strong></h6>
				<h6>Total Qty: <strong>${sexInvoiceJsonData.totalQty}</strong></h6>
				<h6>Volume: <strong>${sexInvoiceJsonData.volume}</strong></h6>
				<h6>Ch.Weight: <strong>${sexInvoiceJsonData.chWt}</strong></h6>
				
			</div>
			<div class="col-xs-3">
				<h6>HBL: <strong> ${sexInvoiceJsonData.hbl}</strong></h6>
				<h6>MBL: <strong> ${sexInvoiceJsonData.mbl}</strong></h6>
				<h6>Destination: <strong> ${sexInvoiceJsonData.detin}</strong></h6>
				<h6>F.Vsl/Voyage <strong> ${sexInvoiceJsonData.fVsl}/${sexInvoiceJsonData.fVoyage}</strong></h6>
			</div>
			<div class="col-xs-3  shipper_info">
				<h6>Commercial Invoice Date: <strong> <fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${sexInvoiceJsonData.commDat}" /></strong></h6>
				<h6>M.Vsl/Voyage: <strong> ${sexInvoiceJsonData.mVsl}/${sexInvoiceJsonData.mVoyage}</strong></h6>
				<h6>ETA Destination: <strong> <fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${sexInvoiceJsonData.eta}" /></strong></h6>
				<h6>Shipping Line: <strong> ${sexInvoiceJsonData.shippingLine}</strong></h6>
			</div>
			
		</div>
	</div>
	
  
<!-- row nopadding -->
<!--   </div>  -->
  
<!-- hamid -->  
<!-- <div class="row">
	 <div class="col-xs-12"> -->
  
<!-- <div class="row table_wraper"> -->
 <c:if test="${fn:toUpperCase(sexInvoiceJsonData.country)!='INDIA'}">
  
	<div class="table-responsive">          
  <table class="table">
    <thead>
      <tr>
        <th>Description</th>
        <th>Rate</th>
        <th>Conversion</th>
        <th>UOM</th>
        <th>Total</th>
       
      </tr>
    </thead>
    <tbody>
    
    <c:forEach items="${sexInvoiceJsonData.tHeaderList}" var="item">
      <tr>
       <td>${item.vText}</td>
        <td>${item.kbetr}  ${item.waers}</td>
        <td>${item.conv}</td>
        <td>${item.tdText}</td>
        <td>${item.kwert}</td>
       
      </tr>
	  </c:forEach>
	  <c:forEach items="${sexInvoiceJsonData.tConList}" var="con">
		 <tr>
       		<td colspan="5">${con.vhilm}-${con.wgbez60}</td>
         </tr>
      <c:forEach items="${sexInvoiceJsonData.tPerconList}" var="perCon">
      	<c:if test="${con.vhilm == perCon.vhilm}">
      		<c:if test="${con.kStat!='X'}">
      			<tr>
       				<td>${perCon.vText}</td>
        			<td>${perCon.kbetr}  ${perCon.waers}</td>
        			<td>${perCon.conv}</td>
        			<td>${perCon.tdText}</td>
        			<td>${perCon.kwert}</td>
       
      			</tr>
      		</c:if>
      	</c:if>
      </c:forEach>
      </c:forEach>
	  <tr>
	   <td colspan="3">
	  
	  </td>
	  <td>
	  <h5>Rounding : </h5>
	  </td>
	   <td>
	  ${sexInvoiceJsonData.roundOff}
	  </td>
	  </tr>
	  
	  <tr>
	   <td colspan="3">
	  
	  </td>
	  <td>
	  <h4>Total Amount :</h4>
	  </td>
	   <td>
	  ${sexInvoiceJsonData.total}
	  </td>
	  </tr>
	  
    </tbody>
  </table>
  </div> <!-- table-responsive -->
  <div class="col-xs-12">
  <h5>Amount in words: <strong> ${sexInvoiceJsonData.words}</strong></h5>
  <h6>Note:<strong> </strong></h6>
  <h6>Prepared By:<strong> ${sexInvoiceJsonData.user}</strong></h6>
  <!--  <h6>Name:<strong> TANZIR AHMED</strong></h6>-->
  </div>  
  
	<%-- <div class="col-xs-12">
		<p style="text-align:right; padding-top:4%; font-weight:bold;">Payable To:${sexInvoiceJsonData.sOrg}</p>
		<c:if test="${sexInvoiceJsonData.tBank.bankName1 !='' }">
		<p style="text-align:right; padding-top:4%; font-weight:bold;">${sexInvoiceJsonData.tBank.bankName1}</p>
		<p style="text-align:right; padding-top:4%; font-weight:bold;">${sexInvoiceJsonData.tBank.bankName2}</p>
		<p style="text-align:right; padding-top:4%; font-weight:bold;">${sexInvoiceJsonData.tBank.bankCity1} ${sexInvoiceJsonData.tBank.portCode1}</p>
		<p style="text-align:right; padding-top:4%; font-weight:bold;">${sexInvoiceJsonData.tBank.country}</p>
		<p style="text-align:right; padding-top:4%; font-weight:bold;">${sexInvoiceJsonData.tBank.acNumber}</p>
		<p style="text-align:right; padding-top:4%; font-weight:bold;">${sexInvoiceJsonData.tBank.swiftid}</p>
		</c:if>
	</div> --%>
	
	<div class="col-xs-12">
		<p style="text-align:right; font-weight:bold;">Payable To:${sexInvoiceJsonData.sOrg}</p>
		<c:if test="${sexInvoiceJsonData.tBank.bankName1 !='' }">
			<p style="text-align:right; font-weight:bold;">${sexInvoiceJsonData.tBank.bankName1}, ${sexInvoiceJsonData.tBank.bankName2}</p>
			<p style="text-align:right; font-weight:bold;">${sexInvoiceJsonData.tBank.bankCity1} ${sexInvoiceJsonData.tBank.portCode1}</p>
			<p style="text-align:right; font-weight:bold;">${sexInvoiceJsonData.tBank.country}</p>
			<p style="text-align:right; font-weight:bold;">${sexInvoiceJsonData.tBank.acNumber}</p>
			<p style="text-align:right; font-weight:bold;">${sexInvoiceJsonData.tBank.swiftid}</p>
		</c:if>
	</div>
</c:if>



<!--  FOR INDIA START-->
 <c:if test="${fn:toUpperCase(sexInvoiceJsonData.country) =='INDIA'}">
  
	<div class="table-responsive">
	<c:if test="${sexInvoiceJsonData.tConList.size() ==1}">
		<table class="table">
			<tr>
				<td>Container Number: ${sexInvoiceJsonData.tConList[0].vhilm}</td>
				<td>Container Size: ${sexInvoiceJsonData.tConList[0].wgbez60}</td>
			</tr>
		</table>
	</c:if>          
	<c:if test="${sexInvoiceJsonData.tFinalConList.size() ==0}">
		<c:if test="${sexInvoiceJsonData.tConList.size() >1}">
			<table class="table">
				<c:forEach items="${sexInvoiceJsonData.tConList}" var="con">
				<tr>
					<td>Container Number: ${con.vhilm}</td>
					<td>Container Size: ${con.wgbez60}</td>
				</tr>
				</c:forEach>
			</table>
		</c:if>  
	</c:if>     
  	<table class="table">
    <thead>
      <tr>
        <th>Description</th>
        <th>Rate</th>
        <th>Conversion</th>
        <th>UOM</th>
        <th>TAXABLE (INR)</th>
        <th>NON-TAXABLE (INR)</th>
       
      </tr>
    </thead>
    <tbody>
    <c:forEach items="${sexInvoiceJsonData.tFHeaderList}" var="itemf">

      <tr>
       <td>${itemf.vText}</td>
        <td>${itemf.kbetr}  ${itemf.waers}</td>
        <td>${itemf.conv}</td>
        <td>${itemf.tdText}</td>
        <td>${itemf.tKwert}</td>
        <td>${itemf.ntKwert}</td>
      </tr>
  </c:forEach>
  <tr style="font-weight:bold;">

  <td colspan="4">Sub Total: </td>
  <td>${sexInvoiceJsonData.gvTotal}</td>
  <td>${sexInvoiceJsonData.gvNtTotal}</td>	   
  </tr>

  <c:forEach items="${chargeLstData}" var="it">
      <tr>
       <td>${it.vText}</td>
        <td>${it.kbetr}  ${it.waers}</td>
        <td></td>
        <td></td>
        <td></td>
        <td>${it.kwert} </td>
      </tr>
  </c:forEach>
  <c:if test="${sexInvoiceJsonData.tFinalConList.size()>0}">
  <c:forEach items="${sexInvoiceJsonData.tFinalConList}" var="con">
  <c:if test="${con.type=='1' }">
<tr>

       <td colspan="4">Container Number: ${con.vhilm}</td>
       <td colspan="4">Container Size: ${con.wgbez60}</td>
      </tr>
      </c:if>
      <c:if test="${con.type=='2' }">
<tr>

       <td>${con.vText}</td>
        <td>${con.kbetr}  ${con.waers}</td>
        <td>${con.conv}</td>
        <td>${con.tdText}</td>
        <td>${con.tKwert}</td>
        <td>${con.ntKwert}</td>
      </tr>
      </c:if>
      <c:if test="${con.type=='3' }">
<tr>
       <td colspan="4">Total:</td>
        <td>${con.tKwert}</td>
        <td>${con.ntKwert}</td>
      </tr>
      </c:if>
      </c:forEach>
      </c:if>
      <tr style="font-weight:bold;">
       <td colspan="5">Tax Total:</td>
        <td>${sexInvoiceJsonData.vTax}</td>
      </tr>
      <tr style="font-weight:bold;">
       <td colspan="5">Total Amount:</td>
        <td>${sexInvoiceJsonData.total}</td>
      </tr>

      
    </tbody>
  </table>
  </div> <!-- table-responsive -->
  <div class="col-xs-12">
	  <h5>Amount in words: <strong> ${sexInvoiceJsonData.words}</strong></h5>
	  <h6>Note:<strong> </strong></h6>
	  <h6>Prepared By:<strong> ${sexInvoiceJsonData.user}</strong></h6>
	  <h6>Name:<strong> </strong></h6>
	  <h6>Service Tax No.:<strong> ${sexInvoiceJsonData.gvStaxNo}</strong></h6>
	  <h6>PAN No.:<strong> ${sexInvoiceJsonData.gvPanNo}</strong></h6>
  </div>
  
<%--   <div class="col-xs-12">
		<p style="text-align:right; padding-top:4%; font-weight:bold;">Payable To:${sexInvoiceJsonData.sOrg}</p>
		<p style="text-align:right; padding-top:4%; font-weight:bold;">${sexInvoiceJsonData.tBank.bankName1}</p>
		<p style="text-align:right; padding-top:4%; font-weight:bold;">${sexInvoiceJsonData.tBank.bankName2}</p>
		<p style="text-align:right; padding-top:4%; font-weight:bold;">${sexInvoiceJsonData.tBank.bankCity1} ${sexInvoiceJsonData.tBank.portCode1}</p>
		<p style="text-align:right; padding-top:4%; font-weight:bold;">${sexInvoiceJsonData.tBank.country}</p>
		<p style="text-align:right; padding-top:4%; font-weight:bold;">${sexInvoiceJsonData.tBank.acNumber}</p>
		<p style="text-align:right; padding-top:4%; font-weight:bold;">${sexInvoiceJsonData.tBank.swiftid}</p>
	</div> --%>
	<div class="col-xs-12">
		<p style="text-align:right; font-weight:bold;">Payable To:${sexInvoiceJsonData.sOrg}</p>
		<p style="text-align:right; font-weight:bold;">${sexInvoiceJsonData.tBank.bankName1}, ${sexInvoiceJsonData.tBank.bankName2}</p>
		<p style="text-align:right; font-weight:bold;">${sexInvoiceJsonData.tBank.bankCity1} ${sexInvoiceJsonData.tBank.portCode1}</p>
		<p style="text-align:right; font-weight:bold;">${sexInvoiceJsonData.tBank.country}</p>
		<p style="text-align:right; font-weight:bold;">${sexInvoiceJsonData.tBank.acNumber}</p>
		<p style="text-align:right; font-weight:bold;">${sexInvoiceJsonData.tBank.swiftid}</p>
	</div>
</c:if>

<!--  FOR INDIA END -->


  
<!--   </div> table -->
  
  
  <!-- hamid -->
 <!--  </div>	</div> -->

  
  <section class="footer1">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-1"></div>
					<div class="col-xs-10" style="text-align: center;">
					<span style="font-weight: bold; font-size: 14px;">${sexInvoiceJsonData.sOrg}</span></br> 
					Sales Office:${sexInvoiceJsonData.tFooter.name2},${sexInvoiceJsonData.tFooter.name3},${sexInvoiceJsonData.tFooter.name4}</br>
					Tel:${sexInvoiceJsonData.tFooter.telNumber}, Fax:${sexInvoiceJsonData.tFooter.faxNumber}</br>
				 	Email:${sexInvoiceJsonData.tFooter.email}</div>
					
				</div>
			</div>
		</section>
  </div>
  
</div>
		</section>
		
		<%@include file="footer.jsp" %>
		
	<script>
	
function printContent(el){
var restorepage = $('body').html();
var printcontent = $('#' + el).clone();
$('body').empty().html(printcontent);
window.print();
$('body').html(restorepage);
}

	</script>