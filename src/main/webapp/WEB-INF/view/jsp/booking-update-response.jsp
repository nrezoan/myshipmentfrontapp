<%-- <jsp:include page="header.jsp"></jsp:include> --%>
<jsp:include page="header_v2.jsp"></jsp:include>
<jsp:include page="navbar.jsp"></jsp:include>

<style>
html {
	height: auto;
	background-color: #FFF;
}
</style>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!--END OF HEADER-->
<c:if test="${success==true}">
	<section>
		<div class="container" style="height: 20px;"></div>
	</section>
	<section>
	<!--
	<div class="container">
    	<div class="row">
        	<div class="input-group-addon commercial">
            	<h4 style="text-align:center;">Booking Update was Successful</h4>
			</div>
		</div>
	</div> -->
		<div class="container">
			<div class="panel-group">
				<div class="panel panel-success">
					<div class="panel-heading"
						style="font-size: 20px; font-weight: bold; text-align: center;">
						<i class="fa fa-check-circle-o" aria-hidden="true"></i> You Have
						Successfully Updated Booking
					</div>
					<div class="panel-body">
						<div
							style="font-size: 18px; font-weight: bold; text-align: center; height: 38px;">Booking
							Information</div>
	<%-- 					<table class="table ">
							<tbody>
								<tr>
									<td style="text-align: center;">HBL NO.</td>
									<td><a
										href="shippingOrderFromUrl?searchString=${ jsonResponse.hblnumber}">${ jsonResponse.hblnumber}</a></td>

								</tr>
								<tr>
									<td style="text-align: center;">DOCUMENT NO.</td>
									<td>${ jsonResponse.zsalesdocument}</td>

								</tr>
							</tbody>
						</table> --%>
												<div class="row">
							<div class="col-md-5">
								<p style="float: right">HBL/HAWB NO.</p>
							</div>
							<div class="col-md-7">
								<p>
								<a href="shippingOrderFromUrl?searchString=${ jsonResponse.hblnumber}">${ jsonResponse.hblnumber}</a>
										<span style="color: darkorange;"> (Please Click to Download/View Shipping Order)</span>
								</p>		
							</div>
						</div>
						<div class="row">
							<div class="col-md-5">
								<p style="float: right">DOCUMENT NO.</p>
							</div>
							<div class="col-md-7">
								<p>${jsonResponse.zsalesdocument}</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</c:if>
<!--         <section>
            <div class="container">
             <div style="float:right;">
             <button type="button" class="btn btn-default" id="print">Print</button>
             <button type="button" class="btn btn-primary" id="pdf">PDF</button>
             </div>
            </div>
        </section> -->
<!-- <div class="col-xs-12" style="height:20px;"></div> -->
<%--          <c:if test="${success==true}">
         <section>
            <div class="container">
            <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6" style="margin-top:-20px;">
            <div class="col-xs-12" style="height:20px;"></div>
            <table class="table commercial-table packaging-table1" style="margin-top:-20px;">
             
               
               <tbody>
                   <tr>
                     <td id="hdl">HBL NO. :</td>
                     <td style="border-top: 1px solid #337ab7">${jsonResponse.hblnumber}</td>
                 </tr>
                   
                   <tr>
                     <td id="hdl">DOCUMENT NO. :</td>
                     <td>${jsonResponse.zsalesdocument}</td>
                   </tr>
                  
               </tbody>
            </table>
                </div>
                </div>
             </div>
        </section>
        </c:if>
        <div class="col-xs-12" style="height:20px;"></div> --%>

<!--         <section>
            <div class="container">
             <div class="row">
                <div class="input-group-addon commercial">
                    <h4 style="text-align:center;background-color:#e60000;"></h4>
				</div>
             </div>
            </div>
        </section> -->

<%--         <section>
            <div class="container">
            <div class="row">
            <div class="col-md-6" style="height:20px;"></div>
            <table class="table commercial-table packaging-table1" style="margin-top:-20px;">
                <thead>
                  <tr>
                      <td >Id</td>
                      <td >Type</td>
                      <td >number</td>
                      <td >message</td>
                      <td >logNo</td>
                      <td >log_msg_no</td>
                      <td >messageV1</td>
                      <td >messageV2</td>
                       <td >messageV3</td>
                        <td >messageV4</td>
                         <td >parameter</td>
                          <td >row</td>
                           <td >field</td>
                            <td >system</td>
                            
                  </tr>
                </thead>
                
                <tbody>
                 <c:forEach items="${ jsonResponse.bapiReturn3}" var="bapiReturn">
                  <tr>
                      <td >${bapiReturn.id }</td>
                      <td >${bapiReturn.type }</td>
                      <td >${bapiReturn.number }</td>
                      <td >${bapiReturn.message }</td>
                      <td >${bapiReturn.logNo }</td>
                      <td >${bapiReturn.log_msg_no }</td>
                      <td >${bapiReturn.messageV1 }</td>
                      <td >${bapiReturn.messageV2 }</td>
                      <td >${bapiReturn.messageV3 }</td>
                      <td >${bapiReturn.messageV4 }</td>
                      <td >${bapiReturn.parameter }</td>
                      <td >${bapiReturn.row }</td>
                      <td >${bapiReturn.field }</td>
                      <td >${bapiReturn.system }</td>
                      
                  </tr>
                  
                  </c:forEach>
                </tbody>
                </table>
                </div>
                </div>
                
           
        </section> --%>
<c:if test="${success==false and empty jsonResponse.zsalesdocument}">
	<section>
		<div class="container" style="height: 20px;"></div>
	</section>
	<section>
		<div class="container">
			<div class="panel-group">
				<div class="panel panel-danger">
					<div class="panel-heading"
						style="font-size: 20px; font-weight: bold; text-align: center;">
						<i class="fa fa-times-circle-o" aria-hidden="true"></i> Error
						Occured, Booking update Failed
					</div>
					<div class="panel-body">

						<table class="table table-responsive">
							<thead>
								<tr style="font-weight: bold;">
									<td>Id</td>
									<td>Type</td>
									<td>number</td>
									<td>message</td>
									<!-- <td >logNo</td>
									<td >log_msg_no</td> -->
									<td>messageV1</td>
									<td>messageV2</td>
									<td>messageV3</td>
									<td>messageV4</td>
									<td>parameter</td>
									<td>row</td>
									<td>field</td>
									<td>system</td>
								</tr>
							</thead>

							<tbody>
								<c:forEach items="${ jsonResponse.bapiReturn3}" var="bapiReturn">
									<tr>
										<td>${bapiReturn.id }</td>
										<td>${bapiReturn.type }</td>
										<td>${bapiReturn.number }</td>
										<td>${bapiReturn.message }</td>
										<%-- <td>${bapiReturn.logNo }</td>
										<td>${bapiReturn.log_msg_no }</td> --%>
										<td>${bapiReturn.messageV1 }</td>
										<td>${bapiReturn.messageV2 }</td>
										<td>${bapiReturn.messageV3 }</td>
										<td>${bapiReturn.messageV4 }</td>
										<td>${bapiReturn.parameter }</td>
										<td>${bapiReturn.row }</td>
										<td>${bapiReturn.field }</td>
										<td>${bapiReturn.system }</td>
									</tr>

								</c:forEach>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>

</c:if>






<jsp:include page="footer_v2_fixed.jsp"></jsp:include>