<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<script
	src="${pageContext.request.contextPath}/resources/bootstrap-select/js/bootstrap-select.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/booking-update.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/booking-updatedHelperMethods.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bootstrap-select/css/bootstrap-select.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/myshipment_v2/update-order-booking.css">

<style>
html, body {
	background-color: #FFF;
}
.select2 {
	width: 100% !important;
}
</style>

<script type="text/javascript">
	var modelArray = [];
	var copyArray = [];
	var sendArray = [];
	var model;
	var rowNumber;
	var modelCopy;
	var directBookingParams = ${directBookingParamsJson};
	var grShow = "${grDone}";
</script>

<c:forEach items="${directBookingParams.orderItemLst }" var="item">
	<script type="text/javascript">
		model = {
			itemNumber : "${item.itemNumber}",
			poNumber : "${item.poNumber}",
			styleNo : "${item.styleNo}",
			sizeNo : "${item.sizeNo}",
			color : "${item.color}",
			hsCode : "${item.hsCode}",
			materialText : "${item.materialText}",
			material : "${item.material}",
			styleNo : "${item.styleNo}",
			refNo : "${item.refNo}",
			projectNo : "${item.projectNo}",
			productCode : "${item.productCode}",
			color : "${item.color}",
			sizeNo : "${item.sizeNo}",
			totalPieces : "${item.totalPieces}",
			curtonQuantity : "${item.curtonQuantity}",
			totalCbm : "${item.totalCbm}",
			totalVolume : "${item.totalVolume}",
			grossWeight : "${item.grossWeight}",
			netWeight : "${item.netWeight}",
			payCond : "${item.payCond}",
			unitPrice : "${item.unitPrice }",
			currency : "${item.currency}",
			hsCode : "${item.hsCode}",
			commInvoice : "${item.commInvoice}",
			commInvoiceDate : "${item.commInvoiceDate}",
			netCost : "${item.netCost}",
			pcsPerCarton : "${item.pcsPerCarton}",
			cbmPerCarton : "${item.cbmPerCarton}",
			grossWeightPerCarton : "${item.grossWeightPerCarton}",
			netWeightPerCarton : "${item.netWeightPerCarton}",
			cartonLength : "${item.cartonLength}",
			cartonWidth : "${item.cartonWidth}",
			cartonHeight : "${item.cartonHeight}",
			cartonUnit : "${item.cartonUnit}",
			cartonSerNo : "${item.cartonSerNo}",
			reference1 : "${item.reference1}",
			reference2 : "${item.reference2}",
			reference3 : "${item.reference3}",
			reference4 : "${item.reference4}",
			qcDate : "${item.qcDate}",
			releaseDate : "${item.releaseDate}",
			description : "${item.description}",
			itemDescription : "${item.itemDescription}",
			shippingMark : "${item.shippingMark}"
		};

		modelArray.push(model);
		//console.log(modelArray);
	</script>
</c:forEach>
<c:forEach items="${directBookingParams.oldOrderItemList }" var="item">
	<script type="text/javascript">
		model2 = {
			itemNumber : "${item.itemNumber}",
			poNumber : "${item.poNumber}",
			styleNo : "${item.styleNo}",
			sizeNo : "${item.sizeNo}",
			color : "${item.color}",
			hsCode : "${item.hsCode}",
			materialText : "${item.materialText}",
			material : "${item.material}",
			styleNo : "${item.styleNo}",
			refNo : "${item.refNo}",
			projectNo : "${item.projectNo}",
			productCode : "${item.productCode}",
			color : "${item.color}",
			sizeNo : "${item.sizeNo}",
			totalPieces : "${item.totalPieces}",
			curtonQuantity : "${item.curtonQuantity}", /* curtonQuantity */
			totalCbm : "${item.totalCbm}",
			totalVolume : "${item.totalVolume}",
			grossWeight : "${item.grossWeight}",
			netWeight : "${item.netWeight}",
			payCond : "${item.payCond}",
			unitPrice : "${item.unitPrice }",
			currency : "${item.currency}",
			hsCode : "${item.hsCode}",
			commInvoice : "${item.commInvoice}",
			commInvoiceDate : "${item.commInvoiceDate}",
			netCost : "${item.netCost}",
			pcsPerCarton : "${item.pcsPerCarton}",
			cbmPerCarton : "${item.cbmPerCarton}",
			grossWeightPerCarton : "${item.grossWeightPerCarton}",
			netWeightPerCarton : "${item.netWeightPerCarton}",
			cartonLength : "${item.cartonLength}",
			cartonWidth : "${item.cartonWidth}", /* cartonWidth */
			cartonHeight : "${item.cartonHeight}", /* cartonHeight */
			cartonUnit : "${item.cartonUnit}",
			cartonSerNo : "${item.cartonSerNo}",
			reference1 : "${item.reference1}",
			reference2 : "${item.reference2}",
			reference3 : "${item.reference3}",
			reference4 : "${item.reference4}",
			qcDate : "${item.qcDate}",
			releaseDate : "${item.releaseDate}",
			description : "${item.description}",
			itemDescription : "${item.itemDescription}",
			shippingMark : "${item.shippingMark}"
		};
		copyArray.push(model2);
	</script>
</c:forEach>

<div class="container-fluid-booking-update-234903">

	<div class="nav-tabs-custom-949974">
		<section class="" style="display: block;">
			<span id="bookingUpdateHeader"></span>
			<hr>
		</section>
	</div>

	<ul class="nav nav-tabs nav-justified nav-book-update"
		style="margin-left: 0px;">
		<li class="active"><a data-toggle="tab" href="#headerItem">Header</a></li>
		<li class=""><a data-toggle="tab" href="#lineItem">Line
				Item</a></li>
		<!-- <li><a data-toggle="tab" href="#packingList">Packing List</a></li> -->
	</ul>
	<div class="tab-content tab-book-update">
		<div id="headerItem" class="tab-pane fade in active">
		<!-- <div id="headerItem" class="tab-pane fade"> -->
			<!-- hamid -->
			<div class="row">
				<div class="col-md-12">
					<span class="booking-update-information-header">B/L
						Information</span>
					<hr>
				</div>
				<div class="col-md-12">
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">HBL/HAWB No.</label>
							</div>
							<div class="col-md-12">
								<span>${directBookingParams.orderHeader.hblNumber } </span>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group'>
							<div class="col-md-12">
								<label class="text-center">Carton Quantity</label>
							</div>
							<div class="col-md-12">
								<span>${totalQuantity } </span>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group'>
							<div class="col-md-12">
								<label class="text-center">Total Gross Weight</label>
							</div>
							<div class="col-md-12">
								<span>${grossWeight } </span>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group'>
							<div class="col-md-12">
								<label class="text-center">Total Volume</label>
							</div>
							<div class="col-md-12">
								<span>${totalCBM} </span>
							</div>
						</div>
					</div>
				</div>
				<br>
				<div class="col-md-12 booking-update-padding-column">
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Exp. Number</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="expNo" class="form-control" id="expNo"
									value="${directBookingParams.orderHeader.expNo }" />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Exp. Date</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="expDate" id="expDate"
									value="${directBookingParams.orderHeader.expDate }"
									class="form-control date-picker" />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">LC/TT/PO Number</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="lcTtPono" id="lcTtPono"
									value="${directBookingParams.orderHeader.lcTtPono }"
									class="form-control" />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">LC/TT/PO Date</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="lcTtPoDate" id="lcTtPoDate"
									value="${directBookingParams.orderHeader.lcTtPoDate }"
									class="form-control date-picker" />
							</div>
						</div>
					</div>
				</div>				
				<div class="col-md-12 booking-update-padding-column">
					<div class="col-md-6">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Description of Goods</label>
							</div>
							<div class="col-md-12">
								<textarea name="description" id="description"
										onkeypress="checkCharacterLimit()" class="textarea color-change" required>${directBookingParams.orderHeader.description }</textarea>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Shipping Marks</label>
							</div>
							<div class="col-md-12">
								<textarea name="shippingMark" id="shippingMark"
									onkeypress="checkCharacterLimitShipingMark()" class="textarea color-change" required>${directBookingParams.orderHeader.shippingMark }</textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<hr>
				</div>
			</div>
			
			
			
			<!-- Sanjana Khan...CommInv No and Date Edit -->

			<div class="row">
			
				<div class="col-md-12">
					<span class="booking-update-information-header">Comm. Invoice Details</span>
					<hr>
				</div>
			
				<div class="col-md-12 booking-update-padding-column">
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Commercial Invoice Number</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="commInvoice" class="form-control"
									id="commInvoice"
									value="${directBookingParams.orderHeader.comInvNo }" />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Commercial Invoice Date</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="commInvoiceDate" id="commInvoiceDate"
									value="${directBookingParams.orderHeader.comInvDate }"
									onkeydown="return false" class="form-control date-picker" />
							</div>
						</div>
					</div>
				</div>
				
				<div class="col-md-12">
					<hr>
				</div>
			</div>

			<!-- Partner Information -->
			<div class="row">
				<div class="col-md-12">
					<span class="booking-update-information-header">Partner
						Information</span>
					<hr>
				</div>
				<!-- shipper -->
				<div class="col-md-12 booking-update-padding-column">
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Shipper</label>
							</div>
							<div class="col-md-12">
								<input name="shipper" id="shipper"
									value="${directBookingParams.orderHeader.shipperName }"
									class="form-control" readonly />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Shipper Address 1</label>
							</div>
							<div class="col-md-12">
								<input name="shipperAddress1" id="shipperAddress1"
									value="${directBookingParams.orderHeader.shipperAddress1 }"
									class="form-control" readonly />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Shipper Address 2</label>
							</div>
							<div class="col-md-12">
								<input name="shipperAddress2" id="shipperAddress2"
									value="${directBookingParams.orderHeader.shipperAddress2 }"
									class="form-control" readonly />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Shipper Address 3</label>
							</div>
							<div class="col-md-12">
								<input name="shipperAddress3" id="shipperAddress3"
									value="${directBookingParams.orderHeader.shipperAddress3 }"
									class="form-control" readonly />
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 booking-update-padding-column">
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Shipper City</label>
							</div>
							<div class="col-md-12">
								<input name="shipperCity" id="shipperCity"
									value="${directBookingParams.orderHeader.shipperCity }"
									class="form-control" readonly />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Shipper Country Code</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="shipperCountry" id="shipperCountry"
									value="${directBookingParams.orderHeader.shipperCountry }"
									class="form-control" readonly />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Shipper Country</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="shipperCountryDesc" id="shipperCountryDesc"
									value="${directBookingParams.orderHeader.shipperCountryDesc }"
									class="form-control" readonly />
							</div>
						</div>
					</div>
				</div>
				<!-- buyer -->
				<div class="col-md-12 booking-update-padding-column">
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Buyer</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="buyer" id="buyer"
									value="${directBookingParams.orderHeader.buyerName}"
									class="form-control" readonly />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Buyer Address 1</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="buyerAddress1" id="buyerAddress1"
									value="${directBookingParams.orderHeader.buyerAddress1 }"
									class="form-control" readonly />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Buyer Address 2</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="buyerAddress2" id="buyerAddress2"
									value="${directBookingParams.orderHeader.buyerAddress2 }"
									class="form-control" readonly />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Buyer Address 3</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="buyerAddress3" id="buyerAddress3"
									value="${directBookingParams.orderHeader.buyerAddress3 }"
									class="form-control" readonly />
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 booking-update-padding-column">
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Buyer City</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="buyerCity" id="buyerCity"
									value="${directBookingParams.orderHeader.buyerCity }"
									class="form-control" readonly />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Buyer Country Code</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="buyerCountry" id="buyerCountry"
									value="${directBookingParams.orderHeader.buyerCountry }"
									class="form-control" readonly />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Buyer Country</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="buyerCountryDesc" id="buyerCountryDesc"
									value="${directBookingParams.orderHeader.buyerCountryDesc }"
									class="form-control" readonly />
							</div>
						</div>
					</div>					
				</div>
				<!-- notify party -->
				<div class="col-md-12 booking-update-padding-column">
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Notify Party</label>
							</div>
							<div class="col-md-12">
<%-- 								<select class="select2 predictable-select"	id="notifyPartyName" onchange="">
									<option value="">Please Select</option>
									<c:forEach
										items="${supplierDetails.buyersSuppliersmap.notifyParty }"
										var="notifyParty">
										<option value="${notifyParty.key }">${notifyParty.value }</option>
									</c:forEach>
								</select> --%>
								<input type="text" name="notifyPartyName" id="notifyPartyName"
									value="${directBookingParams.orderHeader.notifyPartyName}"
									class="form-control" readonly />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Notify Party's Address 1</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="notifyPartyAddress1"
									id="notifyPartyAddress1"
									value="${directBookingParams.orderHeader.notifyPartyAddress1 }"
									class="form-control" />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Notify Party's Address 2</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="notifyPartyAddress2"
									id="notifyPartyAddress2"
									value="${directBookingParams.orderHeader.notifyPartyAddress2 }"
									class="form-control" />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Notify Party's Address 3</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="notifyPartyAddress3"
									id="notifyPartyAddress3"
									value="${directBookingParams.orderHeader.notifyPartyAddress3 }"
									class="form-control" />
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 booking-update-padding-column">
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Notify Party's City</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="notifyPartyCity" id="notifyPartyCity"
									value="${directBookingParams.orderHeader.notifyPartyCity }"
									class="form-control" />
							</div>
						</div>
					</div>
					<div class="col-md-3" style="display:none">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Notify Party's Country Code</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="notifyPartyCountry"
									id="notifyPartyCountry"
									value="${directBookingParams.orderHeader.notifyPartyCountry }"
									class="form-control" readonly/>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Notify Party's Country</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="notifyPartyCountryDesc" id="notifyPartyCountryDesc"
									value="${directBookingParams.orderHeader.notifyPartyCountryDesc }"
									class="form-control" readonly/>
							</div>
						</div>
					</div>					
				</div>
				<!-- shipper bank -->
				<div class="col-md-12 booking-update-padding-column">
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Shipper's Bank</label>
							</div>
							<div class="col-md-12">
								<%-- <select style="" class="select2 predictable-select" id="shippersBankName" onchange="">
									<option value="">Please Select</option>
									<c:forEach items="${supplierDetails.buyersSuppliersmap.shippersBank }" var="shippersBank">
										<option value="${shippersBank.key }">${shippersBank.value }</option>
									</c:forEach>
								</select> --%>
								<input type="text" name="shippersBankName" id="shippersBankName"
									value="${directBookingParams.orderHeader.shippersBankName}"
									class="form-control" readonly />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Shipper's Bank Address 1</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="shippersBankAddress1"
									id="shippersBankAddress1"
									value="${directBookingParams.orderHeader.shippersBankAddress1 }"
									class="form-control " />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Shipper's Bank Address 2</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="shippersBankAddress2"
									id="shippersBankAddress2"
									value="${directBookingParams.orderHeader.shippersBankAddress2 }"
									class="form-control " />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Shipper's Bank Address 3</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="shippersBankAddress3"
									id="shippersBankAddress3"
									value="${directBookingParams.orderHeader.shippersBankAddress3 }"
									class="form-control " />
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 booking-update-padding-column">
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Shipper's Bank City</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="shippersBankCity" id="shippersBankCity"
									value="${directBookingParams.orderHeader.shippersBankCity }"
									class="form-control" />
							</div>
						</div>
					</div>
					<div class="col-md-3" style="display:none">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Shipper's Bank Country Code</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="shippersBankCountry"
									id="shippersBankCountry"
									value="${directBookingParams.orderHeader.shippersBankCountry }"
									class="form-control" readonly/>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Shipper's Bank Country</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="shippersBankCountryDesc" id="shippersBankCountryDesc"
									value="${directBookingParams.orderHeader.shippersBankCountryDesc }"
									class="form-control" readonly/>
							</div>
						</div>
					</div>					
				</div>
				<div class="col-md-12 booking-update-padding-column">
					<div class="col-md-6">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Remarks 1</label>
							</div>
							<div class="col-md-12">
								<textarea name="remarks1" id="remarks1" class="textarea">${directBookingParams.orderHeader.remarks1 }</textarea>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Remarks 2</label>
							</div>
							<div class="col-md-12">
								<textarea name="remarks2" id="remarks2" class="textarea">${directBookingParams.orderHeader.remarks2}</textarea>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

		<div id="lineItem" class="tab-pane fade">
		<!-- <div id="lineItem" class="tab-pane fade in active"> -->

			<!-- table part -->
			<div class="row row-no-margin-331932" style="padding-top: 10px;">
				<div class="col-xs-12 col-md-12">
					<div class="table-responsive" id="headingTwo">
						<table class="table table-hover" id="grValueTable">
							<thead>
								<tr>
									<th style="text-align: center;">Item No.</th>
									<th style="text-align: center;">PO No.</th>
									<th style="text-align: center;">Style</th>
									<th style="text-align: center;">Size</th>
									<th style="text-align: center;">Color</th>
									<th style="text-align: center;">HS Code</th>
									<th style="text-align: center;">Material</th>
									<th colspan="2" style="text-align: center;">Action</th>
								</tr>
							</thead>
							<tbody id="grValueTableBody" style="text-align: center;">
							</tbody>

						</table>
						<script>
							createTableGoodsReceived(modelArray);
						</script>
					</div>
				</div>
			</div>
		</div>
	</div>
	
<!-- 	<div id="packingList" class="tab-pane fade" style="display: none;">
		<h3>Packing List</h3>
		<p>
			container demo
		<div>
			<strong><a role="button" data-toggle="collapse"
				data-parent="#accordion" href="#Three" aria-expanded="true"
				aria-controls="collapseThree">Expand</a></strong>
			<div id="collapseThree" class="panel-collapse collapse"
				role="tabpanel" aria-labelledby="headingOne">
				<div class="panel-body">
					<div class="row">
						<div class="col-xs-12" style="height: 20px;"></div>
						<div class="col-md-6">
							<div class="forms">
								<div class="form-group required">
									<div class="">
										<div style="color: black;">
											<span style="z-index: 1; margin-top: -30px; display: block;">Please
												add sizes (e.g. L, M XL etc) first before adding packing
												list items !</span>
										</div>
										<div id="errorMsg-pkl" style="color: red;">
											<span
												style="position: absolute; z-index: 1; margin-bottom: 50px;">
											</span>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-2 col-lg-2 col-sm-2 col-xs-2">
													<div class=""
														style="background-color: #334d4d; color: #fff;">
														<span class="control-label" style="margin-left: 10px;">Size:</span>
													</div>
												</div>
												<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
													<input type="text" name="text"
														class="form-control auto-complete color-change"
														id="txt-add-size" />
												</div>
												<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
													<button type="button" class="btn btn-primary"
														id="btn-add-size">Add Size</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12" id="table-container">
							<div class="table-responsive">
								<table class="table table-bordered" id="tbl-packing-list"
									style="background-color: #5c8a8a; color: #fff;"
									id="tbl-pack-list">
									<thead style="background-color: #334d4d;">
										<tr>
											<th rowspan="2" style="width: 100px;">Ctn No</th>
											<th rowspan="2">Ctn</th>
											<th rowspan="2">Colour</th>
											<th colspan="0" style="text-align: center;">Size</th>
											<th rowspan="2">Pcs/Ctn</th>
											<th rowspan="2">Total pcs</th>
											<th rowspan="2">Remarks</th>
											<th rowspan="2" style="width: 20px;">
												<button type="button" class="btn btn-primary"
													id="btn-add-item">Add item</button>
											</th>
										</tr>
										<tr>
										</tr>
									</thead>
									<tbody style="color: #000;">
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="col-xs-12" style="height: 10px;"></div>
					<div class="row"
						style="float: right; margin-right: 2%; margin-top: 2%;">
						<input type="submit" class="btn btn-primary" value="Update"
							id="btn-update-pck-lst">
						<button type="button" class="btn btn-danger" id="cancel-packlist">Cancel</button>
					</div>
				</div>
			</div>
		</div>
	</div> -->

	<div class="container">
		<div class="row">
			<span class="" id="span-message"></span>
			<c:if test="${updateStatus==true }">
				<span class="message-success" id="span-message">${updateMessage }</span>
			</c:if>
			<c:if test="${updateStatus==false }">
				<span class="message-error" id="span-message">${updateMessage }</span>
			</c:if>
		</div>
	</div>
	<c:if test="${directBookingParams.orderHeader.cargoStatus!=null}">
		<p align="center">GR DONE, LINE ITEM UPDATE IS NOT ALLOWED</p>
	</c:if>
	<div class="row" style="float: right; margin-top: 1%; margin-right: 1%;">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<input class="btn btn-primary" value="Finish Update" id="btn-finish-update">
			<input type="submit" class="btn btn-danger" id="cancelUpdate" value="Cancel Update">
		</div>
	</div>
</div>

<div class="modal fade" id="displayFormModal" role="dialog">
	<div class="modal-dialog" style="width: 90%">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<!-- <h4 class="modal-title">Modal Header</h4> -->
				<span id="bookingUpdateModalHeader"></span>
			</div>
			<div class="modal-body">
				<div class="row row-no-margin-331932">
					<div class="col-md-12">
						<input type="hidden" name="itemNumber" id="id_itemNumber">
						<span class="booking-update-modal-info-header">Item Details</span>
						<hr class="hr-update-modal">
						<div class="col-md-4">
							<div class="forms" id="formUpdate">
								<div class="form-group required">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="">PO No:</span><span class="required-alone">*</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="poNumber" id="id_poNo"
													class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Total Gross Weight:</span><span
														class="required-alone">*</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="grossWeight" id="id_grossWeight"
													class="form-control color-change" required
													onkeypress="return validateDecimalDataTypeStrict(this,event);" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Carton Length:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="cartonLength" id="id_cartonLength"
													class="form-control" onkeypress="return validateDecimalDataTypeStrict(this,event);"
													onchange="return calculateCBM();" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Carton Width:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="cartonWidth" id="id_cartonWidth"
													onkeypress="return validateDecimalDataTypeStrict(this,event);"
													class="form-control" onchange="return calculateCBM();" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Carton Height:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="cartonHeight" id="id_cartonHeight"
													onkeypress="return validateDecimalDataTypeStrict(this,event);"
													class="form-control" onchange="return calculateCBM();" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Carton Unit:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<select class="form-control" name="cartonMeasurementUnit"
													id="id_cartonMeasurementUnit"
													onchange="return calculateCBM();">
													<option value="">Select</option>
													<option value="IN">IN</option>
													<option selected value="CM">CM</option>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Total CBM:</span><span class="required-alone">*</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="totalVolume" id="id_totalVolume"
													class="form-control color-change" required
													onkeypress="return validateDecimalDataTypeStrict(this,event);" />
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--  class="col-md-4" -->
						<div class="col-md-4">
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Carton Quantity:</span><span class="required-alone">*</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="curtonQuantity"
													id="id_cartonQuantity" class="form-control color-change"
													required onkeypress="return validateDataType(this,event);"
													onchange="return calculateCBMUpdate()" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group   required">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="">Total Pieces:</span><span
														class="required-alone">*</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="totalPieces" id="id_totalPieces"
													class="form-control color-change" required
													onkeypress="return validateDataType(this,event);" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Total Net Weight:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="netWeight" class="form-control"
													id="id_netWeight"
													onkeypress="return validateDecimalDataTypeStrict(this,event);" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Style No:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="styleNo" id="id_styleNo"
													class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>HS Code:</span><span class="required-alone">*</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="hsCode" id="id_hsCode"
													class="form-control color-change" required />
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--  class="col-md-4" -->
						<div class="col-md-4">
							<div class="forms" style="display: none">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Unit:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<select class="form-control drop" name="unit"
													id="add_order_frm_orderItem_unit"
													style="border: 1px solid gray; border-radius: 5px;">
													<option selected value="KG">Kilogram</option>
													<option value="MOK">Mol/kilogram</option>
													<option value="KGM">Kilogram/Mol</option>
													<option value="MOL">Mol</option>
													<option value="MON">Months</option>
													<option value="EA">Each</option>
													<option value="FT2">Square foot</option>
													<option value="FT3">Cubic foot</option>
													<option value="KGS">Kilogram/second</option>
													<option value="KGV">Kilogram/cubic meter</option>
													<option value="TM3">1/cubic meter</option>
													<option value="DZ">Dozen</option>
													<option value="JAR">JAR</option>
													<option value="DM">Decimeter</option>
													<option value="PWC">PLYWOOD CASE</option>
													<option value="?GQ">Microgram/cubic meter</option>
													<option value="?GL">Microgram/liter</option>
													<option value="MPB">Mass parts per billion</option>
													<option value="DR">Drum</option>
													<option value="MPA">Megapascal</option>
													<option value="KGF">Kilogram/Square meter</option>
													<option value="ROL">Role</option>
													<option value="&quot;3">Cubic inch</option>
													<option value="&quot;2">Square inch</option>
													<option value="KGK">Kilogram/Kilogram</option>
													<option value="&quot;">Inch</option>
													<option value="MNM">Millinewton/meter</option>
													<option value="%">Percentage</option>
													<option value="EU">Enzyme Units</option>
													<option value="KVA">Kilovoltampere</option>
													<option value="V%">Percent volume</option>
													<option value="C3S">Cubic centimeter/second</option>
													<option value="D">Days</option>
													<option value="ACR">Acre</option>
													<option value="KWH">Kilowatt hours</option>
													<option value="F">Farad</option>
													<option value="G">Gram</option>
													<option value="MMO">Millimol</option>
													<option value="BDR">Board Drums</option>
													<option value="MMH">Millimeter/hour</option>
													<option value="A">Ampere</option>
													<option value="MMK">Millimol/kilogram</option>
													<option value="L">Liter</option>
													<option value="M">Meter</option>
													<option value="N">Newton</option>
													<option value="QML">Kilomol</option>
													<option value="MMG">Millimol/gram</option>
													<option value="H">Hour</option>
													<option value="MMA">Millimeter/year</option>
													<option value="J">Joule</option>
													<option value="K">Kelvin</option>
													<option value="W">Watt</option>
													<option value="V">Volt</option>
													<option value="A/V">Siemens per meter</option>
													<option value="FT">Foot</option>
													<option value="P">Points</option>
													<option value="S">Second</option>
													<option value="MMS">Millimeter/second</option>
													<option value="MLK">Milliliter/cubic meter</option>
													<option value="R-U">Nanofarad</option>
													<option value="MLI">Milliliter act. ingr.</option>
													<option value="EML">Enzyme Units / Milliliter</option>
													<option value="HA">Hectare</option>
													<option value="BUN">BUNDLES</option>
													<option value="WK">Weeks</option>
													<option value="LHK">Liter per 100 km</option>
													<option value="SKD">SKIDS</option>
													<option value="GM">Gram/Mol</option>
													<option value="GJ">Gigajoule</option>
													<option value="MM3">Cubic millimeter</option>
													<option value="MM2">Square millimeter</option>
													<option value="NAM">Nanometer</option>
													<option value="RF">Millifarad</option>
													<option value="M-2">1 / square meter</option>
													<option value="VPB">Volume parts per billion</option>
													<option value="GHG">Gram/hectogram</option>
													<option value="%O">Per mille</option>
													<option value="PKG">Package</option>
													<option value="VPM">Volume parts per million</option>
													<option value="QT">Quart, US liquid</option>
													<option value="VPT">Volume parts per trillion</option>
													<option value="STR">String</option>
													<option value="AU">Activity unit</option>
													<option value="PT">Pint, US liquid</option>
													<option value="KJM">Kilojoule/Mol</option>
													<option value="JMO">Joule/Mol</option>
													<option value="KJK">Kilojoule/kilogram</option>
													<option value="PS">Picosecond</option>
													<option value="LMS">Liter/Molsecond</option>
													<option value="BT">Bottle</option>
													<option value="NMM">Newton/Square millimeter</option>
													<option value="TO">Tonne</option>
													<option value="PMI">1/minute</option>
													<option value="MIJ">Millijoule</option>
													<option value="TS">Thousands</option>
													<option value="KIK">kg act.ingrd. / kg</option>
													<option value="ST">items</option>
													<option value="SET">SETS</option>
													<option value="MIS">Microsecond</option>
													<option value="MIN">Minute</option>
													<option value="LMI">Liter/Minute</option>
													<option value="UNI">Unit</option>
													<option value="TES">Tesla</option>
													<option value="?C">Degrees Celsius</option>
													<option value="KHZ">Kilohertz</option>
													<option value="CV">Case</option>
													<option value="RLS">ROLLS</option>
													<option value="?F">Fahrenheit</option>
													<option value="GKG">Gram/kilogram</option>
													<option value="000">Meter/Minute</option>
													<option value="MI2">Square mile</option>
													<option value="JKK">Spec. Heat Capacity</option>
													<option value="CD">Candela</option>
													<option value="MHZ">Megahertz</option>
													<option value="JKG">Joule/Kilogram</option>
													<option value="CM">Centimeter</option>
													<option value="OHM">Ohm</option>
													<option value="MHV">Megavolt</option>
													<option value="CL">Centiliter</option>
													<option value="GPH">Gallons per hour (US)</option>
													<option value="KOH">Kiloohm</option>
													<option value="TC3">1/cubic centimeter</option>
													<option value="M3">Cubic meter</option>
													<option value="M2">Square meter</option>
													<option value="MG">Milligram</option>
													<option value="COI">COIL</option>
													<option value="ONE">One</option>
													<option value="DAY">Days</option>
													<option value="ML">Milliliter</option>
													<option value="MI">Mile</option>
													<option value="IDR">Iron Drum</option>
													<option value="GOH">Gigaohm</option>
													<option value="MA">Milliampere</option>
													<option value="KPA">Kilopascal</option>
													<option value="MV">Millivolt</option>
													<option value="MGW">Megawatt</option>
													<option value="MW">Milliwatt</option>
													<option value="M/M">Mol per cubic meter</option>
													<option value="MWH">Megawatt hour</option>
													<option value="M/L">Mol per liter</option>
													<option value="MN">Meganewton</option>
													<option value="MGQ">Milligram/cubic meter</option>
													<option value="MM">Millimeter</option>
													<option value="MGO">Megohm</option>
													<option value="M/H">Meter/Hour</option>
													<option value="COL">COLLI</option>
													<option value="MGL">Milligram/liter</option>
													<option value="MGK">Milligram/kilogram</option>
													<option value="CON">Container</option>
													<option value="VAM">Voltampere</option>
													<option value="VAL">Value-only material</option>
													<option value="NI">Kilonewton</option>
													<option value="MGG">Milligram/gram</option>
													<option value="MGE">Milligram/Square centimeter</option>
													<option value="NM">Newton/meter</option>
													<option value="NA">Nanoampere</option>
													<option value="M/S">Meter/second</option>
													<option value="V%O">Permille volume</option>
													<option value="KMH">Kilometer/hour</option>
													<option value="CM2">Square centimeter</option>
													<option value="M2S">Square meter/second</option>
													<option value="PAL">Pallet</option>
													<option value="BLK">BULK</option>
													<option value="MVA">Megavoltampere</option>
													<option value="NS">Nanosecond</option>
													<option value="KMN">Kelvin/Minute</option>
													<option value="PAA">Pair</option>
													<option value="NO">NOS</option>
													<option value="KMK">Cubic meter/Cubic meter</option>
													<option value="PPT">Parts per trillion</option>
													<option value="PAC">Pack</option>
													<option value="OM">Spec. Elec. Resistance</option>
													<option value="TRS">TRUSSES</option>
													<option value="CMH">Centimeter/hour</option>
													<option value="YD2">Square Yard</option>
													<option value="PPM">Parts per million</option>
													<option value="KMS">Kelvin/Second</option>
													<option value="PPB">Parts per billion</option>
													<option value="YD3">Cubic yard</option>
													<option value="WDP">Wooden Pallet</option>
													<option value="CMS">Centimeter/second</option>
													<option value="?A">Microampere</option>
													<option value="OZ">Ounce</option>
													<option value="MEJ">Megajoule</option>
													<option value="WDC">Wooden Case</option>
													<option value="LPH">Liter per hour</option>
													<option value="WDB">WOODEN BOX</option>
													<option value="KM2">Square kilometer</option>
													<option value="GM2">Gram/square meter</option>
													<option value="GM3">Gram/Cubic meter</option>
													<option value="?M">Micrometer</option>
													<option value="?L">Microliter</option>
													<option value="GLI">Gram/liter</option>
													<option value="WCR">WOODEN CRATE</option>
													<option value="PA">Pascal</option>
													<option value="?F">Microfarad</option>
													<option value="RHO">Gram/cubic centimeter</option>
													<option value="WMK">Heat Conductivity</option>
													<option value="CRT">Crate</option>
													<option value="OCM">Spec. Elec. Resistance</option>
													<option value="HL">Hectoliter</option>
													<option value="DEG">Degree</option>
													<option value="PRS">Number of Persons</option>
													<option value="HR">Hours</option>
													<option value="FOZ">Fluid Ounce US</option>
													<option value="MTE">Millitesla</option>
													<option value="HZ">Hertz (1/second)</option>
													<option value="PRC">Group proportion</option>
													<option value="MBA">Millibar</option>
													<option value="CAR">Carton</option>
													<option value="KBK">Kilobecquerel/kilogram</option>
													<option value="HPA">Hectopascal</option>
													<option value="IB">Pikofarad</option>
													<option value="CAN">Canister</option>
													<option value="BOX">BOXES</option>
													<option value="GAU">Gram Gold</option>
													<option value="M3H">Cubic meter/Hour</option>
													<option value="PBG">POLY BAG</option>
													<option value="M3S">Cubic meter/second</option>
													<option value="GAL">US gallon</option>
													<option value="MSC">Microsiemens per centimeter</option>
													<option value="YD">Yards</option>
													<option value="FDR">FIBER DRUM</option>
													<option value="MSE">Millisecond</option>
													<option value="GAI">Gram act. ingrd.</option>
													<option value="WKY">Evaporation Rate</option>
													<option value="PAS">Pascal second</option>
													<option value="GRO">Gross</option>
													<option value="MBZ">Meterbar/second</option>
													<option value="KD3">Kilogram/cubic decimeter</option>
													<option value="22S">Square millimeter/second</option>
													<option value="MS2">Meter/second squared</option>
													<option value="G/L">gram act.ingrd / liter</option>
													<option value="YR">Years</option>
													<option value="CCM">Cubic centimeter</option>
													<option value="KA">Kiloampere</option>
													<option value="M%O">Permille mass</option>
													<option value="BQK">Becquerel/kilogram</option>
													<option value="REL">REEL</option>
													<option value="KJ">Kilojoule</option>
													<option value="CD3">Cubic decimeter</option>
													<option value="KG">Kilogram</option>
													<option value="MPZ">Meterpascal/second</option>
													<option value="BAL">BALES</option>
													<option value="MPS">Millipascal seconds</option>
													<option value="MPT">Mass parts per trillion</option>
													<option value="KM">Kilometer</option>
													<option value="BAG">Bag</option>
													<option value="KW">Kilowatt</option>
													<option value="KT">Kilotonne</option>
													<option value="KV">Kilovolt</option>
													<option value="BRL">Barrel</option>
													<option value="MPG">Miles per gallon (US)</option>
													<option value="LB">US pound</option>
													<option value="GPM">Gallons per mile (US)</option>
													<option value="PCS">Pieces</option>
													<option value="KAI">Kilogram act. ingrd.</option>
													<option value="JCN">JERRICANS</option>
													<option value="MPL">Millimol per liter</option>
													<option value="BAR">bar</option>
													<option value="M%">Percent mass</option>
													<option value="TOM">Ton/Cubic meter</option>
													<option value="MPM">Mass parts per million</option>
													<option value="TON">US ton</option>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Refer No:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="refNo" id="id_referNo"
													class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Product Code/SKU:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="productCode" id="id_productCode"
													class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Project No/Article No:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="projectNo" id="id_projectNo"
													class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Size:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="sizeNo" id="id_sizeNo"
													class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Color:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="color" id="id_color"
													class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- class="col-md-4" -->
						<div class="col-md-8">
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-3 col-lg-3 col-sm-6 col-xs-6">
												<div class="">
													<span>Commodity:</span><span class="required-alone">*</span>
												</div>
											</div>
											<div class="col-md-9 col-lg-9 col-sm-6 col-xs-6">
												<!-- <input type="text" name="material" class="form-control"/> -->
												<select name="material" id="id_material" class="form-control">
													<!-- class="form-control color-change auto-complete selectpicker"
													data-live-search="true" required> -->
													<!-- <option value="" selected="true">--Please Select--</option> -->
													<%
														try {
													%>
													<c:forEach var="materials" items="${materials }">
														<%-- <c:choose>
															<c:when test="${materials.value == ${directBookingParams.  }">
																<option value="${materials.value }" selected>${materials.label}</option>
															</c:when>
															<c:otherwise>
																<option value="${materials.value }">${materials.label}</option>
															</c:otherwise>
														</c:choose> --%>
														<option value="${materials.value }" >${materials.label}</option>
													</c:forEach>
													<%
														} catch (Exception e) {
															e.printStackTrace();
														}
													%>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- edit end -->
					</div>
				</div>
				<div class="row row-no-margin-331932">
					<div class="col-md-12">
						<span class="booking-update-modal-info-header">Comm.
							Invoice Details</span>
						<hr class="hr-update-modal">
						<div class="col-md-4">
							<div class="forms">
								<div class="form-group   required">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="">Pay Conday Cond.:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="payCond" id="id_payCond"
													class="form-control auto-complete color-change" required />
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="forms">
								<div class="form-group required">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="">Unit Price/Pcs:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="unitPrice" id="id_unitPrice"
													onkeypress="return validateDecimalDataTypeStrict(this,event);"
													class="form-control auto-complete color-change" required />
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="forms">
								<div class="form-group required">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="">Currency Unit:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="currency" id="id_currency"
													class="form-control auto-complete color-change" required />
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row row-no-margin-331932">
					<div class="col-md-12">
						<span class="booking-update-modal-info-header">Item Extra</span>
						<hr class="hr-update-modal">
						<div class="col-md-4">
							<div class="forms">
								<div class="form-group required">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="">Comm. Invoice No.:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="commInvoice" id="id_commInvoice"
													class="form-control auto-complete color-change" required />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class=" required">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="control-label">GWeight/Carton:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="grossWeightPerCarton"
													onkeypress="return validateDecimalDataTypeStrict(this,event);"
													id="id_grossWeightPerCarton" class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class=" required">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="control-label">Net Cost:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="netCost" id="id_netCost"
													onkeypress="return validateDecimalDataTypeStrict(this,event);"
													class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group required">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="">QC Date:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="qcDate" id="id_qcDate"
													class="date-picker form-control auto-complete color-change"
													required />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group required">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="">Release Date:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="releaseDate" id="id_releaseDate"
													class="date-picker form-control auto-complete color-change"
													required />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group required">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="">Shipping Mark:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<textarea name="shippingMark" id="id_shippingMark"
													class="textarea color-change" required></textarea>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="forms">
								<div class="form-group">
									<div class=" required">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="control-label">Comm. Inv. Date:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="commInvoiceDate"
													id="id_commInvoiceDate" class="form-control date-picker" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class=" required">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="control-label">Net Weight/Carton:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="netWeightPerCarton"
													onkeypress="return validateDecimalDataTypeStrict(this,event);"
													id="id_netWeightPerCarton" class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class=" required">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="control-label">Reference#1:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="reference1" id="id_reference1"
													class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class=" required">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="control-label">Reference#3:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="reference3" id="id_reference3"
													class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group required">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="">Description of Goods:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<textarea name="description" id="id_description"
													class="textarea"></textarea>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="forms">
								<div class="form-group">
									<div class=" required">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="control-label">Carton Serial No.:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="cartonSerNo" id="id_cartonSerNo"
													class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class=" required">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="control-label">Pieces/Carton:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="pcsPerCarton" id="id_pcsPerCarton"
													onkeypress="return validateDataType(this,event);"
													class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class=" required">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="control-label">Reference#2:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="reference2" id="id_reference2"
													class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class=" required">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="control-label">Reference#4:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="reference4" id="id_reference4"
													class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Item Description:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<textarea name="itemDescription" id="id_itemDescription"
													class="textarea"></textarea>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-offset-10 col-md-1 col-xs-6 col-sm-6">
					</div>
					<div class="col-md-1 col-xs-6 col-sm-6" style="margin-left: -20px;">
						<button type="button" class="btn btn-danger btn-block" id="cancel-item"
							data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	if (loginDetails.divisionSelected == "SE"
			&& loginDetails.disChnlSelected == "EX") {
		$("#bookingUpdateHeader").text("SEA EXPORT BOOKING UPDATE");
		$("#bookingUpdateModalHeader").text(
				"SEA EXPORT BOOKING : EDIT LINE ITEM");
		$("#bookingUpdateAddItemModalHeader").text(
				"SEA EXPORT BOOKING : ADD LINE ITEM");
	} else if (loginDetails.divisionSelected == "AR"
			&& loginDetails.disChnlSelected == "EX") {
		$("#bookingUpdateHeader").text("AIR EXPORT BOOKING UPDATE");
		$("#bookingUpdateModalHeader").text(
				"AIR EXPORT BOOKING : EDIT LINE ITEM");
		$("#bookingUpdateAddItemModalHeader").text(
				"AIR EXPORT BOOKING : ADD LINE ITEM");
	}
	$(function () {
	    //Initialize Select2 Elements
	    $('.select2').select2();
	});
	$(document).ready(function() {		
		$("#btn-finish-update").click(function() {
			if (validateHeaderMandatory()) {
				methodCallBookingUpdate();
			}
			else {
				alert("Cannot process booking update");
			}
		});
		var lines = 5;
		var linesUsed = $('#linesUsed'); 
	    $('#description').keydown(function(e) {
	        
	        newLines = $(this).val().split("\n").length;
	        linesUsed.text(newLines);
	        
	        if(e.keyCode == 13 && newLines >= lines) {
	            linesUsed.css('color', 'red');
	            return false;
	        }
	        else {
	            linesUsed.css('color', '');
	        }
	    });
	});
</script>

<%@include file="footer_v2.jsp"%>