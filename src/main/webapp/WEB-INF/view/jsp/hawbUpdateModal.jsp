<!-- Update Modal -->
<div class="modal fade" id="hawbUpdateFormModal" role="dialog">
    <div class="modal-dialog" style="width: 90%">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>

                <span id="hawbUpdateModalHeader"></span>
            </div>
            <div class="modal-body">
                <div class="row row-no-margin-331932">
                    <div class="col-md-12">
                        <input type="hidden" name="itemNumber" id="id_itemNumber">
                        <span class="booking-update-modal-info-header">Item Details</span>
                        <hr class="hr-update-modal">
                        <div class="col-md-4">
                            <div class="forms" id="formUpdate">
                                <div class="form-group required">
                                    <div class="">
                                        <div class="row">
                                            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                                <div class="">
                                                    <span class="">HBL/HAWB:</span>
                                                    <span class="required-alone">*</span>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                                <input type="text" class="form-control" value="HBL123456789" readonly/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="forms"></div>
                            <div class="form-group">
                                <div class="">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                            <div class="">
                                                <span>Origin:</span>
                                                <span class="required-alone">*</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                            <input type="text" class="form-control color-change" value="Dhaka" /readonly>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="forms">
                            <div class="form-group">
                                <div class="">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                            <div class="">
                                                <span>Destination:</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                            <input type="text" class="form-control" value="Germany" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="forms">
                            <div class="form-group">
                                <div class="">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                            <div class="">
                                                <span>Gross Weight:</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                            <input type="text" class="form-control" value="550" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="forms">
                            <div class="form-group">
                                <div class="">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                            <div class="">
                                                <span>Volume:</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                            <input type="text" class="form-control" value="60" readonly/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="forms">
                            <div class="form-group">
                                <div class="">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                            <div class="">
                                                <span>Chargeable Weight:</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                            <input type="text" class="form-control" value="80" readonly/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="forms">
                            <div class="form-group">
                                <div class="">
                                    <div class="row">
                                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                            <div class="">
                                                <span>Actual Receiver:</span>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
                                            <input type="text" class="form-control" value="Poland" readonly/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row row-no-margin-331932">

                <div class="col-md-offset-10 col-md-1 col-xs-6 col-sm-6">
                    <input type="submit" class="btn btn-primary" value="Save" id="addItem" style="display: none;" onclick="saveLineItem('this')">
                    <input type="submit" class="btn btn-primary btn-block" onclick="submit_by_id()" value="Update" id="update-2">
                </div>
                <div class="col-md-1 col-xs-6 col-sm-6" style="margin-left: -20px;">
                    <button type="button" class="btn btn-danger btn-block" id="cancel-item" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>