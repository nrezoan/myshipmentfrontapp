<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<style>
html {
	background-color: #fff;
}
</style>

	<section class="main-body">

		<section>
			<section class="content-header" style="display: display;">
				<h1>Cargo Acknowledgement Certificate</h1>
				<ol class="breadcrumb" style="position: unset; float: none;">
					<li><i class="fa fa-dashboard"></i>Report Management</li>
					<li class="active">Cargo Acknowledgement Certificate</li>
				</ol>
			</section>
			<hr>

		</section>
	</section>
<c:if test="${empty cargoAckJson.itHeaderList }">
	<div class="row">
		<div class="col-md-4"></div>
		<div class="col-md-4">
			<div class="text-center"
				style="height: 50px; color: red; font-weight: bold; margin-top: 10%;">
				No Record found!</div>
			<div class="text-center" style="">
				<a class="btn btn-primary"
					href="${pageContext.request.contextPath}/getCargoAckPage"
					class="">Search Again</a>
			</div>
			<br>
			<br>
			<br>
			<br>
			<br>
		</div>
		<div class="col-md-4"></div>

	</div>
</c:if>
<c:if test="${ not empty cargoAckJson.itHeaderList }">
	<section>
		<div class="container">
			<div style="float: right;">
				<button type="button" class="btn btn-default no-print"
					style="background-color: #5cb85c;" id="print"
					onclick="printContent('print-con')">Print</button>
				<!--   <button type="button" class="btn btn-primary no-print" id="pdf">PDF</button> -->
			</div>
		</div>
	</section>
	<div class="container-fluid" style="margin-left: 2%;">

		<section class="main_body_wraper1" id="print-con"
			style="margin-left: 10px;">
			<div class="row nopadding">
				<div class="col-xs-6">
					<div class="row">
						<div class="col-xs-3 nopadding shipper_info">
							<h6>FCR No.</h6>
							<h6>Shipper</h6>
							<h6>Com Inv No.</h6>
							<h6>Total QTY.</h6>
							<h6>FCR Date</h6>
						</div>
						<div class="col-xs-6 nopadding">
							<h6>: ${cargoAckJson.itHeaderList[0].zzHblHawbNo }</h6>
							<h6>: ${cargoAckJson.itHeaderList[0].name1 }</h6>
							<h6>: ${cargoAckJson.itHeaderList[0].zzCommInvNo }</h6>
							<h6>: ${cargoAckJson.itHeaderList[0].zzSoQuantity }</h6>
							<h6>
								:
								<fmt:formatDate pattern="yyyy-MM-dd" type="date"
									value="${cargoAckJson.itHeaderList[0].buDat }" />
							</h6>
						</div>

					</div>
				</div>
				<div class="col-xs-6"></div>
			</div>
			<div class="col-xs-12 row nopadding">
				<h4 style="padding: 1% 0%;">This is to certify that the
					following orders have been received from the above shipper.</h4>
			</div>

			<div class="col-xs-12 row table_wraper">
				<div class="table-responsive">
					<table class="table">
						<thead>
							<tr>
								<th>Sr. No.</th>
								<th>Order No</th>
								<th>Carton Quantity</th>
							</tr>
						</thead>
						<tbody>
							<c:forEach items="${cargoAckJson.itItemList}" var="item">
								<tr>
									<td>${item.poSnr}</td>
									<td>${item.zzPoNumber}</td>
									<td>${item.menge}</td>
								</tr>
							</c:forEach>

						</tbody>
					</table>
				</div>

			</div>
			<div class="col-xs-6 row nopadding">
				<h6>${cargoAckJson.itAddList[0].name1}</h6>
				<h6>${cargoAckJson.itAddList[0].name2}</h6>
				<h6>${cargoAckJson.itAddList[0].name3}</h6>
				<h6>${cargoAckJson.itAddList[0].name4}</h6>
			</div>

		</section>
















	</div>

</c:if>






<!-- jquery
		============================================ -->
<script src="js/vendor/jquery-1.11.3.min.js"></script>
<!-- bootstrap JS
		============================================ -->
<script src="js/bootstrap.min.js"></script>
<!-- plugins JS
		============================================ -->
<script src="js/plugins.js"></script>
<!-- main JS
		============================================ -->

<script src="js/canvasjsmin.js"></script>
<script src="js/main.js"></script>

<script>
	function printContent(el) {
		/* var restorepage = $('body').html();
		var printcontent = $('#' + el).clone();
		$('body').empty().html(printcontent);
		window.print();
		$('body').html(restorepage); */

		var printContents = document.getElementById(el).innerHTML;
		var originalContents = document.body.innerHTML;

		document.body.innerHTML = printContents;

		window.print();

		document.body.innerHTML = originalContents;
	}
</script>
<%@include file="footer_v2_fixed.jsp"%>
