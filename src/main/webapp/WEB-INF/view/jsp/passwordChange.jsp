<%@include file="header.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script type="text/javascript">
	
</script>

<section class="main-body">
	<div class="well assign-po-search">
		<form:form action="setNewPassword" method="post"
			commandName="editparam">
			<fieldset>
                <legend style="color: green; margin-top: 2px;font-size: 140%; text-align:center">${message}</legend>
				<div class='row'>
					<div class="col-md-4"></div>
					<div class="col-md-4 well">
						<div class="col-md-12">
							<legend>Password Change</legend>
						</div>
						<div class='col-md-12'>
							<div class='form-group'>
								<label>Old Password</label>
								<form:input path="password" id="pass" cssClass="form-control"
									type="password" />
							</div>
						</div>
						<div class='col-md-12'>
							<div class='form-group'>
								<label>New Password</label>
								<form:input path="newPassword" id="pass1"
									cssClass="form-control" type="password" />
							</div>
						</div>
						<div class='col-md-12'>
							<div class='form-group'>
								<label>Verify Password</label>
								<form:input path="verifyPassword" id="pass2"
									cssClass="form-control" type="password" />
							</div>
						</div>
						<div class='col-md-12'>
							<div class='form-group'>
								<label style="display: block; color: #f5f5f5">. </label>
								<!-- added for alignment purpose, dont delete -->
								<input type="submit" id="btn-appr-po-search"
									class="btn btn-success btn-lg form-control" value="Submit"
									onclick="return Validate()" />
							</div>
						</div>
					</div>
		<!-- 			<div class='row'>
						<div class='col-md-12'>
							<div class='form-group'>
								
							</div>
						</div>
					</div> -->
					<div class="col-md-4"></div>








				</div>


			</fieldset>
		</form:form>
	</div>
</section>

<!-- ----------------------------- -->
<%@include file="footer.jsp"%>


<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/passwordChange.js"></script>