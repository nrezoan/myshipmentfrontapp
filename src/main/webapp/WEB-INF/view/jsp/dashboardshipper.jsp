<jsp:include page="header.jsp"></jsp:include>
<script>
/*$(window).bind("load", function() {
	   alert("page is loaded successfully");
	});*/
	var contextPath=<%=request.getContextPath() %>
</script>
		<section class="main-body">
		<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
    <div class="col-sm-6">
    	
    </div>
    	<!--  <div class='col-sm-3 dash-ship-frm-div' style="">
                       
                       <label class="dash-ship-frm-lbl">From Date</label>-->
                       <input id="dashboard_from_date" type="hidden" class="date-picker glyphicon glyphicon-calendar dashboard-ship-frm-date" style="width: 160px;"/>
                  
       <!--    </div>
         <div class='col-sm-3 dash-ship-to-div' style="">
                 
                   <label class="dash-ship-to-lbl">To Date</label>-->
                   <input id="dashboard_to_date" type="hidden" class="date-picker glyphicon glyphicon-calendar dashboard-ship-to-date" style="width: 160px;"/>
                   <!--  <span style="display: inline;"><a href="${pageContext.request.contextPath}/download/excel/dashboard/dashboard-excel(Shipper).xlsx" title="Export Dashboard data to excel"><img class="excel-export" src="${pageContext.request.contextPath}/resources/images/excel.png"/></a> </span> --> 
         <!-- </div>-->
           
    </div>
  </div><!--/row-->
  <div class="row">
   
    
    
	 <div class="col-md-6" style="margin-top: 0%;">
        <div class="panel panel-default" style="margin-left:10px">
    <div class="panel-heading" style="background-color: #222534;color: white;font-weight: bold;">Quick Stats</div>
  <div class="panel-body">
		<div class="col-lg-4 col-md-6 btn-primary" style="border-radius: 25px;margin-top:38px;margin-bottom:38px;margin-left:17px;">
    <div class=" panel-green rectangle ">
        <div class="panel-heading">
            <div class="row">
                
                <div class="col-xs-9 text-center">
                    <div class="huge" id="total-shipment" style="font-size:30px;font-align:center;"></div>
                    <div>
					<a href="#total-shipment" class="link-color" id="total-shipment">Total Shipment</a>
					</div>
                </div>
            </div>
        </div>
      
    </div>
</div>
<div class="col-lg-4 col-md-6 btn-success" style="border-radius: 25px;margin-top:38px;margin-bottom:38px;">
    <div class=" panel-green rectangle ">
        <div class="panel-heading">
            <div class="row rectangle ">
                
                <div class="col-xs-9 text-center">
                    <div class="huge" id="total-cbm" style="font-size:30px;font-align:center;"></div>
                    <div>
					<a href="#total-cbm" class="link-color" id="total-cbm">Total CBM</a>
					</div>
                </div>
            </div>
        </div>
      
    </div>
</div>
<div class="col-lg-4 col-md-6 btn-warning" style="border-radius: 25px;margin-top:38px;margin-bottom:38px;">
    <div class=" panel-green rectangle">
        <div class="panel-heading">
            <div class="row">
                
                <div class="col-xs-9 text-center">
                    <div class="huge" id="total-gwt" style="font-size:30px;font-align:center;"></div>
                    <div>
					<a href="#total-gwt" class="link-color" id="total-gwt">Total GWT</a>
					</div>
                </div>
            </div>
        </div>
      
    </div>
</div>
</div></div>
    </div>
    
     <div class="col-md-6">
    <div class="panel panel-default" style="margin-left:10px">
    <div class="panel-heading" style="background-color: #222534;color: white;font-weight: bold;">Top 5 Destinations</div>
  <div class="panel-body">
   
   
    <div class="col-sm-12">
      <table class="table table-striped" id="tbl-top-shipment">
		  <thead style="">
      <tr>
        <th>Port of Discharge</th>
        <th>Total Shipment</th>
        <th>Total CBM</th>
		<th>Total Gross Weight</th>
		
      </tr>
    </thead>
    <tbody>
      
    </tbody>
  </table>
    </div>
    </div>
    </div>
    </div>
   
   
    
   
  </div>
</div>

<!-- Recent Shipments -->

<div class="container-fluid">
  <div class="row">
  <div class="panel panel-default" style="margin-left:10px">
    <div class="panel-heading" style="background-color: #222534;color: white;font-weight: bold;">Recent Shipments</div>
  <div class="panel-body">
    
    
  
    
   <div class="table-responsive">
    <div class="col-md-12">
      <table class="table table-striped" id="tbl-recent-shipment">
		  <thead style="background-color:#222534;color:#fff">
      <tr>
        <th>HBL NO</th>
        <th>Comm. Inv</th>
        <th>POL</th>
		<th>POD</th>
        <th>Fvsl Name</th>
		<th>Fvsl ETD</th>
		<th>Fvsl ETA</th>
		<th>Mvsl Name</th>
		<th>Mvsl ETD</th>
		<th>Mvsl ETA</th>
		</tr>
    </thead>
    <tbody>
      
    </tbody>
  </table>
    </div>
    </div>
	<!--  <div class="col-md-4">
        
	
<div id="chartContainer" style="width: 100%; height: 300px"></div>
    </div> -->
    </div>
    </div>
  </div>
</div>
<!-- Top 5 Shipments -->

<div class="container-fluid">
  <div class="row">
	 <div class="col-md-6">
    <div class="panel panel-default" style="margin-left:10px">
    <div class="panel-heading" style="background-color: #222534;color: white;font-weight: bold;">Monthly Shipment Statistics</div>
  <div class="panel-body">
        <div id="bar-example" style="height: 206px;"></div>
        </div>
        </div>
        
    </div>
    
    <div class="col-md-6">
    <div class="panel panel-default" style="margin-left:">
    <div class="panel-heading" style="background-color: #222534;color: white;font-weight: bold;">BL Status</div>
  <div class="panel-body">
	 <div class="col-sm-12">
        
	
		<div id="chartContainer" style="width: 100%; height: 206px"></div>
    </div>
    </div>
    </div>
    </div>
    
  </div>
</div>






		</section>
		<jsp:include page="footer.jsp"></jsp:include>
	<%-- 	<script type="text/javascript" src="${pageContext.request.contextPath}\resources\js\canvasjsmin.js"></script> --%>
		<script type="text/javascript" src="${pageContext.request.contextPath}\resources\js\dashboardshipper.js"></script>
		<script>
		var loginDto=${loginDto};
		var sowiseShipmentSummary = ${sowiseShipmentSummary};
		var topFiveShipment = ${topFiveShipment};
		var lastNShipmentsJsonOutputData = ${lastNShipmentsJsonOutputData};
	 $(document).ready( 
                function () {
                	$.LoadingOverlay("show");
                	var loginDTO=loginDto;
                	
                	  if(loginDTO!=undefined)
                 	{
                 	  $("#dashboard_to_date").datepicker({ dateFormat: "dd-mm-yy"}).datepicker("setDate",loginDTO.dashBoardToDate);
                 	  $("#dashboard_from_date").datepicker({ dateFormat: "dd-mm-yy"}).datepicker("setDate",loginDTO.dashBoardFromDate);
                 	}
                 else
                 	{
                 	 $("#dashboard_to_date").datepicker({ dateFormat: "dd-mm-yy"}).datepicker("setDate", new Date());
                		 var currentDate=new Date();
                		 currentDate.setMonth(currentDate.getMonth()-2);
                		 $("#dashboard_from_date").datepicker({ dateFormat: "dd-mm-yy"}).datepicker("setDate", currentDate);
                 	} 
                 	
                	
                	var fromDate=$("#dashboard_from_date").val();
                	var toDate=$("#dashboard_to_date").val();
                	
					 drawChart(loginDTO.dashBoardFromDate,loginDTO.dashBoardToDate);
                    $("#dashboard_from_date").change(function(){
                    	var fromDate=$("#dashboard_from_date").val();
                    	var toDate=$("#dashboard_to_date").val();
                    
                    	drawChart(fromDate,toDate);
                    })
                     $("#dashboard_to_date").change(function(){
                    	var fromDate=$("#dashboard_from_date").val();
                    	var toDate=$("#dashboard_to_date").val();
                    
                    	drawChart(fromDate,toDate);
                    })
                    
                    $('a[href="#total-shipment"]').click(function(){
                    	var fromDate=$("#dashboard_from_date").val();
                    	var toDate=$("#dashboard_to_date").val();
                    	var url=myContextPath+'/buyerwiseshipdetails?fromDate='+fromDate+'&toDate='+toDate;
                    	window.location.assign(url);
                    })
                     $('a[href="#total-cbm"]').click(function(){
                    	var fromDate=$("#dashboard_from_date").val();
                    	var toDate=$("#dashboard_to_date").val();
                    	var url=myContextPath+'/buyerwisecbmdetails?fromDate='+fromDate+'&toDate='+toDate;
                    	window.location.assign(url);
                    })
                    $('a[href="#total-gwt"]').click(function(){
                    	var fromDate=$("#dashboard_from_date").val();
                    	var toDate=$("#dashboard_to_date").val();
                    	var url=myContextPath+'/buyerwisegwtdetails?fromDate='+fromDate+'&toDate='+toDate;
                    	window.location.assign(url);
                    })
                    $('a[href="#pod"]').click(function(){
                    	var fromDate=$("#dashboard_from_date").val();
                    	var toDate=$("#dashboard_to_date").val();
                    	var pod=this;
                    	var url=myContextPath+'/buyerwisecbmdetails?fromDate='+fromDate+'&toDate='+toDate;
                    	window.location.assign(url);
                    })
                    $.LoadingOverlay("hide");  
                   
                    
                }
            );	
			</script>	
