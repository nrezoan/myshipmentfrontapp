<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">

	

<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/poTracking.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/newoverlay.js"></script>

<style>
	html {
		background-color: #f5f5f5;
	}
	body {
		background-color: #f5f5f5;
	}
	
	tbody td {
		font-size: 14px;
	}
</style>
	



<section class="main-body">
	<div class="well assign-po-search">
		<form:form action="getCommInvTrackingDetail" method="post"
			commandName="commInvTrackingParams" id="poTrack">
			<fieldset>
				<legend>Commercial Invoice Tracking</legend>
				<div class='row'>

					<div class='col-sm-2'>
						<div class='form-group'>
							<label>Comm. Invoice Number</label>
							<form:input path="commInvNo" id="commInvNo" cssClass="form-control" />
						</div>
					</div>
					<div class='col-sm-2'>
						<div class='form-group'>
							<label>From Date</label>
							<form:input path="fromDate" id="fromDate"
								class="date-picker form-control glyphicon glyphicon-calendar" />
						</div>
					</div>
					<div class='col-sm-2'>
						<div class='form-group'>
							<label>TO Date</label>
							<form:input path="toDate" id="toDate"
								class="date-picker form-control glyphicon glyphicon-calendar" />
						</div>
					</div>

					<div class='col-sm-2'>
						<div class='form-group'>
							<label>Status Type</label>
							<form:select path="statusType" id="sType" cssClass="form-control">
								<form:option value="NONE">--- Select ---</form:option>
								<form:options items="${statusTypes}" />
							</form:select>
						</div>
					</div>
					<!--  
					<div class='col-sm-2' style="margin-top: 2%;">
						<div class='form-group'>

							<button type="submit" class="btn btn-success btn-lg form-control">Search</button>
						</div>
					</div>
					-->
					<div class='col-sm-2'>
                  <div class='form-group'>
                   <label style="display:block;color:#f5f5f5">. </label> <!-- added for alignment purpose, dont delete -->       
                   <input type="submit" id="btn-appr-po-search"class="btn btn-success form-control" value="Search"/>
                 </div>
                 </div>
                 
				</div>

			</fieldset>
		</form:form>
	</div>



	<c:if test="${ not empty poTrackingResultBeanLst}">
	<div class="container-fluid table-responsive">
		
		<div class="row search-result-datatable">
			
				<table class="table table-striped" >
					<thead style="background-color:#222534;color:#fff;">
						<tr>

							<th>Comm. Invoice No</th>
							<th>Po No</th>
							<th>Goods Receive Date</th>
							<th>Shipment date</th>
							<th>PO Status</th>
							<th>Exception Status</th>
							
						</tr>
					</thead>
					<tbody id="accordion">

						<c:forEach items="${poTrackingResultBeanLst}" var="i" varStatus="index">
							<tr class="">
								<td style="font-size:12px;">${i.comm_invoice_no} <a data-toggle="collapse" href="#${index.count}"  onclick="hideexpand(this);"><span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a></td>
								<td style="font-size:12px;">${i.po_no}</td>
								<fmt:parseDate value="${i.gr_date}" var="grDate" pattern="yyyyMMdd" />
								<td style="font-size:12px;"><fmt:formatDate pattern="yyyy-MM-dd" type="date" value="${grDate}" /></td>
								<fmt:parseDate value="${i.shipment_date}" var="shipmentDate" pattern="yyyyMMdd" />
								<td style="font-size:12px;"><fmt:formatDate pattern="yyyy-MM-dd" type="date" value="${shipmentDate}" /></td>
								<td style="font-size:12px;">${i.poStatus}</td>
								<td style="font-size:12px;">${i.shipmentStatus}</td>
								
							</tr>
							
							<tr class="hide">
	 <td colspan="7">
	 <div class="collapse" id="${index.count}">
	 <table class="table table-striped" style="width:80%;margin-left: 8%;">
	 	<th>HBL/AWB No</th>
	 	<th>Shipper</th>
	 	<th>Consignee</th>
	 	<th>Delivery Agent</th>
	 	<th>Carrier</th>
	 	
	 	<tr >
        	<td style="font-size:12px;"><a href="getTrackingInfoFrmUrl?searchString=${i.bl_no}">${i.bl_no}</a></td>
        	<td style="font-size:12px;" >${i.shipper_name}</td>
        	<td style="font-size:12px;">${i.buyer_name}</td>
			<td style="font-size:12px;">${i.agent_name}</td>
			<td style="font-size:12px;">${i.carrier_name}</td>
			
		</tr>
		<th>Comm. Invoice No</th>
	 	<th>POL</th>
	 	<th>POD</th>
	 	<th>Total Pcs.</th>
	 	<th>Gross Wt.</th>
	 	
		 <tr>
        	<td style="font-size:12px;">${i.comm_invoice_no}</td>
        	<td style="font-size:12px;">${i.pol_name}</td>
        	<td style="font-size:12px;">${i.pod_name}</td>
			<td style="font-size:12px;">${i.tot_pcs}</td>
			<td style="font-size:12px;">${i.gross_wt}</td>
			
		</tr>
		
		<th>Booking Date</th>
	 	<th>ETD</th>
	 	<th>ATD</th>
	 	<th>ETA</th>
	 	<th>ATA</th>
	 	
		 <tr>
		    <fmt:parseDate value="${i.booking_date}" var="bookingDate" pattern="yyyyMMdd" />
        	<td style="font-size:12px;"><fmt:formatDate pattern="yyyy-MM-dd" type="date" value="${bookingDate }"/></td>
        	<td style="font-size:12px;"><fmt:formatDate pattern="yyyy-MM-dd" type="date" value="${i.etd}"/></td>
        	<td style="font-size:12px;"><fmt:formatDate pattern="yyyy-MM-dd" type="date" value="${i.atd}"/></td>
			<td style="font-size:12px;"><fmt:formatDate pattern="yyyy-MM-dd" type="date" value="${i.eta}"/></td>
			<td style="font-size:12px;"><fmt:formatDate pattern="yyyy-MM-dd" type="date" value="${i.ata}"/></td>
			
		</tr>
		</table>
		</div>
		</td>
	 </tr>
						</c:forEach>


					</tbody>
				</table>
			
		</div>

	</div>
</c:if>


</section>

<%@include file="footer_v2_fixed.jsp"%>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/buttons.flash.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jszip.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/pdfmake.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/vfs_fonts.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/buttons.html5.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/buttons.print.min.js"></script>


<script>
	$(document).ready(function() {
		$("#toDate").datepicker({
			autoclose : true,
			dateFormat : "dd-mm-yy"
		});
		//.datepicker("setDate", new Date());
		$("#fromDate").datepicker({
			autoclose : true,
			dateFormat : "dd-mm-yy"
		});
		//.datepicker("setDate", new Date());
	});
	$('#po_tables').DataTable({
		responsive : true,
		dom : 'Bfrtip',
		buttons : ['csv','excel' 
		            
		            
		            ]
	});
	
	function hideexpand(varobj){
		   var res=varobj.href.split("#")
		   //alert(res[1]);
		   var $myGroup = $('#accordion');
		   $($myGroup).find('.collapse').each(function(){
		   //alert($(this).attr('id'));
		   if(res[1]==$(this).attr('id')){
		  // alert("dd");
		  $(this).closest("tr").toggleClass("hide");
		    $(this).toggle();
		   }
		   else{
		   //alert($(this).attr('id'))
		   //$(this).slideUp( "slow", "linear" )
		    $(this).closest("tr").addClass("hide");
		   $(this).hide();
		   }    
		    });

		        
		}
</script>


