<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css">

<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/css/bootstrap-select.min.css" />
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/js/bootstrap-select.min.js"></script>

<style>
	html {
		background-color: #f5f5f5;
	}
</style>

<section class="main-body">
	<div class="well assign-po-search">
		<form:form action="approvebookedporeportdetail" method="post" commandName="sapposearchparams">
			<fieldset>
				<legend>Booked PO Approval Report</legend>
				<div class='row'>
					<div class='col-sm-2'>
						<div class='form-group'>
							<label>From Date</label>
							<form:input path="fromDate" id="fromDate" class="date-picker form-control glyphicon glyphicon-calendar" required="true"/>
						</div>
					</div>
					<div class='col-sm-2'>
						<div class='form-group'>
							<label>To Date</label>
							<form:input path="toDate" id="toDate" class="date-picker form-control glyphicon glyphicon-calendar" required="true"/>
						</div>
					</div>
					<div class='col-sm-2'>
						<div class='form-group'>
							<label>Buyer</label>
							<form:select path="buyer" id="buyer" class="form-control" required="true">
								<%-- <form:option value="None">Please Select</form:option> --%>
								<form:options items="${buyersList}" />
							</form:select>
						</div>
					</div>
					<div class='col-sm-2'>
						<div class='form-group'>
							<label>Status Type</label>
							<form:select path="status" id="status" cssClass="form-control">
								<%-- <form:option value="None">Select</form:option> --%>
								<%-- <form:options items="${statusTypes}" /> --%>
								<form:option value="APPROVED">APPROVED</form:option>
								<form:option value="REJECTED">REJECTED</form:option>
							</form:select>
						</div>
					</div>
					<div class='col-sm-2'>
						<div class='form-group'>
							<label style="display: block; color: #f5f5f5">. </label>
							<!-- added for alignment purpose, dont delete -->
							<input type="submit" class="btn btn-success form-control" value="Submit" />
						</div>
					</div>
				</div>
				<c:if test="${errorDialogue == 'FAILURE'}">
					<div class='row'>
						<div class='col-sm-4'>							
							<div class='form-group'>
								<label style="color: red;margin-top: 2px;">No Purchse Orders Found</label>								
							</div>				
						</div>
					</div>
				</c:if>

			</fieldset>
		</form:form>
	</div>
	
	<!-- RESULT PART OF ORACLE -->
	<c:if test="${fn:length(myshipTrackItemListOracle) > 0}">
	<c:set var="sap" value="${sapposearchparams}"></c:set>
	<c:set var="actionuser" value="${loggedinuser}"></c:set>
	<div class="container-fluid table-responsive">
		
		<div class="row" style="margin-left:1px;">
		<%-- <form:form action="shipmentdetaildatapostservlet" method="post" commandName="" id="myform">  --%>
			<div class="dataTable_wrapper">
			<table class="table table-striped table-hover display" id="adminTable">
				
				<thead style="background-color:#222534;color:#fff;">
					<tr>
						<th>HBL NO.</th>
						<th>PO NO.</th>						
						<th style="display: none;">SO NO.</th>
						<th style="display: none;">BL DATE</th>
						<th style="display: none;">ITEM NO.</th>
						<th style="display: none;">STYLE</th>
						<th style="display: none;">SIZE</th>
						<th style="display: none;">COLOR</th>
						<th style="display: none;">PIECES</th>
						<th style="display: none;">CARTON QTY.</th>
						<th style="display: none;">GROSS WT.</th>
						<th style="display: none;">VOLUME</th>
						<th style="display: none;">EXP. CH DT.</th>
						<th style="display: none;">BUYER NO.</th>
						<th style="display: none;">ACTION USER</th>
						<th style="display: none;">SHIPPER NO.</th>
						<th style="display: none;">BUYER NAME</th>
						<th style="display: none;">SHIPPER NAME</th>					
						<th>INDC DATE</th>
						<th>PO REQ. SHIP. DATE</th>
						<th>FVSL NAME</th>
						<th>FVSL ETD</th>
						<th>MVSL NAME</th>
						<th>MVSL ETA</th>
						<th>CARRIER</th>
						<th>STATUS</th>
						<th>REMARKS</th>
						<!-- <th>SELECT</th> -->							
					</tr>
				</thead>
				<tbody id="accordion">
				<c:forEach items="${myshipTrackItemListOracle}" var="mti" varStatus="index">
						<tr id="123" class="tableRow">
							<td class="blno" style="font-size:12px;">${mti.bl_no}</td>
							<td class="pono" style="font-size:12px;">${mti.po_no}</td>							
							<td class="sono" style="font-size:12px; display: none;">${mti.so_no}</td>
							<c:set var = "bldate" value="${mti.item_bl_dt}" />
							<td class="item_bl_dt" style="font-size:12px; display: none;"><fmt:formatDate pattern = "dd.MM.yyyy" value = "${bldate}" /></td>
							<td class="item_no" style="font-size:12px; display: none;">${mti.item_no}</td>
							<td class="style" style="font-size:12px; display: none;">${mti.style}</td>	
		 					<td class="size" style="font-size:12px; display: none;">${mti.size_no}</td>
		 					<td class="color" style="font-size:12px; display: none;">${mti.color}</td>				   
					        <td class="item_pcs" style="font-size:12px; display: none;">${mti.item_pcs}</td>
					        <td class="item_qty" style="font-size:12px; display: none;">${mti.item_qty}</td>
					        <td class="item_grwt" style="font-size:12px; display: none;">${mti.item_grwt}</td>
							<td class="item_vol" style="font-size:12px; display: none;">${mti.item_vol}</td>
							<c:set var = "chdate" value="${mti.item_chdt}" />
							<td class="item_chdt" style="font-size:12px; display: none;"><fmt:formatDate pattern = "dd.MM.yyyy" value = "${chdate}" /></td>
							<td class="buyerno" style="font-size:12px; display: none;">${sap.buyer}</td>
							<td class="actionuser" style="font-size:12px; display: none;">${actionuser}</td>							
							<td class="shipperno" style="font-size:12px; display: none;">${mti.shipper_no}</td>
							<td class="buyername" style="font-size:12px; display: none;">${mti.buyer_name}</td>
							<td class="shippername" style="font-size:12px; display: none;">${mti.shipper_name}</td>	
							<c:set var = "indcdate" value="${mti.indc_dt}" />							
							<td class="indc_dt" style="font-size:12px; "><fmt:formatDate pattern = "dd.MM.yyyy" value = "${indcdate}" /></td>
							<c:set var = "poreqdate" value="${mti.po_req_ship_dt}" />
							<td class="po_req_ship_dt" style="font-size:12px; "><fmt:formatDate pattern = "dd.MM.yyyy" value = "${poreqdate}" /></td>
							<td class="fvsl_name" style="font-size:12px; ">${mti.fvsl_name}</td>
							<c:set var = "fetd" value="${mti.fetd}" />
							<td class="fetd" style="font-size:12px; "><fmt:formatDate pattern = "dd.MM.yyyy" value = "${fetd}" /></td>
							<td class="mvsl_name" style="font-size:12px; ">${mti.mvsl_name}</td>
							<c:set var = "meta" value="${mti.meta}" />
							<td class="meta" style="font-size:12px; "><fmt:formatDate pattern = "dd.MM.yyyy" value = "${meta}" /></td>		
							<td class="carrier_name" style="font-size:12px; ">${mti.carrier_name}</td>
							<td class="status" style="font-size:12px; ">${mti.status}</td>
							<td class="remarks" style="font-size:12px; ">${mti.remarks}</td>
							<!-- <td style="text-align:center"><input type="checkbox" name="check1" class="checker" value="123"/></td>	 -->									
						</tr>
							
<%-- 						<tr class="hide" >
	 						<td colspan="12">
	 						<div class="collapse" id="${index.count}">
	 							<table class="table table-striped" style="width:80%;margin-left: 115px;">
	 								<tr style="background-color: #337ab7; color: white;">
									 	<th>Style</th>
									 	<th>Size</th>
									 	<th>Color</th>
									 	<th>Pieces</th>
									 	<th>Carton Qty.</th>
									 	<th>Gross Wt.</th>
									 	<th>Volume</th>
									 	<th>Exp. CH Dt.</th>
									</tr>	 	
		 							<tr>
		 								<td style="font-size:12px;">${mti.style}</td>	
		 								<td style="font-size:12px;">${mti.size_no}</td>
		 								<td style="font-size:12px;">${mti.color}</td>				   
					        			<td style="font-size:12px;">${mti.item_pcs}</td>
					        			<td style="font-size:12px;">${mti.item_qty}</td>
					        			<td style="font-size:12px;">${mti.item_grwt}</td>
										<td style="font-size:12px;">${mti.item_vol}</td>
										<c:set var = "chdate" value="${mti.item_chdt}" />
										<td style="font-size:12px;"><fmt:formatDate pattern = "dd.MM.yyyy" value = "${chdate}" /></td>					
									</tr>	
								</table>
							</div>
							</td>
	 					</tr> --%>
				</c:forEach>
				</tbody>
			</table>
			</div>
<%-- 			<div class='row'>
				<div class='col-sm-10'></div>
				<div class='col-sm-2'><input type="submit" class="btn btn-success btn-lg form-control" value="Save" /></div>
			</div>
			
			</form:form> --%>		
		</div>
	</div>
	</c:if>
</section>

<!-- ----------------------------- -->
<%@include file="footer_v2_fixed.jsp"%>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/buttons.flash.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jszip.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/pdfmake.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/vfs_fonts.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/buttons.html5.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/buttons.print.min.js"></script>

<script>
	$(document).ready(function() {

		$('.date-picker').each(function() {
		    $(this).removeClass('hasDatepicker').datepicker({
		        dateFormat: "dd.mm.yy"
		    });
		});		
	});
 	$('#adminTable').DataTable({
		responsive : true,
		dom : 'Bfrtip',
		buttons : ['csv','excel']
	});
	
/* 	function hideexpand(varobj){
		var res=varobj.href.split("#")
		//alert(res[1]);
		var $myGroup = $('#accordion');
		//var toggleicon = $myGroup.children('.collapsed');		   
		//alert(toggleicon);
		$($myGroup).find('.collapse').each(function(){
		if(res[1]==$(this).attr('id')){
			$(this).closest("tr").toggleClass("hide");
		    $(this).toggle();
		    
		    
 		//alert("entering");
 		//alert(document.getElementById("myicon"));
 		//document.getElementById("#"+res[1]).className = "glyphicon glyphicon-chevron-up";
		//alert(document.getElementById("myicon"));
			if(document.getElementById("#"+res[1]).className == "glyphicon glyphicon-chevron-down"){
				//alert("entering");
		    	//document.getElementById("#"+res[1]).removeClass();
		    	document.getElementById("#"+res[1]).className = "glyphicon glyphicon-chevron-up";
			}else{
				//document.getElementById("#"+res[1]).removeClass();
		    	document.getElementById("#"+res[1]).className = "glyphicon glyphicon-chevron-down";
			}
		}else{
		   //alert($(this).attr('id'))
		   //$(this).slideUp( "slow", "linear" )
		   //$(this).closest("tr").addClass("hide");
		   //$(this).hide();
		}
		//toggleicon.html("<span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>");
		});
	} */
	
</script>