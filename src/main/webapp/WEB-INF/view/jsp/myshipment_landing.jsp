<!--
Author: MGH Group
Author URL: www.mghgroup.com
-->
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="en">
<head>

<title>myshipment</title>
<!-- for-mobile-apps -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE-edge">
<meta name="keywords"
	content="Freightage Responsive web template, MGH Logistics, Logistics, Freight Forwarding, Freight, MGH, MGH Group, Track your shipment, myshipment, shipment, Transmarine Logistics Limited, MGH Logistics Limited, MLL, TML" />
<script type="application/x-javascript">
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } 

</script>
<script>
	var contextPath = '${pageContext.request.contextPath}';
</script>
<!-- //for-mobile-apps -->
<link
	href="${pageContext.request.contextPath}/resources/myshipment_landing/css/bootstrap.css"
	rel="stylesheet" type="text/css" media="all" />
<link
	href="${pageContext.request.contextPath}/resources/myshipment_landing/css/style.css"
	rel="stylesheet" type="text/css" media="all" />
<link
	href="${pageContext.request.contextPath}/resources/myshipment_landing/css/font-awesome.min.css"
	rel="stylesheet" type="text/css" media="all">

<link
	href="${pageContext.request.contextPath}/resources/myshipment_landing/css/getTheApp.css"
	rel="stylesheet" type="text/css" media="all">
<link
	href="${pageContext.request.contextPath}/resources/myshipment_landing/css/getTheAppresponsive.css"
	rel="stylesheet" type="text/css" media="all">
	
<link
	href="${pageContext.request.contextPath}/resources/myshipment_landing/css/snackbar.css"
	rel="stylesheet" type="text/css" media="all">

<!-- Animate CSS -->
<!-- <link href="css/animate/animate.css" rel="stylesheet" type="text/css" media="all"> -->
<%-- <link
	href="${pageContext.request.contextPath}/resources/myshipment_landing/css/animate/animate.css"
	rel="stylesheet"> --%>
	
<!-- animate CSS -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/myshipment_v2/animate/animate.css">

<!-- FONT LINK-->
<link href="https://fonts.googleapis.com/css?family=Quicksand"
	rel="stylesheet">
<!-- <link href="https://fonts.googleapis.com/css?family=Bubbler+One" rel="stylesheet"> -->
<!--web-fonts-->
<!--//web-fonts-->
<!--//fonts-->

<%-- <!-- wow js -->
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/myshipment_landing/js/wow.min.js"></script> --%>
</head>
<!-- data-spy="scroll" data-target=".navbar" data-offset="50" -->
<body data-spy="scroll" data-target=".navbar" data-offset="50">
	<div class="header navbar-fixed-top" data-spy="affix" data-offset="65">
		<nav class="navbar navbar-default">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
					data-target="#bs-example-navbar-collapse-1">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a href=""><img id="logo"
					class="myshipment-header-logo"
					src="${pageContext.request.contextPath}/resources/myshipment_landing/images/logo-xs.png"
					alt="myshipment"></a>
			</div>

			<!-- navbar-header -->
			<div class="collapse navbar-collapse"
				id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right" style="display: none;">
					<li><a class="hvr-underline-from-center scroll home-inactive-top" href="#home">Home</a></li>
					
					<li><a href="#concern"
						class="hvr-underline-from-center scroll">Concern</a></li>
					<li><a href="#solutions" class="hvr-underline-from-center scroll">Services</a></li>
					<li><a href="#faq"
						class="hvr-underline-from-center scroll scroll">FAQ</a></li>
					<li><a href="#app"
						class="hvr-underline-from-center scroll scroll">App</a></li>
					<li><a href="#contact"
						class="hvr-underline-from-center scroll scroll">Contact</a>
				</ul>
			</div>
			<div class="clearfix"></div>
		</nav>

	</div>
	<div class="banner-w3layouts translucent-home-bg blue" id="home">
		<!--Top-Bar-->

		<!--banner-->
		<br> <br> <br> <br>
		<!--Slider-->
		<div class="row w3l_banner_info">
			<div class="col-md-6 slider">
				<div class="callbacks_container">
					<ul class="rslides" id="slider3">
						<li>
							<div class="slider_banner_info text-center">
								<h4>Real Time</h4>
								<p>Vessels, Flights, Where&#39;s your PO<br><br></p>
							</div>

						</li>
						<li>
							<div class="slider_banner_info text-center">
								<h4>Faster-Cheaper</h4>
								<p>Find your fastest & most economic routes (Faster is no longer expensive<sup>TM</</sup>)</p>
							</div>

						</li>
						<li>
							<div class="slider_banner_info text-center">
								<h4>End to End</h4>
								<p>Delivering your last Miles (Stores, DCs, Offices)!</p>
							</div>

						</li>
						<li>
							<div class="slider_banner_info text-center">
								<h4>Embracing Changes</h4>
								<p>Staying Relevant to your changing Customer demands (or you don&#39;t need us)</p>
							</div>

						</li>
					</ul>
				</div>
			</div>
			<!-- LOGIN -->
			<div class="col-md-6 col-sm-12 col-xs-12 banner-form-agileinfo myshipment-login-track-media">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs nav-tabs-custom-login-track nav-justified">
						<li class="active"><a href="#login" data-toggle="tab">Login</a></li>
						<li><a href="#track" data-toggle="tab">Track</a></li>
						<!-- class="wow bounce animated" data-wow-iteration="infinite" data-wow-duration="800ms" -->
					</ul>
					<div class="tab-content">
						<div class="tab-pane active login-track-panel" id="login">
							<form:form action="${pageContext.request.contextPath}/selectedorg" modelAttribute="loginMdl" method="post" class="login-track-form">
								<h4 class="myshipmentLogin text-center">
									Log in to&nbsp;
									<strong class="my-login-panel">my<span class="shipment-login-panel">shipment</span></strong>
								</h4>
								<h5 class="errorMsg text-center" style="">${error}</h5>
								<input type="text" class="email" id="customerCode" name="customerCode"
									placeholder="Enter myshipment ID" required="" />
								<input type="password" class="password" id="password" name="password"
									placeholder="Password" required="" />
								<input type="submit" class="hvr-shutter-in-vertical" value="Login">
								<div class="row bottom-login-panel">
									<div class="col-md-6 col-sm-6 col-xs-6">
										<input type="checkbox" class="" value=""><span
											class="remember-me-login-panel remember-me-forgot-password-text">&nbsp;Remember Me</span>
									</div>
									<div class="col-md-6 col-sm-6 col-xs-6">
										<p class="remember-me-login-panel text-right">
											<a href="${pageContext.request.contextPath}/forgetPasswordPage" class="remember-me-forgot-password-text">Forgot password?</a>
										</p>
									</div>
								</div>

							</form:form>

						</div>
						<div class="tab-pane login-track-panel" id="track">
							<form:form action="${pageContext.request.contextPath}/getCommonTrackingDetailInfo" method="post" commandName="trackingRequestParam" class="login-track-form" >
								<h4 class="myshipmentLogin text-center">
									Track your&nbsp;
									<strong class="my-login-panel">
									<span class="shipment-login-panel">order</span></strong>
								</h4>
								<select name="action" class="form-control">
									<option value="01">Sea Export HBL</option>
									<option value="02">Sea Import HBL</option>
									<option value="03">Air Export HAWB</option>
									<option value="04">Air Import HAWB</option>
								</select> <input type="text" class="password" name="blNo"
									placeholder="Enter HBL/HAWB Number" required /> 
									<input type="submit" class="hvr-shutter-in-vertical" value="Track">


							</form:form>
						</div>

					</div>
				</div>

			</div>
		</div>
		<!--//Slider-->
		<div class="clearfix"></div>
	</div>

	<!--//banner-->

	<!-- welcome -->
	<div class="features" id="concern">
		<div class="container">
			<h2 class="title-w3 wow fadeIn" data-wow-delay="0.2s">Concern</h2>
			<h4 class="text-center wow fadeIn" data-wow-delay="0.2s">Quality our strongest fortitude</h4>
			<br>
			<div class="w3-agile-top-info">
				<div class="w3-welcome-grids">
					<div class="row">
						<div class="col-md-6 col-sm-6 why-fa wow fadeInUp" data-wow-delay="0.4s">
							<div class="col-md-12 text-center">
								<i class="fa fa-thumbs-up fa-3x" aria-hidden="true"></i>
							</div>
							<h3 class="text-center">Customer Satisfaction</h3>
							<p class="text-center">Delivering your load safe and on time
								is not our goal but our standard. Our customers can count on the
								same standard wherever they are, in terms of service quality,
								innovative and flexible solutions, transparency, and a smart and
								safe logistical process.</p>
						</div>

						<div class="col-md-6 col-sm-6 why-fa wow fadeInUp" data-wow-delay="0.4s">
							<div class="col-md-12 text-center">
								<i class="fa fa-trophy fa-3x" aria-hidden="true"></i>
							</div>
							<h3 class="text-center">Clientele Range</h3>
							<p class="text-center">Trusted by more than 20,000 shippers,
								buyers, and delivery agents, we are one of the leading online
								logistics management services. Expanding our reach in more than
								eight nations around the globe, we have evolved into the de
								facto expert in this industry.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- //welcome -->

	<!-- capabilities -->
	<div id="solutions">
		<div class="capabilities translucent-bg bg-image-1 blue"
			id="capabilities">
			<div class="container">
				<h2 class="title-w3 wow fadeIn" data-wow-delay="0.4s">Are we worth your energy?</h2>
				<div class="row">
					<div class="col-sm-6">
						<div class="media wow fadeInLeft" data-wow-delay="0.6s">
							<div class="media-body text-right">
								<h3 class="media-heading">Online Booking</h3>
								<p>Once you book with your entity, date of pick up, ETA goods
									arrivals, you are able to track your booking as well as seek
									approval of the order from your Buyer(s) prior dispatch.
								</p>
							</div>
							<div class="media-right">
								<i class="fa fa-bookmark"></i>
							</div>
						</div>
						<br> <br>
						<div class="media wow fadeInLeft" data-wow-delay="0.6s">
							<div class="media-body text-right">
								<h3 class="media-heading">PO Management</h3>
								<p>Your PO number is good enough to know about the whereabout or
									even plan arrival or receipt of yours goods from anywhere. Once
									you have entered your PO, it remains in the Master data base
									for you to use in the reports, and dashboards for your supply
									chain planning, and measurements.
								</p>
							</div>
							<div class="media-right">
								<i class="fa fa-first-order"></i>
							</div>
						</div>
					</div>
					<div class="space visible-xs"></div>
					<div class="col-sm-6">
						<div class="media wow fadeInRight" data-wow-delay="0.6s">
							<div class="media-left">
								<i class="fa fa-truck"></i>
							</div>
							<div class="media-body">
								<h3 class="media-heading">Track & Trace</h3>
								<p>Myshipment is secured by multiple layers of cyber
									security measures. You can retrieve the current status of your
									consignment simply by providing the MBL/HBL number, PO number
									or even by your Commercial Invoice number.
								</p>
							</div>
						</div>
						<br>
						<div class="media wow fadeInRight" data-wow-delay="0.6s">
							<div class="media-left">
								<i class="fa fa-book"></i>
							</div>
							<div class="media-body">
								<h3 class="media-heading">Reports & Dashboard</h3>
								<p>With our easy to use consolidated dashboard, you can
									graphically visualize all your operations with clickable tabs
									and identify statistics, correlations and BL status. You can
									even generate and download several reports concerning vital
									stages of whole process.
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
	<!-- //capabilities -->

	<div class="default-bg space blue" style="display: none;">
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2">
					<h1 class="text-center">Let's Work Together!</h1>
				</div>
			</div>
		</div>
	</div>
	<div class="faq" id="faq">
		<div class="section clearfix object-non-visible"
			data-animation-effect="fadeIn">
			<div class="container">
				<div class="row">
					<h2 class="title-w3 faq-header text-center wow fadeInDown" data-wow-delay="0.4s">FREQUENTLY&nbsp;ASKED&nbsp;QUESTIONS</h2>
					<p class="faq-sub-header text-center" style="display: none;">
						Answers to some common questions...<i
							class="fa fa-question-circle fa-x" aria-hidden="true"></i>
					</p>
					<div class="col-md-12">

						<div class="row">


							<div class="col-md-12">
								<div class="panel-group" id="accordion" role="tablist"
									aria-multiselectable="true">
									<div class="panel panel-default wow fadeInUp" data-wow-delay="0.6s">
										<div class="panel-heading" role="tab" id="headingOne">
											<h4 class="panel-title">
												<a data-toggle="collapse" data-parent="#accordion"
													href="#collapseOne" aria-expanded="true"
													aria-controls="collapseOne"> Login </a>
											</h4>
										</div>
										<div id="collapseOne" class="panel-collapse collapse in"
											role="tabpanel" aria-labelledby="headingOne">
											<div class="panel-body">
												<h4>How to create membership in myshipment?</h4>
												<p>Myshipment user creation is a facility given only to
													the companies and individuals who have communicated to our
													sales/CRM personnel and have a business profile created
													within our system. Hence this is not a functionality
													supported by myshipment.</p>
												<hr>
												<h4>How to change password of your account?</h4>
												<p>After logging in click, the settings icon on top
													right corner of the homepage and select the change password
													option from the settings list.</p>
											</div>
										</div>
									</div>
									<div class="panel panel-default wow fadeInUp" data-wow-delay="0.7s">
										<div class="panel-heading" role="tab" id="headingTwo">
											<h4 class="panel-title">
												<a class="collapsed" data-toggle="collapse"
													data-parent="#accordion" href="#collapseTwo"
													aria-expanded="false" aria-controls="collapseTwo">
													Booking </a>
											</h4>
										</div>
										<div id="collapseTwo" class="panel-collapse collapse"
											role="tabpanel" aria-labelledby="headingTwo">
											<div class="panel-body">
												<h4>How can I ensure my booking is confirmed?</h4>
												<p>Whenever a booking is successfully completed a
													success message is shown, furthermore an auto mail is sent
													to the user&#39;s previously set email address. myshipment
													application also give a notification to the parties
													affiliated with the order.</p>
												<hr>
												<h4>How can I know my HBL number?</h4>
												<p>After successful completion of the order HBL number
													is provided in the success page. Moreover you can also find
													in the email sent for booking confirmation.</p>
												<hr>
												<h4>Why can&#39;t I find a previously created order?</h4>
												<p>There could be two possible scenarios :</p>
												<ul>
													<li>You have not selected the proper company after
														logging in, simply change the company name where you made
														the order from the option bar in top right corner of the
														page.</li>
													<li>Another reason could be the wrong distribution
														channel (sea/air) and/or wrong division (export/import).</li>
												</ul>
												<hr>
												<h4>How can the previous shipments be found?</h4>
												<p>All the pervious orders made by the user can be found
													in Last N shipment under the Report management tab in the
													menu.</p>
												<hr>
											</div>
										</div>
									</div>
									<div class="panel panel-default wow fadeInUp" data-wow-delay="0.8s">
										<div class="panel-heading" role="tab" id="headingThree">
											<h4 class="panel-title">
												<a class="collapsed" data-toggle="collapse"
													data-parent="#accordion" href="#collapseThree"
													aria-expanded="false" aria-controls="collapseThree">
													Tracking </a>
											</h4>
										</div>
										<div id="collapseThree" class="panel-collapse collapse"
											role="tabpanel" aria-labelledby="headingThree">
											<div class="panel-body">
												<h4>How can I track a shipment without the HBL number?</h4>
												<p>Myshipment facilitates four different ways of
													tracking a shipment :</p>
												<ul>
													<li>PO tracking</li>
													<li>Commercial Invoice tracking</li>
													<li>HBL number-wise tracking</li>
													<li>MBL number-wise tracking</li>
												</ul>
											</div>
										</div>
									</div>
									<div class="panel panel-default wow fadeInUp" data-wow-delay="0.9s">
										<div class="panel-heading" role="tab" id="headingFour">
											<h4 class="panel-title">
												<a class="collapsed" data-toggle="collapse"
													data-parent="#accordion" href="#collapseFour"
													aria-expanded="false" aria-controls="collapseThree">
													Reports </a>
											</h4>
										</div>
										<div id="collapseFour" class="panel-collapse collapse"
											role="tabpanel" aria-labelledby="headingFour">
											<div class="panel-body">
												<h4>How can I print a report?</h4>
												<p>When the report is shown in the browser you will find
													download, print icons. You can either choose to directly
													download from the browser or you can download it in your
													local file system and then print.</p>
												<hr>
												<h4>Why can&#39;t I find the Non-negotiable bill of lading?</h4>
												<p>Non-negotiable bill of lading is not created until
													the shipment is done if your distribution channel is sea.
													If your channel is air Non-negotiable bill is created after
													the shipping order is made.</p>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- section end -->
	</div>

	<div id="app">
		<div id="bgimage" class="header-image">
			<div class="container">
				<div class="row">

					<div class="col-sm-5 col-xs-12">
						<div class="iphone center-content wow fadeInLeft" data-wow-duration="2s">
							<img class="img-responsive" src="${pageContext.request.contextPath}/resources/myshipment_landing/images/iphone8-android-mockup.png" alt="" />
						</div>
					</div>

					<div class="col-sm-6">
						<div class="heading-text">
							<h1 class="text-center wow fadeIn" data-wow-duration="1.5s">Download Myshipment APP</h1>
							<br>
							<p class="bannerDescription text-center wow fadeInUp" data-wow-duration="1.5s">Why bother going to www?</p>

							<div class="right_desc_bottom_text">
								<div class="right_single_bottom_text wow zoomIn" data-wow-duration="1.5s">
									<i class="fa fa-shield" style="color: #26acf5;"></i>
									<div class="right_bottom_description">
										<h3>All-Round Tracking App</h3>

										<p>Considering the convenience of customers, we have
											brought myshipment at arm&#39;s length. myshipment Mobile
											Application provides a more flexible experience with 24/7
											availability irrespective of location.</p>
									</div>
								</div>

								<div class="right_single_bottom_text wow zoomIn" data-wow-duration="1.5s">
									<i class="fa fa-css3" style="color: #26acf5;"></i>
									<div class="right_bottom_description">
										<h3>Your Own Handy Tool</h3>

										<p>By creating visibility, no matter where you are,
											presently it is easy as ever to track shipment with our new
											global mobile device application.</p>
									</div>
								</div>

								<br>
								<div class="row" style="display: block;">
								<h3 class="text-center">We're Coming Soon</h3>
									<!-- <a href="" class="btn">Downlod APP</a> -->
									<!-- <a href="" class="btn white-btn youtube-media"><i class="fa fa-play"></i> Watch video</a> -->
									<br><br>
									<div
										class="col-sm-6 col-md-6 col-xs-6 center-content wow fadeInLeft"
										data-wow-duration="1s">
										<!-- href="https://play.google.com/store/apps/details?id=com.mgh.shipment" -->
										<a target="_blank" href="https://play.google.com/store/apps/details?id=com.myshipment.app"><img
											class="img-responsive" src="${pageContext.request.contextPath}/resources/myshipment_landing/images/Android-App-Store-G.png"
											align="right" class="img-responsive" alt="" width="150" height="100" /></a>
									</div>
									<div
										class="col-sm-6 col-md-6 col-xs-6 center-content wow fadeInRight"
										data-wow-duration="1s">
										<!-- href="https://itunes.apple.com/za/app/myshipment-supplier/id1168561725?mt=8" -->
										<a target="_blank"><img
											class="img-responsive" src="${pageContext.request.contextPath}/resources/myshipment_landing/images/Apple-Coming-Soon.png"
											alt="" class="img-responsive" width="150" height="100" /></a>
									</div>
									
								</div>
								
								<br><br>
								<div class="row">
									
									
								</div>
							</div>
						</div>
					</div>

				</div>
				<!-- end of row -->
			</div>
			<!-- end of container -->

			<div class="scrolldown" style="display: none;">
				<a href="#home" class="scroll_btn"></a>
			</div>

		</div>
	</div>
	<br>
	<br>

	<!-- footer start -->
	<!-- ================ -->
	<footer id="footer">

		<!-- .footer start -->
		<!-- ================ -->
		<div class="footer section" id="contact">
			<div class="container">
				<h1 class="title text-center wow fadeInDown" data-wow-duration="2s">Get in Touch</h1>
				<div class="space"></div>
				<div class="row">
					<div class="col-md-6">
						<div class="footer-content">
							<h3 class="large wow fadeInLeft" data-wow-duration="1s">Connect with us:</h3>
							<p class="connect-with-us-subheader wow fadeInLeft" data-wow-duration="1s">For support or any questions</p>
							<ul class="list-icons wow fadeInUp" data-wow-duration="2s">
								<!-- <li><i class="fa fa-map-marker pr-10"></i> One infinity loop, 54100</li>
									<li><i class="fa fa-phone pr-10"></i> +00 1234567890</li>
									<li><i class="fa fa-fax pr-10"></i> +00 1234567891 </li> -->
								<li><i class="fa fa-envelope-o pr-10"></i>
									itsupport@mghgroup.com</li>
							</ul>
							<ul class="social-links">
								<li class="facebook wow bounce" data-wow-duration="800ms"><a target="_blank"
									href="https://www.facebook.com/mghgroupglobal"><i
										class="fa fa-facebook"></i></a></li>
								<!-- <li class="twitter"><a target="_blank" href="https://twitter.com/HtmlcoderMe"><i class="fa fa-twitter"></i></a></li> -->
								<li class="linkedin wow bounce" data-wow-duration="900ms"><a target="_blank"
									href="https://www.linkedin.com/company/mghgroup"><i
										class="fa fa-linkedin"></i></a></li>
								<li style="" class="android wow bounce" data-wow-duration="1000ms"><a target="_blank"
									href="https://play.google.com/store/apps/details?id=com.myshipment.app"><i
										class="fa fa-android"></i></a></li>
								<li style="display: none;" class="apple wow bounce" data-wow-duration="1100ms"><a target="_blank"
									href="https://itunes.apple.com/za/app/myshipment-supplier/id1168561725?mt=8"><i
										class="fa fa-apple"></i></a></li>
								<!-- <li class="googleplus"><a target="_blank" href="http://plus.google.com"><i class="fa fa-google-plus"></i></a></li>
									<li class="skype"><a target="_blank" href="http://www.skype.com"><i class="fa fa-skype"></i></a></li> -->

								<!-- <li class="youtube"><a target="_blank" href="http://www.youtube.com"><i class="fa fa-youtube"></i></a></li>
									<li class="flickr"><a target="_blank" href="http://www.flickr.com"><i class="fa fa-flickr"></i></a></li>
									<li class="pinterest"><a target="_blank" href="http://www.pinterest.com"><i class="fa fa-pinterest"></i></a></li> -->
							</ul>
						</div>
					</div>
					<div class="col-md-6 wow zoomIn" data-wow-duration="1.5s">
						<h3 class="large get-in-touch-fill-up-form">Please fill out the form and we will be in touch.</h3>
						<div class="footer-content">
						<div id="footer-form" >
						<%-- 	<form role="form" id="footer-form"> --%>
								<div class="form-group has-feedback">
									<label class="sr-only" for="name2">Name</label> <input
										type="text" class="form-control" id="name2" placeholder="Name"
										name="name2" required> <i
										class="fa fa-user form-control-feedback"></i>
								</div>
								<div class="form-group has-feedback">
									<label class="sr-only" for="email2">Email address</label> <input
										type="email" class="form-control" id="email2"
										placeholder="Enter email" name="email2" required> <i
										class="fa fa-envelope form-control-feedback"></i>
								</div>
								<div class="form-group has-feedback">
									<label class="sr-only" for="message2">Message</label>
									<textarea class="form-control" rows="8" id="message2"
										placeholder="Message" name="message2" required></textarea>
									<i class="fa fa-pencil form-control-feedback"></i>
								</div>
								<input type="submit" value="Send"
									class="btn btn-primary contact-us-send" onclick="return mailUser();">
								<div id="snackbar">Thanks for contacting us.We will be contacting you back soon. </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- .footer end -->
		<br> <br> <br> <br> <br> <br>
		<div class="copy">
			<p>
				&copy; <script type="text/javascript">
                                var today = new Date()
                                var year = today.getFullYear()
                                document.write(year)

                            </script> my<span class="shipment-footer-panel">shipment</span> | All
				Rights Reserved
			</p>
		</div>

		<!-- .subfooter start -->
		<!-- ================ -->
		<div class="subfooter" style="display: none;">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<p class="text-center">
							Copyright © 2014 Worthy by <a target="_blank"
								href="http://htmlcoder.me">HtmlCoder</a>.
						</p>
					</div>
				</div>
			</div>
		</div>
		<!-- .subfooter end -->

	</footer>
	<!-- footer end -->


	<!-- js -->
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/myshipment_landing/js/jquery-2.1.4.min.js"></script>
	<!-- jarallax-js -->
	<!-- <script src="js/jarallax.js"></script>
			<script src="js/SmoothScroll.min.js"></script>
			<script type="text/javascript">
				/* init Jarallax */
				$('.jarallax').jarallax({
					speed: 0.5,
					imgWidth: 1366,
					imgHeight: 768
				})
			</script> -->
	<!-- //jarallax-js -->
	<!--banner Slider starts Here-->
	<script
		src="${pageContext.request.contextPath}/resources/myshipment_landing/js/responsiveslides.min.js"></script>
	<script>
		// You can also use "$(window).load(function() {"
		$(function() {
			// Slideshow 4
			$("#slider3").responsiveSlides({
				auto : true,
				pager : true,
				nav : false,
				speed : 1000,
				namespace : "callbacks",
				before : function() {
					$('.events').append("<li>before event fired.</li>");
				},
				after : function() {
					$('.events').append("<li>after event fired.</li>");
				}
			});

		});
	</script>

	<!--banner Slider starts Here-->


	<!--light-box-files -->
	<script
		src="${pageContext.request.contextPath}/resources/myshipment_landing/js/modernizr.custom.js"></script>

	<!-- Appear javascript -->
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/myshipment_landing/js/jquery.appear.js"></script>




	<!-- //js -->
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/myshipment_landing/js/easing.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event) {
				event.preventDefault();
				$('html,body').animate({
					scrollTop : $(this.hash).offset().top
				}, 1000);
			});
		});
	</script>
	<!-- start-smoth-scrolling -->
	<!--//footer-section-->
	<!-- Starts-Number-Scroller-Animation-JavaScript -->
	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/myshipment_landing/js/numscroller-1.0.js"></script>
	
	<!-- WOW JS -->
        <script src="${pageContext.request.contextPath}/resources/js/myshipment_v2/wow.min.js"></script>

	
	<!-- //Starts-Number-Scroller-Animation-JavaScript -->
	<!-- smooth scrolling -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			 */
			//$().UItoTop({ easingType: 'easeOutQuart' });
		});
		

		/**********************************
		WOW JS
		 **********************************/

		$(function() {
			new WOW().init();

		});

		/* $(document).ready(function() {
			$(document).on("scroll", onScroll);

			//smoothscroll
			$('a[href^="#"]').on('click', function(e) {
				e.preventDefault();
				$(document).off("scroll");

				$('a').each(function() {
					$(this).removeClass('active');
				})
				$(this).addClass('active');

				var target = this.hash, menu = target;
				$target = $(target);
				$('html, body').stop().animate({
					'scrollTop' : $target.offset().top + 2
				}, 500, 'swing', function() {
					window.location.hash = target;
					$(document).on("scroll", onScroll);
				});
			});
		});

		function onScroll(event) {
			var scrollPos = $(document).scrollTop();
			$('#bs-example-navbar-collapse-1 a').each(
					function() {
						var currLink = $(this);
						var refElement = $(currLink.attr("href"));
						if (refElement.position().top <= scrollPos
								&& refElement.position().top
										+ refElement.height() > scrollPos) {
							if (refElement.selector != "#home") {
								$('#bs-example-navbar-collapse-1 ul li a')
										.removeClass("active");
								currLink.addClass("active");
							}

						} else {
							currLink.removeClass("active");
						}
					});
		} */
	</script>
	
	<script type="text/javascript" src="${pageContext.request.contextPath}/resources/myshipment_landing/js/helpMail.js">

	</script>
	<a href="#" id="toTop" style="display: none;"> <span
		id="toTopHover" style="opacity: 1;"> </span></a>
	<!-- //smooth scrolling -->


	<script type="text/javascript"
		src="${pageContext.request.contextPath}/resources/myshipment_landing/js/bootstrap-3.1.1.min.js"></script>

</body>
</html>