<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">

<%@page import="java.util.Date"%>
<%@ include file="header_v2.jsp"%>
<%@ include file="navbar.jsp"%>

<style>
html, body {
	margin-bottom: 0px !important;
}
</style>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/newoverlay.js"></script>
	
<script type="text/javascript">
function validateForm(){ 
	//alert('hi');
	var deliveryDate=document.getElementById("deliveryDate").value;
	//alert("+++" + deliveryDate);
	var dt = new Date(deliveryDate);
	//alert(deliveryDate);
	//dt.setDate(dt.getDate()-15);
	dt.setDate(dt.getDate());
	//alert(dt);
	//alert(dt.getDate());
	var currentDate=new Date();
	//alert(currentDate);
	//alert(currentDate.getDate());
	var diffDay=(((((currentDate-dt)/1000)/60)/60/24));
	//alert((((((currentDate-dt)/1000)/60)/60/24)));
	//alert('diff day: '+diffDay);
	if(diffDay<-21){
		alert('To Early to Book The Order, Please Book Order Prior to 15 Days Only');
		return false;
	}
	//return false;
	<%
	Date todayDate=new Date();
	System.out.println(todayDate);
	int time=todayDate.getHours();
	int year=todayDate.getYear();
	int month=todayDate.getMonth();
	int date=todayDate.getDate();
	System.out.println(time);
	System.out.println(year);
	System.out.println(month);
	System.out.println(date);
    
	out.println("var currentHour="+ time);
	%>
	//alert(currentHour);
	var todateDate=new Date();
	//alert(todateDate);
	var day=todateDate.getDay();
	var time=todateDate.getHours();
	//alert(time);
	var year=parseFloat(todateDate.getFullYear());
	var month=parseFloat(todateDate.getMonth())+1;
	var date=parseFloat(todateDate.getDate());
	//alert("'"+month+"-"+date+"-"+year+"'");
	//alert(month);
	//alert(year);
	//alert(date);

	
	var poNumber=document.getElementById("poNumber").value;
	//var supplierId=document.getElementById("supplierId").value;
			
	var inspectionDate=document.getElementById("inspectionDate").value;
	var inspectionQuantity=document.getElementById("inspectionQuantity").value;
	/*if(supplierId==''){
		alert('Use Proper Way, Dont Use Direct Link');
		return false;
	}*/
	if(poNumber==''){
		alert('Please Enter PO Number');
		return false;
	}
	if(inspectionDate==''){
		alert('Please Select Inspection Date');
		return false;
		
		}
	else{
		//alert(inspectionDate);
		
		var d = new Date(inspectionDate);
		var inspectionDay=d.getDay();
		//alert(inspectionDay);
		if(inspectionDay==6 ||inspectionDay==0){
			alert('Invalid Inspection Date, Please Select Other Day [Dont Select Saturday And Sunday]')
			return false;
		} 
		var inspectionDateArray=inspectionDate.split("/");
		//alert(inspectionDate.split("/").length);
		//alert(inspectionDateArray);
		var inspectionMonth=parseFloat(inspectionDateArray[0]);
		//alert(inspectionMonth);
		
		var inspectionDate=parseFloat(inspectionDateArray[1]);
		//alert(inspectionDate);

		
		var inspectionYear=inspectionDateArray[2];
		var currentDate = new Date();
		var valueofcurrentDate=currentDate.valueOf()+(24*60*60* 1000);
		var valueofyesterDayDate=currentDate.valueOf()-(24*60*60* 1000);
		
		var newDate =new Date(valueofcurrentDate);
		var yesterDayDate=new Date(valueofyesterDayDate);
		
		var nonYear=parseFloat(newDate.getFullYear());
		var nonMonth=parseFloat(newDate.getMonth())+1;
		var nonDate=parseFloat(newDate.getDate());
		//alert(nonYear);
		//alert(nonMonth);
		//alert(nonDate);
		
		//alert(newDate.toLocaleDateString());
		
		 if(inspectionYear==year && month==inspectionMonth && date==inspectionDate){
                                alert('Inspection Booking is not Allowed in Same Day');
                                return false;
                                }
		
		
		if(currentHour>=16)
		{
			if(inspectionYear==nonYear && nonMonth==inspectionMonth && nonDate==inspectionDate){
				alert('Inspection Booking Time Over');
				return false;
				}
		}

		var yesterDayDateday=parseFloat(yesterDayDate.getYear());
		var yesterDayDatemonth=parseFloat(yesterDayDate.getMonth());
		var yesterDayDateyear=parseFloat(yesterDayDate.getDate());

		
					
		if(inspectionYear<year)
		{
			alert('Inspection Booking is Not Allowed');
			return false;
		}
		else
		{
			if(inspectionMonth<month && inspectionYear==year){
				alert('Inspection Booking  is Not Allowed');
				return false;
			}
			else if (inspectionMonth==month && inspectionDate<date){
				alert('Inspection Booking is Not Allowed');
				return false;
			}
		}
			if(inspectionMonth==month && inspectionDate<date){
				alert('Inspection Booking is Not Allowed');
				return false;
		}
		//alert(inspectionYear);
		/*if(inspectionYear>=year && inspectionMonth>=month && inspectionDate>=date){
			if(time>=16)
			{
				if(){
					
				}
				}
			else{
				alert('Inspection Booking Time Over');
				//return false;
			}
		}
		else{
			alert('Inspection Booking Time Over');
			//return false;
		}*/
	}
	if(inspectionQuantity==''){
		alert('Please Enter Inspection Quantity');
		return false;
	}
/*	var sapNo=document.getElementById("sapNo").value;
	if(sapNo==''){
		alert('Please Enter Valid PO Number');
		return false;
	}*/
 var quantity=document.getElementById("quantity").value;
        if(quantity==''){
                alert('Please Enter Valid PO Number');
                return false;
        }

	var packMethod=document.getElementById("packMethod").value;
	if(packMethod==''){
		alert('Select Pack Method');
		return false;
		}
	
	
	var orderType=document.getElementById("advOrder").value;
	if(orderType==''){
		alert('Select Order Type');
		return false;
		}
	
	var scotaFileSubmission=document.getElementById("scotaFileSubmission").value;
	if(orderType=='N'){
		alert('SCOTA File Submission needed');
		return false;
		}
	/* scotaFileSubmission */
	
	
	var packingList=document.getElementById("packingList").value;
	//alert(packingList);
	//alert(packingList.length);
	
	if(packingList.length==0){
		alert('Please Select Packing List File to Upload');
		return false;
		}
		
	if(inspectionQuantity!='' && quantity!=''){
	var receivedQuantityPer=(inspectionQuantity*100)/quantity;
	receivedQuantityPer=receivedQuantityPer-100;
	//alert(receivedQuantityPer);
	if(receivedQuantityPer<=-3){
		//alert('Order Insepction Quantity if -3% less than Order Actual Quantity');
		document.getElementById("remarks").value='Less Quantity '+receivedQuantityPer.toFixed(2)+' %'
		}
	
	if( receivedQuantityPer>=3){
		//alert('Order Insepction Quantity if +3% more than Order Actual Quantity');
		document.getElementById("remarks").value='Over Quantity '+receivedQuantityPer.toFixed(2)+' %'
		}
	}	
	//Date delivDate=new Date(deliveryDate);
	//alert(delivDate)
	//return false;
}
</script>

<script type="text/javascript">
function newXMLHttpRequest() {

	  var xmlreq = false;
	  
	  if (window.XMLHttpRequest) {

	    // Create XMLHttpRequest object in non-Microsoft browsers
	    xmlreq = new XMLHttpRequest();

	  } else if (window.ActiveXObject) {

	    // Create XMLHttpRequest via MS ActiveX
	    try {
	      // Try to create XMLHttpRequest in later versions
	      // of Internet Explorer

	      xmlreq = new ActiveXObject("Msxml2.XMLHTTP");

	    } catch (e1) {

	      // Failed to create required ActiveXObject

	      try {
	        // Try version supported by older versions
	        // of Internet Explorer

	        xmlreq = new ActiveXObject("Microsoft.XMLHTTP");

	      } catch (e2) {

	        // Unable to create an XMLHttpRequest with ActiveX
	      }
	    }
	  }

	  return xmlreq;
	}
	//Tayeba starts  
function getPoDetail(poNumber) {
	var pOnumber = poNumber;
    var stringedPOnumber = JSON.stringify(pOnumber);
    console.log("the stringedPOnumber is"+stringedPOnumber);
    console.log("the pOnumber is"+pOnumber);
    

   $.ajax({
       type: "POST",
        url: "/MyShipmentFrontApp/api/searchPo/",
       data: JSON.stringify(pOnumber),
       contentType: "application/json",
       dataType: 'json',
       cache: false,
       timeout: 600000,
       success: function (data) {

           var json = JSON.stringify(data, null, 4) ;
       
           console.log("now the array is"+ data);
           console.log("now the array is"+ json);
           
           console.log("SUCCESS : ", data[0].order_no);
           console.log("SUCCESS : ", json[0].order_no); 
           for (i in data){
        	   if (data[i].flag!="Y")
        		   {
        		   console.log(data[i].flag);
        		   $('#mode').val(data[i].shipment_mode);
                   $('#deliveryDate').val(data[i].delivery_date);
                   $('#quantity').val(data[i].quantity);
                   $('#sapNo').val(data[i].sap_no);
                   $('#merchantDiser').val(data[i].inspector_code);
                   $('#customerName').val(data[i].customer_name);
        		   }
        	   else{
            	   $('#mode').val("");
                   $('#deliveryDate').val("");
                   $('#quantity').val("");
                   $('#sapNo').val("");
                   $('#merchantDiser').val("");
                   $('#customerName').val("");
               }
           }
           
          
           
       },
       error: function (e) {

           var json = e.responseText ;
          
           console.log("ERROR : ", e);
           console.log("ERROR : ", e);
           
       }
   });

	  
	}
	//Tayeba ends


function getReadyStateHandler(req, responseXmlHandler) {

	  // Return an anonymous function that listens to the 
	  // XMLHttpRequest instance
	  return function () {

	    // If the request's status is "complete"
	    if (req.readyState == 4) {
		    
	      // Check that a successful server response was received
	      if (req.status == 200) {

	        // Pass the XML payload of the response to the 
	        // handler function
	        
	        responseXmlHandler(req.responseXML);
	        
	      } else {

	        // An HTTP problem has occurred
	        alert("HTTP error: "+req.status);
	      }
	    }
	  }
	}
function populatePoDetail(poXML) {

	//initialize form
	document.getElementById("sapNo").value='';
    document.getElementById("merchantDiser").value='';
    document.getElementById("mode").value='';
    document.getElementById("deliveryDate").value='';
   // document.getElementById("advOrder").value='';
    document.getElementById("quantity").value='';
    document.getElementById("customerName").value='';
    var advOrderType ='';
	// Get the root "cart" element from the document
	 var po = poXML.getElementsByTagName("PO");
	 
	 /*var sapNo = poXML.getElementsByTagName("sapNo");
	 var merchantDiser = poXML.getElementsByTagName("merchantDiser");
	 var shipmentMode = poXML.getElementsByTagName("shipmentMode");
	 var deliveryDate = poXML.getElementsByTagName("deliveryDate");
	 var advOrder = poXML.getElementsByTagName("advOrder");
*/
	//alert(po.length);

for (var I = 0 ; I < po.length ; I++) {

    var detail = po[I];
       var message  = detail.getElementsByTagName("message")[0].firstChild.nodeValue;
    
    // Extract the text nodes from the name and quantity elements
    	var sapNo = detail.getElementsByTagName("sapNo")[0].firstChild.nodeValue;
    	var merchantDiser = detail.getElementsByTagName("merchantDiser")[0].firstChild.nodeValue;
    	var shipmentMode = detail.getElementsByTagName("shipmentMode")[0].firstChild.nodeValue;
    	var deliveryDate = detail.getElementsByTagName("deliveryDate")[0].firstChild.nodeValue;
    	var quantity = detail.getElementsByTagName("quantity")[0].firstChild.nodeValue;
    	advOrderType = detail.getElementsByTagName("advOrder")[0].firstChild.nodeValue;
    
        var customerName = detail.getElementsByTagName("customerName")[0].firstChild.nodeValue;
	if(sapNo=='null'){
		sapNo='';
//		alert('Invalid PO Number, Please Enter Valid PO Number');
//	    document.getElementById("poNumber").value='';
	     
	}
	if(merchantDiser=='null')
		merchantDiser='';
	if(shipmentMode=='null')
		shipmentMode='';
	if(deliveryDate=='null')
		deliveryDate='';
	
	if(quantity=='null' || quantity==''){
	
	     quantity='';
              alert('Invalid PO Number, Please Enter Valid PO Number');
            document.getElementById("poNumber").value='';

        }
	if(orderType=='null')
		orderType='';
	if(customerName=='null')
		customerName='';
	
    document.getElementById("sapNo").value=sapNo;
    document.getElementById("merchantDiser").value=merchantDiser;
    document.getElementById("mode").value=shipmentMode;
    document.getElementById("deliveryDate").value=deliveryDate;
    //document.getElementById("advOrder").value=advOrder;
    document.getElementById("quantity").value=quantity;
    //alert(customerName);
    document.getElementById("customerName").value=customerName;

    
    
    
}
var orderType=document.getElementById("advOrder");
var orderTypeLength=orderType.options.length;
//alert(orderTypeLength);
var selection=false;
for(var i=0;i<orderTypeLength;i++){
		if(orderType.options[i].value==advOrderType && orderType.options[i].value!=''){
			orderType.options[i].selected=true;
			selection=true;
			break;
		}
    }
//alert(selection);

if(selection==false)
	orderType.options[0].selected=true;


}
</script>



<section class="main-body">
	<!-- <div class="well assign-po-search"> -->
	<div class="well-white-269490">
		<form:form action="submitinspectionbooking" method="post" commandName="insBookingData" id="insBooking" enctype="multipart/form-data">
			<fieldset>
				<legend>Booking Information<span style="color: #5bc0de;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;${message}</span></legend>
				<div class='row'>

					<div class='col-sm-3'>
						<div class='form-group'>
							<label>Buying House</label>
							<form:select path="buyingHouse" id="buyingHouse" cssClass="form-control">
								<option value="1020000095" selected>C & A Sourcing International Ltd</option>
							</form:select>
						</div>
					</div>
					
					<div class='col-sm-3'>
						<div class='form-group'>
							<label>PO Number<span style="color: red;">*</span></label>
							<form:input path="poNumber" id="poNumber" cssClass="form-control auto-complete color-change" onchange="getPoDetail(this.value)" onkeyup ="fillPOData(this)" required="true"/>
						</div>
					</div>

					<div class='col-sm-3'>
						<div class='form-group'>
							<label>Inspection Date<span style="color: red;">*</span></label>
							<form:input path="inspectionDate" id="inspectionDate"
								class="date-picker form-control glyphicon glyphicon-calendar" required="true"/>
						</div>
					</div>

					<div class='col-sm-3'>
						<div class='form-group'>
							<label>Mode<span style="color: red;">*</span></label>
							<form:input path="mode" id="mode" cssClass="form-control" required="true"/>
						</div>
					</div>
					
					<div class='col-sm-3'>
						<div class='form-group'>
							<label>Quantity<span style="color: red;">*</span></label>
							<form:input path="inspectionQuantity" id="inspectionQuantity" cssClass="form-control" required="true"/>
						</div>
					</div>
					
					<div class='col-sm-3'>
						<div class='form-group'>
							<label>Cargo Delivery Date</label>
							<form:input path="cargoDeliveryDate" id="cargoDeliveryDate"
								class="date-picker form-control glyphicon glyphicon-calendar" />
						</div>
					</div>
					
					<div class='col-sm-3'>
						<div class='form-group'>
							<label>Pack Method<span style="color: red;">*</span></label>
							<form:select path="packMethod" id="packMethod" cssClass="form-control" required="true">
								<form:option value="NONE">--- Select ---</form:option>
								<form:option value="LL">LL</form:option>
								<form:option value="LH">LH</form:option>
								<form:option value="HH">HH</form:option>
							</form:select>
						</div>
					</div>					
					
					<div class='col-sm-3'>
						<div class='form-group'>
							<label>Order Type<span style="color: red;">*</span></label>
							<form:select path="advOrder" id="advOrder" cssClass="form-control" required="true">
								<form:option value="NONE">--- Select ---</form:option>
								<form:option value="A01">Advert</form:option>
								<form:option value="FKI">FKI</form:option>
								<form:option value="N">Normal</form:option>
							</form:select>
						</div>
					</div>
					
					<div class='col-sm-3'>
						<div class='form-group'>
							<label>Location</label>
							<form:input path="location" id="location" cssClass="form-control" maxlength="20"/>
						</div>
					</div>

					<div class='col-sm-3'>
						<div class='form-group'>
							<label>Remarks</label>
							<form:textarea path="remarks" id="remarks" cssClass="form-control" style="height: 34px !important;"/>
						</div>
					</div>



				</div>

			</fieldset>
			
			<!-- ORDER DETAILS PART -->
			<fieldset style="margin-top: 15px;">
				<legend>Order Details</legend>
				<div class='row'>

					<div class='col-sm-2'>
						<div class='form-group'>
							<label>Delivery Date</label>
							<form:input path="deliveryDate" id="deliveryDate" readonly="true" size="15" cssClass="form-control"/>
						</div>
					</div>
					
					<div class='col-sm-2'>
						<div class='form-group'>
							<label>Order Quantity</label>
							<form:input path="quantity" id="quantity" readonly="true" size="10" cssClass="form-control"/>
						</div>
					</div>

					<div class='col-sm-2'>
						<div class='form-group'>
							<label>SAP No</label>
							<form:input path="sapNo" id="sapNo" readonly="true" size="15" cssClass="form-control"/>
						</div>
					</div>

					<div class='col-sm-2'>
						<div class='form-group'>
							<label>Merchandiser</label>
							<form:input path="merchantDiser" id="merchantDiser" readonly="true" size="15" cssClass="form-control"/>
						</div>
					</div>
					
					<div class='col-sm-2'>
						<div class='form-group'>
							<label>Customer Name</label>
							<form:input path="customerName" id="customerName" readonly="true" size="15" cssClass="form-control" />
						</div>
					</div>
					
					<div class='col-sm-2'>
						<div class='form-group'>
							<label style="display: block; color: #f5f5f5">. </label>
							<input type="submit" id="btn-appr-po-search" class="btn btn-success form-control" value="Submit" onClick="return validateForm()"/>
						</div>
					</div>

				</div>

			</fieldset>
		</form:form>
	</div>



</section>



<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/buttons.flash.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jszip.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/pdfmake.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/vfs_fonts.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/buttons.html5.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/buttons.print.min.js"></script>


<script>
	$(document).ready(function() {
		$("#inspectionDate").datepicker({
			autoclose : true,
			dateFormat : "mm/dd/yy"
		});
		//.datepicker("setDate", new Date());
		$("#cargoDeliveryDate").datepicker({
			autoclose : true,
			dateFormat : "mm/dd/yy"
		});
		//.datepicker("setDate", new Date());
	})	
	
</script>


<%@include file="footer_v2.jsp"%>