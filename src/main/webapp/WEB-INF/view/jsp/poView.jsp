<jsp:include page="header_v2.jsp"></jsp:include>
<jsp:include page="navbar.jsp"></jsp:include>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style>
	.main-body {
		margin-bottom: 5%;
	}
	.commercial h3 {
	    background-color: #222534;
	    padding-top: 7px;
	    font-weight: 400;
	    height: 40px;
	    margin-top: 10px;
	    color: #fff;
	}
</style>
<!-- DataTables -->
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

<section class="main-body">
	<div tabindex="-1">
		<div class="row-without-margin">
			<div class="col-md-12">
				<div class="row-without-margin" style="margin-left: -1%; width: 100%;">
					<div class="col-md-12">
						<div class="commercial">
							<h4 class="text-center" style="background-color: #222534;">Uploaded/Generated PO Information</h4>
						</div>
					</div>
				</div>
				<div>
					<div class="container-fluid table-responsive">

						<table class="table table-striped" id="poTable">
							<thead style="background-color: #222534; color: #fff;">
								<tr>
									<th>Po No</th>
									<th>Style No</th>
									<th>Product No</th>
									<th>Color</th>
									<th>Size</th>
									<th>Total Pieces</th>
									<th>Total G. Weight</th>
									<th>Total Net Weight</th>
									<th>Total CBM</th>
									<!-- <th>Action</th> -->
								</tr>
							</thead>
							<tbody>
								<c:forEach items="${uploadedDataLst}" var="i">
									<tr class="">
										<td>${i.vc_po_no}</td>
										<td>${i.vc_style_no}</td>
										<td>${i.vc_product_no}</td>
										<!--  <td>${i.vc_sku_no}</td> -->
										<td>${i.vc_color}</td>
										<td>${i.vc_size}</td>
										<td>${i.vc_tot_pcs}</td>
										<td>${i.vc_gr_wt}</td>
										<td>${i.vc_nt_wt}</td>
										<td>${i.vc_cbm_sea}</td>
										<!-- <td>
            <div id="segregate">
               <a href="segregate.html"><input type="submit" value="Edit"></a>
            </div>
        </td> -->
									</tr>
								</c:forEach>


							</tbody>
						</table>

					</div>
				</div>
				<div></div>
			</div>
		</div>
	</div>

</section>

<script>

	$(function() {
		$('#poTable').DataTable();
	});
	
	
</script>

<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>


<%@include file="footer_v2_fixed.jsp"%>
