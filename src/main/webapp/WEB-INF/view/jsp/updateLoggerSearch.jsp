<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bootstrap-select/css/bootstrap-select.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<script type="text/javascript">
var choosen = ${selectedValue};
console.log(choosen);
</script>
<div class="container-fluid container-fluid-441222">
	<div class="row row-without-margin">
		<h1 class="h1-report">Booking Update Logger</h1>
		<ol class="breadcrumb">
			<li><i class="fa fa-edit"></i>&nbsp;Booking Update Logger</li>
		</ol>
	</div>
	<hr>
	<div class="row row-without-margin">
		<form:form action="loggerSearch" method="post" commandName="req"
			cssClass="form-incline">
			<div class="col-md-4 col-sm-12 col-xs-12">
				<div class='form-group'>
					<label class="text-center">Shipper</label>
					<form:select class="form-control auto-complete selectpicker"
						data-live-search="true" id="shipper" name="shipper" path="shipper"
						required="required">
						<option value="">Select</option>
						<c:if test="${shipper != null}">
							<c:forEach items="${shipper}" var="shipper">
								<option value="${shipper.key }">${shipper.value}</option>
							</c:forEach>
						</c:if>
					</form:select>
				</div>
			</div>
			<div class="col-md-4 col-sm-12 col-xs-12">
				<div class='form-group'>
					<label class="text-center">HBL No.</label>
					<form:input path="hblNumber" id="number" cssClass="form-control" />
				</div>
			</div>
			<div class="col-md-2 col-sm-6 col-xs-6">
				<label style="display: block; color: transparent">.</label>
				<!-- added for alignment purpose, dont delete -->
				<input type="submit" name="search" id="search"
					class="btn btn-success btn-block" value="Search" />
			</div>
		</form:form>
	</div>

	<c:if test="${ not empty searchResults}">

		<div class="row row-without-margin">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="container-fluid">

					<table class="table table-hover" id="loggerTable">
						<thead style="background-color: #222534; color: #fff;">
							<tr>

								<th>HBL</th>
								<th>Update Type</th>
								<th>Field Name</th>
								<th>Line Item No</th>
								<th>Previous</th>
								<th>Updated</th>
								<th>Updated Time</th>

							</tr>
						</thead>
						<tbody>
							<c:forEach items="${searchResults}" var="i">
								<tr>
									<td style="font-size: 13px;">${i.hblNumber}</td>
									<td style="font-size: 13px;">${i.dataType}</td>
									<td style="font-size: 13px;">${i.fieldName}</td>
									<td style="font-size: 13px;">${i.lineItemNo}</td>
									<td style="font-size: 13px; color: #DE5322; font-weight: bold;">${i.prev}</td>
									<td style="font-size: 13px; color: #268314; font-weight: bold;">${i.updated}</td>
									<fmt:parseDate value="${i.updatedDate}" var="updateDate"
										pattern="EEE MMM dd HH:mm:ss z yyyy" />
									<td style="font-size: 13px;"><fmt:formatDate
											pattern="yyyy-MM-dd" type="date" value="${updateDate}" /></td>
									<%-- <td>${i.updatedDate}</td>  --%>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</c:if>
</div>
<jsp:include page="footer_v2.jsp"></jsp:include>
<script
	src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<script>
	$(function() {
		$('#loggerTable').DataTable();
	});
</script>
<script>
$(function(){
    $('#shipper option[value= '+ choosen+']').attr('selected', true);
});
</script>


<script
	src="${pageContext.request.contextPath}/resources/bootstrap-select/js/bootstrap-select.min.js"></script>