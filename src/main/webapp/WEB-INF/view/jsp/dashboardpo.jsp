<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/dashboardpo.css"/>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.css"/>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap-theme.css">
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap-theme.min.css">
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/jquery.dataTables.min.css">
  
  <script src="${pageContext.request.contextPath}/resources/js/bootstrap.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/jquery-1.12.0.min.js"></script>
  <script src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
  <style>
  hr {
    
    margin-bottom: 0px !important;
    border: 0 !important;
    border-top: 0px solid #eee !important;
}
  </style>
  <script>
  $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
  <title>Dashboard</title>
</head>
    <body>
        <header>
            <div class="dashboard-top">
               <figure>
                 <img src="${pageContext.request.contextPath}/resources/images/logo.png" />
               </figure> 
              <form> 
                  <input type="text" placeholder="Track Your Shipment" /><label id="go">GO</label>
              </form>
            </div> 
            
            <div id="company">
               <form>
                <label>EXPORT</label><input type="radio" name="options" /><hr class="horizontal"style="width: 37px;margin-left: 65px;margin-top: -13px;"><br>
                <label>&nbsp;IMPORT</label><input type="radio" name="options" /><hr style="width: 55px;margin-left: 65px;margin-top: -13px;"><br>
                <label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CHA</label><input type="radio" name="options" /><hr style="width: 35px;margin-left: 65px;margin-top: -13px;">
                 <div class="vertical-line"></div>
                  <select id="company_name">
                    <option>ABC Company Name</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                   </select>
                   <div id="change">Change</div>
                </form>
            </div>            
        </header>
        
      <section class="container clearfix">
         <div id="top">
             <div id="ship">
               <img src="${pageContext.request.contextPath}/resources/images/ship.png" /><p><strong>SEA</strong></p>
             </div>   
             <div id="plane">
               <img src="${pageContext.request.contextPath}/resources/images/plane.png" /><p>SWITCH TO AIR</p>
             </div>
             <div id="welcome">
             <p>Welcome</p>
             <p id="username"><strong>User Name</strong><img src="${pageContext.request.contextPath}/resources/images/settings.png" /><img src="images/question.png" /></p>
             </div>
         </div>
      </section>
	  <section class="container table">
      <div class="row">
         <div class="col-sm-12">
		 <h2>XL Table</h2>
  <p>tagline</p>            
  <table class="table table-bordered" id="example">
    <thead>
      <tr>
        <th>Firstname</th>
        <th>Lastname</th>
        <th>Email</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>John</td>
        <td>Doe</td>
        <td>john@example.com</td>
      </tr>
      <tr>
        <td>Mary</td>
        <td>Moe</td>
        <td>mary@example.com</td>
      </tr>
      <tr>
        <td>July</td>
        <td>Dooley</td>
        <td>july@example.com</td>
      </tr>
    </tbody>
  </table>
		 </div>
</div>
</section>
    </body> 
</html>