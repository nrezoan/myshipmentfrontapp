<jsp:include page="header_v2.jsp"></jsp:include>
<jsp:include page="navbar.jsp"></jsp:include>


<div class="entire-content-c">
	<div class="content" style="background-color: #eaeaea; display: block;">
		<section class="content">
			<div class="row">
	               <div class="col-md-6">

	                  <div class="box box-myshipment">
	                     <div class="box-header with-border">
	                        <h3 class="box-title">Buyerwise Shipment</h3>
	                     </div>
	                     <div class="box-body no-padding" style="display: block;">
	                     	<div id="chartContainer" style="width: 95%; height: 300px"></div>
	                     </div>
	                  </div>
	                </div>
	                
	                <div class="col-md-6">

	                  <div class="box box-myshipment">
	                     <div class="box-header with-border">
	                        <h3 class="box-title">Top Buyers</h3>
	                     </div>
	                     <div class="box-body no-padding" style="display: block;">
	                     	<table class="table table-hover">
								  <thead style="">
						      <tr>
						        <th>Buyer Name</th>
						        <th>Total Shipment</th>
						        <th>% of Shipment</th>
								
								
						      </tr>
						    </thead>
						    <tbody>
						     
							</tbody>
						  </table>
	                     </div>
	                  </div>
	                </div>
	          </div>
		</section>
	</div>
</div>

<script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/buyer-wise-shipment.js"></script>
		<script type="text/javascript">
var details=${shipdetails};
$(document).ready(function(){
	renderTable(details);
	drawChart(details);
})

function renderTable(details)
{
	var tr='';
	$.each(details.lstBuyerWiseShipmentJson,function(index,value){
		tr+='<tr>';
		tr+='<td>'+value.buyerName+'</td><td>'+value.totalShipment.toFixed(2)+'</td><td>'+value.totalShipmentPerc.toFixed(2)+'%</td>';
		tr+='</tr>'
	})
	$("table").find("tbody").append(tr);
	}

</script>
		<jsp:include page="footer_v2_fixed.jsp"></jsp:include>