<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<link rel="stylesheet"
	href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/css/bootstrap-select.min.css" />
<script
	src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.6.2/js/bootstrap-select.min.js"></script>
<style>
html {
	background-color: #f5f5f5;
}
</style>

<section class="main-body">
	<div class="well assign-po-search">
		<form:form action="shipmentdetaildatabydate" method="post"
			commandName="sapposearchparams">
			<fieldset>
				<legend>Booked PO Approval</legend>
				<div class='row'>
					<div class='col-sm-2'>
						<div class='form-group'>
							<label>From Date</label>
							<form:input path="fromDate" id="fromDate"
								class="date-picker form-control glyphicon glyphicon-calendar"
								required="true" />
						</div>
					</div>
					<div class='col-sm-2'>
						<div class='form-group'>
							<label>To Date</label>
							<form:input path="toDate" id="toDate"
								class="date-picker form-control glyphicon glyphicon-calendar"
								required="true" />
						</div>
					</div>
					<div class='col-sm-2'>
						<div class='form-group'>
							<label>Buyer</label>
							<form:select path="buyer" id="buyer" class="form-control"
								required="true">
								<%-- <form:option value="None">Please Select</form:option> --%>
								<form:options items="${buyersList}" />
							</form:select>
						</div>
					</div>
					<div class='col-sm-2'>
						<div class='form-group'>
							<label>Status Type</label>
							<form:select path="status" id="status" cssClass="form-control">
								<%-- <form:option value="None">Select</form:option> --%>
								<form:options items="${statusTypes}" />
							</form:select>
						</div>
					</div>
					<div class='col-sm-2'>
						<div class='form-group'>
							<label style="display: block; color: #f5f5f5">. </label>
							<!-- added for alignment purpose, dont delete -->
							<input type="submit" class="btn btn-success form-control"
								value="Submit" />
						</div>
					</div>
				</div>
				<c:if test="${errorDialogue == 'FAILURE'}">
					<div class='row'>
						<div class='col-sm-4'>
							<div class='form-group'>
								<label style="color: red; margin-top: 2px;">No Purchse
									Orders Found</label>
							</div>
						</div>
					</div>
				</c:if>

			</fieldset>
		</form:form>
	</div>

	<!-- result part -->
	<c:if test="${fn:length(myshipTrackItemList) > 0}">
		<c:set var="sap" value="${sapposearchparams}"></c:set>
		<c:set var="actionuser" value="${loggedinuser}"></c:set>
		<div class="container-fluid table-responsive">

			<div class="row" style="margin-left: 1px;">
				<form:form action="shipmentdetaildatapostservlet" method="post"
					commandName="" id="myform">

					<div class="col-md-12">
						<table class="table table-striped" id="adminTable">

							<thead style="background-color: #222534; color: #fff;">
								<tr>
									<th>HBL NO.</th>
									<th>PO NO.</th>
									<th style="display: none;">SO NO.</th>
									<th style="display: none;">BL DATE</th>
									<th style="display: none;">ITEM NO.</th>
									<th style="display: none;">STYLE</th>
									<th style="display: none;">SIZE</th>
									<th style="display: none;">COLOR</th>
									<th style="display: none;">PIECES</th>
									<th style="display: none;">CARTON QTY.</th>
									<th style="display: none;">GROSS WT.</th>
									<th style="display: none;">VOLUME</th>
									<th style="display: none;">EXP. CH DT.</th>
									<th style="display: none;">BUYER NO.</th>
									<th style="display: none;">ACTION USER</th>
									<th style="display: none;">SHIPPER NO.</th>
									<th style="display: none;">BUYER NAME</th>
									<th style="display: none;">SHIPPER NAME</th>
									<th>INDC DATE</th>
									<th>PO REQ. SHIP. DATE</th>
									<th>FVSL NAME</th>
									<th>FVSL ETD</th>
									<th>MVSL NAME</th>
									<th>MVSL ETA</th>
									<th>CARRIER</th>
									<th>STATUS</th>
									<th>REMARKS</th>
									<th>SELECT</th>
								</tr>
							</thead>
							<tbody id="accordion">
								<c:forEach items="${myshipTrackItemList}" var="mti"
									varStatus="index">
									<tr id="table#${index.count}" class="tableRow">
										<td class="blno" id="blno#${index.count}"
											style="font-size: 12px;">${mti.bl_no}</td>
										<td class="pono" style="font-size: 12px;">${mti.po_no}<a
											data-toggle="collapse" href="#${index.count}"
											onclick="hideexpand(this);"><span id="#${index.count}"
												class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a></td>
										<td class="sono" style="font-size: 12px; display: none;">${mti.so_no}</td>
										<fmt:parseDate value="${mti.item_bl_dt}" var="bldate"
											pattern="yyyyMMdd" />
										<td class="item_bl_dt" style="font-size: 12px; display: none;"><fmt:formatDate
												pattern="dd.MM.yyyy" type="date" value="${bldate}" /></td>
										<td class="item_no" style="font-size: 12px; display: none;">${mti.item_no}</td>
										<td class="style" style="font-size: 12px; display: none;">${mti.style}</td>
										<td class="size" style="font-size: 12px; display: none;">${mti.size_no}</td>
										<td class="color" style="font-size: 12px; display: none;">${mti.color}</td>
										<td class="item_pcs" style="font-size: 12px; display: none;">${mti.item_pcs}</td>
										<td class="item_qty" style="font-size: 12px; display: none;">${mti.item_qty}</td>
										<td class="item_grwt" style="font-size: 12px; display: none;">${mti.item_grwt}</td>
										<td class="item_vol" style="font-size: 12px; display: none;">${mti.item_vol}</td>
										<fmt:parseDate value="${mti.item_chdt}" var="chDate"
											pattern="yyyyMMdd" />
										<td class="item_chdt" style="font-size: 12px; display: none;"><fmt:formatDate
												pattern="dd.MM.yyyy" type="date" value="${chDate}" /></td>
										<td class="buyerno" style="font-size: 12px; display: none;">${sap.buyer}</td>
										<td class="actionuser" style="font-size: 12px; display: none;">${actionuser}</td>
										<td class="shipperno" style="font-size: 12px; display: none;">${mti.shipper_no}</td>
										<td class="buyername" style="font-size: 12px; display: none;">${mti.buyer_name}</td>
										<td class="shippername"
											style="font-size: 12px; display: none;">${mti.shipper_name}</td>
										<td><input
											class="date-picker form-control glyphicon glyphicon-calendar indcdt"
											type="text" id="indcdt#${index.count}"
											onchange="copyRowAll(${index.count}, this)"></td>
										<td><input
											class="date-picker form-control glyphicon glyphicon-calendar poreqdt"
											type="text" id="poreqdt#${index.count}"></td>
										<td><input class="fvsl form-control" type="text"
											id="fvsl#${index.count}"></td>
										<td><input
											class="fetd date-picker form-control glyphicon glyphicon-calendar"
											type="text" id="fetd#${index.count}"></td>
										<td><input class="mvsl form-control" type="text"
											id="mvsl#${index.count}"></td>
										<td><input
											class="meta date-picker form-control glyphicon glyphicon-calendar"
											type="text" id="meta#${index.count}"></td>
										<td><input class="car form-control" type="text"
											id="car#${index.count}"></td>
										<td>
											<div class="form-group" style="width: 85px;">
												<select class="sta form-control" id="sta#${index.count}"
													onchange="statusChange('${index.count}');">
													<option selected value="PENDING">PENDING</option>
													<option value="APPROVED">APPROVED</option>
													<option value="REJECTED">REJECTED</option>
												</select>
											</div>
										</td>
										<td><input class="rem form-control" type="text"
											id="rem#${index.count}"></td>
										<td style="text-align: center"><input type="checkbox"
											id="select#${index.count}" name="check1" class="checker"
											value="123" disabled /></td>
									</tr>

									<tr class="hide">
										<td colspan="12">
											<div class="collapse" id="${index.count}">
												<table class="table table-striped"
													style="width: 80%; margin-left: 115px;">
													<tr style="background-color: #337ab7; color: white;">
														<th>Style</th>
														<th>Size</th>
														<th>Color</th>
														<th>Pieces</th>
														<th>Carton Qty.</th>
														<th>Gross Wt.</th>
														<th>Volume</th>
														<th>Exp. CH Dt.</th>
													</tr>
													<tr>
														<td style="font-size: 12px;">${mti.style}</td>
														<td style="font-size: 12px;">${mti.size_no}</td>
														<td style="font-size: 12px;">${mti.color}</td>
														<td style="font-size: 12px;">${mti.item_pcs}</td>
														<td style="font-size: 12px;">${mti.item_qty}</td>
														<td style="font-size: 12px;">${mti.item_grwt}</td>
														<td style="font-size: 12px;">${mti.item_vol}</td>
														<fmt:parseDate value="${mti.item_chdt}" var="chDate"
															pattern="yyyyMMdd" />
														<td style="font-size: 12px;"><fmt:formatDate
																pattern="dd.MM.yyyy" type="date" value="${chDate}" /></td>
													</tr>
												</table>
											</div>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
					</div>

					<div class='row'>
						<div class='col-sm-10'></div>
						<div class='col-sm-2'>
							<input type="submit" class="btn btn-success form-control"
								value="Save" />
						</div>
					</div>

				</form:form>
			</div>
		</div>
	</c:if>

	<!-- RESULT PART OF ORACLE -->
	<c:if test="${fn:length(myshipTrackItemListOracle) > 0}">
		<c:set var="sap" value="${sapposearchparams}"></c:set>
		<c:set var="actionuser" value="${loggedinuser}"></c:set>
		<div class="container-fluid table-responsive">

			<div class="row" style="margin-left: 1px;">
				<form:form action="shipmentdetaildatapostservlet" method="post"
					commandName="" id="myform">

					<table class="table table-striped" id="adminTable">

						<thead style="background-color: #222534; color: #fff;">
							<tr>
								<th>HBL NO.</th>
								<th>PO NO.</th>
								<th style="display: none;">SO NO.</th>
								<th style="display: none;">BL DATE</th>
								<th style="display: none;">ITEM NO.</th>
								<th style="display: none;">STYLE</th>
								<th style="display: none;">SIZE</th>
								<th style="display: none;">COLOR</th>
								<th style="display: none;">PIECES</th>
								<th style="display: none;">CARTON QTY.</th>
								<th style="display: none;">GROSS WT.</th>
								<th style="display: none;">VOLUME</th>
								<th style="display: none;">EXP. CH DT.</th>
								<th style="display: none;">BUYER NO.</th>
								<th style="display: none;">ACTION USER</th>
								<th style="display: none;">SHIPPER NO.</th>
								<th style="display: none;">BUYER NAME</th>
								<th style="display: none;">SHIPPER NAME</th>
								<th>INDC DATE</th>
								<th>PO REQ. SHIP. DATE</th>
								<th>FVSL NAME</th>
								<th>FVSL ETD</th>
								<th>MVSL NAME</th>
								<th>MVSL ETA</th>
								<th>CARRIER</th>
								<th>STATUS</th>
								<th>REMARKS</th>
								<th>SELECT</th>
							</tr>
						</thead>
						<tbody id="accordion">
							<c:forEach items="${myshipTrackItemListOracle}" var="mti"
								varStatus="index">
								<tr id="123" class="tableRow">
									<td class="blno" style="font-size: 12px;">${mti.bl_no}</td>
									<td class="pono" style="font-size: 12px;">${mti.po_no}<a
										data-toggle="collapse" href="#${index.count}"
										onclick="hideexpand(this);"><span id="#${index.count}"
											class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span></a></td>
									<td class="sono" style="font-size: 12px; display: none;">${mti.so_no}</td>
									<c:set var="bldate" value="${mti.item_bl_dt}" />
									<td class="item_bl_dt" style="font-size: 12px; display: none;"><fmt:formatDate
											pattern="dd.MM.yyyy" value="${bldate}" /></td>
									<td class="item_no" style="font-size: 12px; display: none;">${mti.item_no}</td>
									<td class="style" style="font-size: 12px; display: none;">${mti.style}</td>
									<td class="size" style="font-size: 12px; display: none;">${mti.size_no}</td>
									<td class="color" style="font-size: 12px; display: none;">${mti.color}</td>
									<td class="item_pcs" style="font-size: 12px; display: none;">${mti.item_pcs}</td>
									<td class="item_qty" style="font-size: 12px; display: none;">${mti.item_qty}</td>
									<td class="item_grwt" style="font-size: 12px; display: none;">${mti.item_grwt}</td>
									<td class="item_vol" style="font-size: 12px; display: none;">${mti.item_vol}</td>
									<c:set var="chdate" value="${mti.item_chdt}" />
									<td class="item_chdt" style="font-size: 12px; display: none;"><fmt:formatDate
											pattern="dd.MM.yyyy" value="${chdate}" /></td>
									<td class="buyerno" style="font-size: 12px; display: none;">${sap.buyer}</td>
									<td class="actionuser" style="font-size: 12px; display: none;">${actionuser}</td>
									<td class="shipperno" style="font-size: 12px; display: none;">${mti.shipper_no}</td>
									<td class="buyername" style="font-size: 12px; display: none;">${mti.buyer_name}</td>
									<td class="shippername" style="font-size: 12px; display: none;">${mti.shipper_name}</td>
									<c:set var="indcdate" value="${mti.indc_dt}" />
									<td><input
										class="date-picker form-control glyphicon glyphicon-calendar indcdt"
										type="text" id="indcdt#${index.count}"
										value="<fmt:formatDate pattern = "dd.MM.yyyy" value = "${indcdate}" />"></td>
									<c:set var="poreqdate" value="${mti.po_req_ship_dt}" />
									<td><input
										class="date-picker form-control glyphicon glyphicon-calendar poreqdt"
										type="text" id="poreqdt#${index.count}"
										value="<fmt:formatDate pattern = "dd.MM.yyyy" value = "${poreqdate}" />"></td>
									<td><input class="fvsl form-control" type="text"
										id="fvsl#${index.count}" value="${mti.fvsl_name}"></td>
									<c:set var="fetd" value="${mti.fetd}" />
									<td><input
										class="fetd date-picker form-control glyphicon glyphicon-calendar"
										type="text" id="fetd#${index.count}"
										value="<fmt:formatDate pattern = "dd.MM.yyyy" value = "${fetd}" />"></td>
									<td><input class="mvsl form-control" type="text"
										id="mvsl#${index.count}" value="${mti.mvsl_name}"></td>
									<c:set var="meta" value="${mti.meta}" />
									<td><input
										class="meta date-picker form-control glyphicon glyphicon-calendar"
										type="text" id="meta#${index.count}"
										value="<fmt:formatDate pattern = "dd.MM.yyyy" value = "${meta}" />"></td>
									<td><input class="car form-control" type="text"
										id="car#${index.count}" value="${mti.carrier_name}"></td>
									<td>
										<div class="form-group" style="width: 85px;">
											<c:set var="sts" value="${sap.status}" />
											<c:choose>
												<c:when test="${sts eq 'APPROVED'}">
													<select class="sta form-control" id="sta#${index.count}">
														<option selected value="APPROVED">APPROVED</option>
														<option value="REJECTED">REJECTED</option>
													</select>
												</c:when>
												<c:when test="${sts eq 'REJECTED'}">
													<select class="sta form-control" id="sta#${index.count}">
														<option selected value="REJECTED">REJECTED</option>
														<option value="APPROVED">APPROVED</option>
													</select>
												</c:when>
												<c:otherwise>
													<select class="sta form-control" id="sta#${index.count}">
														<option selected value="PENDING">PENDING</option>
														<option value="APPROVED">APPROVED</option>
														<option value="REJECTED">REJECTED</option>
													</select>
												</c:otherwise>
											</c:choose>

										</div>
									</td>
									<td><input class="rem form-control" type="text"
										id="rem#${index.count}" value="${mti.remarks}"></td>
									<td style="text-align: center"><input type="checkbox"
										name="check1" class="checker" value="123" /></td>
								</tr>

								<tr class="hide">
									<td colspan="12">
										<div class="collapse" id="${index.count}">
											<table class="table table-striped"
												style="width: 80%; margin-left: 115px;">
												<tr style="background-color: #337ab7; color: white;">
													<th>Style</th>
													<th>Size</th>
													<th>Color</th>
													<th>Pieces</th>
													<th>Carton Qty.</th>
													<th>Gross Wt.</th>
													<th>Volume</th>
													<th>Exp. CH Dt.</th>
												</tr>
												<tr>
													<td style="font-size: 12px;">${mti.style}</td>
													<td style="font-size: 12px;">${mti.size_no}</td>
													<td style="font-size: 12px;">${mti.color}</td>
													<td style="font-size: 12px;">${mti.item_pcs}</td>
													<td style="font-size: 12px;">${mti.item_qty}</td>
													<td style="font-size: 12px;">${mti.item_grwt}</td>
													<td style="font-size: 12px;">${mti.item_vol}</td>
													<c:set var="chdate" value="${mti.item_chdt}" />
													<td style="font-size: 12px;"><fmt:formatDate
															pattern="dd.MM.yyyy" value="${chdate}" /></td>
												</tr>
											</table>
										</div>
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
					<div class='row'>
						<div class='col-sm-10'></div>
						<div class='col-sm-2'>
							<input type="submit" class="btn btn-success form-control"
								value="Save" />
						</div>
					</div>

				</form:form>
			</div>
		</div>
	</c:if>
</section>

<!-- ----------------------------- -->
<%@include file="footer_v2_fixed.jsp"%>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/buttons.flash.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jszip.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/pdfmake.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/vfs_fonts.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/buttons.html5.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/buttons.print.min.js"></script>

<script>
	$(document).ready(function() {

		$('.date-picker').each(function() {
		    $(this).removeClass('hasDatepicker').datepicker({
		        dateFormat: "dd.mm.yy"
		    });
		});
		
		$("#myform").submit(function (event) { // function to process submitted table
            var tableData = []; // we will store rows' data into this array
            $("#adminTable") // select table by id
                    .find(".tableRow") // select rows by class
                    .has('input[type="checkbox"]:checked') // select only rows with checked checkboxes
                    .each(function () { // for each selected row extract data               
                        var tableRow = {};
                        var jRow = $(this);
                        tableRow.blno = jRow.find('td.blno').text();
                        tableRow.pono = jRow.find('td.pono').text();                        
                        tableRow.sono = jRow.find('td.sono').text();
                        tableRow.item_bl_dt = jRow.find('td.item_bl_dt').text();
                        tableRow.item_no = jRow.find('td.item_no').text();
                        tableRow.style = jRow.find('td.style').text();
                        tableRow.size = jRow.find('td.size').text();
                        tableRow.color = jRow.find('td.color').text();
                        tableRow.item_pcs = jRow.find('td.item_pcs').text();
                        tableRow.item_qty = jRow.find('td.item_qty').text();
                        tableRow.item_grwt = jRow.find('td.item_grwt').text();
                        tableRow.item_vol = jRow.find('td.item_vol').text();
                        tableRow.item_chdt = jRow.find('td.item_chdt').text();
                        tableRow.buyerno = jRow.find('td.buyerno').text();
                        tableRow.actionuser = jRow.find('td.actionuser').text();
                        tableRow.shipperno = jRow.find('td.shipperno').text();
                        tableRow.buyername = jRow.find('td.buyername').text();
                        tableRow.shippername = jRow.find('td.shippername').text();
                        tableRow.indcdt = jRow.find('input.indcdt').val();
                        tableRow.poreqdt = jRow.find('input.poreqdt').val();
                        tableRow.fvsl = jRow.find('input.fvsl').val();
                        tableRow.fetd = jRow.find('input.fetd').val();
                        tableRow.mvsl = jRow.find('input.mvsl').val();
                        tableRow.meta = jRow.find('input.meta').val();
                        tableRow.car = jRow.find('input.car').val();
                        tableRow.sta = jRow.find('select.sta').val();
                        tableRow.rem = jRow.find('input.rem').val();
                        tableData.push(tableRow);
                    });

/*             $.post(
            		"/MyShipmentFrontApp/saveBookedPOApprovalData",
                    {tableData: tableData},
                    function () {
                        alert("Success!");
                    },
                    "json"
            ); */
            $.LoadingOverlay("show");
            
            $.ajax({
/*                 type: "POST",
                //data: JSON.stringify(tableData),
                url: myContextPath + '/saveBookedPOApprovalData',
                //contentType: "application/json; charset=utf-8",                
                //dataType: 'json',
                //timeout: 600000,
                data : {
                	myArray: tableData
                }, */
                url : myContextPath + '/saveBookedPOApprovalData',
				data : JSON.stringify(tableData),
				type : "POST",
				contentType : 'application/json',
                success: function (data) {
                	//alert("SUCCESS");
                	window.location = myContextPath + '/bookedpoapprovalsuccess';
					$.LoadingOverlay("hide");
                },
                error: function (e) {
                	alert("FAILED");
                	console.log("ERROR: ", e);
                	alert(e);
                }
       		});
            
            event.preventDefault(); //Prevent sending form by browser
        }
	);
	});
/* 	$('#po_tables').DataTable({
		responsive : true,
		dom : 'Bfrtip',
		buttons : ['csv','excel' 
		            
		            
		            ]
	}); */

	function statusChange(id) {
		
		var e = document.getElementById("sta#"+id);
		var status = e.options[e.selectedIndex].value;
		
		if(status == "PENDING") {
			document.getElementById("select#"+id).checked = false;
			document.getElementById("select#"+id).disabled = true;
		} else {
			document.getElementById("select#"+id).disabled = false;
		}
		
	}

	function copyRowAll(id, field) {
		
		var fieldValue = field.value;
		var blnoFieldValue = document.getElementsByClassName("blno")[id-1].innerHTML;
		$("#adminTable")
        .find(".tableRow")
        .each(function () {
        	var actualRowId = this.id.split('#');
        	var rowId = actualRowId[1];
        	if(rowId != id) {
        		var jRow = $(this);
            	var blnoValue = jRow.find("td.blno").text();
            	var indcDateValue = jRow.find('input.indcdt').val();
            	if(blnoValue == blnoFieldValue) {
            		if(indcDateValue != "") {
            			if(fieldValue == indcDateValue) {
                			document.getElementById('fvsl#1').value = '';
                			
                			document.getElementById('poreqdt#'+id).value = jRow.find('input.poreqdt').val();
                			document.getElementById('fvsl#'+id).value = jRow.find('input.fvsl').val();
                			document.getElementById('fetd#'+id).value = jRow.find('input.fetd').val();
                			document.getElementById('mvsl#'+id).value = jRow.find('input.mvsl').val();
                			document.getElementById('meta#'+id).value = jRow.find('input.meta').val();
                			document.getElementById('car#'+id).value = jRow.find('input.car').val();
                			document.getElementById('sta#'+id).value = jRow.find('select.sta').val();
                			document.getElementById('rem#'+id).value = jRow.find('input.rem').val();
                			return false;
                		}
            		}
            		
            	}
        	}
        });
		
	}

	function hideexpand(varobj){
		var res=varobj.href.split("#")
		//alert(res[1]);
		var $myGroup = $('#accordion');
		//var toggleicon = $myGroup.children('.collapsed');		   
		//alert(toggleicon);
		$($myGroup).find('.collapse').each(function(){
		if(res[1]==$(this).attr('id')){
			$(this).closest("tr").toggleClass("hide");
		    $(this).toggle();
		    
		    
 		//alert("entering");
 		//alert(document.getElementById("myicon"));
 		//document.getElementById("#"+res[1]).className = "glyphicon glyphicon-chevron-up";
		//alert(document.getElementById("myicon"));
			if(document.getElementById("#"+res[1]).className == "glyphicon glyphicon-chevron-down"){
				//alert("entering");
		    	//document.getElementById("#"+res[1]).removeClass();
		    	document.getElementById("#"+res[1]).className = "glyphicon glyphicon-chevron-up";
			}else{
				//document.getElementById("#"+res[1]).removeClass();
		    	document.getElementById("#"+res[1]).className = "glyphicon glyphicon-chevron-down";
			}
		}else{
		   //alert($(this).attr('id'))
		   //$(this).slideUp( "slow", "linear" )
		   //$(this).closest("tr").addClass("hide");
		   //$(this).hide();
		}
		//toggleicon.html("<span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span>");
		});
	}
	
</script>