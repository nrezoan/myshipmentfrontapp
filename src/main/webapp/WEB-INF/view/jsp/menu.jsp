<section class="booking-table-heading-area no-print">
<script type="text/javascript">
function renderMenuItems(data)
{
	
	var $menuContainer=$('<ul class="table-h-nav"></ul>');
	var menuBooking='';
	var menutracking='';
	var report='';
	report='<li role="presentation" class="dropdown" id="report-menu">';
	report=report+'<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Report Management<span class="caret"></span></a>';
	var menuPO='';
	var nonNegoSea='<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getSeaBillOfLandingPage\',\'Non Nego Bill of Lading\');\">Non Nego Bill of Lading</a></li>';
	var nonNegoAir='<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getAirBillOfLandingPage\',\'Non Nego Bill of Lading\');\">Non Nego Bill of Lading</a></li>';
	var exception='<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getExceptionReportPage\',\'Exception\');\">Exception</a></li>';
	
	//var freqAsked='<li role="presentation" class="dropdown">';
	var freqAsked='';
	freqAsked=freqAsked+'<li role="presentation" class="dropdown" id="frequentUsed-menu" ><a class="dropdown-toggle" data-toggle="dropdown" href="#"'+'role="button" aria-haspopup="true" aria-expanded="false">Favourites<span class="caret"></span></a><ul class="dropdown-menu">';
	
	if(data.frequentUsedMenuList!=null){
		var frqMenuobj = data.frequentUsedMenuList;
		for(var i=0;i<frqMenuobj.length;i++){
			var lMenuWithCounterStr = frqMenuobj[i].menuName+"&nbsp;&nbsp;";
			//+frqMenuobj[i].counter
			freqAsked=freqAsked+'<li><a  href="javascript:;" onclick="javascript:savefavoriteAndRedirect(\''+frqMenuobj[i].url+'\',\''+frqMenuobj[i].menuName+'\');\">'+lMenuWithCounterStr+'</a></li><br>';
		}
		freqAsked=freqAsked+'</ul></li>';
	}
	if(data.accgroup=='ZMSP' || data.accgroup=='ZMFF')
		{
		menuBooking='<li role="presentation" class="dropdown" id="booking-menu" ><a class="dropdown-toggle" data-toggle="dropdown" href="#"'+'role="button" aria-haspopup="true" aria-expanded="false">Booking<span class="caret"></span></a><ul class="dropdown-menu">';
		if(data.disChnlSelected=='EX')
		{
			
			 menuBooking=menuBooking+'<li><a  href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'orderheader\',\'Direct Booking\');\">Direct Booking</a></li><br>';
			 menuBooking=menuBooking+'<li><a  href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'buyerwiseposearch\',\'Booking As Per PO\');\");>Booking As Per PO</a></li><br>';
			 menuBooking=menuBooking+'<li><a  href="http://121.200.242.134/MyshipmentWT/web/OrderBookingPerPO.action?orderHeader.shipmentMode=SE&t=1495608794371");>Booking As Per PO Old</a></li><br>';
			 menuBooking=menuBooking+'<li><a  href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'doinspectionbooking\',\'Inspection Booking\');\">Inspection Booking</a></li><br>';
			 //menuBooking=menuBooking+'<li><a  href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'loadpodashboard\',\'Booking Template\');\">Booking Template</a></li><br>';
			 //menuBooking=menuBooking+'<li><a  href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'loadpodashboard\',\'C&F Booking\');\">C&F Booking</a></li><br>';
			 menuBooking=menuBooking+'<li><a  href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getBookingDisplayPage\',\'Booking Update\');\">Booking Update</a></li><br>';
			 menuBooking=menuBooking+'</ul></li>';
			
		}
		menutracking='<li role="presentation" class="dropdown" id="tracking-menu" ><a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Tracking<span class="caret"></span></a> <ul class="dropdown-menu">'
			menutracking=menutracking+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getPoTrackingPage\',\'PO Tracking\');\">PO Tracking</a></li>';
			menutracking=menutracking+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getCommInvTrackingPage\',\'Comm. INV Tracking\');\">Comm. INV Tracking</a></li>';
			//menutracking=menutracking+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'loadpodashboard\',\'Container Tracking\');\">Container Tracking</a></li>';
			menutracking=menutracking+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getTrackingSeaPage\',\'HBL Wise Tracking\');\">HBL Wise Tracking</a></li>';
			menutracking=menutracking+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'mblWiseTrackingSearchPage\',\'MBL Wise Tracking\');\">MBL Wise Tracking</a></li>';
			//menutracking=menutracking+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'containerTracking\',\'Container Tracking\');\">Container Tracking</a></li>';
			menutracking=menutracking+'</ul></li>';
			
			report=report+'<ul class="dropdown-menu">';
			if(data.disChnlSelected=='EX')
				{
				report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'stuffingAdvice\',\'Stuffing Report\');\">Stuffing Report</a></li>';
				if(data.divisionSelected=='SE')
				{
				report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getSeaExportInvoice\',\'Freight Invoice\');\">Freight Invoice</a></li>';
				}
			    if(data.divisionSelected=='AR')
				{
			    	report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getAirExportInvoice\',\'Freight Invoice\');\">Freight Invoice</a></li>';
				}
				report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getShippingOrderPage\',\'Shipping Order\');\">Shipping Order</a></li>';
				report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getCommInvoicePage\',\'Commercial Invoice\');\">Commercial Invoice</a></li>';
				//report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'loadpodashboard\',\'Packing List\');\">Packing List</a></li>';
				report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getCargoAckPage\',\'Cargo Ack. Certificate\');\">Cargo Ack. Certificate</a></li>';
				//report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'loadpodashboard\',\'Warehouse Report\');\">Warehouse Report</a></li>';
				//report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'loadpodashboard\',\'Container Load\');\">Container Load</a></li>';
				report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getExceptionReportPage\',\'PO Exception Report\');\">PO Exception Report</a></li>';
				report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getPoTrackingReportPage\',\'PO Detail\');\">PO Detail</a></li>';
				report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getInspectionTrackingPage\',\'Inspection Tracking\');\">Inspection Tracking</a></li>';
				report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getLastNShipmentPage\',\'Last N Shipment\');\">Last N Shipment</a></li>';
				
				
				}
			if(data.disChnlSelected=='IM')
				{
				report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'loadpodashboard\',\'Descrepency Report\');\">Descrepency Report</a></li>';
				report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'loadpodashboard\',\'NOC\');\"> NOC</a></li>';
				report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'loadpodashboard\',\'Dr/Cr Report\');\">Dr/Cr Report</a></li>';
				report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'loadpodashboard\',\'Lifting Invoice\');\">Lifting Invoice</a></li>';
				report=report+'<li><a href="${pageContext.request.contextPath}/loadpodashboard">Delivery/Undelivery</a></li>';
				report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'loadpodashboard\',\'Freight Invoice\');\">Freight Invoice</a></li>';
				}
			report=report+'</ul>';
			
			
		}
	if(data.accgroup=='ZMBY' || data.accgroup=='ZMBH')
		{
		menuPO='<li role="presentation" class="dropdown" id="po--menu">';
		menuPO=menuPO+'<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"> Po Management <span class="caret"></span> </a>';
		menuPO=menuPO+'<ul class="dropdown-menu">';
		menuPO=menuPO+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getPoUploadPage\',\'Create PO\');\" >Create PO</a></li></br>';
		menuPO=menuPO+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getPoUploadPage\',\'Upload PO\';\">Upload PO</a></li></br>';
		menuPO=menuPO+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getSearchPoPage\',\'Update PO\');\" >Update PO</a></li></br>';
		menuPO=menuPO+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getassignsearch\',\'Assign Carrier and Schedule\');\" >Assign Carrier and Schedule</a></li></br>';
		menuPO=menuPO+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'approvepo\',\'Approve PO\');\" >Approve PO</a></li></br>';
		menuPO=menuPO+'</ul></li>';
		
		menutracking='<li role="presentation" class="dropdown" id="tracking-menu" ><a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Tracking<span class="caret"></span></a> <ul class="dropdown-menu">'
			menutracking=menutracking+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getPoTrackingPage\',\'PO Tracking\');\" >PO Tracking</a></li>';
			menutracking=menutracking+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getCommInvTrackingPage\',\'Comm. INV Tracking\');\">Comm. INV Tracking</a></li>';
			//menutracking=menutracking+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'loadpodashboard\',\'Container Tracking\');\">Container Tracking</a></li>';
			menutracking=menutracking+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getTrackingSeaPage\',\'HBL Wise Tracking\');\" >HBL Wise Tracking</a></li>';
			menutracking=menutracking+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'mblWiseTrackingSearchPage\',\'Container Tracking\');\">MBL Wise Tracking</a></li>';
			menutracking=menutracking+'</ul></li>';
			
			report=report+'<ul class="dropdown-menu">';
			if(data.disChnlSelected=='EX')
				{
				report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'stuffingAdvice\',\'Stuffing Report\');\">Stuffing Report</a></li>';
				//report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getSeaExportInvoice\',\'Freight Invoice\');\">Freight Invoice</a></li>';
				//report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getShippingOrderPage\',\'Shipping Order\');\">Shipping Order</a></li>';
				//report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getCommInvoicePage\',\'Commercial Invoice\');\">Commercial Invoice</a></li>';
				//report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'loadpodashboard\',\'Packing List\');\">Packing List</a></li>';
				//report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getCargoAckPage\',\'Cargo Ack. Certificate\');\">Cargo Ack. Certificate</a></li>';
				//report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'loadpodashboard\',\'Warehouse Report\');\">Warehouse Report</a></li>';
				//report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'loadpodashboard\',\'Container Load\');\">Container Load</a></li>';
				report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getExceptionReportPage\',\'PO Exception Report\');\">PO Exception Report</a></li>';
				report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getPoTrackingReportPage\',\'PO Detail\');\">PO Detail</a></li>';
				report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getLastNShipmentPage\',\'Last N Shipment\');\">Last N Shipment</a></li>';
					if(data.accgroup=='ZMBH') {
						report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getInspectionTrackingPage\',\'Inspection Tracking\');\">Inspection Tracking</a></li>';						
					}					
				}
			if(data.disChnlSelected=='IM')
				{
				report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'loadpodashboard\',\'Descrepency Report Report\);\">Descrepency Report Report</a></li>';
				report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'loadpodashboard\',\'NOC\);\"> NOC</a></li>';
				report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'loadpodashboard\',\'Dr/Cr Report\);\">Dr/Cr Report</a></li>';
				report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'loadpodashboard\',\'Lifting Invoice\);\">Lifting Invoice</a></li>';
				report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'loadpodashboard\',\'Delivery/Undelivery\);\">Delivery/Undelivery</a></li>';
				report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'loadpodashboard\',\'Freight Invoice\);\">Freight Invoice</a></li>';
				}
			report=report+'</ul>';
		
		}
 	if(data.accgroup=='ZMDA' || data.accgroup=='ZMOA')
	{
	menuPO='<li role="presentation" class="dropdown" id="po--menu">';
	menuPO=menuPO+'<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false"> Po Management <span class="caret"></span> </a>';
	menuPO=menuPO+'<ul class="dropdown-menu">';
	menuPO=menuPO+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getPoUploadPage\',\'Create PO\');\" >Create PO</a></li></br>';
	menuPO=menuPO+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getPoUploadPage\',\'Upload PO\';\">Upload PO</a></li></br>';
	menuPO=menuPO+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getSearchPoPage\',\'Update PO\');\" >Update PO</a></li></br>';
	//menuPO=menuPO+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getassignsearch\',\'Assign Carrier and Schedule\');\" >Assign Carrier and Schedule</a></li></br>';
	//menuPO=menuPO+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'approvepo\',\'Approve PO\');\" >Approve PO</a></li></br>';
	menuPO=menuPO+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'approvebookedpo\',\'Booked PO Approval\');\" >Booked PO Approval</a></li></br>';
	menuPO=menuPO+'</ul></li>';
	
	menutracking='<li role="presentation" class="dropdown" id="tracking-menu" ><a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Tracking<span class="caret"></span></a> <ul class="dropdown-menu">'
		menutracking=menutracking+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getPoTrackingPage\',\'PO Tracking\');\" >PO Tracking</a></li>';
		menutracking=menutracking+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getCommInvTrackingPage\',\'Comm. INV Tracking\');\">Comm. INV Tracking</a></li>';
		//menutracking=menutracking+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'loadpodashboard\',\'Container Tracking\');\">Container Tracking</a></li>';
		menutracking=menutracking+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getTrackingSeaPage\',\'HBL Wise Tracking\');\" >HBL Wise Tracking</a></li>';
		//menutracking=menutracking+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'mblWiseTrackingSearchPage\',\'Container Tracking\');\">MBL Wise Tracking</a></li>';
		menutracking=menutracking+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'containerTracking\',\'Container Tracking\');\">Container Tracking</a></li>';
		menutracking=menutracking+'</ul></li>';
		
		report=report+'<ul class="dropdown-menu">';
		if(data.disChnlSelected=='EX')
			{
			//report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'stuffingAdvice\',\'Stuffing Report\');\">Stuffing Report</a></li>';
			//report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getSeaExportInvoice\',\'Freight Invoice\');\">Freight Invoice</a></li>';
			//report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getShippingOrderPage\',\'Shipping Order\');\">Shipping Order</a></li>';
			//report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getCommInvoicePage\',\'Commercial Invoice\');\">Commercial Invoice</a></li>';
			//report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'loadpodashboard\',\'Packing List\');\">Packing List</a></li>';
			//report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getCargoAckPage\',\'Cargo Ack. Certificate\');\">Cargo Ack. Certificate</a></li>';
			//report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'loadpodashboard\',\'Warehouse Report\');\">Warehouse Report</a></li>';
			//report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'loadpodashboard\',\'Container Load\');\">Container Load</a></li>';
			report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getExceptionReportPage\',\'PO Exception Report\');\">PO Exception Report</a></li>';
			report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getPoTrackingReportPage\',\'PO Detail\');\">PO Detail</a></li>';
			report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'getLastNShipmentPage\',\'Last N Shipment\');\">Last N Shipment</a></li>';
			report=report+'<li><a href="javascript:;" onclick="\javascript:savefavoriteAndRedirect(\'approvebookedporeport\',\'Booked PO Approval Report\');\">Booked PO Approval Report</a></li>';
			}
		report=report+'</ul>';
	
	} 
	
	report=report+'</li>';
	if(menuBooking!="")
	$menuContainer.append(menuBooking).append('<li><p  id="nav_slash_three">|</p></li>');
	
	if(data.accgroup=='ZMBY' || data.accgroup=='ZMBH'){
		$menuContainer.append(exception).append('<li><p  id="nav_slash_three">|</p></li>');
	}
	if(menutracking!="")
	$menuContainer.append(menutracking).append('<li><p  id="nav_slash_three">|</p></li>');
	if(menuPO!="")
	$menuContainer.append(menuPO).append('<li><p  id="nav_slash_three">|</p></li>');
	if(report!="")
	$menuContainer.append(report).append('<li><p  id="nav_slash_three">|</p></li>');
	if(data.accgroup=='ZMSP' || data.accgroup=='ZMFF' || data.accgroup=='ZMBY' || data.accgroup=='ZMBH') {
		if(nonNegoSea!="" && data.divisionSelected=='SE')
			$menuContainer.append(nonNegoSea).append('<li><p  id="nav_slash_three">|</p></li>');
		if(nonNegoAir!="" && data.divisionSelected=='AR')
			$menuContainer.append(nonNegoAir).append('<li><p  id="nav_slash_three">|</p></li>');
	}	
	$menuContainer.append(freqAsked);
	
	$("#menu-bar").append($menuContainer);

	}
	
	function savefavoriteAndRedirect(_url,_pageName){
		//var url = myContextPath+"/"+_url;
		//window.location=url;
		saveUserFavouritePage(_url,_pageName)
		return true;
	}
	function saveUserFavouritePage(url,pageName){
		$.ajax({
			url : myContextPath + '/saveFrequentUsedForm',
			data : {
				'pageName' : pageName,
				'path' : url
			},
			
			type : "POST",
			success : function(data) {
				//window.location=myContextPath+'/orderdetails';
				//alert("template updated");
				window.location=myContextPath+"/"+url;
				$( "#submitBtn" ).trigger( "click" );
			},
			error : function(jqXHR, textStatus, errorThrown) {
				$("#display-message").removeClass("green");
				$("#display-message").addClass("red");
				$("#display-message").text(
						"An error occured" + textStatus + ':'
						+ errorThrown);
				$("#display-message").show();
				var $overlay = $.LoadingOverlay("hide");
				window.location=myContextPath+"/"+url;
			}
		})
	}
</script>
	<div class="container-fluid">
		<div class="row" style="padding: 0px; margin-left: 0%;background-color:#337ab7">
		<input name="input-hidden" id="input-hidden" type="hidden" value="">
			<div class="col-md-12" id="menu-bar">
				
			</div>
		</div>
	</div>
</section>