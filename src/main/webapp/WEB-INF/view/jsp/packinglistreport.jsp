<jsp:include page="header.jsp"></jsp:include>
<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/packing-report.css">
		<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/print-packing-report.css">
		<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		
		<section class="main-body">
		
<div class="container-fluid">
  <div class="row">
    <div class="col-sm-12">
    	<h3 class="ship_desti">Shipping Order</h3>
       		<div class="pull-right">
			<!--<input type="button" class="btn btn-info" style="margin-top: -25%;font-weight: bolder;border: 1px solid;" value="Print Invoice" onclick="printContent('print-con')" />-->
			<input type="button" class="btn btn-info" style="margin-top: -17%;
    font-weight: bolder;
    border: 1px solid;
    /* border: 2px solid #a1a1a1; */
    padding: 11px 31px;
    /* background: lavenderblush; */
    border-radius: 2px solid;
    border-radius: 6px;
    color: black;
    background-color: white;" value="Print Invoice" onclick="printContent('print-con')" />
			</div>
    </div>
  </div><!--/row-->
  <hr>
  
  
   <div id="print-con" class="report-header col-xs-12">
   
  <div class="row nopadding ship_header_row">
	<div class="col-xs-5 box-view">
	<span>Seller:</span>
	<h4>AMTRANET LIMITED</h4>
	<span>160,West Rajashan, savar, dhaka</span>
	<span>Bangladesh</span>
	</div>
	<div class="col-xs-5 box-view">
	<span>Maker:</span>
	<h4>AMTRANET LIMITED</h4>
	<span>160,West Rajashan, savar, dhaka</span>
	<span>Bangladesh</span>
	</div>
	
	<div class="col-xs-2 box-view">
	<strong>Packing List no:</strong>
	<h6>29/16/100</h6>
	<strong>Date:</strong>
	<h6>09-nov-2016</h6>
	<strong>HBL No:</strong>
	<h6>TML205046</h6>
	<strong>So No:</strong>
	<h6>0001768517</h6>
	</div>
  
  
  </div>
  <div class="row nopadding ship_header_row">
	<div class="col-xs-5 box-view-1">
	<span>Buyer:</span>
	<h4>carerefour Import SAS</h4>
	<span>160,West Rajashan, savar, dhaka</span>
	<span>Bangladesh</span>
	</div>
	<div class="col-xs-5 box-view-1">
	<span>send to (Consignee)::</span>
	<h4>CARREFOUR COMERCIO E INDUSTRIA</h4>
	<span>-TDA RUA GEORGE EASTIVAN,213 i,lORUMBl, CNPJ:45.543.91 5/0001 -81-lLlAN ]hzilSOARES.PHT+55'1 1 3755-2596</span>
	<span>Bangladesh</span>
	</div>
	
	<div class="col-xs-2 box-view-1">
	
	</div>
  
  
  </div>
  <div class="row nopadding ship_header_row">
	
	<div class="col-xs-3 box-view-2">
	<p class="border-bottom">Country of origin:</p>
    <p class="border-bottom">Count of Destination:</p>
	<p class="border-bottom">Incoterm:</p>
	<p class="border-bottom">Mode of Shipment:</p>
	</div>
	
	<div class="col-xs-3 box-view-2">
	<p class="border-bottom">Bangladesh</p>
	<p class="border-bottom">Brazil</p>
	<p class="border-bottom">FCA,Dhaka, SEA</p>
	<p class="border-bottom">By Sea</p>
	</div>
	
	<div class="col-xs-3 box-view-2">
	<p class="border-bottom">Country of origin:</p>
    <p class="border-bottom">Count of Destination:</p>
	<p class="border-bottom">Incoterm:</p>
	<p class="border-bottom">Mode of Shipment:</p>
	</div>
	
	<div class="col-xs-3 box-view-2">
	<p class="border-bottom">Bangladesh</p>
	<p class="border-bottom">Brazil</p>
	<p class="border-bottom">FCA,Dhaka, SEA</p>
	<p class="border-bottom">By Sea</p>
	</div>
  
  
  </div>
  
  <div class="row table_wraper">
	<div class="table-responsive">          
  <table class="table table-bordered" style="margin-bottom: 0px;">
    <thead>
      <tr>
        <th>order No.</th>
        <th>Item Reference</th>
        <th>Product Description</th>
        <th>pack type</th>
        <th>Total Qty.</th>
        <th>Ctn Dimension</th>
        <th>NB of Ctns</th>
        <th>Unit vol</th>
        <th>Net Wt</th>
		<th>Gross Wt</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>000639</td>
        <td>RMG IN CARTON</td>
        <td>SS I.AN IASY SHIR I </td>
        <td></td>
        <td>2400</td>
        <td>47*36*21</td>
        <td>200</td>
        <td>0.36.000</td>
        <td>3.770</td>
		 <td>4.22</td>
      </tr>
	 
    </tbody>
  </table>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>carton No.</th>
        <th>Item Ref</th>
        <th>NB of ctns</th>
        <th>color(s)</th>
        <th colspan="14" style="text-align: center;">Size Assortment</th>
        <th>Pcs/inner</th>
        <th>Pcs/ctn</th>
        <th>Quantity</th>
        
      </tr>
    </thead>
    <tbody>
	<tr rowspan="14">
	<td colspan="4"></td>
	<td>1
		
		<td>2</td>
		<td>3</td>
		<td>4</td>
		<td>5</td>
		<td>6</td>
		<td>7</td>
		<td>8</td>
		<td>9</td>
		<td>10</td>
		<td>11</td>
		<td>12</td>
		<td>13</td>
		<td>14</td>
		</td>
		<td colspan="3"></td>
	</tr>
      <tr>
        <td>01-100</td>
        <td>364SS16005</td>
        <td>100</td>
        <td>BA1</td>
        <td>2
		<td>2</td>
		<td>3</td>
		<td>3</td>
		<td>2</td>
		<td>5</td>
		<td>6</td>
		<td>7</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		
		</td>
        <td></td>
        <td>12</td>
        <td>1200</td>
      
      </tr>
	 <tr>
        <td>01-100</td>
        <td>364SS16005</td>
        <td>100</td>
        <td>BA1</td>
        <td>2
		<td>2</td>
		<td>3</td>
		<td>3</td>
		<td>2</td>
		<td>5</td>
		<td>6</td>
		<td>7</td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		
		</td>
        <td></td>
        <td>12</td>
        <td>1200</td>
      
      </tr>
	  <tr>
	  <td colspan="2">
	  </td>
	  <td>
	  Total: 200
	  </td>
	   <td colspan="17">
	     
	  </td>
	  <td>
	  Total: 2400
	  </td>
	  </tr>
	  
    </tbody>
  </table>
  
  </div>
	<div class="row" style="margin-top: -1.5%;">
<div class="col-xs-12">
		<h5 class="box-view-3">observation:</h5>
	
	
	<div class="row nopadding ship_header_row" style="margin-top: -.9%;" >
	<div class="col-xs-12">
	<div class="col-xs-4 box-view-1">
	<span>Company chop and signature</span>
	<p style="    font-weight: bold;border-top: 1px solid black;margin: 13%;text-align: center;font-size: 13px;">Authorized Signature</p>
	</div>
	<div class="col-xs-4 box-view-1">
	<span>"OK" packing list chop Approval(Carefour use only)</span>
	
	</div>
	
	<div class="col-xs-4 box-view-1">
	 <table class="table table-bordered" style="    margin-top: 3%;">
    <thead>
      <tr>
        <th>total ctns</th>
        <th>Total vol</th>
        <th>Net wgt kg</th>
        <th>Gross wgt kg</th>
       
      </tr>
    </thead>
    <tbody>
	<tr>
	<td>200</td>
	<td>7.29</td>
	<td>700</td>
	<td>851</td>
	</tr>
	</tbody>
	</table>
	</div>
  </div>
  
  </div>
  </div>
  </div>
  </div>
 
  <div class="row" style="height: 70px;"></div>
  <div class="row nopadding ship_header_row">
	<div class="col-xs-5 box-view-5">
	<span>Seller:</span>
	<h4>AMTRANET LIMITED</h4>
	<span>160,West Rajashan, savar, dhaka</span>
	<span>Bangladesh</span>
	</div>
	<div class="col-xs-5 box-view-5">
	<span>Maker:</span>
	<h4>AMTRANET LIMITED</h4>
	<span>160,West Rajashan, savar, dhaka</span>
	<span>Bangladesh</span>
	</div>
	
	<div class="col-xs-2 box-view-5">
	<strong>Packing List no:</strong>
	<h6>29/16/100</h6>
	<strong>Date:</strong>
	<h6>09-nov-2016</h6>
	
	</div>
  
  
  </div>
  
  <div class="row nopadding ship_header_row">
	<div class="col-xs-5 box-view-5">
	<span>Buyer:</span>
	<h4>carerefour Import SAS</h4>
	<span>160,West Rajashan, savar, dhaka</span>
	<span>Bangladesh</span>
	</div>
	<div class="col-xs-5 box-view-5">
	<span>send to (Consignee)::</span>
	<h4>CARREFOUR COMERCIO E INDUSTRIA</h4>
	<span>-TDA RUA GEORGE EASTIVAN,213 i,lORUMBl, CNPJ:45.543.91 5/0001 -81-lLlAN ]hzilSOARES.PHT+55'1 1 3755-2596</span>
	<span>Bangladesh</span>
	</div>
	
	<div class="col-xs-2 box-view-5">
	
	</div>
  
  
  </div>
  <div class="row nopadding ship_header_row">
	
	<div class="col-xs-3 box-view-2">
	<p class="border-bottom">Country of origin:</p>
    <p class="border-bottom">Count of Destination:</p>
	<p class="border-bottom">Incoterm:</p>
	<p class="border-bottom">Mode of Shipment:</p>
	</div>
	
	<div class="col-xs-3 box-view-2">
	<p class="border-bottom">Bangladesh</p>
	<p class="border-bottom">Brazil</p>
	<p class="border-bottom">FCA,Dhaka, SEA</p>
	<p class="border-bottom">By Sea</p>
	</div>
	
	<div class="col-xs-3 box-view-2">
	<p class="border-bottom">Country of origin:</p>
    <p class="border-bottom">Count of Destination:</p>
	<p class="border-bottom">Incoterm:</p>
	<p class="border-bottom">Mode of Shipment:</p>
	</div>
	
	<div class="col-xs-3 box-view-2">
	<p class="border-bottom">Bangladesh</p>
	<p class="border-bottom">Brazil</p>
	<p class="border-bottom">FCA,Dhaka, SEA</p>
	<p class="border-bottom">By Sea</p>
	</div>
  
  
  </div>
  
  

		
		
		<div class="main_body_wraper nopadding">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-8 side_mark">
						<h4>Side Mark</h4>
					</div>
					<div class="col-xs-4 side_mark">
						<h4>Shipping Mark</h4>
					</div>
				</div>
				
				<div class="row">
					<div class="col-xs-8 weight_qty">
						<p>DIMENSIONS: 47 x 36 x 21 CMS</p>
						<p>NET WEIGHT: 3.5 KGS</p>
						<p>GROSS WEIGHT: 4.5 KGS</p>
						<p>COLOR: BLM</p>
						<p>SIZE/QTY:</p>
						
						<table class="table table-bordered" style="margin-bottom: 6px;">
    <tr>
      <th>COLOR</th>
      <th>1</th>
      <th>2</th>
      <th>3</th>
      <th>4</th>
      <th>5</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th>UNITS</th>
     
    </tr>
	<tr>
      <th>BA1</th>
      <th>2</th>
      <th>2</th>
      <th>3</th>
      <th>3</th>
      <th>2</th>
      <th>0</th>
      <th>0</th>
      <th>0</th>
      <th>0</th>
      <th>0</th>
      <th>0</th>
      <th>0</th>
      <th>NaN</th>
      <th>NaN</th>
      <th>NaN</th>
    </tr>
	
	</table>
		<h5>CARTON N#: 01-100</h5>				
	</div>
					<div class="col-xs-3">
					<div class="div_trademark">
						<p>CARREFOUR COMERCIO</p>
						<p>E INDUSTRIAL TDA</p>
						<p>64AMW16BRCA0003</p>
						<p>B64SS16005</p>
						<p>CAMISA MGA LGA</p>
						<p>FANTASIA VESTIR</p>
						<p>Made in Bangladesh</p>
					</div>
					</div>
				</div>
				
				
		<div class="row">
					<div class="col-xs-8 weight_qty">
						<p>DIMENSIONS: 47 x 36 x 21 CMS</p>
						<p>NET WEIGHT: 3.5 KGS</p>
						<p>GROSS WEIGHT: 4.5 KGS</p>
						<p>COLOR: BLM</p>
						<p>SIZE/QTY:</p>
						
						<table class="table table-bordered" style="margin-bottom: 6px;">
    <tr>
      <th>COLOR</th>
      <th>1</th>
      <th>2</th>
      <th>3</th>
      <th>4</th>
      <th>5</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th>UNITS</th>
     
    </tr>
	<tr>
      <th>BA1</th>
      <th>2</th>
      <th>2</th>
      <th>3</th>
      <th>3</th>
      <th>2</th>
      <th>0</th>
      <th>0</th>
      <th>0</th>
      <th>0</th>
      <th>0</th>
      <th>0</th>
      <th>0</th>
      <th>NaN</th>
      <th>NaN</th>
      <th>NaN</th>
    </tr>
	
	</table>
		<h5>CARTON N#: 01-100</h5>				
	</div>
					<div class="col-xs-3">
					<div class="div_trademark">
						<p>CARREFOUR COMERCIO</p>
						<p>E INDUSTRIAL TDA</p>
						<p>64AMW16BRCA0003</p>
						<p>B64SS16005</p>
						<p>CAMISA MGA LGA</p>
						<p>FANTASIA VESTIR</p>
						<p>Made in Bangladesh</p>
					</div>
					</div>
				</div>		
		
		
		<div class="row">
					<div class="col-xs-8 weight_qty">
						<p>DIMENSIONS: 47 x 36 x 21 CMS</p>
						<p>NET WEIGHT: 3.5 KGS</p>
						<p>GROSS WEIGHT: 4.5 KGS</p>
						<p>COLOR: BLM</p>
						<p>SIZE/QTY:</p>
						
						<table class="table table-bordered" style="margin-bottom: 6px;">
    <tr>
      <th>COLOR</th>
      <th>1</th>
      <th>2</th>
      <th>3</th>
      <th>4</th>
      <th>5</th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th></th>
      <th>UNITS</th>
     
    </tr>
	<tr>
      <th>BA1</th>
      <th>2</th>
      <th>2</th>
      <th>3</th>
      <th>3</th>
      <th>2</th>
      <th>0</th>
      <th>0</th>
      <th>0</th>
      <th>0</th>
      <th>0</th>
      <th>0</th>
      <th>0</th>
      <th>NaN</th>
      <th>NaN</th>
      <th>NaN</th>
    </tr>
	
	</table>
		<h5>CARTON N#: 01-100</h5>				
	</div>
					<div class="col-xs-3">
					<div class="div_trademark">
						<p>CARREFOUR COMERCIO</p>
						<p>E INDUSTRIAL TDA</p>
						<p>64AMW16BRCA0003</p>
						<p>B64SS16005</p>
						<p>CAMISA MGA LGA</p>
						<p>FANTASIA VESTIR</p>
						<p>Made in Bangladesh</p>
					</div>
					</div>
				</div>
		
		
		
		
		
		
			</div>
		</div>
  </div>
  
</div>
</div>
		</section>
		
		<section class="footer">
			<div class="container-fluid">
				<div class="row">
					<div class="col-lg-4 col-xs-4 col-sm-12 col-xs-12">
						<p class="footer-para">Copyright &copy MyShipment.com</p>
						
					</div>
					<div class="col-lg-3 col-xs-3 footer-img">
						<img src="img/logo.png" alt="" class=""/>
					</div>
					<div class="col-lg-5 col-xs-5 col-sm-12 col-xs-12 footer_nav">
						<ul class="table-h-nav-footer">
							<li><a href="">Booking</a></li>
							<li><a href="">booking template</a></li>
							<li><a href="">Mis</a></li>
							<li><a href="">frequently used form</a></li>
							<li><a href="">other reports</a></li>
							
							<li><a href="">Update</a></li>
						</ul>
					</div>
				</div>
			</div>
		</section>
		
	
			
		
		

		<!-- jquery
		============================================ -->		
        <script src="js/vendor/jquery-1.11.3.min.js"></script>
		<!-- bootstrap JS
		============================================ -->		
        <script src="js/bootstrap.min.js"></script>
		<!-- plugins JS
		============================================ -->		
        <script src="js/plugins.js"></script>
		<!-- main JS
		============================================ -->
		
<script src="js/canvasjsmin.js"></script>		
        <script src="js/main.js"></script>
		
	<script>
	
function printContent(el){
var restorepage = $('body').html();
var printcontent = $('#' + el).clone();
$('body').empty().html(printcontent);
window.print();
$('body').html(restorepage);
}

	</script>
    </body>
</html>
