<%@page import="com.myshipment.dto.LoginDTO"%>
<%@page import="com.google.gson.Gson"%>
<%
	LoginDTO loginDto = (LoginDTO) (session.getAttribute("loginDetails"));
%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<style>

	.affix {
		top: 0;
		box-shadow: 5px 5px 10px rgba(0, 0, 0, 0.5);
	}
	
	.after-menu {
		background-color: #eaeaea;
	}
	
	.affix + .after-menu {
	    /* padding-top: 3.25%; */
	    padding-top: 3.80%;
	}
	
	.menubar-fixed-top {
	    border-width: 0 0 1px;
	    z-index: 1030;
	}
	
	@media (min-width: 768px) {
		.menubar-fixed-bottom, .menubar-fixed-top {
	    	border-radius: 0;
	}
		.menubar-fixed-bottom, .menubar-fixed-top {
		    z-index: 1030;
		}
	}
	
	.menubar-design-pattern {
		background: #fff;
		/* margin-bottom: -12px; */
		border-bottom: 1px solid #a7a5a5;
		/* box-shadow: 5px 5px 10px rgba(0, 0, 0, 0.5); */
		height: 43px;
		width: -moz-available;
	}
	
	.dropdown-menu-dist {
		min-width: 35px !important;
		border-radius: 0px !important;
		background-color: #0c99d5 !important;
	}
	
	.dropdown-menu-dist>li>a:hover {
		/* background-color: #0c99d5 !important; */
		color: #fff !important;
		cursor: pointer;
	}
	
	.dropdown-menu-dist>li>a.active {
		background-color: #204d74; !important;
		color: #fff !important;
		cursor: pointer;
	}
	
</style>
<body>
<script>
	$.LoadingOverlay("show");
	$(function () {
	    //Initialize Select2 Elements
	    $('.select2').select2();
	});
</script>
	<!-- <div class="entire-content-c"> -->
	<nav class="navbar container-fluid login-nav navbarExcludeDesign"
		style="display: block;">
		<div class="row row-margin-unset-all">
			<div class="col-md-2">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed"
						data-toggle="collapse" data-target="#myshipment-menu">
						<span class="sr-only">Toggle navigation</span> <span
							class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
					<a class="navbar-brand-455723" href="${pageContext.request.contextPath}/homepage"> <img
						src="${pageContext.request.contextPath}/resources/img/logo-xs.png"
						alt="logo">
					</a>
				</div>
			</div>
			<div class="col-md-10 company-details">
				<div class="collapse  row navbar-collapse" id="myshipment-menu">
					<ul class="nav navbar-nav col-md-12" id="top-nav">
						<li class="col-md-2 col-xs-4 col-sm-4" style="padding-left: 0px;"><!--  mode-col -->
							<div class="sea-air">
								<div class="sea" id="sea">
									<img src="${pageContext.request.contextPath}/resources/img/ship.png" title="SEA">
								</div>
								<div class="plane" id="air">
									<img src="${pageContext.request.contextPath}/resources/img/plane.png" title="AIR">
								</div>
							</div>
						</li>
						<li class="col-md-4 col-xs-8 col-sm-8 distributionChannel">
							<div>
								
							</div>
	
							<div class="input-group distributionChannel-inputgroup-new">
								<div class="input-group-btn">
									<span id="EXIM" class="btn dropdown-toggle expimp-inputgroup-new"
										data-toggle="dropdown"><span id="export-import-text"></span>&nbsp;<span class="fa fa-caret-down"></span>
									</span>
									<ul class="dropdown-menu dropdown-menu-dist" style="width: 100%; border: 0px; margin: 0px;">
										<li id="LIexport"><a id="EX" onclick="return selectEx();">Export</a></li>
										<li id="LIimport"><a id="IMP" onclick="return selectIm();">Import</a></li>
									</ul>
								</div>
								<!-- /btn-group -->
								<select class="form-control" id="salesOrgList"
									onchange="changeCompany();">

								</select>
							</div>
						</li>
						<%-- <li class="col-md-4 col-xs-8 col-sm-8 distributionChannel">
							<!-- <div class="eic">
                                    <p class="exp">Export&nbsp;</p><input class="export-radio" type="radio" name="radio" /></div>
                                <div class="eic">
                                    <p class="imp">Import</p><input class="import-radio" type="radio" name="radio" /></div>
                                <div class="eic">
                                    <p class="cha">Cha</p><input class="cha-radio" type="radio" name="radio" /></div> -->
							<div class="eic">
								<p class="top-nav-exp">Export</p>
								<input id="exp-radio" type="radio" name="radio" value="EX" />
							</div>
							<div class="eic">
								<p class="top-nav-imp">Import</p>
								<input id="imp-radio" type="radio" name="radio" value="IMP" />
							</div>
							<div class="eic">
								<p class="top-nav-cha">Cha</p>
								<input id="cha-radio" type="radio" name="radio" value="CHA" />
							</div> <img class="trident"
							src="${pageContext.request.contextPath}/resources/img/arrow-header-image-1.png">
							<form class="partner-form">
								<div class="form-group">
									<select class="form-control" id="salesOrgList" onchange="changeCompany();">
										
									</select>
								</div>
							</form>
						</li> --%>
						<!-- <li class="col-lg-2 track-col dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">TRACK&nbsp;&nbsp;<i class="fa fa-caret-down" aria-hidden="true"></i></a>
                                <div class="dropdown-menu">
                                    <i class="material-icons">&#xE55D;</i>
                                    <form>
                                        <fieldset class="form-group">
                                            <input type="text" class="form-control form-control-sm" id="name" placeholder="Action here..." required>
                                        </fieldset>
                                        <fieldset class="form-group">
                                            <input type="number" class="form-control form-control-sm" id="email" placeholder="..." required>
                                        </fieldset>
                                        <button type="submit" class="btn btn-info btn-block">Search&nbsp;&nbsp;&nbsp;&nbsp;<i class="fa fa-search" aria-hidden="true"></i></button>
                                    </form>
                                </div>
                            </li> -->

						<li class="col-md-6 col-xs-12 dropdown welcome">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#"
							role="button" aria-haspopup="true" aria-expanded="false">
							<span class="userCompanyName" style="font-size: 12px;">
								<span></span>&nbsp;&nbsp;
								<i class="fa fa-caret-down" aria-hidden="true"></i> 
							</span>
						</a>
							<div style="clear: right;"></div>
							<div class="dropdown-menu" style="background-color: white; width: 50% !important; float: right;">
								<a class="dropdown-item" href="${pageContext.request.contextPath}/homepage">
									<i class="fa fa-home" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Dashboard
								</a>
								<%-- <a class="dropdown-item" href="${pageContext.request.contextPath}/editPasswordPage">
									<i class="fa fa-cogs" aria-hidden="true"></i>&nbsp;&nbsp;Change&nbsp;Password
								</a> --%>
								 <a class="dropdown-item" href="${pageContext.request.contextPath}/download/pdf/MyshipmentManual.pdf">
								 	<i class="fa fa-download" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Download&nbsp;Manual
        						</a>
        						<a class="dropdown-item" href="${pageContext.request.contextPath}/myAccount"">
									<i class="fa fa-cogs" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Settings
								</a>
								<a class="dropdown-item" href="${pageContext.request.contextPath}/logout">
									<i class="fa fa-power-off" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;&nbsp;Log Out
								</a>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>

	</nav>
	<!-- <br><br><br><br> -->
	<!-- <div class="row" style="background: #337ab7;"> -->
	<div class="row row-margin-unset-all menubar-fixed-top menubar-design-pattern" data-spy="affix" data-offset-top="64">
		<div class="col-md-9 menu-list" id="menu-bar" style="color: #252525;"></div>
		<div class="col-md-3 menu-track" style="color: white; margin-top: 5px;">
			<div class="row row-margin-unset-all">
				<!-- <div class="col-md-1"></div> -->
				<div class="col-md-12">
					<form action="getTrackingDetailInfo" method="post">
						<div class="form-group">
							<div class="input-group">
								<c:choose>
									<c:when test="${loginDetails.accgroup == 'ZEMP'}">
										<select style="width: 100%;" class="form-control select2-buyers" id="crm-buyers-list" onchange="return changeConsignee();">
											<%-- <option value="">Please Select</option>
											<c:forEach
												items="${loginDetails.supplierDetailsDTO.buyersSuppliersmap.buyers }"
												var="buyers">
												<option value="${buyers.key }">${buyers.value }</option>
											</c:forEach> --%>
										</select>
										<span class="input-group-btn">
											<span class="btn btn-primary" type="submit" id="trackHBL" style="height: 34px; font-size: 13px; background-color: #252525; border-color: #252525;">
												Consignee
											</span>
										</span>
									</c:when>
									<c:otherwise>
										<input type="text" class="form-control" id="trackHeaderGo" name="blNo" placeholder="Track Shipment By HBL No">
										<span class="input-group-btn">
											<button class="btn btn-primary" type="submit" id="trackHBL" onclick="return isHBLEmpty();" style="height: 30px; font-size: 13px; background-color: #252525; border-color: #252525;">
												TRACK
											</button>
										</span>
									</c:otherwise>
								</c:choose>
								
								<!-- <span class="input-group-addon form-control-feedback">
									        <i class="fa fa-search"></i>
									    </span> 
										<div class="input-group-addon">
										</div> -->
							</div>
						</div>
					</form>

					<!-- <div class="form-group">
				                <div class="input-group input-group-md">
				                    <span class="input-group-addon"><i class="glyphicon glyphicon-filter"></i></span>
				                    <div class="icon-addon addon-md">
				                        <input type="text" placeholder="Email" class="form-control" id="email">
				                        <label for="email" class="glyphicon glyphicon-search" rel="tooltip" title="email"></label>
				                    </div>
				                    <span class="input-group-btn">
				                        <button class="btn btn-primary" style="margin-top: -20px;" type="button">Go!</button>
				                    </span>
				                </div>
				            </div> -->
				</div>
				<!-- <div class="col-md-1"></div> -->
			</div>

		</div>
	</div>
	
	<div class="after-menu"></div>
	
	

	<jsp:include page="menu_v2.jsp"></jsp:include>
	<script type="text/javascript">
	
		/* $(document).ajaxStart(function(){
		    $.LoadingOverlay("show");
		});
		$(document).ajaxStop(function(){
		    $.LoadingOverlay("hide");
		}); */
		
		
        var loginDetails = <%=new Gson().toJson(session.getAttribute("loginDetails"))%>;
        
        var selectedColor = "rgb(12, 153, 213)";
        var notSelectedColor = "transparent";
        
        var distArr = {};

        setSupplierListSelectItemCRM();
        
        $(".userCompanyName").find("span").text(loginDetails.peAddress.name);
        headerNavDetails(loginDetails);
        renderMenuItems(loginDetails);

        if(loginDetails.accgroup == 'ZEMP') {
        	$.each(loginDetails.supplierDetailsDTO.buyersSuppliersmap.buyers, function(index,value){
        		if (index == loginDetails.crmDashboardBuyerId) {
        			$("#crm-buyers-list").val(index);//.trigger("change")
        			//$("#buyerNameSubHeader").text(value);
        			return;
        		}

        	});
        }

        function setSupplierListSelectItemCRM() {
            var supplierSelect = $('#crm-buyers-list');
            var supplierDetails = loginDetails.supplierDetailsDTO;
            if(typeof(supplierDetails) != "undefined") {

                var buyersSuppliersMap = supplierDetails.buyersSuppliersmap;
                if(typeof(buyersSuppliersMap) == "undefined") return false;

                var buyers = buyersSuppliersMap.buyers;
                //if(typeof(buyers) != "undefined") return false;

                var option = "";
                if (Object.keys(buyers).length === 0 && buyers.constructor === Object) {
                    option = option + '<option value="" selected >No Consignee Found</option>';
                } else {
                    //option = option + '<option value="" >Please Select</option>';
                }
                //var count = 0;
                //var buyerIdSelect = '';
                $.each(buyers, function (index, value) {
                    option = option + '<option value="' + index + '" >' + value + '</option>';
                    /* if(count == 0) {
						buyerIdSelect = index;
						option = option + '<option value="' + index + '" selected >' + value + '</option>';
				   } else {
					   option = option + '<option value="' + index + '" >' + value + '</option>';
				   }
				   count++;  */
                });

                supplierSelect.empty().append(option);
                //supplierSelect.val(buyerIdSelect);
            }
        }
        
        function headerNavDetails(data) {
    		
    		var division='';
    		var $option='';
    		
    		//company name list-->which one is selected and add the other list to the options
	    	$.each(data.mpSalesOrgTree,function(index,value){
	    	//$.each(data.salesOrgDivisionWise,function(index,value){
	    		var salesOrg=value;
	    		if(salesOrg.salesOrg==data.salesOrgSelected) {
	    			$option=$option+'<option value="'+salesOrg.salesOrg+'" selected >'+salesOrg.salesOrgText+'</option>';
	    		} else {
	    			$option=$option+'<option value="'+salesOrg.salesOrg+'" >'+salesOrg.salesOrgText+'</option>';
	    		}
	    	});
    		
	    	//appends the generated list into salesOrgList(Company Selection Option)
    		$("#salesOrgList").empty().append($option);
	    	
	    	$.each(data.mpSalesOrgTree,function(index,value){
	    		
	    		if(value.salesOrg==data.salesOrgSelected) {
	    			
	    			var distChnlLst=value.lstDistChnl;
	    			var Ex='';
	    			var Im='';
	    			var cha='';
	    			
	    			//identify Export, Import and CHA of the selected distribution channel
	    			//sets variable Ex,Im,cha value if selected distribution channel has any of these business
	    			$.each(distChnlLst,function(index,value) {
	    				
	    				if(value.distrChan==data.disChnlSelected) {
	    					division=value.lstDivision;
	    				}
						
	    				var disChnl=value;
	    				
						if(disChnl.distrChan=='EX') {
	    					Ex=disChnl.distrChan;
	    				}
	    				if(disChnl.distrChan=='IM') {
	    					Im=disChnl.distrChan;
	    				}
	    				/* if(disChnl.distrChan=='CHA') {
	    					cha=disChnl.distrChan;
	    				} */
	    				
	    			});
	    			
	    			/***
	    			 *	if ex, imp or cha is exists then enable them and select the one which is selected as distribution channel
	    			 *	else (if ex, imp or cha doesn't exist) disable them accordingly
	    			***/
	    			
	    			//process for export
	    			if(Ex=='EX') {
	    				if(data.disChnlSelected=='EX') {
	    					//$(".btn_radio").append('<p class="exp">Export</p><input class="export-radio" type="radio" name="radio" value="'+Ex+'" checked/><br>');
	    					//$(".distributionChannel").append('<div class="eic"><p class="top-nav-exp">Export</p><input id="exp-radio" type="radio" name="radio" value="'+Ex+'" checked /></div>');
	    					
	    					//$('#exp-radio').prop('checked', true);
							$('#export-import-text').text("Export");
							$('#IMP').removeClass("active");
							//$('#EX').addClass("active");
							
							//$('#LIexport').remove();
							$('#LIexport').css("display", "none");
							distArr['Export'] ='enabled';
	    				} else {
	    					//$(".distributionChannel").append('<div class="eic"><p class="top-nav-exp">Export</p><input id="exp-radio" type="radio" name="radio" value="'+Ex+'" /></div>');
	    					distArr['Export'] ='enabled';
	    				}
	    			} else {
	    				//$(".distributionChannel").append('<div class="eic"><p class="top-nav-exp">Export</p><input id="exp-radio" type="radio" name="radio" value="" disabled /></div>');
	    				
	    				//$('#exp-radio').prop('disabled', true);
						distArr['Export'] ='disabled';
	    			}
	    			
	    			//process for import
	    			if(Im=='IM') {
		    			if(data.disChnlSelected=='IM') {
		    				//$(".distributionChannel").append('<div class="eic"><p class="top-nav-imp">Import</p><input id="imp-radio" type="radio" name="radio" value="'+Imp+'" checked /></div>');
		    				
		    				//$('#imp-radio').prop('checked', true);
							$('#export-import-text').text("Import");
							$('#EX').removeClass("active");
							//$('#IMP').addClass("active");
							
							
							//$('#LIimport').remove();
							$('#LIimport').css("display", "none");
							distArr['Import'] ='enabled';
		    			} else {
	    					//$(".distributionChannel").append('<div class="eic"><p class="top-nav-imp">Import</p><input id="imp-radio" type="radio" name="radio" value="'+Imp+'" /></div>');
		    				distArr['Import'] ='enabled';
	    				}
	    			} else {
	    				//$(".distributionChannel").append('<div class="eic"><p class="top-nav-imp">Import</p><input id="imp-radio" type="radio" name="radio" value="" disabled /></div>');
	    				
	    				//$('#imp-radio').prop('disabled', true);
						distArr['Import'] ='disabled';
	    			}
	    			
	    			//process for cha
	    			/* if(cha=='CHA') {
	    				if(data.disChnlSelected=='CHA') {
		    				//$(".distributionChannel").append('<div class="eic"><p class="top-nav-cha">CHA</p><input id="cha-radio" type="radio" name="radio" value="'+cha+'" checked /></div>');
	    					$('#cha-radio').prop('checked', true);
	    				} else {
	    					//$(".distributionChannel").append('<div class="eic"><p class="top-nav-cha">CHA</p><input id="cha-radio" type="radio" name="radio" value="'+cha+'" /></div>');
	    				}
	    			} else {
	    				//$(".distributionChannel").append('<div class="eic"><p class="top-nav-cha">CHA</p><input id="cha-radio" type="radio" name="radio" value="" disabled /></div>');
	    				$('#cha-radio').prop('disabled', true);
	    			} */
	    		}
	    	});
	    	
	    	if(division.length==1){
	    		
	    		$.each(division,function(index,value){
		    		if(value.division=='SE') {
			    		$("#air").removeAttr("style");
			    		if(data.divisionSelected==value.division)
			    			selectSEA();
		    		}
			    	if(value.division=='AR') {
			    		$("#sea").removeAttr("style");
			    		if(data.divisionSelected==value.division)
			    			selectAIR();
			    	}
	    		});
	    	}
	    	if(division.length>1){
	    		
		    	$.each(division,function(index,value){
		    		if(value.division=='SE') {
		    			$("#sea").removeAttr("style");
		    			if(data.divisionSelected==value.division)
		    				selectSEA_disableAIR();
		    		}
		    		if(value.division=='AR'){
		    			$("#air").removeAttr("style");
		    			if(data.divisionSelected==value.division)
		    				selectAIR_disableSEA();
		    		}
		    		if(data.divisionSelected==null && division.length==2) {
		    			selectSEA_disableAIR();
		    		}
		    	});
    		}
    	}
        
        function selectSEA() {
        	$('#sea').css("background-color", selectedColor);
        	//$('#air').remove();
        	$('#air').css("display", "none");
        }
		function selectAIR() {
			$('#air').css("background-color", selectedColor);
        	//$('#sea').remove();
			$('#sea').css("display", "none");
		}
		function selectSEA_disableAIR() {
			$('#sea').css("background-color", selectedColor);
        	//$('#air').css("background-color", notSelectedColor);
			$('#air').css("display", "block");
		}
		function selectAIR_disableSEA() {
			$('#air').css("background-color", selectedColor);
        	//$('#sea').css("background-color", notSelectedColor);
			$('#sea').css("display", "block");
		}
		
		function rgb2hex(rgb) {
		    rgb = rgb.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
		    function hex(x) {
		        return ("0" + parseInt(x).toString(16)).slice(-2);
		    }
		    return "#" + hex(rgb[1]) + hex(rgb[2]) + hex(rgb[3]);
		}
		
		function changeCompany() {
	    	var salesOrg=$("#salesOrgList").val();
			//var distChannel=$(".distributionChannel").find('input:checked').val();
			var distChannel = "";
			var exportImportText = $('#export-import-text').text();
			if(exportImportText == "Export") {
				distChannel = 'EX';
			} else if(exportImportText == "Import") {
				distChannel = 'IM';
			}
			var division=null;
			
			if($("#air").css("background-color")==selectedColor)
				division="AIR";
			else if($("#sea").css("background-color")==selectedColor)
				division="SEA";
			$.LoadingOverlay("show");
			$.ajax({
				url: myContextPath + '/changebusiness',
	    		type: "POST",
	    		data:{salesOrg:salesOrg,
	    			distChannel:distChannel,
	    			division:division
	    			},
	    		
	    		success:function(data, textStatus, xhr){
	    			//window.location=xhr.getResponseHeader("Location");
	    			window.location.reload();
	    		},
	    		error: function(XMLHttpRequest, textStatus, errorThrown) { 
	    			$.LoadingOverlay("hide");
	    	    }
			});
	    }
		
		$("#downloadMyshipment").click(function () {
			$.ajax({
			    type: "GET",
			    url: myContextPath + '/download/pdf/myshipment.pdf',
			    /* data: {"data": JSON.stringify(myData)}, */
			    success: function(data, textStatus, jqXHR) {
			        var pdfWin= window.open("data:application/pdf;base64, " + data, '', 'height=650,width=840');
			        // some actions with this win, example print...
			    },
			    error: function(jqXHR) {
			        console.log("Error ")
			    }
			});	
		});
		
		
		function isHBLEmpty() {
			if($("#trackHeaderGo").val() == "") {
				swal ( "HBL Empty" ,  "Please Enter a valid HBL!" ,  "error" );
				return false;
			}
			return true;
		}
		
		function selectEx(){	
			var text = distArr["Export"];
			if(text =='enabled') {
				//$("#IMP").removeClass("active");
				//$("#EX").addClass("active");
				$("#LIexport").css("display", "none");
				$("#LIimport").css("display", "block");
				$('#export-import-text').text("Export");
				//changeCompany();
				return true;
			}
			if(text =='disabled'){
				swal("Request Denied", "You are not authorized for Export Channel", "error");
				return false;
			}
		}
		
		function selectIm(){	
			var text = distArr["Import"];
			if(text =='enabled'){
				//$("#EX").removeClass("active");
				//$("#IMP").addClass("active");
				$("#LIexport").css("display", "block");
				$("#LIimport").css("display", "none");
				$('#export-import-text').text("Import");
				//changeCompany();
				return true;
			}
			if(text =='disabled'){
				swal("Request Denied", "You are not authorized for Import Channel", "error");
				return false;
			}
		}
		
		//sea click
    	$("#sea").click(function(){
            
    		var salesOrg=$("#salesOrgList").val();
    		//var distChannel=$(".distributionChannel").find('input:checked').val(); 
    		
    		var distChannel = "";
			var exportImportText = $('#export-import-text').text();
			if(exportImportText == "Export") {
				distChannel = 'EX';
			} else if(exportImportText == "Import") {
				distChannel = 'IM';
			}
    		
    		var division=null;
    		division="SEA";
    		$.LoadingOverlay("show");
    		$.ajax({
    			url: myContextPath + '/changebusiness',
        		type: "POST",
        		data:{salesOrg:salesOrg,
        			distChannel:distChannel,
        			division:division
        			},
        		
        		success:function(data, textStatus, xhr){
        			//$.LoadingOverlay("show");
        			//window.location=xhr.getResponseHeader("Location");
        			window.location.reload();
        			//$.LoadingOverlay("hide");
        		},
	    		error: function(XMLHttpRequest, textStatus, errorThrown) { 
	    			$.LoadingOverlay("hide");
	    	    }
    		});
    		
    	});
    	

    	//air click
    	$("#air").click(function(){
    		var salesOrg=$("#salesOrgList").val();
    		//var distChannel=$(".distributionChannel").find('input:checked').val(); 
    		
    		var distChannel = "";
			var exportImportText = $('#export-import-text').text();
			if(exportImportText == "Export") {
				distChannel = 'EX';
			} else if(exportImportText == "Import") {
				distChannel = 'IM';
			}
    		
    		var division=null;
    		division="AIR";
    		$.LoadingOverlay("show");
    		$.ajax({
    			url: myContextPath + '/changebusiness',
        		type: "POST",
        		data:{salesOrg:salesOrg,
        			distChannel:distChannel,
        			division:division
        			},
        		
        		success:function(data, textStatus, xhr){
        			//$.LoadingOverlay("show");
        			//window.location=xhr.getResponseHeader("Location");
        			window.location.reload();
        			//$.LoadingOverlay("hide");
        		},
	    		error: function(XMLHttpRequest, textStatus, errorThrown) { 
	    			$.LoadingOverlay("hide");
	    	    }
    		});
    		
    	});

    	function changeConsignee() {
    		var consigneeId=$("#crm-buyers-list").val();
    		
    		$.LoadingOverlay("show");
    		$.ajax({
    			url: myContextPath + '/filterBuyer',
    			type: "POST",
    			data:{buyerId:consigneeId},
    			
    			success:function(data, textStatus, xhr){
    				window.location.reload();
    			},
    			error: function(data, textStatus, xhr) {
    				$.LoadingOverlay("hide");
    				swal({
    					title : "Error Occured",
    					text : data.responseText,
    					type : "error",
    					showCancelButton : false,
    					confirmButtonColor : "#dd4b39",
    					confirmButtonText : "OK",
    					cancelButtonText : "",
    					closeOnConfirm : false,
    					closeOnCancel : false
    				}, function(isConfirm) {
    					if (isConfirm) {
    						swal.close();
    					}
    				});
    		    }
    		});
    	}
        
        </script>