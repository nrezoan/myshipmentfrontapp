
<%@include file="header.jsp"%>

<!--END OF HEADER-->

<section id="bill-of-landing">
<!-- 	<section>
		<div class="container-fluid">
			<div class="row">
				<div class="input-group-addon commercial">
					<h4 style="text-align: left;">Tracking</h4>
				</div>
			</div>
		</div>
	</section>
	<div class="col-xs-12" style="height: 20px;"></div> -->

<%-- 	<div class="container" style="width: 80%; margin: 0 20% 0 12%;">
		<ul class="tracking-items">
			<li id="booking-conf" class="stateC"><a	class="tracking-item active" style="font-size: 12px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Booking
					Confirm</a></li>
			<li id="goods-rec" class="stateA"><a class="tracking-item"	style="font-size: 12px">Goods Receive</a></li>
			<c:forEach items="${documentHeaderBeanLst}" var="docHeader">
				<c:set var="division" value="${docHeader.division}" />
			</c:forEach>
			<c:if test="${division=='SE' }">
				<li id="stuffing-done" class="stateB"><a class="tracking-item"	style="font-size: 12px">Stuffing Done</a></li>
			</c:if>
			<c:if test="${division=='AR' }">
				<li id="stuffing-done" class="stateB"><a class="tracking-item"	style="font-size: 12px"> Palletization Done</a></li>
			</c:if>
			<li id="delivered" class="stateB"><a class="tracking-item"	style="font-size: 12px">Delivered</a></li>
		</ul>
		<div style="clear: both;"></div>
	</div> --%>
	
	<c:set var="compCode"  value="${companyCode}"/>
	<c:set var="distChan" value="${myshipmentTrackParams.distChan}"/>
	<c:set var="division" value="${myshipmentTrackParams.division}"/>		
	
	<div class="col-xs-12" style="height: 10px;"></div>
	
	<div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="row" style="">
				<div class="col-md-1"></div>
				<c:forEach items="${myshipTrackHeaderList}" var="trackHead" >
				<div class="col-md-6">					
						<h3 style="color: #222534; font-weight: bold;">${trackHead.sales_org_desc}</h3>					
				</div>
				<div class="col-md-4">
					<!-- <img alt="Company Logo" class="img-thumbnail" style="float: right;"> -->
					<img src="${pageContext.request.contextPath}/resources/images/${trackHead.comp_code}.jpg" alt="Company Logo" class="img-thumbnail" style="float: right;"/>
				</div>
				</c:forEach>
				<div class="col-md-1"></div>
			</div>
			
			<div class="row" style="height: 10px;"></div>
			
			<div class="row" style="background-color: #13357e; margin-left: 114px; margin-right: 114px;">
				<div class="col-md-2"></div>
				<div class="col-md-8" style="text-align: center">
					<c:if test="${division=='SE'}">
						<c:if test="${distChan=='EX'}">
							<h4 style="color: #fff; font-weight: bold;">Sea Export HBL Tracking Details</h4>
						</c:if>
						<c:if test="${distChan=='IM'}">
							<h4 style="color: #fff; font-weight: bold;">Sea Import HBL Tracking Details</h4>
						</c:if>
					</c:if>
					<c:if test="${division=='AR'}">
						<c:if test="${distChan=='EX'}">
							<h4 style="color: #fff; font-weight: bold;">Air Export HBL Tracking Details</h4>
						</c:if>
						<c:if test="${distChan=='IM'}">
							<h4 style="color: #fff; font-weight: bold;">Air Import HBL Tracking Details</h4>
						</c:if>
					</c:if>
				</div>
				<div class="col-md-2"></div>
			</div>

			<!-- <div class="col-xs-12" style="height: 20px;"></div> -->
			<div class="row">
				<div class="col-xs-1"></div>				
				<div class="col-xs-10">
					<table class="table table-hover">
						<thead>
							<tr>
								<th colspan="12" style="background-color: #337ab7; color: #fff; font-weight:bold;text-align: center; font-size: 17px;">Consignment Details</th>
							</tr>
						</thead>
						<tbody>
						<c:forEach items="${myshipTrackHeaderList}" var="docHeader" >
							<tr>								
								<td style="font-weight: bold;">HBL No.</td><td>${docHeader.bl_no}</td>								
								<td style="font-weight: bold;">Commercial Inv. No.</td><td>${docHeader.comm_inv}</td>								
							</tr>
							<tr>
								<fmt:parseDate value="${docHeader.bl_bt}" var="bl_date" pattern="yyyyMMdd" />
								<td style="font-weight: bold;">HBL Date</td><td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${bl_date}"/></td>
								<fmt:parseDate value="${docHeader.comm_inv_dt}" var="comm_inv_date" pattern="yyyyMMdd" />
								<td style="font-weight: bold;">Commercial Inv. Date</td><td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${comm_inv_date}"/></td>								
							</tr>
							<tr>
								<td style="font-weight: bold;">Shipper Name</td><td>${docHeader.shipper}</td>
								<td style="font-weight: bold;">Place of Receipt</td><td>${docHeader.por}</td>								
							</tr>
							<tr>
								<td style="font-weight: bold;">Consignee Name</td><td>${docHeader.buyer}</td>
								<fmt:parseDate value="${docHeader.gr_dt}" var="gr_date" pattern="yyyyMMdd" />
								<td style="font-weight: bold;">Cargo Received Date</td><td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${gr_date}"/></td>			<!-- cargo received date -->								
							</tr>
							<tr>
								<td style="font-weight: bold;">Port Of Loading</td><td>${docHeader.pol}</td>
								<td style="font-weight: bold;">MBL Number</td><td>${docHeader.mbl_no}</td>								
							</tr>
							<tr>
								<td style="font-weight: bold;">Port of Discharge</td><td>${docHeader.pod}</td>
								<td style="font-weight: bold;">Shipping Line</td><td>${docHeader.carrier}</td>								
							</tr>
							<tr>
								<td style="font-weight: bold;">Place of Discharge</td><td>${docHeader.plod}</td>
								<fmt:parseDate value="${docHeader.stuff_dt}" var="stuff_date" pattern="yyyyMMdd" />
								<td style="font-weight: bold;">Stuffing Date</td><td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${stuff_date}"/></td>								
							</tr>
							<tr>
								<td style="font-weight: bold;">TOS</td><td>${docHeader.frt_mode_ds}</td>
								<fmt:parseDate value="${docHeader.dep_dt}" var="dep_date" pattern="yyyyMMdd" />
								<td style="font-weight: bold;">Shipped on Board </td><td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${dep_date}"/></td>	<!-- shipped on board -->								
							</tr>
							<tr>
								<td style="font-weight: bold;">Total Quantity</td><td>${docHeader.tot_qty}</td>
								<fmt:parseDate value="${docHeader.load_dt}" var="load_date" pattern="yyyyMMdd" />
								<td style="font-weight: bold;">ETD</td><td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${load_date}"/></td>								
							</tr>
							<tr>
								<td style="font-weight: bold;">Total Volume (CBM)</td><td>${docHeader.volume}</td>
								<fmt:parseDate value="${docHeader.eta_dt}" var="eta_date" pattern="yyyyMMdd" />
								<td style="font-weight: bold;">ETA</td><td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${eta_date}"/></td>							
							</tr>
							<tr>
								<td style="font-weight: bold;">Total Gross Weight</td><td>${docHeader.grs_wt}</td>
								<td style="font-weight: bold;">Destination Agent</td><td>${docHeader.agent}</td>								
							</tr>
							</c:forEach>																																			
						</tbody>
					</table>
				</div>
				<div class="col-xs-1"></div>
			</div>
			<div class="col-xs-12" style="height: 20px;"></div>
			<!-- table 2 -->			
			<div class="row">
				<div class="col-md-1"></div>				
				<div class="col-md-10">
					<table class="table table-hover">
						<thead>
							<tr>
								<th colspan="12" style="background-color: #337ab7; color: #fff; font-weight:bold;text-align: center; font-size: 17px;">Item Details</th>
							</tr>
						</thead>
						<tbody>
							<tr class="info">
								<td style="font-weight: bold;">Sl</td>
								<td style="font-weight: bold;">PO No.</td>
								<td style="font-weight: bold;">Commodity</td>
								<td style="font-weight: bold;">Style</td>				
								<td style="font-weight: bold;">Size</td>
								<td style="font-weight: bold;">SKU</td>
								<td style="font-weight: bold;">Article No.</td>
								<td style="font-weight: bold;">Color</td>
								<td style="font-weight: bold;">Pieces</td>
								<td style="font-weight: bold;">Carton</td>
								<td style="font-weight: bold;">Vol.(CBM)</td>
								<td style="font-weight: bold;">Weight</td>
							</tr>
							<% int sl=1; %>
							<c:forEach items="${myshipTrackItemList}" var="docDetail" >								
             					<tr>
									<td><% out.print(sl);%></td>
									<% sl++; %>
					                <td>${docDetail.po_no}</td>
					                <td>${docDetail.commodity}</td>
					                <td>${docDetail.style}</td>
					                <td>${docDetail.size}</td>
					                <td>${docDetail.sku}</td>
					                <td>${docDetail.art_no}</td>
					                <td>${docDetail.color}</td>
					                <td>${docDetail.item_pcs}</td>
					                <td>${docDetail.item_qty}</td>
					                <td>${docDetail.item_vol}</td>
					                <td>${docDetail.item_grwt}</td>
             					</tr>
      						</c:forEach>

						</tbody>
					</table>
				</div>
				<div class="col-md-1"></div>
			</div>
			<div class="col-xs-12" style="height: 20px;"></div>
			<!-- table 3 -->
			<div class="row">
				<div class="col-md-1"></div>				
				<div class="col-md-10">
					<table class="table table-hover">
						<thead>
							<tr>
								<th colspan="8" style="background-color: #337ab7; color: #fff; font-weight:bold;text-align: center; font-size: 17px;">Vessel Schedule</th>
							</tr>
						</thead>
						<tbody>
							<tr class="info">
								<td style="font-weight: bold;">Leg</td>
								<td style="font-weight: bold;">Vessel Name</td>
								<!-- <td style="font-weight: bold;">Vessel Type</td> -->
								<td style="font-weight: bold;">Voyage</td>				
								<td style="font-weight: bold;">POL</td>
								<td style="font-weight: bold;">POD</td>
								<td style="font-weight: bold;">ETD</td>
								<td style="font-weight: bold;">ETA</td>								
							</tr>
							<% int sch=1; %>
							<c:forEach items="${myshipTrackScheduleList}" var="docSchedule" >								
             					<tr>
									<td><% out.print(sch);%></td>
									<% sch++; %>
					                <td>${docSchedule.car_name}</td>
					                <%-- <td>${docSchedule.car_type}</td> --%>
					                <td>${docSchedule.car_no}</td>
					                <td>${docSchedule.pol}</td>
					                <td>${docSchedule.pod}</td>
					                <fmt:parseDate value="${docSchedule.etd}" var="etd_date" pattern="yyyyMMdd" />
					                <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${etd_date}"/></td>
					                <fmt:parseDate value="${docSchedule.eta}" var="eta_date" pattern="yyyyMMdd" />
					                <td><fmt:formatDate pattern="dd-MM-yyyy" type="date" value="${eta_date}"/></td>					                					                
             					</tr>
      						</c:forEach>

						</tbody>
					</table>
				</div>
				<div class="col-md-1"></div>
			</div>
			<div class="col-xs-12" style="height: 20px;"></div>
			<!-- table 4 -->
			<div class="row">
				<div class="col-md-1"></div>				
				<div class="col-md-10">
					<table class="table table-hover">
						<thead>
							<tr>
								<th colspan="10" style="background-color: #337ab7; color: #fff; font-weight:bold;text-align: center; font-size: 17px;">Container Details</th>
							</tr>
						</thead>
						<tbody>
							<tr class="info">
								<td style="font-weight: bold;">Sl</td>
								<td style="font-weight: bold;">Container No.</td>
								<td style="font-weight: bold;">Seal No.</td>
								<td style="font-weight: bold;">Container Size</td>				
								<td style="font-weight: bold;">Mode</td>
								<td style="font-weight: bold;">PO No.</td>
								<td style="font-weight: bold;">Style</td>
								<td style="font-weight: bold;">Packed Carton</td>
								<td style="font-weight: bold;">Packed Vol.</td>
								<td style="font-weight: bold;">Packed Wt.</td>								
							</tr>
							<% int ves=1; %>
							<c:forEach items="${myshipTrackContainerList}" var="docContainer" >								
             					<tr>
									<td><% out.print(ves);%></td>
									<% ves++; %>
					                <td>${docContainer.cont_no}</td>
					                <td>${docContainer.seal_no}</td>
					                <td>${docContainer.cont_size}</td>
					                <td>${docContainer.cont_mode}</td>
					                <td></td>
					                <td></td>
					                <td>${docContainer.pacd_qty}</td>
					                <td>${docContainer.pacd_vol}</td>
					                <td>${docContainer.pacd_wt}</td>					                
             					</tr>
      						</c:forEach>

						</tbody>
					</table>
				</div>
				<div class="col-md-1"></div>
			</div>
			
		</div>
	</div>
</div>			
			
</section>

<!-- <script type="text/javascript">
	
	$(document).ready(function() {
		//var trackingObject = "${trackingDto.zzmblmawbno}";
		var bookingConfirmed = "${trackingStatusParams.bookingDate}";
		var goodsReceived = "${trackingStatusParams.goodsReceivedDate}";
		var stuffingDone = "${trackingStatusParams.stuffingDate}";
		var shippedOnboard = "${trackingStatusParams.shippedOnboardDate}";
		var delivered = "${trackingStatusParams.deliveredDate}";

		
		/*if (bookingConfirmed != null) {			
			$('#booking-conf').removeClass('stateC').addClass('stateC');
		}*/
		if (bookingConfirmed == null || bookingConfirmed == '') {
			$('#booking-conf').removeClass('stateC').addClass('stateA');
		}
		if (goodsReceived != null && goodsReceived != '') {
			$('#goods-rec').removeClass('stateA').addClass('stateC');
		}
		if (goodsReceived == null || goodsReceived == '') {
			if (bookingConfirmed != null && bookingConfirmed != '') {
				//$('#goods-rec').removeClass('stateA').addClass('stateC');
			}
			if (bookingConfirmed == null || bookingConfirmed == '') {
				$('#goods-rec').removeClass('stateA').addClass('stateB');
			}

		}
		//Stuffing done
		if (stuffingDone != null && stuffingDone != '') {
			
			$('#stuffing-done').removeClass('stateB').addClass('stateC');
		}
		if (stuffingDone == null || stuffingDone == '') {
			
			if (goodsReceived != null && goodsReceived != '') {
				$('#stuffing-done').removeClass('stateB').addClass('stateA');
			}
			if (goodsReceived == null || goodsReceived == '') {
				//$('#stuffing-done').removeClass('stateB').addClass('stateB');
			}

		}

		//Shipped Onboard
		if (shippedOnboard != null && shippedOnboard != '') {
			//alert("in sh")
			$('#shipped-on').removeClass('stateB').addClass('stateC');
		}
		if (shippedOnboard == null || shippedOnboard == '') {
			if (stuffingDone != null && stuffingDone != '') {
				$('#shipped-on').removeClass('stateB').addClass('stateA');
			}
			if (stuffingDone == null || stuffingDone == '') {
				//$('#stuffing-done').removeClass('stateB').addClass('stateB');
			}

		}

		//Delivered
		if (delivered != null && delivered != '') {
			$('#delivered').removeClass('stateB').addClass('stateC');
		}
		if (delivered == null || delivered == '') {
			if (shippedOnboard != null && shippedOnboard != '') {
				$('#delivered').removeClass('stateB').addClass('stateA');
			}
			if (shippedOnboard == null || shippedOnboard == '') {
				//$('#stuffing-done').removeClass('stateB').addClass('stateB');
			}

		}

	});
</script> -->

<%@include file="footer.jsp" %>