<%@include file="header.jsp"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<style>
.panel {
	margin-bottom: 0px !important;
}

.panel-body {
	padding: 0px;
	padding-top: 1%;
	padding-left: 2%;
	background: #fff;
}

.col-xs-6 {
	padding-right: 0px;
	padding-left: 0px;
}
</style>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/print.css">

<section id="bill-of-landing">
	<section>
		<div class="container-fluid">
			<div class="row">
				<div class="input-group-addon commercial">
					<h4 style="text-align: left;">Non-Nego Bill Of Lading</h4>
				</div>
			</div>
		</div>
	</section>



	<section>
		<div class="container no-print">
			<div style="float: right;">
				<button type="button" class="btn btn-primary" id="print"
					onclick="printContent('bill-of-landing')">Print</button>
				<!-- <button type="button" class="btn btn-primary" id="pdf">PDF</button>  -->
			</div>
		</div>
	</section>

	<div class="col-xs-12" style="height: 20px;"></div>

	<section>
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="col-xs-6" style="margin-left: -5px;">
<%-- 						<div class="panel panel-default"
							style="border: 1px solid #337ab7;">
							<div class="panel-heading"
								style="border-bottom: 0px solid #fff; background: #337ab7; color: white; font-size: 16px; font-weight: bold;">
								Shipper Name and Address</div>
							<div class="panel-body">
								<table class="table commercial-table">


									<tbody>
										<c:forEach items="${airBillOfLandingJsonData.itShipper}"
											var="itShipper">
											<tr style="background: #fff; font-size: 12px">
												TO THE ORDER OF:
												</br>
												<span>
													${airBillOfLandingJsonData.itShipperBank[0].name1},${airBillOfLandingJsonData.itShipperBank[0].name2}</br>
													${airBillOfLandingJsonData.itShipperBank[0].name3},${airBillOfLandingJsonData.itShipperBank[0].name4}</br>
													${airBillOfLandingJsonData.itShipperBank[0].city1},${airBillOfLandingJsonData.itShipperBank[0].country}</br>
												</span>
												<span>A/C OF: ${itShipper.name1},${itShipper.name2}</br>
													${itShipper.name3},${itShipper.name4}</br>
													${itShipper.city1},${airBillOfLandingJsonData.itShipperCountry[0].landX}
												</span>
											</tr>


										</c:forEach>
									</tbody>
								</table>
							</div>
						</div> --%>
						<table class="table commercial-table">
							<thead style="border-right: 1px solid #337ab7;">
								<tr style="border-right: 1px solid #337ab7;">
									<td style="border: 1px solid #337ab7; background: #337ab7; color: white; font-size: 16px;font-weight: bold;">
									Shipper Name and Address</td>									
								</tr>
							</thead>
							<tbody>
								<tr style="background: #fff; font-size: 14px">
									<td><c:forEach
											items="${airBillOfLandingJsonData.itShipper}" var="itShipper">
												TO THE ORDER OF:
												</br>
											<span>
												${airBillOfLandingJsonData.itShipperBank[0].name1},${airBillOfLandingJsonData.itShipperBank[0].name2}</br>
												${airBillOfLandingJsonData.itShipperBank[0].name3},${airBillOfLandingJsonData.itShipperBank[0].name4}</br>
												${airBillOfLandingJsonData.itShipperBank[0].city1},${airBillOfLandingJsonData.itShipperBank[0].country}</br>
											</span>
											<span>A/C OF: ${itShipper.name1},${itShipper.name2}</br>
												${itShipper.name3},${itShipper.name4}</br>
												${itShipper.city1},${airBillOfLandingJsonData.itShipperCountry[0].landX}
											</span>

										</c:forEach></td>
								</tr>
							</tbody>
						</table>
<%-- 						<div class="panel panel-default"
							style="border: 1px solid #337ab7;">
							<div class="panel-heading"
								style="border-bottom: 0px solid #fff; background: #337ab7; color: white; font-size: 16px;font-weight: bold;">
								Consignee Name and Address</div>
							<div class="panel-body">
								<table class="table commercial-table">

									<c:forEach items="${airBillOfLandingJsonData.itConsignee}"
										var="itConsignee">
										<tr style="background: #fff; font-size: 12px">
											UNTO THE ORDER OF:
											</br>
											<span>
												${airBillOfLandingJsonData.itConsigneeBank[0].name1},${airBillOfLandingJsonData.itConsigneeBank[0].name2}</br>
												${airBillOfLandingJsonData.itConsigneeBank[0].name3},${airBillOfLandingJsonData.itConsigneeBank[0].name4}</br>
												${airBillOfLandingJsonData.itConsigneeBank[0].city1},${airBillOfLandingJsonData.itConsigneeBank[0].country}</br>

											</span>
										
										<span>A/C OF: ${itConsignee.name1},${itConsignee.name2}</br>
											${itConsignee.name3},${itConsignee.name4}</br>
											${itConsignee.city1},${airBillOfLandingJsonData.itConsigneeCountry[0].landX}
										</span>
										</tr>




									</c:forEach>
									</tbody>
								</table>
							</div>
						</div> --%>
						
						<table class="table commercial-table" style="margin-top: -20px;">
							<thead style="border-right: 1px solid #337ab7;">
								<tr style="border-right: 1px solid #337ab7;">
									<td style="border: 1px solid #337ab7; background: #337ab7; color: white; font-size: 16px;font-weight: bold;">
									Consignee Name and Address</td>									
								</tr>
							</thead>
							<tbody>
								<tr style="background: #fff; font-size: 14px">
								<td>
									<c:forEach items="${airBillOfLandingJsonData.itConsignee}"
										var="itConsignee">
										
											UNTO THE ORDER OF:
											</br>
											<span>
												${airBillOfLandingJsonData.itConsigneeBank[0].name1},${airBillOfLandingJsonData.itConsigneeBank[0].name2}</br>
												${airBillOfLandingJsonData.itConsigneeBank[0].name3},${airBillOfLandingJsonData.itConsigneeBank[0].name4}</br>
												${airBillOfLandingJsonData.itConsigneeBank[0].city1},${airBillOfLandingJsonData.itConsigneeBank[0].country}</br>

											</span>
										
										<span>A/C OF: ${itConsignee.name1},${itConsignee.name2}</br>
											${itConsignee.name3},${itConsignee.name4}</br>
											${itConsignee.city1},${airBillOfLandingJsonData.itConsigneeCountry[0].landX}
										</span>										
									</c:forEach>
								</td>
								</tr>
							</tbody>
						</table>
						

<%-- 						<div class="panel panel-default"
							style="border: 1px solid #337ab7;">
							<div class="panel-heading"
								style="border-bottom: 0px solid #fff; background: #337ab7; color: white; font-size: 16px;font-weight: bold;">
								Notify Party (No claim shall attach for failure to notify)</div>
							<div class="panel-body">
								<table class="table commercial-table">

									<c:forEach items="${airBillOfLandingJsonData.itNotify}"
										var="itNotify">
										<tr style="background: #fff; font-size: 12px">
											<span> ${itNotify.name1},</br>${itNotify.name2}
												${itNotify.name3},${itNotify.name4}</br> ${itNotify.city1}
											</span>
									</c:forEach>
									<c:forEach items="${airBillOfLandingJsonData.itNotifyCountry}"
										var="itNotifyCountry">
										<span>${itNotifyCountry.landX}</span>
									</c:forEach>
									</tr>
									</tbody>
								</table>
							</div>
						</div> --%>
						
						<table class="table commercial-table" style="margin-top: -20px;">
							<thead style="border-right: 1px solid #337ab7;">
								<tr style="border-right: 1px solid #337ab7;">
									<td style="border: 1px solid #337ab7; background: #337ab7; color: white; font-size: 16px;font-weight: bold;">
									Notify Party (No claim shall attach for failure to notify)</td>									
								</tr>
							</thead>
							<tbody>
								<tr style="background: #fff; font-size: 14px">
								<td>
									<c:forEach items="${airBillOfLandingJsonData.itNotify}" var="itNotify">
											<span> ${itNotify.name1},</br>
											${itNotify.name2} ${itNotify.name3},${itNotify.name4}</br>
											${itNotify.city1}
											</span>
									</c:forEach>
									<c:forEach items="${airBillOfLandingJsonData.itNotifyCountry}" var="itNotifyCountry">
										<span>${itNotifyCountry.landX}</span>
									</c:forEach>
								</td>
								</tr>
							</tbody>
						</table>



						<table class="table commercial-table" style="margin-top: -20px;">
							<thead style="border-right: 1px solid #337ab7;">
								<tr style="border-right: 1px solid #337ab7;">
									<td style="border: 1px solid #337ab7;">Place of Receipt</td>
									<td style="border: 1px solid #337ab7;">Precarriage by</td>
									<td style="border: 1px solid #337ab7;">Flight No</td>
								</tr>
							</thead>

							<tbody>

								<tr style="background: #fff; font-size: 12px">
									<td>${airBillOfLandingJsonData.itPor[0].lGobe}</td>
									<td>${airBillOfLandingJsonData.itHeader[0].zzAirlineName1}&nbsp;</td>
									<td>${airBillOfLandingJsonData.itHeader[0].zzAirlineNo1}</td>
								</tr>

							</tbody>
						</table>




						<table class="table commercial-table" style="margin-top: -20px;">
							<thead>
								<tr>
									<td style="border: 1px solid #337ab7;">Airport of Departure</td>
									<td style="border: 1px solid #337ab7;">Requested Routing</td>
									<td style="border: 1px solid #337ab7;">Airport of Destination</td>
								</tr>
							</thead>

							<tbody>
								<tr style="background: #fff; font-size: 12px">
									<td>${airBillOfLandingJsonData.itPol[0].zzPortName}</td>

									<td>${airBillOfLandingJsonData.itPod[0].zzPortName}</td>


									<td>${airBillOfLandingJsonData.itPolDel[0].zzPortName}</td>
								</tr>
							</tbody>
						</table>




						<table class="table commercial-table" style="margin-top: -20px;">
							<thead>
								<tr>
									<td style="border: 1px solid #337ab7; width: 50%">Shipping Marks</td>

								</tr>
							</thead>

							<tbody>

								<tr style="background: #fff;">
									<c:forEach items="${airBillOfLandingJsonData.itHeader}" var="itHeader">
										<td>${itHeader.desc_Z112}</td>
									</c:forEach>




								</tr>

							</tbody>
						</table>
						<!--  -->
						<table class="table commercial-table" style="margin-top: -20px;">
							<thead style="border-right: 1px solid #fff;">
								<tr style="border-right: 1px solid #fff;">
									<td style="border: 1px solid #337ab7;">I.A.T.A. Code</td>
									<td style="border: 1px solid #337ab7;">Master Airway Bill no</td>
									<td style="border: 1px solid #337ab7;">Flight Date</td>
								</tr>
							</thead>

							<tbody>
								<c:forEach items="${airBillOfLandingJsonData.itPor}" var="itPor">
									<tr style="background: #fff; font-size: 12px">
										<!--  <td>${itPor.lGobe}</td> -->
										<td></td>
										<td>${airBillOfLandingJsonData.itHeader[0].zzMblMabwNO}&nbsp;</td>
										<td>
										<fmt:formatDate value="${airBillOfLandingJsonData.itHeader[0].zzLoadingDt}" pattern="yyyy-MM-dd"/>
										</td>
									</tr>
								</c:forEach>
							</tbody>
						</table>
						<table class="table commercial-table" style="margin-top: -20px;">
							<thead style="border-right: 1px solid #fff;">
								<tr style="border-right: 1px solid #fff;">
									<td style="border: 1px solid #337ab7;">Number of pieces</td>
									<td style="border: 1px solid #337ab7;">Gross Weight</td>
									<td style="border: 1px solid #337ab7;">Kg /lb</td>
									<td style="border: 1px solid #337ab7;">Chargeable Weight</td>
									<td style="border: 1px solid #337ab7;">Rate</td>
									<td style="border: 1px solid #337ab7;">Total</td>
								</tr>
							</thead>

							<tbody>

								<tr style="background: #fff; font-size: 12px">
									<!-- <td>${airBillOfLandingJsonData.itHeader[0].zzSoquantity}</td> -->
									<fmt:parseNumber var="i" integerOnly="true" type="number" value="${airBillOfLandingJsonData.itHeader[0].zzSoquantity}" />
									<td><c:out value="${i}" /></td>
									<td>${airBillOfLandingJsonData.itHeader[0].zzGrossWeigth}</td>
									<td>${airBillOfLandingJsonData.itHeader[0].ntGew}</td>
									<td>${airBillOfLandingJsonData.itHeader[0].zzTot_Chrg_Wt}</td>
									<td>As arranged</td>
									<td>${airBillOfLandingJsonData.itHeader[0].zzTotalVolWt}</td>
								</tr>

							</tbody>
						</table>
						<!--  
                        <div class="panel panel-default" style="margin-top: -20px;">
  <div class="panel-heading" style="border: 1px solid #337ab7;background: #337ab7;color: white;">Charges</div>
  <div class="panel-body">
  Charge div
  </div>
  </div>
   <div class="panel panel-default">
  <div class="panel-heading" style="border: 1px solid #337ab7;background: #337ab7;color: white;">Carriage</div>
  <div class="panel-body">
  Carriage div
  </div>
  </div>-->
					</div>
					<div class="col-xs-6" style="margin-left: 5px;">
					
					<table class="table commercial-table">
							<thead style="border-right: 1px solid #fff;">
								<tr style="border-right: 1px solid #fff;">
									<td style="border: 1px solid #337ab7;">Bill of Lading No.</td>
									<td style="border: 1px solid #337ab7;">Country of Origin</td>
								</tr>
							</thead>
							<tbody>
								<tr style="height: 20px; background: #fff;">
									<c:forEach items="${airBillOfLandingJsonData.itHeader}"
										var="itHeader">
										<td style="border-top: 1px solid #337ab7; font-size: 16px;font-weight: bold; text-align:center;">${itHeader.zzHblHawbNo}</td>
									</c:forEach>
									<c:forEach items="${airBillOfLandingJsonData.itCountryOfOrig}"
										var="itCountryOfOrig">
										<td style="border-top: 1px solid #337ab7;text-align:center;">${itCountryOfOrig.landX}</td>
									</c:forEach>
								</tr>
							</tbody>
						</table>
					
					
						
<%-- 						<div class="panel panel-default" style="border: 1px solid #337ab7;">
							<div class="panel-heading" class="panel-heading"
								style="border-bottom: 1px solid #337ab7; background: white; color: #000; text-align:center;font-size: 16px;">
								Not Negotiable <span style="font-weight:bold;">Air Waybill</span>(Air Consignment Note) Issued by
								</div>
							<div class="panel-body" style="text-align:center;">
								<table class="table commercial-table"
									style="padding-top: -90px;">
									<tbody>
										<c:forEach items="${airBillOfLandingJsonData.itAddress}" var="itAddress">
											<tr style="background: #fff; font-size: 12px;">
												<span style="font-size: 24px;font-weight: bold">${itAddress.name1}</span></br>
												<span style="font-size: 16px;">${itAddress.name2}</span></br>
												<span style="font-size: 16px;">${itAddress.name3}</span></br>
												<span style="font-size: 16px;">${itAddress.name4}</span></br>
												<span style="font-size: 16px;">${itAddress.city1}</span>
											</tr>
										</c:forEach>
										<c:forEach items="${airBillOfLandingJsonData.itCompCountry}" var="itCompCountry">
											<span>${itCompCountry.landX}</span>
										</c:forEach>
										
									</tbody>
								</table>
							</div>
						</div> --%>


						<table class="table commercial-table" style="margin-top: -21px;">
							<thead style="border-right: 1px solid #fff;">
								<tr style="border-right: 1px solid #fff;">
									<td	style="border: 1px solid #337ab7; background: white; color: #000; text-align: center; font-size: 16px;">Not
										Negotiable <span style="font-weight: bold;">Air Waybill</span>(Air Consignment Note) Issued by
									</td>
								</tr>
							</thead>
							<tbody>
								<tr style="background: #fff; font-size: 12px;text-align:center;">
									<td><c:forEach
											items="${airBillOfLandingJsonData.itAddress}" var="itAddress">
											<span style="font-size: 24px; font-weight: bold">${itAddress.name1}</span>
											</br>
											<span style="font-size: 16px;">${itAddress.name2}</span>
											</br>
											<span style="font-size: 16px;">${itAddress.name3}</span>
											</br>
											<span style="font-size: 16px;">${itAddress.name4}</span>
											</br>
											<span style="font-size: 16px;">${itAddress.city1},</span>
										</c:forEach> <c:forEach items="${airBillOfLandingJsonData.itCompCountry}"
											var="itCompCountry">
											<span style="font-size: 16px">${itCompCountry.landX}</span>
										</c:forEach></td>
								</tr>

							</tbody>
						</table>



						<%-- 						<div class="panel panel-default"
							style="border: 1px solid #337ab7;">
							<div class="panel-heading"
								style="border-bottom: 1px solid #337ab7; background: #337ab7; color: white;">
								Accounting Information</div>
							<div class="panel-body" style="border-top: 1px solid #337ab7;font-size: 16px;font-weight: bold;">
								${airBillOfLandingJsonData.itHeader[0].zzFreightModes}</div>
						</div> --%> 
						
						<table class="table commercial-table" style="margin-top: -20px;">
							<thead style="border-right: 1px solid #fff;">
								<tr style="border-right: 1px solid #fff;">
									<td style="border: 1px solid #337ab7;">Accounting Information</td>									
								</tr>
							</thead>
							<tbody>
								<tr style="background: #fff; font-size: 12px; text-align:center;">
									<td style="font-size: 16px;font-weight: bold;">${airBillOfLandingJsonData.itHeader[0].zzFreightModes}</td>
								</tr>
							</tbody>
						</table>						
						<table class="table commercial-table" style="margin-top: -20px;">
							<thead style="border-right: 1px solid #fff;">
								<tr style="border-right: 1px solid #fff;">
									<td style="border: 1px solid #337ab7;">Cargo Arrival Date</td>
									<td style="border: 1px solid #337ab7;">Time and Date
										Consigned</td>

								</tr>
							</thead>

							<tbody>

								<tr style="background: #fff; font-size: 12px">
									<td></td>
									<td></td>

								</tr>

							</tbody>
						</table>
						<table class="table commercial-table" style="margin-top: -20px;">
							<thead style="border-right: 1px solid #fff;">
								<tr style="border-right: 1px solid #fff;">
									<td style="border: 1px solid #337ab7;">Currency</td>
									<td style="border: 1px solid #337ab7;">Wt/vl</td>
									<td style="border: 1px solid #337ab7;">Other</td>
									<td style="border: 1px solid #337ab7;">Declared value for carriage NVD</td>
									<td style="border: 1px solid #337ab7;">Declared value for customs</td>
								</tr>
							</thead>

							<tbody>

								<tr style="background: #fff; font-size: 12px">
									<td>USD</td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>

								</tr>

							</tbody>
						</table>


						<table class="table commercial-table" style="margin-top: -20px;">
							<thead style="border-right: 1px solid #fff;">
								<tr style="border-right: 1px solid #fff;">
									<td style="border: 1px solid #337ab7;">Delivery Agent</td>
								</tr>
							</thead>
							<tbody>
								<tr style="background: #fff; font-size: 12px">
								<td>
									<c:forEach items="${airBillOfLandingJsonData.itDelevery}" var="itDelevery">											
												<span>${itDelevery.name1},${itDelevery.name2}</span>
												<span>${itDelevery.name3},${itDelevery.name4}</span>
												<span>${itDelevery.city1}</span>											
									</c:forEach>
									<c:forEach items="${airBillOfLandingJsonData.itDeleveryCountry}" var="itDeleveryCountry">
										<span>${itDeleveryCountry.landX}</span>
									</c:forEach>
								</td>
								</tr>
							</tbody>
						</table>


						<%-- 						<table class="table commercial-table" style="">
							<thead style="border-right: 1px solid #fff;">
								<tr style="border-right: 1px solid #fff;">
									<td style="border: 1px solid #337ab7;">Delivery Agent</td>
								</tr>
							</thead>
							<tbody>

								<c:forEach items="${airBillOfLandingJsonData.itDelevery}"
									var="itDelevery">
									<tr style="background: #fff; font-size: 12px">
										<span>${itDelevery.name1},${itDelevery.name2}</span>
										<span>${itDelevery.name3},${itDelevery.name4}</span>
										<span>${itDelevery.city1}</span>
								</c:forEach>
								<c:forEach items="${airBillOfLandingJsonData.itDeleveryCountry}"
									var="itDeleveryCountry">

									<span>${itDeleveryCountry.landX}</span>

								</c:forEach>

							</tbody>
						</table> --%>


<!-- 						<div class="panel panel-default">
							<div class="panel-heading"
								style="border: 1px solid #337ab7; background: #337ab7; color: white;text-align:center;font-weight:bold;">
								COPY NON NEGOTIABLE</div>
						</div> -->
						<table class="table commercial-table" style="margin-top: -20px;">
							<thead style="border-right: 1px solid #fff;">
								<tr style="border-right: 1px solid #fff;">
									<td	style="border: 1px solid #337ab7; background: white; color: #000; text-align: center; font-size: 18px;">
									COPY NON NEGOTIABLE</td>
								</tr>
							</thead>
						</table>
						
						



						<table class="table commercial-table" style="margin-top: -20px;">
							<thead style="border-right: 1px solid #fff;">
								<tr style="border-right: 1px solid #fff;">
									<td style="border: 1px solid #337ab7;">Description of
										Goods and Pkgs.</td>
								</tr>
							</thead>


							<tbody>
								<tr style="background: #fff;">
									<c:forEach items="${airBillOfLandingJsonData.itHeader}" var="itHeader">
										<td>${itHeader.desc_Z023}</td>
									</c:forEach>

								</tr>

							</tbody>
						</table>




						<table class="table commercial-table" style="margin-top: -20px;">

							<tbody>

								<tr style="background: #fff; font-size: 14px">


									<td>INVOICE NO:
										${airBillOfLandingJsonData.itHeader[0].zzCommInvNo}</br>
										${airBillOfLandingJsonData.itHeader[0].zzLcpottNoType} NO:
										${airBillOfLandingJsonData.itHeader[0].zzLcpotNo}</br> Exp NO:
										${airBillOfLandingJsonData.itHeader[0].zzExpNo}</br> Leg 2:
										${airBillOfLandingJsonData.itHeader[0].zzAirline}${airBillOfLandingJsonData.itHeader[0].zzAirlineNo2}</br>
										S. B. NO:${airBillOfLandingJsonData.itHeader[0].zzShipping_Bl_No}</br>
										DIMN:</br>										
										<c:forEach items="${airBillOfLandingJsonData.itVbap}" var="itVbap">
<%-- 											<fmt:parseNumber var="l" integerOnly="true" type="number"
												value="${itVbap.zzLenght}" />
											<fmt:parseNumber var="w" integerOnly="true" type="number"
												value="${itVbap.zzWidth}" />
											<fmt:parseNumber var="h" integerOnly="true" type="number"
												value="${itVbap.zzHeight}" />
											<fmt:parseNumber var="p" integerOnly="true" type="number"
												value="${itVbap.zzTotalNoPcs}" />
											<fmt:parseNumber var="q" integerOnly="true" type="number"
												value="${itVbap.zzQuantityUom}" />
											</br>
											<c:out value="${l}" />x<c:out value="${w}" />x<c:out
												value="${h}" />/<c:out value="${p}" /> - <c:out
												value="${q}" /> --%>
										${airBillOfLandingJsonData.itVbap[0].zzLenght}x${airBillOfLandingJsonData.itVbap[0].zzLenght}x${airBillOfLandingJsonData.itVbap[0].zzLenght}/${airBillOfLandingJsonData.itVbap[0].zzTotalNoPcs}
										- ${airBillOfLandingJsonData.itVbap[0].zzQuantityUom}
										
										</c:forEach>
									</td>
									

									<jsp:useBean id="commInvDateValue" class="java.util.Date" />
									<jsp:setProperty name="commInvDateValue" property="time"
										value="${airBillOfLandingJsonData.itHeader[0].zzCommInvDt}" />
									<jsp:useBean id="lcDateValue" class="java.util.Date" />
									<jsp:setProperty name="lcDateValue" property="time"
										value="${airBillOfLandingJsonData.itHeader[0].zzLcdt}" />
									<jsp:useBean id="expDateValue" class="java.util.Date" />
									<jsp:setProperty name="expDateValue" property="time"
										value="${airBillOfLandingJsonData.itHeader[0].zzExpDt}" />
									<jsp:useBean id="zzShipping_Bl_Dt" class="java.util.Date" />
									<jsp:setProperty name="zzShipping_Bl_Dt" property="time"
										value="${airBillOfLandingJsonData.itHeader[0].zzShipping_Bl_Dt}" />										
									

									<td>DATE: <fmt:formatDate value="${commInvDateValue}" pattern="yyyy-MM-dd" />
										</br> DATE: 
										<c:choose>
											<c:when test = "${lcDateValue == 'Thu Jan 01 06:00:00 BDT 1970'}">
												<c:set var="lcdt" value="N/A"></c:set>
 									    		<c:out value="${lcdt}"></c:out>
											</c:when>
											<c:otherwise>
												<fmt:formatDate value="${lcDateValue}" pattern="yyyy-MM-dd" />
											</c:otherwise>
										</c:choose>																			
										</br> 
										DATE:
										<c:choose>
											<c:when test = "${expDateValue == 'Thu Jan 01 06:00:00 BDT 1970'}">
												<c:set var="expdt" value="N/A"></c:set>
 									    		<c:out value="${expdt}"></c:out>
											</c:when>
											<c:otherwise>
												<fmt:formatDate value="${expDateValue}" pattern="yyyy-MM-dd" />
											</c:otherwise>
										</c:choose> 
										</br>
										</br> 
										DATE: 
										<c:choose>
											<c:when test = "${zzShipping_Bl_Dt == 'Thu Jan 01 06:00:00 BDT 1970'}">
												<c:set var="zsbldt" value="N/A"></c:set>
 									    		<c:out value="${zsbldt}"></c:out>
											</c:when>
											<c:otherwise>
												<fmt:formatDate value="${zzShipping_Bl_Dt}" pattern="yyyy-MM-dd" />
											</c:otherwise>
										</c:choose> 										
										</br>

									</td>


								</tr>

							</tbody>
						</table>


					</div></div></div></div>
					</section>



	<section style="margin-top: -10px;">
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="col-xs-6"></div>

					<div class="col-xs-6"></div>
				</div>
			</div>
		</div>
	</section>

	<div class="col-xs-12" style="height: 20px;"></div>

	<!--<section>
            <div class="container">
            <div class="col-xs-12">
            <table class="table commercial-table" style="margin-top:-32px;">
             <thead>
               <tr>
                   <td>Container No</td>
                   <td>Seal No.</td>
                   <td>Qty.</td>
                   <td>Size</td>
                   <td>CBM</td>
                   <td>Mode</td>
                   <td>Kgs.</td>
               </tr>
            </thead>
               
               <tbody>
                  <c:forEach items="${airBillOfLandingJsonData.itItems}" var="itItems"  > 
                   <tr>
                     <td>${itItems.vhilm}</td>
                     <td>${itItems.vhilm_ku}</td>
                     <td>${itItems.vemng}</td>
                     <td>${itItems.wgbez60}</td>
                     <td>${itItems.ntvol}</td>
                     <c:forEach items="${airBillOfLandingJsonData.itHeader}" var="itHeader"  > 
                     <td>${itHeader.zzMode_Shipment}</td>
                     </c:forEach>
                     <td>${itItems.ntGew}</td>
                 </tr>
                 </c:forEach>
               </tbody>
            </table>
            </div>
            </div>
        </section>-->

	<section>
		<div class="container">
			<div class="row">
				<div class="col-xs-12">
					<div class="col-xs-6" style="margin-left: -5px;">
                    <table class="table commercial-table" style="margin-top:-10px;">
						<thead>
							<tr>
							<!--   <td style="border: 1px solid #337ab7;">Freight Details, Charges etc.</td>-->
							<td style="border: 1px solid #337ab7;font-size: 16px;font-weight: bold;" colspan="2">Signature</td>
							</tr>
						</thead>
						<tbody>
						
						
                        <tr style="height:116px;background:#fff;">
                        <td style="font-size: 15px; font-weight: bold; text-align: center;">
							EXECUTED ON: <fmt:formatDate value="${airBillOfLandingJsonData.itHeader[0].zzHblHawBdt}" pattern="yyyy-MM-dd"/></br>
                         	At: ${airBillOfLandingJsonData.itPlaceOfIssue[0].city1}  
                         </td>
                        <td style="text-align: center;"><img style="max-width: 40%; height: 80px"
											src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEAYABgAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCAGBA6MDASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD3+iiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiqlzcxW8YlmlWNGdYwXbA3MwVV+pYhaALdFFFABRRVS5uYbS3kuLiVIoIlMjyO21UVfvMT9KALdFedeHfi54b8U+JhodgLzz5PM8iWSHakpXJO35iw+UFhuVeFPQkLXotABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUVRvbyz060e6vbqG1t0+/LPIqIO3LHj86AL1FeWa/8cvCWjrJFZTTapdJ5i7bVMIGXoGkbjax/iXd61wGo/tHa5JMG0zRtOt4duCtyZJmLf7ylBj8KAPpKqlzcw2lvJcXEqRQRKZHkdtqoq/eYn6V8rP8R/iT4slubPT7y+kL5maDS7b54l3A/K0a+YFB2ry3sTzUH/Cv/iV4rBvrvTdVupIx5Ik1Ofy5AOu1RMysV+Y9OM7u9AH03/wnXhD/AKGrQ/8AwYRf/FVzl78ZfA1h9oX+2vPmgLZS3gd/MZR91W27TnHDbtv+1ivI/D/wA8R6gJH1m6g0iMZVF+W4kZvl52q23b97nduyv3a6ey/ZutEuUe98TTzQH78cNmsTn6MzMB/3yaANmP8AaC8JTNte21eHgtueCMj5Ru28SHk/dHuecCq1x+0V4aFu7W+mas8yqdiSJGis3bLB22/Xa1aVh8AfBdlcGSddQv1ZcGK5uQFHuPLVW/Wtb/hSfw9/6F4f+Blx/wDHKAOA/wCGmP8AqU//ACpf/aqP+GmP+pT/APKl/wDaq7+D4QeBLaeOaLw/GzIQwWS4mkXj+8rMVb6MMV0H/CC+EP8AoVdD/wDBfF/8TQB5B/w0x/1Kf/lS/wDtVa9r+0X4cktVa+0nVYLkgh0g8uVF+jMy5/75r0j/AIQXwh/0Kuh/+C+L/wCJrP1D4aeDNTgENx4X05EB3A20PkN/31Htb8OlAHI2/wC0H4QnukSS31aGN2CtLNAm1P8AaO2Rjj6LW4vxf8CSF3TxDHshG991vMuRuVflyvzHJHyjJxlugNYdx+z54QnuneO41aGN2LLFDOm1P9kbo2OPq1UNQ/Zx0OSALpus6jbzbgS9yqTLt/3VVDn8aAPVLDxHoWrzm30vWdPvZ1XeY7a6SRlHqQrZxzWzXzTd/s6eI47plsdW0qe2GCjz+ZE7fVVVsf8AfVVLfwx8XPBkDS6WNQ+yWTlY47S5W4iYMSu5bfLbgxbdynGd3GOAD6hor5tX4xfEDw0lnH4i0RCGdj5l7ZSW0s4U5baflXIBC8Lxx1JrrNE/aC0K8YR6vp9zphZjiRT9oiVQOrEKrDn5eFagD2aiua0fxt4a1/yF0vW7Oea43GK383bMwXJJ8tsNj5T/AA9BXS0AFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFeeePfilpPgZUg8k3+puCDaxTKPKxtOJDyyFlbcvy/Ng9OtZPxU+KqeEIZNI0qRJtdmXk/eW0Vv4m9W/ur9GPGFbzHwV8ItZ8dWDa7fatHa2t28hWd1M8077sMzLkcFt3LNncv3SDmgCpqvxY1LxBrU0utG9bRG3KulWF79mRlbaPLkkVS0isqkMDj7x27elc/q/iTRb6BYrDwbpVmqwvGsnnzvIhZ3ZWyJFVmUMoyytnb2XCr9F6Z8F/BGmxwiTSnvZoW3CW6mZt/zZAZV2ow9iuMdav6j8KPA2pTC4n8N2qMq422zNAuP92NlXPvQB81+FfiJ4i8HvA2n6hJLbRIyiwuWZ4PmO7Ozd8vzHO5cHjr8zCvqHwT4usfG2gR6tZq8bbjFPDJ1hlCqSoOBuGGGG7g9jkVl/8Ko8HJot/pUWjtBBfBDKUlkd1dd2yRWdm2su5unHzENkV5L4cuZvgn8TrjTtemkk0y8twPOtyQrRlvkmaMZOV2spXqu5tu4Y3AH0zXG/FHUv7L+GXiC4Efmb7ZoMbsf60iLP4b8/hXT21zDd28dxbypLBKokSRG3K6t91gfpXE/F3VH0r4Zaw0dwkM1wq2qByP3gkZVdV3d/L8zp2BNAHzv8KLaa8+J+hRQ3LwMs5l3JnlUVnZP+BKrL/wACr7Jr5R+A+m/b/iXBceZtFhbS3GNud+4eVj2/1mfwr6uoAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAoorzzx58TtI8DoY2ddQ1MuALGOZVMYwrZc87BtYY+XLZ6YywAPQ64bxD8U/CHh2ee3utXR7uEMWt7ZDK25Sw2ZX5VbKkbWZffGa+ffEPjXxj8SdQntrW3vZbEhnXS7FGkVY8rjftXdJ8yr8zcBj8u3pXUeD/AID3mpQRXviO6ksLeVdy2cKfv+jfeZhtjIO1sYbrtbawoAzte+PXinUbtW0nydIt15WONFndiQv3mkU55zjaq9ed1ZGmeBvHnxAuoruaK+kjMShb7VpJAnltuZdrNlmXr93d973r6W0fwT4a0DyG0zRLSCa33CK48rdMobII8xstj5j/ABdDXS0AeKaP+zxo1lN5ur6vc6goZWWKKMQIwHVW+ZmO7/ZZf5V31n8OfB1jbrbReGNLaNfutNarK+Pdn3MfxrraKACiiigAooooAKKKKACiiigAooooAKKKKACiiigArhtb+FXg7XIBG+hW1qyqypLZKIGUn+L5flY/7ytXc0UAeAax+zl/x8y6HrvoILe/i+m4tKn/AAIj937f7Vcrc3fxO+Fc1rNeXVz/AGcrG2gWSf7Tauq7SFC5+QELxwrbd23GDX1TRQB8+aJ+0VOsirr+kRsCxLTaeSu1dvA8tyd3zd9y8H8/VPDXxA8N+LBB/ZuqQfaJelpKwjnBC7mXyzy20fxLuX5WweDT9f8AA3h7xNDeLqOk2puLkBWvEiVbhduAGEm3dkAD2/hORXj3iv8AZ/v7TNx4XuhfRf8APndMqTDoPlf5VbqW524A/iNAH0bRXy94e+MHirwdNcaZ4itLzUFjUbbe+kaKeJjlstIylmDBujA8bduBXu/hjxvoPjGLfpGoRSSgBntZPlmj+6WyvcLuA3LuXP8AFQB1VFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFcf478Ww+C/C91q02WuCPJs42TcrzsDtBwR8vylmyw4U45wK7Cvmr4/eJ01HxDB4bhE0aaZ88+5/klkkVWXC/wCyv8R/vN/wIA4vw/oWtfFLxo6Pe+Zd3ANxeXc5B2INqswX8VVVX/ZGFXkfTXgXwifBPhuPRxqU1/tlaTzJE2Ku4/djXJ2r3PP3mZv4sVifCLwOfCHhwXVw041LU4o5LqF/uw7SxRdu1WVtr/MG/izXpVABRRRQAV538WfCtv4j8DX9xM4iutMhlvYJVjVjhULNGc87WA5wR8wVudu2vRKqXNtDd28lvcRJLBKpjeN13K6t95SPpQB5t8GPFkeveDLbTbi6i/tHTd0CRCRd7wKF2uUGCqqGEeeeV+9lq82+O3jD+1/Ekeg2soax0vPnBWyr3DfezhiG2j5eQGVvMFeZ6HreoaBrEGqaXOIL2Dd5cvlq23cpVvlYFfuseopskF5e291q87POouFSeeSTczTSB2XdzuYt5bnd+ZoA9n/Zut4Xn8RXBiRpo0t41kKjKqxkLLn3Kr/3yK+hK8E/ZvuJng8RW7Ss0Mb28iRluFZhIGYD3Cr/AN8ive6ACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigArP1LU7LRtOm1DULlLe2hXLyPnA/wA+grn/ABx4407wPorX94xkuJdy21orYedh6f3VHG5j90Y6khW+ab6+8V/FrxPiGB7u6Kl0tIpCsMChVViokbCBtoz83LfUUAdZ4y+Ouq6tJLa+GhJpliyAeeyA3D/Kwb+JljHzdV+b5d24Z2iHwJ8FtV1ycXXiK3utK03awClVWeZvmUAK2fLAPOWXn5dv3tw734Z/CG10Oxg1jxFZl9eEwkjjZwyWm0sq7drFWYjDbj935cbSpavY6AOX8HeF4fCHhq30e38h2iJaSaOExiVs/fZSzHccLn5vTG0AKOooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA5vxH4S0fxbpsljq9os+UYRTMo86AsesbfwnIX2O0Z3DNeD+Mfgv4g8LsNU8OT3GpQQyq0Qt1K3cJ3EqQq/e2/L8y4OedqgE19OUUAeEeB/jXdLqCaD41Rbe7EzRNqMi+UEbdwk0eBt/iG7oPl3LwzV7ZbXMN3bx3FvKksEqiRJEbcrq33WB+lef8AxE+FGmeNRJqNufsetCLaky4CTMMACUAZPAK7l5Ax94KFrybwl4q1r4N6veaLr+lTm3ufLmeETcpnAMkfVGyu5W9WVRuG00AfUlFZ+m6nZazp0OoafcpcW0y5SRM4P+fQ1oUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABXxdpFjN49+I1vDPGBJq1801yLdtuxWYvKU3dNq7sZz93+Kvqj4g3EVv8PfEMk0qxI2nTxhnOBuZCqr9SzAV8//AAB06K9+JSzylg1jZS3EeP4mYrHz/wABkb9KAPqmiiigAooooAK5zxtrQ8PeB9Y1RJvJlgtn8l9ucSt8sfH++y10dfP/AO0D4w/49vCVnN6XN+Ub/v3GcN/wIqy/882FAHhtlYz6lqNtYWsYe5uZViiTdjLMwVV59/519CeP/BdloHwHjs/ISK405oLmYwH5ZrlisbszHlh+8bHT7qjgLtqn8C/AcJt4PGd2svnhpI7FFYbdoyjScH3Zdp/us3zblK+gfF2xn1D4W67DbpvkWJJmGQMLHIsjH/vlTQB51+zN/wAzP/26/wDtavf6+Zv2d7mZfGeo2wlYQyac0jJu+VmWRNpx6rub/vo19M0AFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFcf448cad4H0Vr+8YyXEu5ba0VsPOw9P7qjjcx+6MdSQrdLc3MNpbyXFxKkUESmR5HbaqKv3mJ+lfJnjTWLz4lfEiT+yIHvFdvsenwoo+aJd2G5VcZ+aT5vuhuThaAKX/FTfFjxn3ub6cfSG2iH/AKDGufc5P8TN830x4E8DWXgXRRp8JjuJ3Ym4vBEY2uD8235dzcKuFC9PvN1Y0/wP4H07wPoq2FmpkuJdrXN2y4edh6/3VHO1R90Z6klm7CgAooooAKKKKACiiigAooooAKKKKACiiigAoqpc3MNpbyXFxKkUESmR5HbaqKv3mJ+leIeIf2hVhuLu18PaTFOquBb31zK21+hY+VhWx94D5h24/hoA96or5Xf4m/E3xW15/ZIuTCVVZYdKsN4i3LgYbazqTg87s5zjpWT/AMXf/wCp4/8AJugD6+or5R03V/jJpXm/ZofFEnmYz9pspLjp/d8xWx17f0q8fir8T/DFxHLrsEmyZWWKLUtO8lWK7SzLtVGYr/vY+YUAfUFFeMeGPjc2p2/2rW/D91p+lxLsuNWtleaBJvl+UgJ8oYt03MRuX13D1myvLPUbRLqyuobq3f7ksEiuh7cMOPyoAvUUUUAFFFFABRRRQAUUUUAFFFFABXH+OPA+neONFawvFMdxFua2u1XLwMfT+8p43KfvDHQgMvYUUAfJ2ja94l+DPiyfTtQgaaAhi1oZ2WGUMV2zRnG3P7tRuK527lO0/d+odN1Oy1nTodQ0+5S4tplykiZwf8+hrkfiR4Ah8e6IkHmGDU7Xc1pMx+QM23crL/dbavI5XgjPKt5l8F/HEmiakPBOsxyRI9xItu0z7Wgl53QsrH5QzLwq4O89G3fKAfRdFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAHKfEayt7/wCHXiGG5j3xrYSzBd2PmjUyKfwZVrwr9nH/AJKHf/8AYKk/9GxV7/4ygmufBOvW0ETyzTadcRxRxruZ2aNgFC9zkivnL4D6l9g+JcFv5e4X9tLb53Y2bR5uff8A1ePxoA+rqKKKACiiuO8ceO9M8DaeJ9QZ3ubhH+yW0StmdlK5G7BVfvL17ZwG6UAVviN45i8B6AbxY47i/uXMVnAzAAtg5dsHcVXvt/vKvy7tw+a/CmhXvxD8fRWs7yO17O1xfzoNu2Pdukb5VKqT91eNu5lHFUtd1zVvGuvte3zNd3tywjhiiDNj5vljjXn+9wvfJ/iNfUHw78BQeBNAWABH1OcL9tuEZtsrKzFcA9l3bei56ntQB2FtbQ2lvHb28SRQRKI0jRdqoq/dUD6VDq+nQ6vpF9pk7OsV5bvA5TqFZSpx+BrRooA+QPgn/wAld0L/ALeP/RElfX9fHt1Fo/hf4zIlvcj+ydO1mNjJ5bfulWRWZecs2zDLnndtz/FX2FQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQB5B8dfFM2i+E4NHthIkusO8byA7dsSbd65z1bcq9wV3Z7Vzf7PvheGVr7xPcFJJIWNlbKPm8tsBpG+78p2sqqwboz561wHxc11tc+JGpsWcx2T/YohIACvl/K3Ttv8xvxr6U+H1lHp/gHQbSO1uLZ47YedFPG8brL96Tcr/N9/d+m35aAOuooooAKKKKACiiigAooooAKKKKACiiigAooooA8J/aI1O7ttL0XT7e4kjtbxp2uIl6SlNm3Pf5dx46Zxn7orQ+Bfg/T7TwzB4mdEnvr0t5bPCubZUZ48I3XLZbJ7jAx8vOv8Y/Bc3irwgtzZW8lxqmms0sUcY3NIjY8xFG7rwrfxN8m1R81cN8CvHMFiW8KandtGk0vmaczk7dzZ3RZLYXcSpVcAFi3O5lyAfRFFFFABRRRQBxWueALHUjc3Ol3d7oOp3IzJd6VM0HnN82DKinEnzOzFuG/2hXjGo6549+E3jOP+1dVvdXsTu8r7TcO8N3Fxu27t3luvHup/vK3zfTlYHiLw7pvinRp9L1W382FySrLjfE38Lq38J/xIPGaAIvCfinT/GOhRavphkEEjMhjl2+ZGysRtYKzBT0b/dZa6Svk/wAPape/CX4pT6deXbrYRzi3vl6LLC3KTFVDcqrLJtHzcsufmavrCgAooooAKKKKACiiigAooooAKKKKACvmb43+GJfDni2DxPpu+CC/YOZYMp5N0vO4MqjbuHzddzMHPavpmuJ+JPhQ+MfBl3psQLXkQM9mMZPnL0X7yqNyll+bhd27+GgDR8FeIW8U+ENN1poxG9zEfMXGB5ikq+35m+XcrbcnOMV0tfPn7OuvHz9V8PM0hDKt7CoVQqlSEkyeuW3R47fKeh+99B0AFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFfF15GPA/wAS2C28zw6Rqglijnby5JY45Nybjt43KF+bb/FX2jXzx8fvClwmoW/im2tC1s8S2960cQ/dsCfLd2zltwYLnHGxV3fMooA98trmG7t47i3lSWCVRIkiNuV1b7rA/SrdeI/BX4ijV4YfCup+WLq0t8WUuQgliTA2Fe7qvTb95VO7lctV+MPxSvtKv10Dw3qEEZETG7uoHWSRWyyND6Rsu3ccfNkrjbj5gDtPiF8TtM8FWMsUE0F1rTHbFZq4bymIB3TYOVXBB29W7d2X5kvL3XfGWt+ZLNe6vqU27y0CmSTHzMVVV+6o+Ztqjao/Gq9jYal4l1yO1tknvtRvpCcFizyMfmZmY/iSx/3j3NfUXw4+Gdj4HtFunb7TrUsRinuAWCKrEMUVf7vyr8zDcf8AZztAAz4b/DCDwNbyXM8sdzq8y+XJcRBlVEbYWjCltrDcgO7arV6RRRQAUUUUAfJXxt0YaV8Srx1SKOG+iS8QRrjlgVbdx95nRm/4FX0Z4L1eXX/B2j6nM7PcT2qGWTKYZxlWb5PlzuDHb1XjKqcqPL/2jNHDWGi60kcI8uWS0lbGHbcN0Y6cquyT/vrjqa1/2fta+2+DbjSWuN8unXB2xhNuyKT5l+bHzfP5vfP4YoA9iooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiis7VLqay0i8u4LWS6lggeWO3T70rKpYIOD94/L0oA+PvDFhceMviNp8N4gvJNRvxNeDcE8xS3mTH5du35dx4x/s9q+0q+Pfg9bw3PxT0KOaNJFEksm1lzhlidlb6gqCPevsKgAooooAKKKKACiiigAooooAKKKKACiiigAooooAK+dPip8G5bS5bXPCtk8ttK/+kadbxlmhZj96NV6rzyo+72+XhfouigD5/wDhp8a/9Ro3i26/uRWl+w/3s/aJN3+4obb/ALTHq1fQFeV+P/g/pvi6T7fpfkaXqw3MzrCPLuS25v3m3o24/fAJ65DfLt8f/wCLh/Bu/wD+Xi3sml/67WVzlvyVmEf+zJt/u0AfWlFeO+F/jzoOqPYWesRz6ffynZPOAv2VW7EMW3Kp4+8vy55bClq9Usryz1G0S6srqG6t3+5LBIroe3DDj8qAL1FFeffEn4j2XgTTNiFLjV5wfs1tu+6McSPjkKP/AB4/KP4mUA8J+Lk7at8WdShsVW5cNFbRrbRhmkfYqlfl5Zt2V/vZG3+EAfW9fNXwT8JT+IfEsni3U/31vZyNtklbzHmuyQ25shjlQ27d8p3bcZ+avpWgAooooAKKKKACiiigAooooAKKKKACiiigD5HihbwP8cI7eNo9OgtdWUKZJFZY7V2/iZs9YpOrcjPYivrivl/42S3Oi/FSy1K2aCO5W2guYZEhAKsrsFZ8lvMYFPvegVcfLz9QUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFZerw2U+i38GotGtjJBIty0knlqImUhtzZ+Ubc5b61zPjn4k6T4CgQXpkub6dC0FnEw3N1+Zj/AAru+Xd164Vtpr538V/EPxH481A2jS3C2U8u2DSrXO1tzLtVgo3StlVPzfxE7QvSgDnfEFtpVtrt1Dol8b7TUbMEzxtGzLtDYIbn5T8vQZ25A5rofBXwx13xoyzQxmz01SrNeTxtsKltreX/AH2GG/2fl+Zl4r0vwJ8C3tbu01XxbJBcMhDrpsY3IDhcCRuA2Pmyqgqdo+ZlyK9ttraG0t47e3iSKCJRGkaLtVFX7qgfSgDC8L+DtG8G2H2XRrNovM2maV2LSTMo2hmY/oowoLNhRk11FFFABRRRQAUUUUAcj8SbCHU/hr4ghmZ1WOyknG04O6IeYv6oK8N+AWpraeP57KS5eNb+ydUhG7bJKrKw6d1VZOT0+YZ55+oK+NfGdpN4R+J+p/Y2ht5bO++12hgQFIQzCWMKpG35VZeOnagD7KoqjY30GpadbX9q5e2uY1lifbjcrKGVufbFXqACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAqpc20N3byW9xEksEqmN43Xcrq33lI+lW6KAPjT4fA6d8UdEhvvttrLFfrC6wny5VfdtCsD/AA7sKy/3dwr7Lr5B+ItjfeE/ivqF1FJIkxvf7Qtp2iwPmbzAy7uG2sWX0ypr6m0DWLXxDodnq1nJvt7uISLkjK+qnBPzKcqeeCtAGxRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFVLm2hu7eS3uIklglUxvG67ldW+8pH0q3RQB4r40+BNhq7fbPDTw6ZckHzLaUN5EjM2cgj/V9W+VQR90ALXl8OrfEL4XX8FrLJe2cMYxHaXLebbSLlZGC87TjcNzIQw3HkZNfVWpanZaNp02oahcpb20K5eR84H+fQV85fFD4vDxXZXOgaPCI9JaYeZcyH57pV2suFK/u13Lu5+Zvl+78y0AdJdftG250gtZ6FKNVbcuyaYNBH97a25fmf8Ah+Xavf5u553wd8P9S+Kms33ifxBLPb6bcyuxkjPzzNk/LDu3bY1IC5+b7oUfxFc2y+BnjG+0VdSKWUErxb1s55WSfP8ACpXbtVm9GYYz8205wnhn4g+Kfhhqc2h6jayzWtsZFl02dgvluw3bo32ttHQ/xKwZmH3t1AH1HbW0Npbx29vEkUESiNI0XaqKv3VA+lW64vwh8Q/D/jdGXSLqVbqMb5LSddkqLuI3H+Fh0+6WxuXOCcV2lABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAHzF8frGQ+PhNEk0gGlxTSnc0gjXzXQNzwi52jjA3N6tz9I2q3QtohcPG84Ub2RCqluc4UsxX/vpvxr5v8Ajiupf8LBSK7ae10u5ihSCWZi0LBd25sIu75TLJ8p3MNxI+VlUfTlABRRRQAUUUUAFFFFABRRRQAUVUubmG0t5Li4lSKCJTI8jttVFX7zE/SvIPF/x507S/tdj4ci/tG+hlVBczDNqR/EV2sGfuB0HcMRjcAepaxremeH7J77Vr2Gztl48yZtu44LBVz95vlbCjJ9K8T8Z/HppBNY+EUMJ3D/AImNwq9AzZ8uMg/eG35m7FvlB+auBtLfxt8WdZCtPNqUkBUySTMEgtVYqudowq525wq7m2scNg17f4E+DeieFxaalqCtf6zFtk8wn91DIN3+rXvjcPmbPKhhtNAHk3g74S+I/GOo22p6uJbXSbwG6mvZpFaacZyQqsS25ifvMNuPm+bgN9BeGPBGg+DotmkafFHKQFe6k+aaT7obLdg20Hau1c/w11VFABRRRQAUUUUAFFFFABRRRQAV8+ftFaCfP0rxCqyEMrWUzFlCqVJePA65bdJnt8o6H730HXA/FzQxrvw41RSqGSyT7bG0hIC+X8zdO/l+Yv8AwKgBnwm8RR+IPh9YeVA8DaeqafIjEEM0ca/Mp9GDKfz+teg18+fs668fP1Xw8zSEMq3sKhVCqVISTJ65bdHjt8p6H730HQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAHz/APtFeHv+QZ4kV/SwkQt/vSIV+X/rpuJP93Hetn4DeKIdR8Lv4dlZYrjTGaSNAQvmQyMW3ct821mbd8uAGTvXp+v6Pa+IdDvNJvI99vdxGNsAZX0YZB+ZThhxwVr5K0zUdZ+GHj58h0udPnMNzErMq3EW7lfmHKsuGVivdWAzigD7NorP03U7LWdOh1DT7lLi2mXKSJnB/wA+hrQoAKKKKACiiigAooooAKKKKACiiigAooooAKKK8v8AFvxm8PeGQsdhLHrN1KoZEtLhWiUbgP3kgJ2nG7CqrH5edoIoA9QryDxh8ctD0aBoNAePV7/c6EgssMRX+Itt+cZ/u/eAY7l+Xd5Rqes+O/i9fi1itZLuKAiRba0j8u3gYrwzFjwWCtgyMerBeuK9Q8GfAnS9JiiuvExj1O+VyfIRiLdPmUr2DSH5eQ3y/Nt2nG4gHmVrpPjj4x6tLfu/m2qSsolnkKW1sWUEoi/NjhVHyhm5Vm67q9u8BfCrRvBLJdqTqGqruxfSLt2hh91E3ELxxn73zN82G216LRQAVy/ijwdo3jKw+y6zZtL5e4wyoxWSFmG0srD9VOVJVcqcCuoooA+XPFXw08TfDjVl1/w/PNcWdvK0kN1bqTLbKq5zKoXG3G5S3KsAd23dtrsvBPx2sr57TS/FEX2a7O2P+0lI8qRvm+aQceX/AA8ruXLE/KvT3GvEvHnwNs9Qim1DwssdndqhZ7Dnyrht275WZsR9T8o+XhR8oy1AHttFfOXwu+KdzoV/B4V8UzeTYQboIprhCsltICqiORiw2oPmXlcqduSFHy/RtABRRRQAUUUUAFFFFABRRRQAUUVnavqMOkaRfanOrtFZ27zuE6lVUscfgKAPnS5ubzxf+0fCiv5ZtNVWOOOWZmVY7ZtzbePl3eWzbem5/wDgVfTlfLvwM0241j4ky6xcPczGyhe4e43bt0snyfOzf3g0jep2+xr6ioAKKKKACiiigAoqje3lnp1o91e3UNrbp9+WeRUQduWPH51434r/AGgbG0Bt/C9qb6bOftd0rJCOh+VPlZupHO3BA+8KAPZ7m5htLeS4uJUigiUyPI7bVRV+8xP0rzDxt8bdE8PxtBobwaxqJDANHJ+4i4yrMw+/yV+VfRvmUivELvWfGHxO1prYSXuoSSyecLKFm8iHnarKmdqKu4Lub1yzck16V4O/Z+/1N54suf7rfYLQ/wC6dskn/fSsq/VWoA86k1Lxl8U9cltFe51CWd2nWyRytvAFUDcqs21cA7dzc/N1LNz6l4U/Z+sbQC48UXRvps4+yWrMkI6j5n+Vm6g8bcEH7wr1rR9E0zw/ZJY6TZQ2dsvPlwrt3HAUs2PvN8q5Y5PrWxQBRsrOz060S1srWG1t0+5FBGqIO/Cjj8qvUUUAFFFFABRRRQAUUUUAFFFFABRRRQAVUubaG7t5Le4iSWCVTG8bruV1b7ykfSrdFAHyN8O55/B/xitLG5lclL2TTZxbsdrsxaNQemV37W5/u19c18jfFGY6J8ZdSutNCW0sE8FzGyxj5ZPLR9xB4zuy3P419XW1zDd28dxbypLBKokSRG3K6t91gfpQBbooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACvFPjX8PDrGmt4o0yC2jvrOJmvVHytcQqv3t2cbkUH3Ze/yqp9rooA+Xvg18Qh4c1dtI1nUGTSrpAsJmkLR20u7cDy21Fbc244+9tJwAxr6hr5f+MPw1m8O6jNrulWsa6FcOC8duhxaSNjIYdlY/dx8oLbcL8u7r/hJ8Wv7VEHhvxJcf8TA4js76Rv8Aj4/uxyN/z0/ut/F0+994A9xooooAKKKKACiiigAooooAKKqXNzDaW8lxcSpFBEpkeR22qir95ifpXlHjD47aJpJktNAQateoGXzuVtkb5h9770mGVT8uFZW4agD2KvLfFfxr8MaAzQaax1m9BBxbSYhU5XlpeV6N/Du5Uq22vHryf4gfGG8kmhtZ7mzi5EEDeTaIy8cF22+Zh/Vmw392vQfCn7P1jaAXHii6N9NnH2S1ZkhHUfM/ys3UHjbgg/eFAHn154i+IPxZumsrZJ57bjfaWQ8q2j43DzGZsc+WWXzG+8Dtx0rvvCfwAtrKSS58VzR3z78RWtpKyxFdvV22q2c9Nu37v8W7aPZ7Kzs9OtEtbK1htbdPuRQRqiDvwo4/Kr1AFGys7PTrRLWytYbW3T7kUEaog78KOPyq9RRQAUUUUAFFFFABRRRQB5L8Wvhhb+JrCbXdMjZNbt4yzBI932xVU4UqBzJgYVup4U/w7eY+BPjqWS4/4RTVLq4llY+ZpzOWcBVj+aLJb5VVVDKMY+93K19A18t/GjwyfC/jqHV9MM9supbrtZllwVuVfc5XncuNyN/vN8vTaoB9SUVgeFddt/FHhyy1q1UrHeRB2TJPlv0Zc7Ru2kFd3fbxW/QAUUUUAFFFFABRRRQAV4d+0F4kgh0m08NpcTpdTyrdzRplUMC7lUN2bLgEL6xknHymvZrm5htLeS4uJUigiUyPI7bVRV+8xP0r5VS3m+LfxemeOJ4bS8n8x2VdrRW0aquW+9tbaqrn7u9uuKAPWvgX4al0bwRJqc9uI7nVZRKu7duMK5WPcrYA6uwx95WXmvW6qW1tDaW8dvbxJFBEojSNF2qir91QPpWTrXi7QfDUbf2xq1tauEWTyWfMrKzbdyxj5mGc9F7GgDoaK8R8VftAaXawTweGoZL66DLsnuYikG3qx27ldj/DyF9ecfN5PrXjXxn49n/s2a5ubpZnZk06xiwrZ+bbtX5nC7c/Nu27aAPo3xD8U/CHh2ee3utXR7uEMWt7ZDK25Sw2ZX5VbKkbWZffGa8i8S/H/W7x5Y/D1rDptuPljmmXzZz8xO7n5F3Lt+Xa2Pmw3Sjw18ANbvHik8Q3UOm24+aSGFvNnPzAbePkXcu75tzY+XK9a9g8PfDTwl4cWOSy0a3e6j8tvtNz++k3L91xuyEbv8u3n6UAfOdn4b8c/EvVo797e8vBMP8AkIXW5YFTzMNtZvl2qzN8idMHavBr1Dwp+z9Y2gFx4oujfTZx9ktWZIR1HzP8rN1B424IP3hXuVFAGbpuk6bpEDW+nWFrZRM3mNHbQrGpb12r9K0qKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA+Vvj9p0Vl8SmniLFr6yiuJM/wspaPj/gMa/rX0B4AmSf4f+H3VXA/s6AEOjLjaijgHtxwRweo4ryn9pn/mWP8At6/9o16F8Hrie5+FmhyTyPIwSVAztn5VldVH4KoFAHe0UUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQBn6lpllrOnTafqFslxbTLh43zg/wCfUV8ufEX4Uan4LMmo2x+16IZdqTLnfCpAwJQBxkkjcvyk/wB0sFr6zqpc20N3byW9xEksEqmN43Xcrq33lI+lAHzb4O+O2uaQIrTX0/tayQqvncLcovyjO77smFVj82GZm5avoPQfEOmeJNMW/wBIvFu7diULBWUqw6hlYZU/X29q8v8AGnwJsNXb7Z4aeHTLkg+ZbShvIkZmzkEf6vq3yqCPugBa8YnsPF/w41uOZ47zR77ACSKfklVdrFQy5WRfu7l+YZ+9QB9o0V4X4W/aCtb24jtvE1klipVi17bMzR7uq5jwzDjj7zc9sH5fVtB8UaJ4lszc6PqUN4qj51Xh4zzjcrfMuSDjcKAN6isTWvEWj+HYPtGrapbWSlWZVmkAZwvXav3mPTgV474n/aFSG5+z+F9PSdI2ybq+Vtr43fdjUg8/KdzMO/yg80Ae5XNzDaW8lxcSpFBEpkeR22qir95ifpXk3ib4+aPpWovZ6RYPqywsyyXH2gRRMQP+WbbW3DO4Z+UcDG4GvMG0P4hfFq+/tSSCaW0eX93JM5itYFY7W8sMeVXYN20M3y/Nubr6d4V+A+j6Y9ne65ctqV3GwkaARhbfO0fKykbnAbPPy7uNy9RQB5h/xcP4yX//AC8XFksv/XGytsN+TMok/wBqTb/er1bwX8EdG0Ex3mttHq97t5geEfZoyVUEhWH7zb82Gbgg52hhXqFlZ2enWiWtlaw2tun3IoI1RB34UcflV6gCpbW0Npbx29vEkUESiNI0XaqKv3VA+lW6KKACiiigAooooAKKKKACiiigAooooAK8p+P2nS33w1M8W1Vsb2K4kz/ErBo+PfdIv616tXlvx51L7B8M57cR7zfXUVvndjZg+bu9/wDV4/GgCv8As/Xs918OZIZpMraX8sMS45VdqOR/31I35161Xj/7OX/JPb//ALCsn/oqGvYKACiis3UtW03SIFuNRv7WyiZvLWS5mWNS3pub6UAaVFcBrvxb8GaJG27V0v5QodYdOPnlhnb95fkB9mYfyrkdQ/aO0OOANpujajcTbgClyyQrt/3lZzn8KAPbaqXNzDaW8lxcSpFBEpkeR22qir95ifpXy9qPx48bX3lG2uLLT9gO77NbK28/7Xmbuf8Adx1rgtU1/VtaWIarqt7f+SW8v7VO0pTdjdjceM4HT0oA9V+MXxPt/EOfDeiTLJp0Uu64u0fidl/hXnDIDzuOdzKpXhQzcJ4L8e6h4FnurjTbLT5p7lVQvdwszRqueFZWXG7I3eu1T2q54L+F+u+OtPuL7TZbKGCCbyS9zKy7m2hiF2q3TK9v4q9b0f8AZ40G1Ktq+qXmoOJd22JRAjL/AHWHzN1zyGXrQB4zrHjvxh4xSHTL7VLi8ErbFtoIwnmsxXarLGAHOVXAOcHpzWvofwX8Za6qzGwj06F0Zlkv5NhyrY27BudT/vL/AEz9OaL4d0fw7B9n0nS7ayUqqs0MYDOF6bm+8x68mtugDxbQv2fNFsbgy61qM+qRrgpbxobdDwQd2GZm/hI2lfu87q9R0fRNM8P2SWOk2UNnbLz5cK7dxwFLNj7zfKuWOT61sUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQB85/tG3FmdZ0S3VY/tyW8rSkR/MY2YBMtjkZWTC/w/N/eFeo/CHZ/wqzQfJ8jHlSZ8nftz5jbvv/Nu3Z3fw7t235dteNftHf8AJQ7D/sFR/wDo2Wvf/Av/ACTzwz/2CrX/ANFLQB0NFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABWPrGiaZ4gsnsdWsoby2bny5l3bTgqGXP3W+ZsMMH0rjfEvxm8J6BBKttef2pdo2EisvmRm27hmX7u3op27iM/d4NeMa98RPGvxFnnsNMguUtXVmbT9Mjd2MfzL+8Zfmb5X2t0Vvl+WgCX4o+D/Bvhjy5PD2teffPcyRz6d5yzeQvflfmTafl2vuZs/7LV51Y3t3p92l1ZXU1tcJ9yWCRo3Hb5WXmvYPDP7P2q38KS+Ib4aZhlzbW+2WRl3HduYHap2jg/N97nptr2Lw18P/AA34TEH9m6XB9oi6XcqiSfJXazeYeV3D+Fdq/M2ByaAPj2+vbvULt7q9uprm4f78s8jSOe3zM3Nd54L8SeANFkQ634SubyWJd32mS5W4DPhV2+UwjTbyzDduZT3PVffNb+FXg7XIBG+hW1qyqypLZKIGUn+L5flY/wC8rVwl/wDs3adJcK2neIbq2i2/MtzbrOd3+8rJ/KgDrdM+NHgjUo4TJqr2U0zbRFdQsuz5sAsy7kUe5bGOtddYeI9C1ec2+l6zp97Oq7zHbXSSMo9SFbOOa+bNR+A/jax8oW1vZahvB3fZrlV2H/a8zbz/ALuelcxe/DjxjYXLW8vhnVWkX7xhtWlT8GTcp/OgD7Tor4hh8Z+KIII4LfxHq0MMSqiRxXsiqir90BQ3QV1EHxp8diRi+txyja4xJaRDquA3yrn5fvY6cc5HFAH1tRXyY/xu8dMJdmqwKrRKi4tY/wB2y7cuMr95tpznK/M2FX5dulb/ALQfi+C2SOS30meRFCmWWB9znuSFdVz9FoA+oaK+Yf8Aho3xf/0DtD/78y//ABytDTf2jdWiEh1TQrK5LbRH9llaDb/e3bt+e3pQB9HUV4B/w0x/1Kf/AJUv/tVH/DTH/Up/+VL/AO1UAe/0V88Xv7SV41s62XhmCG4H3JJrxpUH1VVUn/voVY/4aY/6lP8A8qX/ANqoA9/or5T1H48eNr7yjbXFlp+wHd9mtlbef9rzN3P+7jrWb/wuz4h/9DAf/AO3/wDjdAH19VG9vLPTrR7q9uobW3T78s8iog7csePzr41vPFfinxDeLBcaxql7LcyMI7dZnYM8gZSqxqcfMHZdqjG1ioGDipbL4ceMb+5W3i8M6qsjfdM1q0Sfiz7VH50AfU158RvB1jbtcy+J9LaNfvLDdLK+PZU3Mfwrwz4x/Eiy8YTWmk6K0sumW5815njKieUj5SqsAwCqzDJ+8W6cKTzGsfDTxJ4b0S41XXILaxihKJHHNcxs9wzHGI1XcDt+8Q2Pl6dDS/DrwFL8QNWurFbtrKG1t/Na4FuZV3FgFT7y4z8xHOflPvgA6zwf8bIfBvhDTdDi0N75rZZC8rXPlctK7AAbW7MP1p9x+0V4lNy7W+maSkLM2xJEkZgvbLB13fXatdfa/s6eHI7VVvtW1We5AJd4PLiRvorK2P8Avquss/hD4EsLpLqHw7Azpn5Z5ZJU59VdmU/lQB86v458feJdSjS31vV57tlKpBp7GMuFy33Idu7vlsZqrp/w18aancGGDwvqMbAbibmHyF/76k2r+Ga+wrKzs9OtEtbK1htbdPuRQRqiDvwo4/Kr1AHzTafs6eI5LpVvtW0qC2OS7weZK6/RWVc/99V12j/s8aDalW1fVLzUHEu7bEogRl/usPmbrnkMvWuv8R/E7wt4WWRbrU47i6VjH9ksysswZW2srfNhSuedzL91sZ6V4T4v+LPiLxy66Xp8T2FtKfLW1s3dpZwwC7HYY3g/MNoUfexhqANX4g6x4B0LRbjw/wCDNL064vJHUy3/AJS3SxxsvzLHK7Md3C/d+Ubm6NVX4YfCS98TXcepa7BPa6Iu1xGytG94DhlC9MRkYyw6g4Xuy9P8PvgfdQ6hFq3i+KApCMxacHD5YE483HylcfNtDHduG7oyt77QBUtraG0t47e3iSKCJRGkaLtVFX7qgfSrdFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRVS5uYbS3kuLiVIoIlMjyO21UVfvMT9KAPln4x6jJr/wAU7iztVS5a2WKwt1g/eM7feZcc5bzJGXH+zjrX1NbW0Npbx29vEkUESiNI0XaqKv3VA+lfL/gWyTxt8bjqMEU62S38uqOcqroquXTdn/bZFbbnr/wKvqugAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKK4HxN8VvCXhiWS3uNRNzdwfetbRfNfdu2su77qsvzZVmU4+ooA76qN7eWenWj3V7dQ2tun35Z5FRB25Y8fnXzprf7QOvXqGPR9PttMDKuZHbz5QwbqpKqo4+XlW+vpg6R4B8c/EKb7XdNcsqW6Ml9q8k22RG+ZVjZlZm+8zfLlf++huAPUvFXx40fTHvLLQ7dtSu42MazmQLb52n5lYHc4DY4+Xdztboa8tuNR+IvxPlcQrqF3aTMY2gt/3VmpXMm1jwmV+Ujexb7oyeK9g8NfA3wvosavqKvrN0rK3mTfJEpUt91FOD23BiwO33xXpNlZ2enWiWtlaw2tun3IoI1RB34UcflQB4h4Y/Z6SG5+0eKNQSdI2wLWxZtr42/ekYA8/MNqqO3zA8V7Fovh3R/DsH2fSdLtrJSqqzQxgM4Xpub7zHrya26KACiiigAooooAKKKKAKlzbQ3dvJb3ESSwSqY3jddyurfeUj6Vlf8IL4Q/6FXQ//AAXxf/E10FFAHJXnw58HX1u1tL4Y0tY2+80NqsT49mTaw/Csz/hSfw9/6F4f+Blx/wDHK9AooA8zufgf4FnaExaZPbCOVXbybuT96oyCjbmb5W4zja3owq5/wpP4e/8AQvD/AMDLj/45XoFFAHk97+z94Nurh5YZdUtFI+WCGdSi/TerN/49S2nwG8GW5i81L+6WJmLLLc484MuFDbQv3TkjbtOTzuGBXq9FAHk83wF8GTX8ksaajDE4ZRbpcgpH8u3cu5WbOfm5bqOmPlrY/wCFJ/D3/oXh/wCBlx/8cr0CigDkrP4c+DrG3W2i8MaW0a/daa1WV8e7PuY/jVuHwb4YtbhJ7fw5pMM0TK6SR2UasjLyGDBc5HtXRUUAFFFFAHh/7Rd1droOjWiQE2ct08ks204V1XCLu6Dcskh5/u8dDWr8BH0xfAbQ2V2Jbv7S8t7AW+aF2+VcAfwsiKe/O7njC9h428I2PjbQJNJvGeNtwlgmj6wyhWAYjI3DDHK9wexwa+e4/hh8SvDU91c6ZaXULRK6m4069VWljHzYUKyyNnbnbt3HjjPFAH1fWPrGt6Z4fsnvtWvYbO2XjzJm27jgsFXP3m+VsKMn0r5js7P4wX9yttFJ4wSRvumee4hQ/VnZVH502z+B3jy5u0im0uCzRs7pri7jKL352MzfkKAPW9e+PXhbTrRW0nztXuDyqRo0CKAR95pFGOM42q3Q5215f4k+MPi/xTqBtNGe40y3uNkUNpZHdMz7geJFUOWZuMLjj5cH5i3V+H/2cz+8fxJrHqqQ6afoQxkkX/e+Xb6Hd2r1rQvBfh3wpvbRdJhtZHyGmy0ku3jK+Y25tvyr8ucZoA+cfDXwY8W6/NE1zZ/2XaONzy3w2yKu7af3X3t3VgG2g4+9zX0J4R8B6F4LtfI0u03XBJ8y7nCtO6tglSwUfL8owowMrnrmuwooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigArzT4z+JI9B8AXdqk+y81Mm0iQbSzKw+f5W/h2ZUsOhZfWvS6+V/i3rUvi/4mLoumiScWrLp1vGXwrzs3zYVgoX5mEef+mYbdigDtP2dNHiTR9W1xtjTyzrZplBlFVVZvm9GMi/L/ALAr3Suf8JaGvhvwppekKqB7aBVlKOWVpTy7DdzgsWaugoAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiqN7eWenWj3V7dQ2tun35Z5FRB25Y8fnQBeory/Wvjj4M0i6MEF1dakysyubKIMq7T/eZlVgecMpNebal8ePFmr3UlnoWn2tibhkW3CI1xcK52/Kpb5WLHjHl/xY6/NQB79rXiLR/DsH2jVtUtrJSrMqzSAM4XrtX7zHpwK8q8QftDaba+bHoGmz3r/OizXLeVGG/hYKNzMpPO07DjHr8vD6N8IvG3jC5Opa08lkJSu+fVHZp5FGFPyHLZUL/Ftz8uD6eo+Gvgb4X0WNX1FX1m6VlbzJvkiUqW+6inB7bgxYHb74oA8mvPGfxH+Jby2FgLt7WRljlttNi2RJuUriR+oVvm4dtvX+7xv6F+z3rFzc28uvXtrZ2rKryRWpaSYcrlD8oVTt3DcC3OOGFfQdlZ2enWiWtlaw2tun3IoI1RB34UcflV6gDgPC/wr8LeEi721ib66Eqypc3yxyyQsp+XYQqhcfe459+BXf0UUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAcH8S/GyeCvDMk8DE6nd7oLJQygq23JkKt94Lx2b5iqn71eRfAXwzPqXiiTxFKrxW+lq0cbgbRJK6ldv3fm2qzFvmBBaPtVD4o+MLrx94vt9C0d3k06G4FrawiZSlzcFtvm5HGGztUljgfN8u4ivfvA3hiLwb4Vs9IUK0yL5lzIo4kmb7x3bV3c4UE87VXNAHVUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRXivxL+Mn/CO3cui+G2hn1CMutzcyDclu3I2qM/M4Pr8q4wQxyFAPaqK888J6p461IeHZr2DTJNIudMW6vL0blkaRi2xVXd97b5TN8u3O/DfdFeh0AFFFFABRRRQAUUUUAFFFFABRRRQAUVwGu/FvwZokbbtXS/lCh1h04+eWGdv3l+QH2Zh/KvNPEH7Q97cQTwaHpEdlKxYR3VxL5jBOQreXtCq33W+8wHT5qAPoquA8UfFTwt4SKLc3xvrkStE9tYtHLJCyn5t4LKFx93nn24NeKpp/xX+JUardPqDafKkSs0+21tmRmLK+z5VkA+9uVWbhf9mu08P/ALPFlBPBPruryXsahTJa28XlqXGCy+ZuLMv3l+6rHr8tAHN63+0Dr16hj0fT7bTAyrmR28+UMG6qSqqOPl5Vvr6Z9v4C+JPxCuGuNXN1GEdysmsySRKrNtLLHHt3L/wFdvy47V9EaL4R0Hw1Gv8AZGk21q4Ro/OVMysrNu2tIfmYZx1bsK6GgDwjw7+z3BDLBP4l1Q3AX5ntLJdqFt3AMjfMy7Rg/Kp+bhuOfUtH8E+GtA8htM0S0gmt9wiuPK3TKGyCPMbLY+Y/xdDXS0UAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAV5d8aPFsWgeB5tOSaSO91ZGt4FEYbMfy+aWJ4UbW2+uWGO7L6jXx54912f4g/EZzp6+aksq2GnRkgbl3bV5ZVI3MzN833d2M4UUAdX8CvBj6xrv8AwklzCjafpzskIYqd9ztGPlIPCq27dx823HQ19MVzXgrw83hbwhpuitIJHtoj5jZyPMYln2/Kvy7mbbkZxiuloAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiuc8ba0PD3gfWNUSbyZYLZ/JfbnErfLHx/vstAHlXjz4g+INd8SDwx8PZ55wkZW4ubERyrLvC4ZZBny1XcAZNy4Zu23J6DwV8GNC8P2Eb65a22r6lu8zfJGxijyqqU2FirgHcQzKD8w4XFZf7OWnRReGNW1IM/nT3qwMpPG2OMMv6yt+le2UAFFFFABRRRQAUUUUAFFed6x8YfBWjfaIv7VN9cQ7T5NijS79237r/6sjnn5vXvxXmXiX9oDVL6B4fD1immgO2LmfbLKy7sqVXG1TtHOd33uOm6gD6Rrhtb+Kvg7Q4BI+u210zKzJFZMJ2Yj+H5flU/7zLXiM3hX4r+Pv3mqw6i8DTqrLfuLeKIhdu8Qtt6K33lXn5upzXWad+zjCr28mr6/JIhUGeC0gCEMV/hkYtnDdyvI7LQBQ8SftCXtw0kXhmwjtoWVl+0Xo3SjK8MqqdqlWz13bvl46isYeD/in8RJS2rm+S2eb5v7TfyIo3VOGWHjHB27lTqx/wBrHv2j+CfDWgeQ2maJaQTW+4RXHlbplDZBHmNlsfMf4uhrpaAPCtG/Zy09Id2uazcyysq/u7FVjWNv4huYNuH/AAFa9P0LwX4d8Kb20XSYbWR8hpstJLt4yvmNubb8q/LnGa6aigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKAPKPjn4lGi+BjpsUjpd6o3lLsYrtiUq0jf8AoK7c9Hrk/wBn7wq5nu/E11ZMFUCCxmkVSrbt3mMv8WRtVdw4+Z1/vY5L416/Nq3xDurPz45LXTgsEIjk3KrbVaTI3Fd247W4/wCWar/DX0D8N9Gm0H4e6Lp1xvEqQebIsieWyNIxkZCp6FSxX8O1AHX0UUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABVS5tobu3kt7iJJYJVMbxuu5XVvvKR9Kt0UAfLdtNr3wK8azLcWv27TruNgmJGjjuVU/KwI3BXXIyrbtoY/3lavWdG+NvgrVAolv59PleTy1jvYiv8AwIsu5VGSfvN2rvb2zs9RtHtb21hurd/vxTxq6Hvyp4/OvN7/AOAPgu9uBJAuoWCquBFbXIKn3PmKzfrQBuR/FTwRPp82oDxJaCGJwpVw6ynp0iYb2HzD5lXHX+6ahn+L/gS2nkhl8QRsyEqWjt5pF4/usqlW+qnFcvdfs6eHJLVlsdW1WC5IBR5/LlRfqqquf++qX/hnfw19pZv7U1YwbAApePcG+bd82zv8uPl4weu75QDRv/j94LsrgRwNqF+rLkS21sAo9j5jK36Vial+0bpMRjOl6Fe3IO4P9pkWDb/d27fMz/F6Vv2nwI8F28cazW13dsspdnmuWDOu3Gw7Noxk7uADnvt+WtrTvhR4G02Y3EHhu1dmXG25Zp1x/uyMy596APGH+Nvj7X3hsNKtbWK8ZtyLp9m0ssgCncu1i/GPm+7n5fTOah8H/FPxxzqMeptbS3OWGpT+SkTnq4iY5VVDH7q9MqvpX1VRQB4Jof7O0AjVtf1eViVIMOngKFbd8p8xwd3y9tq8n/vr1rRfCOg+Go1/sjSba1cI0fnKmZWVm3bWkPzMM46t2FdDRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFUb6+g03Trm/unKW1tG0sr7c7VVSzNx7ZoA+OPD9vN4t+IenpfRvetqOorJdBV2mRWfdKflxt+Us3y9BX2rXyB8E/+Su6F/wBvH/oiSvr+gAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigArH8SadLq/hjVtMgKrPeWU1ujN0DMhUZ/Fq2KKAPjf4UajHp3xR0K4mV2V52g+QfxSq0a/huda+yK+KvFmly+FPHuqWVqr2TWV472myU7oo926IhgxOdpVvX8a+u/DesxeIfDmnarEVAvIFlKq+/YzD5k3Y/hbcv/AaANqiiigAooooAKKKKACiiigAooooAKKKKAKlzbQ3dvJb3ESSwSqY3jddyurfeUj6VboooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooA+fP2gfCrie08TWtkxVgYL6aNVCrt2+WzfxZO5l3Hj5UX+7l/wG8d/d8GahJ/ek03bH/vPJGWH/AH0uR/eG77q17fq+nQ6vpF9pk7OsV5bvA5TqFZSpx+Br5E8X+ENX+HviFIZnbAPm2N9CGUSbTkMvPysvG5f4eOoKsQD7Morx/wCGXxet/EiQ6Prjx22sBAonLpHFcncqqFy3+sYsPlUY+UkcfKPYKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACsDxF4d03xTo0+l6rb+bC5JVlxvib+F1b+E/4kHjNb9FAHxn4v8Iav8PfEKQzO2AfNsb6EMok2nIZeflZeNy/w8dQVY+x/Dr41W2rRLpvii7t7XUQTsu2VYoJFVV+8xb5XY7j91V9OcLXpXiLw7pvinRp9L1W382FySrLjfE38Lq38J/xIPGa+XfiN8PLrwJrDovnXGkShWtrpojtG7d+7dsbfMG1jheow2B0AB9g0V8u+AvjXe+F9Li0jVrZ9Ss4iqwSibbLbxd1GR8+ONqkr6bsbdv0JoPijRPEtmbnR9ShvFUfOq8PGecblb5lyQcbhQBvUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAVRvbOz1G0e1vbWG6t3+/FPGroe/Knj86vUUAeAeMf2fv9deeE7n+832C7P+8dscn/AHyqq31Zq8k/4qb4fa//AMv2kanH/wAA8xVb8Vkj3L/tK22vtqsXXvD2meJNMaw1ezW7t2IcKWZSrDoVZTlT9Pf3oA8R8O/tCTxRQQeJdLFwV+V7uyba7Lt4Jjb5WbcMn5lHzcLxz7VoPijRPEtmbnR9ShvFUfOq8PGecblb5lyQcbhXiviL9nueKKefw1qguCvzJaXq7XZdvIEi/KzbhgfKo+bluOfKr3TPEfgvUo5bm21DSb1GdYp/miyVG1vLcfe+9jcpx83vQB9u0V8t+HPjr4m0a3hg1WODVraM7d0xMc5XZtUeYufZtzKzNlvm9PWfC3xl8M+IpGhubhdIuY4VkkW/lRI26ZVJN2G25/i2kg52/e2gHpdFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFUb2zs9RtHtb21hurd/vxTxq6Hvyp4/Or1FAHj2v8AwB8Oag0b6NczaRIMK6HdcRsvzc7Wbdu+7zu24X7teSaz8HvGujG4kGlfb7eHaPOsZFl8wnb91M7+/Py+vbmvryigD4m0Hxp4i8K7xourT2scmS0OFeMt8vzeWwK7vlX5sbsDrXqWj/tG3a7U1rQoZd0oDS2UjJtj9kbdubqfvL2r2bWvCOg+JY2/tfSba6cosfnMmJVVW3bVkHzKM56N3NeS65+ztAY2bQNXlUhQBDqADBm3fMfMQDb8vba3I/75APQ9O+K/gbUpjbweJLVGVc7rlWgXH+9Iqrn2ruK+P9d+EnjPRJG3aQ99EGCLNp374McbvuqN49Mso/lWHo/iDxB4P1KY6XeXWm3aMY7iEjHzLuXbJG3ysVy33l+U+hoA+3KK+bdO/aO1yOYtqejadcQ7cBbYyQsG/wB5i4x+FdvpHx98JX5Cagt5pjeXudpIjIm7gFVZNzN3+bavSgD1uisaw8R6Fq85t9L1nT72dV3mO2ukkZR6kK2cc1s0AFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAVk6poOla00R1XSrO/wDJB8v7VbrMI92N2Nw4ztGcegrWooA8pv8A4A+C724EkC6hYKq4EVtcgqfc+YrN+tcPqX7OWrRCMaXrtlcltxk+1RNBt/u7du/Pf0r6OooA+NtX+GHjDR797SXQ7y6C/dlsYmnR13FchlB67futtbkcCodP+JXjTTLgzQeKNSkYjaRczeev/fMm5fxxX2fXM674L8O+K9ja1pMN1ImAs2Wjl284XzF2tt+ZvlzjNAHi+m/tG6tEJDqmhWVyW2iP7LK0G3+9u3b89vSvQ9G+NvgrVAolv59PleTy1jvYiv8AwIsu5VGSfvN2rH1j9n/w3ei5fS7q90yZyojQN50SDjJ2t8zZGf4+p9Plrz7V/gH4usAz6c1lqiebtRY5RG4XkhmWTCjt8oZuv40AfSWm6tpurwNcadf2t7EreW0ltMsihvTcv1rSr4ifS/FPhVo9UlsNW0dg3lx3LwyW5DMrcKxxyV3dPeum0P40eMtCVYTfx6jCiMqx38e85Zs7t42ux/3m/pgA+t6K8H0f9o2zbamtaFPFtiG6WykEm6TvhG27V6n7zdq9G8N/EXwv4smSDTNTT7UyK32SZTHLnBYqoP3yoU527gPXFAHZUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAVx+tfDfwfr05uNR0C2aTLO0kW6BnZuWLMjKW/wCBV2FFAHg+vfs62z2iv4f1aaO5BOY9QKtHJyuPmRVK4G7+Fs8fd5rzDxH8MPFXhdpGutNkubVVMn2uyDSxBVXczN8u5QueSwX7rYz1r7HooA+ONE+K3jLQ596a5c3SMys8V63nqwH8PzfMo/3WWvR/D/7Rh/eJ4k0f1ZJtNH0AUxyN/vfNu9Bt716trvgXwx4mLPq2i2s87srPOFMcrFVKrmRdrHjHBPQD0FeXa5+ztAY2bQNXlUhQBDqADBm3fMfMQDb8vba3I/75APTPDXxA8N+LBB/ZuqQfaJelpKwjnBC7mXyzy20fxLuX5WweDXX18VeJ/A2u+D5tmr6fJFGSVjuo/mhkPzBdrdi20na2Gx/DXXeGfjl4p0aRU1Fk1i0CKuycbJVCq33XUcnpuZgxO33zQB9T0V5lbfHTwLPbpLJc3ULsOY5rb519jjI/I0UAem0UUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAV8AUUUAFFFFAH/2Q==" />
						
						
						</br>
						<span style="font-size: 15px; text-align: center;">
                         	Subhash Chandra Dev, Director</br>    
                            <span style="font-size:12px">Signature of issuing center or its Agent</span>
						</span>
						</td>

						 </tr>
                        </tbody>
                    </table>
					</div>

					<div class="col-xs-6" style="margin-left: 5px;">
						<table class="table commercial-table" style="margin-top: -10px;">

							<tbody>
								<tr style="height: 100px; background: #fff;">
									<td style="border-top: 1px solid #337ab7; font-size:12px;">
										Shipper certifies that the particulars on the face hereof are correct and that in so far as part of the consignment contains
										restricted articles, such part is properly described by name and is in proper condition for carriage by air according to the
										applicable Dangerous Goods Regulaion. 
									</td>
								</tr>
								<tr style="height: 76px; background: #fff;">
									<td style="border-top: 1px solid #337ab7;">
										${airBillOfLandingJsonData.itShipper[0].name1},${airBillOfLandingJsonData.itShipper[0].name2}</br>
										${airBillOfLandingJsonData.itShipper[0].name3},${airBillOfLandingJsonData.itShipper[0].name4}</br>
										${airBillOfLandingJsonData.itShipper[0].city1}, ${airBillOfLandingJsonData.itShipperCountry[0].landX}</br>
<%-- 										Executed On:
										<fmt:formatDate value="${airBillOfLandingJsonData.itHeader[0].zzHblHawBdt}" pattern="yyyy-MM-dd"/> --%>
									</td>
								</tr>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</section>
<%@include file="footer.jsp"%>

<script>
	
function printContent(el){
var restorepage = $('body').html();
var printcontent = $('#' + el).clone();
$('body').empty().html(printcontent);
window.print();
$('body').html(restorepage);
}

	</script>