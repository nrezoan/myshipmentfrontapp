<jsp:include page="header_v2.jsp"></jsp:include>
<jsp:include page="navbar.jsp"></jsp:include>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/plugins/datatables/dataTables.bootstrap.css">
<script src="${pageContext.request.contextPath}/resources/plugins/datatables/dataTables.bootstrap.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/plugins/datatables/jquery.dataTables.min.js"></script> --%>
<style>
html {
	background-color: #fff;
}

html, body {
	/* height: 100%; */
	margin-bottom: 25px !important;
}

.container-fluid-default-476378 {
	padding-right: 15px;
	padding-left: 15px;
	margin-right: auto;
	margin-left: auto;
}
</style>
<script>
	$(document).ready(function() {
	    $('#shipment-detail-table').DataTable();	    
	} );
</script>

<section class="main-body">

	<div class="container-fluid-default-476378">
		<div class="row">
			<div class="col-sm-12">
				<h3>Shipment Detail</h3>
			</div>
		</div>
		<!--/row-->
		<hr>
		<div class="row">


			<div class="col-md-12">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>B/L No.</th>
							<th>B/L Date</th>
							<th>Booking Date</th>
							<th>Buyer</th>
							<th>POL</th>
							<th>POD</th>
							<th>Gross Wt.(Kgs)</th>
							<th>Chargeable Wt.(Kgs)</th>
							<th>Total Volume</th>
							<th>GR Date</th>
							<th>Shipment Date</th>


						</tr>
					</thead>
					<tbody>
						<c:forEach items="${shipdetails.lstSOWiseShipDetailsBuyerJson }"
							var="sodetails">
							<tr>
								<td><a
									href="getTrackingInfoFrmUrl?searchString=${sodetails.blNo }">${sodetails.blNo }</a></td>
								<td>${sodetails.bldate }</td>
								<td>${sodetails.bookingDate }</td>
								<td>${sodetails.buyerName }</td>
								<td>${sodetails.polCode }</td>
								<td>${sodetails.podCode }</td>
								<td>${sodetails.grossWeight }</td>
								<td>${sodetails.chargeWeight }</td>
								<td>${sodetails.totVolume }</td>
								<td>${sodetails.grDate }</td>
								<td>${sodetails.shipmentDate }</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>

<jsp:include page="footer_v2.jsp"></jsp:include>