<%-- <%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style>
	html {
		background-color: #fff;
	}
</style>
<script type="text/javascript">
	
</script>

<div class="container-fluid container-fluid-441222">
	<div class="row row-without-margin">
		<h1 class="h1-report">Freight Invoice</h1>
		<ol class="breadcrumb">
			<li><i class="fa fa-bar-chart"></i>&nbsp;Report Management</li>
			<li class="active">Freight Invoice</li>
		</ol>
	</div>
	<hr>
	<div class="row row-without-margin">
		<form:form  action="getSeaExportInvoiceList" method="post" commandName="req" cssClass="form-incline">
			<div class="col-md-3 col-sm-12 col-xs-12">
				<div class='form-group'>
					<label class="text-center">HBL No.</label>
					<form:input path="hblNumber" id="hblNumber" cssClass="form-control" />
				</div>
			</div> 
			<div class="col-md-2 col-sm-6 col-xs-6">	
				<label style="display: block; color: #f5f5f5">. </label>
				<!-- added for alignment purpose, dont delete -->
				<input type="submit" id="btn-appr-po-search reportSearchBtn" onclick="return reportSearch();"
					class="btn btn-primary form-control" value="Submit" />
			</div>
		</form:form>
	</div>
</div>



<script>
	function reportSearch() {
		var number = $("#hblNumber").val();
		if(number == "") {
			swal ( "HBL Number" ,  "Please enter HBL Number!" ,  "error" );
			return false;
		} else {
			return true;
		}
	}
</script>

<!-- ----------------------------- -->
<%@include file="footer_v2_fixed.jsp"%> --%>

<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style>
html {
	background-color: #fff;
}
</style>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/invoice.js"></script>

<div class="container-fluid container-fluid-441222">
	<div class="row row-without-margin">
		<h1 class="h1-report">Freight Invoice</h1>
		<ol class="breadcrumb">
			<li><i class="fa fa-bar-chart"></i>&nbsp;Report Management</li>
			<li class="active">Freight Invoice</li>
		</ol>
	</div>
	<hr>
	<div class="row row-without-margin">
		<form:form action="getSeaExportInvoiceList" method="post"
			commandName="req" cssClass="form-incline">
			<div class='col-md-2 col-sm-6 col-xs-6'>
				<div class='form-group'>
					<label><b>BL Type:</b></label>
					<form:select class="form-control" path="blType" id="blType">
						<option value="1">HBL</option>
						<option value="2">MBL</option>
					</form:select>
				</div>
			</div> 
			<div class="col-md-2 col-sm-12 col-xs-12">
				<div class='form-group'>
					<label class="text-center">BL Value</label>
					<form:input path="hblNumber" id="hblNumber" cssClass="form-control" onchange="return disableFields()"/>
				</div>
			</div>

			<div class='col-md-2 col-sm-6 col-xs-6'>
				<div class='form-group'>
					<label>From Date</label>
					<form:input path="fromDate" id="fromDate"
						class="date-picker form-control glyphicon glyphicon-calendar" onkeydown="return false"/>
				</div>
			</div>
			<div class='col-md-2 col-sm-6 col-xs-6'>
				<div class='form-group'>
					<label>TO Date</label>
					<form:input path="toDate" id="toDate"
						class="date-picker form-control glyphicon glyphicon-calendar" onkeydown="return false"/>
				</div>
			</div>
			<div class='col-md-2 col-sm-6 col-xs-6'>
				<div class='form-group'>
					<label><b>Status Type:</b></label>
					<form:select class="form-control" path="billStatus" id="billStatus">
						<option value="1">Open</option>
						<option value="2">Cleared</option>
						<option value="3">All</option>
					</form:select>
				</div>
			</div>
			<div class="col-md-2 col-sm-6 col-xs-6">
				<label style="display: block; color: #f5f5f5">. </label>
				<!-- added for alignment purpose, dont delete -->
				<input type="submit" id="btn-appr-po-search reportSearchBtn"
					onclick="return mandatoryValidation();"
					class="btn btn-primary form-control" value="Submit" />
			</div>

		</form:form>
	</div>
</div>



<!-- <script>
	function reportSearch() {
		var number = $("#hblNumber").val();
		if (number == "") {
			swal("HBL Number", "Please enter HBL Number!", "error");
			return false;
		} else {
			return true;
		}
	}
</script> -->
<script>
$(document).ready(function() {
	$("#toDate").datepicker({
		autoclose : true,
		dateFormat : "dd-mm-yy"
	});
	$("#fromDate").datepicker({
		autoclose : true,
		dateFormat : "dd-mm-yy"
	});
	$('.date-picker').each(function() {
		$(this).removeClass('hasDatepicker').datepicker({
			dateFormat : "dd.mm.yy"
		});
	});
});
</script>

<!-- ----------------------------- -->
<%@include file="footer_v2_fixed.jsp"%>
