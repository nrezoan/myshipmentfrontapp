<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
<style>
html, body {
	background-color: #fff;
}
}
</style>
<script type="text/javascript">
	
</script>

<div class="container-fluid container-fluid-441222">
	<div class="row row-without-margin">
		<h1 class="h1-report">MBL/MAWB Tracking</h1>
		<ol class="breadcrumb">
			<li><i class="fa fa-map-marker"></i>&nbsp;Tracking</li>
			<li class="active">MBL wise Tracking</li>
		</ol>
	</div>
	<hr>
	<form:form action="mblWiseTracking" method="POST"
		commandName="mblTrackingParams" id="mblTrack">
		<div class='row row-without-margin'>
			<div class="col-md-2 col-sm-12 col-xs-12">
				<div class='form-group'>
					<label>MBL/MAWB No.</label>
					<form:input path="doc_no" id="doc_no" cssClass="form-control" />
				</div>
			</div>
			<div class="col-md-2 col-sm-12 col-xs-12">
				<label style="display: block; color: transparent">. </label>
				<!-- added for alignment -->
				<input type="submit" id="btn-appr-mbl-search"
					class="btn btn-success form-control" value="Submit"
					onclick="return reportSearch();" />
			</div>
		</div>
	</form:form>
	

	<div>
		<%-- <form:form action="trackingResLst" method="POST"
			commandName="mblTrackingParams" id="mblTrackinfo"> --%>

			<c:if test="${trackingResLst.size()== 0 }">
				<fieldset>
					<legend>Your Search Yielded No Results</legend>
				</fieldset>
				<span style="color: red">- Check if entered MBL is correct.</span><br>
				<span style="color: red">- Check if selected company/sales organization is correct.</span><br>
				<span style="color: red">- Check if selected shipment mode(SEA or AIR) is correct.</span>

			</c:if>


			<c:if test="${ not empty trackingResLst }">
				<fieldset>
					<legend>Searched Information</legend>
					<div class="table-responsive">						
							<table class="table table-hover" id="tbl-mbl-track">
								<thead style="background-color: #252525; color: #fff">
									<tr>
										<th>HBL NO</th>
										<th>Buyer</th>
										<th>POL</th>
										<th>POD</th>
										<th>Carrier</th>
										<th>Departure Date</th>
										<th>ETA</th>
										<th>Status</th>

									</tr>
								</thead>
								<tbody>
									<tr>
										<c:forEach items="${trackingResLst}" var="i">
											<tr>
												<td><a href='getTrackingInfoFrmUrl?searchString=${i.bl_no}' target="_blank">${i.bl_no } </a></td>
												<td>${i.buyer}</td>
												<td>${i.pol}</td>
												<td>${i.pod}</td>
												<td>${i.carrier}</td>
												<td><fmt:formatDate pattern="dd-MMM-yyyy" type="date" value="${i.dep_dt}" /></td>
												<td><fmt:formatDate pattern="dd-MMM-yyyy" type="date" value="${i.eta_dt}" /></td>
												<td>${i.status}</td>
											</tr>
										</c:forEach>
									</tr>

								</tbody>
							</table>
						
					</div>

				</fieldset>

			</c:if>
		<%-- </form:form> --%>
	</div>

	<div class="modal fade" id="mblTrackingInfoModal" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Modal Header</h4>
				</div>
				<div class="modal-body">
					<p>Some text in the modal.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				</div>
			</div>

		</div>
	</div>

</div>
<script>
	$(function() {
		$('#tbl-mbl-track').DataTable();
	});
	$(document).ready(function() {
		function open(x) {
			$(x).modal('show');
		}
	})
</script>
<script>
	function reportSearch() {
		var number = $("#doc_no").val();
		if (number == "") {
			swal("MBL/MAWB Number", "MBL/MAWB No. Cannot be Empty!", "error");
			return false;
		} else {
			return true;
		}
	}
</script>

<script
	src="${pageContext.request.contextPath}/resources/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
	
<%@include file="footer_v2_fixed.jsp"%>