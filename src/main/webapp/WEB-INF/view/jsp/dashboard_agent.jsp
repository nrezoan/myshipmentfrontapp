<!-- header -->
<%@include file="header_v2.jsp" %>
<!-- navbar -->
<%@include file="navbar.jsp" %>

<script src="${pageContext.request.contextPath}/resources/ammap/ammap/ammap.js"></script>
<script src="${pageContext.request.contextPath}/resources/ammap/ammap/maps/js/worldLow.js"></script>
<script src="${pageContext.request.contextPath}/resources/ammap/ammap/themes/light.js"></script>
<script src="${pageContext.request.contextPath}/resources/ammap/ammap/plugins/export/export.min.js"></script>

	<script type="text/javascript">
	      
      	var loginDTO = ${loginDto};
      
      	/* var sowiseShipmentSummary = ${suppWiseShipSummaryJson};
		var lastNShipmentsJsonOutputData = ${lastNShipmentsJsonOutputData}; */
		/* var topFiveShipmentListJSON = ${topFiveShipmentListJSON}; */
		
		var topFiveShipmentListJSON = ${topFiveShipmentListJSON};
		var dashboardInfoBox = ${dashboardInfoBox};
		
        
      </script>

<link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/dashboardMap.css" type="text/css" media="all" />
	
	<script src="${pageContext.request.contextPath}/resources/js/dashboardMap.js"></script>

<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!-- Theme style -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/AdminLTE.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/dist/css/skins/_all-skins.min.css">

	
		<div class="entire-content-c">
			<!-- <br><br><br> -->
			<input id="dashboard_from_date" type="hidden" class="date-picker glyphicon glyphicon-calendar dashboard-ship-frm-date" style="width: 160px;"/>
			<input id="dashboard_to_date" type="hidden" class="date-picker glyphicon glyphicon-calendar dashboard-ship-to-date" style="width: 160px;"/>
			
	        <div class="content" style="background-color: #eaeaea; display: block;">
	         <!-- Content Header (Page header) -->
	         <section class="content-header" style="display: none;">
	            <h1>
	               Dashboard
	            </h1>
	            <ol class="breadcrumb" style="position: unset; float: none;">
	               <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
	               <li class="active">Dashboard</li>
	            </ol>
	         </section>
	         <!-- Main content -->
	         <section class="content">
	            <!-- Info boxes -->
	            <div class="row dashboard-box-menu-row-gap">
	               <div class="col-md-2 col-sm-6 col-xs-12">
	                  <div class="info-box wow animated zoomIn" data-wow-delay="0.3s" id="total-bookings">
	                     <span class="info-box-icon total-bookings-box"><i class="fa fa-envelope"></i></span>
	                     <div class="info-box-content">
	                        <span class="info-box-text" id="totalBookings-title"></span>
	                        <span class="dashboard-info-box-number" id="totalBooking-number"></span>
	                        <span class="progress-description" id="totalBooking-dateRange"></span>
	                     </div>
	                     <!-- /.info-box-content -->
	                  </div>
	                  <!-- /.info-box -->
	               </div>
	               <!-- /.col -->
	               <div class="col-md-2 col-sm-6 col-xs-12">
	                  <div class="info-box wow animated zoomIn" data-wow-delay="0.3s" id="open-bookings">
	                     <span class="info-box-icon open-bookings-box"><i class="fa fa-envelope-open"></i></span>
	                     <div class="info-box-content">
	                        <span class="info-box-text" id="openBookings-title"></span>
	                        <span class="dashboard-info-box-number" id="openBookings-number"></span>
	                        <span class="progress-description" id="openBookings-dateRange"></span>
	                     </div>
	                     <!-- /.info-box-content -->
	                  </div>
	                  <!-- /.info-box -->
	               </div>
	               <!-- /.col -->
	               <!-- fix for small devices only -->
	               <div class="clearfix visible-sm-block"></div>
	               <div class="col-md-2 col-sm-6 col-xs-12">
			          <div class="info-box wow animated zoomIn" data-wow-delay="0.3s" id="goods-hand-over">
			            <span class="info-box-icon goods-handover-box"><i class="fa fa-handshake-o"></i></span>
			
			            <div class="info-box-content">
			              <span class="info-box-text" id="goodsHandOver-title"></span>
			              <!-- <marquee behavior="alternate" scrolldelay="500" onmouseover="this.stop()" onmouseout="this.start()">
						  	
						  </marquee> -->
			              
			              <span class="dashboard-info-box-number" id="goodsHandOver-number"></span>
				          <span class="progress-description" id="goodsHandOver-dateRange"></span>
			            </div>
			          </div>
			        </div>
	               <!-- /.col -->
	               <div class="col-md-2 col-sm-6 col-xs-12">
			          <div class="info-box wow animated zoomIn" data-wow-delay="0.3s" id="stuffing-done">
			            <span class="info-box-icon stuffing-done-box"><i class="fa fa-archive"></i></span>
			
			            <div class="info-box-content">
			              <span class="info-box-text" id="stuffingDone-title"></span>
			              <span class="dashboard-info-box-number" id="stuffingDone-number"></span>
				          <span class="progress-description" id="stuffingDone-dateRange"></span>
			            </div>
			          </div>
			        </div>
			        <div class="col-md-2 col-sm-6 col-xs-12">
	                  <div class="info-box wow animated zoomIn" data-wow-delay="0.3s" id="in-transit">
	                     <span class="info-box-icon in-transit-box"><i id="inTransit-image" class=""></i></span>
	                     <div class="info-box-content">
	                        <span class="info-box-text" id="inTransit-title"></span>
	                        <span class="dashboard-info-box-number" id="inTransit-number"></span>
	                        <span class="progress-description" id="inTransit-dateRange"></span>
	                     </div>
	                     <!-- /.info-box-content -->
	                  </div>
	                  <!-- /.info-box -->
	               </div>
			        
			        <div class="col-md-2 col-sm-6 col-xs-12">
			          <div class="info-box wow animated zoomIn" data-wow-delay="0.3s" id="arrived">
			            <span class="info-box-icon arrived-box"><i class="fa fa-check"></i></span>
			
			            <div class="info-box-content">
			            
			              <span class="info-box-text" id="arrived-title"></span>
			              <span class="dashboard-info-box-number" id="arrived-number"></span>
				          <span class="progress-description" id="arrived-dateRange"></span>
			            </div>
			          </div>
			        </div>
			        
			        <!-- /.col -->
	            </div>
	            <!-- /.row -->
	            <!-- Main row -->
	            <div class="row">
	               <!-- Left col -->
	               <div class="col-md-8">
	                  <!-- MAP & BOX PANE -->
	                  <div class="box box-myshipment" style="display: block;">
	                     <div class="box-header with-border">
	                        <h3 class="box-title">Top Origin</h3>
	                        
	                     </div>
	                     <!-- /.box-header -->
	                     <div class="box-body no-padding" style="display: block;">
	                     <p class="text-center not-found" id="mapNone"></p>
	                        <div class="row" id="mapBody">
	                           <div class="col-md-12 col-sm-12">
	                              <div class="pad">
	                                 <!-- Map will be created here -->
	                                 <!-- <canvas id="salesChart" style="height: 0px; visibility: hidden;"></canvas>
	                                 <div id="world-map-markers" style="height: 325px;"></div> -->
	                                 <div id="chartdiv"></div>
	                              </div>
	                           </div>
	                        </div>
	                        <!-- /.row -->
	                     </div>
	                     <!-- /.box-body -->
	                  </div>
	                  <!-- /.box -->
	                  <!-- TABLE: LATEST ORDERS -->
	                  <div class="box box-myshipment">
	                     <div class="box-header with-border">
	                        <h3 class="box-title">Recent Shipments</h3>
	                        
	                     </div>
	                     <!-- /.box-header -->
	                     <div class="box-body">
	                        <div class="table-responsive">
	                           <table class="table no-margin" id="tbl-recent-shipment">
	                              <thead>
	                                 <tr>
	                                    <th class="text-center">HBL NO</th>
	                                    <th class="text-center">Supplier Name</th>
	                                    <th class="text-center">Booking Date</th>
	                                    <th class="text-center">POL</th>
	                                    <th class="text-center">POD</th>
	                                    <th class="text-center">ETD</th>
	                                    <th class="text-center">ETA</th>
	                                    <th class="text-center">Status</th>
	                                 </tr>
	                              </thead>
	                              <tbody>
	                                 <!-- table row generated from javascript -->
	                                 <c:choose>
	                                 	<c:when test="${fn:length(recentShipmentList) gt 0}">
	                                 		<c:forEach items="${recentShipmentList}" var="recentShipment" begin="0" end="4">
			                                 	<tr>
			                                 		<td class="text-center"><a href='getTrackingInfoFrmUrl?searchString=${recentShipment.hblNo }'>${recentShipment.hblNo }</a></td>
			                                 		<td class="text-center">${recentShipment.name }</td>
			                                 		<td class="text-center"><fmt:formatDate pattern="dd-MMM-yyyy" type="date" value="${recentShipment.bookingDate }" /></td>
			                                 		<td class="text-center">${recentShipment.pol }</td>
			                                 		<td class="text-center">${recentShipment.pod }</td>
			                                 		<td class="text-center"><fmt:formatDate pattern="dd-MMM-yyyy" type="date" value="${recentShipment.etd }" /></td>
			                                 		<td class="text-center"><fmt:formatDate pattern="dd-MMM-yyyy" type="date" value="${recentShipment.eta }" /></td>
			                                 		<td class="text-center">
			                                 			<c:choose>
													        <c:when test="${recentShipment.status == 'Order Booked'}">
																<span class="badge" style="background-color: #f44336;">${recentShipment.status}</span>
															</c:when>
													        <c:when test="${recentShipment.status == 'GR Done'}">
													        	<span class="badge" style="background-color: #3a88a7;">${recentShipment.status}</span>
													        </c:when>
													        <c:when test="${recentShipment.status == 'Stuffing Done'}">
													        	<span class="badge" style="background-color: #0c99d5;">${recentShipment.status}</span>
													        </c:when>
													        <c:when test="${recentShipment.status == 'Shipment Done'}">
													        	<span class="badge" style="background-color: #0c99d5;">${recentShipment.status}</span>
													        </c:when>
													        <c:when test="${recentShipment.status == 'In Transit'}">
													        	<span class="badge status-badge-orange" style="/* background-color: #00a65a; */">${recentShipment.status}</span>
													        </c:when>
													        <c:when test="${recentShipment.status == 'Delivered'}">
													        	<span class="badge" style="background-color: #00a65a;">${recentShipment.status}</span>
													        </c:when>
													        <c:otherwise>
													        	<span class="badge">${recentShipment.status}</span>
													        </c:otherwise>
													    </c:choose>
			                                 		</td>
			                                 	</tr>
			                                 </c:forEach>
	                                 	</c:when>
	                                 	<c:otherwise>
											<td class="text-center" colspan="8"><a class="not-found">No Shipment Found</a></td>
										</c:otherwise>
	                                 </c:choose>
	                                 
	                              </tbody>
	                           </table>
	                        </div>
	                     </div>
	                     
	                  </div>
	               </div>
	               <div class="col-md-4" style="display: block;">
					<div class="box box-myshipment">
						<div class="box-body no-padding">
							<table class="table table-striped" id="tbl-pre-alert">
								<thead>
									<tr>
										<th class="text-center">Pending MBL/MAWB</th>
									</tr>
								</thead>
								<tbody>
									<!-- Topshipment table data generated from javascript -->
								</tbody>
								<c:choose>
									<c:when test="${fn:length(mblWiseHbl) gt 0}">
										<c:if test="${not empty mblWiseHbl}">
											<tr>
												<th class="text-center"
													style="font-size: xx-large; color: #dd4b39 !important;"><a href="preAlertBLtoDownload" id="pendingMBLDownload" style="color: #dd4b39 !important;">${fn:length(mblWiseHbl)}</a></th>
												
											</tr>
										</c:if>
									</c:when>
									<c:otherwise>
										<td class="text-center" colspan="1"><a class="not-found">No
												Pre-Alerts Found</a></td>
									</c:otherwise>
								</c:choose>



							</table>
						</div>
					</div>
					<div class="box box-myshipment">
	                     <div class="box-header" style="display: none;">
	                        <h3 class="box-title">Shipment Details</h3>
	                     </div>
	                     <!-- /.box-header -->
	                     <div class="box-body no-padding">
	                        <table class="table table-striped" id="tbl-totalGWTCBM">
	                           <thead>
		                           <tr>
		                              <th id="tableHeaderGWT" class="text-center"></th>
		                              <th id="tableHeaderCBM" class="text-center"></th>
		                           </tr>
	                           </thead>
	                           <tbody>
	                           		<!-- Topshipment table data generated from javascript -->
	                           		<!-- <tr>
	                           			<td class="text-center" id="no-data" colspan="6" style="display: none;"><a class="not-found">No Shipment Found</a></td>
	                           			<td class="text-center" id="gwt-data"><a href='dashboardInfoDetails?details=totalGWT' id="tableDataGWT"></a></td>
	                           			<td class="text-center" id="cbm-data"><a href='dashboardInfoDetails?details=totalCBM' id="tableDataCBM"></a></td>
	                           		</tr> -->
	                           </tbody>
	                           
	                           
	                           
	                        </table>
	                     </div>
	                     <!-- /.box-body -->
	                  </div>
	                  <div class="box box-myshipment">
	                     <div class="box-header">
	                        <h3 class="box-title">Origin wise Shipment</h3>
	                     </div>
	                     <div class="box-body no-padding">
	                        <table class="table table-striped" id="tbl-top-shipment">
	                           <thead>
		                           <tr>
		                              <th>POD</th>
		                              <th>Total Shipment</th>
		                              <th>Total CBM</th>
		                              <th>Total GWT</th>
		                           </tr>
	                           </thead>
	                           <tbody>
	                           		<!-- Topshipment table data generated from javascript -->
	                           </tbody>
	                           
	                           <%-- <c:if test="${not empty topFive}">
	                           		<c:forEach items="${topFive.topFiveShipmentJsonData}" var="shipment" varStatus="index">
										<tr>
											<td>Port:${shipment.portOfDischarge}</td>
			                                <td>${shipment.totalShipment}</td>
			                                <td>${shipment.totalCBM}
				                                 <!-- <div class="progress progress-xs">
				                                    <div class="progress-bar progress-bar-danger" style="width: 55%"></div>
				                                 </div> -->
			                                </td>
			                                <td>${shipment.totalGWT}<!-- <span class="badge bg-red">55%</span> --></td>
			                             </tr>
									</c:forEach>
	                           </c:if> --%>
	                           
	                           <c:choose>
	                           		<c:when test="${fn:length(topFiveShipmentList) gt 0}">
	                           			<c:if test="${not empty topFiveShipmentList}">
			                           		<c:forEach items="${topFiveShipmentList}" var="shipment" varStatus="index" begin="0" end="4">
												<tr>
													<td><a href='topShipment?port=${shipment.port}'>${shipment.port}</a></td>
					                                <td>${shipment.totalShipment}</td>
					                                <td>${shipment.totalCBM}
						                                 <!-- <div class="progress progress-xs">
						                                    <div class="progress-bar progress-bar-danger" style="width: 55%"></div>
						                                 </div> -->
					                                </td>
					                                <td>${shipment.totalGWT}<!-- <span class="badge bg-red">55%</span> --></td>
					                             </tr>
											</c:forEach>
			                           </c:if>
	                           		</c:when>
	                           		<c:otherwise>
										<td class="text-center" colspan="4"><a class="not-found">No Shipment Found</a></td>
									</c:otherwise>
	                           </c:choose>
	                           
	                        </table>
	                     </div>
	                     <!-- /.box-body -->
	                  </div>
	                  <!-- /.box -->
	                  <!-- /.info-box -->
	                  <div class="box box-myshipment" style="display: none;">
	                     <div class="box-header with-border">
	                        <h3 class="box-title">Browser Usage</h3>
	                        
	                     </div>
	                     <!-- /.box-header -->
	                     <div class="box-body">
	                        <div class="row">
	                           <div class="col-md-8">
	                              <div class="chart-responsive">
	                                 <canvas id="pieChart" height="150"></canvas>
	                              </div>
	                              <!-- ./chart-responsive -->
	                           </div>
	                           <!-- /.col -->
	                           <div class="col-md-4">
	                              <ul class="chart-legend clearfix">
	                                 <li><i class="fa fa-circle-o text-light-blue"></i> Pending</li>
	                                 <li><i class="fa fa-circle-o text-red"></i> Released</li>
	                              </ul>
	                           </div>
	                           <!-- /.col -->
	                        </div>
	                        <!-- /.row -->
	                     </div>
	                     <!-- /.box-body -->
	                     <div class="box-footer no-padding">
	                        <ul class="nav nav-pills nav-stacked">
	                           
	                        </ul>
	                     </div>
	                     <!-- /.footer -->
	                  </div>
	                  <div class="box box-myshipment">
	                     <div class="box-header with-border">
	                        <h3 class="box-title">Shipment Status</h3>
	                        
	                     </div>
	                     <div class="box-body">
	                     	<p class="text-center not-found" id="shipmentStatusNone"></p>
	                        <div class="row" id="shipmentStatus">
	                           <div class="col-md-12">
	                              <div class="chart-responsive" style="display: none;">
	                                 <canvas id="pieChart" height="150"></canvas>
	                              </div>
	                              <div id="chartContainer" style="width: 100%; height: 250px"></div>
	                              <!-- ./chart-responsive -->
	                           </div>
	                           <!-- <div class="col-md-4">
	                              <ul class="chart-legend clearfix">
	                                 <li><i class="fa fa-circle-o text-light-blue"></i> Pending</li>
	                                 <li><i class="fa fa-circle-o text-red"></i> Released</li>
	                              </ul>
	                           </div> -->
	                        </div>
	                     </div>
	                     <div class="box-footer no-padding">
	                        <ul class="nav nav-pills nav-stacked">
	                           
	                        </ul>
	                     </div>
	                  </div>
	                  <!-- /.box -->
	                  <!-- /.box -->
	               </div>
	               <!-- /.col -->
	            </div>
	            <!-- /.row -->
	         </section>
	         <!-- /.content -->
	      </div>
      </div>
      
      
      <script src="${pageContext.request.contextPath}/resources/js/dashboard_agent.js" type="text/javascript"></script>

<!-- footer -->
<%@include file="footer_v2.jsp" %>