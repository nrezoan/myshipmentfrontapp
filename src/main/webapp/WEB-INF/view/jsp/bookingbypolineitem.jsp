<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- <script
	src="${pageContext.request.contextPath}/resources/js/directBooking.js"></script> --%>
<style>

html {
	background-color: #fff;
	/* margin-bottom: 30px; */
}
.tab-content {
    border: none;
</style>
<script type="text/javascript">
	var arrayObject = [];
	$(document).ready(function() {
		var appId = $("#app-id").val();
		$.ajax({
			url : myContextPath + '/orderdetails',
			data : {
				appId : appId
			},
			type : "GET",
			dataType : "json",
			success : function(data) {
				$.LoadingOverlay("hide");
				console.log(data);
				arrayObject = data;
				renderTable(data);
			},
			error : function() {
			}

		})
		$("#btn-submit").click(function() {
			alert("submmiting booking");
			$.LoadingOverlay("show");
			var poObject = {};
			poObject["lstAppPurchaseOrder"] = arrayObject;
			alert(poObject);
			$.ajax({
				url : myContextPath + '/saveorderforpo',
				data : JSON.stringify(poObject),
				type : "POST",
				contentType : 'application/json',
				success : function(data) {
					$.LoadingOverlay("hide");
					window.location = myContextPath + '/bookingresponse';

				},
				error : function(jqXHR, textStatus, errorThrown) {
					$.LoadingOverlay("hide")
				}
			})

		})

	})

	function renderTable(data) {
		var tr = '<tr>';
		var itemID = 1;
		$
				.each(
						data,
						function(index, value) {

							var tr = '<tr>';
							tr += '<td>' + itemID + '</td>';
							tr += '<td>' + value.vc_po_no + '</td>';
							tr += '<td>' + value.vc_color + '</td>';
							tr += '<td>' + value.vc_product_no + '</td>';
							tr += '<td>' + value.vc_hs_code + '</td>';
							tr += '<td>' + value.nu_no_pcs_ctns + '</td>';
							tr += '<td>' + value.vc_tot_pcs + '</td>';
							tr += '<td>' + value.vc_nt_wt + '</td>';
							tr += '<td>' + value.vc_gr_wt + '</td>';
							tr += '<td>' + value.vc_cbm_sea + '</td>';
							tr += "<td>";
							tr += '<input type="submit" id="'
									+ itemID
									+ '" class="btn btn-danger btn_btn remove_btn" value="Delete" onClick="handleDelete(this)"/>';
							tr += "</td>";
							tr += '</tr>'
							$("#booking_table").append(tr);
							itemID += 1;
						})

	}

	function handleDelete(element) {
		var td = $(element).parent();
		var tr = $(td).parent();
		$(tr).remove();
		arrayObject.splice(element.id - 1, 1);
		if ($("#update-item") != undefined)
			$("#update-item").attr("daisabled", "disabled");

	}
</script>
<c:if test="${approvedPurchaseOrderId ==null }">
	<ul class="nav nav-tabs nav-justified nav_hov" role="tablist">
		<li role="presentation"><a href="#messages"
			aria-controls="messages" role="tab" data-toggle="tab">Packing
				List</a></li>
	</ul>
</c:if>
<!-- Tab panes -->
<input type="hidden" name="app-id" id="app-id"
	value="${approvedPurchaseOrderId }">
<div class="tab-content">

	<div role="tabpanel" class="tab-pane" id="messages">

		<section class="">
			<div class="container-fluid" id="clone-body">

				<div class='row tab_input_area  well'>
					<div class="table-responsive">
						<table class="table table-bordered table-striped table-highlight"
							id="table-data">
							<thead>
								<th>Cartoon No</th>
								<th>No Of Cartoon</th>
								<th>Color</th>
								<th>size 1</th>
								<th>size 2</th>
								<th>size 3</th>
								<th>size 4</th>
								<th>size 5</th>
								<th>size 6</th>
								<th>size 7</th>
								<th>size 8</th>
								<th>size 9</th>
								<th>size 10</th>
								<th>size 11</th>
								<th>size 12</th>
								<th>size 13</th>
								<th>Pcs/Crtn</th>
								<th>Total Pcs</th>
								<th>Remarks</th>
								<th>Action</th>

							</thead>
							<tbody>
								<tr class="tr_clone">
									<td><div style="width: 6em" class="color-hide">
											<input onkeyup="updateWidth(this)" type="text"
												class="form-control" size="80" value="" id="serial-no_1" />
										</div></td>
									<td><div style="width: 6em" class="color-hide">
											<input onkeyup="updateWidth(this)" type="text"
												class="form-control" size="80" value="" id="no-of-cartoon_1" />
										</div></td>
									<td><div style="width: 6em" class="color-hide">
											<input onkeyup="updateWidth(this)" type="text"
												class="form-control" value="" id="color_1" />
										</div></td>
									<td><div style="width: 3em">
											<input onkeyup="updateWidth(this)" type="text"
												class="form-control" value="" id="size[1]_1" />
										</div></td>
									<td><div style="width: 3em">
											<input onkeyup="updateWidth(this)" type="text"
												class="form-control" size="80" value="" id="size[2]_1" />
										</div></td>
									<td><div style="width: 3em">
											<input onkeyup="updateWidth(this)" type="text"
												class="form-control" value="" id="size[3]_1" />
										</div></td>
									<td><div style="width: 3em">
											<input onkeyup="updateWidth(this)" type="text"
												class="form-control" value="" id="size[4]_1" />
										</div></td>
									<td><div style="width: 3em">
											<input onkeyup="updateWidth(this)" type="text"
												class="form-control" size="80" value="" id="size[5]_1" />
										</div></td>
									<td><div style="width: 3em">
											<input onkeyup="updateWidth(this)" type="text"
												class="form-control" value="" id="size[6]_1" />
										</div></td>
									<td><div style="width: 3em">
											<input onkeyup="updateWidth(this)" type="text"
												class="form-control" value="" id="size[7]_1" />
										</div></td>
									<td><div style="width: 3em">
											<input onkeyup="updateWidth(this)" type="text"
												class="form-control" size="80" value="" id="size[8]_1" />
										</div></td>
									<td><div style="width: 3em">
											<input onkeyup="updateWidth(this)" type="text"
												class="form-control" value="" id="size[9]_1" />
										</div></td>
									<td><div style="width: 3em">
											<input onkeyup="updateWidth(this)" type="text"
												class="form-control" value="" id="size[10]_1" />
										</div></td>
									<td><div style="width: 3em">
											<input onkeyup="updateWidth(this)" type="text"
												class="form-control" size="80" value="" id="size[11]_1" />
										</div></td>
									<td><div style="width: 3em">
											<input onkeyup="updateWidth(this)" type="text"
												class="form-control" value="" id="size[12]_1" />
										</div></td>
									<td><div style="width: 3em">
											<input onkeyup="updateWidth(this)" type="text"
												class="form-control" value="" id="size[13]_1" />
										</div></td>
									<td><div style="width: 3em" class="color-hide">
											<input onkeyup="updateWidth(this)" type="text"
												class="form-control" value="" id="pcs-crtn_1" />
										</div></td>
									<td><div style="width: 3em" class="color-hide">
											<input onkeyup="updateWidth(this)" type="text"
												class="form-control" value="" id="total-pcs_1" />
										</div></td>
									<td><div style="width: 3em" class="color-hide">
											<input onkeyup="updateWidth(this)" type="text"
												class="form-control" value="" id="remarks_1" />
										</div></td>
								</tr>
							</tbody>
						</table>
					</div>

				</div>


			</div>
			<div class="row btn_addmore form-control">
				<button type="submit" data-type="horizontal" name="singlebutton"
					class="btn btn-success btn-lg center-block tr_clone_add pull-right">Add
					More</button>
			</div>
	</section>
	</div>
	






	<div class="row row-margin-unset">
		<table style="" class="table table-striped">
			<thead style="">
				<tr>
					<th>Item No</th>
					<th>PO No</th>
					<th>Colour</th>
					<th>Product Code/SKU</th>
					<th>hs Code</th>
					<th>Carton Qty</th>
					<th>Total Pcs</th>
					<th>Net Weight</th>
					<th>Gross Weight</th>
					<th>CBM</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="booking_table">


			</tbody>
		</table>
	</div>
		<div class="row div_btn">
		
		<div class="col-lg-4 col-md-4" style="margin-left: 81%;">
			<div class="row">
				<div class="col-lg-2 col-md-2"></div>
				<button type="submit" data-type="horizontal" name="singlebutton"
							class="btn btn-primary btn_addmore" id="btn-submit"
							value="Submit" style="border-radius: 5px; height: 35px; padding-top: 8px;">Submit</button>
				<button type="submit" data-type="horizontal" name="singlebutton"
							class="btn btn-danger btn_addmore" style="border-radius: 5px; height: 35px; padding-top: 8px;">Cancel</button>
				
	<!-- 			<div class="col-lg-3 col-md-3">
					<div class="btn_addmore">
						<button type="submit" data-type="horizontal" name="singlebutton"
							class="btn btn-primary btn_addmore" id="btn-submit"
							value="Submit" style="border-radius: 5px; height: 35px; padding-top: 8px;">Submit</button>
					</div>
				</div>
				<div class="col-lg-3 col-md-3">
					<div class="btn_addmore">
						<button type="submit" data-type="horizontal" name="singlebutton"
							class="btn btn-danger btn_addmore" style="border-radius: 5px; height: 35px; padding-top: 8px;">Cancel</button>
					</div>
				</div> -->
			</div>
		</div>
	</div>
</div>


<script>
	
</script>
<%-- <jsp:include page="footer.jsp"></jsp:include> --%>
<%@include file="footer_v2_fixed.jsp"%>

