<link rel="stylesheet"
	href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet"
	href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css">
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/newoverlay.js"></script>

<!--END OF HEADER-->

<style>
	html {
		background-color: #fff;
	}
</style>

<section>
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-2">
				
			</div>
			<div class="col-md-8">
				<div class=row>
					<div class="commercial">
						<h4 style="text-align: center; padding-left: 0px; margin-right: 0px;"><i class="fa fa-ship" aria-hidden="true"></i>Container Tracking</h4>
					</div>
				</div>
				<br>
				<div class="row">
					<form:form action="getContainerDetails" method="post"
						commandName="containerTrackingParams" id="containerTrack">
						<div class="col-md-3"></div>
						<div class="col-md-4">
							<form:input cssClass="form-control" id="containerNumber"
								path="containerNumber" placeholder="Container Number"
								type="text" />
						</div>
						<div class="col-md-2">
							<input type="submit" class="btn btn-primary form-control"
								value="Search" onclick="return isContainerExists();" />
						</div>
						<div class="col-md-3"></div>
					</form:form>
				</div>
				<br>
			</div>
			<div class=" col-md-2"></div>
		</div>
	</div>
</section>

<hr>

<div class="col-xs-12" style="height: 20px;"></div>

<div style="clear: both;"></div>

<c:if test="${not empty containerTypeList}">
	<div class="container-fluid">
			<div class="" style="height: 40px;"></div>
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					
					<table border="1" class="table commercial-table package-detail text-center table-bordered"
						style="margin-top: -10px;">
						<thead>
							
							<tr>
								<td colspan="3" style="font-size: 12px;">Container&nbsp;Information</td>
							</tr>
						</thead>
	
						<tbody>
							<!-- <tr>
								<td style="font-size: 12px;"><b>CONTAINER&nbsp;TYPE</b></td>
								<td style="font-size: 12px;"><b>DESCRIPTION</b></td>
								<td style="font-size: 12px;"><b>CONTAINER&nbsp;SIZE</b></td>
							</tr> -->
							<tr>
								<td style="font-size: 12px;"><b>Type</b>:&nbsp;${containerTypeList.typeGroup}</td>
								<td style="font-size: 12px;"><b>Description</b>:&nbsp;${containerTypeList.typeDescription}</td>
								<td style="font-size: 12px;"><b>Size</b>:&nbsp;${containerTypeList.typeSize}</td>
								
							</tr>
							
						</tbody>
					</table>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
</c:if>

<c:if test="${not empty containerInfo}">
	<div class="container-fluid">
			<div class="" style="height: 40px;"></div>
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					
					<table border="1" class="table commercial-table package-detail text-center table-bordered"
						style="margin-top: -10px;">
						<thead>
							
							<tr>
								<td style="font-size: 12px;">Booking&nbsp;Number</td>
								<td style="font-size: 12px;">Bill&nbsp;of&nbsp;Lading</td>
								<td style="font-size: 12px;">Container&nbsp;Number</td>
								<td style="font-size: 12px;">Status</td>
								<!-- <td style="font-size: 12px;">Last&nbsp;Movement</td> -->
							</tr>
						</thead>
	
						<tbody>
							<tr>
								<td style="font-size: 12px;">${containerInfo.bookingNumber}</td>
								<td style="font-size: 12px;">${containerInfo.billOfLading}</td>
								<td style="font-size: 12px;">${containerInfo.containerNumber}</td>
								<td style="font-size: 12px; width: 50px;">${containerInfo.equipmentStatus}</td>
								<%-- <td style="font-size: 12px;">
									<c:if test="${not empty lastMovement}">
										${lastMovement.activityLocation}
									</c:if>
								</td> --%>
							</tr>
							
						</tbody>
					</table>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
</c:if>

<c:if test="${not empty lastMovement}">
	<div class="container-fluid">
			<div class="" style="height: 40px;"></div>
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					
					<table border="1" class="table commercial-table package-detail text-center table-bordered"
						style="margin-top: -10px;">
						<thead>
							
							<tr>
								<td colspan="4" style="font-size: 12px;">Recent&nbsp;Movement</td>
							</tr>
						</thead>
	
						<tbody>
							<!-- <tr>
								<td style="font-size: 12px;"><b>CONTAINER&nbsp;TYPE</b></td>
								<td style="font-size: 12px;"><b>DESCRIPTION</b></td>
								<td style="font-size: 12px;"><b>CONTAINER&nbsp;SIZE</b></td>
							</tr> -->
							<tr>
								<td colspan="4" style="font-size: 12px;">
									<c:if test="${not empty lastMovement}">
										${lastMovement.activityLocation}&nbsp;at&nbsp;
										<fmt:formatDate pattern="dd-MM-yyyy, HH:MM" type="date" value="${lastMovement.dateTime}" />
										&nbsp;(${lastMovement.locDetails}),&nbsp;Voyage&nbsp;NO:&nbsp;${lastMovement.voyageNo}
									</c:if>
								</td>
							</tr>
							
						</tbody>
					</table>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>
</c:if>

<c:if test="${not empty containerSummaryList}">

		<div class="container-fluid">
			<div class="" style="height: 40px;"></div>
			<div class="row">
				<div class="col-md-2"></div>
				<div class="col-md-8">
					
					<table border="1" class="table commercial-table package-detail text-center table-bordered">
						<thead>
							<!-- <tr>
								<td colspan="5" style="font-size: 13px;">Container Details</td>
							</tr> -->
							<tr>
								<td colspan="2" style="font-size: 12px;">Activity&nbsp;Date&nbsp;Time</td>
								<td colspan="2" style="font-size: 12px;"><b>Movement&nbsp;Port</b></td>
								<td style="font-size: 12px;">Vessel&nbsp;Name</td>
								<td style="font-size: 12px; width: 50px;">Voyage&nbsp;NO</td>
							</tr>
						</thead>
	
						<tbody>
							
							<c:forEach items="${containerSummaryList}" var="sum"
							varStatus="index">
								<tr>
									<td style="font-size: 12px; width: 5%;">
										${sum.status}
									</td>
									<td style="font-size: 12px;">
										<fmt:formatDate pattern="dd-MMM-yyyy, HH:mm" type="date" value="${sum.dateTime}" />
									</td>
									<td style="font-size: 12px;">
										${sum.activityLocation}
									</td>
									<td style="width:5px;">
										<c:choose>
									        <c:when test="${sum.dtmDetails == 'ESTIMATED'}">
												<span class="badge" style="background-color: #008080;">${sum.dtmDetails}</span>
											</c:when>
									        <c:when test="${sum.dtmDetails == 'ACTUAL'}">
									        	<span class="badge" style="background-color: #00a65a;">${sum.dtmDetails}</span>
									        </c:when>
									        <c:otherwise>
									        	<span class="badge">${sum.dtmDetails}</span>
									        </c:otherwise>
									    </c:choose>
									</td>
									<td style="font-size: 12px;">
										${sum.vesselName}
									</td>
									<td style="font-size: 12px;">
										${sum.voyageNo}
									</td>
								</tr>
							</c:forEach>
						</tbody>
					</table>
				</div>
				<div class="col-md-2"></div>
			</div>
		</div>

</c:if>

<c:if test="${not empty containerDetailsList}">

	<div class="container-fluid table-responsive">

		<div class="row" >
			
			<div class="col-md-2"></div>
			<div class="col-md-8">
				<table class="table commercial-table package-detail table-bordered data-table table-hover">
					<thead style="background-color: #337ab7; color: #fff;">
						<tr>

							<th class="text-center" style="font-size: 12px; width: 20%;">Movement&nbsp;Date</th>
							<th class="text-center" colspan="2" style="width: 50%; font-size: 12px;">Movement&nbsp;Details</th>
							<th class="text-center" style="font-size: 12px;">Movement&nbsp;Location</th>
							<th class="text-center" style="font-size: 12px;">Voyage&nbsp;NO</th>

						</tr>
					</thead>
					<tbody id="accordion">

						<c:forEach items="${containerDetailsList}" var="cont"
							varStatus="index">
							<tr class="text-center">
								
								<td style="font-size: 12px; width:15px;">
									<fmt:formatDate pattern="dd-MMM-yyyy, HH:mm" type="date" value="${cont.dateTime}" />
								</td>
								<td style="font-size: 12px; width:5px;">
									<span class="text-left">
										${cont.locDetails}
									</span>
								</td>
								<td style="font-size: 12px; width:5px;">
									<c:choose>
								        <c:when test="${cont.dtmDetails == 'ESTIMATED'}">
											<span class="badge" style="background-color: #008080 ;">${cont.dtmDetails}</span>
										</c:when>
								        <c:when test="${cont.dtmDetails == 'ACTUAL'}">
								        	<span class="badge" style="background-color: #00a65a;">${cont.dtmDetails}</span>
								        </c:when>
								        <c:otherwise>
								        	<span class="badge">${cont.dtmDetails}</span>
								        </c:otherwise>
								    </c:choose>
								</td>
								
								<td style="font-size: 12px;">${cont.activityLocation}</td>
								<td style="font-size: 12px;">${cont.voyageNo}</td>
								


								<!--<fmt:parseDate value="${i.gr_date}" var="grDate" pattern="yyyyMMdd" />
								<td style="font-size:12px;"><fmt:formatDate pattern="yyyy-MM-dd" type="date" value="${grDate}" /></td>
								<fmt:parseDate value="${i.shipment_date}" var="shipmentDate" pattern="yyyyMMdd" />
								<td style="font-size:12px;"><fmt:formatDate pattern="yyyy-MM-dd" type="date" value="${shipmentDate}" /></td>
								
								<td style="font-size:12px;">${i.shipmentStatus}</td>-->

							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div class="col-md-2"></div>

		</div>

	</div>
</c:if>

<script type="text/javascript">
	
</script>
<%@include file="footer_v2_fixed.jsp"%>

<script>
	function isContainerExists() {
		var containerNumber = $('#containerNumber').val();
		if(containerNumber == "") {
			swal("Container Number", "Please Enter Container Number", "error");
			return false;
		} else {
			return true;
		}
	}
</script>

<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/dataTables.bootstrap.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jquery.dataTables.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/dataTables.buttons.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/buttons.flash.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/jszip.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/pdfmake.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/vfs_fonts.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/buttons.html5.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/buttons.print.min.js"></script>
