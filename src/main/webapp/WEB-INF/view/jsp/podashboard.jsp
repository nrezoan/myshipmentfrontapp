<%@include file="header.jsp" %>
<section class="main-body">
		<div class="container-fluid table-responsive">
			<div class="row table_header">
				<div class="col-lg-11 col-md-11 col-sm-10 col-xs-9">
					<span class="glyphicon glyphicon-triangle-bottom"></span><h3>Recent import orders status</h3>
				</div>
				<div class="col-lg-1 col-md-1 col-sm-2 col-xs-3">
					<a href="#">view all</a>
				</div>
			</div>
			<div class="row">
		<table class="table table-striped">
		  <thead style="">
      <tr>
        <th>Po No</th>
        <th>Client Name</th>
        <th>Product Number</th>
		<th>Sku-No</th>
		<th>Color</th>
		<th>Size</th>
		<th>Sales Org</th>
		<th>Date of Creation</th>
      </tr>
    </thead>
    <tbody>
    <c:forEach items="${purchaseOrder }" var="po">
      <tr><input type="hidden"  name="po.po_id" id='${po.po_id}' value='${po.po_id}'/>
      <td>${po.vc_po_no}</td>
      <td>${po.nu_client_code}</td>
      <td>${po.vc_product_no}</td>
      <td>${po.vc_sku_no}</td>
      <td>${po.vc_color}</td>
      <td>${po.vc_size}</td>
      <td>${po.sales_org}</td>
      <td>${po.po_creation_date }</td>
      </tr>
      </c:forEach>
    </tbody>
  </table>
  </div>

		</div>
		</section>
		
<%@include file="footer.jsp" %>