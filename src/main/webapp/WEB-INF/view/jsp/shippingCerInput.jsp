<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<style>
	html, body {
		background-color: #fff;
	}
</style>
<script type="text/javascript">
	
</script>
<div class="container-fluid container-fluid-441222">
	<div class="row row-without-margin">
		<h1 class="h1-report">OK To Ship Certificate</h1>
		<ol class="breadcrumb">
			<li><i class="fa fa-bar-chart"></i>&nbsp;Report Management</li>
			<li class="active">OK To Ship Certificate/Store Zone</li>
		</ol>
	</div>
	<hr>
	<div class="row row-without-margin">
		<form:form action="shippingCer" method="post" commandName="req" target="_blank"
			cssClass="form-incline">
			<div class="col-md-3 col-sm-12 col-xs-12">
				<div class='form-group'>
					<label class="text-center">HBL No.</label>
					<form:input path="hblNumber" id="number" cssClass="form-control" />
				</div>
			</div> 
			<div class="col-md-2 col-sm-6 col-xs-6">	
				<label style="display: block; color: transparent">.</label>
				<!-- added for alignment purpose, dont delete -->
				<input type="submit" name="download" id="download" onclick="return reportSearch();"	class="btn btn-success btn-block" value="Download"/>				
			</div>
		</form:form>
	</div>

</div>

<script>
	function reportSearch() {
		var number = $("#number").val();
		if (number == "") {
			swal("HBL Number", "HBL number cannot be empty!", "error");
			return false;
		} else {
			return true;
		}
	}
</script>

<!-- ----------------------------- -->
<%@include file="footer_v2_fixed.jsp"%>