<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<script
	src="${pageContext.request.contextPath}/resources/bootstrap-select/js/bootstrap-select.min.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/booking-update.js"></script>
<script type="text/javascript"
	src="${pageContext.request.contextPath}/resources/js/booking-updatedHelperMethods.js"></script>
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/bootstrap-select/css/bootstrap-select.min.css">
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/myshipment_v2/update-order-booking.css">

<style>
html, body {
	background-color: #FFF;
}

.select2 {
	width: 100% !important;
}
</style>

<script type="text/javascript">
</script>

<div class="container-fluid-booking-update-234903">

	<div class="nav-tabs-custom-949974">
		<section class="" style="display: block;">
			<span id="bookingUpdateHeader"></span>
			<hr>
		</section>
	</div>

	<ul class="nav nav-tabs nav-justified nav-book-update"
		style="margin-left: 0px;">
		<li id="listitemhead" class="active"><a data-toggle="tab"
			href="#headerItem">Header</a></li>
		<li id="listitemline" class=""><a data-toggle="tab"
			href="#lineItem">Line Item</a></li>
		<!-- <li><a data-toggle="tab" href="#packingList">Packing List</a></li> -->
	</ul>
	<div class="tab-content tab-book-update">

		<div class="nav-tabs-custom-949974">
			<section class="" style="display: block;">
				<span id="demoPrint"></span>
				<hr>
			</section>
		</div>

		<div id="headerItem" class="tab-pane fade in active">
			<!-- <div id="headerItem" class="tab-pane fade"> -->
			<!-- hamid -->
			<div class="row">
				<div class="col-md-12">
					<span class="booking-update-information-header">B/L
						Information</span>
					<hr>
				</div>
				<div class="col-md-12">
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">HBL/HAWB No.</label>
							</div>
							<div class="col-md-12">
								<span>HAWB NO</span>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group'>
							<div class="col-md-12">
								<label class="text-center">GHA</label>
							</div>
							<div class="col-md-12">
								<span>GHA Value</span>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group'>
							<div class="col-md-12">
								<label class="text-center">Origin</label>
							</div>
							<div class="col-md-12">
								<span>Origin Value</span>
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group'>
							<div class="col-md-12">
								<label class="text-center">Total Volume</label>
							</div>
							<div class="col-md-12">
								<span>${totalCBM} </span>
							</div>
						</div>
					</div>
				</div>
				<br>
				<div class="col-md-12 booking-update-padding-column">
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Exp. Number</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="expNo" class="form-control" id="expNo"
									value="${directBookingParams.orderHeader.expNo }" />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Exp. Date</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="expDate" id="expDate"
									value="${directBookingParams.orderHeader.expDate }"
									onkeydown="return false" class="form-control date-picker" />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">LC/TT/PO Number</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="lcTtPono" id="lcTtPono"
									value="${directBookingParams.orderHeader.lcTtPono }"
									class="form-control" />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">LC/TT/PO Date</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="lcTtPoDate" id="lcTtPoDate"
									value="${directBookingParams.orderHeader.lcTtPoDate }"
									onkeydown="return false" class="form-control date-picker" />
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 booking-update-padding-column">
					<div class="col-md-6">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Description of Goods</label>
							</div>
							<div class="col-md-12">
									<textarea name="description" id="description" onkeypress="checkCharacterLimit()"
									class="textarea color-change" required>${directBookingParams.orderHeader.description }</textarea>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Shipping Marks</label>
							</div>
							<div class="col-md-12">
								<textarea name="shippingMark" id="shippingMark" onkeypress="checkCharacterLimitShipingMark()"
									class="textarea color-change" required>${directBookingParams.orderHeader.shippingMark }</textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12">
					<hr>
				</div>
			</div>




			<!-- Sanjana Khan...CommInv No and Date Edit -->

			<div class="row">
			
				<div class="col-md-12">
					<span class="booking-update-information-header">Comm. Invoice Details</span>
					<hr>
				</div>
			
				<div class="col-md-12 booking-update-padding-column">
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Commercial Invoice Number</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="comInvNo" class="form-control"
									id="comInvNo"
									value="${directBookingParams.orderHeader.comInvNo }" />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Commercial Invoice Date</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="comInvDate" id="comInvDate"
									value="${directBookingParams.orderHeader.comInvDate }"
									onkeydown="return false" class="form-control date-picker" />
							</div>
						</div>
					</div>


				</div>
				
				<div class="col-md-12">
					<hr>
				</div>
			</div>





			<!-- Partner Information -->
			<div class="row">
				<div class="col-md-12">
					<span class="booking-update-information-header">Partner
						Information</span>
					<hr>
				</div>
				<!-- shipper -->
				<div class="col-md-12 booking-update-padding-column">
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Shipper</label>
							</div>
							<div class="col-md-12">
								<input name="shipperName" id="shipperName"
									value="${directBookingParams.orderHeader.shipperName }"
									class="form-control" readonly />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Shipper Address 1</label>
							</div>
							<div class="col-md-12">
								<input name="shipperAddress1" id="shipperAddress1"
									value="${directBookingParams.orderHeader.shipperAddress1 }"
									class="form-control" readonly />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Shipper Address 2</label>
							</div>
							<div class="col-md-12">
								<input name="shipperAddress2" id="shipperAddress2"
									value="${directBookingParams.orderHeader.shipperAddress2 }"
									class="form-control" readonly />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Shipper Address 3</label>
							</div>
							<div class="col-md-12">
								<input name="shipperAddress3" id="shipperAddress3"
									value="${directBookingParams.orderHeader.shipperAddress3 }"
									class="form-control" readonly />
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 booking-update-padding-column">
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Shipper City</label>
							</div>
							<div class="col-md-12">
								<input name="shipperCity" id="shipperCity"
									value="${directBookingParams.orderHeader.shipperCity }"
									class="form-control" readonly />
							</div>
						</div>
					</div>
					<div class="col-md-3" style="display: none">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Shipper Country Code</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="shipperCountry" id="shipperCountry"
									value="${directBookingParams.orderHeader.shipperCountry }"
									class="form-control" readonly />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Shipper Country</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="shipperCountryDesc"
									id="shipperCountryDesc"
									value="${directBookingParams.orderHeader.shipperCountryDesc }"
									class="form-control" readonly />
							</div>
						</div>
					</div>
				</div>
				<!-- buyer -->
				<div class="col-md-12 booking-update-padding-column">
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Buyer</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="buyerName" id="buyerName"
									value="${directBookingParams.orderHeader.buyerName}"
									class="form-control" readonly />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Buyer Address 1</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="buyerAddress1" id="buyerAddress1"
									value="${directBookingParams.orderHeader.buyerAddress1 }"
									class="form-control" readonly />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Buyer Address 2</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="buyerAddress2" id="buyerAddress2"
									value="${directBookingParams.orderHeader.buyerAddress2 }"
									class="form-control" readonly />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Buyer Address 3</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="buyerAddress3" id="buyerAddress3"
									value="${directBookingParams.orderHeader.buyerAddress3 }"
									class="form-control" readonly />
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 booking-update-padding-column">
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Buyer City</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="buyerCity" id="buyerCity"
									value="${directBookingParams.orderHeader.buyerCity }"
									class="form-control" readonly />
							</div>
						</div>
					</div>
					<div class="col-md-3" style="display: none">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Buyer Country Code</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="buyerCountry" id="buyerCountry"
									value="${directBookingParams.orderHeader.buyerCountry }"
									class="form-control" readonly />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Buyer Country</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="buyerCountryDesc" id="buyerCountryDesc"
									value="${directBookingParams.orderHeader.buyerCountryDesc }"
									class="form-control" readonly />
							</div>
						</div>
					</div>
				</div>
				<!-- notify party -->
				<div class="col-md-12 booking-update-padding-column">
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Notify Party</label>
							</div>
							<div class="col-md-12">
								<%-- 								<select class="select2 predictable-select"	id="notifyPartyName" onchange="">
									<option value="">Please Select</option>
									<c:forEach
										items="${supplierDetails.buyersSuppliersmap.notifyParty }"
										var="notifyParty">
										<option value="${notifyParty.key }">${notifyParty.value }</option>
									</c:forEach>
								</select> --%>
								<input type="text" name="notifyPartyName" id="notifyPartyName"
									value="${directBookingParams.orderHeader.notifyPartyName}"
									class="form-control" readonly />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Notify Party's Address 1</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="notifyPartyAddress1"
									id="notifyPartyAddress1"
									value="${directBookingParams.orderHeader.notifyPartyAddress1 }"
									class="form-control" />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Notify Party's Address 2</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="notifyPartyAddress2"
									id="notifyPartyAddress2"
									value="${directBookingParams.orderHeader.notifyPartyAddress2 }"
									class="form-control" />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Notify Party's Address 3</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="notifyPartyAddress3"
									id="notifyPartyAddress3"
									value="${directBookingParams.orderHeader.notifyPartyAddress3 }"
									class="form-control" />
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 booking-update-padding-column">
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Notify Party's City</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="notifyPartyCity" id="notifyPartyCity"
									value="${directBookingParams.orderHeader.notifyPartyCity }"
									class="form-control" />
							</div>
						</div>
					</div>
					<div class="col-md-3" style="display: none">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Notify Party's Country Code</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="notifyPartyCountry"
									id="notifyPartyCountry"
									value="${directBookingParams.orderHeader.notifyPartyCountry }"
									class="form-control" readonly />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Notify Party's Country</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="notifyPartyCountryDesc"
									id="notifyPartyCountryDesc"
									value="${directBookingParams.orderHeader.notifyPartyCountryDesc }"
									class="form-control" readonly />
							</div>
						</div>
					</div>
				</div>
				<!-- shipper bank -->
				<div class="col-md-12 booking-update-padding-column">
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Shipper's Bank</label>
							</div>
							<div class="col-md-12">
								<%-- <select style="" class="select2 predictable-select" id="shippersBankName" onchange="">
									<option value="">Please Select</option>
									<c:forEach items="${supplierDetails.buyersSuppliersmap.shippersBank }" var="shippersBank">
										<option value="${shippersBank.key }">${shippersBank.value }</option>
									</c:forEach>
								</select> --%>
								<input type="text" name="shippersBankName" id="shippersBankName"
									value="${directBookingParams.orderHeader.shippersBankName}"
									class="form-control" readonly />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Shipper's Bank Address 1</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="shippersBankAddress1"
									id="shippersBankAddress1"
									value="${directBookingParams.orderHeader.shippersBankAddress1 }"
									class="form-control " />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Shipper's Bank Address 2</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="shippersBankAddress2"
									id="shippersBankAddress2"
									value="${directBookingParams.orderHeader.shippersBankAddress2 }"
									class="form-control " />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Shipper's Bank Address 3</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="shippersBankAddress3"
									id="shippersBankAddress3"
									value="${directBookingParams.orderHeader.shippersBankAddress3 }"
									class="form-control " />
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 booking-update-padding-column">
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Shipper's Bank City</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="shippersBankCity" id="shippersBankCity"
									value="${directBookingParams.orderHeader.shippersBankCity }"
									class="form-control" />
							</div>
						</div>
					</div>
					<div class="col-md-3" style="display: none">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Shipper's Bank Country Code</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="shippersBankCountry"
									id="shippersBankCountry"
									value="${directBookingParams.orderHeader.shippersBankCountry }"
									class="form-control" readonly />
							</div>
						</div>
					</div>
					<div class="col-md-3">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Shipper's Bank Country</label>
							</div>
							<div class="col-md-12">
								<input type="text" name="shippersBankCountryDesc"
									id="shippersBankCountryDesc"
									value="${directBookingParams.orderHeader.shippersBankCountryDesc }"
									class="form-control" readonly />
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-12 booking-update-padding-column">
					<div class="col-md-6">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Remarks 1</label>
							</div>
							<div class="col-md-12">
								<textarea name="remarks1" id="remarks1" class="textarea">${directBookingParams.orderHeader.remarks1 }</textarea>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class='form-group required'>
							<div class="col-md-12">
								<label class="text-center">Remarks 2</label>
							</div>
							<div class="col-md-12">
								<textarea name="remarks2" id="remarks2" class="textarea">${directBookingParams.orderHeader.remarks2}</textarea>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div>

		<div id="lineItem" class="tab-pane fade">
			<!-- <div id="lineItem" class="tab-pane fade in active"> -->
			<div class="row row-no-margin-331932" style="padding-top: 10px;">
				<div class="col-xs-6 col-md-5" style="padding-left: 20px;">
					<div class="form-inline">
						<span class="" id="span-message"></span>
						<c:if test="${updateStatus==true }">
							<span class="message-success" id="span-message">${updateMessage }</span>
						</c:if>
						<c:if test="${updateStatus==false }">
							<span class="message-error" id="span-message">${updateMessage }</span>
						</c:if>
					</div>
				</div>
			</div>
			<div class="row row-no-margin-331932" style="padding-top: 10px;">

				<div class="col-xs-6 col-md-5" style="padding-left: 20px;">
					<div class="form-inline">
						<form:form
							action="${pageContext.request.contextPath }/upload/uploadExcel"
							commandName="" type="POST" enctype="multipart/form-data">
							<div class="form-group">
								<div class="input-group">
									<input type="file" name="file" class="file" id="uploadXL">
									<input type="text" class="form-control input-lg" disabled
										placeholder="Upload" style="height: 35px; text-align: center;">
									<span class="input-group-btn">
										<button class="browse btn btn-primary" type="button"
											style="height: 35px;">
											<i class="glyphicon glyphicon-search"></i> Browse
										</button>
									</span>
								</div>

								<input type="submit" class="btn btn-success" value="Upload"
									id="btn-upload">
							</div>
						</form:form>
					</div>
				</div>
				<div class="col-xs-6 col-md-3">
					<span style="display: inline;"> <a
						href="${pageContext.request.contextPath}/download/excel/booking-update.xlsx">
							<img
							src="${pageContext.request.contextPath}/resources/images/excel.png"
							style="height: 45px; width: auto;" />
					</a>
					</span>
				</div>
				<div class="col-xs-6 col-md-4">
					<div class="row" style="float: right; margin-right: 0%;">
						<a data-toggle="modal" data-target="#addItemModal"
							class="btn btn-primary" id="addItemBtn"> <i
							class="fa fa-plus" aria-hidden="true"></i> Add Item
						</a>
					</div>
				</div>
			</div>
			<!-- table part -->
			<div class="row row-no-margin-331932" style="padding-top: 10px;">
				<div class="col-xs-12 col-md-12">
					<div class="table-responsive" id="headingTwo">
						<table class="table table-hover" id="valueTable">
							<thead>
								<tr>
									<th style="text-align: center;">Item No.</th>
									<th style="text-align: center;">PO No.</th>
									<th style="text-align: center;">Style</th>
									<th style="text-align: center;">Size</th>
									<th style="text-align: center;">Color</th>
									<th style="text-align: center;">HS Code</th>
									<th style="text-align: center;">Material</th>
									<th colspan="2" style="text-align: center;">Action</th>
								</tr>
							</thead>
							<tbody id="valueTableBody" style="text-align: center;">
							</tbody>

						</table>
						<script>
							createTable(modelArray);
						</script>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div id="packingList" class="tab-pane fade" style="display: none;">
		<h3>Packing List</h3>
		<p>
			<!-- container demo -->
		<div>
			<strong><a role="button" data-toggle="collapse"
				data-parent="#accordion" href="#Three" aria-expanded="true"
				aria-controls="collapseThree">Expand</a></strong>
			<div id="collapseThree" class="panel-collapse collapse"
				role="tabpanel" aria-labelledby="headingOne">
				<div class="panel-body">
					<div class="row">
						<div class="col-xs-12" style="height: 20px;"></div>
						<div class="col-md-6">
							<div class="forms">
								<div class="form-group required">
									<div class="">
										<div style="color: black;">
											<span style="z-index: 1; margin-top: -30px; display: block;">Please
												add sizes (e.g. L, M XL etc) first before adding packing
												list items !</span>
										</div>
										<div id="errorMsg-pkl" style="color: red;">
											<span
												style="position: absolute; z-index: 1; margin-bottom: 50px;">
											</span>
										</div>
										<div class="row">
											<div class="col-md-12">
												<div class="col-md-2 col-lg-2 col-sm-2 col-xs-2">
													<div class=""
														style="background-color: #334d4d; color: #fff;">
														<span class="control-label" style="margin-left: 10px;">Size:</span>
													</div>
												</div>
												<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
													<input type="text" name="text"
														class="form-control auto-complete color-change"
														id="txt-add-size" />
												</div>
												<div class="col-md-4 col-lg-4 col-sm-4 col-xs-4">
													<button type="button" class="btn btn-primary"
														id="btn-add-size">Add Size</button>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-12" id="table-container">
							<div class="table-responsive">
								<table class="table table-bordered" id="tbl-packing-list"
									style="background-color: #5c8a8a; color: #fff;"
									id="tbl-pack-list">
									<thead style="background-color: #334d4d;">
										<tr>
											<th rowspan="2" style="width: 100px;">Ctn No</th>
											<th rowspan="2">Ctn</th>
											<th rowspan="2">Colour</th>
											<th colspan="0" style="text-align: center;">Size</th>
											<th rowspan="2">Pcs/Ctn</th>
											<th rowspan="2">Total pcs</th>
											<th rowspan="2">Remarks</th>
											<th rowspan="2" style="width: 20px;">
												<button type="button" class="btn btn-primary"
													id="btn-add-item">Add item</button>
											</th>
										</tr>
										<tr>
										</tr>
									</thead>
									<tbody style="color: #000;">
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="col-xs-12" style="height: 10px;"></div>
					<div class="row"
						style="float: right; margin-right: 2%; margin-top: 2%;">
						<input type="submit" class="btn btn-primary" value="Update"
							id="btn-update-pck-lst">
						<button type="button" class="btn btn-danger" id="cancel-packlist">Cancel</button>
					</div>
				</div>
			</div>
		</div>
	</div>

	<c:if test="${directBookingParams.orderHeader.cargoStatus!=null}">
		<p align="center">GR DONE, LINE ITEM UPDATE IS NOT ALLOWED</p>
	</c:if>
	<div class="row"
		style="float: right; margin-top: 1%; margin-right: 1%;">
		<div class="col-md-12 col-sm-12 col-xs-12">
			<input type="submit" class="btn btn-primary" value="Finish Update"
				id="btn-finish-update"> <input type="submit"
				class="btn btn-danger" id="cancelUpdate" value="Cancel Update">
		</div>
	</div>
</div>

<!-- Update Modal -->
<div class="modal fade" id="updateFormModal" role="dialog">
	<div class="modal-dialog" style="width: 90%">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<!-- <h4 class="modal-title">Modal Header</h4> -->
				<span id="bookingUpdateModalHeader"></span>
			</div>
			<div class="modal-body">
				<div class="row row-no-margin-331932">
					<div class="col-md-12">
						<input type="hidden" name="itemNumber" id="id_itemNumber">
						<span class="booking-update-modal-info-header">Item Details</span>
						<hr class="hr-update-modal">
						<div class="col-md-4">
							<div class="forms" id="formUpdate">
								<div class="form-group required">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="">PO No:</span><span class="required-alone">*</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="poNumber" id="id_poNo"
													class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Total Gross Weight:</span><span
														class="required-alone">*</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="grossWeight" id="id_grossWeight"
													class="form-control color-change" required
													onkeypress="return validateDecimalDataTypeStrict(this,event);" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Carton Length:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="cartonLength" id="id_cartonLength"
													class="form-control"
													onkeypress="return validateDecimalDataTypeStrict(this,event);"
													onchange="return calculateCBMUpdate();" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Carton Width:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="cartonWidth" id="id_cartonWidth"
													onkeypress="return validateDecimalDataTypeStrict(this,event);"
													class="form-control" onchange="return calculateCBMUpdate();" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Carton Height:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="cartonHeight" id="id_cartonHeight"
													onkeypress="return validateDecimalDataTypeStrict(this,event);"
													class="form-control" onchange="return calculateCBMUpdate();" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Carton Unit:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<select class="form-control" name="cartonMeasurementUnit"
													id="id_cartonMeasurementUnit"
													onchange="return calculateCBMUpdate();">
													<option value="">Select</option>
													<option value="IN">IN</option>
													<option selected value="CM">CM</option>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Total CBM:</span><span class="required-alone">*</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="totalVolume" id="id_totalVolume"
													class="form-control color-change" required
													onkeypress="return validateDecimalDataTypeStrict(this,event);" />
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--  class="col-md-4" -->
						<div class="col-md-4">
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Carton Quantity:</span><span class="required-alone">*</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="curtonQuantity"
													id="id_cartonQuantity" class="form-control color-change"
													required onchange="return calculateCBMUpdate();" onkeypress="return validateDataType(this,event);"
													/>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group   required">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="">Total Pieces:</span><span
														class="required-alone">*</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="totalPieces" id="id_totalPieces"
													class="form-control color-change" required
													onkeypress="return validateDataType(this,event);" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Total Net Weight:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="netWeight" class="form-control"
													id="id_netWeight"
													onkeypress="return validateDecimalDataTypeStrict(this,event);" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Style No:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="styleNo" id="id_styleNo"
													class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>HS Code:</span><span class="required-alone">*</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="hsCode" id="id_hsCode"
													class="form-control color-change" required />
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!--  class="col-md-4" -->
						<div class="col-md-4">
							<div class="forms" style="display: none">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Unit:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Refer No:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="refNo" id="id_referNo"
													class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Product Code/SKU:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="productCode" id="id_productCode"
													class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Project No/Article No:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="projectNo" id="id_projectNo"
													class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Size:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="sizeNo" id="id_sizeNo"
													class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Color:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="color" id="id_color"
													class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- class="col-md-4" -->
						<div class="col-md-8">
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-3 col-lg-3 col-sm-6 col-xs-6">
												<div class="">
													<span>Commodity:</span><span class="required-alone">*</span>
												</div>
											</div>
											<div class="col-md-9 col-lg-9 col-sm-6 col-xs-6">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- edit end -->
					</div>
				</div>
				<div class="row row-no-margin-331932">
					<div class="col-md-12">
						<span class="booking-update-modal-info-header">Comm.
							Invoice Details</span>
						<hr class="hr-update-modal">
						<div class="col-md-4">
							<div class="forms">
								<div class="form-group   required">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="">Pay Conday Cond.:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="payCond" id="id_payCond"
													class="form-control auto-complete color-change" required />
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="forms">
								<div class="form-group required">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="">Unit Price/Pcs:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="unitPrice" id="id_unitPrice"
													onkeypress="return validateDecimalDataTypeStrict(this,event);"
													class="form-control auto-complete color-change" required />
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="forms">
								<div class="form-group required">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="">Currency Unit:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="currency" id="id_currency"
													class="form-control auto-complete color-change" required />
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="row row-no-margin-331932">
					<div class="col-md-12">
						<span class="booking-update-modal-info-header">Item Extra</span>
						<hr class="hr-update-modal">
						<div class="col-md-4">
							<div class="forms">
								<div class="form-group required">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="">Comm. Invoice No.:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="commInvoice" id="id_commInvoice"
													class="form-control auto-complete color-change" required />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class=" required">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="control-label">GWeight/Carton:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="grossWeightPerCarton"
													onkeypress="return validateDecimalDataTypeStrict(this,event);"
													id="id_grossWeightPerCarton" class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class=" required">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="control-label">Net Cost:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="netCost" id="id_netCost"
													onkeypress="return validateDecimalDataTypeStrict(this,event);"
													class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group required">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="">QC Date:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="qcDate" id="id_qcDate" onkeydown="return false"
													class="date-picker form-control auto-complete color-change"
													required />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group required">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="">Release Date:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="releaseDate" id="id_releaseDate" onkeydown="return false"
													class="date-picker form-control auto-complete color-change"
													required />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group required">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="">Shipping Mark:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<textarea name="shippingMark" id="id_shippingMark"
													class="textarea color-change" required></textarea>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="forms">
								<div class="form-group">
									<div class=" required">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="control-label">Comm. Inv. Date:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="commInvoiceDate" onkeydown="return false"
													id="id_commInvoiceDate" class="form-control date-picker" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class=" required">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="control-label">Net Weight/Carton:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="netWeightPerCarton"
													onkeypress="return validateDecimalDataTypeStrict(this,event);"
													id="id_netWeightPerCarton" class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class=" required">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="control-label">Reference#1:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="reference1" id="id_reference1"
													class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class=" required">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="control-label">Reference#3:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="reference3" id="id_reference3"
													class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group required">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="">Description of Goods:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<textarea name="description" id="id_description"
													class="textarea"></textarea>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="forms">
								<div class="form-group">
									<div class=" required">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="control-label">Carton Serial No.:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="cartonSerNo" id="id_cartonSerNo"
													class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class=" required">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="control-label">Pieces/Carton:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="pcsPerCarton" id="id_pcsPerCarton"
													onkeypress="return validateDataType(this,event);"
													class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class=" required">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="control-label">Reference#2:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="reference2" id="id_reference2"
													class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class=" required">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span class="control-label">Reference#4:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<input type="text" name="reference4" id="id_reference4"
													class="form-control" />
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="forms">
								<div class="form-group">
									<div class="">
										<div class="row">
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<div class="">
													<span>Item Description:</span>
												</div>
											</div>
											<div class="col-md-6 col-lg-6 col-sm-6 col-xs-6">
												<textarea name="itemDescription" id="id_itemDescription"
													class="textarea"></textarea>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-offset-10 col-md-1 col-xs-6 col-sm-6">
						<input type="submit" class="btn btn-primary" value="Save"
							id="addItem" style="display: none;"
							onclick="saveLineItem('this')"> <input type="submit"
							class="btn btn-primary btn-block" onclick="submit_by_id()"
							value="Update" id="update-2">
					</div>
					<div class="col-md-1 col-xs-6 col-sm-6" style="margin-left: -20px;">
						<button type="button" class="btn btn-danger btn-block"
							id="cancel-item" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<script type="text/javascript">
	
</script>

<%@include file="footer_v2.jsp"%>