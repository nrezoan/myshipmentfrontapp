<%@include file="header_v2.jsp"%>
<%@include file="navbar.jsp"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<style>
html, body {
	background-color: #fff;
}
</style>
<div class="container-fluid container-fluid-441222">
	<div class="row row-without-margin">
		<h1 class="h1-report">Download Pre-Alert Document</h1>
		<ol class="breadcrumb">
			<li class="active"><i class="fa fa-bar-chart"></i>Download Pre-Alert Document</li>
		</ol>
	</div>
	<hr>
	<div class="row row-without-margin">
		<div class="col-md-6 col-sm-12 col-xs-12">
			<div class='form-group'>
				<%-- <a href="${zipdownloadlink}" target="_blank">Download Zip</a> --%>
				<span style="display: inline;"> 
					<a href="${pageContext.request.contextPath}/download/zip/${zipdownloadlink}" class="btn btn-primary form-control" style="width:50%">
						<i class="fa fa-file-archive-o" aria-hidden="true"></i>&nbsp; Download Zipped Pre-Alert Documents 						
					</a>
				</span>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	function validate() {
		var inp = document.getElementById('prealertfile');
		if (inp.files.length == 0) {
			//alert("Attachment Required");
			swal(
					"No File Attached",
					"Scanned Copy of Commercial Invoice & Packing List Missing",
					"error");
			inp.focus();
			return false;
		} else
			return true;
	}
</script>

<!-- ----------------------------- -->
<%@include file="footer_v2_fixed.jsp"%>