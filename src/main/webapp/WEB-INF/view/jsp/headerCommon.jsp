<!doctype html>
<%@page errorPage="error.jsp" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html class="no-js" lang="">
    <head>
    <style>
 		@media print {		.no-print {display: none !important;}} 
</style>   
    <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/jquery-2.2.2.min.js"></script>
     <script type="text/javascript">
     var myContextPath = "${pageContext.request.contextPath}"
     </script>
     <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/newoverlay.js"></script>
     <script type="text/javascript" src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
    
    
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Myshipment</title>
        <meta name="description" content="HTML5 template">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery-ui.css">
		<!-- favicon
		============================================ -->		
        <link rel="shortcut icon" type="image/x-icon" href="${pageContext.request.contextPath}/resources/images/favicon.ico">
		
		<!-- Google Fonts
		============================================ -->		
       <link href='${pageContext.request.contextPath}/resources/css/fonts.css' type='text/css' rel="stylesheet">
	   
		<!-- Bootstrap CSS
		============================================ -->		
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
		<!-- Bootstrap CSS
		============================================ -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css">
		<!-- normalize CSS
		============================================ -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/normalize.css">
		<!-- main CSS
		============================================ -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/main.css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/dashboard.css">
		<!-- style CSS
		============================================ -->
        <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/style.css">
		<!-- modernizr JS
		============================================ -->	
		 <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/graph.css">
		<!-- modernizr JS
		============================================	-->	
                	
        <script src="${pageContext.request.contextPath}/resources/js/vendor/modernizr-2.8.3.min.js"></script>
                
               
    </head>
    <body>
       
		<header class="header-area">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
						<div class="logo">
							<a href="${pageContext.request.contextPath}\homepage"><img src="${pageContext.request.contextPath}/resources/images/logo.png" alt="Logo"></a>
						</div>
					</div>
					
		
					
				</div>
				
				
		
		
				
			</div>
		</header>


		