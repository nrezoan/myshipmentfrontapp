var targetSVG = "M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z";

/**
 * SVG path for plane icon
 */
var planeSVG = "m2,106h28l24,30h72l-44,-133h35l80,132h98c21,0 21,34 0,34l-98,0 -80,134h-35l43,-133h-71l-24,30h-28l15,-47";

/**
 * Create the map
 */
if (depLat === '') {
	var depPort = _port_list.filter(function(e) {
		if (e['code'] === depCode)
			return e;
	});
	var arrPort = _port_list.filter(function(e) {
		if (e['code'] === arrCode)
			return e;
	});
	depLat=depPort[0].lat;
	depLong=depPort[0].lon;
	arrLat=arrPort[0].lat;
	arrLong=arrPort[0].lon;
}
if (status === 'INGATE' || status === 'LANDED'||status==='ARRIVED') {
	flightLat = arrLat;
	flightLong = arrLong;
}
if (flightLat === '' || flightLong === '') {
	flightLat = depLat;
	flightLong = depLong;
}
var map = AmCharts.makeChart( "chartdiv", {
  "type": "map",
"theme": "light",


  "dataProvider": {
    "map": "worldLow",
    "zoomLevel": 1,
//    "zoomLongitude": -55,
//    "zoomLatitude": 42,

    "lines": [ {
      "id": "line1",
      "arc": -0.85,
      "alpha": 0.3,
      "latitudes": [ depLat, flightLat],
      "longitudes": [ depLong, flightLong]


    }],
    "images": [ 
    	{
      "svgPath": targetSVG,
      "title": dep,
      "latitude": depLat,
      "longitude": depLong
    }, {
      "svgPath": targetSVG,
      "title": arr,
      "latitude": arrLat,
      "longitude": arrLong
    },{
      "svgPath": planeSVG,
      "title" : flight,
      "positionOnLine": 1,
      "color": "#585869",
      "animateAlongLine": false,
      "lineId": "line1",
      "flipDirection": true,
      "loop": true,
      "scale": 0.03,
      "positionScale": 1.8
    } 
      ]
  },

  "areasSettings": {
    "unlistedAreasColor": "#8dd9ef"
  },

  "imagesSettings": {
    "color": "#585869",
    "rollOverColor": "#585869",
    "selectedColor": "#585869",
    "pauseDuration": 0.2,
    "animationDuration": 2.5,
    "adjustAnimationSpeed": true
  },

  "linesSettings": {
    "color": "#585869",
    "alpha": 0.4
  },

} );

function vehicleBearing(startpoint, endpoint) {
    var x1 = endpoint.lat;
    var y1 = endpoint.lng;
    var x2 = startpoint.lat;
    var y2 = startpoint.lng;

    var radians = getAtan2((y1 - y2), (x1 - x2));

    function getAtan2(y, x) {
        return Math.atan2(y, x);
    };

    var compassReading = radians * (180 / Math.PI);

    var coordNames = ["N", "NE", "E", "SE", "S", "SW", "W", "NW", "N"];
    var coordPoints = ["0", "45", "90", "135", "180", "225", "270", "315", "359"];
    var coordIndex = Math.round(compassReading / 45);
    if (coordIndex < 0) {
        coordIndex = coordIndex + 8
    };

    return coordPoints[coordIndex]; // returns the coordinate value
}

function initMap() {
    // Change this array to test.
    var points = [
        { lat: parseFloat(depLat), lng: parseFloat(depLong) },
        { lat: parseFloat(flightLat), lng: parseFloat(flightLong) }
    ];

    var coordinates = getRoute();

    points = coordinates.points;
    var airborne = coordinates.airborne;

    function getBoundsZoomLevel(bounds, mapDim) {
        var WORLD_DIM = { height: 256, width: 256 };
        var ZOOM_MAX = 21;

        function latRad(lat) {
            var sin = Math.sin(lat * Math.PI / 180);
            var radX2 = Math.log((1 + sin) / (1 - sin)) / 2;
            return Math.max(Math.min(radX2, Math.PI), -Math.PI) / 2;
        }

        function zoom(mapPx, worldPx, fraction) {
            return Math.floor(Math.log(mapPx / worldPx / fraction) / Math.LN2);
        }

        var ne = bounds.getNorthEast();
        var sw = bounds.getSouthWest();

        var latFraction = (latRad(ne.lat()) - latRad(sw.lat())) / Math.PI;

        var lngDiff = ne.lng() - sw.lng();
        var lngFraction = ((lngDiff < 0) ? (lngDiff + 360) : lngDiff) / 360;

        var latZoom = zoom(mapDim.height, WORLD_DIM.height, latFraction);
        var lngZoom = zoom(mapDim.width, WORLD_DIM.width, lngFraction);

        return Math.min(latZoom, lngZoom, ZOOM_MAX);
    }

    function createMarkerForPoint(point, planeRotation) {
        var icon = new google.maps.MarkerImage(
            '/MyShipmentFrontApp/resources/img/pinmarker.png', null, null, null, new google.maps.Size(50,50)
        );

        var planeIconSVG = {

            path: "m2,106h28l24,30h72l-44,-133h35l80,132h98c21,0 21,34 0,34l-98,0 -80,134h-35l43,-133h-71l-24,30h-28l15,-47",
            fillColor: '#252525',
            fillOpacity: 1,
            anchor: new google.maps.Point(0,150),
            strokeWeight: 0,
            scale: 0.08
        }

        var pointIconSVG = {

            path: "M9,0C4.029,0,0,4.029,0,9s4.029,9,9,9s9-4.029,9-9S13.971,0,9,0z M9,15.93 c-3.83,0-6.93-3.1-6.93-6.93S5.17,2.07,9,2.07s6.93,3.1,6.93,6.93S12.83,15.93,9,15.93 M12.5,9c0,1.933-1.567,3.5-3.5,3.5S5.5,10.933,5.5,9S7.067,5.5,9,5.5 S12.5,7.067,12.5,9z",
            fillColor: '#252525',
            fillOpacity: 1,
            anchor: new google.maps.Point(0,10),
            strokeWeight: 0,
            scale: 1
        }
        //path: plane == '' ? google.maps.SymbolPath.CIRCLE : icon,
        if(planeRotation == '') {

            return new google.maps.Marker({
                position: new google.maps.LatLng(point.lat, point.lng),
                icon: {
                    path: google.maps.SymbolPath.CIRCLE,
                    scale: 5
                }
            });
        } else {

            var planeSymbol = {
                path: 'M362.985,430.724l-10.248,51.234l62.332,57.969l-3.293,26.145 l-71.345-23.599l-2.001,13.069l-2.057-13.529l-71.278,22.928l-5.762-23.984l64.097-59.271l-8.913-51.359l0.858-114.43 l-21.945-11.338l-189.358,88.76l-1.18-32.262l213.344-180.08l0.875-107.436l7.973-32.005l7.642-12.054l7.377-3.958l9.238,3.65 l6.367,14.925l7.369,30.363v106.375l211.592,182.082l-1.496,32.247l-188.479-90.61l-21.616,10.087l-0.094,115.684',
                scale: 0.0444,
                strokeOpacity: 1,
                fillOpacity: 1,
                fillColor: '#252525',
                strokeWeight: 0,
                rotation: parseInt(planeRotation),
                anchor: new google.maps.Point(400, 400)

            };

            return new google.maps.Marker({
                position: new google.maps.LatLng(point.lat, point.lng),
                icon: planeSymbol
            });
        }

    }

    function createBoundsForMarkers(markers) {
        var bounds = new google.maps.LatLngBounds();
        $.each(markers, function() {
            bounds.extend(this.getPosition());
        });
        return bounds;
    }

    var $mapDiv = $('#map');

    var mapDim = {
        height: $mapDiv.height(),
        width: $mapDiv.width()
    }

    var markers = [];
    $.each(points, function(index, value) {
        markers.push(createMarkerForPoint(this, ''));
    });

    var planeRotation = vehicleBearing(points[points.length-1], points[0]);

    if(!(Object.keys(airborne).length === 0 && airborne.constructor === Object)) {
        markers.push(createMarkerForPoint(airborne, planeRotation));
    }

    var bounds = (markers.length > 0) ? createBoundsForMarkers(markers) : null;

    var map = new google.maps.Map($mapDiv[0], {
        center: (bounds) ? bounds.getCenter() : new google.maps.LatLng(0, 0),
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        zoom: (bounds) ? (getBoundsZoomLevel(bounds, mapDim)) : 0,
        minZoom: 1.5,
        style: [
            {
                featureType: "administrative",
                elementType: "geometry",
                stylers: [
                    { visibility: "off" }
                ]
            }
        ]
    });

    $.each(markers, function() { this.setMap(map); });

    var flightPath = new google.maps.Polyline({
        path: getActualRoutePoints(points,airborne),
        geodesic: true,
        strokeColor: '#0f100d',
        strokeOpacity: 1.0,
        strokeWeight: 1
    });

    flightPath.setMap(map);

}

function initMap1() {
    var bounds =  new google.maps.LatLngBounds();

    bounds.extend( new google.maps.LatLng(parseFloat(flightLong), parseFloat(flightLong)) );
    bounds.extend( new google.maps.LatLng(parseFloat(depLat), parseFloat(depLong)) );



    console.log(bounds.getCenter());
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 2,
        minZoom: 2,
        center: bounds.getCenter(),
        //mapTypeId: 'terrain'
        mapTypeId: google.maps.MapTypeId.TERRAIN
    });

    console.log(depLat + "-----" + depLong + "-----" + flightLat + "-----" + flightLong);

    var flightPlanCoordinates = [
        {lat: parseFloat(depLat), lng: parseFloat(depLong)},
        {lat: parseFloat(flightLat), lng: parseFloat(flightLong)}
    ];
    var flightPath = new google.maps.Polyline({
        path: flightPlanCoordinates,
        geodesic: true,
        strokeColor: '#0f100d',
        strokeOpacity: 1.0,
        strokeWeight: 1
    });

    flightPath.setMap(map);

}



function generateFlightLegs(flightJsonParamList) {

    flightJsonParamList.filter(function(e) {

    });
}

function convertJsonArrayDateToJSDate(json) {

    json.filter(function (e) {
        var keys = Object.keys(e);
        for (var i = 0; i < keys.length; i++) {
            if (typeof(e[keys[i]]['iLocalMillis']) != "undefined") {
            	if(keys[i] == 'dtArrGtSchDate') {
            		var tempDate = new Date(parseInt(e[keys[i]]['iLocalMillis']));
            		 e[keys[i]] = convertDateTime(tempDate, e['vcArrGtSchTime']);
            		
            	} else {
            		e[keys[i]] = new Date(parseInt(e[keys[i]]['iLocalMillis']));
            	}
                
            }
        }
    });
}

$(document).ready(function() {

    var gateDepTime = convertDateTime(currentFlightJson[0]['depGTDate'], currentFlightJson[0]['depGTTime']);
    $('#gateDeparture .time').text(formatAMPM(gateDepTime));

    var takeOffTime = convertDateTime(currentFlightJson[0]['depRTDate'], currentFlightJson[0]['depRTTime']);
    $('#takeoffDeparture .time').text(formatAMPM(takeOffTime));

    $('#depAirpotName').text(getAirportName('depAirport', 'depCity'));
    $('#arrAirpotName').text(getAirportName('arrAirport', 'arrCity'));

    var gateArrTime = convertDateTime(currentFlightJson[0]['arrGTDate'], currentFlightJson[0]['arrGTTime']);
    $('#gateArrival .time').text(formatAMPM(gateArrTime));

    var landingTime = convertDateTime(currentFlightJson[0]['arrRTDate'], currentFlightJson[0]['arrRTTime']);
    $('#landingArrival .time').text(formatAMPM(landingTime));



    $('#flightAirlineName').text(currentFlightJson[0]['airlines']);
    $('#recentFlightNumber').text(currentFlightJson[0]['flightNo']);

    var flightStatus = currentFlightJson[0]['flightStatus'];
    if(typeof (flightStatus) != "undefined") {
        if(flightStatus == "SCHEDULED" || flightStatus == "PROPOSED") {
            $('#recentFlightStatus').text('Scheduled to Fly');
        } else if(flightStatus == "INAIR") {
            $('#recentFlightStatus').text('Flying');
        } else if(flightStatus == "OUTGATE") {
            $('#recentFlightStatus').text('Ready to Fly');
        } else if(flightStatus == "LANDED" || flightStatus == "INGATE") {
            $('#recentFlightStatus').text('Landed');
        } else if(flightStatus == "CANCELLED") {
            $('#recentFlightStatus').text('Flight Cancelled');
        } else if(flightStatus == "DELAYED") {
            $('#recentFlightStatus').text('Delayed');
        } else {
            $('#recentFlightStatus').text(flightStatus);
        }
    }

    $('#recentFlightRemaining').text(typeof (currentFlightJson[0]['remainingTime']) != "undefined" ? currentFlightJson[0]['remainingTime'] : 'N/A');

    var legs = flightInfoJson['oagResponse'].length;

    var legOneDepAirportCode = flightInfoJson['oagResponse'][0]['vcDepAirportCode'];
    var legOneDepAirportName = flightInfoJson['oagResponse'][0]['vcDepAirportName'];
    var legOneDepAirportCityName = flightInfoJson['oagResponse'][0]['vcDepAirportCityName'];
    var legOneDepAirlineName = flightInfoJson['oagResponse'][0]['vcAirlineName'];

    var legOneDepDate = getGateOrRunwayDate(flightInfoJson['oagResponse'][0]['dtFlightOrigDate']);
    var legOneDepTime = getFinalDepartureTime();


    var legOneArrAirportCode = flightInfoJson['oagResponse'][legs-1]['vcArrAirportCode'];
    var legOneArrAirportName = flightInfoJson['oagResponse'][legs-1]['vcArrAirportName'];
    var legOneArrAirportCityName = flightInfoJson['oagResponse'][legs-1]['vcArrAirportCityName'];
    var legOneArrAirlineName = flightInfoJson['oagResponse'][legs-1]['vcAirlineName'];

    var legOneArrDate = getFinalArrivalDate();
    var legOneArrTime = getFinalArrivalTime();


    $('#legOneDepartureAirportName').text(typeof (legOneDepAirlineName) != "undefined" ? legOneDepAirlineName : '');
    $('#legOneDepartureAirportCode').text(typeof (legOneDepAirportCode) != "undefined" ? legOneDepAirportCode : '');
    $('#legOneDepartureAirlineName').html('<a>'+(typeof (legOneDepAirportName) != "undefined" ? legOneDepAirportName : '')+'</a>');
    $('#legOneDepartureDate').text(legOneDepDate);
    $('#legOneDepartureTime').html('<em>' + legOneDepTime + '</em>')

    $('#legOneArrivalAirportName').text(typeof (legOneArrAirlineName) != "undefined" ? legOneArrAirlineName : '');
    $('#legOneArrivalAirportCode').text(typeof (legOneArrAirportCode) != "undefined" ? legOneArrAirportCode : '');
    $('#legOneArrivalAirlineName').html('<a>'+(typeof (legOneArrAirportName) != "undefined" ? legOneArrAirportName : '')+'</a>');
    $('#legOneArrivalDate').text(legOneArrDate);
    $('#legOneArrivalTime').html('<em>' + legOneArrTime + '</em>');



});

function getGateOrRunwayDate(date_parameter) {
    var weekDays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var date = date_parameter;
    var date_DD_MMM_YYYY = getDate_dd_MMM_YYYY(date);
    return weekDays[date.getDay()] + ' ' + date_DD_MMM_YYYY;
}

function getFinalArrivalDate() {

    var responseDate = getFinalArrivalDateFromJson();

    if(responseDate != '') {
        responseDate = getGateOrRunwayDate(responseDate);
    }

    return responseDate;
}

function getFinalDepartureTime() {

    var responseDate = flightInfoJson['oagResponse'][0]['dtFlightOrigDate'];

    var responseTime = getFinalDepartureTimeFromJson();

    if(responseDate == '' || responseTime == '') return '';

    var dateTime = convertDateTime(responseDate, responseTime);

    return formatAMPM(dateTime);


}

function getFinalArrivalTime() {

    var responseDate = getFinalArrivalDateFromJson();

    var responseTime = getFinalArrivalTimeFromJson();

    if(responseDate == '' || responseTime == '') return '';

    var dateTime = convertDateTime(responseDate, responseTime);

    return formatAMPM(dateTime);


}

function getFinalArrivalDateFromJson() {

    var lastLeg_index = flightInfoJson['oagResponse'].length - 1;

    var responseDate = '';

    if(typeof (flightInfoJson['oagResponse'][lastLeg_index]['dtArrGtActDate']) != 'undefined') {
        responseDate = flightInfoJson['oagResponse'][lastLeg_index]['dtArrGtActDate'];
    } else if(typeof (flightInfoJson['oagResponse'][lastLeg_index]['dtArrRtActDate']) != 'undefined') {
        responseDate = flightInfoJson['oagResponse'][lastLeg_index]['dtArrRtActDate'];
    } else if(typeof (flightInfoJson['oagResponse'][lastLeg_index]['dtArrGtSchDate']) != 'undefined') {
        responseDate = flightInfoJson['oagResponse'][lastLeg_index]['dtArrGtSchDate'];
    } else if(typeof (flightInfoJson['oagResponse'][lastLeg_index]['dtArrRtSchDate']) != 'undefined') {
        responseDate = flightInfoJson['oagResponse'][lastLeg_index]['dtArrRtSchDate'];
    } else if(typeof (flightInfoJson['oagResponse'][lastLeg_index]['dtArrGtEstDate']) != 'undefined') {
        responseDate = flightInfoJson['oagResponse'][lastLeg_index]['dtArrGtEstDate'];
    } else if(typeof (flightInfoJson['oagResponse'][lastLeg_index]['dtArrRtEstDate']) != 'undefined') {
        responseDate = flightInfoJson['oagResponse'][lastLeg_index]['dtArrRtEstDate'];
    } else {
        responseDate = '';
    }

    return responseDate;
}

function getFinalArrivalTimeFromJson() {

    var lastLeg_index = flightInfoJson['oagResponse'].length - 1;

    var responseTime = '';

    if(typeof (flightInfoJson['oagResponse'][lastLeg_index]['vcArrGtActTime']) != 'undefined') {
        responseTime = flightInfoJson['oagResponse'][lastLeg_index]['vcArrGtActTime'];
    } else if(typeof (flightInfoJson['oagResponse'][lastLeg_index]['vcArrRtActTime']) != 'undefined') {
        responseTime = flightInfoJson['oagResponse'][lastLeg_index]['vcArrRtActTime'];
    } else if(typeof (flightInfoJson['oagResponse'][lastLeg_index]['vcArrGtSchTIme']) != 'undefined') {
        responseTime = flightInfoJson['oagResponse'][lastLeg_index]['vcArrGtSchTIme'];
    } else if(typeof (flightInfoJson['oagResponse'][lastLeg_index]['vcArrRtSchTime']) != 'undefined') {
        responseTime = flightInfoJson['oagResponse'][lastLeg_index]['vcArrRtSchTime'];
    } else if(typeof (flightInfoJson['oagResponse'][lastLeg_index]['vcArrGtEstTime']) != 'undefined') {
        responseTime = flightInfoJson['oagResponse'][lastLeg_index]['vcArrGtEstTime'];
    } else if(typeof (flightInfoJson['oagResponse'][lastLeg_index]['vcArrRtEstTIme']) != 'undefined') {
        responseTime = flightInfoJson['oagResponse'][lastLeg_index]['vcArrRtEstTIme'];
    } else {
        responseTime = '';
    }

    return responseTime;
}

function getFinalDepartureTimeFromJson() {

    var lastLeg_index = flightInfoJson['oagResponse'].length - 1;

    var responseTime = '';

    if(typeof (flightInfoJson['oagResponse'][0]['vcDepGtActTime']) != 'undefined') {
        responseTime = flightInfoJson['oagResponse'][0]['vcDepGtActTime'];
    } else if(typeof (flightInfoJson['oagResponse'][0]['vcDepRtActTime']) != 'undefined') {
        responseTime = flightInfoJson['oagResponse'][0]['vcDepRtActTime'];
    } else if(typeof (flightInfoJson['oagResponse'][0]['vcDepGtSchTime']) != 'undefined') {
        responseTime = flightInfoJson['oagResponse'][0]['vcDepGtSchTime'];
    } else if(typeof (flightInfoJson['oagResponse'][0]['vcDepRtSchTime']) != 'undefined') {
        responseTime = flightInfoJson['oagResponse'][0]['vcDepRtSchTime'];
    } else if(typeof (flightInfoJson['oagResponse'][0]['vcDepGtEstTime']) != 'undefined') {
        responseTime = flightInfoJson['oagResponse'][0]['vcDepGtEstTime'];
    } else if(typeof (flightInfoJson['oagResponse'][0]['vcDepRtEstTime']) != 'undefined') {
        responseTime = flightInfoJson['oagResponse'][0]['vcDepRtEstTime'];
    } else {
        responseTime = '';
    }

    return responseTime;
}


function getAirportName(airportKey, airportCityKey) {
    var airportString = '';
    if(typeof (currentFlightJson[0][airportKey]) != "undefined") {
        airportString += currentFlightJson[0][airportKey];
        if(typeof (currentFlightJson[0][airportKey]) != "undefined") {
            if((currentFlightJson[0][airportKey]) != '') {
                airportString += (', ' +  currentFlightJson[0][airportCityKey]);
                return airportString;
            } else return airportString;
        } else return airportString;
    } else {
        return airportString;
    }

    return '';
}

function getDate_dd_MMM_YYYY(dateTime) {
    if(dateTime == null) return '-';
    var month_names = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    var month_names_short = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

    return dateTime.getDate() + '-' + month_names_short[dateTime.getMonth()].toUpperCase() + '-' + dateTime.getFullYear();
}

function formatAMPM(date) {
    if(date == null) return '--';
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0'+minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

function convertDateTime(date, time) {
    if(typeof(date) == 'undefined') return null;
    if(typeof(time) == 'undefined') return null;
    console.log(date.getFullYear());
    console.log(date.getMonth());
    console.log(date.getDate());
    if(date.getMonth()==9){
    	var d = new Date(date.getFullYear() + '-' + (date.getMonth()+1) + '-' + (date.getDate().toString().length == 1 ? '0'+date.getDate() : date.getDate())+ 'T' + time + 'Z');
    }
    //var d = new Date(date.getFullYear() + '-' + (date.getMonth()+1) + '-' + (date.getDate().toString().length == 1 ? '0'+date.getDate() : date.getDate())+ 'T' + time + 'Z');
    	else{
    		var d = new Date(date.getFullYear() + '-' + (date.getMonth().toString().length == 1 ? '0'+(date.getMonth()+1) :(date.getMonth()+1)) + '-' + (date.getDate().toString().length == 1 ? '0'+date.getDate() : date.getDate())+ 'T' + time + 'Z');
    	}
    console.log(d);
    return d;
    //return new Date(date.getFullYear() + '-' + (date.getMonth()+1) + '-' + date.getDate() + 'T' + time + 'Z');
}

function getFirstLegAndLastLegAirport() {
    var flightList = flightInfoJson['oagResponse'];

}

function getRoute() {
    var lastIndex = flightInfoJson['oagResponse'].length - 1;

    var result = {};
    var points = [];
    var airborne = {};

    for(var i=0; i<flightInfoJson['oagResponse'].length; i++) {
        if(i == 0) {
            if(false) {
                var flightRoute = flightInfoJson['oagResponse'][i]['vcRoute'];
                if(flightRoute != null) {


                }
            } else {
                var depLat='';
                var depLong='';
                var arrLat='';
                var arrLong='';

                _port_list.filter(function(e) {

                    if(depLat != '' && depLong != '' && arrLat != '' && arrLong != '') {
                        return e;
                    }

                    if (e['code'] === flightInfoJson['oagResponse'][i]['vcDepAirportCode']) {
                        depLat = e['lat'];
                        depLong = e['lon'];
                    }
                    if (e['code'] === flightInfoJson['oagResponse'][i]['vcArrAirportCode']) {
                        arrLat = e['lat'];
                        arrLong = e['lon'];
                    }

                });

                var tempPoints = {
                    lat: parseFloat(depLat),
                    lng: parseFloat(depLong)
                }
                points.push(tempPoints);

                tempPoints = {
                    lat: parseFloat(arrLat),
                    lng: parseFloat(arrLong)
                }
                points.push(tempPoints);
            }
        } else {
            if(false) {
                var flightRoute = flightInfoJson['oagResponse'][i]['vcRoute'];
                if(flightRoute != null) {

                }
            } else {
                var arrLat='';
                var arrLong='';

                _port_list.filter(function(e) {

                    if(arrLat != '' && arrLong != '') {
                        return e;
                    }

                    if (e['code'] === flightInfoJson['oagResponse'][i]['vcArrAirportCode']) {
                        arrLat = e['lat'];
                        arrLong = e['lon'];
                    }

                });

                var tempPoints = {
                    lat: parseFloat(arrLat),
                    lng: parseFloat(arrLong)
                }
                points.push(tempPoints);


            }
        }



        if(typeof (flightInfoJson['oagResponse'][i]['vcFlightStatus']) != "undefined") {

            if(flightInfoJson['oagResponse'][i]['vcFlightStatus'] == 'INAIR') {
                if(typeof (flightInfoJson['oagResponse'][i]['vcAirporneLatitude']) != "undefined" && typeof (flightInfoJson['oagResponse'][i]['vcAirporneLongitude']) != "undefined") {

                    var latitude = parseFloat(flightInfoJson['oagResponse'][i]['vcAirporneLatitude']);
                    var longitude = parseFloat(flightInfoJson['oagResponse'][i]['vcAirporneLongitude']);

                    airborne = {
                        lat: latitude,
                        lng: longitude
                    }
                }
            }
        }
    }

    result = {
        points: points,
        airborne: airborne
    }

    return result;
}

function getActualRoutePoints(points, airborne) {
    if(!(Object.keys(airborne).length === 0 && airborne.constructor === Object)) {
        points.pop();
        points.push(airborne);
    }
    return points;
}