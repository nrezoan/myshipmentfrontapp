$(document).ready(function() {
	$("form[name='forgetPassword']").validate({
		// Specify validation rules
		rules : {
			customerId : {
				required : true
			},
			email : {
				required : true,
				email : true
			}
		},
		messages : {
			customerId : {
				required : "Enter your Myshipment Id",
			},
			email : {
				required : "Enter your email address",
				email : "Please enter a valid email address",
			},
		},
		submitHandler : function(form) {
			form.submit();
		}
	});
});