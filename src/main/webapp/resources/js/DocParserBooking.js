var rowIdOfitemServing;

$(document).ready(function(){
	var jsonString = jsonStringReceiver();
	var  lineItemJsonObject = JSON.parse(jsonString);
	for (i = 0; i < lineItemJsonObject.length; i++) { 
		generateLineItemTable(lineItemJsonObject[i],i+1);
		}
});

function jsonStringReceiver(){
	var val=$("#lineItemVal").text();
	return val;
}

/*Thiis function creates and generates cell  value. Based on the value  of the  string it  iterates over item and creates one specific row*/
function generateLineItemTable(itemRow,itemId) {
	var html = ""
	html += "<tr id=\"rowpreview"+itemId+"\">";
	html += "<td id=\"rowpreview"+itemId+"column1"+"\">";
	html += itemId;
	html += "</td>";
	html += "<td id=\"rowpreview"+itemId+"column2"+"\">";
	html += itemRow.poNo;
	html += "</td>";
	html += "<td id=\"rowpreview"+itemId+"column3"+"\">";
	html += itemRow.hSCode;
	html += "</td>";
	html += "<td id=\"rowpreview"+itemId+"column4"+"\">";
	html += itemRow.cartonQuantity;
	html += "</td>";
	html += "<td id=\"rowpreview"+itemId+"column5"+"\">";
	html += itemRow.totalPieces;
	html += "</td>";
	html += "<td id=\"rowpreview"+itemId+"column6"+"\">";
	html += itemRow.totalGrossWeight;
	html += "</td>";
	html += "<td id=\"rowpreview"+itemId+"column7"+"\">";
	html += itemRow.cartonLength;
	html += "</td>";
	html += "<td id=\"rowpreview"+itemId+"column8"+"\">";
	html += itemRow.cartonWidth;
	html += "</td>";
	html += "<td id=\"rowpreview"+itemId+"column9"+"\">";
	html += itemRow.cartonHeight;
	html += "</td>";
	html += "<td id=\"rowpreview"+itemId+"\">";
	html += "<input type=\"submit\" id="	
		+"preview"	
		+ itemId
		+ " class=\"btn btn-primary\"  value=\"Preivew\"  onclick=\"valuePopulate(this.id)\" data-toggle=\"modal\" data-target=\"#exampleModal\"/>";
	html += "</td>";
	html += "</tr>";
	$("#lineItem_table").append(html);
}
/*This function calculate and populate the form fields from the temporary table with specific id*/
function  valuePopulate(clicked_id){
	var cell= "#row"+clicked_id+"column2";
	$("#PONumber").val($(cell).text());
	
	cell= "#row"+clicked_id+"column5";
	$("#TotalPcs").val($(cell).text());
	
	cell= "#row"+clicked_id+"column6";
	$("#GrossWeight").val($(cell).text());
	
	cell= "#row"+clicked_id+"column4";
	$("#TotalCarton").val($(cell).text());
	
	cell= "#row"+clicked_id+"column7";
	$("#CartonLength").val($(cell).text());
	
	cell= "#row"+clicked_id+"column9";
	$("#CartonHeight").val($(cell).text());
	
	cell= "#row"+clicked_id+"column8";
	$("#CartonWidth").val($(cell).text());
	
	cell= "#row"+clicked_id+"column3";
	$("#HSCode").val($(cell).text());
	
	var row="#row"+clicked_id;
	
	rowIdOfitemServing=row;
	
	calculateCBM();
}

/*This function will hide the corresponding row of the table which is added in the final table*/
function hidingSpecificRow(){
    $(rowIdOfitemServing).fadeOut();
}

