var hAndMidList = ["1010018683","1010017515","1010017355","1010017509","1010017354","1010017508","1010017353","1010017352","1010017507","1010017350","1010018683","1010017350"];

var modal = document.querySelector(".modal");
//var closeButton = document.querySelector(".close-button");

function toggleModal() {
    modal.classList.toggle("show-modal");
}

//closeButton.addEventListener("click", toggleModal);

$(document).ready(function() {
	toggleModal();
	//taking the select event listener
	
	console.log($("#loggedInUserId").text());
	
	$("#modal_select").change(function(){
		
		if($("#loggedInUserId").text()=="1000001343" && isHnMbuyer()){
	        //calling out the second modal
	        document.getElementById('invoice_myModal').style.display = "block";
		}else{
			//appending the action text of the form so that it will be redirected to the normal line item page 
			var contextAddress = $("#frm-direct-booking").attr("action");
			contextAddress+="/orderdetails";
			$("#frm-direct-booking").attr("action",contextAddress);
			console.log($("#frm-direct-booking").attr("action"));
		}
		//setting the value in consignee field
        $("#buyers").val($("#modal_select").val()).change();
        //making the modal invisible
        toggleModal();

    });
	
});

//return true if the selected buyer from the drop down is from h&m or not
function isHnMbuyer(){
	var hAndMidList = ["1010018683","1010017515","1010017355","1010017509","1010017354","1010017508","1010017353","1010017352","1010017507","1010017350","1010018683","1010017350"];
	if(hAndMidList.includes($("#modal_select").val())){
		return true;
	}
	return false;
}

