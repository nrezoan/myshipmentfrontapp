
$(document).ready(function(){
	/*
	$(".date-picker").datepicker({ dateFormat: "dd-mm-yy"}).datepicker("setDate", new Date());
	$("#btn-assgn-carr").attr("disabled","disabled");
	*/

	
	
	

	
	function changeOrgOptions(salesOrg,distributionChannel)
	{
		var $overlay=$.LoadingOverlay("show");
		$.ajax({
			url: myContextPath + '/currentSelectedOrg',
			data: {salesOrg:salesOrg,distributionChannel:distributionChannel},
			type: "GET",
			dataType : "json",
			success:function(data){
			$.each(data,function(index,value){
				if(value=='EX')
					$(".exp").removeAttr("disabled");
				
				if(value=='IM')
					$(".imp").removeAttr("disabled");
				if(value=='CHA')
					$(".cha").removeAttr("disabled");
			})
			
			var $overlay=$.LoadingOverlay("hide");
		},
		error:function( jqXHR, textStatus, errorThrown ){
			var $overlay=$.LoadingOverlay("hide");
		}
		})
	}
	
	/*$("button").click(function(){
		var salesOrg=$("input[name='salesOrgdrpDwn']").val();
		var distChnl=$("input:checked").val();
		changeOrgOptions(salesOrg,distChnl);
	})*/
	
	
	
	$("#btn-appr-po-search").click(function(){
		var $overlay=$.LoadingOverlay("show");
		var buyer=$("#buyer").val();
		var poNumber=$("#po_number").val();
		var fromDate=$("#from_date").val();
		var toDate=$("#to_date").val();
		searchForApprPoToBook(buyer,poNumber,fromDate,toDate);
		var $overlay=$.LoadingOverlay("hide");
	})
	
	function searchForApprPoToBook(buyer,poNumber,fromDate,toDate)
	{
		$.ajax({
			url: myContextPath + '/approvednotbookedpo',
			data: {

				po_no: poNumber,
				from_date: fromDate,
				to_date: toDate,
				buyer: buyer
			},
			type: "GET",
			dataType: "json",
			success:function(data){
			console.log(data);	
			},
			error:function(jqXHR, textStatus, errorThrown)
			{
				
			}
		})
	}
	
	
	$("#btn-proceed-to-book").click(function(){
		var $overlay=$.LoadingOverlay("show");
		var arrObj=null;
		var tr=$("#tbl-directbook-search-po").find("tbody").find("tr");
		var checkBoxes=$("#tbl-directbook-search-po").find("tbody").find("input[type=checkbox]:checked");
		if(checkBoxes.length<1)
			{
			alert("Please select atleast one PO!");
			var $overlay=$.LoadingOverlay("hide");
			return false;
			}
		else{	
		$.each(tr,function(index,value){
			var checkBox=$(value).find("input:checked");
			
			if(checkBox.length>0)
				{
				var hiddenField=$(value).find("input[type=hidden]")[0];
				if(arrObj==null)
				arrObj=$(hiddenField).val();
				else
					arrObj=arrObj+","+$(hiddenField).val();	
				}
			
		})
		
		window.location=myContextPath+'/orderheader?appPurchOrder='+arrObj;
		var $overlay=$.LoadingOverlay("hide");
		}
	})
	
	
	
	
})



function fillData(element)
	{
	
	if(element.value.length>3)
		{
		var $overlay=$.LoadingOverlay("show");
	$.ajax({
		url:myContextPath +'/portdetails',
		data:{param:element.value},
		dataType : "json"
		
	})
	.done(function(data){
		$(element).autocomplete({
			
			source:function(request,response){
				
				response(data);
			}
		})
		var $overlay=$.LoadingOverlay("hide");
	})
		}
	}


function fillPOData(element)
{

if(element.value.length>5)
	{
	var $overlay=$.LoadingOverlay("show");
$.ajax({
	url:myContextPath +'/populatepo',
	data:{param:element.value},
	dataType : "json"
	
})
.done(function(data){
	$(element).autocomplete({
		
		source:function(request,response){
			
			response(data);
		}
	})
	var $overlay=$.LoadingOverlay("hide");
})
	}
}

function fillPlaceOfReciept(element)
{
	
if(element.value.length>3)
	{
	var $overlay=$.LoadingOverlay("show");
$.ajax({
	url:myContextPath +'/portofreciept',
	data:{
		searchString:element.value
		},
	dataType : "json"
	
})
.done(function(data){
	$(element).autocomplete({
		
		source:function(request,response){
			
			response(data);
		}
	})
	var $overlay=$.LoadingOverlay("hide");
})
	}

}





