
function zoom()
 {
   document.body.style.zoom = "90%" ;
        
 }
	function drawChart(details){	
		var dataArray=createDataArray(details.lstBuyerWiseCBMJson);
		$.getScript('resources/js/canvasjsmin.js',function(){
	$("#chartContainer").CanvasJSChart({
		
		backgroundColor: null,
		title: { 
/*			text: "Buyer Wise CBM",
			fontSize: 24*/
		}, 
		axisY: { 
			title: "Products in %" 
		}, 
		legend :{ 
			verticalAlign: "center", 
			horizontalAlign: "right" 
		},
        		
		data: [ 
		{ 
			type: "pie", 
			showInLegend: true, 
			toolTipContent: "{label} <br/> {y} %", 	
			indexLabel: "{y} %", 
			dataPoints:dataArray,
			click: function(e){ 
        	    //alert(  e.dataSeries.type+ " x:" + e.dataPoint.label + ", y: "+ e.dataPoint.y);
        	    
        	    var buyerDetails=e.dataPoint.label.split('-');
        	    var buyerNo=buyerDetails[buyerDetails.length-1];
        	    window.location.href=myContextPath+'/shipdetshipper?buyerNo='+buyerNo;
        	  }
		} 
		] 
		
	}); 
	
	
})


	}	
	function createDataArray(data)
	{
		var arrObj=[];
		var object={};
		$.each(data,function(index,value){
			object['label']=value.buyerName+'-'+value.buyerNo;
				object['legendText']=value.buyerName;
				object['y']=Number(value.totalCBMPerc.toFixed(2));
				object['cursor']='pointer'
			arrObj.push(object);
			object={};
		})
		return arrObj;
		}
