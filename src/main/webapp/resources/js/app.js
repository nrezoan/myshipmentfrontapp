
function doPrint() {
	w = window.open();
	w.document.write($('.printable_area').html());
	w.print();
	w.close();
}
function getLoginDetails(customerCode, password) {
	var $overlay = $.LoadingOverlay("show");
	$
	.ajax({
		url : myContextPath + '/login',
		data : {
			customerCode : customerCode,
			password : password
		},
		type : "GET",
		dataType : "json"

	})
	.done(
			function(data) {
				var jsonString = JSON.stringify(data);
				var code = data.bapiReturn2.type;
				if (code == 'E') {
					document.getElementById("loginIncorrect").style.display = 'block';
					 $("#loginIncorrect").text("Invalid User/Password, Please Try Again");
				} else {
					document.getElementById("loginIncorrect").style.display = 'none';
					var salesAreas = data.mpSalesOrgTree;
					$(".companyDrpDwn").empty();
					var $dropDown = '';
					$.each(salesAreas, function(index, value) {
						$dropDown = $dropDown + '<option value="'
						+ value.salesOrg + '">'
						+ value.salesOrgText + '</option>';

					})
					$(".companyDrpDwn").removeAttr("disabled");
					$(".companyDrpDwn").append($dropDown);
				}

				$.LoadingOverlay("hide")
			}).fail(function(xhr, status, errorThrown) {
				alert("Sorry, there was a problem!");
				console.log("Error: " + errorThrown);
				console.log("Status: " + status);
				console.dir(xhr);
				$.LoadingOverlay("hide")
			})

}

function approvePo(itemId, NoOfPcs, Qty, Comment) {
	$.ajax({
		url : myContextPath + '/updatepo',
		data : {
			itemId : itemId,
			NoOfPcs : NoOfPcs,
			Qty : Qty,
			Comment : Comment
		},
		type : "POST",
		dataType : "json"

	}).done(data)
	{
		alert(data.message)
	}
}

function updateCarrierInfo(arrObj) {
	var jsonString = JSON.stringify(arrObj);
	$.ajax({
		url : myContextPath + '/updateassigncarrier',
		contentType : 'application/json',
		data : JSON.stringify(arrObj),
		type : "POST",
		dataType : "json"

	}).done()
	{
		// alert(data.message)
	}
}

$(document)
.ready(
		function() {
			$("#btn-approve").attr("disabled", "disabled");
			$("#btn-aprv-search").click(
					function() {
						var $overlay = $.LoadingOverlay("show");
						$("#display-message").hide();
						var po_no = $("#po_number").val();
						var from_date = $("#from_date").val();
						var to_date = $("#to_date").val();
						var supplier_code = $("#supplier_code").val();
						$("#from_date_lbl").text(from_date);
						$("#to_date_lbl").text(to_date);
						searchDataToApprove(po_no, from_date, to_date,
								supplier_code);

					})

					function searchDataToApprove(po_no, from_date, to_date,
							supplier_code) {
				$
				.ajax({
					url : myContextPath + '/posearchapprove',
					data : {

						po_no : po_no,
						from_date : from_date,
						to_date : to_date,
						supplier_code : supplier_code
					},
					type : "GET",
					dataType : "json"
				})
				.done(
						function(data) {
							if (data.length == 0) {
								$(".table").find("tbody")
								.empty();
								$("#display-message")
								.removeClass("green");
								$("#display-message").addClass(
								"red");
								$("#display-message").text(
								"No Data Found !");
								$("#display-message").show();
								$("#btn-approve").attr(
										"disabled", "disabled");
							} else {
								$(".table").find("tbody")
								.empty();
								// $("#display-message").hide();
								$
								.each(
										data,
										function(index,
												value) {
											var column = '<td><input type="hidden" name="po_id" id="'
												+ value.po_id
												+ '" value="'
												+ value.po_id
												+ '"/><input type="hidden" name="seg_id" id="'
												+ value.seg_id
												+ '" value="'
												+ value.seg_id
												+ '"/><div class="checkbox"><label><input type="checkbox" id="chk_'
												+ value.seg_id
												+ '"/></label></div></td><td>'
												+ value.vc_po_no
												+ '</td><td>'
												+ value.vc_sku_no
												+ '</td><td>'
												+ value.vc_color
												+ '</td><td>'
												+ value.vc_size
												+ '</td><td><input name="vc_tot_pcs" class="form-control" size="3" value="'
												+ value.vc_tot_pcs
												+ '"/></td>'
												var selectedCarrierId = value.carrierId;
											/*
											 * <td>'+value.supplier_code+'</td><td>'+value.po_creation_date+'</td>';
											 */
											var carriers = '<td><select class="form-control" name="carr-drpdwn"><option>Select</option>';
											$
											.each(
													value.carrierMaster,
													function(
															index,
															value) {
														if (selectedCarrierId == value.carrier_id) {
															carriers = carriers
															+ '<option value="'
															+ value.carrier_id
															+ '" selected>'
															+ value.carrier_name
															+ '('
															+ value.carrier_code
															+ ')</option>'
														} else {
															carriers = carriers
															+ '<option value="'
															+ value.carrier_id
															+ '">'
															+ value.carrier_name
															+ '('
															+ value.carrier_code
															+ ')</option>'
														}

													})
													carriers = carriers
													+ '</select></td>';
											column = column
											+ carriers;
											column = column
											+ '<td><input id="" name="date-picker-result" type="text" class="date-picker form-control glyphicon glyphicon-calendar" value="'
											+ value.carrierSchedule
											+ '"/></td><td><textarea class="form-control" name="comment" row="1"></textarea></td>'
											var $row = $(
											"<tr></tr>")
											.append(
													column);

											$(".table")
											.find(
											"tbody")
											.append(
													$row);
											$(
											"input[name='date-picker-result']")
											.datepicker(
													{
														dateFormat : "dd-mm-yy"
													})
													.datepicker();

											$(
											"#btn-approve")
											.removeAttr(
											"disabled");
										})
							}
							var $overlay = $
							.LoadingOverlay("hide");
						})

			}

			$("#btn-search-asncrr").click(
					function() {
						var $overlay = $.LoadingOverlay("show");
						$("#display-message").hide();
						var po_no = $("#po_number").val();
						var from_date = $("#from_date").val();
						var to_date = $("#to_date").val();
						var supplier_code = $("#supplier_code").val();
						$("#from_date_lbl").text(from_date);
						$("#to_date_lbl").text(to_date);
						searchDatToAssignCarrieer(po_no, from_date,
								to_date, supplier_code);
					})

					function searchDatToAssignCarrieer(po_no, from_date,
							to_date, supplier_code) {
				$
				.ajax({
					url : myContextPath + '/searchasgncrr',
					data : {

						po_no : po_no,
						from_date : from_date,
						to_date : to_date,
						supplier_code : supplier_code
					},
					type : "GET",
					dataType : "json"
				})
				.done(
						function(data) {
							if (data.length == 0) {
								$(".table").find("tbody")
								.empty();
								$("#display-message")
								.removeClass("green");
								$("#display-message").addClass(
								"red");
								$("#display-message").text(
								"No Data Found !");
								$("#display-message").show();
								$("#btn-assgn-carr").attr(
										"disabled", "disabled");
							} else {
								// $("#display-message").hide();
								$(".table").find("tbody")
								.empty();
								$
								.each(
										data,
										function(index,
												value) {
											// var
											// column =
											// '<td><input
											// type="hidden"
											// name="po_id"
											// id="' +
											// value.po_id
											// + '"
											// value="'
											// +
											// value.po_id
											// +
											// '"/><input
											// type="hidden"
											// name="app_id"
											// id="' +
											// value.app_id
											// + '"
											// value="'
											// +
											// value.app_id
											// +
											// '"/><div
											// class="checkbox"><label><input
											// type="checkbox"
											// id="chk_'
											// +
											// value.seg_id
											// +
											// '"/></label></div></td><td>'
											// +
											// value.vc_po_no
											// +
											// '</td><td>'
											// +
											// value.nu_client_code
											// +
											// '</td><td>'
											// +
											// value.vc_product_no
											// +
											// '</td><td>'
											// +
											// value.vc_sku_no
											// +
											// '</td><td>'
											// +
											// value.vc_color
											// +
											// '</td><td>'
											// +
											// value.vc_size
											// +
											// '</td><td>'
											// +
											// value.sales_org
											// +
											// '</td><td><input
											// name="vc_tot_pcs"
											// class="form-control"
											// value="'
											// +
											// value.vc_tot_pcs
											// +
											// '"/></td>'
											var column = '<td><input type="hidden" name="po_id" id="'
												+ value.po_id
												+ '" value="'
												+ value.po_id
												+ '"/><input type="hidden" name="seg_id" id="'
												+ value.seg_id
												+ '" value="'
												+ value.seg_id
												+ '"/><div class="checkbox"><label><input type="checkbox" id="chk_'
												+ value.seg_id
												+ '"/></label></div></td><td>'
												+ value.vc_po_no
												+ '</td><td>'
												+ value.vc_sku_no
												+ '</td><td>'
												+ value.vc_color
												+ '</td><td>'
												+ value.vc_size
												+ '</td><td>'
												+ value.vc_tot_pcs
												+ '</td>'
												var selectedCarrierId = value.carrierId;
											/*
											 * <td>'+value.supplier_code+'</td><td>'+value.po_creation_date+'</td>';
											 */
											var carriers = '<td><select class="form-control" name="crr-drp-dwn"><option>Select</option>';
											$
											.each(
													value.carrierMaster,
													function(
															index,
															value) {
														if (selectedCarrierId == value.carrier_id) {
															carriers = carriers
															+ '<option value="'
															+ value.carrier_id
															+ '" selected>'
															+ value.carrier_name
															+ '('
															+ value.carrier_code
															+ ')</option>'
														} else {
															carriers = carriers
															+ '<option value="'
															+ value.carrier_id
															+ '">'
															+ value.carrier_name
															+ '('
															+ value.carrier_code
															+ ')</option>'

														}
													})
													carriers = carriers
													+ '</select></td>';
											column = column
											+ carriers;
											column = column
											+ '<td><input name="date-picker-result" type="text" class="date-picker form-control glyphicon glyphicon-calendar"  value="'
											+ value.carrierSchedule
											+ '"/></td><td><textarea class="form-control" name="comment" row="5"></textarea></td>'
											var $row = $(
											"<tr></tr>")
											.append(
													column);

											$(".table")
											.find(
											"tbody")
											.append(
													$row);
											// $(".date-picker").datepicker({dateFormat:
											// "dd-mm-yy"}).datepicker();
											$(
											"input[name='date-picker-result']")
											.datepicker(
													{
														dateFormat : "dd-mm-yy"
													})
													.datepicker();

											$(
											"#btn-assgn-carr")
											.removeAttr(
											"disabled");
										})
							}
							var $overlay = $
							.LoadingOverlay("hide");
						})

			}

			$("#btn-approve")
			.click(
					function() {
						var $overlay = $.LoadingOverlay("show");
						var arrObj = new Array();
						var assignCarrierApproveVO;
						var po_id = $("input[name='po_id']")
						.val();
						var vc_tot_pcs;
						var seg_id;
						var carrier_id;
						var carrier_schedule;
						var approve_comment;

						var table = $("#tbl-aprv-po");
						$(table)
						.find("tbody")
						.find("tr")
						.each(
								function(tr) {
									var $this = this;

									var inputChecked = $(
											this)
											.find(
													"input:checkbox");
									if (inputChecked.length > 0
											&& inputChecked[0].checked == true) {
										seg_id = $(this)
										.find(
												"input[name='seg_id']")
												.val();
										vc_tot_pcs = $(
												this)
												.find(
														"input[name='vc_tot_pcs']")
														.val();
										carrier_id = $(
												this)
												.find(
														"select[name='carr-drpdwn']")
														.val();
										if (carrier_id == 'Select')
											carrier_id = '0';
										carrier_schedule = $(
												this)
												.find(
														".date-picker")
														.val();
										approve_comment = $(
												this)
												.find(
														"textarea[name='comment']")
														.val();
										assignCarrierApproveVO = new Object();
										assignCarrierApproveVO.po_id = po_id;
										assignCarrierApproveVO.seg_id = seg_id;
										assignCarrierApproveVO.carrier_id = carrier_id;
										assignCarrierApproveVO.vc_tot_pcs = vc_tot_pcs;
										assignCarrierApproveVO.carrier_schedule = carrier_schedule;
										assignCarrierApproveVO.approve_comment = approve_comment;
										arrObj
										.push(assignCarrierApproveVO);
									}
								});
						approveSelectedPO(arrObj);
					})

					function approveSelectedPO(arrObj) {
				$.ajax({
					url : myContextPath + '/updatepo',
					data : JSON.stringify(arrObj),
					contentType : 'application/json',
					type : "POST",
					success : function(data) {
						$("#display-message").removeClass("red");
						$("#display-message").addClass("green");
						$("#display-message").text(
						"Approved Successfuly");
						$("#display-message").show();
						var po_no = $("#po_number").val();
						var from_date = $("#from_date").val();
						var to_date = $("#to_date").val();
						var supplier_code = $("#supplier_code").val();
						$("#from_date_lbl").text(from_date);
						$("#to_date_lbl").text(to_date);
						searchDataToApprove(po_no, from_date, to_date,
								supplier_code);
						var $overlay = $.LoadingOverlay("hide");
					},
					error : function(jqXHR, textStatus, errorThrown) {
						$("#display-message").removeClass("green");
						$("#display-message").addClass("red");
						$("#display-message").text(
								"An error occured" + textStatus + ':'
								+ errorThrown);
						$("#display-message").show();
						var $overlay = $.LoadingOverlay("hide");
					}
				})

			}

			$("#btn-assgn-carr")
			.click(
					function() {
						var $overlay = $.LoadingOverlay("show");
						var arrObj = new Array();
						var assignCarrierApproveVO;
						var po_id = $("input[name='po_id']")
						.val();
						var app_id;
						var carrier_id;
						var carrier_schedule;
						var vc_total_pcs;
						var comments;
						var selectedCarrier = 0;
						var seg_id = 0;
						var table = $("#tbl-asgn-crrr");
						$(table)
						.find("tbody")
						.find("tr")
						.each(
								function(tr) {
									var $this = this;

									var inputChecked = $(
											this)
											.find(
													"input:checkbox");
									if (inputChecked.length > 0
											&& inputChecked[0].checked == true) {
										app_id = $(this)
										.find(
												"input[name='app_id']")
												.val();
										carrier_id = $(
												this)
												.find(
														"select[name='crr-drp-dwn']")
														.val();
										if (carrier_id == 'Select')
											carrier_id = '0';
										carrier_schedule = $(
												this)
												.find(
														".date-picker")
														.val();
										vc_total_pcs = $(
												this)
												.find(
														"input[name='vc_tot_pcs']")
														.val();
										comments = $(
												this)
												.find(
														"textarea[name='comment']")
														.val();
										seg_id = $(this)
										.find(
												"input[name='seg_id']")
												.val();
										assignCarrierApproveVO = new Object();
										assignCarrierApproveVO.po_id = po_id;
										assignCarrierApproveVO.seg_id = seg_id;
										//assignCarrierApproveVO.app_id=app_id;
										assignCarrierApproveVO.carrier_id = carrier_id;
										assignCarrierApproveVO.carrier_schedule = carrier_schedule;
										//assignCarrierApproveVO.vc_total_pcs=vc_total_pcs;
										assignCarrierApproveVO.comments = comments;
										arrObj
										.push(assignCarrierApproveVO);
										selectedCarrier++;
									}
								});
						if (selectedCarrier > 0)
							assignCarrier(arrObj);
						else {
							$("#display-message").removeClass(
							"green");
							$("#display-message").addClass(
							"red")
							$("#display-message")
							.text(
							"Please select Carrier to assign");
							$("#display-message").show();
						}
					})
					function assignCarrier(arrObj) {
				$.ajax({
					url : myContextPath + '/updateassigncarrier',
					data : JSON.stringify(arrObj),
					contentType : 'application/json',
					type : "POST",
					success : function(data) {
						$("#display-message").removeClass("red");
						$("#display-message").addClass("green");
						$("#display-message").text(
						"Assigned Successfuly");
						$("#display-message").show();
						var po_no = $("#po_number").val();
						var from_date = $("#from_date").val();
						var to_date = $("#to_date").val();
						var supplier_code = $("#supplier_code").val();
						$("#from_date_lbl").text(from_date);
						$("#to_date_lbl").text(to_date);
						searchDatToAssignCarrieer(po_no, from_date,
								to_date, supplier_code);
						var $overlay = $.LoadingOverlay("hide");
					},
					error : function(jqXHR, textStatus, errorThrown) {
						$("#display-message").removeClass("green");
						$("#display-message").addClass("red");
						$("#display-message").text(
								"An error occured" + textStatus + ':'
								+ errorThrown);
						$("#display-message").show();
						var $overlay = $.LoadingOverlay("hide");
					}
				})

			}

			$(".companyDrpDwn").change(function() {
				var $overlay = $.LoadingOverlay("show");
				var salesOrg = $(".companyDrpDwn").val();
				$.ajax({
					url : myContextPath + '/distibutionchannels',
					data : {
						salesOrg : salesOrg
					},
					type : "GET",
					dataType : "json",
					success : function(data) {
						$.each(data, function(index, value) {
							if (value == 'EX')
								$(".exp").removeAttr("disabled");

							if (value == 'IM')
								$(".imp").removeAttr("disabled");
							if (value == 'CHA')
								$(".cha").removeAttr("disabled");
						})
						var $overlay = $.LoadingOverlay("hide");

					},
					error : function(jqXHR, textStatus, errorThrown) {
						var $overlay = $.LoadingOverlay("hide");
					}
				})
			})

		})
