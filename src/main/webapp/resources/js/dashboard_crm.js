
$(document).ready(function() {
    if (loginDTO != undefined) {
        $("#dashboard_to_date").datepicker({
            dateFormat: "dd-mm-yy"
        }).datepicker("setDate", loginDTO.dashBoardToDate);
        $("#dashboard_from_date").datepicker({
            dateFormat: "dd-mm-yy"
        }).datepicker("setDate", loginDTO.dashBoardFromDate);
    } else {
        $("#dashboard_to_date").datepicker({
            dateFormat: "dd-mm-yy"
        }).datepicker("setDate", new Date());
        var currentDate = new Date();
        currentDate.setMonth(currentDate.getMonth() - 2);
        $("#dashboard_from_date").datepicker({
            dateFormat: "dd-mm-yy"
        }).datepicker("setDate", currentDate);
    }
    
    /*$("#totalBooking-dateRange").text("In Last 60 Days");
    $("#openBookings-dateRange").text("In Last 60 Days");
    $("#goodsHandOver-dateRange").text("In Last 60 Days");
    $("#stuffingDone-dateRange").text("In Last 60 Days");
    $("#inTransit-dateRange").text("In Last 60 Days");
    $("#arrived-dateRange").text("In Last 60 Days");*/
    
});

$("#totalBookings-title").text("Total Bookings");
$("#openBookings-title").text("Open Bookings");
$("#goodsHandOver-title").text("Goods Handover");
if(loginDTO.divisionSelected == 'SE') {
	$("#stuffingDone-title").text("Stuffing Done");
	$("#inTransit-image").addClass("fa fa-ship");
} else if(loginDTO.divisionSelected == 'AR') {
	$("#stuffingDone-title").text("MAWB Issued");
	$("#inTransit-image").addClass("fa fa-plane");
}

$("#inTransit-title").text("In Transit (*EST)");
$("#arrived-title").text("Arrived (*EST)");

$("#totalBooking-number").text(Math.round(dashboardInfoBox.totalShipment));
$("#openBookings-number").text(Math.round(dashboardInfoBox.openOrder));
$("#goodsHandOver-number").text(Math.round(dashboardInfoBox.goodsReceived));
$("#stuffingDone-number").text(Math.round(dashboardInfoBox.stuffingDone));
$("#inTransit-number").text(Math.round(dashboardInfoBox.inTransit));
$("#arrived-number").text(Math.round(dashboardInfoBox.delivered));

var gwtValue;
var cbmValue;

if(loginDTO.divisionSelected == 'SE') {
	$("#tableHeaderGWT").text("Total Gross Weight");
	$("#tableHeaderCBM").text("Total Volume (CBM)");
	
	gwtValue = Number(dashboardInfoBox.totalGWT.toFixed(2)).toLocaleString();
	cbmValue = Number(dashboardInfoBox.totalCBM.toFixed(3)).toLocaleString();
} else if(loginDTO.divisionSelected == 'AR') {
	$("#tableHeaderGWT").text("Total Gross Weight");
	$("#tableHeaderCBM").text("Total Charge Weight");
	
	gwtValue = Number(dashboardInfoBox.totalGWT.toFixed(2)).toLocaleString();
	cbmValue = Number(dashboardInfoBox.totalCRGWT.toFixed(2)).toLocaleString()
}



var pendingIPLNumber = 0;
var pendingMBLNumber = 0;
var pendingPreAlertNumber = 0;
$.each(hblWisePreAlert, function(index,value){
	if(value.preAlertHbl.isPreAlertDoc == "0") {
		pendingIPLNumber++;
	}
	/*if(value.preAlertHbl.isPreAlertDoc == "1" && value.preAlertHbl.isMBL == "1") {
		pendingPreAlertNumber++;
	}*/
	
});
$.each(mblWisePreAlert, function(index,value){
	var isMblExists = false;
	$.each(value, function(subIndex,subValue){
		if(subValue.preAlertHbl.isMBL == '1') {
			isMblExists = true;
			return false;
		}
	});
	if(!isMblExists) {
		pendingMBLNumber++;
	} else {
		pendingPreAlertNumber++;
	}
});

$("#pendingIPLNumber").text(pendingIPLNumber);
$("#pendingMBLNumber").text(pendingMBLNumber);
$("#pendingPreAlertNumber").text(pendingPreAlertNumber);



