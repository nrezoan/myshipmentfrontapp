function DisableCopyPaste(e) {

	// Message to display
	var message = "Copy - Paste Option is disabled, Please type and select from the populated list";
	// check mouse right click or Ctrl key press
	var kCode = event.keyCode || e.charCode || e.which;

	// alert(kCode);
	// FF and Safari use e.charCode, while IE use e.keyCode
	if ( kCode == 17 || kCode == 2 || kCode == 91 || kCode == 3 ) {
		alert(message);
		return false;
	}
}

function handleBookingTemplate() {

	if ( validateDateFields() && validateMandatoryFields() ) {
		var tempName = $("#buyers").val();
		var _Obj = [];
		$('form').find('input[type="text"], textarea, select').each(function(i, field) {

			// data[this.name] = this.value;
			var data = {};
			if ( !checkIfDate(this.value) ) {
				if ( this.id != 'comInvNo' || this.id != 'expNumber' || this.id != 'lcTtPono' || this.id != 'disccriptonOfGoods' || this.id != 'shippingMark' ) {
					data["name"] = this.id;
					data["value"] = this.value;
					_Obj.push(data);
				}
			}
		});
		saveDataAsTemplate(_Obj, tempName);
	}
}
function validateMandatoryFields() {

	if ( $("#buyers").val() == undefined || $("#buyers").val() == null || $("#buyers").val() == "" ) {
		$("#buyers").focus();
		alert("Please Select A Valid Buyer");
		return false;
	}
	else if ( $("#orderType").val() == undefined || $("#orderType").val() == null || $("#orderType").val() == "" ) {
		$("#orderType").focus();
		return false;
	}
	else if ( $("#termOfShipment").val() == undefined || $("#termOfShipment").val() == null || $("#termOfShipment").val() == "" ) {
		$("#termOfShipment").focus();
		return false;
	}
	else if ( $("#hblInitial").val() == undefined || $("#hblInitial").val() == null || $("#hblInitial").val() == "" ) {
		$("#hblInitial").focus();
		return false;
	}
	else if ( $("#freightMode").val() == undefined || $("#freightMode").val() == null || $("#freightMode").val() == "" ) {
		$("#freightMode").focus();
		return false;
	}
	else if ( $("#comInvNo").val() == undefined || $("#comInvNo").val() == null || $("#comInvNo").val() == "" ) {
		$("#comInvNo").focus();
		return false;
	}
	else if ( $("#lcomInvDate").val() == undefined || $("#lcomInvDate").val() == null || $("#lcomInvDate").val() == "" ) {
		$("#lcomInvDate").focus();
		return false;
	}
	else if ( $("#tosDes").val() == undefined || $("#tosDes").val() == null || $("#tosDes").val() == "" ) {
		$("#tosDes").focus();
		return false;
	}
	else if ( $("#placeOfReceipt").val() == undefined || $("#placeOfReceipt").val() == null || $("#placeOfReceipt").val() == "" ) {
		alert('Please Select Place of Receipt');
		$("#placeOfReceipt").focus();
		return false;
	}
	else if ( $("#portOfLoading").val() == undefined || $("#portOfLoading").val() == null || $("#portOfLoading").val().indexOf('-') == -1 || $("#portOfLoading").val() == "" ) {
		alert('Invalid Port of Loading, Please Type Correct Port Of Loading');
		$("#portOfLoading").focus();
		return false;
	}
	else if ( $("#cargoHandoverDate").val() == undefined || $("#cargoHandoverDate").val() == null || $("#cargoHandoverDate").val() == "" ) {
		$("#cargoHandoverDate").focus();
		return false;
	}
	else if ( $("#portOfDischarge").val() == undefined || $("#portOfDischarge").val() == null || $("#portOfDischarge").val().indexOf('-') == -1 || $("#portOfDischarge").val() == "" ) {
		alert('Invalid Port of Discharge, Please Type Correct Port Of Discharge');
		$("#portOfDischarge").focus();
		return false;
	}
	else if ( $("#plcOfDel").val() == undefined || $("#plcOfDel").val() == null || $("#plcOfDel").val().indexOf('-') == -1 || $("#plcOfDel").val() == "" ) {
		alert('Invalid Place of Delivery, Please Type Correct Place of Delivery');
		$("#plcOfDel").focus();
		return false;
	}
	else if ( $("#placeOfDel").val() == undefined || $("#placeOfDel").val() == null || $("#placeOfDel").val() == "" ) {
		$("#placeOfDel").focus();
		return false;
	}
	else if ( $("#disccriptonOfGoods").val() == undefined || $("#disccriptonOfGoods").val() == null || $("#disccriptonOfGoods").val() == "" ) {
		$("#disccriptonOfGoods").focus();
		return false;
	}
	else if ( $("#shippingMark").val() == undefined || $("#shippingMark").val() == null || $("#shippingMark").val() == "" ) {
		$("#shippingMark").focus();
		return false;
	}
	else if ( $("#cargoHandoverDate").val() != null && $("#cargoHandoverDate").val() != "" ) {

		var buyerList = [ '1010000009', '1010000019', '1010000031', '1010000013', '1010000015', '1010000018', '1010000022', '1010000023', '1010000048', '1010000071', '1010000117', '1010000125', '1010026593' ];
		// alert('buyer: '+$("#buyers").val());
		if ( $.inArray($("#buyers").val(), buyerList) > -1 ) {
			return true;

		}
		else {
			var handOverDate = $("#cargoHandoverDate").val();
			var hangoverDateArray = handOverDate.split("-");
			// alert(hangoverDateArray[2]+hangoverDateArray[1]+hangoverDateArray[0]);
			var dt = new Date(hangoverDateArray[2], hangoverDateArray[1] - 1, hangoverDateArray[0]);
			// alert(dt);
			var currentDateTime = new Date();
			// alert(currentDateTime);
			if ( currentDateTime.getFullYear() == dt.getFullYear() && currentDateTime.getMonth() == dt.getMonth() && currentDateTime.getDate() == dt.getDate() ) {
				alert("Cargo Handover Date Can't be the Current Date");
			}
			dt.setHours(currentDateTime.getHours());
			dt.setMinutes(currentDateTime.getMinutes());
			dt.setSeconds(currentDateTime.getSeconds());
			// alert(dt.getTime());
			// alert(currentDateTime.getTime());
			var diffinHours = ((dt.getTime() - currentDateTime.getTime()) / 36e5);
			if ( diffinHours >= 24 ) {
				var status = confirm("Do You Confirm To Proceed With Cargo Handover Date : " + handOverDate);
				if ( status == true ) {
					return true;
				}
				else {
					return false;
				}
			}
			else {
				alert("Invalid Cargo Handover Date, Please Provide Correct Cargo Handover Date");
				$("#cargoHandoverDate").focus();
			}
			// $("#cargoHandoverDate").focus();
			return false;
		}
	}
	else
		return true;
}
$(document).ready(function() {

	/*
	 * var date=new Date(); processTimeTrack["processName"]="directBooking";
	 * processTimeTrack["startTime"]=date;
	 */
	var btn = document.getElementById("tempSave");
	btn.onclick = function() {

		var _Obj = [];
		$('form').find('input[type="text"], textarea, select').each(function(i, field) {

			// data[this.name] = this.value;
			var data = {};
			if ( !checkIfDate(this.value) ) {
				data["name"] = this.id;
				data["value"] = this.value;
				_Obj.push(data);
			}
		});
		var tempName = $("#tempName").val();
		saveDataAsTemplate(_Obj, tempName);

	}
	// for invoice modal
	modal.style.display = "block";
});

function checkIfDate(date) {

	var dateArray = date.split("-");
	if ( dateArray.length >= 3 ) {
		var d = dateArray[0];
		var m = dateArray[1];
		var y = dateArray[2];
		if ( (d > 0 && d < 32) && (m > 0 && m < 13) && (y > 0 && y < 2050) )
			return true;
		else
			return false;
	}
	else
		return false;
}
function saveDataAsTemplate(arrObj, temp_name) {

	var jsonData = JSON.stringify(arrObj);
	$.ajax({
		url : myContextPath + '/saveDataAsTemp',
		data : {
			'temp_name' : temp_name,
			'json_data' : jsonData
		},

		type : "POST",
		success : function(data) {

			// alert("template saved");
			// $( "#submitBtn" ).trigger( "click" );
			$("#frm-direct-booking").submit();
		},
		error : function(jqXHR, textStatus, errorThrown) {

			$("#display-message").removeClass("green");
			$("#display-message").addClass("red");
			$("#display-message").text("An error occured" + textStatus + ':' + errorThrown);
			$("#display-message").show();
			var $overlay = $.LoadingOverlay("hide");
		}
	})

}

$('#buyers').on('changed.bs.select', function(e) {

	alert("hello");
});

function getTemplateDataAndfillInComponent(obj) {

	var value = $("#buyers").selectpicker('val');
	if ( obj == "" || obj == null || obj == undefined ) {
		alert("Please select the Template Name");
		return false;
	}
	else {
		var $overlay = $.LoadingOverlay("show");
		$.ajax({
			url : myContextPath + '/getTempateInfo',
			data : {
				'template_Id' : obj
			},
			type : "GET",
			dataType : "json"
		}).done(function(data) {

			var jsonString = JSON.stringify(data);
			var templateData = data.templateData;
			setValueInComponent(templateData);
			$("#myBtnSv").hide();
			$("#myBtnUpdt").show();
			$.LoadingOverlay("hide")
		}).fail(function(xhr, status, errorThrown) {

			// alert("Problem loading template! Please select other options!");
			console.log("Error: " + errorThrown);
			console.log("Status: " + status);
			console.dir(xhr);
			$.LoadingOverlay("hide")
		})

	}
}
function setValueInComponent(templateData) {

	// var tempData = JSON.stringify(templateData);
	$.each(JSON.parse(templateData), function(idx, obj) {

		// alert(obj.name);
		// alert(obj.value);
		if ( obj.name == "placeOfReceipt" ) {
			$("#" + obj.name).val(obj.value).trigger("change");
		}
		else if ( obj.name != "" ) {
			$("#" + obj.name).val(obj.value);
		}
	});
}
function closeTemplateModel() {

	var modal = document.getElementById('myModal');
	// modal.style.display = "block";
	modal.style.display = "none";
}

function updateTemplateInfo() {

	var _Obj = [];
	$('form').find('input, textarea, select').each(function(i, field) {

		var data = {};
		data["name"] = this.id;
		data["value"] = this.value;
		_Obj.push(data);
	});
	var template_id = $("#template_ID").val();
	updateTemplate(_Obj, template_id);
}
function updateTemplate(_Obj, temp_Id) {

	var jsonData = JSON.stringify(_Obj);
	$.ajax({
		url : myContextPath + '/updateTempInfo',
		data : {
			'temp_Id' : temp_Id,
			'json_data' : jsonData
		},

		type : "POST",
		success : function(data) {

			// window.location=myContextPath+'/orderdetails';
			// alert("template updated");
			$("#submitBtn").trigger("click");
		},
		error : function(jqXHR, textStatus, errorThrown) {

			$("#display-message").removeClass("green");
			$("#display-message").addClass("red");
			$("#display-message").text("An error occured" + textStatus + ':' + errorThrown);
			$("#display-message").show();
			var $overlay = $.LoadingOverlay("hide");
		}
	})
}

/*
 * hamid 17.09.17
 */

function validateDateFields() {

	var count = 0;
	if ( $("#lcexpdt").val() != "" ) {
		var isLcExpDtValid = validatedate($("#lcexpdt").val());
		if ( !isLcExpDtValid ) {
			$("#lcexpdt").focus();
			alert("Invalid Date, Only dd-mm-yyyy Format Valid For LC Expiry Date");
			return false;
		}
		else {
			// return true;
			count++;
		}
	}
	else {
		count++;
	}
	if ( $("#lcomInvDate").val() != "" ) {
		var isComInvDtValid = validatedate($("#lcomInvDate").val());
		if ( !isComInvDtValid ) {
			$("#lcomInvDate").focus();
			alert("Invalid Date, Only dd-mm-yyyy Format Valid For Commercial Invoice Date");
			return false;
		}
		else {
			// return true;
			count++;
		}
	}
	else {
		count++;
	}
	if ( $("#expdt").val() != "" ) {
		var isExpDtValid = validatedate($("#expdt").val());
		if ( !isExpDtValid ) {
			$("#expdt").focus();
			alert("Invalid Date, Only dd-mm-yyyy Format Valid For Exp. Date");
			return false;
		}
		else {
			// return true;
			count++;
		}
	}
	else {
		count++;
	}
	if ( $("#lcttpodt").val() != "" ) {
		var isLcTtPoDtValid = validatedate($("#lcttpodt").val());
		if ( !isLcTtPoDtValid ) {
			$("#lcttpodt").focus();
			alert("Invalid Date, Only dd-mm-yyyy Format Valid For LC/TT/PO Date");
			return false;
		}
		else {
			// return true;
			count++;
		}
	}
	else {
		count++;
	}
	if ( $("#cargoHandoverDate").val() != "" ) {
		var ischdtValid = validatedate($("#cargoHandoverDate").val());
		if ( !ischdtValid ) {
			$("#cargoHandoverDate").focus();
			alert("Invalid Date, Only dd-mm-yyyy Format Valid For Cargo Handover Date");
			return false;
		}
		else {
			// return true;
			count++;
		}
	}
	else {
		count++;
	}
	if ( count == 5 ) {
		return true;
	}
	else {
		return false;
	}
}
/* script for validating date */
function validatedate(inputText) {

	var dateformat = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
	// Match the date format through regular expression
	if ( inputText.match(dateformat) ) {
		// document.form1.text1.focus();
		// Test which seperator is used '/' or '-'
		/*
		 * var opera1 = inputText.split('/'); var opera2 = inputText.split('-');
		 * lopera1 = opera1.length; lopera2 = opera2.length; if (lopera1 > 1) {
		 * var pdate = inputText.split('/'); } else if (lopera2 > 1) {
		 */
		var pdate = inputText.split('-');
		// }
		var dd = parseInt(pdate[0]);
		var mm = parseInt(pdate[1]);
		var yy = parseInt(pdate[2]);
		// Create list of days of a month [assume there is no leap year by
		// default]
		var ListofDays = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];
		if ( yy < 1900 || yy > 2100 ) {
			// alert('Invalid date format!');
			return false;
		}
		if ( mm < 0 || mm > 12 ) {
			// alert('Invalid date format!');
			return false;
		}
		if ( mm == 1 || mm > 2 ) {
			if ( dd > ListofDays[mm - 1] ) {
				// alert('Invalid date format!');
				return false;
			}
		}
		if ( mm == 2 ) {
			var lyear = false;
			if ( (!(yy % 4) && yy % 100) || !(yy % 400) ) {
				lyear = true;
			}
			if ( (lyear == false) && (dd >= 29) ) {
				// alert('Invalid date format!');
				return false;
			}
			if ( (lyear == true) && (dd > 29) ) {
				// alert('Invalid date format!');
				return false;
			}
		}
		return true;
	}
	else {
		// alert("Invalid date format!");
		// document.form1.text1.focus();
		return false;
	}
}
/*
 * function checkCharacterLimit() { var text = $("#disccriptonOfGoods").val();
 * var arr = text.split("\n");
 * 
 * if(arr.length > 5) { alert("You've exceeded the 4 line limit!");
 * event.preventDefault(); // prevent characters from appearing } else { for(var
 * i = 0; i < arr.length; i++) { if(arr[i].length > 24 && i < 3) { alert("Length
 * exceeded in line 1, 2, or 3!"); event.preventDefault(); // prevent characters
 * from appearing } } }
 * 
 * console.log(arr.length + " : " + JSON.stringify(arr)); }
 */
function checkCharacterLimit() {

	/* var text = $("#disccriptonOfGoods").val(); */
	var LINE_LENGTH_CHARS = 33;
	var lines = $("#disccriptonOfGoods").val().split("\n");
	var last_line = lines[lines.length - 1];
	if ( last_line.length >= LINE_LENGTH_CHARS ) {
		$("#disccriptonOfGoods").val($("#disccriptonOfGoods").val() + "\n")
	}
}
function checkCharacterLimitShippingMark() {

	/* var text = $("#disccriptonOfGoods").val(); */
	var LINE_LENGTH_CHARS = 33;
	var lines = $("#shippingMark").val().split("\n");
	var last_line = lines[lines.length - 1];
	if ( last_line.length >= LINE_LENGTH_CHARS ) {
		$("#shippingMark").val($("#shippingMark").val() + "\n")
	}
}