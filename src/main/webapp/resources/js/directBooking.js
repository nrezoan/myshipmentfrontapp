var itemId = 0;
var requestObject = [];
var rowNum = 1;
var currentEditingItem = 0;

var sizeArray = [];
var extraItemDetailString = "";
var packingItemDetailString = ""
$(function() {
	var availableTags = [ "port Link", "Singapore", "Dhaka", "India",
			"Port Rashid", "Sharjah", "Bagram", "COBOL", "Sheghnan",
			"Sardeh Band", "Sarandï¿½" ];
	$(".auto-complete").autocomplete({
		source : availableTags
	});
});

$(document).ready(function() {

	$(".date-picker").datepicker({
		dateFormat : "dd-mm-yy"
	})/* .datepicker("setDate", new Date()); */
	$("#btn-submit").click(function() {
		if (requestObject.length > 0) {
			if (createPackingObject()) {
				var packingObj = {};
				packingObj["packinklist"] = sizeArray;
				requestObject.push(packingObj)
			}
			$.LoadingOverlay("show");

			/*
			 * $.ajax({ url : myContextPath + '/saveorder', data :
			 * JSON.stringify(requestObject), type : "POST", contentType :
			 * 'application/json', success : function(data) { var
			 * bapiRet=data.bapiReturn2[0]; if(bapiRet.type=='C') {
			 * $.LoadingOverlay("hide"); alert(bapiRet.message);
			 * requestObject=[]; $("#booking_table").html("");
			 * $("#uploadXL").val(""); $(".form-control.input-lg").val(""); }
			 * else { window.location = myContextPath + '/bookingresponse';
			 * $.LoadingOverlay("hide"); } }, error : function(jqXHR,
			 * textStatus, errorThrown) { $.LoadingOverlay("hide"); } })
			 */

			$.ajax({
				url : myContextPath + '/saveorder',
				data : JSON.stringify(requestObject),
				type : "POST",
				contentType : 'application/json',
				success : function(data) {
					window.location = myContextPath + '/bookingresponse';
					$.LoadingOverlay("hide");
				},
				error : function(jqXHR, textStatus, errorThrown) {
					$.LoadingOverlay("hide");
				}
			})

		} else {
			alert("Please add atleast one item in the Booking");
		}
	})

	/*
	 * var xlf = document.getElementById("uploadXL"); if(xlf.addEventListener)
	 * xlf.addEventListener('change', handleFile, false);
	 */
	$("#btn-upload").click(function(e) {
		var file = $("#uploadXL")[0].files;
		handleFile(file);
	})

})
$(".tr_clone_add").click(function() {
	$(".tr_clone").clone().removeClass('tr_clone').appendTo("#clone-body");

});
$(document).on('click', '.browse', function() {
	var file = $(this).parent().parent().parent().find('.file');
	file.trigger('click');
});
$(document).on(
		'change',
		'.file',
		function() {

			$(this).parent().find('.form-control').val(
					$(this).val().replace(/C:\\fakepath\\/i, ''));
		});

function validateDataType(element, e) {
	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
		// display error message
		$(element).tooltip({

			tooltipClass : "alert alert-info"

		})
		return false;

	} else
		return true;
}
function validateDecimalDataTypeStrict(element, event) {
	// alert(e.which);
	var charCode = (event.which) ? event.which : event.keyCode
	var ident = event.target.value;
	if ((charCode != 46 || ident.indexOf('.') != -1)
			&& (charCode < 48 || charCode > 57)) {
		return false;
	} else {
		return true;
	}

}

function validateDecimalDataType(element, e) {
	// alert(e.which);
	if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)
			&& e.which != 46) {
		// display error message
		$(element).tooltip({

			tooltipClass : "alert alert-info"

		})
		return false;

	} else
		return true;
}

function calculateCBM() {
	if (checkMandatoryForCBM()) {
		try {
			var length = document.getElementById('CartonLength').value;
			var width = document.getElementById('CartonWidth').value;
			var height = document.getElementById('CartonHeight').value;
			var unit = document.getElementById('CartonMeasurementUnit').value;
			var noOfCar = document.getElementById('TotalCarton').value;
			if (unit == 'IN') {
				var cbm = ((length * width * height) / 61024)
						* noOfCar;
			}
			if (unit == 'CM') {
				var cbm = ((length * width * height) / 1000000) * noOfCar;
			}

			document.getElementById('TotalCBM').value = cbm.toFixed(3);
			cbmFieldFunction();
		} catch (Exception) {
			document.getElementById('TotalCBM').value = '';
		}
	} else {
		$("#CartonMeasurementUnit").refresh();
		return false;
	}
}

function checkMandatoryForCBM() {
	if ($("#GrossWeight").val() == null || $("#GrossWeight").val() == undefined
			|| $("#GrossWeight").val() == "") {
		alert("Please fill mandatory fields ! ");
		$("#GrossWeight").focus();
		return false;
	}
	/*
	 * else if($("#CartonLength").val()==null ||
	 * $("#CartonLength").val()==undefined || $("#CartonLength").val()=="") {
	 * alert("Please fill mandatory fields ! "); $("#CartonLength").focus();
	 * return false; }
	 */
	else if ($("#TotalCarton").val() == null
			|| $("#TotalCarton").val() == undefined
			|| $("#TotalCarton").val() == "") {
		alert("Please fill mandatory fields ! ");
		$("#TotalCarton").focus();
		return false;
	}
	/*
	 * else if($("#CartonWidth").val()==null ||
	 * $("#CartonWidth").val()==undefined || $("#CartonWidth").val()=="") {
	 * alert("Please fill mandatory fields ! "); $("#CartonWidth").focus();
	 * return false; } else if($("#CartonHeight").val()==null ||
	 * $("#CartonHeight").val()==undefined || $("#CartonHeight").val()=="") {
	 * alert("Please fill mandatory fields ! "); $("#CartonHeight").focus();
	 * return false; }
	 */
	else if ($("#TotalPcs").val() == null || $("#TotalPcs").val() == undefined
			|| $("#TotalPcs").val() == "") {
		alert("Please fill mandatory fields ! ");
		$("#TotalPcs").focus();
		return false;
	} else
		return true;

}
function generateBookingTable(itemRow) {
	var html = ""
	var itemDetails = itemRow.itemDetail;
	html += "<tr>";
	html += "<td>";
	html += itemRow.itemId;
	html += "</td>";
	html += "<td>";
	html += itemDetails[0].PONumber;
	html += "</td>";
	html += "<td>";
	html += itemDetails[0].MaterialNo;
	html += "</td>";
	html += "<td>";
	html += itemDetails[0].Color;
	html += "</td>";
	html += "<td>";
	html += itemDetails[0].ProductCode;
	html += "</td>";
	html += "<td>";
	html += itemDetails[0].HSCode;
	html += "</td>";
	html += "<td>";
	html += itemDetails[0].TotalCarton;
	html += "</td>";
	html += "<td>";
	html += itemDetails[0].TotalPcs;
	html += "</td>";
	html += "<td>";
	html += itemDetails[0].NetWeight;
	html += "</td>";
	html += "<td>";
	html += itemDetails[0].GrossWeight;
	html += "</td>";
	html += "<td>";
	html += itemDetails[0].TotalCBM;
	html += "</td>";
	html += "<td>";
	html += '<input type="submit" id="'
			+ itemRow.itemId
			+ '" class="btn btn-danger btn_btn remove_btn" value="Delete" onClick="handleDelete(this)"/>';
	html += "</td>";
	html += '<td>';
	html += '<input type="checkbox" id="' + itemRow.itemId + '" name="items"'
			+ 'style="vertical-align: middle;"/>';
	html += '</td>';
	html += "</tr>";
	$("#booking_table").append(html);
	document.getElementById("TotalCBM").disabled = false;
	/*
	 * document.getElementById("CartonWidth").value="";
	 * document.getElementById("CartonHeight").value="";
	 * document.getElementById("CartonLength").value="";
	 * document.getElementById("TotalCBM").onclick = myFunction();
	 */
	hidingSpecificRow();
}
$(".remove_btn").on('click', function() {
	$("#data-table").closest("tr").remove();
});
function calculateCBMValidation() {
	try {
		var po_num = $("#PONumber").val();
		var noOfCar = $("#TotalCarton").val();
		var CBM = $("#TotalCBM").val();
		var material = $("#MaterialNo").val();
		var grossWeight = $("#GrossWeight").val();
		var totalPieces = $("#TotalPcs").val();
		var cartonLength = $("#CartonLength").val();
		var cartonWidth = $("#CartonWidth").val();
		var cartonHeight = $("#CartonHeight").val();
		var cartonQuantity = $("#TotalCarton").val();
		var pcsPerCarton = $("#Line_Pcs/Carton").val();

		if (pcsPerCarton == null || pcsPerCarton == "") {
			/*
			 * bootbox.alert({ message: "Pcs Per carton can not be empty",
			 * callback: function () { $("#itemExtra_pcsPerCarton").focus(); } })
			 */
			// $("#myModal1").modal();
			$("#Line_Pcs/Carton").focus();
			return false;
		} else if (cartonLength == null || cartonLength == "") {
			alert(" Carton length can not be empty!");
			$("#CartonLength").focus();
			return false;
		} else if (cartonWidth == null || cartonWidth == "") {
			alert(" Carton width can not be empty!");
			$("#CartonWidth").focus();
			return false;
		} else if (cartonHeight == null || cartonHeight == "") {
			alert(" Carton height can not be empty!");
			$("#CartonHeight").focus();
			return false;
		} else if (cartonQuantity == null || cartonQuantity == "") {
			alert(" Carton Quantity can not be empty!");
			$("#TotalCarton").focus();
			return false;
		} else if (po_num == null || po_num == "") {
			alert("PO Number Can not be Empty!")
			$("#PONumber").focus();
			return false;
		} else if (noOfCar == null || noOfCar == "") {
			alert("Number of Carton  Can not be Empty!")
			$("#TotalCarton").focus();
			return false;
		} else if (material == null || material == "") {
			alert(" Commoodity Can not be empty");
			$("#MaterialNo").focus();

			return false;
		} else if (grossWeight == null || grossWeight == "") {
			alert(" Gross Weight Can not be Empty")
			$("#GrossWeight").focus();
			return false;
		} else if (totalPieces == null || totalPieces == "") {
			alert("Total Pieces Can not be empty !");
			$("#TotalPcs").focus();
			return false;
		} else if (CBM == null || CBM == "") {

			alert("CBM Can not be Empty!")
			$("#TotalCBM").focus();
			return false;
		} else {
			return true;
		}
	} catch (Exception) {
		alert(Exception);
	}
}
function validateHMFields() {
	if ($("#Style").val() == null || $("#Style").val() == ""
			|| $("#Style").val() == undefined) {
		$("#Style").focus();
		return false;
	} else if ($("#ReferenceNo1").val() == null
			|| $("#ReferenceNo1").val() == ""
			|| $("#ReferenceNo1").val() == undefined) {
		$("#ReferenceNo1").focus();
		return false;
	} else if ($("#ProductCode").val() == null || $("#ProductCode").val() == ""
			|| $("#ProductCode").val() == undefined) {
		$("#ProductCode").focus();
		return false;
	} else if ($("#ProjectNo").val() == null || $("#ProjectNo").val() == ""
			|| $("#ProjectNo").val() == undefined) {
		$("#ProjectNo").focus();
		return false;
	} else if ($("#Line_ReferenceNo5").val() == null
			|| $("#Line_ReferenceNo5").val() == ""
			|| $("#Line_ReferenceNo5").val() == undefined) {
		$("#Line_ReferenceNo5").focus();
		return false;
	} else
		return true;

}

function validateMandatory() {
	/*
	 * if(planningList.length >0){ validateHMFields(); }
	 */
	if ($("#PONumber").val() == null || $("#PONumber").val() == ""
			|| $("#PONumber").val() == undefined) {
		$("#PONumber").focus();
		return false;
	} else if ($("#TotalCarton").val() == null || $("#TotalCarton").val() == ""
			|| $("#TotalCarton").val() == undefined) {
		$("#TotalCarton").focus();
		return false;
	} else if ($("#GrossWeight").val() == null || $("#GrossWeight").val() == ""
			|| $("#GrossWeight").val() == undefined) {
		$("#GrossWeight").focus();
		return false;
	} else if ($("#TotalPcs").val() == null || $("#TotalPcs").val() == ""
			|| $("#TotalPcs").val() == undefined) {
		$("#TotalPcs").focus();
		return false;
	} else if ($("#TotalCBM").val() == null || $("#TotalCBM").val() == ""
			|| $("#TotalCBM").val() == undefined || $("#TotalCBM").val() <= 0) {
		$("#TotalCBM").focus();
		return false;
	} else if ($("#HSCode").val() == null || $("#HSCode").val() == ""
			|| $("#HSCode").val() == undefined) {
		$("#HSCode").focus();
		return false;
	} else if ($("#MaterialNo").val() == null || $("#MaterialNo").val() == ""
			|| $("#MaterialNo").val() == undefined) {
		$("#MaterialNo").focus();
		return false;
	} else
		return true;

}

function createBookingObj() {
	if (validateMandatory()) {
		if (planningList.length > 0) {
			if (validateHMFields()) {
				var itemDetail = [];
				var itemExtra = [];
				var itemRow = {};
				item = {}
				/* if (calculateCBMValidation() == true) { */
				itemId += eval(1);
				$("#itemdetail").find(
						'input:text, input:password, select, textarea').each(
						function() {

							item[$(this).attr("id")] = $(this).val();

						});
				itemDetail.push(item);
				item = {}
				$("#second-tab").find(
						'input:text, input:password, select, textarea').each(
						function() {

							item[$(this).attr("id")] = $(this).val();

						});
				itemExtra.push(item);
				itemRow["itemId"] = itemId;
				itemRow["itemDetail"] = itemDetail;
				itemRow["itemExtra"] = itemExtra;
				requestObject.push(itemRow);
				if (requestObject.length > 1)
					if ($("#update-item") != undefined)
						$("#update-item").attr("daisabled", "disabled");
				generateBookingTable(itemRow);
			} else {
				alert("Please fill the mandatory fields!");
				return false;

			}
		} else {
			var itemDetail = [];
			var itemExtra = [];
			var itemRow = {};
			item = {}
			/* if (calculateCBMValidation() == true) { */
			itemId += eval(1);
			$("#itemdetail").find(
					'input:text, input:password, select, textarea').each(
					function() {

						item[$(this).attr("id")] = $(this).val();

					});
			itemDetail.push(item);
			item = {}
			$("#second-tab").find(
					'input:text, input:password, select, textarea').each(
					function() {

						item[$(this).attr("id")] = $(this).val();

					});
			itemExtra.push(item);
			itemRow["itemId"] = itemId;
			itemRow["itemDetail"] = itemDetail;
			itemRow["itemExtra"] = itemExtra;
			requestObject.push(itemRow);
			if (requestObject.length > 1)
				if ($("#update-item") != undefined)
					$("#update-item").attr("daisabled", "disabled");
			generateBookingTable(itemRow);

		}
	} else {
		alert("Please fill the mandatory fields!");
		return false;
	}
}

function createPackingObject() {
	if (sizeArray.length <= 0) {
		if (checkIfPackingListAdded()) {
			var tr = $("#table-data").find("tbody").find("tr");
			$.each(tr, function(index, value) {
				var packingObj = {};
				$(this).find("input").each(function() {
					packingObj[$(this).attr("id")] = $(this).val();
				})
				sizeArray.push(packingObj);

			})
			return true;
		}
		/*
		 * else { alert("There is no packing list added in the Booking! Want to
		 * add ?"); var
		 * tabs=$(".nav.nav-tabs.nav-justified.nav_hov").find("li"); return
		 * false; }
		 */
	}
	return true;
}

function checkIfPackingListAdded() {
	var tr = $("#table-data").find("tbody").find("tr");
	if (tr.length <= 1)
		return false;
	else
		return true
}

function createPackingListFromExcel(bookingObject) {
	var packingList = bookingObject.pack_list;
	var packListArray = {};
	packingListObject = {};
	// packListArray=[];
	$.each(packingList[0], function(index, value) {
		if (index == 'Carton No' || index == 'Carton' || index == 'Color'
				|| index == 'Pcs/Cartoon' || index == 'Total Pcs')
			return true;
		else
			packListArray[index] = index;

	})
	var i = 1;
	$.each(packListArray, function(index, value) {

		packingListObject['size[' + i + ']_1'] = value;
		i++;
	})
	sizeArray.push(packingListObject);

	i = 2;
	$.each(packingList, function(index, value) {
		packingListObject = {};
		var j = 1;
		$.each(value, function(index, value) {
			if (index == 'Carton No') {
				packingListObject['serial-no_' + i] = value;
			} else if (index == 'Carton') {
				packingListObject['no-of-cartoon_' + i] = value;
			} else if (index == 'Color') {
				packingListObject['color_' + i] = value;

			} else if (index == 'Pcs/Cartoon') {
				packingListObject['pcs-crtn_' + i] = value;

			} else if (index == 'Total Pcs') {
				packingListObject['total-pcs_' + i] = value;

			} else {
				packingListObject['size[' + j + ']_' + i] = value;
				j++;
			}

		})
		sizeArray.push(packingListObject);
		i++;
	})

}

function handleDelete(element) {
	var td = $(element).parent();
	var tr = $(td).parent();
	$(tr).remove();
	requestObject.splice(element.id - 1, 1);
	for (var i = 0; i < requestObject.length; i++) {
		requestObject[i].itemId = i + 1;
	}
	$("#booking_table").html("");
	$.each(requestObject, function(index, value) {
		generateBookingTable(value);
	})
	itemId -= eval(1);
	if ($("#update-item") != undefined)
		$("#update-item").attr("daisabled", "disabled");

}

// Code for excell upload start here.//

var X = XLSX;
var XW = {
	/* worker message */
	msg : 'xlsx',
	/* worker scripts */
	rABS : './xlsxworker2.js',
	norABS : './xlsxworker1.js',
	noxfer : './xlsxworker.js'
};

var wtf_mode = false;
function fixdata(data) {
	var o = "", l = 0, w = 10240;
	for (; l < data.byteLength / w; ++l)
		o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w, l
				* w + w)));
	o += String.fromCharCode.apply(null, new Uint8Array(data.slice(l * w)));
	return o;
}

function to_json(workbook) {
	var result = {};
	var itemDetailCount = 0;
	var itemExtraCount = 0;
	workbook.SheetNames.forEach(function(sheetName) {
		var roa = XLSX.utils
				.sheet_to_row_object_array(workbook.Sheets[sheetName]);
		/*
		 * if(sheetName=='item_detail') itemDetailCount=roa.length;
		 * if(sheetName=='item_extra') itemExtraCount=roa.length;
		 */
		if (roa.length > 0) {
			result[sheetName] = roa;
		}
	});
	/*
	 * if(itemDetailCount!=itemExtraCount) { alert("No of records in item
	 * details and item extra should be same.Please upload complete excel
	 * sheet!"); return false; }
	 */
	return result;
}

function process_wb(wb) {
	var output = "";
	output = JSON.stringify(to_json(wb), 2, 2);

	alert(output);

	// hamid
	// var itemDetail = [];
	var discard;
	var discard2;
	var discard3;
	var discard4;
	var discard5;
	var itemDetail1 = to_json(wb).item_detail;
	for (var i = 0; i < itemDetail1.length; i++) {
		var itemDT = itemDetail1[i];
		var itemDetailObj = parseItemDetails(itemDT);
		if (itemDetailObj.MaterialNo && itemDetailObj.PONumber
				&& itemDetailObj.GrossWeight && itemDetailObj.TotalCarton
				&& itemDetailObj.TotalPcs && itemDetailObj.HSCode) {

			if (itemDetailObj.TotalCBM) {
				if (itemDetailObj.TotalCBM > 0) {

				} else {
					var line = i + 2;
					discard2 = true;
					break;
				}
			} else if (itemDetailObj.CartonLength && itemDetailObj.CartonWidth
					&& itemDetailObj.CartonHeight
					&& itemDetailObj.CartonMeasurementUnit) {
				if (itemDetailObj.CartonLength > 0
						&& itemDetailObj.CartonWidth > 0
						&& itemDetailObj.CartonHeight > 0
						&& itemDetailObj.TotalCarton > 0) {

				} else {
					var line = i + 2;
					discard5 = true;
					break;
				}

				if (itemDetailObj.CartonMeasurementUnit.toUpperCase() == 'IN'
						|| itemDetailObj.CartonMeasurementUnit.toUpperCase() == 'CM') {

				} else {
					var line = i + 2;
					discard3 = true;
					break;
				}

				if (isNaN(itemDetailObj.CartonLength)
						|| isNaN(itemDetailObj.CartonWidth)
						|| isNaN(itemDetailObj.CartonHeight)) {
					var line = i + 2;
					discard4 = true;
					break;
				}

			} else {
				var line = i + 2;
				discard = true;
				break;

			}
		}

		else {
			var line = i + 2;
			// alert("Data Not Found in Line Number "+line);
			discard = true;
			break;
		}
	}
	if (discard)
		alert("Mandatory Field(s) Missing in Line Number " + line
				+ " , Please Try Again");
	else if (discard2)
		alert("TotalCBM can not be 0. Please put a greater value");
	else if (discard3)
		alert("Please provide correct CartonMeasurementUnit.");
	else if (discard4)
		alert("Please provide correct Carton Measurement Values (Length, Height, Width).");
	else if (discard5)
		alert("Carton Measurement Values (Length, Height, Width) can not be 0. Please provide greater value/Provide Total CBM");
	else
		createBookingObjFromExcel(to_json(wb));
}

function generateBookingTableFromExcel(requestObject) {
	var html = "";
	$("#booking_table").html("");
	$
			.each(
					requestObject,
					function(index, value) {

						itemId += eval(1);
						html += "<tr>";
						html += "<td>";
						html += itemId;
						html += "</td>";
						html += "<td>";
						html += value.itemDetail[0].orderItem_poNumber;
						html += "</td>";
						html += "<td>";
						html += itemDetails[0].orderItem_commodity;
						html += "</td>";
						html += "<td>";
						html += value.itemDetail[0].orderItem_color;
						html += "</td>";
						html += "<td>";
						html += value.itemDetail[0].orderItem_productCode;
						html += "</td>";
						html += "<td>";
						html += value.itemDetail[0].orderItem_hsCode;
						html += "</td>";
						html += "<td>";
						html += value.itemDetail[0].orderItem_car_number;
						html += "</td>";
						html += "<td>";
						html += value.itemDetail[0].orderItem_totalPieces;
						html += "</td>";
						html += "<td>";
						html += value.itemDetail[0].orderItem_netWeight;
						html += "</td>";
						html += "<td>";
						html += value.itemDetail[0].orderItem_grossWeight;
						html += "</td>";
						html += "<td>";
						html += value.itemDetail[0].orderItem_total_cbm;
						html += "</td>";
						html += "<td>";
						html += '<input type="submit" id="'
								+ itemId
								+ '" class="btn btn-danger btn_btn remove_btn" value="Delete" onClick="handleDelete(this)"/>';
						html += "</td>";
						html += '<td>';
						html += '<input type="checkbox" id="' + itemId
								+ '" name="items"/>';
						html += '</td>';
						html += "</tr>";
						$("#booking_table").append(html);
						html = "";
					})
}
function createBookingObjFromExcel(bookingObject) {

	var itemDetail = [];
	var itemExtra = [];
	var itemRow = {};
	item = {}
	var itemDetail1 = bookingObject.item_detail;
	for (var i = 0; i < itemDetail1.length; i++) {
		var itemDT = itemDetail1[i];
		var itemDetailObj = parseItemDetails(itemDT);

		if (itemDetailObj.TotalCBM) {
			if (itemDetailObj.CartonLength && itemDetailObj.CartonWidth
					&& itemDetailObj.CartonHeight
					&& itemDetailObj.CartonMeasurementUnit) {
				if (itemDetailObj.CartonMeasurementUnit.toUpperCase() != 'IN'
						&& itemDetailObj.CartonMeasurementUnit.toUpperCase() != 'CM') {

					itemDetailObj.CartonLength = '';
					itemDetailObj.CartonWidth = '';
					itemDetailObj.CartonHeight = '';
					itemDetailObj.CartonMeasurementUnit = '';

				}

				else if (isNaN(itemDetailObj.CartonLength)
						|| isNaN(itemDetailObj.CartonWidth)
						|| isNaN(itemDetailObj.CartonHeight)
						|| itemDetailObj.CartonLength <= 0
						|| itemDetailObj.CartonWidth <= 0
						|| itemDetailObj.CartonHeight <= 0) {
					itemDetailObj.CartonLength = '';
					itemDetailObj.CartonWidth = '';
					itemDetailObj.CartonHeight = '';
					itemDetailObj.CartonMeasurementUnit = '';

				}

				else {

					if (itemDetailObj.CartonMeasurementUnit.toUpperCase() == 'IN') {
						var cbmD = ((itemDetailObj.CartonLength
								* itemDetailObj.CartonWidth * itemDetailObj.CartonHeight) / (1728 * 0.028317))
								* itemDetailObj.TotalCarton;
						var cbm = cbmD + '';
						itemDetailObj.TotalCBM = cbm;
					}
					if (itemDetailObj.CartonMeasurementUnit.toUpperCase() == 'CM') {
						var cbmD = ((itemDetailObj.CartonLength
								* itemDetailObj.CartonWidth * itemDetailObj.CartonHeight) / 1000000)
								* itemDetailObj.TotalCarton;
						var cbm = cbmD + '';
						itemDetailObj.TotalCBM = cbm;
						console.log(itemDetailObj.TotalCBM);
					}

				}

			} else {
				itemDetailObj.CartonLength = '';
				itemDetailObj.CartonWidth = '';
				itemDetailObj.CartonHeight = '';
				itemDetailObj.CartonMeasurementUnit = '';
			}

		}

		if (itemDetailObj.CartonLength && itemDetailObj.CartonWidth
				&& itemDetailObj.CartonHeight
				&& itemDetailObj.CartonMeasurementUnit) {

			if (itemDetailObj.CartonMeasurementUnit.toUpperCase() == 'IN') {
				var cbmD = ((itemDetailObj.CartonLength
						* itemDetailObj.CartonWidth * itemDetailObj.CartonHeight) / 61024)
						* itemDetailObj.TotalCarton;
				var cbm = cbmD + '';
				itemDetailObj.TotalCBM = cbm;
			}
			if (itemDetailObj.CartonMeasurementUnit.toUpperCase() == 'CM') {
				var cbmD = ((itemDetailObj.CartonLength
						* itemDetailObj.CartonWidth * itemDetailObj.CartonHeight) / 1000000)
						* itemDetailObj.TotalCarton;
				var cbm = cbmD + '';
				itemDetailObj.TotalCBM = cbm;
				console.log(itemDetailObj.TotalCBM);
			}

		}
		var itemExObj = parseItemExtra(itemDT);
		itemDetail.push(itemDetailObj);
		itemExtra.push(itemExObj);

		// itemRow["itemId"] = i + 1;
		itemId += eval(1);
		itemRow["itemId"] = itemId;

		itemRow["itemDetail"] = itemDetail;
		itemRow["itemExtra"] = itemExtra;
		// mainItemRow = itemRow;
		requestObject.push(itemRow);
		generateBookingTable(itemRow);
		// itemId += eval(1);
		itemDetail = [];
		itemExtra = [];
		itemRow = {};
	}
	console.log(JSON.stringify(requestObject));
	createPackingListFromExcel(bookingObject);
}
function parseItemDetails(itemDT) {
	var itemDetail = {};
	$.each(itemDT, function(index, value) {
		if (!(index.startsWith("Line_")))
			itemDetail[index] = value;
	})
	return itemDetail;
}

function parseItemExtra(itemDT) {
	var itemDetail = {};
	$.each(itemDT, function(index, value) {
		if (index.startsWith("Line_"))
			itemDetail[index] = value;
	})
	return itemDetail;
}

function handleFile(files) {
	/* var files = e.target.files; */
	var f = files[0];
	{
		var reader = new FileReader();
		var name = f.name;
		reader.onload = function(e) {
			// if(typeof console !== 'undefined') console.log("onload", new
			// Date(), rABS, use_worker);
			var data = e.target.result;
			var wb;
			var arr = fixdata(data);
			wb = X.read(btoa(arr), {
				type : 'base64'
			});
			process_wb(wb);
		};
		// if(rABS) reader.readAsBinaryString(f);
		reader.readAsArrayBuffer(f);
	}
}

function handleEdit() {

	// currentEditingItem = element.id;
	currentEditingItem = $("#booking_table").find("input:checked");
	var object = requestObject[currentEditingItem[0].id - 1];
	var itemDetail = object.itemDetail;
	var itemExtra = object.itemExtra;
	$("#itemdetail").find("input").each(function(index, element) {
		$(element).val(getValueByKey(itemDetail[0], element.id));
	})
	$("#itemdetail").find("textarea").each(function(index, element) {
		$(element).text(getValueByKey(itemDetail[0], element.id));
	})
	$("#second-tab").find("input").each(function(index, element) {
		$(element).val(getValueByKey(itemExtra[0], element.id));
	})
	$("#second-tab").find("textarea").each(function(index, element) {
		$(element).text(getValueByKey(itemExtra[0], element.id));
	})
	/*
	 * $('#orderItem_commodity').val(getValueByKey(itemDetail[0],'orderItem_commodity'));
	 * var value=$('#orderItem_commodity').val(); var
	 * text=$('#orderItem_commodity option[value="'+value+'"]').text();
	 * $('button[data-id="orderItem_commodity"]').find('span[class="filter-option
	 * pull-left"]').text(text);
	 */

	$('#ItemUoM').val(getValueByKey(itemDetail[0], 'ItemUoM'));
	var value = $('#ItemUoM').val();
	var text = $('#ItemUoM option[value="' + value + '"]').text();
	$('button[data-id="ItemUoM"]')
			.find('span[class="filter-option pull-left"]').text(text);

	$("#itemId").val(currentEditingItem);
	$("#add-item").attr("disabled", "disabled");
	$("#edit-item").attr("disabled", "disabled");
	$("#add-item").attr("disabled", "disabled");
	$("#cancel-update").removeAttr("disabled");
	$("#btn-submit").attr("disabled", "disabled");
	$("#btn-upload").attr("disabled", "disabled");

}
function getValueByKey(object, keyName) {
	var returnValue;
	$.each(object, function(key, value) {
		if (key == keyName) {
			returnValue = value;
			return false;
		}
	})
	return returnValue;
}

function bookingUpdate() {
	var object = requestObject[currentEditingItem[0].id - 1];
	var itemDetail = object.itemDetail;
	var itemExtra = object.itemExtra;
	var itemDetailObj = {};
	var itemExtraObj = {};
	$("#itemdetail").find('input[type="text"]').each(function(index, element) {
		itemDetailObj[element.id] = $(element).val();
	})
	$("#itemdetail").find("textarea").each(function(index, element) {
		itemDetailObj[element.id] = $(element).text();
	})
	$("#itemdetail").find('select').each(function(index, element) {
		itemDetailObj[element.id] = $(element).val();
	})

	$("#second-tab").find("input").each(function(index, element) {
		itemExtraObj[element.id] = $(element).val();
	})
	$("#second-tab").find("textarea").each(function(index, element) {
		itemExtraObj[element.id] = $(element).text();
	})

	itemDetail.splice(0, 1);
	itemExtra.splice(0, 1);
	itemDetail.push(itemDetailObj);
	itemExtra.push(itemExtraObj);
	object['itemDetail'] = itemDetail;
	object['itemExtra'] = itemExtra;
	object['itemId'] = currentEditingItem[0].id;
	requestObject.splice(currentEditingItem[0].id - 1, 1, object);
	// requestObject[currentEditingItem[0].id - 1]=object;
	console.log(JSON.stringify(requestObject));
	// generateBookingTableFromExcel(requestObject);
	$("#booking_table").html("");
	$.each(requestObject, function(index, value) {
		generateBookingTable(value);
	})
	// generateBookingTable

	$("#add-item").removeAttr("disabled");
	$("#edit-item").removeAttr("disabled");
	$("#add-item").removeAttr("disabled");
	$("#cancel-update").removeAttr("disabled");
	$("#btn-submit").removeAttr("disabled");
	$("#btn-upload").removeAttr("disabled");
}

function fillMaterial(element) {
	if (element.value.length > 0) {
		// var $overlay=$.LoadingOverlay("show");
		$.ajax({
			url : myContextPath + '/getMaterials',
			data : {
				param : element.value
			},
			dataType : "json"

		}).done(function(data) {
			$(element).autocomplete({

				source : function(request, response) {

					response(data);
				}
			})
			// var $overlay=$.LoadingOverlay("hide");
		}).fail(function(data) {
			// var $overlay=$.LoadingOverlay("hide");
		})
	}
}

function cbmFieldFunction() {
	var TotalCbm = document.getElementById("TotalCBM");
	var width = document.getElementById("CartonWidth").value;
	var height = document.getElementById("CartonHeight").value;
	var length = document.getElementById("CartonLength").value;
	var quantity = document.getElementById("TotalCarton").value;

	if (width <= 0 || height <= 0 || length <= 0 || quantity <= 0) {
		TotalCbm.disabled = false;
	} else if (width != "" && height != "" && length != "" && quantity != "") {
		TotalCbm.disabled = true;
	} else {
		TotalCbm.disabled = false;
	}
}

function foo() {
	var selObj = window.getSelection();
	alert(selObj);
	var selRange = selObj.getRangeAt(0);
	// do stuff with the range
}
function sortWHCodeList() {
	console.log("hit");
	var planningMarketList = document.getElementById("Style");
	var planningMarket = planningMarketList.options[planningMarketList.selectedIndex].value;
	var codeList = [];
	var warehouseCode;
	var code;
	var oldList = document.getElementById("ReferenceNo1");
	oldList.options.length = 0;
	oldList.options[oldList.options.length] = new Option('Please Select', '');
	for (var i = 0, size = warehouseList.length; i < size; i++) {
		warehouseCode = warehouseList[i].pm;
		code = warehouseList[i].whCode;
		var whpm = warehouseList[i];
		if (warehouseCode == planningMarket) {
			codeList.push(whpm);
			oldList.options[oldList.options.length] = new Option(code, code);
		}
	}

	/*
	 * whList = codeList;
	 * 
	 * oldList.options[oldList.options.length] = new Option('Text 1', 'Value1');
	 */
	/*
	 * var oldList = document.getElementById("ReferenceNo1").value; var
	 * objOption = document.createElement("option"); objOption.text = "12345";
	 * objOption.value = 1; oldList.add(objOption);
	 */

}