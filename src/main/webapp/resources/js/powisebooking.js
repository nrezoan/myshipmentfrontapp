function check() {
	if($('input[name=checking]:checked').length == 0) {
		alert("Please Select atleast one Purchase Order(PO)");
		return false;
	}
	var checkboxes = document.getElementsByName("checking");
	var checkboxesChecked = [];
	var poDetails = [];
	// loop over them all
	for (var i = 0; i < checkboxes.length; i++) {
		// And stick the checked ones onto an array...
		if (checkboxes[i].checked) {

			var id = checkboxes[i].id;
			var count = id.substring(id.indexOf("_") + 1);
			checkboxesChecked.push(checkboxes[i]);
			var e = document.getElementById("unit_" + count);
			var unit = e.options[e.selectedIndex].value;
			var po_no = document.getElementById("po_no_" + count).value;

			var index = -1;
			var filteredObj = modelArray.find(function(item, i) {
				if (item.po_no === po_no) {
					index = i;
					return i;
				}
			});

			model = {
				division : modelArray[index].division,
				pol : modelArray[index].pol,
				pod : modelArray[index].pod,
				place_of_delivery : modelArray[index].place_of_delivery,
				place_of_delivery_address : modelArray[index].place_of_delivery_address,
				cargo_handover_date : modelArray[index].cargo_handover_date,
				comm_inv : modelArray[index].comm_inv,
				comm_inv_date : modelArray[index].comm_inv_date,
				freight_mode: modelArray[index].freight_mode,
				terms_of_shipment: modelArray[index].terms_of_shipment,
				buyer_id: modelArray[index].buyer_id,
				fvsl: modelArray[index].fvsl,
				mvsl1: modelArray[index].mvsl1,
				mvsl2: modelArray[index].mvsl2,
				mvsl3: modelArray[index].mvsl3,
				voyage1: modelArray[index].voyage1,
				voyage2: modelArray[index].voyage2,
				voyage3: modelArray[index].voyage3,
				etd: modelArray[index].etd,
				eta: modelArray[index].eta,
				ata: modelArray[index].ata,
				atd: modelArray[index].atd,
				transhipment1etd: modelArray[index].transhipment1etd,
				transhipment2etd: modelArray[index].transhipment2etd,
				transhipment1eta: modelArray[index].transhipment1eta,
				transhipment2eta: modelArray[index].transhipment2eta,
				app_id : document.getElementById("app_id_" + count).value,
				po_no : document.getElementById("po_no_" + count).value,
				total_gw : document.getElementById("total_gw_" + count).value,
				carton_length : document.getElementById("carton_length_"
						+ count).value,
				carton_height : document.getElementById("carton_height_"
						+ count).value,
				carton_width : document.getElementById("carton_width_" + count).value,
				carton_quantity : document.getElementById("carton_quantity_"
						+ count).value,
				total_cbm : document.getElementById("total_cbm_" + count).value,
				total_pieces : document.getElementById("total_pieces_" + count).value,
				style_no : document.getElementById("style_no_" + count).value,
				color : document.getElementById("color_" + count).value,
				hs_code : document.getElementById("hs_code_" + count).value,
				unit : unit,
				size_no : document.getElementById("size_no_" + count).value,
				article_no : document.getElementById("article_no_" + count).value,
				department : document.getElementById("department_" + count).value,
				wh_code : document.getElementById("wh_code_" + count).value,
				sku : document.getElementById("sku_" + count).value,
				total_nw : document.getElementById("total_nw_" + count).value,
				carton_unit : document.getElementById("carton_unit_" + count).options[document
						.getElementById("carton_unit_" + count).selectedIndex].value,
				commodity : document.getElementById("commodity_" + count).options[document
						.getElementById("commodity_" + count).selectedIndex].value
			}


			console.log(model);
			poDetails.push(model);

		}
	}
	console.log(checkboxesChecked);
if (checkMandatoryForBooking(poDetails)) {
var data = JSON.stringify(poDetails);
	$.ajax({
        type: 'POST',
        url: myContextPath+ "/poBooking",
        data: data,
        contentType: "application/json",
		success : function (resultData, textStatus, xhr) {
			console.log("Successfully Sent");
			window.location = myContextPath + '/powiseheader';
			$.LoadingOverlay("hide");
		},
		error : function(jqXHR, textStatus, errorThrown) {
			$.LoadingOverlay("hide");
		}
/*        success: function (resultData, textStatus, xhr) {
            console.log("Successfully Sent");
            //  window.location = xhr.getResponseHeader("Location");
            if (xhr.status === 201) {
                $("#orderSaveSuccessModal").modal();
                var link = xhr.getResponseHeader("Location");
                var win = window.open(link, '_blank');
                win.focus();
            }
        },
        error: function (resultData, textStatus, xhr) {

            if (textStatus === "error" || resultData === 400) {
            	 document.getElementById("msgBody").innerHTML= "Order Can not Be created";
                $("#orderSaveUnsuccessModal").modal();
            }
        },

        failure: function (resultData) {
            console.log("Error Occured");
        }*/
    });
	// Return the array if it is non-empty, or null
	return checkboxesChecked.length > 0 ? checkboxesChecked : null;
	}
}

function calculateCBM(count) {
	if (checkMandatoryForCBM(count)) {
		try {
			var length = document.getElementById('carton_length_'+count).value;
			var width = document.getElementById('carton_width_'+count).value;
			var height = document.getElementById('carton_height_'+count).value;
			var unit = document.getElementById('carton_unit_'+count).value;
			var noOfCar = document.getElementById('carton_quantity_'+count).value;
			if (unit == 'IN') {
				var cbm = ((length * width * height) / 61024)
						* noOfCar;
			}
			if (unit == 'CM') {
				var cbm = ((length * width * height) / 1000000) * noOfCar;
			}

			cbmFieldFunction(count);
			document.getElementById('total_cbm_'+count).value = cbm.toFixed(3);	
		} catch (Exception) {
			document.getElementById('total_cbm_'+count).value = '';
		}
	} else {
		cbmFieldFunction(count);
		return false;
	}
}

function checkMandatoryForCBM(count) {
	if ($("#carton_length_" + count).val() == null
			|| $("#carton_length_" + count) == undefined
			|| $("#carton_length_" + count) == "") {
	/*	alert("Please fill mandatory fields ! ");
		$("#carton_length_" + count).focus();*/
		return false;
	} else if ($("#carton_quantity_" + count).val() == null
			|| $("#carton_quantity_" + count).val() == undefined
			|| $("#carton_quantity_" + count).val() == "") {
	/*	alert("Please fill mandatory fields ! ");
		$("#carton_quantity_" + count).focus();*/
		return false;
	} else if ($("#carton_height_" + count).val() == null
			|| $("#carton_height_" + count).val() == undefined
			|| $("#carton_height_" + count).val() == "") {
	/*	alert("Please fill mandatory fields ! ");
		$("#carton_height_" + count).focus();*/
		return false;
	} else if ($("#carton_width_" + count).val() == null
			|| $("#carton_width_" + count).val() == undefined
			|| $("#carton_width_" + count).val() == "") {
	/*	alert("Please fill mandatory fields ! ");
		$("#carton_width_" + count).focus();*/
		return false;
	}  else
		return true;

}

function cbmFieldFunction(count) {
	var TotalCbm = document.getElementById('total_cbm_'+count);
	var quantity = document.getElementById('carton_quantity_'+count).value;
	var width = document.getElementById('carton_width_'+count).value;
	var height = document.getElementById('carton_height_'+count).value;
	var length = document.getElementById('carton_length_'+count).value;
	var unit = document.getElementById('carton_unit_'+count).value;

	if (width <= 0 || height <= 0 || length <= 0 || quantity <= 0) {
		TotalCbm.disabled = false;
	} else if (width != "" && height != "" && length != "" && quantity != "" && unit!="") {
		TotalCbm.disabled = true;
	} else {
		TotalCbm.disabled = false;
	}
}

function checkMandatoryForBooking(poDetails){
	for(var i=0; i < poDetails.length; i++){
		if(poDetails[i].total_gw == null || poDetails[i].total_gw == undefined || poDetails[i].total_gw == ""){
			alert("Please fill mandatory fields ! ");
			/*$("#carton_length_" + count).focus();*/
			return false;
		}else if(poDetails[i].total_pieces == null || poDetails[i].total_pieces == undefined || poDetails[i].total_pieces == ""){
			alert("Please fill mandatory fields ! ");
			/*$("#carton_length_" + count).focus();*/
			return false;
		}else if(poDetails[i].hs_code == null || poDetails[i].hs_code == undefined || poDetails[i].hs_code == ""){
			alert("Please fill mandatory fields ! ");
			/*$("#carton_length_" + count).focus();*/
			return false;
		}else if(poDetails[i].carton_quantity == null || poDetails[i].carton_quantity == undefined || poDetails[i].carton_quantity == ""){
			alert("Please fill mandatory fields ! ");
			/*$("#carton_length_" + count).focus();*/
			return false;
		}else if(poDetails[i].carton_unit == null || poDetails[i].carton_unit == undefined || poDetails[i].carton_unit == ""){
			alert("Please fill mandatory fields ! ");
			/*$("#carton_length_" + count).focus();*/
			return false;
		}else if(poDetails[i].commodity == null || poDetails[i].commodity == undefined || poDetails[i].commodity == ""){
			alert("Please fill mandatory fields ! ");
			/*$("#carton_length_" + count).focus();*/
			return false;
		}else if(poDetails[i].total_cbm == null || poDetails[i].total_cbm == undefined || poDetails[i].total_cbm == ""){
			alert("Please fill mandatory fields ! ");
			/*$("#carton_length_" + count).focus();*/
			return false;
		}/*else{
			return true;
			}*/
		}
	
		return true;
	}