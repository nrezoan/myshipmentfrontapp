
function zoom()
 {
   document.body.style.zoom = "90%" ;
        
 }
	function drawChart(details){	
		var $overlay=$.LoadingOverlay("show");
		var dataArray=createDataArray(details.lstSupplierWiseCBMJson);
		$.getScript('resources/js/canvasjsmin.js',function(){
	$("#chartContainer").CanvasJSChart({
		
		backgroundColor: null,
		title: { 
			text: "",
			fontSize: 24
		}, 
		axisY: { 
			title: "Products in %" 
		}, 
		legend :{ 
			verticalAlign: "center", 
			horizontalAlign: "right" 
		},
        		
		data: [ 
		{ 
			type: "pie", 
			showInLegend: true, 
			toolTipContent: "{label} <br/> {y} %", 	
			indexLabel: "{y} %", 
			dataPoints:dataArray,
			click: function(e){ 
        	    //alert(  e.dataSeries.type+ " x:" + e.dataPoint.label + ", y: "+ e.dataPoint.y);
        	    
        	    var shipperDetails=e.dataPoint.label.split('-');
        	    var shipperNo=shipperDetails[shipperDetails.length-1];
        	    window.location.href=myContextPath+'/shipdetbuyer?shipperNo='+shipperNo;
        	  }
		} 
		] 
		
	}); 
	
	
})
var $overlay=$.LoadingOverlay("hide");

	}	
	function createDataArray(data)
	{
		var arrObj=[];
		var object={};
		$.each(data,function(index,value){
			object['label']=value.shipperName+'-'+value.shipperNo;
				object['legendText']=value.shipperName;
				object['y']=Number(value.totalCBMPerc.toFixed(2));
				object['cursor']='pointer';
			arrObj.push(object);
			object={};
		})
		return arrObj;
		}
