var colorCode = "#C0CCF0";
var contextAddress = "";
$(document).ready(function() {

	assigningRouteInformationToDefault();
	$("#file-upload-form").submit(function(event) {

		//checking whether the file is been uploaded or not
		if ( !$("#file-upload").val() ) {
			alert("Upload The Invoice First");
			event.preventDefault();
		}
		else {
			//shows the overlay
			$("#overlayHeader").css("display", "block");
			closeInvModal();
			event.preventDefault();
			uploadFile();
		}
	});
});

	function uploadFile() {

		var data = new FormData($("#file-upload-form")[0]);
		$.ajax({
			url : myContextPath + "/saveInvoice",
			type : "POST",
			data : new FormData($("#file-upload-form")[0]),
			enctype : 'multipart/form-data',
			processData : false,
			contentType : false,
			cache : false,
			success : function(data) {

				var object_whole_data = JSON.parse(data);
				console.log(object_whole_data[0].invoice_no_nazib__version);
				console.log(object_whole_data[0].invoice_checker);
				// checking out whether the uploaded document is actually an
				// invoice or not
				if ( object_whole_data[0].invoice_checker == 'INVOICE' ) {
					// checking whether the supplier is actually LIBAS OR NOT
					if ( object_whole_data[0].supplier == 'LIBAS TEXTILES LTD' ) {
						//this if block is to check for the buyer specific 
						if ( object_whole_data[0].buyer == 'H & M Hennes & Mauritz GBC AB' ) {
							// checking out if the order number in the packing list
							// and in the invoices matches or not
							if ( object_whole_data[0].orderidinpl == object_whole_data[0].orderidininvoice ) {

								$("#comInvNo").val(object_whole_data[0].invoice_no);
								// changing the color of manipulated
								// field to highlight it
								$("#comInvNo").css("background-color", colorCode);
								$("#disccriptonOfGoods").val(object_whole_data[0].description_of_goods);
								// changing the color of manipulated
								// fied to highlight it
								$("#disccriptonOfGoods").css("background-color", colorCode);
								$("#portOfLoading").val(object_whole_data[0].port_of_loading);
								// changing the color of manipulated
								// fied to highlight it
								$("#portOfLoading").css("background-color", colorCode);
								if ( object_whole_data[0].invoice_date.formatted ) {
									$("#lcomInvDate").val(object_whole_data[0].invoice_date.formatted);
								}

								// changing the color of manipulated
								// fied to highlight it
								$("#lcomInvDate").css("background-color", colorCode);
								$("#expNumber").val(object_whole_data[0].exp_number);
								// changing the color of manipulated
								// fied to highlight it
								$("#expNumber").css("background-color", colorCode);
								if ( object_whole_data[0].exp_date.formatted ) {
									$("#expdt").val(object_whole_data[0].exp_date.formatted);

								}
								// changing the color of manipulated
								// fied to highlight it
								$("#expdt").css("background-color", colorCode);
								$("#termOfShipment").val(object_whole_data[0].terms_of_delivery);
								// changing the color of manipulated
								// fied to highlight it
								$("#termOfShipment").css("background-color", colorCode);
								if ( object_whole_data[0].terms_of_delivery === "FCA" ) {
									$("#tosDes").val("DESTINATION");
									// changing the color of
									// manipulated fied to highlight
									// it
									$("#tosDes").css("background-color", colorCode);
									$("#freightMode").val("CC");
									// changing the color of
									// manipulated fied to highlight
									// it
									$("#freightMode").css("background-color", colorCode);
								}
								$("#lcTtPono").val(object_whole_data[0].lcsc_number);
								// changing the color of manipulated
								// fied to highlight it
								$("#lcTtPono").css("background-color", colorCode);
								if ( object_whole_data[0].lcttpodate.formatted ) {
									$("#lcttpodt").val(object_whole_data[0].lcttpodate.formatted);
								}

								// changing the color of manipulated
								// fied to highlight it
								$("#lcttpodt").css("background-color", colorCode);

								getRouteInformation(object_whole_data[0].final_destination, object_whole_data[0].orderidinpl);
								console.log(typeof data);
								var my_returned_string = $("#docParserJson").val(data);

								// removing the overlay after
								// unsuccessful parsing
								$("#overlayHeader").css("display", "none");
								// sweet alert notification for blue
								// mark
								swal("Document Checked!", "Extracted Information are Highlighted in Blue", "success");
								// here we are setting the form
								// redirection to the special page
								// for parsed line item
								var routeSuccess = contextAddress;
								routeSuccess += "/docparserorderdetails";
								$("#frm-direct-booking").attr("action", routeSuccess);
								console.log($("#frm-direct-booking").attr("action"));
							}
							// if blocks ends checking the order number
							else {
								// removing the overlay after
								// unsuccessful parsing
								$("#overlayHeader").css("display", "none");
								swal("Unable To Check Document!", "Order Number In The Invoice and Packing List Did Not Match", "error");
								// here we are setting the form
								// redirection to the normal/manual
								// page for parsed line item
								assigningRouteToManual();
							}
						}//if block ends for buyer check
						else {
							// removing the overlay after
							// unsuccessful parsing
							$("#overlayHeader").css("display", "none");
							swal("Unable To Check Document!", "The Uploaded Document Is Not For \"H & M\"", "error");
							// here we are setting the form
							// redirection to the normal/manual
							// page for parsed line item
							assigningRouteToManual();
						}

					}
					//if blocks ends for the supplier checking block
					else {
						// removing the overlay after
						// unsuccessful parsing
						$("#overlayHeader").css("display", "none");
						swal("Unable To Check Document!", "The Uploaded Document Is Not From Libas", "error");
						// here we are setting the form
						// redirection to the normal/manual
						// page for parsed line item
						assigningRouteToManual();
					}

				}// if blocks ends here for invoice
				// checking
				else {
					// removing the overlay after
					// unsuccessful parsing
					$("#overlayHeader").css("display", "none");
					swal("Unable To Check Document!", "Wrong Document Inserted", "error");
					// here we are setting the form
					// redirection to the normal/manual
					// page for parsed line item
					assigningRouteToManual();
				}

			},
			error : function(e) {

				// Handle upload error
				// ...
				console.log(e);
				$("#overlayHeader").css("display", "none");
				swal("Unable To Check Document!", "Wrong Document Inserted", "error");
			}

		});
	}
	// this function will take the destination and call another
	// rest api to get other values as json response
	// which will be then set to the front end
	function getRouteInformation(destination, orderNumber) {

		var tempDes = destination;
		var tempOrderNumber = orderNumber;

		$.ajax({
			type : "POST",
			url : myContextPath + "/routeinfo",
			data : {
				destination : tempDes,
				orderNumber : tempOrderNumber
			},
			timeout : 100000,
			success : function(data) {

				var checking = JSON.stringify(data);
				var object_whole_data = JSON.parse(checking);
				console.log(object_whole_data);
				// this portion of code populates data
				// depending on the text of the selects
				// options
				var selectedText = object_whole_data.placeOfReciept;
				$("#placeOfReceipt option").filter(function() {

					return this.text.toUpperCase() == selectedText.toUpperCase();
				}).attr('selected', true).change();
				$("#portOfLoading").val(object_whole_data.portOfLoading);
				// changing the color of manipulated
				// fied to highlight it
				$("#portOfLoading").css("background-color", colorCode);
				$("#portOfDischarge").val(object_whole_data.portOfDischarge);
				// changing the color of manipulated
				// fied to highlight it
				$("#portOfDischarge").css("background-color", colorCode);
				$("#plcOfDel").val(object_whole_data.placeOfDelivery);
				// changing the color of manipulated
				// fied to highlight it
				$("#plcOfDel").css("background-color", colorCode);
				$("#placeOfDel").val(object_whole_data.placeOfDeliveryAddress);
				// changing the color of manipulated
				// fied to highlight it
				$("#placeOfDel").css("background-color", colorCode);

			},
			error : function(e) {

				// Handle upload error
				// ...
				console.log(e);
			}

		});
	}



// appending the action text of the form so that it will be redirected to the
// normal line item page
function assigningRouteInformationToDefault() {

	contextAddress = $("#frm-direct-booking").attr("action");

}

function assigningRouteToManual() {

	var routeInfo = contextAddress;
	routeInfo += "/orderdetails";
	$("#frm-direct-booking").attr("action", routeInfo);
	console.log($("#frm-direct-booking").attr("action"));
}

// variables for invoice modal Start
function closeInvModal() {

	console.log("Working");
	document.getElementById('invoice_myModal').style.display = "none";
	assigningRouteToManual();

}
// variables for invoice modal Ends

window.onclick = function(event) {

	var modal = document.getElementById('invoice_myModal');
	if ( event.target == modal ) {
		modal.style.display = "none";
		assigningRouteToManual();
	}
}

// variables for invoice ends here

