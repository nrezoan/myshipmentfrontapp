package com.myshipment.util;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPathFactory;

public class SessionUtil {
	public static final String LOGIN_DETAILS="loginDetails" ;
	public static final String SUPPLIER_PRE_LOAD_DATA="supplierPreLoadDetail";
	public static final String DIRECT_BOOKING_PARAMS="directBookingParams";
	public static final String FREQUENT_MENU_LIST="frequentMenu";
	public static final String BOOKING_UPDATE_ATTRIBUTE="bookingupdate";
	public static final String DASHBOARD_INFO_DETAILS="dashboardInfoDetails";
	public static HttpSession getHttpSession(HttpServletRequest request)
	{
		HttpSession session=null;
		if(null!=request)
		{
			session=request.getSession(false);
		}
		return session;
	}
	public static HttpSession createSession(HttpServletRequest servletRequest)
	{
		HttpSession httpSession=null;
		if(servletRequest!=null)
		{
			httpSession=getHttpSession(servletRequest);
			if(httpSession==null)
			{
			httpSession=servletRequest.getSession(true);
			
			//httpSession.setMaxInactiveInterval(600);
			}
		}
		return httpSession;
	}
	

}
