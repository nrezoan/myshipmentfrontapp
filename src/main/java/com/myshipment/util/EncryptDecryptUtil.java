/**
*
*@author Ahmad Naquib
*/
package com.myshipment.util;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;

public class EncryptDecryptUtil {
	private String KEY = "Bar12345Bar12345"; // 128 bit key
	private String INIT_VECTOR = "RandomInitVector"; // 16 bytes IV
	
    public String encrypt(String value) {
        try {
            IvParameterSpec iv = new IvParameterSpec(INIT_VECTOR.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(KEY.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);

            byte[] encrypted = cipher.doFinal(value.getBytes());
            /*System.out.println("encrypted string: "
                    + Base64.encodeBase64String(encrypted));*/

            return Base64.encodeBase64String(encrypted);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }

    public String decrypt(String encrypted) {
        try {
            IvParameterSpec iv = new IvParameterSpec(INIT_VECTOR.getBytes("UTF-8"));
            SecretKeySpec skeySpec = new SecretKeySpec(KEY.getBytes("UTF-8"), "AES");

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
            cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);

            byte[] original = cipher.doFinal(Base64.decodeBase64(encrypted));

            return new String(original);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        return null;
    }
    
    

    public String getKEY() {
		return KEY;
	}

	public void setKEY(String kEY) {
		KEY = kEY;
	}

	public String getINIT_VECTOR() {
		return INIT_VECTOR;
	}

	public void setINIT_VECTOR(String iNIT_VECTOR) {
		INIT_VECTOR = iNIT_VECTOR;
	}

	public static void main(String[] args) { 
        
		EncryptDecryptUtil encryptDecrypt = new EncryptDecryptUtil();
		
        String encrypt = encryptDecrypt.encrypt("1212%-asd");
        System.out.println("Encrypt : " + encrypt);
        String decrypt = encryptDecrypt.decrypt(encrypt);
        System.out.println("Decrypt : " + decrypt);
        //System.out.println(decrypt(key, initVector, encrypt(key, initVector, "Hello World")));
    }
}
