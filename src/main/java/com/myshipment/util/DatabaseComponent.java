package com.myshipment.util;
import java.sql.*;
import java.util.*;
import java.io.*;


	/***********************Program Header********************************
	 * DatabaseComponent is a class which has functions similar to SQL queries
	 *
	 * It has got all the fuction corresponding to INSERT,UPDATE,DELETE,SELECT
	 *
	 * @Classfile name    DatabaseComponent.java
	 * @author 						Swapnil Bhagwat
	 * @modified					Satish Kumar.G.S
	 * @modified Summary	insert method made to single and changed the input parameter
	 * 										to Object LinkedList.
	 * @reviewed by
	 * @version	      		V 1.0
	 * @since             JDK1.2
	 *
	 **********************************************************************/

	
	public class DatabaseComponent  {

	Connection con;
	Statement stmt;
	PreparedStatement pstmt;
	ResultSet rs;
	ResultSetMetaData rsmd;

	/*************************************************************************
	*overloading default constructor
	*It makes new instant for ReadyConn class and gets Connection to database
	*by calling getConnection method of ReadyConn class
	*/

	public DatabaseComponent() throws Exception {


	    DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
	    //con= DriverManager.getConnection("jdbc:oracle:thin:@192.168.1.1:1521:MGHGROUP","sales","sales");
	    //con= DriverManager.getConnection("jdbc:oracle:thin:@202.65.11.56:1521:MGHGROUP","mondial","mondial");
	   // con= DriverManager.getConnection("jdbc:oracle:thin:@192.168.7.77:1521:XE","myshipment","myshipment");
	    con= DriverManager.getConnection("jdbc:oracle:thin:@121.200.242.138:1521:XE","myshipment","myshipment");
	    stmt=con.createStatement();

	    }


	/****************************************************************************
	 * insert(String[]tablenames,String[]columnnames,String[]values) can be used by
	 *  calling programme for inserting values in multiple tables .
	 * The method is calling setAutoComit(false) method of Connection class.
	 * This method call maintains the executed query in rollback segment
	 * <p>
	 * @param						LinkedList Having first element Vector( having list of tables
	 *									in arraged order) and second element and later on elements
	 *									will be Hashtable having key(column name) and values(column values)
	 *									first Hashtable corrosponds to first element of vector(table)
	 * @return          String
	 * @since           JDK1.2
	 *
	 *
	 **************************************************************************/
	public String insert(String qry) throws Exception {
		try{
			stmt.executeUpdate(qry);
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return "successful";
	}

	public String insert(LinkedList ll) throws Exception
	{
	   			String tablename="";
					String columns="";
					String values="";
					String query="";
					String columnvalue="";

		//			con.setAutoCommit(false); // creating rollback segment for database
					for(int j=1;j<ll.size();j++)
					{
					Vector v1=(Vector)ll.get(0);
					Vector v2=new Vector();
					tablename=v1.elementAt(j-1).toString();
					Hashtable ht=(Hashtable)ll.get(j);
					Enumeration e=ht.keys();
					query="insert into ".concat(tablename).concat(" (");
							for(;e.hasMoreElements();)
						{
							String columname=(String)e.nextElement();
							columns=columns.concat(columname).concat(",");
							columnvalue=(String)ht.get(columname);
							values=values.concat("?,");
							v2.addElement(columnvalue);
						}

					query=query.concat(columns.substring(0,columns.length()-1)).concat(") ").concat("values (").concat(values.substring(0,values.length()-1)).concat(")");
		//			System.out.println("query"+query);
					pstmt=con.prepareStatement(query);

					for(int i=0;i<v2.size();i++)
					pstmt.setString(i+1,v2.elementAt(i).toString());
					//System.out.println(con);
					pstmt.executeUpdate();
					v2.removeAllElements();
					columns="";
					values="";
					query="";
					}

		//con.commit();
		return "successful";
		}

	/***********************************************************************
	 * The fuction is used by calling program to retrieve values from database
	 * @param		String
	 * @return		ResultSet
	 * @see			java.sql.ResultSet
	 *
	 **************************************************************************/

	public ResultSet retrieve(String retrievestring) throws Exception{
	//try{
		rs=stmt.executeQuery(retrievestring);
	//   }catch(Exception sqle){System.out.println(sqle);}
	   return rs;
	}

	/***********************************************************************
	 * The fuction is used by calling program to UPDATE database
	 * @param		String tablename		table name to be updated
	 * @param		String setcolumnname 	String containing "," separated column names
	 * @param		String setcolumnvalues	String containing "," separated values of columns to set
	 * @param		String conditioncolumnnames		String containing "," separated column names
	 * @param		String conditionalcolumnvalues	String containing "," separated column values
	 * @return		void
	 * @see			java.sql.Statement.executeUpdate()
	 *
	 **************************************************************************/

	public void update(String tablename,String setcolumnname,String setcolumnvalue,String conditioncolumnname,String conditioncolumnvalue)
		{
		String update="";
		if (tablename!=null){
		try{
		con.setAutoCommit(false); // creating rollback segment for database
		update="update "+tablename+" set "+setcolumnname+"='"+setcolumnvalue+"' where "+conditioncolumnname+"='"+conditioncolumnvalue+"'";
		System.out.println(update);
		int count=stmt.executeUpdate(update);
		if(count==0)
			con.commit();
		}catch(SQLException sql){
			System.out.println("sql "+sql);
			try{
			con.rollback();
			}catch(Exception er){System.out.println("er "+er);}

		}}

		}


	/*******************************************************************************
	 * updates the database using single query
	 * @param		String update		sql to update the database
	 * @return		void
	 *********************************************************************************/


	public void update(String update) throws Exception{
		if (update!=null)
		{

		try{
			//con.setAutoCommit(false);
			int count = stmt.executeUpdate(update);
		    //if(count==1)
			//con.commit();
		}catch(Exception ex){
			ex.printStackTrace();
		}
			//System.out.println("Error: "+sql);
		//	 try{
			 // con.rollback();
		//	 }catch(Exception er){System.out.println("Error in Rollback: "+er);}
		//     }
		}
		}


	/**************************************************************************
	 * Deletes the record from database with given conditions
	 * @param		String tablename				Name of table
	 * @param		String Conditionalcolumnname	name of the column in condition
	 * @param		String conditionalcolumnvalue	value of column in condition
	 * @return		void
	 * @see			java.sql.Statement.executeUpdate()
	 *
	 *******************************************************************************/

	public void delete(String tablename,String conditioncolumnname,String conditioncolumnvalue)
	{
		String delete="";
		if (tablename!=null)
		{
		try{
		con.setAutoCommit(false);
		delete="delete from "+tablename+" where "+conditioncolumnname+"='"+conditioncolumnvalue+"'";
		System.out.println(delete);
		int count=stmt.executeUpdate(delete);
		if(count==1)
		con.commit();
		}catch(SQLException sql){
		  System.out.println(sql);
		  try{
		  con.rollback();
		  }catch(Exception er){
			  System.out.println("Error in Rollback: "+er);
		  }
		}
		}
		}

	/*********************************************************************************
	 * deletes the record from database with given query
	 * @param		String delete
	 * @return		void
	 * @see			java.sql.Statement.executeUpdate()
	 *
	 *
	 ********************************************************************************/
	public void delete(String delete){
		if (delete!=null)
		{
		try{
		System.out.println(delete);
		stmt.executeUpdate(delete);
		}catch(SQLException sql){System.out.println(sql);}
		}
		}

	/*****************************************************************************
	 * The fuction is used to separate the String with delimiters
	 *
	 * @param	String value		String containing the delimiters
	 * @param	String token		The token used as delimiter (@,!,~,^,....	)
	 * @return	String []			An array if tokenised string
	 * @see		java.util.StringTokenizer
	 ****************************************************************************/

	public String[] tokenizeString(String value,String token)
		{
		StringTokenizer setcolname=new StringTokenizer(value,token);
		String [] setcolnames=new String[setcolname.countTokens()];
		int i=0;
		while(setcolname.hasMoreTokens()){
		setcolnames[i++]=setcolname.nextToken();
		}
		return setcolnames;
		}

		public void setAutoCommit() throws Exception
		{
	con.setAutoCommit(false);
		}

	public void commitTrans() throws Exception
		{
			con.commit();
		}
	public void rollbackTRans() throws Exception
		{
			con.rollback();
		}
	public String executeProcedures(String procName)
		{
			try{
			CallableStatement cs=con.prepareCall("{call repadmin.comm_work}");
			cs.execute();
			return "success";
			}catch(Exception e){return e.toString();}
		}

	    public String executeP(String a1,String a2,String a3,String a4,String a5,String a6,String a7,String a8,String a9,String a10,String a11,String a12,String a13)
		//public  String executeP()

		 {
			try
						{
						    CallableStatement proc = con.prepareCall("{ call PROC_PO_REPORT(?,?,?,?,?,?,?,?,?,?,?,?,?) }");

		/*				    proc.setString(1, a1);
							proc.setString(2, "79");
							proc.setString(3, "D");
							proc.setString(4, null);
							proc.setString(5, "01-Apr-05");
							proc.setString(6, "31-May-05");
							proc.setString(7, "ETD_ORIGIN");
							proc.setString(8, "01-Apr-05");
							proc.setString(9, "31-May-05");
							proc.setString(10, "01-Apr-05");
							proc.setString(11, "31-May-05");
							proc.setString(12, "ET_ORIGIN");
							proc.setString(13, "TEST.XL");

	*/

					    proc.setString(1, a1);
							proc.setString(2, a2);
							proc.setString(3, a3);
							proc.setString(4, a4);
							proc.setString(5, a5);
							proc.setString(6, a6);
							proc.setString(7, a7);
							proc.setString(8, a8);
							proc.setString(9, a9);
							proc.setString(10, a10);
							proc.setString(11, a11);
							proc.setString(12, a12);
							proc.setString(13, a13);
						    proc.execute();
						    return "success";

			}
			catch (SQLException e)
			{
			    return "failure"+e;
		    }

		}

	public void closeConn() throws Exception
		{
			con.close();
		}


	public Connection getCon() {
		return con;
	}


	public void setCon(Connection con) {
		this.con = con;
	}
	

	/*
	public static void main(String args[])
	  {

	       try{
	       DatabaseComponent db = new DatabaseComponent();
	       ResultSet rs = db.retrieve("select sysdate s from dual");
	       if(rs.next())
	        {
			System.out.println(rs.getString(1));
			}
		}
			catch(Exception e)
			 {
			 System.out.println(e);
			 }

	  } */

	}


