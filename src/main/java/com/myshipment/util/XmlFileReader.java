/*package com.myshipment.util;

import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;

import com.myshipment.controller.XMLParserHandler;
import com.myshipment.model.POFileInfo;
import com.myshipment.service.IPOFileReaderService;
import com.myshipment.service.IXMLFileReaderService;
import com.myshipment.xmlpobean.CarfourPOXMLBean;


 * @Ranjeet Kumar
 
public class XmlFileReader {

	private final static Logger logger = Logger.getLogger(XmlFileReader.class);

	@Autowired
	private IPOFileReaderService iPOFileReaderService;
	@Autowired
	private IXMLFileReaderService iXMLFileReaderService;

	@Scheduled(fixedDelay = 5000*12*5, initialDelay = 5000*12*2)
	@Transactional
	public void saveData(){

		logger.info("Method XmlFileReader.saveData starts.");
		try{

			DirectoryStream<Path> ds = Files.newDirectoryStream(FileSystems.getDefault().getPath(CommonConstant.PO_UPLOAD_FILE_LOCATION));
			logger.info("File Directory : " + ds);

			//It stores the filename and file data that need to store in the database.
			List<POFileInfo> fileDataLst = new ArrayList<POFileInfo>();
			//Every CarfourPOXMLBean contains complete data of an XML file.
			List<CarfourPOXMLBean> carfourPOXMLBeanLst = new ArrayList<CarfourPOXMLBean>();

			for(Path path : ds){		
				if(!Files.isHidden(path)){						
					if(!Files.isDirectory(path)){						
						//First we check for the duplicate file.
						POFileInfo poFileInfo = iPOFileReaderService.checkForDuplicateFile(path.getFileName().toString());
						if(poFileInfo == null){						
							String contents = new String(Files.readAllBytes(Paths.get(path.toUri())));
							//Preparing the file related info to save to database.
							POFileInfo fileInfo = prepareTheFileData(path, contents);
							fileDataLst.add(fileInfo);
							//Parsing the XML and preparing an object.
							CarfourPOXMLBean carfourPOXMLBean = getParsedXMLObject(path);
							carfourPOXMLBeanLst.add(carfourPOXMLBean);
						}
					}
				}
			}
			//This method saves the xml files as a whole in the database.
			iPOFileReaderService.processTheFileData(fileDataLst);
			//This method parses the xml and preapre the line item POs and save it to database.
			iXMLFileReaderService.processXMLData(carfourPOXMLBeanLst);

			logger.info("Method XmlFileReader.saveData ends.");
		}catch(Exception ex){
			logger.debug("Exception occurred in the method XmlFileReader.saveData :"+ex);
			ex.printStackTrace();
		}

	}

	private POFileInfo prepareTheFileData(Path path, String contents){

		POFileInfo poFileInfo = null;

		if(path != null && (contents != null && !contents.isEmpty())){
			String directory = path.getParent().toString();
			String fileName = path.getFileName().toString();
			String completePath = path.toString();

			poFileInfo = new POFileInfo();
			poFileInfo.setCompleteFilePath(completePath);
			poFileInfo.setFileDirectory(directory);
			poFileInfo.setFileName(fileName);
			poFileInfo.setFileContent(contents);
			poFileInfo.setCreated_on(DateUtil.getCurrentDateAsString());

		}

		return poFileInfo;
	}

	private CarfourPOXMLBean getParsedXMLObject(Path path){

		SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
		CarfourPOXMLBean parsedBean = null;
		try{
			SAXParser saxParser = saxParserFactory.newSAXParser();
			XMLParserHandler handler = new XMLParserHandler();
			saxParser.parse(Files.newInputStream(path), handler);
			parsedBean = handler.getParsedXMLObject();

		}catch(Exception ex){
			ex.printStackTrace();
		}
		return parsedBean;
	}

		
	private List<SegPurchaseOrder> prepareDataAsPerLineItemPO(CarfourPOXMLBean xmlData){

		List<SegPurchaseOrder> segPurchaseOrderLst = new ArrayList<SegPurchaseOrder>();
		//List<List<QteProduit>> qteProduitLst = new ArrayList<List<QteProduit>>();
		//SegPurchaseOrder segPurchaseOrder = null;
		Map<String, Produit> codProProduitMap = new HashMap<String, Produit>();


		List<Produit> proditLst =  xmlData.getProduitLst();
		List<Tranche> trancheLst = xmlData.getTrancheLst();

		//prepare a map for Produit
		for(Produit produit : proditLst){
			String codPro = produit.getCod_pro();
			codProProduitMap.put(codPro, produit);
		}

		//get the QteProduit list. Based on the this line item is defined.
		//The no of QteProduit is equal to the no of line item to be created in the database.

		for(Tranche tranche : trancheLst){
			List<QteProduit> qtes = tranche.getQteProduit();
			for(QteProduit qteProduit : qtes){
				SegPurchaseOrder segPurchaseOrder = new SegPurchaseOrder();
				segPurchaseOrder.setVc_po_no(xmlData.getNum_uat());
				segPurchaseOrder.setVc_pol(xmlData.getCod_vil_inc());

				//get the respective Produit
				Produit produit = codProProduitMap.get(qteProduit.getCod_pro_qte_produit());
				if(produit != null){
					//Setting the values from the Produit
					segPurchaseOrder.setVc_sku_no(produit.getCod_pro());
					segPurchaseOrder.setVc_style_no(produit.getCod_pro_pro());
					segPurchaseOrder.setVc_product_no(produit.getCod_ray());
					segPurchaseOrder.setNu_length(Double.valueOf(produit.getCol_lon()));
					segPurchaseOrder.setNu_width(Double.valueOf(produit.getCol_lar()));
					segPurchaseOrder.setNu_height(Double.valueOf(produit.getCol_hau()));
					segPurchaseOrder.setVc_ref_field3(produit.getCod_unl_ach());
					segPurchaseOrder.setNu_no_pcs_ctns(Double.valueOf(produit.getPcb()));

					//Setting the values from the Tranche
					segPurchaseOrder.setDt_etd(tranche.getDat_etd());
					segPurchaseOrder.setVc_ref_field2(tranche.getCod_trs_cli());
					segPurchaseOrder.setVc_pod(tranche.getCod_vil_prc2());
					segPurchaseOrder.setVc_ref_field5(tranche.getCod_trs_dep());

					//Setting the values from the QteProduit
					segPurchaseOrder.setVc_color(qteProduit.getCod_cou_qte_produit());
					segPurchaseOrder.setVc_size(qteProduit.getCod_tai_qte_produit());
					segPurchaseOrder.setVc_quan(Double.valueOf(qteProduit.getNbr_col()));
					segPurchaseOrder.setVc_tot_pcs(Double.valueOf(qteProduit.getNbr_uvc()));

					segPurchaseOrderLst.add(segPurchaseOrder);
				}
			}
		}

		return segPurchaseOrderLst;
	}

	 

}
*/