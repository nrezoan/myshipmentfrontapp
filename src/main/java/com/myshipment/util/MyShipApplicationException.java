package com.myshipment.util;

public class MyShipApplicationException  extends MyShipBaseException{
		private static final Long serialVersionUID=-23423424L;

		public MyShipApplicationException() {
			super();
			// TODO Auto-generated constructor stub
		}

		public MyShipApplicationException(ErrorCode errorCode, String message, String[] dynamicValues) {
			super(errorCode, message, dynamicValues);
			// TODO Auto-generated constructor stub
		}

		public MyShipApplicationException(ErrorCode errorCode, String message) {
			super(errorCode, message);
			// TODO Auto-generated constructor stub
		}

		public MyShipApplicationException(Throwable rootCause) {
			super(rootCause);
			// TODO Auto-generated constructor stub
		}


}
