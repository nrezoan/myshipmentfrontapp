package com.myshipment.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.StringTokenizer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SessionAuthFilter implements Filter{

	private long startDateTime; 
	private long endDateTime;
	private long processingTime;
	/*private ArrayList<String> urls;
	private String url;*/
	@Override
	public void destroy() {
		endDateTime=System.currentTimeMillis();
		processingTime=endDateTime-startDateTime;
		System.out.println("Processing Time: "+processingTime);
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest request=(HttpServletRequest)req;
		HttpServletResponse response=(HttpServletResponse)res;
		if(request.getRequestURI().startsWith("/MyShipmentFrontApp/getCommonTrackingDetailInfo")||request.getRequestURI().startsWith("/getCommonTrackingDetailInfo")||request.getRequestURI().equals("/MyShipmentFrontApp/")||request.getRequestURI().equals("/")||request.getRequestURI().equals("/MyShipment/")||request.getRequestURI().startsWith("/MyShipmentFrontApp/resources/")||request.getRequestURI().startsWith("/MyShipmentFrontApp/login")||request.getRequestURI().startsWith("/MyShipmentFrontApp/selectedorg")||request.getRequestURI().startsWith("/selectedorg")||request.getRequestURI().startsWith("/MyShipmentFrontApp/selectedorgcookies")||request.getRequestURI().startsWith("/selectedorgcookies")|| request.getRequestURI().startsWith("/MyShipmentFrontApp/signin")||request.getRequestURI().startsWith("/signin")||request.getRequestURI().startsWith("/MyShipmentFrontApp/signin")||request.getRequestURI().startsWith("/forgetPasswordPage")||request.getRequestURI().startsWith("/MyShipmentFrontApp/forgetPasswordPage")||request.getRequestURI().startsWith("/MyShipmentFrontApp/recoverPassword")||request.getRequestURI().startsWith("/recoverPassword")||request.getRequestURI().startsWith("/trackAirExport")||request.getRequestURI().startsWith("/MyShipmentFrontApp/trackAirExport")||request.getRequestURI().startsWith("/MyShipmentFrontApp/helpEmail")||request.getRequestURI().startsWith("/helpEmail")){
			chain.doFilter(req, res);
		}
		else
		{
			if(SessionUtil.getHttpSession(request)!=null){
				if(SessionUtil.getHttpSession(request).getAttribute(SessionUtil.LOGIN_DETAILS)==null)
				{
				response.sendRedirect(request.getContextPath()+"/");
				System.out.println("come into login null");
				}else
					chain.doFilter(req, res);
			}
			else
			{
				response.sendRedirect(request.getContextPath()+"/");
				//chain.doFilter(req, res);
			}
		}
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		startDateTime=System.currentTimeMillis();
		/*String urlss=arg0.getInitParameter("excludingUrls");
		StringTokenizer token = new StringTokenizer(urlss, ",");

		urls = new ArrayList<String>();
		while(token.hasMoreTokens())
		{
			urls.add(token.nextToken());
			
		}*/
	}

}
