package com.myshipment.util;

public class MyShipBaseException extends Exception{
	private static final Long serialVersionUID=-1343445346363L;
	private ErrorCode errorCode;
	private String message;
	private String[] dunamicValues;
	public MyShipBaseException() {
		super();
	
	}
	
	
	public MyShipBaseException(Throwable rootCause) {
		super(rootCause);
	
	}
	
	public MyShipBaseException(ErrorCode errorCode, String message) {
		this.message=message;
		ErrorCode ec=new ErrorCode(errorCode.getErrorCode().longValue());
		ec.setParameters(errorCode.getParameters());
		ec.setDetailMessage(errorCode.getDetailMessage());
		this.errorCode=ec;
		
		
	}
	public ErrorCode getErrorCode() {
		return errorCode;
	}


	public void setErrorCode(ErrorCode errorCode) {
		this.errorCode = errorCode;
	}


	public String getMessage() {
		return message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public String[] getDunamicValues() {
		return dunamicValues;
	}


	public void setDunamicValues(String[] dunamicValues) {
		this.dunamicValues = dunamicValues;
	}


	public MyShipBaseException(ErrorCode errorCode, String message,String [] dynamicValues) {
		this.message=message;
		ErrorCode ec=new ErrorCode(errorCode.getErrorCode().longValue());
		ec.setParameters(errorCode.getParameters());
		ec.setDetailMessage(this.message);
		this.setDunamicValues(dynamicValues);
		this.errorCode=errorCode;
		
		
	}

	

}
