package com.myshipment.util;

public class StringUtil {
	public static String splitCodefromString(String str,String separator)
	{
		String []result=null;;
		if(null!=str && str!="" )
		{
			result =str.split(separator);
			if(result.length>1)
				return result[1];
			else
				return "";
		}
		else
			return "";
		
		
		
	}
	
	public static String prepareItemNumberForSAP(String itemNumber)
	{
		int padding=6-itemNumber.length();
		for (int i = 0; i < padding; i++) {
			itemNumber="0"+itemNumber;
			
		}
		return itemNumber;
	}

}
