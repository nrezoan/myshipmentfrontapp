package com.myshipment.util;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class ServiceLocator implements ApplicationContextAware {

	private transient ApplicationContext _applicationContext;
	private static ServiceLocator _instance = new ServiceLocator();

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {

		_instance._applicationContext = applicationContext;
	}

	public ApplicationContext getApplicationContext() {
		return _instance._applicationContext;
	}

	public static Object findService(String serviceName) {
		return _instance._applicationContext.getBean(serviceName);
	}

}
