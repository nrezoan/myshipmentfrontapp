/**
*
*@author Ahmad Naquib
*/
package com.myshipment.util;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

public class MyshipmentDatabaseComponent {

	private Logger logger = Logger.getLogger(MyshipmentDatabaseComponent.class);
	
	public Connection connection;
	public Statement statement;
	public PreparedStatement preparedStatement;
	public ResultSet resultSet;
	public ResultSetMetaData resultSetMetaData;

	/**
	* overloading default constructor
	* It makes new instant for ReadyConn class and gets Connection to database
	* by calling getConnection method of ReadyConn class
	*/
	public MyshipmentDatabaseComponent() throws Exception {
		logger.info(this.getClass() + ": Creating myshipment database component...");
		
		DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
		
		if (RestUtil.REST_URL.equals("http://apps.myshipment.com")) {
			logger.info(this.getClass() + ": Server connecting to Production...");
		} else {
			logger.info(this.getClass() + ": Server connecting to Development Database...");
			connection = DriverManager.getConnection("jdbc:oracle:thin:@202.65.11.56:1521:MGHGROUP", "mondial_dev",
					"mondial_dev");
			statement = connection.createStatement();
		}

	}
	
	public void executeProcedure(String procedure) {
		boolean status = false;
		try {
			CallableStatement callableStatement = connection.prepareCall("{call " + procedure + "}");
			status = callableStatement.execute();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	
	public void closeConnection() throws Exception {
		connection.close();
	}
	
	public void commitTransaction() throws Exception {
		connection.commit();
	}
	
	public void rollBackTransaction() throws Exception {
		connection.rollback();
	}
	
	public void autoCommitTransaction() throws Exception {
		connection.setAutoCommit(false);
	}

}
