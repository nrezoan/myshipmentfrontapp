package com.myshipment.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateUtil {

	public static Date formatStringToDate(String dateToFarmat) {
		SimpleDateFormat dateFormatter = new SimpleDateFormat(CommonConstant.DATE_FORMAT_DD_MM_YYYY);
		Date date = null;
		try {
			date = dateFormatter.parse(dateToFarmat);
			//System.out.println(date);
			//System.out.println(dateFormatter.format(date));

		} catch (ParseException e) {

			e.printStackTrace();
		}
		return date;
	}

	public static String formatDateToString(Date dateToFarmat) {
		if (null != dateToFarmat) {
			SimpleDateFormat dateFormatter = new SimpleDateFormat(CommonConstant.DATE_FORMAT_DD_MM_YYYY);
			String date = null;

			date = dateFormatter.format(dateToFarmat);

			return date;
		} else {
			return "";
		}
	}

	public static String formatDD_MM_YYYY_to_YYYYMMDD(String date) {
		if (null == date || date == "")
		{
			return "";
		}else if(date.isEmpty()){
			return "";
		}
		else {
			String strArr[] = date.split("-");
			if(strArr.length>2)
			return strArr[2] + strArr[1] + strArr[0];
			else return "";
		}
	}

	public static void main(String[] args) {
		//System.out.println(DateUtil.formatStringToDate("07-06-2013"));
		
		/*Date departureDate = DateUtil.formatStringToDatenew("20170620", "dd-MM-yyyy");
		Date arrivalDate = DateUtil.formatStringToDatenew("20170620", "dd-MM-yyyy");
		
		System.out.println(departureDate.compareTo(arrivalDate));
		System.out.println(arrivalDate.compareTo(departureDate));*/
		
		Date tempDate = new Date();
		tempDate.setDate(tempDate.getDate() - 60);
		System.out.println(tempDate);
	}

	public static Date formatStringToDatenew(String dateString, String dateFormat) {
		SimpleDateFormat dateFormatter = new SimpleDateFormat(dateFormat);
		Date date = null;
		if(dateString != "") {
			try {
				date = dateFormatter.parse(DateUtil.YYYYMMDDToDD_MM_YYYY(dateString));
				//System.out.println(date);
				//System.out.println(dateFormatter.format(date));

			} catch (ParseException e) {

				e.printStackTrace();
			}
		}
		return date;
	}

	public static String YYYYMMDDToDD_MM_YYYY(String datetoFormat) {
		if(null!=datetoFormat && datetoFormat.length()>=8)
		{
		String year = datetoFormat.substring(0, 4);
		String month = datetoFormat.substring(4, 6);
		String date = datetoFormat.substring(6, 8);
		return date + "-" + month + "-" + year;
		}
		else
			return "";
	}
	
	public static Date stringToDateTime(String dateString) {
		dateString = dateString + "00";
		
		Date date;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		
		try {
			date = sdf.parse(dateString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException("Failed to parse date : " + e);
		}
		
		return date;
	}

	public static String getCurrentDateAsString(){
		
		String dateAsString = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
		return dateAsString;
	}
}
