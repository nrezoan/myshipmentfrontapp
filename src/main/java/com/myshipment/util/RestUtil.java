package com.myshipment.util;

public class RestUtil {
	
	//public static final String REST_URL = "http://192.168.0.227:8081/MyShipmentBackend";
	//public static final String REST_URL = "http://localhost:8081/MyShipmentBackend";
	//public static final String REST_URL_LOCALHOST = "http://localhost:8081/MyShipmentBackend";
	//public static final String REST_URL = "http://apps.myshipment.com";
	
	//public static final String REST_URL = "http://192.168.14.203:8080/MyShipmentBackend";
	public static final String REST_URL = "http://192.168.7.77:8080/MyShipmentBackend";//production server
	//public static final String REST_URL = "http://121.200.242.147:8080/MyShipmentBackend";//development server
	//public static final String REST_URL = "http://192.168.0.130:8080/InventryManagementSystem";
	//public static final String REST_URL = "http://localhost:8080/MyShipmentBackend";
	
	
	//----------------------OAG URL----------------------------//
	//public static final String OAG_URL = "http://localhost:8909/";
	//public static final String OAG_URL = "http://localhost:8081/oag";
	public static final String OAG_URL="http://192.168.7.77:8081/OAG-Parser/";

	
	public static final String PORT_DETAILS="getPortDetails";
	public static final String LOGIN_DETAILS="getLoginDetail";
	public static final String SUPPLIER_DETAILS="getSupplierDetails";

	public static final String AirBillOfLanding_DETAILS="getAirBillOfLandingDetails";
	public static final String SeaBillOfLanding_DETAILS="getSeaBillOfLandingDetails";
	public static final String StuffingReport_DETAILS="getSeaStuffingReportDetails";

	public static final String DIRECT_BOOKING="doDirectBooking";
	public static final String COMMERCIAL_INVOICE = "getCommercialInvoice";
	public static final String TRACKING = "getTrackingDetail";
	public static final String TRACKING_DETAIL = "getTrackingDetailData";	
	public static final String COMMON_TRACKING_DETAIL = "getCommonTrackingDetailData";	
	public static final String VESSEL_TRACKING_DETAIL = "vesselTracking";	
	
	//DASHBOARD API
	public static final String DASHBOARD_API = "getMyshipmentTrackDetailsDashboard";
	//---------------//
	public static final String MYSHIPMENT_COMPLETE_TRACK = "getMyshipmentTrackDetails";
	public static final String LAST_N_SHIPMENT = "getLastNShipmentsDetail";
	public static final String AIR_BILL_OF_LANDING = "getAirBillOfLandingDetails";
	public static final String SEA_BILL_OF_LANDING = "getSeaBillOfLandingDetails";
	public static final String EMAIL="getMyshipmentEmailList";
	public static final String PLACE_OF_RECIEPT="getStoreLocDetails";
	public static final String BOOKING_DISPLAY = "getBookingDetailForDisplay";
	public static final String SO_BY_HBL="getSoNoBasedOnHblNo";
	public static final String BOOKING_UPDATE = "doDirectBookingUpdate";
	public static final String PO_TRACKING_INFO = "poTrackingInfo";
	public static final String PO_TRACKING = "poTracking";
	
	public static final String PO_TRACKING_REPORT = "poTrackingReport";
	public static final String COMM_INV_TRACKING = "commInvTrackingInfo";
	public static final String MBL_WISE_TRACKING = "getMblWiseTrackingInfo";
	
	public static final String SHIPPING_ORDER = "getShippingOrderReport";
	public static final String CARGO_ACK = "getCargoAckReport";
	public static final String PACKING_LIST_REPORT = "getPackingListReportDetail";
	

	public static final String SO_WISE_SHIP_SUMMARY_SHIPPER="sowiseshipsummaryshipper";
	public static final String SUPP_WISE_SHIP_SUMMARY_BUYER="sowiseshipsummarybuyer";
	public static final String TOP_FIVE_SHIPMENT_SHIPPER="top5shipmentshipper";
	public static final String TOP_FIVE_SHIPMENT_BUYER="top5shipmentbuyer";
	public static final String SUPP_WISE_SHIP_DETAILS_BLSTATUS="suppwiseshipdetailsbyblstatus";
	public static final String BUY_WISE_SHIP_DETAILS_BY_BLSTATUS="buywiseshipdetailsbyblstatus";
	public static final String BUYER_WISE_SHIP_DETAILS="buyerwiseshipdetails";
	public static final String BUYER_WISE_CBM_DETAILS="buyerwisecbmdetails";
	public static final String BUYER_WISE_GWT_DETAILS="buyerwisegwtdetails";

	public static final String SEA_EXPORT_INVOICE_LIST="getInvoiceList";
	public static final String SEA_EXPORT_INVOICE_DETAIL="getSeaExportInvoiceDetail";
	public static final String AIR_EXPORT_INVOICE_LIST="getInvoiceList";
	public static final String AIR_EXPORT_INVOICE_DETAIL="getSeaExportInvoiceDetail";

	public static final String SUPP_WISE_SHIP_DETAILS="suppwiseshipdetails";
	public static final String SUPP_WISE_CBM_DETAILS="suppwisecbmdetails";
	public static final String SUPP_WISE_GWT_DETAILS="suppwisegwtdetails";
	public static final String SO_WISE_SHIP_DETAILS_SHIPPER="sowiseshipdetailsshipper";
	public static final String BUYER_WISE_SHIP_DETAILS_BY_POD="buyerwiseshipdetailsbypod";
	public static final String SUPP_WISE_SHIP_DETAILS_BUYER="suppwiseshipdetailsbuyer";
	public static final String SUPP_WISE_SHIP_DETAILS_BY_POO="suppwiseshipdetailsbypoo";
	public static final String SHIP_DET_SHIPPER="shipdetshipper";
	public static final String SHIP_DET_BUYER="shipdetbuyer";
	public static final String SHIP_DETAILS_TO_EXPORT="shipdetailstoexport";
	public static final String PACKING_LIST_BY_HBL="packinglistbyhbl";
	public static final String OPEN_TRACKING_DETAIL="getOpenTrackingDetail";
	public static final String PASSWORD_CHANGE="setNewPassword";
	public static final String RECOVER_PASSWORD="recoverPassword";
	public static final String GET_GOODS_RECEIVED_ITEMS ="getGoodsReceiveItems";
	
	
	//---------------------OAG Service------------------//
	public static final String GET_FLIGHT_DETAILS = "myshipment/oag/track/flights";
	
	
	
	
	public static StringBuffer prepareUrlForService(StringBuffer servicePath)
	{
		return new StringBuffer(REST_URL).append("/").append(servicePath);
		
	}
	
	public static StringBuffer prepareUrlForOAGCall(StringBuffer servicePath) {
		
		return new StringBuffer(OAG_URL).append("/").append(servicePath);
	}

}
