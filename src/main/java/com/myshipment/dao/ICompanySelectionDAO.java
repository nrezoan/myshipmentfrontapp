/**
*
*@author Ahmad Naquib
*/
package com.myshipment.dao;

import java.util.List;

import com.myshipment.model.DefaultCompany;
import com.myshipment.model.IntelligentCompanyUsage;

public interface ICompanySelectionDAO {

	public List<IntelligentCompanyUsage> getCompanyUsageList(String userCode) throws Exception;
	public DefaultCompany getDefaultCompany(String userCode) throws Exception;
	public int saveDefaultCompany(DefaultCompany defaultCompany) throws Exception;
	public int updateDefaultCompany(DefaultCompany defaultCompany) throws Exception;
	public int setActiveDefaultCompany(DefaultCompany defaultCompany) throws Exception;
	
}
