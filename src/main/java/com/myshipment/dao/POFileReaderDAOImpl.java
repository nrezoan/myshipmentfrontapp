package com.myshipment.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.myshipment.mappers.POFileReaderMapper;
import com.myshipment.model.POFileInfo;
/*
 * @Ranjeet Kumar
 */
@Repository
public class POFileReaderDAOImpl implements IPOFileReaderDAO{

	private static final Logger logger = Logger.getLogger(POFileReaderDAOImpl.class);

	@Autowired
	private POFileReaderMapper poFileReaderMapper;

	@Override
	public void savePOFileData(List<POFileInfo> fileInfoLst) {

		logger.info("Method POFileReaderDAOImpl.savePOFileData starts.");
		if(fileInfoLst != null && fileInfoLst.size() > 0){
			for(POFileInfo fileInfo : fileInfoLst){
				poFileReaderMapper.insert(fileInfo);
			}
		}
		else{
			logger.debug("fileInfoLst is null or empty in the method POFileReaderDAOImpl.savePOFileData");
		}
		logger.info("Method POFileReaderDAOImpl.savePOFileData ends.");
	}

	@Override
	public POFileInfo getFileInfoForDuplicateCheck(String fileName) {

		logger.info("Method POFileReaderDAOImpl.getFileInfoForDuplicateCheck starts.");

		if(fileName != null){
			POFileInfo poFileInfo = poFileReaderMapper.getFileBasedOnFileName(fileName);
			return poFileInfo;
		}
		else{
			logger.debug("fileName is missing(method POFileReaderDAOImpl.getFileInfoForDuplicateCheck)");
		}
		logger.info("Method POFileReaderDAOImpl.getFileInfoForDuplicateCheck ends.");
		
		return null;
	}

}
