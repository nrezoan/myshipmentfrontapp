package com.myshipment.dao;

import java.util.List;

import com.myshipment.model.CRMLogger;
import com.myshipment.model.ReportParams;

public interface IUpdateLoggerDAO {
	public int getUpdateLogger(List<CRMLogger> updateLogger);
	public List<CRMLogger> getLoggerHblWise(CRMLogger req);

}
