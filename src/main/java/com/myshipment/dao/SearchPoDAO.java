package com.myshipment.dao;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.myshipment.dto.SearchPoDTO;
import com.myshipment.mappers.PoSearchMapper;
import com.myshipment.model.HeaderPOData;
import com.myshipment.model.SegPurchaseOrder;
/*
 * @ Ranjeet Kumar
 */
public class SearchPoDAO implements ISearchPoDAO{

	@Autowired
	private PoSearchMapper poSearchMapper;
	
	@Override
	public List<HeaderPOData> getPosBasedOnPoNo(SearchPoDTO searchPoDto) {

		String poNo = searchPoDto.getPoNo();
		List<HeaderPOData> headerData = poSearchMapper.selectPoBasedOnPoNo(poNo);

		return headerData;
	}
	
	@Override
	public List<HeaderPOData> getPosBasedOnPoNoAndDate(SearchPoDTO searchPoDto) {
		String poNo = searchPoDto.getPoNo();
		//Date toDate = searchPoDto.getToDate();
		//List<HeaderPOData> headerData = poSearchMapper.getPosBasedOnPoNoAndDate(poNo, toDate);

		//return headerData;
		return null;
	}
/*	
	@Override
	public List<HeaderPOData> getPosBasedOnTodateAndFromdate(SearchPoDTO searchPoDto) {			

		//Date toDate = searchPoDto.getToDate();
		//Date fromDate = searchPoDto.getFromDate();
		String toDate = searchPoDto.getToDate();
		String fromDate = searchPoDto.getFromDate();
		List<HeaderPOData> headerData = poSearchMapper.getPosBasedOnTodateAndFromdate(fromDate, toDate);

		return headerData;
	}
*/	
	@Override
	public List<SegPurchaseOrder> getAllLineItemPoData(String poNo) {
		List<SegPurchaseOrder> segOrders = poSearchMapper.getAllLineItemPos(poNo);
		return segOrders;
	}
	
	@Override
	public List<SegPurchaseOrder> getPosBasedOnTodateAndFromdate(SearchPoDTO searchPoDto) {			

		//Date toDate = searchPoDto.getToDate();
		//Date fromDate = searchPoDto.getFromDate();
		String toDate = searchPoDto.getToDate();
		String fromDate = searchPoDto.getFromDate();
		List<SegPurchaseOrder> headerData = poSearchMapper.getLineItemsBasedOnFromdateAndTodate(fromDate, toDate);

		return headerData;
	}

	@Override
	public List<SegPurchaseOrder> getPosBasedOnTodateAndFromdateAndPoNo(SearchPoDTO searchPoDto) {
		String toDate = searchPoDto.getToDate();
		String fromDate = searchPoDto.getFromDate();	
		List<SegPurchaseOrder> headerData = poSearchMapper.getLineItemsBasedOnFromdateAndTodateAndPoNo(fromDate, toDate, searchPoDto.getPoNo());
		
		return headerData;
	}

	@Override
	public List<SegPurchaseOrder> getAllDataForNoInput() {
		//String toDate = searchPoDto.getToDate();
		//String fromDate = searchPoDto.getFromDate();	
		List<SegPurchaseOrder> headerData = poSearchMapper.getLineItemsDataIfNoInputProvided();
		
		return headerData;
	}

	@Override
	public Long updatePoBasedOnSegId(SegPurchaseOrder segPurchaseOrder) {
		
		Long id = poSearchMapper.updatePoOnSegId(segPurchaseOrder);
		return id;
	}
	
}
