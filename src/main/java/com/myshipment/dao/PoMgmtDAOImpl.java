package com.myshipment.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.myshipment.mappers.HMMapper;
import com.myshipment.mappers.POMapperNew;
import com.myshipment.mappers.POUploadMapper;
import com.myshipment.mappers.PoMapper;
import com.myshipment.model.ApproOrderCarrierDtl;
import com.myshipment.model.ApprovedPurchaseOrder;
import com.myshipment.model.BuyerDTO;
import com.myshipment.model.CarrierMaster;
import com.myshipment.model.Materials;
import com.myshipment.model.PackListColor;
import com.myshipment.model.PackListSize;
import com.myshipment.model.PackingListMdl;
import com.myshipment.model.PurchaseOrderMdl;
import com.myshipment.model.PurchaseOrdersModel;
import com.myshipment.model.SaveAsTemplateMdl;
import com.myshipment.model.SegPurchaseOrderMdl;
import com.myshipment.model.StorageLocation;
import com.myshipment.model.UpdateEvent;
import com.myshipment.model.WarehouseCode;

public class PoMgmtDAOImpl implements IPoMgmtDAO {

	@Autowired
	private PoMapper poMapper;

	@Autowired
	private POUploadMapper poUploadMapper;

	@Autowired
	private HMMapper hmMapper;

	public PoMapper getPoMapper() {
		return poMapper;
	}

	public void setPoMapper(PoMapper poMapper) {
		this.poMapper = poMapper;
	}

	@Autowired
	private POMapperNew poMapperNew;

	public POMapperNew getPoMapperNew() {
		return poMapperNew;
	}

	public void setPoMapperNew(POMapperNew poMapperNew) {
		this.poMapperNew = poMapperNew;
	}

	// load dash board data
	@Override
	public List<PurchaseOrderMdl> getPoDashBoardData() {

		return poMapper.selectAllPO();
	}

	// load po approve search page
	public PurchaseOrderMdl getPoAndLineItemById(Long id) {
		return poMapper.selectPoLineItemByPoId(id);
	}

	public Long updatePoDetails(ApprovedPurchaseOrder appPoOrder) {
		poMapper.updateApprovedPo(appPoOrder);
		return appPoOrder.getApp_id();
	}

	public void updateApprovedPoBookingStatusById(ApprovedPurchaseOrder appPoOrder) {
		poMapper.updateApprovedPoBookingStatusById(appPoOrder);

	}

	public SegPurchaseOrderMdl getLineItemByItemId(Long lineItemId) {
		return poMapper.selectPoLineItembyLineItemId(lineItemId);
	}

	public void updatepoUpdateEvent(UpdateEvent updateEvent) {
		poMapper.updatePoUpdateEvent(updateEvent);
	}

	public List<ApprovedPurchaseOrder> getApprovedPoLineItemById(Long poId) {
		return poMapper.selectAppPoLineItembyLineItemId(poId);
	}

	public List<CarrierMaster> getAllCarriers() {
		return poMapper.getAllCarriers();
	}

	public List<PurchaseOrderMdl> searchPo(String poNumber, String fromDate, String toDate, String supplierCode,
			String buyerCode) {
		return poMapper.searchPo(poNumber, fromDate, toDate, supplierCode, buyerCode);

	}

	@Override
	public Long updateLineItem(SegPurchaseOrderMdl segPo) {
		return poMapper.updateLineItemById(segPo);
	}

	@Override
	public Long updateLineItemCarrierIdAndScheduleById(SegPurchaseOrderMdl segPo) {
		return poMapper.updateLineItemCarrierIdAndScheduleById(segPo);
	}

	@Override
	public Long insertAppCarrDtl(ApproOrderCarrierDtl appOrdCarrDtl) {
		return poMapper.insertAppCarrDtl(appOrdCarrDtl);
	}

	public List<ApprovedPurchaseOrder> searchApprovedPo(String poNumber, String fromDate, String toDate,
			String supplierCode, String buyerCode) {
		return poMapper.searchApprovedPo(poNumber, fromDate, toDate, supplierCode, buyerCode);
	}

	public ApprovedPurchaseOrder getApprovedItemById(Long appId) {
		return poMapper.getApprovedItemById(appId);
	}

	public void updateApprovedPOById(ApprovedPurchaseOrder appPo) {
		poMapper.updateApprovedPOById(appPo);
	}

	public List<ApprovedPurchaseOrder> getApprovedPoNotBookedList(String poNumber, String buyer, String fromDate,
			String toDate) {
		return poMapper.getApprovedPoNotBookedList(poNumber, buyer, fromDate, toDate);
	}

	/*
	 * public List<ApprovedPurchaseOrder> getPOforBooking(String buyer, String
	 * poNumber,String salesOrg,String division,String shipper) {
	 * List<ApprovedPurchaseOrder> searchedList =
	 * poMapperNew.getPOforBooking(buyer,salesOrg,division,shipper);
	 * List<ApprovedPurchaseOrder> filteredList = new
	 * ArrayList<ApprovedPurchaseOrder>(); if (poNumber != "") { for
	 * (ApprovedPurchaseOrder orders : searchedList) { if
	 * (orders.getVc_po_no().equals(poNumber)) { filteredList.add(orders); } }
	 * return filteredList; } return searchedList; }
	 */

	public List<PurchaseOrdersModel> getPOforBooking(String buyer, String poNumber, String shipper) {
		List<PurchaseOrdersModel> searchedList = poUploadMapper.getPendingPOShipperwise(buyer, shipper);
		List<PurchaseOrdersModel> filteredList = new ArrayList<PurchaseOrdersModel>();
		if (poNumber != "") {
			for (PurchaseOrdersModel orders : searchedList) {
				if (orders.getPo_no().equals(poNumber)) {
					filteredList.add(orders);
				}
			}
			return filteredList;
		}
		return searchedList;
	}

	@Override
	public void savePackListOrderBooking(PackingListMdl packingListMdl) {

		long packingListId = poMapper.savePackingList(packingListMdl);
		for (Iterator iterator = packingListMdl.getPackListColor().iterator(); iterator.hasNext();) {
			PackListColor packListColor = (PackListColor) iterator.next();
			packListColor.setPlId(packingListMdl.getPlId());
			long colorId = poMapper.savePackingListColor(packListColor);
			for (Iterator iterator2 = packListColor.getPackListSize().iterator(); iterator2.hasNext();) {
				PackListSize packListSize = (PackListSize) iterator2.next();
				packListSize.setColorId(packListColor.getColorId());
				poMapper.savePackingListSize(packListSize);
			}

		}

	}

	@Override
	public void saveBookingInfoAsTemplate(SaveAsTemplateMdl bookingInfoAstemplate) {

		// long packingListId=poMapper.savePackingList(packingListMdl);
		poMapper.saveBookingInfoAsTemp(bookingInfoAstemplate);

	}

	public List<SaveAsTemplateMdl> getTemplateInfoByLoginCred(SaveAsTemplateMdl saveAsTemplateMdl) {
		return poMapper.getTemplateInfoByLoginCred(saveAsTemplateMdl);
	}

	public SaveAsTemplateMdl getTemplateInfoByTemplateId(String templateId, String customerCode, String company,
			String operationType, String seaOrAir) {
		SaveAsTemplateMdl mdl = null;
		mdl = poMapper.getTemplateInfoByTemplateId(templateId, customerCode, company, operationType, seaOrAir);
		return mdl;
	}

	public void updateTemplateInfo(String temp_Id, String tempInfo) {

		poMapper.updateTemplateInfo(temp_Id, tempInfo);

	}

	@Override
	public List<Materials> getMaterialsDao() {

		return poMapper.getMaterial();

	}

	@Override
	public List<StorageLocation> getStorageLocationsDao() {
		return poMapper.getStorageLocation();
	}

	@Override
	public List<BuyerDTO> getHMBuyers(String buyer, String disChnlSelected, String divisionSelected,
			String salesOrgSelected) {
		List<BuyerDTO> buyerLst = hmMapper.getBuyerList(buyer, disChnlSelected, divisionSelected, salesOrgSelected);
		return buyerLst;
	}

	@Override
	public List<WarehouseCode> getWHList(String country) {
		List<WarehouseCode> whList = hmMapper.getWHList(country);
		return whList;
	}
}
