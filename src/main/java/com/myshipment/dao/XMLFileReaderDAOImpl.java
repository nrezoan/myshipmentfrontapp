package com.myshipment.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.myshipment.mappers.SegPurchaseOrderMapper;
import com.myshipment.model.SegPurchaseOrder;
/*
 * @Ranjeet Kumar
 */
@Repository("iXMLFileReaderDAO")
public class XMLFileReaderDAOImpl implements IXMLFileReaderDAO{

	@Autowired
	private SegPurchaseOrderMapper segPurchaseOrderMapper;

	@Override
	@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	public void saveXMLDataToTheDatabase(List<List<SegPurchaseOrder>> segPurchaseOrderLst) {
		
		if(segPurchaseOrderLst != null){
			for(List<SegPurchaseOrder> segLst : segPurchaseOrderLst){
				for(SegPurchaseOrder segPurchaseOrder : segLst){
					segPurchaseOrder.setPo_id(new Long(000));
					segPurchaseOrderMapper.insert(segPurchaseOrder);
				}
			}
		}
	}
}
