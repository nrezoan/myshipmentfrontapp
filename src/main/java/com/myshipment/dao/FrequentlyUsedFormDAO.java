package com.myshipment.dao;

import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.myshipment.dto.LoginDTO;
import com.myshipment.dto.SegregationDTO;
import com.myshipment.mappers.FrequentlyUsedFormMapper;
import com.myshipment.mappers.SegregationMapper;
import com.myshipment.model.FrequentUsedMenu;
import com.myshipment.model.FrequentlyUsedFormMdl;
import com.myshipment.model.SegPurchaseOrder;
/*
 * @ Ranjeet Kumar
 */
public class FrequentlyUsedFormDAO implements IFrequentlyUsedFormDAO{

	private static final Logger logger = Logger.getLogger(FrequentlyUsedFormDAO.class);
	
	@Autowired
	private FrequentlyUsedFormMapper mapper;

	public FrequentlyUsedFormMapper getMapper() {
		return mapper;
	}

	public void setMapper(FrequentlyUsedFormMapper mapper) {
		this.mapper = mapper;
	}

	@Override
	@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	public int insetFrequentlyUsedFormIntoTable(FrequentlyUsedFormMdl obj) {

		int updateStatus = 0;
		List <FrequentlyUsedFormMdl> obj1 = null;
		obj1 = mapper.getFrequentlyUsedFormInfo(obj.getCustomerCode(),obj.getCompany(),obj.getDistChannel(),obj.getDivison(),obj.getPageName());
		if(obj1.size()>0){
			FrequentlyUsedFormMdl ffMdl = obj1.get(0);
			int counter = ffMdl.getCounter();
			ffMdl.setCounter(counter+1);
			updateStatus = mapper.updateCounterOfFrequentlyUsedMenu(ffMdl);
		}else{
			updateStatus = mapper.saveFrequentlyUserForm(obj);
		}
		return updateStatus;
	
	}
	@Override
	@Transactional(rollbackFor=Exception.class, propagation=Propagation.REQUIRED)
	public List <FrequentUsedMenu> getFrequentlyUsedMenu(LoginDTO loginDTO) {
		List <FrequentUsedMenu> menu = mapper.getFrequentlyUsedMenu(loginDTO);
		return menu;
	
	}
	

}
