package com.myshipment.dao;

import java.util.List;

import com.myshipment.dto.FileDTO;
import com.myshipment.model.ApprovedPurchaseOrder;
import com.myshipment.model.HeaderAndLineitemData;
import com.myshipment.model.POFileDTO;
import com.myshipment.model.SegPurchaseOrder;
/*
 * @ Ranjeet Kumar
 */
public interface IUploadPODataDAO {

	public void insertPODataToDatabase(List<POFileDTO> poFileDTOLst);

	public List<SegPurchaseOrder> insertDataInPurchaseOrderAndSegregatedTable(HeaderAndLineitemData headerAndLineitemData, FileDTO fileData);

	public List<SegPurchaseOrder> getUploadedRecords(String currentDate);

	public List<List<SegPurchaseOrder>> getCurrentlyUploadedRecordThroughFileUpload(List<Long> poIdLst);
	
	public List<ApprovedPurchaseOrder> savePOGeneratedInfo(List<ApprovedPurchaseOrder> appPOList);
}
