package com.myshipment.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.myshipment.model.DirectBookingParams;
import com.myshipment.model.FileUploadMdl;
import com.myshipment.model.MaterialsDto;
import com.myshipment.model.OrderItem;
import com.myshipment.service.IFileUploadService;
import com.myshipment.service.IOrderService;
import com.myshipment.util.SessionUtil;

@Controller
@RequestMapping("/upload")
public class FileUploadController {
	@Autowired
	private IFileUploadService fileUploadService;

	@Autowired
	IOrderService orderService;

	// Sanjana
	private String updateMessage = null;

	@RequestMapping(value = "/uploadExcel", method = RequestMethod.POST)
	public String upload(@ModelAttribute("fileData") FileUploadMdl fileUploadMdl,
			@RequestParam("file") MultipartFile multipartFile, HttpServletResponse servletResponse,
			HttpServletRequest request, Model model) throws JsonProcessingException {
		Boolean mandatoryCheckFail = false;
		String fileName = multipartFile.getOriginalFilename();
		HttpSession session = SessionUtil.getHttpSession(request);
		DirectBookingParams directBookingParams = (DirectBookingParams) session
				.getAttribute(SessionUtil.BOOKING_UPDATE_ATTRIBUTE);
		fileUploadMdl.setFileName(fileName);
		List<OrderItem> oldOrderItems = directBookingParams.getOrderItemLst();
		directBookingParams = fileUploadService.processFileUploaded(multipartFile, fileUploadMdl.getFileName(),
				directBookingParams);
		mandatoryCheckFail = validationForMandatoryFields(directBookingParams);

		String grDone = "";
		Integer checkSum = 0;
		Integer curtonQuantity = 0;
		Double totalVolume = 0.0;
		Double grossWeight = 0.0;

		List<OrderItem> lstOrderItemForDisplay = null;
		String jSON  = null;

		if (mandatoryCheckFail) {
			directBookingParams.setOrderItemLst(oldOrderItems);

			// Sanjana
			ObjectMapper objectMapper = new ObjectMapper();

			jSON = objectMapper.writeValueAsString(directBookingParams);

			// declaring variables
			/*
			 * List<OrderItem> lstOrderItemForDisplay =
			 * directBookingParams.getOrderItemLst(); String grDone = ""; Integer checkSum =
			 * 0; Integer curtonQuantity = 0; Double totalVolume = 0.0; Double grossWeight =
			 * 0.0;
			 */

			lstOrderItemForDisplay = directBookingParams.getOrderItemLst();

			for (OrderItem orderItem2 : lstOrderItemForDisplay) {
				curtonQuantity = curtonQuantity + orderItem2.getCurtonQuantity();
				grossWeight = grossWeight + orderItem2.getGrossWeight();
				totalVolume = totalVolume + orderItem2.getTotalVolume();

				if (orderItem2.getGrFlag() != null && orderItem2.getGrFlag().equals("x"))
					checkSum++;
				else
					orderItem2.setGrFlag("");
			}

			boolean success = false;
			model.addAttribute("updateStatus", success);
			model.addAttribute("updateMessage", updateMessage);
		}

		else {

			ObjectMapper objectMapper = new ObjectMapper();

			jSON = objectMapper.writeValueAsString(directBookingParams);

			// declaring variables
			/*
			 * List<OrderItem> lstOrderItemForDisplay =
			 * directBookingParams.getOrderItemLst(); String grDone = ""; Integer checkSum =
			 * 0; Integer curtonQuantity = 0; Double totalVolume = 0.0; Double grossWeight =
			 * 0.0;
			 */

			lstOrderItemForDisplay = directBookingParams.getOrderItemLst();
			for (OrderItem orderItem2 : lstOrderItemForDisplay) {
				curtonQuantity = curtonQuantity + orderItem2.getCurtonQuantity();
				grossWeight = grossWeight + orderItem2.getGrossWeight();

				// calculating CBM with all conditions
				if (orderItem2.getTotalVolume() != null) {
					if (orderItem2.getCartonLength() != null && orderItem2.getCartonWidth() != null
							&& orderItem2.getCartonHeight() != null && orderItem2.getCartonUnit() != null) {

						if (!orderItem2.getCartonUnit().toUpperCase().equals("IN")
								&& !orderItem2.getCartonUnit().toUpperCase().equals("CM")) {

							System.out.println(orderItem2.getCartonUnit().toUpperCase());

							orderItem2.setCartonLength(null);
							orderItem2.setCartonWidth(null);
							orderItem2.setCartonHeight(null);
							orderItem2.setCartonUnit(null);
							totalVolume = totalVolume + orderItem2.getTotalVolume();

						} else if (orderItem2.getCartonLength().isNaN() || orderItem2.getCartonWidth().isNaN()
								|| orderItem2.getCartonHeight().isNaN() || orderItem2.getCartonLength() <= 0.0
								|| orderItem2.getCartonWidth() <= 0.0 || orderItem2.getCartonHeight() <= 0.0) {

							orderItem2.setCartonLength(null);
							orderItem2.setCartonWidth(null);
							orderItem2.setCartonHeight(null);
							orderItem2.setCartonUnit(null);
							totalVolume = totalVolume + orderItem2.getTotalVolume();

						} else {
							if (orderItem2.getCartonUnit().toUpperCase().equals("IN")) {
								Double vol = ((orderItem2.getCartonLength() * orderItem2.getCartonWidth()
										* orderItem2.getCartonHeight()) / 61024)
										* orderItem2.getCurtonQuantity();

								totalVolume = vol;
								orderItem2.setTotalCbm(totalVolume);
								orderItem2.setTotalVolume(totalVolume);
								orderItem2.setCartonUnit("IN");
							}

							if (orderItem2.getCartonUnit().toUpperCase().equals("CM")) {
								Double vol = ((orderItem2.getCartonLength() * orderItem2.getCartonWidth()
										* orderItem2.getCartonHeight()) / 1000000) * orderItem2.getCurtonQuantity();

								totalVolume = vol;
								orderItem2.setTotalCbm(totalVolume);
								orderItem2.setTotalVolume(totalVolume);
								orderItem2.setCartonUnit("CM");
							}
						}

					}

					else {

						orderItem2.setCartonLength(null);
						orderItem2.setCartonWidth(null);
						orderItem2.setCartonHeight(null);
						orderItem2.setCartonUnit(null);
						totalVolume = totalVolume + orderItem2.getTotalVolume();
					}

				} else {
					
					if (orderItem2.getCartonLength() != null && orderItem2.getCartonWidth() != null
							&& orderItem2.getCartonHeight() != null && orderItem2.getCartonUnit() != null) {
						
						if (orderItem2.getCartonUnit().toUpperCase().equals("IN")) {
							Double vol = ((orderItem2.getCartonLength() * orderItem2.getCartonWidth()
									* orderItem2.getCartonHeight()) / (1728 * 0.028317))
									* orderItem2.getCurtonQuantity();

							totalVolume = vol;
							orderItem2.setTotalCbm(totalVolume);
							orderItem2.setTotalVolume(totalVolume);
							orderItem2.setCartonUnit("IN");
						}

						if (orderItem2.getCartonUnit().toUpperCase().equals("CM")) {
							Double vol = ((orderItem2.getCartonLength() * orderItem2.getCartonWidth()
									* orderItem2.getCartonHeight()) / 1000000) * orderItem2.getCurtonQuantity();

							totalVolume = vol;
							orderItem2.setTotalCbm(totalVolume);
							orderItem2.setTotalVolume(totalVolume);
							orderItem2.setCartonUnit("CM");
						}
						
					}
					

				}

				//

				// totalVolume = totalVolume + orderItem2.getTotalVolume();

				if (orderItem2.getGrFlag() != null && orderItem2.getGrFlag().equals("x"))
					checkSum++;
				else
					orderItem2.setGrFlag("");
			}

		}
		grossWeight = Math.round(grossWeight * 100.0) / 100.0;
		totalVolume = Math.round(totalVolume * 100.0) / 100.0;
		if (checkSum == 0) {
			// goods not received for any line item
			grDone = "show";
		} else {
			// goods received for all/some line items
			grDone = "hide";
		}
		directBookingParams.setOldOrderItemList(oldOrderItems);
		model.addAttribute("totalQuantity", curtonQuantity);
		model.addAttribute("grossWeight", grossWeight);
		model.addAttribute("totalCBM", totalVolume);
		model.addAttribute("grDone", grDone);

		List<MaterialsDto> lstMaterialsDto = orderService.getMaterialService("");
		model.addAttribute("materials", lstMaterialsDto);
		model.addAttribute("directBookingParamsJson", jSON);
		model.addAttribute("directBookingParams", directBookingParams);
		session.setAttribute(SessionUtil.BOOKING_UPDATE_ATTRIBUTE, directBookingParams);

		boolean itemTab = true;
		model.addAttribute("itemTabActive", new Gson().toJson(itemTab));

		return "booking-update3";// replace with actual page name
	}

	private Boolean validationForMandatoryFields(DirectBookingParams directBookingParams) {
		Boolean mandatoryFieldNotAvailable = false;

		List<OrderItem> orderItemLst = directBookingParams.getOrderItemLst();
		for (OrderItem item : orderItemLst) {
			String poNumber = item.getPoNumber();
			Integer totalPieces = item.getTotalPieces();
			String hsCode = item.getHsCode();
			String material = item.getMaterial();
			Double totalCbm = item.getTotalVolume();
			Double grossWeight = item.getGrossWeight();
			Integer curtonQuantity = item.getCurtonQuantity();

			Double cartonLength = item.getCartonLength();
			Double cartonWidth = item.getCartonWidth();
			Double cartonHeight = item.getCartonHeight();
			String cartonMeasurementUnit = item.getCartonUnit();

			/*
			 * if((poNumber==null || poNumber.isEmpty()) || (totalPieces==null ||
			 * totalPieces==0) || (hsCode== null || hsCode.isEmpty()) || (material==null ||
			 * material.isEmpty()) || (grossWeight==null || grossWeight==0.0) ||
			 * curtonQuantity==null || curtonQuantity==0) { mandatoryFieldNotAvailable =
			 * true; break; }
			 */

			if ((poNumber != null && !poNumber.isEmpty()) && (totalPieces != null && totalPieces > 0)
					&& (hsCode != null && !hsCode.isEmpty()) && (material != null && !material.isEmpty())
					&& (grossWeight != null && grossWeight > 0.0) && curtonQuantity != null && curtonQuantity > 0) {
				if (totalCbm != null) {
					if (totalCbm > 0.0) {

					} else {
						updateMessage = "TotalCBM can not be 0. Please put a greater value";
						mandatoryFieldNotAvailable = true;
						break;
					}
				} else if (cartonLength != null && cartonWidth != null && cartonHeight != null
						&& cartonMeasurementUnit != null) {
					if (cartonLength > 0 && cartonWidth > 0 && cartonHeight > 0 && curtonQuantity > 0) {

					} else {
						updateMessage = "Carton Measurement Values (Length, Height, Width) can not be 0. Please provide greater value.";
						mandatoryFieldNotAvailable = true;
						break;
					}

					if (cartonMeasurementUnit.toUpperCase().equals("IN")
							|| cartonMeasurementUnit.toUpperCase().equals("CM")) {

					} else {
						updateMessage = "Please provide correct CartonMeasurementUnit.";
						mandatoryFieldNotAvailable = true;
						break;
					}

					if (cartonLength.isNaN() || cartonHeight.isNaN() || cartonWidth.isNaN()) {
						updateMessage = "Please provide correct Carton Measurement Values (Length, Height, Width).";
						mandatoryFieldNotAvailable = true;
						break;

					}
				} else {
					updateMessage = "Mandatory Field Missing in Uploaded Excel File";
					mandatoryFieldNotAvailable = true;
					break;
				}

			} else {
				updateMessage = "Mandatory Field Missing in Uploaded Excel File";
				mandatoryFieldNotAvailable = true;
				break;
			}

		}

		return mandatoryFieldNotAvailable;
	}

}
