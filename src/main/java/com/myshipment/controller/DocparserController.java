package com.myshipment.controller;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myshipment.dto.LoginDTO;
import com.myshipment.model.DashboardInfoBox;
import com.myshipment.model.MyshipmentTrackHeaderBean;
import com.myshipment.model.PreAlertHBL;
import com.myshipment.model.PreAlertSaveBLStatus;
import com.myshipment.model.RequestParams;
import com.myshipment.service.IDocparserService;
import com.myshipment.service.IinvoiceRouteService;
import com.myshipment.service.InvoiceRouteServiceImpl;
import com.myshipment.util.CommonConstant;
import com.myshipment.util.SessionUtil;
import com.myshipment.model.DocParserMetaDataModel;
import com.myshipment.model.InvoiceRouteModel;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FilenameUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Controller;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.ModelAndView;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

//This one single controller receives files from front end then sends it back to  here through one ajax call
//invoice_modal_uploader.js file
//returns json which is recieved by the ajax call and send back to the view
@Controller
public class DocparserController {
	@Autowired
	private IinvoiceRouteService invoiceRouteServiceImpl;
	@Autowired
	private IDocparserService docparserService;

	@RequestMapping(value = "/saveInvoice", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> saveInvoice(@RequestParam("invoice") MultipartFile uploadfile) {
		System.out.println("Inside The Controller");
		String valueFromDocParser = "";
		try {

			String encodedString = docparserService.convert(uploadfile);
			String uploadData = docparserService.docparserFileUpload(encodedString);
			Thread.sleep(30000);
			valueFromDocParser = docparserService.docparserFileRetrieve(uploadData);
			System.out.println(valueFromDocParser);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return ResponseEntity.accepted().body(valueFromDocParser);
	}

	@RequestMapping(value = "/routeinfo", method = RequestMethod.POST)
	@ResponseBody
	public InvoiceRouteModel routeinfo(@RequestParam(name = "destination") String destination , @RequestParam(name = "orderNumber") String orderNumber) {
		try {
			return invoiceRouteServiceImpl.routeInformation(destination,orderNumber);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	}

}
