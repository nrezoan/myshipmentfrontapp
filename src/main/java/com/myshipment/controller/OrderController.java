package com.myshipment.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.poi.util.SystemOutLogger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.myshipment.model.DocParserDataModel;
import com.myshipment.model.LineItemObjectModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.myshipment.dto.BuyersSuppliersmap;
import com.myshipment.dto.FileDTO;
import com.myshipment.dto.LoginDTO;
import com.myshipment.dto.OrderHeaderDTO;
import com.myshipment.dto.SupplierDetailsDTO;
import com.myshipment.model.ApprovedPurchaseOrder;
import com.myshipment.model.BookingByPoDTO;
import com.myshipment.model.BuyerDTO;
import com.myshipment.model.DirectBookingJsonData;
import com.myshipment.model.DirectBookingParams;
import com.myshipment.model.MaterialsDto;
import com.myshipment.model.OrderHeader;
import com.myshipment.model.OrderItem;
import com.myshipment.model.PreAlertSaveBLStatus;
import com.myshipment.model.PurchaseOrdersModel;
/*import com.myshipment.model.PreAlertSaveBLStatus;*/
import com.myshipment.model.RedirectParams;
import com.myshipment.model.SaveAsTemplateMdl;
import com.myshipment.model.StorageLocation;
import com.myshipment.model.WarehouseCode;
import com.myshipment.service.IDocparserLineItemService;
import com.myshipment.service.IOrderService;
import com.myshipment.service.IPoMgmtService;
/*import com.myshipment.service.IPreAlertManagementService;*/
import com.myshipment.util.DateUtil;
import com.myshipment.util.ServiceLocator;
import com.myshipment.util.SessionUtil;
import com.myshipment.util.StringUtil;

/**
 * @author virendra
 *
 */
@Controller
public class OrderController extends BaseController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 189274661948711716L;
	private Logger logger = Logger.getLogger(OrderController.class);
	@Autowired
	IOrderService orderService;

	@Autowired
	IPoMgmtService poMgmtService;

	@Autowired
	IDocparserLineItemService iDocparserLineLineItemService;

	/*
	 * @Autowired private IPreAlertManagementService preAlertManagementService;
	 */

	@RequestMapping(value = "/orderheader", method = RequestMethod.GET)
	public String getOrderView(Model model, HttpServletRequest request,
			@RequestParam(value = "appPurchOrder", defaultValue = "") String appPurchOrder) {
		HttpSession session = SessionUtil.getHttpSession(request);
		SupplierDetailsDTO supplierDetailsDTO = (SupplierDetailsDTO) session
				.getAttribute(SessionUtil.SUPPLIER_PRE_LOAD_DATA);
		if (supplierDetailsDTO != null) {
			BuyersSuppliersmap bsm = supplierDetailsDTO.getBuyersSuppliersmap();
			Map<String, String> mapdt = bsm.getDocTypes();
			if (mapdt.containsKey("ZCOR"))
				mapdt.remove("ZCOR");
			if (mapdt.containsKey("ZIBV"))
				mapdt.remove("ZIBV");
			if (mapdt.containsKey("ZMCE"))
				mapdt.remove("ZMCE");
			bsm.setDocTypes(mapdt);
			supplierDetailsDTO.setBuyersSuppliersmap(bsm);
			model.addAttribute("supplierDetails", supplierDetailsDTO);
		} else
			model.addAttribute("supplierDetails", new SupplierDetailsDTO());
		if (appPurchOrder != null && appPurchOrder != "") {
			// ApprovedPurchaseOrder
			// approvedPurchaseOrder=orderService.getAppPurchaseOrderById(Long.getLong(appPurchOrder));
			model.addAttribute("selectedPoId", appPurchOrder);
		}

		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		SaveAsTemplateMdl saveAsTemplateMdl = new SaveAsTemplateMdl();
		saveAsTemplateMdl.setCompany(loginDTO.getSalesOrgSelected());
		saveAsTemplateMdl.setCustomerCode(loginDTO.getLoggedInUserName());
		saveAsTemplateMdl.setOperationType(loginDTO.getDisChnlSelected());
		saveAsTemplateMdl.setSeaOrAir(loginDTO.getDivisionSelected());
		List<SaveAsTemplateMdl> lstTempInfo = orderService.getTemplateInfoByCred(saveAsTemplateMdl);
		model.addAttribute("templateInfo", lstTempInfo);
		List<StorageLocation> lstStorageLocation = orderService.getStorageLocationService("");
		model.addAttribute("lstStorageLocation", lstStorageLocation);

		/*
		 * List<StringBuffer> lstPortList = orderService.getPortList("");
		 * model.addAttribute("lstPortList", lstPortList);
		 */

		model.addAttribute("orderHeader", new OrderHeaderDTO());
		loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		if (loginDTO != null) {
			model.addAttribute("disChannel", loginDTO.getDisChnlSelected());
			String loggedInUserId = loginDTO.getLoggedInUserName();
			System.out.println(loggedInUserId);
			model.addAttribute("loggedInUserId",loggedInUserId);
		}
		return "directbookingheader";
	}

//working  area for doc parser

	@RequestMapping(value = "/docparserorderdetails", method = RequestMethod.POST)
	public String getDocparserOrderLineItemViewDirectBook(Model model, HttpServletRequest request,
			@ModelAttribute("orderHeader") OrderHeaderDTO orderHeader) {
		@SuppressWarnings("unused")

		ApprovedPurchaseOrder approvedPurchaseOrder = null;
		/*
		 * if (orderHeader.getAppId() != 0l) approvedPurchaseOrder =
		 * orderService.getAppPurchaseOrderById(orderHeader.getAppId());
		 */

		// Nazib for docparser integration STARTS
		System.out.println(orderHeader.getDocParserJson());
		try {
			LineItemObjectModel[] lineItems = iDocparserLineLineItemService
					.fetchDocument(orderHeader.getDocParserJson());
			Gson gson = new Gson();
			String json = gson.toJson(lineItems);
			System.out.println(json);
			model.addAttribute("lineItemJson", json);
		} catch (Exception e) {
			System.err.println(e);
		}

		// Nazib for docparser integration ENDS

		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		session.setAttribute("orderHeader", orderHeader);

		// nusrat
		List<BuyerDTO> buyerLst = orderService.getHMBuyers(orderHeader.getBuyer(), loginDTO.getDisChnlSelected(),
				loginDTO.getDivisionSelected(), loginDTO.getSalesOrgSelected());
		if (!buyerLst.isEmpty()) {
			String countryCode = StringUtil.splitCodefromString(orderHeader.getPortOfDischarge(), "-").substring(0, 2);
			List<WarehouseCode> whList = orderService.getWHList(countryCode);
			List<String> pmList = new ArrayList<>();
			List<WarehouseCode> whCodeLst = new ArrayList<>();
			session.setAttribute("warehouseList", whList);
			for (WarehouseCode wh : whList) {
				pmList.add(wh.getPm());
				/* WarehouseCode whCode = new WarehouseCode(); */
				/* whCode.setWhCode(wh.getWhCode()); */
				whCodeLst.add(wh);
			}
			List<String> pmListwithoutDuplicates = new ArrayList<>(new HashSet<>(pmList));
			model.addAttribute("pmList", pmListwithoutDuplicates);
			model.addAttribute("whCodeLst", whCodeLst);
		}
		model.addAttribute("hmBuyerList", buyerLst);
		List<MaterialsDto> lstMaterialsDto = orderService.getMaterialService("");
		model.addAttribute("materials", lstMaterialsDto);
		model.addAttribute("directBookingParams", new DirectBookingParams());
		model.addAttribute("lineItem", new OrderItem());
		if (orderHeader.getAppId() != null && !orderHeader.getAppId().equals("")) {
			model.addAttribute("approvedPurchaseOrderId", orderHeader.getAppId());
			return "bookingbypolineitem";
		} else
			return "directbookingDocparserItem";
	}

//working  area for doc parser

	@RequestMapping(value = "/orderdetails", method = RequestMethod.POST)
	public String getOrderLineItemViewDirectBook(Model model, HttpServletRequest request,
			@ModelAttribute("orderHeader") OrderHeaderDTO orderHeader) {
		@SuppressWarnings("unused")
		ApprovedPurchaseOrder approvedPurchaseOrder = null;
		/*
		 * if (orderHeader.getAppId() != 0l) approvedPurchaseOrder =
		 * orderService.getAppPurchaseOrderById(orderHeader.getAppId());
		 */
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		session.setAttribute("orderHeader", orderHeader);

		// nusrat
		List<BuyerDTO> buyerLst = orderService.getHMBuyers(orderHeader.getBuyer(), loginDTO.getDisChnlSelected(),
				loginDTO.getDivisionSelected(), loginDTO.getSalesOrgSelected());
		if (!buyerLst.isEmpty()) {
			String countryCode = StringUtil.splitCodefromString(orderHeader.getPortOfDischarge(), "-").substring(0, 2);
			List<WarehouseCode> whList = orderService.getWHList(countryCode);
			List<String> pmList = new ArrayList<>();
			List<WarehouseCode> whCodeLst = new ArrayList<>();
			session.setAttribute("warehouseList", whList);
			for (WarehouseCode wh : whList) {
				pmList.add(wh.getPm());
				/* WarehouseCode whCode = new WarehouseCode(); */
				/* whCode.setWhCode(wh.getWhCode()); */
				whCodeLst.add(wh);
			}
			List<String> pmListwithoutDuplicates = new ArrayList<>(new HashSet<>(pmList));
			model.addAttribute("pmList", pmListwithoutDuplicates);
			model.addAttribute("whCodeLst", whCodeLst);
		}
		model.addAttribute("hmBuyerList", buyerLst);
		List<MaterialsDto> lstMaterialsDto = orderService.getMaterialService("");
		model.addAttribute("materials", lstMaterialsDto);
		model.addAttribute("directBookingParams", new DirectBookingParams());
		model.addAttribute("lineItem", new OrderItem());
		if (orderHeader.getAppId() != null && !orderHeader.getAppId().equals("")) {
			model.addAttribute("approvedPurchaseOrderId", orderHeader.getAppId());
			return "bookingbypolineitem";
		} else
			return "directbookingitem";
	}

	@RequestMapping(value = "/orderdetails", method = RequestMethod.GET)
	@ResponseBody
	public List<ApprovedPurchaseOrder> getOrderLineItemViewBookByPO(Model model, HttpServletRequest request,
			@RequestParam("appId") String approvalId) {
		ApprovedPurchaseOrder approvedPurchaseOrder = null;
		List<ApprovedPurchaseOrder> lstApprovedPurchaseOrder = new ArrayList<>();
		String[] appPo = approvalId.split(",");
		if (null != appPo && appPo.length > 0)
			for (int i = 0; i < appPo.length; i++) {
				approvedPurchaseOrder = orderService.getAppPurchaseOrderById(Long.parseLong(appPo[i]));
				if (null != approvedPurchaseOrder)
					lstApprovedPurchaseOrder.add(approvedPurchaseOrder);
			}

		return lstApprovedPurchaseOrder;
	}

	@RequestMapping(value = "/saveorder", method = RequestMethod.POST)
	@ResponseBody
	public DirectBookingJsonData placeOrder(HttpServletRequest request, Model model,
			@RequestBody List<Map<String, Object>> orderDetails) {
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDto = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		OrderHeaderDTO orderHeaderDto = (OrderHeaderDTO) session.getAttribute("orderHeader");
		OrderHeader orderHeader = new OrderHeader();
		if (null != orderHeaderDto)
			BeanUtils.copyProperties(orderHeaderDto, orderHeader);
		orderHeader.setDistChannel(loginDto.getDisChnlSelected());
		orderHeader.setDivision(loginDto.getDivisionSelected());
		orderHeader.setSalesOrg(loginDto.getSalesOrgSelected());
		orderHeader.setShipper(loginDto.getLoggedInUserName());

		DirectBookingJsonData directBookingJsonData = null;

		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		directBookingJsonData = orderService.saveOrderDetailsAndPAckList(orderDetails, orderHeader, loginDTO);
		session.setAttribute("directBookingJsonData", directBookingJsonData);
		/* s */

		return directBookingJsonData;
	}

	@RequestMapping(value = "/saveorderforpo", method = RequestMethod.POST)
	@ResponseBody
	public DirectBookingJsonData saveorderforpo(HttpServletRequest request, Model model,
			@RequestBody BookingByPoDTO bookingByPoDTO) {
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDto = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		OrderHeaderDTO orderHeaderDto = (OrderHeaderDTO) session.getAttribute("orderHeader");
		OrderHeader orderHeader = new OrderHeader();
		if (null != orderHeaderDto)
			BeanUtils.copyProperties(orderHeaderDto, orderHeader);
		orderHeader.setDistChannel(loginDto.getDisChnlSelected());
		orderHeader.setDivision(loginDto.getDivisionSelected());
		orderHeader.setSalesOrg(loginDto.getSalesOrgSelected());
		orderHeader.setShipper(loginDto.getLoggedInUserName());

		DirectBookingJsonData directBookingJsonData = null;
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		directBookingJsonData = orderService.saveOrderDetailsAndPAckListByPo(loginDTO, orderHeader, bookingByPoDTO);
		session.setAttribute("directBookingJsonData", directBookingJsonData);

		return directBookingJsonData;
	}

	@RequestMapping(value = "/bookingresponse", method = RequestMethod.GET)
	public String bookingresponse(HttpServletRequest request, Model model) {
		HttpSession session = SessionUtil.getHttpSession(request);
		DirectBookingJsonData directBookingJsonData = (DirectBookingJsonData) session
				.getAttribute("directBookingJsonData");
		if (null != directBookingJsonData)
			model.addAttribute("jsonResponse", directBookingJsonData);

		PreAlertSaveBLStatus preAlertSaveBLStatus = (PreAlertSaveBLStatus) session.getAttribute("preAlertSaveBLStatus");

		if (preAlertSaveBLStatus != null) {
			model.addAttribute("preAlertSaveBLStatus", preAlertSaveBLStatus);
		}

		return "bookingresponse";
	}

	/*
	 * @RequestMapping(value = "/bookingresponse", method = RequestMethod.GET)
	 * public String getBookingResponse(Model model, DirectBookingJsonData
	 * directBookingJsonData) { model.addAttribute("directBookingJsonData",
	 * directBookingJsonData); return "bookingresponse"; }
	 */

	@RequestMapping(value = "/portdetails", method = RequestMethod.GET)
	@ResponseBody
	public List<StringBuffer> getPortDetails(@RequestParam("param") String param) {
		List<StringBuffer> lstPortList = orderService.getPortList(param.trim().toUpperCase());
		if (null != lstPortList)
			return lstPortList;
		else
			return new ArrayList<StringBuffer>();
	}

	@RequestMapping(value = "/portofreciept", method = RequestMethod.GET)
	@ResponseBody
	public List<StringBuffer> getPlaceOfReciept(@RequestParam("searchString") String searchString,
			HttpServletRequest request) {
		logger.info(this.getClass().getName() + "getPlaceOfReciept() Started");
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		String salesorg = "";
		if (null != loginDTO)
			salesorg = loginDTO.getSalesOrgSelected();
		List<StringBuffer> lstPortOfReceipt = orderService.placeOfReciept(searchString, salesorg);
		logger.info(this.getClass().getName() + "getPlaceOfReciept() : result is :" + lstPortOfReceipt);
		if (null != lstPortOfReceipt)
			return lstPortOfReceipt;
		else
			return new ArrayList<StringBuffer>();

	}

	@RequestMapping(value = "/buyerwiseposearch", method = RequestMethod.GET)
	public String getBuyerSearchView(HttpServletRequest request, Model model) {
		HttpSession session = SessionUtil.getHttpSession(request);
		SupplierDetailsDTO supplierDetailsDTO = (SupplierDetailsDTO) session
				.getAttribute(SessionUtil.SUPPLIER_PRE_LOAD_DATA);
		if (supplierDetailsDTO != null)
			model.addAttribute("buyer", supplierDetailsDTO.getBuyersSuppliersmap().getBuyers());
		else
			model.addAttribute("buyer", new HashMap<String, String>());
		return "buyerwiseposearch";

	}

	@RequestMapping(value = "/approvednotbookedpo", method = RequestMethod.GET)

	public String getApprovedNotBookedPO(@RequestParam("po-number") String poNumber,
			@RequestParam("buyer") String buyer, /*
													 * @RequestParam("from_date") String fromDate,
													 * 
													 * @RequestParam("to_date") String toDate,
													 */ Model model, HttpServletRequest request) {
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		SupplierDetailsDTO supplierDetailsDTO = (SupplierDetailsDTO) session
				.getAttribute(SessionUtil.SUPPLIER_PRE_LOAD_DATA);
		String selectedValue = buyer;

		if (supplierDetailsDTO != null)
			model.addAttribute("buyer", supplierDetailsDTO.getBuyersSuppliersmap().getBuyers());
		else
			model.addAttribute("buyer", new HashMap<String, String>());
		/*
		 * List<ApprovedPurchaseOrder> lstAppPo =
		 * orderService.getApprovedPONotBooked(poNumber, buyer, fromDate, toDate);
		 */
		/*
		 * List<ApprovedPurchaseOrder> lstAppPo = orderService.getPOforBooking(buyer,
		 * poNumber, loginDTO.getSalesOrgSelected(), loginDTO.getDivisionSelected(),
		 * loginDTO.getLoggedInUserName());
		 */
		List<PurchaseOrdersModel> lstAppPo = orderService.getPOforBooking(buyer, poNumber,
				loginDTO.getLoggedInUserName());
		List<MaterialsDto> lstMaterialsDto = orderService.getMaterialService("");
		model.addAttribute("materials", lstMaterialsDto);
		model.addAttribute("approvedpo", lstAppPo);
		model.addAttribute("selectedValue", selectedValue);
		return "buyerwiseposearch";
	}

	@RequestMapping(value = "/uploadLineItemExcel", method = RequestMethod.POST)
	public String readExcelData(@ModelAttribute("fileData") FileDTO fileData, @RequestParam("file") MultipartFile file,
			RedirectAttributes redirectAttributes) {

		logger.info("UploadPODataController.processExcelData method starts.");

		@SuppressWarnings("unused")
		List<Long> poIdLst = null;
		if (fileData != null) {

			// MultipartFile uploadedFile = file.getOriginalFilename()l

			logger.info("File name is : " + file.getOriginalFilename());
			System.out.println("File name is : " + file.getOriginalFilename());
			// poIdLst = uploadPODataService.processData(file);

			logger.info("UploadPODataController.processExcelData method ends.");
		}
		RedirectParams redirectParams = new RedirectParams();
		// redirectParams.setPoIdLst(poIdLst);
		redirectAttributes.addFlashAttribute("redirectParams", redirectParams);

		return "redirect:/getResults";
	}

	@RequestMapping(value = "/saveDataAsTemp", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public void SaveAsTemplate(@RequestParam("temp_name") String temp_name, @RequestParam("json_data") String json_data,
			HttpServletRequest request) {
		orderService = (IOrderService) ServiceLocator.findService("orderService");
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		SaveAsTemplateMdl bookingInfo = orderService.getTemplateInfoById(temp_name, loginDTO.getLoggedInUserName(),
				loginDTO.getSalesOrgSelected(), loginDTO.getDisChnlSelected(), loginDTO.getDivisionSelected());
		if (bookingInfo == null) {
			bookingInfo = new SaveAsTemplateMdl();

			bookingInfo.setTemplateName(temp_name);
			bookingInfo.setTemplateData(json_data);
			bookingInfo.setCompany(loginDTO.getSalesOrgSelected());
			bookingInfo.setCustomerCode(loginDTO.getLoggedInUserName());
			bookingInfo.setOperationType(loginDTO.getDisChnlSelected());
			bookingInfo.setSeaOrAir(loginDTO.getDivisionSelected());
			bookingInfo.setCreatedBy(loginDTO.getLoggedInUserName());
			bookingInfo.setCreatedDate(DateUtil.formatDateToString(new Date()));
			bookingInfo.setIsActive("1");
			orderService.saveBookingHeaderAsTemplate(bookingInfo);
		} else {
			orderService.updateTemplateInfo(String.valueOf(bookingInfo.getTemplate_Id()), json_data);
		}

	}

	@RequestMapping(value = "/getTempateInfo", method = RequestMethod.GET)
	@ResponseStatus(value = HttpStatus.OK)
	public @ResponseBody SaveAsTemplateMdl getTemplateInfoById(@RequestParam("template_Id") String template_Id,
			HttpServletRequest request) {
		orderService = (IOrderService) ServiceLocator.findService("orderService");
		SaveAsTemplateMdl mdl = null;
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		mdl = orderService.getTemplateInfoById(template_Id, loginDTO.getLoggedInUserName(),
				loginDTO.getSalesOrgSelected(), loginDTO.getDisChnlSelected(), loginDTO.getDivisionSelected());
		return mdl;
	}

	@RequestMapping(value = "/updateTempInfo", method = RequestMethod.POST)
	@ResponseStatus(value = HttpStatus.OK)
	public void UpdateTemplate(@RequestParam("temp_Id") String temp_Id, @RequestParam("json_data") String json_data,
			HttpServletRequest request) {
		orderService = (IOrderService) ServiceLocator.findService("orderService");
		orderService.updateTemplateInfo(temp_Id, json_data);

	}

	@RequestMapping(value = "/getMaterials", method = RequestMethod.GET)
	@ResponseBody
	public List<MaterialsDto> getMaterials(@RequestParam("param") String param) {
		List<MaterialsDto> lstPortList = orderService.getMaterialService(param);
		if (null != lstPortList)
			return lstPortList;
		else
			return null;
	}

	@RequestMapping(value = "/poBooking", method = RequestMethod.POST)
	@ResponseBody
	public List<PurchaseOrdersModel> poTest(@RequestBody String param, HttpServletRequest request) {

		HttpSession session = SessionUtil.getHttpSession(request);
		Gson gson = new Gson();
		PurchaseOrdersModel[] itemList = gson.fromJson(param, PurchaseOrdersModel[].class);

		session.setAttribute("itemList", Arrays.asList(itemList));
		return Arrays.asList(itemList);

	}

	/*
	 * @RequestMapping(value = "/getHMBuyers", method = RequestMethod.GET)
	 * 
	 * @ResponseBody public List<BuyerDTO> getHMBuyers(HttpServletRequest request) {
	 * HttpSession session = SessionUtil.getHttpSession(request); LoginDTO loginDTO
	 * = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS); List<BuyerDTO>
	 * buyerLst = orderService.getHMBuyers(loginDTO.getDisChnlSelected(),
	 * loginDTO.getDivisionSelected(), loginDTO.getSalesOrgSelected()); if (buyerLst
	 * != null) { return buyerLst; } else { return null; } }
	 */

	@RequestMapping(value = "/powiseheader", method = RequestMethod.GET)
	public String getHeaderPage(Model model, HttpServletRequest request) {
		HttpSession session = SessionUtil.getHttpSession(request);
		List<PurchaseOrdersModel> itemList = (List<PurchaseOrdersModel>) session.getAttribute("itemList");
		SupplierDetailsDTO supplierDetailsDTO = (SupplierDetailsDTO) session
				.getAttribute(SessionUtil.SUPPLIER_PRE_LOAD_DATA);
		if (supplierDetailsDTO != null) {
			BuyersSuppliersmap bsm = supplierDetailsDTO.getBuyersSuppliersmap();
			Map<String, String> mapdt = bsm.getDocTypes();
			if (mapdt.containsKey("ZCOR"))
				mapdt.remove("ZCOR");
			if (mapdt.containsKey("ZIBV"))
				mapdt.remove("ZIBV");
			if (mapdt.containsKey("ZMCE"))
				mapdt.remove("ZMCE");
			bsm.setDocTypes(mapdt);
			supplierDetailsDTO.setBuyersSuppliersmap(bsm);
			model.addAttribute("supplierDetails", supplierDetailsDTO);
		} else
			model.addAttribute("supplierDetails", new SupplierDetailsDTO());

		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		SaveAsTemplateMdl saveAsTemplateMdl = new SaveAsTemplateMdl();
		saveAsTemplateMdl.setCompany(loginDTO.getSalesOrgSelected());
		saveAsTemplateMdl.setCustomerCode(loginDTO.getLoggedInUserName());
		saveAsTemplateMdl.setOperationType(loginDTO.getDisChnlSelected());
		saveAsTemplateMdl.setSeaOrAir(loginDTO.getDivisionSelected());
		List<SaveAsTemplateMdl> lstTempInfo = orderService.getTemplateInfoByCred(saveAsTemplateMdl);
		model.addAttribute("templateInfo", lstTempInfo);
		List<StorageLocation> lstStorageLocation = orderService.getStorageLocationService("");
		model.addAttribute("lstStorageLocation", lstStorageLocation);

		// model.addAttribute("orderHeader", new OrderHeaderDTO());

		OrderHeaderDTO header = new OrderHeaderDTO();
		if (itemList.get(0) != null) {
			header.setBuyer(itemList.get(0).getBuyer_id());
			header.setFreightMode(itemList.get(0).getFreight_mode());
			header.setTos(itemList.get(0).getTerms_of_shipment());
			header.setComInvNo(itemList.get(0).getComm_inv());
			header.setComInvDate(itemList.get(0).getComm_inv_date());
			header.setCargoHandoverDate(itemList.get(0).getCargo_handover_date());
		}
		model.addAttribute("orderHeader", header);
		model.addAttribute("itemList", itemList);
		loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		if (loginDTO != null)
			model.addAttribute("disChannel", loginDTO.getDisChnlSelected());

		return "powisebookingheader";
	}

	@RequestMapping(value = "/savePoWiseBooking", method = RequestMethod.POST)
	public String savePOOrder(Model model, HttpServletRequest request,
			@ModelAttribute("orderHeader") OrderHeaderDTO orderHeaderDto) {
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDto = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		OrderHeader orderHeader = new OrderHeader();
		if (null != orderHeaderDto)
			BeanUtils.copyProperties(orderHeaderDto, orderHeader);
		orderHeader.setDistChannel(loginDto.getDisChnlSelected());
		orderHeader.setDivision(loginDto.getDivisionSelected());
		orderHeader.setSalesOrg(loginDto.getSalesOrgSelected());
		orderHeader.setShipper(loginDto.getLoggedInUserName());
		DirectBookingJsonData directBookingJsonData = null;
		List<PurchaseOrdersModel> itemList = (List<PurchaseOrdersModel>) session.getAttribute("itemList");
		directBookingJsonData = orderService.saveOrderDetailsByPo(loginDto, orderHeader, itemList);
		if (null != directBookingJsonData)
			model.addAttribute("jsonResponse", directBookingJsonData);

		PreAlertSaveBLStatus preAlertSaveBLStatus = (PreAlertSaveBLStatus) session.getAttribute("preAlertSaveBLStatus");

		if (preAlertSaveBLStatus != null) {
			model.addAttribute("preAlertSaveBLStatus", preAlertSaveBLStatus);
		}

		return "bookingresponse";
	}

}
