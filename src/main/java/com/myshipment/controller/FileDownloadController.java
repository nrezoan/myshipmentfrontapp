package com.myshipment.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.myshipment.dto.LoginDTO;
import com.myshipment.model.DirectBookingParams;
import com.myshipment.service.IFileDownloadService;
import com.myshipment.util.SessionUtil;

@Controller
@RequestMapping("/download")
public class FileDownloadController {
	
	@Autowired
	private IFileDownloadService fileDownloadService;
	
	private Logger logger = Logger.getLogger(FileDownloadController.class);
	
	@RequestMapping("/xls/{fileName:.+}")
	public void downloadXLSResource(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("fileName") String fileName) {
		ServletContext context = request.getServletContext();
		String fileSeparator=System.getProperty("file.separator");
		logger.debug(this.getClass().getName()+" downloadXLSResource() --Start File Name= "+fileName );
		String dataDirectory = request.getServletContext().getRealPath(fileSeparator+"WEB-INF"+fileSeparator+"classes"+fileSeparator+"templates"+fileSeparator);
		logger.debug("file path= "+dataDirectory);
		Path file = Paths.get(dataDirectory, fileName);
		// File file2=new File(dataDirectory+fileName);
		String mimeType = context.getMimeType(dataDirectory + fileName);
		if (Files.exists(file)) {
			if (mimeType == null)
				mimeType = "application/octet-stream";
			response.setContentType(mimeType);

			response.addHeader("Content-Disposition", "attachment; filename=" + fileName);
			try {
				Files.copy(file, response.getOutputStream());
				response.getOutputStream().flush();
				logger.debug(this.getClass().getName()+" downloadXLSResource( )  --end");
			} catch (IOException ex) {
				logger.debug(this.getClass().getName()+""+ex.getStackTrace());
				//ex.printStackTrace();
			}
		}
	}

	@RequestMapping("/excel/{fileName:.+}")
	public void downloadExcelFileBokingUpdate(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("fileName") String fileName) {
		ServletContext context = request.getServletContext();
		logger.debug(this.getClass().getName()+" downloadExcelFileBokingUpdate() --Start File Name= "+fileName );
		String fileSeparator=System.getProperty("file.separator");
		String dataDirectory = request.getServletContext().getRealPath(fileSeparator+"WEB-INF"+fileSeparator+"classes"+fileSeparator+"templates"+fileSeparator);
		logger.debug("file path= "+dataDirectory);
		Path file = Paths.get(dataDirectory, fileName);
		logger.debug(file.toAbsolutePath());
		logger.debug(file.toAbsolutePath());
		HttpSession session=SessionUtil.getHttpSession(request);
		DirectBookingParams directBookingParams=(DirectBookingParams)session.getAttribute(SessionUtil.BOOKING_UPDATE_ATTRIBUTE);
		logger.debug(this.getClass().getName()+" downloadExcelFileBokingUpdate() -- Calling to create file" );
		fileDownloadService.createAndDownloadExcel(directBookingParams, fileName, dataDirectory);
		// File file2=new File(dataDirectory+fileName);
		String mimeType = context.getMimeType(dataDirectory + fileName);
		if (Files.exists(file)) {
			if (mimeType == null)
				mimeType = "application/octet-stream";
			response.setContentType(mimeType);

			response.addHeader("Content-Disposition", "attachment; filename=" + fileName);
			try {
				Files.copy(file, response.getOutputStream());
				response.getOutputStream().flush();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

	}
	@RequestMapping("/excel/dashboard/{fileName:.+}")
	public void downloadExcelFileDashboard(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("fileName") String fileName) {
		ServletContext context = request.getServletContext();
		logger.debug(this.getClass().getName()+" downloadExcelFileDashboard() --Start File Name= "+fileName );
		String fileSeparator=System.getProperty("file.separator");
		String dataDirectory = request.getServletContext().getRealPath(fileSeparator+"WEB-INF"+fileSeparator+"classes"+fileSeparator+"templates"+fileSeparator);
		logger.debug("file path= "+dataDirectory);
		Path file = Paths.get(dataDirectory, fileName);
		HttpSession session=SessionUtil.getHttpSession(request);
		LoginDTO  loginDto  =(LoginDTO)session.getAttribute(SessionUtil.LOGIN_DETAILS);
		
		fileDownloadService.createAndDownloadDashboardExcel(loginDto, fileName, dataDirectory);
		// File file2=new File(dataDirectory+fileName);
		String mimeType = context.getMimeType(dataDirectory + fileName);
		if (Files.exists(file)) {
			if (mimeType == null)
				mimeType = "application/octet-stream";
			response.setContentType(mimeType);

			response.addHeader("Content-Disposition", "attachment; filename=" + fileName);
			try {
				Files.copy(file, response.getOutputStream());
				response.getOutputStream().flush();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}

	}	
    @RequestMapping("/pdf/{fileName:.+}")
    public void downloadPDFResource( HttpServletRequest request,
                                     HttpServletResponse response,
                                     @PathVariable("fileName") String fileName)
    {
    	logger.debug(this.getClass().getName()+" downloadManual() --Start File Name= "+fileName );
    	String fileSeparator=System.getProperty("file.separator");
        String dataDirectory = request.getServletContext().getRealPath(fileSeparator+"WEB-INF"+fileSeparator+"classes"+fileSeparator+"manual"+fileSeparator);
        logger.debug("file path= "+dataDirectory);
        Path file = Paths.get(dataDirectory, fileName);
        if (Files.exists(file))
        {
            response.setContentType("application/pdf");
            response.addHeader("Content-Disposition", "attachment; filename="+fileName);
            try
            {
                Files.copy(file, response.getOutputStream());
                response.getOutputStream().flush();
            }
            catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
    
	@RequestMapping("/generalTemplateExcel/{fileName:.+}")
	public void downloadGeneralTemplate(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("fileName") String fileName) {
		ServletContext context = request.getServletContext();
		String fileSeparator=System.getProperty("file.separator");
		logger.debug(this.getClass().getName()+" downloadXLSResource() --Start File Name= "+fileName );
		String dataDirectory = request.getServletContext().getRealPath(fileSeparator+"WEB-INF"+fileSeparator+"classes"+fileSeparator+"templates"+fileSeparator);
		logger.debug("file path= "+dataDirectory);
		Path file = Paths.get(dataDirectory, fileName);
		// File file2=new File(dataDirectory+fileName);
		String mimeType = context.getMimeType(dataDirectory + fileName);
		if (Files.exists(file)) {
			if (mimeType == null)
				mimeType = "application/octet-stream";
			response.setContentType(mimeType);

			response.addHeader("Content-Disposition", "attachment; filename=" + fileName);
			try {
				Files.copy(file, response.getOutputStream());
				response.getOutputStream().flush();
				logger.debug(this.getClass().getName()+" downloadXLSResource( )  --end");
			} catch (IOException ex) {
				logger.debug(this.getClass().getName()+""+ex.getStackTrace());
				//ex.printStackTrace();
			}
		}
	}
    
    
    @RequestMapping("/zip/{fileName:.+}")
	public void downloadPreAlertZip(HttpServletRequest request, HttpServletResponse response,
			@PathVariable("fileName") String fileName) {
		logger.debug(this.getClass().getName() + " downloadPreAlertZip() -- Start File Name= " + fileName);
		String fileSeparator = System.getProperty("file.separator");
		String dataDirectory = request.getServletContext().getRealPath(fileSeparator + "WEB-INF" + fileSeparator
				+ "classes" + fileSeparator + "pre-alerts" + fileSeparator + "mawb" + fileSeparator);
		logger.debug("file path= " + dataDirectory);
		Path file = Paths.get(dataDirectory, fileName);
		if (Files.exists(file)) {
			response.setContentType("application/zip");
			response.addHeader("Content-Disposition", "attachment; filename=" + fileName);
			try {
				Files.copy(file, response.getOutputStream());
				response.getOutputStream().flush();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
}
