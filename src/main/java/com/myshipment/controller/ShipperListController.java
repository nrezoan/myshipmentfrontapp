package com.myshipment.controller;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.myshipment.dto.LoginDTO;
import com.myshipment.dto.SupplierDetailsDTO;
import com.myshipment.model.CRMLogger;
import com.myshipment.model.SupRequestParams;
import com.myshipment.service.ILoginService;
import com.myshipment.service.IUpdateLogger;
import com.myshipment.util.SessionUtil;

@Controller
public class ShipperListController {
	@Autowired
	ILoginService loginService;

	private Logger logger = Logger.getLogger(UpdateLoggerController.class);
	String buyerSelected = "";

	@RequestMapping(value = "/shipperList", method = RequestMethod.GET)
	public String getShipperLogger(Model model, HttpServletRequest request) {
		logger.info(this.getClass() + ": Update Logger...");

		HttpSession session = SessionUtil.getHttpSession(request);

		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		try {
			if (loginDTO.getCrmDashboardBuyerId() != null) {
				buyerSelected = loginDTO.getCrmDashboardBuyerId();
				SupRequestParams supRequestParams = new SupRequestParams();
				supRequestParams.setCustomerId(buyerSelected);
				supRequestParams.setDistChan(loginDTO.getDisChnlSelected());
				supRequestParams.setDivision(loginDTO.getDivisionSelected());
				supRequestParams.setSalesOrg(loginDTO.getSalesOrgSelected());

				SupplierDetailsDTO supplierDetailsDTO = loginService.getSupplierPreLoadedData(supRequestParams,
						session);
				/* session.setAttribute(SessionUtil.SHIPPER_LIST, supplierDetailsDTO); */
				CRMLogger req = new CRMLogger();
				if (supplierDetailsDTO.getBuyersSuppliersmap() != null) {
					model.addAttribute("shipper", supplierDetailsDTO.getBuyersSuppliersmap().getBuyers());
					model.addAttribute("req", req);
				} else {
					model.addAttribute("shipper", new HashMap<String, String>());
					model.addAttribute("req", req);
				}
			}
		} catch (Exception e) {
			logger.info(this.getClass() + ": Error occured while fetching buyerwise shipper list...");
			e.printStackTrace();
		}

		return "buyerWiseShipperList";
	}
}