package com.myshipment.controller;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.myshipment.dto.LoginDTO;
import com.myshipment.model.PurchaseOrdersModel;
import com.myshipment.model.TemplateModel;
import com.myshipment.service.ITemplateMappingService;
import com.myshipment.util.ReadExcelFile;
import com.myshipment.util.SessionUtil;

@Controller
public class DynamicPoUploadController {

	@Autowired
	private ITemplateMappingService templateMappingService;

	@Autowired
	private ReadExcelFile readExcelFile;

	private Logger logger = Logger.getLogger(DynamicPoUploadController.class);

	@RequestMapping(value = "/sendTemplate", method = RequestMethod.GET)
	public String sendPage(HttpServletRequest request, Model model) {
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		model.addAttribute("buyer", loginDTO.getSupplierDetailsDTO().getBuyersSuppliersmap().getBuyers()
				.get(loginDTO.getCrmDashboardBuyerId()));
		return "dynamicPoUp";
	}

	@RequestMapping(value = "/buyerTemplate", method = RequestMethod.GET)
	public String templateByBuyer(HttpServletRequest request, Model model) {
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		model.addAttribute("buyer", loginDTO.getPeAddress().getName());
		return "dynamicPoUp";
	}

	@RequestMapping(value = "/sendPoUp", method = RequestMethod.GET)
	public String sendPoUpPage(HttpServletRequest request, Model model) {
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		model.addAttribute("buyer", loginDTO.getSupplierDetailsDTO().getBuyersSuppliersmap().getBuyers()
				.get(loginDTO.getCrmDashboardBuyerId()));
		return "poFileUpload";
	}

	@RequestMapping(value = "/sendPoUpByBuyer", method = RequestMethod.GET)
	public String sendPoUpByBuyer(HttpServletRequest request, Model model) {
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		model.addAttribute("buyer", loginDTO.getPeAddress().getName());
		return "poUploadByBuyer";
	}

	@RequestMapping(value = "/poUpdatePage", method = RequestMethod.GET)
	public String getPOUpdatePage(HttpServletRequest request, Model model) {
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		PurchaseOrdersModel poModel = new PurchaseOrdersModel();
		model.addAttribute("poModel", poModel);
		model.addAttribute("buyer", loginDTO.getSupplierDetailsDTO().getBuyersSuppliersmap().getBuyers()
				.get(loginDTO.getCrmDashboardBuyerId()));
		return "poUpdate";
	}

	@RequestMapping(value = "/getPOForUpdate", method = RequestMethod.POST)
	public String getPOForUpdate(@ModelAttribute("poModel") PurchaseOrdersModel poModel, HttpServletRequest request,
			Model model) {
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		List<PurchaseOrdersModel> orderList = templateMappingService.getPoDetails(poModel.getPo_no(),
				loginDTO.getCrmDashboardBuyerId());
		model.addAttribute("orderList", orderList);
		return "poUpdate";
	}

	@RequestMapping(value = "/poForUpdate", method = RequestMethod.POST)
	public ResponseEntity<Object> poForUpdate(@RequestBody String param, HttpServletRequest request, Model model) {
		HttpSession session = SessionUtil.getHttpSession(request);
		Gson gson = new Gson();
		PurchaseOrdersModel[] itemList = gson.fromJson(param, PurchaseOrdersModel[].class);
		List<PurchaseOrdersModel> poLlist = Arrays.asList(itemList);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		templateMappingService.updatePoDetails(poLlist);
		// model.addAttribute("orderList", orderList);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@RequestMapping(value = "/saveTemplate", method = RequestMethod.POST)
	public ResponseEntity<Object> saveTemplate(@RequestBody String templateJsonString, ModelMap model,
			HttpServletRequest request) {

		Gson gson = new Gson();
		HttpSession session = SessionUtil.getHttpSession(request);
		TemplateModel template = gson.fromJson(templateJsonString, TemplateModel.class);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		try {
			if (loginDTO.getAccgroup().equalsIgnoreCase("ZMBY")) {
				template.setTemplate_id(loginDTO.getLoggedInUserName());
				template.setActive(1);
				template.setEntry_by(loginDTO.getLoggedInUserName());
				template.setEntry_date(new Date());
				logger.info("Template model: " + template);
				Boolean check = templateMappingService.saveTemplateMapping(template);
				if (check) {
					return new ResponseEntity<Object>("unsuccesful", HttpStatus.BAD_REQUEST);
				} else {
					return new ResponseEntity<>(HttpStatus.CREATED);

				}
			} else {
				if (loginDTO.getCrmDashboardBuyerId() != null) {
					template.setTemplate_id(loginDTO.getCrmDashboardBuyerId());
					template.setActive(1);
					template.setEntry_by(loginDTO.getLoggedInUserName());
					template.setEntry_date(new Date());
					logger.info("Template model: " + template);
					Boolean check = templateMappingService.saveTemplateMapping(template);
					if (check) {
						return new ResponseEntity<Object>("unsuccesful", HttpStatus.BAD_REQUEST);
					} else {
						return new ResponseEntity<>(HttpStatus.CREATED);

					}
				}
			}
		} catch (Exception e) {
			logger.info(this.getClass() + ": Error occured while fetching buyerwise shipper list...");
			e.printStackTrace();
		}

		return null;
	}

	@RequestMapping(value = "/updateTemplate", method = RequestMethod.POST)
	public ResponseEntity<Object> updateTemplate(@RequestBody String templateJsonString, ModelMap model,
			HttpServletRequest request) {

		Gson gson = new Gson();
		HttpSession session = SessionUtil.getHttpSession(request);
		TemplateModel template = gson.fromJson(templateJsonString, TemplateModel.class);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		try {
			if (loginDTO.getAccgroup().equalsIgnoreCase("ZMBY")) {
				template.setTemplate_id(loginDTO.getLoggedInUserName());
				template.setActive(1);
				template.setEntry_by(loginDTO.getLoggedInUserName());
				template.setEntry_date(new Date());
				logger.info("Template model: " + template);
				templateMappingService.updateTemplateMapping(template);
				return new ResponseEntity<>(HttpStatus.CREATED);
			} else {
				if (loginDTO.getCrmDashboardBuyerId() != null) {
					template.setTemplate_id(loginDTO.getCrmDashboardBuyerId());
					template.setActive(1);
					template.setEntry_by(loginDTO.getLoggedInUserName());
					template.setEntry_date(new Date());
					logger.info("Template model: " + template);
					templateMappingService.updateTemplateMapping(template);
					return new ResponseEntity<>(HttpStatus.CREATED);
				}
			}
		} catch (Exception e) {
			logger.info(this.getClass() + ": Error occured while fetching buyerwise shipper list...");
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return null;
	}

	@RequestMapping(value = "/fileUpload", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> uploadFile(@ModelAttribute(value = "file") MultipartFile uploadfile,
			HttpServletRequest request) {

		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		String dateFormat = request.getParameterValues("dateFormat")[0];
		TemplateModel template = null;

		try {
			if (loginDTO.getAccgroup().equalsIgnoreCase("ZMBY")) {
				template = templateMappingService.getTemplate(loginDTO.getLoggedInUserName());
				if (template == null) {
					return new ResponseEntity<>(HttpStatus.NOT_FOUND);
				} else {
					String filename = uploadfile.getOriginalFilename();

					logger.info("File Name: " + filename);
					File file = convert(uploadfile);

					List<PurchaseOrdersModel> purchaseOrderList = readExcelFile.readExcel(file, template,
							loginDTO.getLoggedInUserName(), loginDTO.getLoggedInUserName());
					for (PurchaseOrdersModel order : purchaseOrderList) {
						if (order.getCargo_handover_date() != null && !order.getCargo_handover_date().equals("")) {
							order.setCargo_handover_date(dateConversion(dateFormat, order.getCargo_handover_date()));
						}
						if (order.getComm_inv_date() != null && !order.getComm_inv_date().equals("")) {
							order.setComm_inv_date(dateConversion(dateFormat, order.getComm_inv_date()));
						}
					}
					templateMappingService.savePoDetails(purchaseOrderList);
					return new ResponseEntity<>(HttpStatus.CREATED);
				}
			} else {
				if (loginDTO.getCrmDashboardBuyerId() != null) {

					template = templateMappingService.getTemplate(loginDTO.getCrmDashboardBuyerId());
					if (template == null) {
						return new ResponseEntity<>(HttpStatus.NOT_FOUND);
					} else {
						String filename = uploadfile.getOriginalFilename();

						logger.info("File Name: " + filename);
						File file = convert(uploadfile);

						List<PurchaseOrdersModel> purchaseOrderList = readExcelFile.readExcel(file, template,
								loginDTO.getCrmDashboardBuyerId(), loginDTO.getLoggedInUserName());
						for (PurchaseOrdersModel order : purchaseOrderList) {
							if (order.getCargo_handover_date() != null && !order.getCargo_handover_date().equals("")) {
								order.setCargo_handover_date(
										dateConversion(dateFormat, order.getCargo_handover_date()));
							}
							if (order.getComm_inv_date() != null && !order.getComm_inv_date().equals("")) {
								order.setComm_inv_date(dateConversion(dateFormat, order.getComm_inv_date()));
							}
						}
						templateMappingService.savePoDetails(purchaseOrderList);
						return new ResponseEntity<>(HttpStatus.CREATED);
					}
				}
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return null;
	}

	@RequestMapping(value = "general/fileUpload", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<?> uploadFileGeneralTemplate(@ModelAttribute(value = "file") MultipartFile uploadfile,
			HttpServletRequest request) {

		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		String dateFormat = request.getParameterValues("dateFormat")[0];

		try {

			if (loginDTO.getCrmDashboardBuyerId() != null) {

				String filename = uploadfile.getOriginalFilename();

				logger.info("File Name: " + filename);
				File file = convert(uploadfile);

				List<PurchaseOrdersModel> purchaseOrderList = readExcelFile.readExcelGeneralTemplate(file,
						loginDTO.getCrmDashboardBuyerId(), loginDTO.getLoggedInUserName());
				for (PurchaseOrdersModel order : purchaseOrderList) {
					if (order.getCargo_handover_date() != null && !order.getCargo_handover_date().equals("")) {
						order.setCargo_handover_date(dateConversion(dateFormat, order.getCargo_handover_date()));
					}
					if (order.getComm_inv_date() != null && !order.getComm_inv_date().equals("")) {
						order.setComm_inv_date(dateConversion(dateFormat, order.getComm_inv_date()));
					}
				}
				templateMappingService.savePoDetails(purchaseOrderList);
				return new ResponseEntity<>(HttpStatus.CREATED);
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return null;
	}

	public File convert(MultipartFile file) throws IOException {

		File convFile = new File(file.getOriginalFilename());
		convFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}

	public String dateConversion(String format, String unformattedDate) {
		SimpleDateFormat formatter = null;
		if (format.equalsIgnoreCase("DD-MM-YYYY")) {
			formatter = new SimpleDateFormat("dd-MM-yyyy");

		} else if (format.equalsIgnoreCase("DDMMYYYY")) {
			formatter = new SimpleDateFormat("ddMMMyyyy");

		} else if (format.equalsIgnoreCase("MM-DD-YYYY")) {
			formatter = new SimpleDateFormat("MM-dd-yyyy");

		} else if (format.equalsIgnoreCase("DD-MMM-YYYY")) {
			formatter = new SimpleDateFormat("dd-MMM-yyyy");

		} else if (format.equalsIgnoreCase("YYYY-MM-DD")) {
			formatter = new SimpleDateFormat("yyyy-MM-dd");

		} else if (format.equalsIgnoreCase("YYYY-DD-MM")) {
			formatter = new SimpleDateFormat("yyyy-dd-MM");

		} else if (format.equalsIgnoreCase("DD/MM/YYYY")) {
			formatter = new SimpleDateFormat("dd/MM/yyyy");

		} else if (format.equalsIgnoreCase("MM/DD/YYYY")) {
			formatter = new SimpleDateFormat("MM/dd/yyyy");

		} else if (format.equalsIgnoreCase("DD/MMM/YYYY")) {
			formatter = new SimpleDateFormat("dd/MMM/yyyy");

		} else if (format.equalsIgnoreCase("YYYY/MM/DD")) {
			formatter = new SimpleDateFormat("yyyy/MM/dd");

		} else if (format.equalsIgnoreCase("DD.MM.YYYY")) {
			formatter = new SimpleDateFormat("dd.MM.yyyy");

		} else if (format.equalsIgnoreCase("MM.DD.YYYY")) {
			formatter = new SimpleDateFormat("MM.dd.yyyy");

		} else if (format.equalsIgnoreCase("DD.MMM.YYYY")) {
			formatter = new SimpleDateFormat("dd.MMM.yyyy");
		} else if (format.equalsIgnoreCase("YYYY.MM.DD")) {
			formatter = new SimpleDateFormat("yyyy.MM.dd");

		}

		try {
			Date formattedDate = formatter.parse(unformattedDate);
			unformattedDate = new SimpleDateFormat("yyyy-MM-dd").format(formattedDate);
			return unformattedDate;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	@RequestMapping(value = "/deletePO", method = RequestMethod.POST)
	public ResponseEntity<Object> deletePO(@RequestBody String app_id, HttpServletRequest request, Model model) {

		try {
			templateMappingService.deletePO(app_id);
			return new ResponseEntity<>(HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

	}

}
