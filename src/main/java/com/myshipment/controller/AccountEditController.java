/*package com.myshipment.controller;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.myshipment.dto.LoginDTO;
import com.myshipment.model.BapiRet1;
import com.myshipment.model.DetailsEditParam;
import com.myshipment.service.IAccountEdit;
import com.myshipment.util.EncryptDecryptUtil;
import com.myshipment.util.MyshipmentJspMapper;

@Controller
public class AccountEditController {

	//private static final long serialVersionUID = 1L;

	@Autowired
	private IAccountEdit editDetailService;

	private Logger logger = Logger.getLogger(AccountEditController.class);

	@RequestMapping(value = "/editPasswordPage", method = RequestMethod.GET)
	public String getEditPage(ModelMap model) {
		logger.info(this.getClass().getName() + " Returning Login Page");
		DetailsEditParam editparam = new DetailsEditParam();
		model.addAttribute("editparam", editparam);
		//return "passwordChange";
		return MyshipmentJspMapper.EDIT_PASSWORD_PAGE;

	}

	@RequestMapping(value = "/setNewPassword", method = RequestMethod.POST)
	public ModelAndView setNewPasswordInput(
			@ModelAttribute("editParam") DetailsEditParam editparam,
			Model model, HttpSession session,ModelAndView modelAndView,RedirectAttributes redirecAttributes) {
		logger.info("Method AccountEditController.setNewPasswordInput starts.");
		LoginDTO loginDto = (LoginDTO) session.getAttribute("loginDetails");
		EncryptDecryptUtil encryptDecrypt = new EncryptDecryptUtil();
		String customerNo = null;
		String password = null;
		if (loginDto != null) {
			customerNo = loginDto.getLoggedInUserName();
			password = encryptDecrypt.decrypt(loginDto.getLoggedInUserPassword());
			logger.info("Customer No found as : " + customerNo);
		}

		if (password.equals(editparam.getPassword())) {
			if (editparam != null) {
				editparam.setCustomerNo(customerNo);
				
				BapiRet1 editJsonData = null;
				editJsonData = editDetailService.getAccountEditData(editparam);
				if(editJsonData.getType().equals("S"))
				{
					String encryptedPassword = encryptDecrypt.encrypt(editparam.getNewPassword());
					if(encryptedPassword != null) {
						loginDto.setLoggedInUserPassword(encryptedPassword);
					}
					 modelAndView.setViewName("redirect:/editPasswordPage");
			    	 redirecAttributes.addFlashAttribute("message","Password Successfully Changed!");
			    	 return modelAndView;
				}
				else if(editJsonData.getType().equals("E"))
				{
					 modelAndView.setViewName("redirect:/editPasswordPage");
			    	 redirecAttributes.addFlashAttribute("message","Password Change is not successful!");
			    	 return modelAndView;
					
				}
				
			}
		}
		else
		{
			 modelAndView.setViewName("redirect:/editPasswordPage");
	    	 redirecAttributes.addFlashAttribute("message","You have entered wrong password in the Old password field");
	    	 return modelAndView;
		}
		return null;

	}

}
*/