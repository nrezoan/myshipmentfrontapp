package com.myshipment.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.myshipment.dto.SearchPoDTO;
import com.myshipment.model.SegPurchaseOrder;
import com.myshipment.service.ISearchPOSerivce;
import com.myshipment.service.ISegregationService;
/*
 * @Ranjeet Kumar
 */
@Controller
public class UpdatePoController {

	private static final Logger logger = Logger.getLogger(UpdatePoController.class);

	@Autowired
	private ISearchPOSerivce iSearchPOSerivce;
	@Autowired
	private ISegregationService segregationService;

	@RequestMapping(value = "/getUpdatePoPage", method = RequestMethod.GET)
	public String getPOUpdatePage(Model model, HttpSession httpSession, HttpServletRequest request)
	{


		try{
			logger.info("Method UpdatePoController.getPOUpdatePage starts.");

			String segId = request.getParameter("seg_id");
			int segregationId = Integer.parseInt(segId);
			String paramPoNo = request.getParameter("param1");
			String paramFromDate = request.getParameter("param2");
			String paramToDate = request.getParameter("param3");

			SegPurchaseOrder segPurchaseOrder = segregationService.getLineItemForSegregation(segregationId);

			segPurchaseOrder.setParamPoNo(paramPoNo);
			segPurchaseOrder.setParamFromDate(paramFromDate);
			segPurchaseOrder.setParamToDate(paramToDate);

			model.addAttribute("segPurchaseOrder", segPurchaseOrder);

			logger.info("Method UpdatePoController.getPOUpdatePage ends.");

			//return "updatePo";
			return "poUpdateLatest";

		}catch(Exception e){
			e.printStackTrace();
			logger.debug("Some exception occurred : "+e);
			return "error";
		}
	}

	@RequestMapping(value="/updateThePo", method = RequestMethod.POST)
	public String updatePO(@ModelAttribute("segPurchaseOrder") SegPurchaseOrder segPurchaseOrder, ModelMap model, HttpSession httpSession, RedirectAttributes redirectAttributes)
	{

		logger.info("Method UpdatePoController.updatePO starts.");

		try{
			Long resultId = iSearchPOSerivce.updatePo(segPurchaseOrder);

			//Prepare the data for redirection
			SearchPoDTO searchPoDTO = new SearchPoDTO();
			searchPoDTO.setFromDate(segPurchaseOrder.getParamFromDate());
			searchPoDTO.setPoNo(segPurchaseOrder.getParamPoNo());
			searchPoDTO.setToDate(segPurchaseOrder.getParamToDate());
			searchPoDTO.setRedirectedRequest("yes");

			//Redirect to search page
			redirectAttributes.addFlashAttribute("searchPoDTO", searchPoDTO);
			if(resultId == 1){
				redirectAttributes.addFlashAttribute("message", "Record Updated Successfully.");
			}
			if(resultId == 0){
				redirectAttributes.addFlashAttribute("message", "Record Not Updated.");
			}
			logger.info("Method UpdatePoController.updatePO ends.");

			return "redirect:/getSearchPoPage";
		}catch(Exception e){
			e.printStackTrace();
			logger.debug("Some exception occurred : "+e);
			return "error";
		}
	}

}
