package com.myshipment.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.myshipment.dto.LoginDTO;
import com.myshipment.model.MblTrackingJsonOutputData;
import com.myshipment.model.MblTrackingParams;
import com.myshipment.model.MblTrackingResultBean;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;
/*
 * @Ranjeet Kumar
 */
@Controller
public class MblTrackingController {

	private static final Logger logger = Logger.getLogger(MblTrackingController.class);

	@Autowired
	private RestService restService;

	@RequestMapping(value = "/mblWiseTracking", method = RequestMethod.POST)
	public String getMblWiseTrackingInfo(@ModelAttribute("mblTrackingParams") MblTrackingParams mblTrackingParams, Model model, HttpServletRequest request){

		logger.info("Method MblTrackingController.getMblWiseTrackingInfo starts.");

		try{
			
			
			
			LoginDTO loginDto = (LoginDTO) request.getSession()
					.getAttribute("loginDetails");
			if (loginDto.getAccgroup().equals("ZMSP")
					|| loginDto.getAccgroup().equals("ZMFF")) {
				mblTrackingParams.setShipper_no(loginDto
						.getLoggedInUserName());
				mblTrackingParams.setBuyer_no("");
				mblTrackingParams.setAgent_no("");
				System.out.println("Shipper name is :" + mblTrackingParams.getShipper_no());
			}
			if (loginDto.getAccgroup().equals("ZMBY")
					|| loginDto.getAccgroup().equals("ZMBH")) {
				mblTrackingParams
						.setBuyer_no(loginDto.getLoggedInUserName());
				mblTrackingParams.setShipper_no("");
				mblTrackingParams.setAgent_no("");
			}
			if (loginDto.getAccgroup().equals("ZMDA")
					|| loginDto.getAccgroup().equals("ZMOA")) {
				mblTrackingParams
						.setAgent_no(loginDto.getLoggedInUserName());
				mblTrackingParams.setBuyer_no("");
				mblTrackingParams.setShipper_no("");
			}

			/*if (mblTrackingParams.getStatusType().equalsIgnoreCase(
					CommonConstant.STATUS_TYPE_NONE)) {
				mblTrackingParams.setStatusType("");
			}*/
			mblTrackingParams.setDivision(loginDto.getDivisionSelected());
			mblTrackingParams.setDistChan(loginDto.getDisChnlSelected());
			if (loginDto.getDisChnlSelected().equals("EX")
					&& loginDto.getDivisionSelected().equals("SE")) {
				mblTrackingParams.setAction("05");
			} else if (loginDto.getDisChnlSelected().equals("EX")
					&& loginDto.getDivisionSelected().equals("AR")) {
				mblTrackingParams.setAction("07");
			}
			
			mblTrackingParams.setCustomer(loginDto.getLoggedInUserName());
			mblTrackingParams.setSalesOrg(loginDto.getSalesOrgSelected());
			
			StringBuffer webServiceUrl = new StringBuffer(RestUtil.MBL_WISE_TRACKING);

			
			MblTrackingJsonOutputData mblTrackingJsonOutputData = restService.postForObject(RestUtil.prepareUrlForService(webServiceUrl).toString(), mblTrackingParams, MblTrackingJsonOutputData.class);
			List<MblTrackingResultBean>  trackingResLst = mblTrackingJsonOutputData.getTrackingData();
			model.addAttribute("trackingResLst", trackingResLst);

			return "mblTracking";
			
			
		}catch(Exception ex){
			ex.printStackTrace();
			logger.debug("Some exception occurred while executing MblTrackingController.getMblWiseTrackingInfo : "+ex);
			return "error";
		}
	}

	@RequestMapping(value = "/mblWiseTrackingSearchPage", method = RequestMethod.GET)
	public String getMblWiseTrackingSearchPage(Model model){

		try{
			MblTrackingParams mblTrackingParams = new MblTrackingParams();
			model.addAttribute("mblTrackingParams", mblTrackingParams);
			
			return "mblTracking";
		}catch(Exception ex){
			logger.debug("Some exception occurred while executing MblTrackingController.getMblWiseTrackingSearchPage : "+ex);
			return "error";
		}
	}
	

}
