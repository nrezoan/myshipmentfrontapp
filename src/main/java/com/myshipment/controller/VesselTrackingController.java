package com.myshipment.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.myshipment.dto.LoginDTO;
import com.myshipment.model.ItDocumentDetailBean;
import com.myshipment.model.ItDocumentHeaderBean;
import com.myshipment.model.PoTrackingDetailResultBean;
import com.myshipment.model.PoTrackingJsonOutputData;
import com.myshipment.model.PoTrackingParams;
import com.myshipment.model.PoTrackingResultBean;
import com.myshipment.model.TrackingOutputJsonDataNew;
import com.myshipment.model.TrackingRequestParam;
import com.myshipment.model.TrackingStatusParams;
import com.myshipment.model.VesselTrackingBean;
import com.myshipment.model.VesselTrackingParam;
import com.myshipment.util.CommonConstant;
import com.myshipment.util.DateUtil;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;
import com.myshipment.util.SessionUtil;
/*
 * @Mohammad Salahuddin
 */
@Controller
public class VesselTrackingController {

	private static final Logger logger = Logger.getLogger(VesselTrackingController.class);

	@Autowired
	private RestService restService;
	
	@RequestMapping(value = "/getVesselTracking", method = RequestMethod.GET)
	public String vesselTracking(@RequestParam("blNo") String blNo,@RequestParam("vesselName") String vesselName, ModelMap model,HttpSession session)
	{
		logger.info("Method VesselTrackingController.vesselTracking starts");
		System.out.println("Search Value: "+blNo);
		try{
			if(blNo != null && vesselName!=null)
			{
				LoginDTO loginDto = (LoginDTO)session.getAttribute(SessionUtil.LOGIN_DETAILS);
				VesselTrackingParam trackingRequestParam=new VesselTrackingParam();
				if(loginDto != null){
					System.out.println("loginDto: "+loginDto);
					trackingRequestParam.setHbl_no(blNo);
					trackingRequestParam.setVessel_name(vesselName);
					
					
				}
				
				
				StringBuffer webServiceUrl = new StringBuffer(RestUtil.VESSEL_TRACKING_DETAIL);
				VesselTrackingBean vesselTrackingBean = restService.postForObject(RestUtil.prepareUrlForService(webServiceUrl).toString(), trackingRequestParam, VesselTrackingBean.class);
				if(vesselTrackingBean!=null){
					model.addAttribute("vesselTrackingBean", vesselTrackingBean);
					
				return "vessel_tracking";		
				}else{
					model.addAttribute("trackingRequestParam", trackingRequestParam);
					return "vessel_tracking";
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.debug("Some exception occurred : ", e);
			return "trackingSearch";
		}
		return "vessel_tracking";
	}

}
