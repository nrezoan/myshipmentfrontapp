package com.myshipment.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.myshipment.model.BapiRet1;
import com.myshipment.model.DetailsEditParam;
import com.myshipment.model.LoginMdl;
import com.myshipment.service.IEmailService;
import com.myshipment.service.IRecoverPassword;
import com.myshipment.util.CommonConstant;
import com.myshipment.util.ServiceLocator;
import com.sun.mail.util.MailSSLSocketFactory;

@Controller
public class PasswordRecoverController {

	private static final long serialVersionUID = 1L;

	@Autowired
	private IRecoverPassword editDetailService;
	private IEmailService emailService;
	private ServiceLocator serviceLocator;

	private Logger logger = Logger.getLogger(PasswordRecoverController.class);

	//@RequestMapping(value = "/forgetPasswordPage", method = RequestMethod.GET)


	@RequestMapping(value = "/recoverPassword", method = RequestMethod.POST)
	public ModelAndView recoverPasswordPage(
			@ModelAttribute("editParam") DetailsEditParam editparam,
			Model model, HttpSession session,ModelAndView modelAndView,RedirectAttributes redirecAttributes) {
		logger.info("Method AccountEditController.setNewPasswordInput starts.");
	//	LoginDTO loginDto = (LoginDTO) session.getAttribute("loginDetails");
		String customerNo = null;
		String mail =editparam.getVerifyMail();
//		if (loginDto != null) {
//			customerNo = loginDto.getLoggedInUserName();
//			logger.info("Customer No found as : " + customerNo);
//		}
		customerNo=editparam.getCustomerNo();
		if (editparam != null) {
		//	editparam.setCustomerNo(customerNo);
			
			BapiRet1 editJsonData = null;
			editJsonData = editDetailService. getRecoveredPass(editparam);
			if(editJsonData.getType().equals("S"))
			{
				 String Password=editJsonData.getMessage();
				 String text="Your password for user id "+customerNo+ " is:"+ Password;
				 String subject="Recover Password for MyShipment.com";
				 Properties properties=new Properties();
				 InputStream inputStream=null;
					try {
						/*inputStream=new FileInputStream(new File("/WEB-INF/classes/email/emailconfig.properties"));*/
						inputStream=this.getClass().getClassLoader().getResourceAsStream("email/emailconfig.properties");
						properties.load(inputStream);
						//properties.put(key, value);
						properties.put("mail.smtp.EnableSSL.enable","true");
			            
			            MailSSLSocketFactory sf=new MailSSLSocketFactory();
			            sf.setTrustAllHosts(true);
			            
			              properties.put("mail.smtp.ssl.socketFactory", sf);
			       
						emailService=(IEmailService)serviceLocator.findService("emailService");
					 try{	
					emailService.recoverPasswordMail(mail, properties.getProperty(CommonConstant.DIRECT_BOOKING_MAIL_FROM),subject,text);
						}
						catch (MessagingException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						} catch (Exception e) {
						e.printStackTrace();
						logger.debug(""+e.getMessage());
					}
				 modelAndView.setViewName("redirect:/forgetPasswordPage");
		    	 redirecAttributes.addFlashAttribute("message","Please check your mail for your password.");
		    	 return modelAndView;
			
			}
			else if(editJsonData.getType().equals("E"))
			{
				 modelAndView.setViewName("redirect:/forgetPasswordPage");
		    	 redirecAttributes.addFlashAttribute("message","Email is not Registered. Please contact your respective CRM to Register!!!!");
		    	 return modelAndView;
				
			}
			
			
		}
		return null;

	}
	
	
	@RequestMapping("/forgetPasswordPage")
	public String getEditPage(Model model) {
		logger.info(this.getClass().getName() + " Returning Login Page");
		
		DetailsEditParam editparam = new DetailsEditParam();
		model.addAttribute("editparam", editparam);
		
		return "forgetPasswordCredentials";

	}
}
