package com.myshipment.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.myshipment.dto.FileDTO;
import com.myshipment.dto.LoginDTO;
import com.myshipment.dto.ManualPODTO;
import com.myshipment.dto.SupplierDetailsDTO;
import com.myshipment.model.RedirectParams;
import com.myshipment.model.SegPurchaseOrder;
import com.myshipment.service.IManualPoCreationService;
import com.myshipment.service.IUploadPODataService;
import com.myshipment.util.SessionUtil;
/*
 * @ Ranjeet Kumar
 */
@Controller
public class ManualPOCreationController {

	private static final Logger logger = Logger.getLogger(ManualPOCreationController.class);
	
	@Autowired
	private IManualPoCreationService manualPoCreationService;
	@Autowired
	private IUploadPODataService uploadPODataService;
	
	@RequestMapping(value = "/manualPoCreation", method = RequestMethod.POST)
	public String createManualPO(@ModelAttribute("manualPODTO") ManualPODTO manualPODTO, RedirectAttributes redirectAttributes, HttpSession session)
	{
		
		LoginDTO loginDto = (LoginDTO)session.getAttribute(SessionUtil.LOGIN_DETAILS);
		String customerNo = null;		
		String salesOrg=null;
		
		if(loginDto != null){
			customerNo = loginDto.getIsUserLoggedIn();			
			salesOrg=loginDto.getSalesOrgSelected();
			logger.info("Customer No found as : "+ customerNo);
		}
		manualPODTO.setNu_client_code(customerNo);
		manualPODTO.setSales_org(salesOrg);
		Long poId = manualPoCreationService.createManualPO(manualPODTO);
		
		
		RedirectParams redirectParams = new RedirectParams();
		redirectParams.setPoId(poId);
		redirectAttributes.addFlashAttribute("redirectParams", redirectParams);
		
		return "redirect:/getResults1";
	}
	
	@RequestMapping(value = "/getManualPoPage", method = RequestMethod.GET)
	public String getManualPoCreationPage(Map<String, Object> model, HttpServletRequest request)
	{
		ManualPODTO manualPODTO = new ManualPODTO();
		FileDTO fileData = new FileDTO();
		model.put("manualPODTO", manualPODTO);
		model.put("fileData", fileData);
		HttpSession session=SessionUtil.getHttpSession(request);
		SupplierDetailsDTO supplierDetailsDTO=(SupplierDetailsDTO)session.getAttribute(SessionUtil.SUPPLIER_PRE_LOAD_DATA);
		if(supplierDetailsDTO.getBuyersSuppliersmap()==null)
			model.put("shipper", new HashMap<String, String>()); 
		else 
			model.put("shipper", supplierDetailsDTO.getBuyersSuppliersmap().getShipper()); 
		
		
		return "uploadLatest";
	}
	
/*	@ModelAttribute("supplierCodeLst")
	public Map<String, String> getSupplierCode(HttpServletRequest request){
		HttpSession session = SessionUtil.getHttpSession(request);
		SupplierDetailsDTO supplierDetailsDTO = (SupplierDetailsDTO)session.getAttribute(SessionUtil.SUPPLIER_PRE_LOAD_DATA);
		Map<String, String> shipperCode = supplierDetailsDTO.getBuyersSuppliersmap().getShipper();
		
		return shipperCode;
	}*/
	
	@RequestMapping(value = "/getResults1", method = RequestMethod.GET)
	public String showUploadedData1(ModelMap model, @ModelAttribute("redirectParams") RedirectParams redirectParams)
	{
		
		if(redirectParams != null){
			Long poId = redirectParams.getPoId();
			List<SegPurchaseOrder> uploadedDataLst = manualPoCreationService.getCurrentManuallyUploadedData(poId);
			model.put("uploadedDataLst", uploadedDataLst);
		}

		return "poView";
	}
	
	private String getCurrentDateAsString()
	{
		Date currDate = new Date();
		String format = "dd-MM-yyyy";
		DateFormat dateFormat = new SimpleDateFormat(format);
		String dateAsString = dateFormat.format(currDate);
		return dateAsString;
	}
}
