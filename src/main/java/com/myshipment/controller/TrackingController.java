package com.myshipment.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.myshipment.model.BapiTrackDTO;
import com.myshipment.model.TrackingOutputJsonData;
import com.myshipment.model.TrackingParams;
import com.myshipment.model.TrackingStatusParams;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;
/*
 * @Ranjeet Kumar
 */
@Controller
public class TrackingController {

	final static Logger logger = Logger.getLogger(TrackingController.class);

	@Autowired
	private RestService restService;

	@RequestMapping(value = "/getTrackingInfo", method = RequestMethod.POST)
	public String getTrackingInfo(@ModelAttribute("trackingParams") TrackingParams trackingParams, ModelMap model)
	{
		logger.info("Method TrackingController.getTrackingInfo starts");
		BapiTrackDTO trackingDto = new BapiTrackDTO();
		//TrackingStatusParams trackingStatusParams = new TrackingStatusParams();

		try{
			if(trackingParams != null)
			{
				StringBuffer webServiceUrl = new StringBuffer(RestUtil.TRACKING);
				TrackingOutputJsonData trackingOutputJsonData = restService.postForObject(RestUtil.prepareUrlForService(webServiceUrl).toString(), trackingParams, TrackingOutputJsonData.class);

				if(trackingOutputJsonData != null){
					trackingDto = trackingOutputJsonData.getBapiTrackDTOLst().get(0);
				}
				//trackingDto = prepareDummyData();	
				TrackingStatusParams trackingStatusParams = prepareTrackingStatusParamsData(trackingDto);

				model.addAttribute("trackingDto", trackingDto);
				model.addAttribute("trackingStatusParams", trackingStatusParams);

				logger.info("Method TrackingController.getTrackingInfo ends");

				if((trackingDto.getFlight_no() != null) && (!trackingDto.getFlight_no().isEmpty())){
					return "airTracking";
				}
				return "tracking";			
			}
		}catch(Exception e){
			e.printStackTrace();
			logger.debug("Some exception occurred : ", e);
		}
		return null;
	}
	/*	
	@RequestMapping(value="/getTrackingPage", method=RequestMethod.GET)
	public String getTrackingPage(Model model){
		BapiTrackDTO trackingDto = new BapiTrackDTO();
		TrackingStatusParams trackingStatusParams = new TrackingStatusParams();
		trackingDto.setZzmblmawbno("abc");
		trackingDto.setZzmblmawbdt(new Date());
		trackingDto.setBudat(new Date());
		trackingDto.setZstuffingdate(new Date());
		trackingStatusParams.setBookingDate(dateToStringConversion(trackingDto.getZzmblmawbdt()));
		trackingStatusParams.setGoodsReceivedDate(dateToStringConversion(trackingDto.getZzmblmawbdt()));
		trackingStatusParams.setStuffingDate(dateToStringConversion(trackingDto.getZzmblmawbdt()));
		//trackingStatusParams.s
		model.addAttribute("trackingDto", trackingDto);
		model.addAttribute("trackingStatusParams", trackingStatusParams);

		return "tracking";
	}
	 */	

	private String dateToStringConversion(Date dateToConvert)
	{
		String pattern = "dd-MM-yyyy";
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		String convertedString = sdf.format(dateToConvert);
		return convertedString;
	}

	private TrackingStatusParams prepareTrackingStatusParamsData(BapiTrackDTO trackingDto)
	{
		TrackingStatusParams trackingStatusParams = new TrackingStatusParams();
		//trackingDto.setZzmblmawbno("abc");
		//trackingDto.setZzmblmawbdt(new Date());
		//trackingDto.setBudat(new Date());
		//trackingDto.setZstuffingdate(new Date());
		
		//trackingDto.getZzmblmawbdt() value set in the following setter methods, need to replaced by the actual.
		//This was just for testing purpose.
		trackingStatusParams.setBookingDate(dateToStringConversion(trackingDto.getZzmblmawbdt()));
		trackingStatusParams.setGoodsReceivedDate(dateToStringConversion(trackingDto.getZzmblmawbdt()));
		trackingStatusParams.setStuffingDate(dateToStringConversion(trackingDto.getZzmblmawbdt()));
		trackingStatusParams.setShippedOnboardDate(dateToStringConversion(trackingDto.getZzmblmawbdt()));
		trackingStatusParams.setDeliveredDate(dateToStringConversion(trackingDto.getZzmblmawbdt()));
		return trackingStatusParams;
	}
/*
	private BapiTrackDTO prepareDummyData(){
		BapiTrackDTO trackingDto = new BapiTrackDTO();
		trackingDto.setZzmblmawbdt(new Date());
		return trackingDto;
	}*/
}



