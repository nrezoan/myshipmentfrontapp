package com.myshipment.controller;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.myshipment.dto.LoginDTO;
import com.myshipment.model.Adrc;
import com.myshipment.model.BankDetail;
import com.myshipment.model.InvoiceListJsonData;
import com.myshipment.model.InvoiceParam;
import com.myshipment.model.InvoiceTrackingParams;
import com.myshipment.model.ReportParams;
import com.myshipment.model.SexInvJsonData;
import com.myshipment.model.TDocHeader;
import com.myshipment.model.ZsdSexInvDetail;
import com.myshipment.model.ZsdSexInvDetailInd;
import com.myshipment.model.freightInvoicePdfGeneration;
import com.myshipment.service.ISeaExportInvoiceDetailService;
import com.myshipment.service.ISeaExportInvoiceListService;
import com.myshipment.util.DateUtil;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Controller
public class SeaExportInvoiceController extends BaseController {

	private static final long serialVersionUID = 1L;

	@Autowired
	private ISeaExportInvoiceListService seaExportInvoiceListService;

	@Autowired
	private ISeaExportInvoiceDetailService seaExportInvoiceDetailService;

	private Logger logger = Logger.getLogger(SeaExportInvoiceController.class);

	@RequestMapping(value = "/getSeaExportInvoice", method = RequestMethod.GET)
	public String getSeaExportInvoice(ModelMap model, HttpSession session) {
		logger.info(this.getClass().getName() + " Returning Login Page");

		LoginDTO loginDto = (LoginDTO) session.getAttribute("loginDetails");
		String division = loginDto.getDivisionSelected();
		if (division.equalsIgnoreCase("AR")) {
			logger.info("Switched from SE to AR");
			return "redirect:/getAirExportInvoice";
		}
		/* ReportParams req = new ReportParams(); */
		InvoiceTrackingParams req = new InvoiceTrackingParams();
		model.addAttribute("req", req);
		return "seaExportInvoice";

	}
	// @RequestMapping(value = "/getSeaExportInvoiceList", method =
	// RequestMethod.POST)
	// public String getSeaExportInvoiceList(@ModelAttribute("req")
	// InvoiceTrackingParams req, Model model, HttpSession session) {
	// logger.debug(this.getClass().getName() + "getSeaExportInvoiceList(): Start
	// ");
	// LoginDTO loginDto = (LoginDTO)session.getAttribute("loginDetails");
	// String customerNo = null;
	// String distChannel=null;
	// String division=null;
	// String salesOrg=null;
	// if(loginDto != null){
	// customerNo = loginDto.getLoggedInUserName();
	// distChannel=loginDto.getDisChnlSelected();
	// division=loginDto.getDivisionSelected();
	// salesOrg=loginDto.getSalesOrgSelected();
	// logger.info("Customer No found as : "+ customerNo);
	// }
	//
	// req.setSaleOrg(salesOrg);
	// req.setDistChannel(distChannel);
	// req.setDivision(division);
	// req.setCustomer(customerNo);
	// InvoiceListJsonData invoiceListJsonData = null;
	// invoiceListJsonData =
	// seaExportInvoiceListService.getSeaExportInvoiceList(req);
	// model.addAttribute("invoiceListJsonData", invoiceListJsonData);
	//
	//
	// return "seaExportInvoiceListNew";
	//
	// }
	//

	@RequestMapping(value = "/getSeaExportInvoiceList", method = RequestMethod.POST)
	public String getSeaExportInvoiceList(@ModelAttribute("req") InvoiceTrackingParams req, Model model,
			HttpSession session) {

		logger.debug(this.getClass().getName() + "getSeaExportInvoiceList():  Start ");
		LoginDTO loginDto = (LoginDTO) session.getAttribute("loginDetails");
		String customerNo = null;
		String distChannel = null;
		String division = null;
		String salesOrg = null;
		if (loginDto != null) {
			customerNo = loginDto.getLoggedInUserName();
			distChannel = loginDto.getDisChnlSelected();
			division = loginDto.getDivisionSelected();
			salesOrg = loginDto.getSalesOrgSelected();
			logger.info("Customer No found as : " + customerNo);
		}
		if (req.getHblNumber() == null || req.getHblNumber() == "") {
			req.setBlType("0");
		} else {
			String blno = req.getHblNumber().toUpperCase();
			req.setHblNumber(blno);
		}
		req.setSaleOrg(salesOrg);
		req.setDistChannel(distChannel);
		req.setDivision(division);
		req.setCustomer(customerNo);

		logger.debug(this.getClass().getName() + "getSeaExportInvoiceList():  Call Service with Parameter:"
				+ req.getCustomer() + ":" + req.getHblNumber());
		InvoiceListJsonData invoiceListJsonData = null;
		invoiceListJsonData = seaExportInvoiceListService.getSeaExportInvoiceList(req);
		model.addAttribute("invoiceListJsonData", invoiceListJsonData);
		return "seaExportInvoiceList";
	}

	// @RequestMapping(value="/getSeaExportInvoiceDetail", method =
	// RequestMethod.GET )
	// public String getSeaExportInvoiceDetail( @RequestParam("param") String
	// param,Model model) {
	// logger.info(this.getClass().getName() + " getSeaExportInvoiceDetail run..");
	//
	// SexInvJsonData sexInvoiceJsonData = null;
	// InvoiceParam parameter=new InvoiceParam();
	// parameter.setVblen(param);
	// sexInvoiceJsonData =
	// seaExportInvoiceDetailService.getSeaExportInvoiceDetail(parameter);
	// List<ZsdSexInvDetail> chargeLstData =
	// prepareChargeData1(sexInvoiceJsonData);
	// model.addAttribute("sexInvoiceJsonData", sexInvoiceJsonData);
	// model.addAttribute("chargeLstData", chargeLstData);
	// return "seaExportInvoiceDetail";
	//
	// }
	// nusrat

	// @RequestMapping(value = "/getSeaExportInvoiceDetail", method =
	// RequestMethod.GET)
	// public ModelAndView getSeaExportInvoiceDetail(@RequestParam("invoiceNo")
	// String invoice, ModelMap model,
	// HttpSession session, HttpServletResponse response, HttpServletRequest
	// request, ModelAndView modelAndView)
	// throws Exception {
	// }

	@RequestMapping(value = "/getSeaExportInvoiceDetail", method = RequestMethod.GET)
	public ModelAndView getSeaExportInvoiceDetail(@RequestParam("param") String param, ModelMap model,
			HttpSession session, HttpServletResponse response, HttpServletRequest request, ModelAndView modelAndView)
			throws Exception {

		try {
			logger.info(this.getClass().getName() + " getSeaExportInvoiceDetail run..");

			SexInvJsonData sexInvoiceJsonData = new SexInvJsonData();
			LoginDTO loginDto = (LoginDTO) session.getAttribute("loginDetails");
			int counter = 0;

			String reportType = request.getParameter("type");
			reportType = reportType == null ? "freightInvoice" : reportType;

			freightInvoicePdfGeneration pdfData = new freightInvoicePdfGeneration();
			List<freightInvoicePdfGeneration> actualPdfData = new ArrayList<freightInvoicePdfGeneration>();
			List<freightInvoicePdfGeneration> actualPdfData2 = new ArrayList<freightInvoicePdfGeneration>();
			List<freightInvoicePdfGeneration> actualPdfData3 = new ArrayList<freightInvoicePdfGeneration>();
			List<freightInvoicePdfGeneration> actualPdfData4 = new ArrayList<freightInvoicePdfGeneration>();
			List<freightInvoicePdfGeneration> actualPdfData5 = new ArrayList<freightInvoicePdfGeneration>();
			List<freightInvoicePdfGeneration> actualPdfData6 = new ArrayList<freightInvoicePdfGeneration>();
			Map<String, Object> parameterMap = new HashMap<String, Object>();

			InvoiceParam parameter = new InvoiceParam();
			parameter.setVblen(param);
			sexInvoiceJsonData = seaExportInvoiceDetailService.getSeaExportInvoiceDetail(parameter);
			if (sexInvoiceJsonData != null) {

				List<ZsdSexInvDetail> tableData2 = sexInvoiceJsonData.gettHeaderList();
				List<ZsdSexInvDetail> tableData4 = sexInvoiceJsonData.gettConList();
				// List<TZConSize> tableData5 = sexInvoiceJsonData.getTzConSizeList();
				List<TDocHeader> tableData6 = sexInvoiceJsonData.gettDocHeader();
				List<ZsdSexInvDetailInd> tableData3 = sexInvoiceJsonData.gettFHeaderList();
				List<ZsdSexInvDetailInd> tableData7 = sexInvoiceJsonData.gettFinalConList();
				List<ZsdSexInvDetail> tableDataCon = sexInvoiceJsonData.gettPerconList();
				BankDetail bankdtl = sexInvoiceJsonData.gettBank();
				if (bankdtl != null) {
					pdfData.setBankName1(bankdtl.getBankName1());
					pdfData.setBankName2(bankdtl.getBankName2());
					pdfData.setBankCity1(bankdtl.getBankCity1());
					pdfData.setBankCountry(bankdtl.getCountry());
					pdfData.setBankCode(bankdtl.getPortCode1());
					pdfData.setIfscCode(bankdtl.getIfscCode());
					pdfData.setActNo(bankdtl.getAcNumber());
					pdfData.setSwiftID(bankdtl.getSwiftid());
				}
				Adrc companyDetail = sexInvoiceJsonData.gettFooter();
				if (companyDetail != null) {
					pdfData.setCompnayAdd1(companyDetail.getName2());
					pdfData.setCompanyAdd2(companyDetail.getName3());
					pdfData.setCompanyAdd3(companyDetail.getName4());
					if (loginDto.getSalesOrgSelected().equalsIgnoreCase("2200")) {
						pdfData.setCompanyCity(companyDetail.getCity1());
					}
					pdfData.setTelNo("Tel:" + companyDetail.getTelNumber());
					pdfData.setFax("Fax:" + companyDetail.getFaxNumber());
				}
				Adrc payerDetail = sexInvoiceJsonData.getPayer();
				if (payerDetail != null) {
					pdfData.setPayerName1(payerDetail.getName1());
					pdfData.setPayerName2(payerDetail.getName2());
					pdfData.setPayerName3(payerDetail.getName3());
					pdfData.setPayerCity1(payerDetail.getCity1());
				}
				if (tableData6 != null && !tableData6.isEmpty()) {
					TDocHeader tdoch = tableData6.get(0);
					pdfData.setOrigin(tdoch.getPol());
					pdfData.setIncoTerm(tdoch.getInco1());
					pdfData.setShippmentNo(tdoch.getShipmentNo());
					pdfData.setCarrier(tdoch.getCarrier());
				}
				int rate = 0;
				freightInvoicePdfGeneration gstTable = new freightInvoicePdfGeneration();
				if (tableData2 != null && !tableData2.isEmpty()) {
					for (int s = 0; s < tableData2.size(); s++) {
						ZsdSexInvDetail table = tableData2.get(s);
						if (table.getKschl().equals("JOCG")) {
							rate = Integer.valueOf(table.getKbetr().trim());
							gstTable.setCgstRate(rate);
						} else if (table.getKschl().equals("JOIG")) {
							rate = Integer.valueOf(table.getKbetr().trim());
							gstTable.setIgstRate(rate);
						} else if (table.getKschl().equals("JOSG")) {
							rate = Integer.valueOf(table.getKbetr().trim());
							gstTable.setSgstRate(rate);
						} else if (table.getKschl().equals("ZOSG")) {
							rate = Integer.valueOf(table.getKbetr().trim());
							gstTable.setSgstRate2(rate);
						} else if (table.getKschl().equals("ZOIG")) {
							rate = Integer.valueOf(table.getKbetr().trim());
							gstTable.setIgstRate2(rate);
						} else if (table.getKschl().equals("ZOCG")) {
							rate = Integer.valueOf(table.getKbetr().trim());
							gstTable.setCgstRate2(rate);
						}

					}
				}

				pdfData.setInvoiceNo(sexInvoiceJsonData.getBillNo());
				pdfData.setInvoiceDate(DateUtil.formatDateToString(sexInvoiceJsonData.getBillDt()));
				pdfData.setPayerCountry(sexInvoiceJsonData.getPayerCountry());
				pdfData.setPayerNo(sexInvoiceJsonData.getPayerNo());
				pdfData.setSoNo(sexInvoiceJsonData.getSoNo());
				pdfData.setCurrency(sexInvoiceJsonData.getCurrency());
				pdfData.setShipper(sexInvoiceJsonData.getShipper());
				pdfData.setConsignee(sexInvoiceJsonData.getConsignee());
				pdfData.setAgent(sexInvoiceJsonData.getDestAgent());
				pdfData.setPaymentMode(sexInvoiceJsonData.getPaymentMode());
				pdfData.setCommIvoiceNo(sexInvoiceJsonData.getCommInv());
				pdfData.setCommInvoiceDt(DateUtil.formatDateToString(sexInvoiceJsonData.getCommDat()));
				pdfData.setTotalQty(sexInvoiceJsonData.getTotalQty());
				pdfData.setVolume(sexInvoiceJsonData.getVolume());
				pdfData.setGw(sexInvoiceJsonData.getGrWt());
				pdfData.setChWt(sexInvoiceJsonData.getChWt());
				pdfData.setDestination(sexInvoiceJsonData.getDetin());
				pdfData.setMvsl(sexInvoiceJsonData.getmVsl());
				pdfData.setMvslVoyage(sexInvoiceJsonData.getmVoyage());
				pdfData.setFvsl(sexInvoiceJsonData.getfVsl());
				pdfData.setFvslVoyage(sexInvoiceJsonData.getfVoyage());
				pdfData.setMbl(sexInvoiceJsonData.getMbl());
				pdfData.setHbl(sexInvoiceJsonData.getHbl());
				pdfData.setEta(DateUtil.formatDateToString(sexInvoiceJsonData.getEta()));
				pdfData.setEtd(DateUtil.formatDateToString(sexInvoiceJsonData.getEtd()));
				pdfData.setBeneficiary(sexInvoiceJsonData.getsOrg());
				pdfData.setPreparedBy(sexInvoiceJsonData.getUser());
				pdfData.setCompanyCode(sexInvoiceJsonData.getCompanyCode());
				pdfData.setCompany(pdfData.getBeneficiary());
				pdfData.setTotalAmount(sexInvoiceJsonData.getTotal());
				pdfData.setAmountWords(sexInvoiceJsonData.getWords());
				pdfData.setServiceTaxNo(sexInvoiceJsonData.getGvStaxNo());
				// pdfData.setPanNo(sexInvoiceJsonData.getGvPanNo());
				// pdfData.setGstNo(sexInvoiceJsonData.getGstNo());
				pdfData.setSubTotal(sexInvoiceJsonData.getGvTotal());
				pdfData.setSubTotal2(sexInvoiceJsonData.getGvNtTotal());
				pdfData.setTaxTotal(sexInvoiceJsonData.getvTax());
				pdfData.setShippingLine(sexInvoiceJsonData.getShippingLine());
				pdfData.setCgstRate(gstTable.getCgstRate());
				pdfData.setIgstRate(gstTable.getIgstRate());
				pdfData.setSgstRate(gstTable.getSgstRate());
				pdfData.setOdnNumber(sexInvoiceJsonData.getOdn());
				pdfData.setPlos(sexInvoiceJsonData.getPlos());
				pdfData.setGstn(sexInvoiceJsonData.getGstn());
				actualPdfData.add(pdfData);

				if (tableData4 != null && !tableData4.isEmpty()) {
					for (int a = 0; a < tableData4.size(); a++) {
						freightInvoicePdfGeneration pdfTableC = new freightInvoicePdfGeneration();
						ZsdSexInvDetail contable = tableData4.get(a);
						pdfTableC.setContainerName(contable.getVhilm());
						pdfTableC.setContainerSize(contable.getWgbez60());
						actualPdfData2.add(pdfTableC);
					}
				}
				String country = sexInvoiceJsonData.getCountry();
				if (country.equalsIgnoreCase("india")) {
					reportType = "freightInvoiceInd";
					if (tableData3 != null && !tableData3.isEmpty()) {
						for (int w = 0; w < tableData3.size(); w++) {
							ZsdSexInvDetailInd tableInd = tableData3.get(w);
							freightInvoicePdfGeneration pdfTableInd = new freightInvoicePdfGeneration();
							pdfTableInd.setDescription(tableInd.getvText());
							pdfTableInd.setSac_code(tableInd.getSacCode());
							pdfTableInd.setRate(tableInd.getKbetr() + " " + tableInd.getWaers());
							pdfTableInd.setConversionI(tableInd.getConv());
							pdfTableInd.setUom(tableInd.getTdText());
							pdfTableInd.setTaxable(tableInd.gettKwert());
							pdfTableInd.setNontaxable(tableInd.getNtKwert());
							if (tableInd.getvText().equalsIgnoreCase("Transportation Charg")) {
								if (gstTable.getCgstRate2() > 0) {
									pdfTableInd.setCgstRate(gstTable.getCgstRate2());
									pdfTableInd.setCgst(rateCalculation(BigDecimal.valueOf(gstTable.getCgstRate2()),
											tableInd.gettKwert()));
								}
								if (gstTable.getIgstRate2() > 0) {
									pdfTableInd.setIgstRate(gstTable.getIgstRate2());
									pdfTableInd.setIgst(rateCalculation(BigDecimal.valueOf(gstTable.getIgstRate2()),
											tableInd.gettKwert()));
								}
								if (gstTable.getSgstRate2() > 0) {
									pdfTableInd.setSgstRate(gstTable.getSgstRate2());
									pdfTableInd.setSgst(rateCalculation(BigDecimal.valueOf(gstTable.getSgstRate2()),
											tableInd.gettKwert()));
									// pdfTableInd.setTotalAmount1(totalCalculation(tableInd.gettKwert(),tableInd.getNtKwert(),pdfTableInd.getSgst()));
								}

							} else {
								if (gstTable.getCgstRate() > 0) {
									pdfTableInd.setCgstRate(gstTable.getCgstRate());
									pdfTableInd.setCgst(rateCalculation(BigDecimal.valueOf(gstTable.getCgstRate()),
											tableInd.gettKwert()));
									// pdfTableInd.setTotalAmount1(totalCalculation(tableInd.gettKwert(),tableInd.getNtKwert(),pdfTableInd.getCgst()));
								}
								if (gstTable.getIgstRate() > 0) {
									pdfTableInd.setIgstRate(gstTable.getIgstRate());
									pdfTableInd.setIgst(rateCalculation(BigDecimal.valueOf(gstTable.getIgstRate()),
											tableInd.gettKwert()));
									// pdfTableInd.setTotalAmount1(totalCalculation(tableInd.gettKwert(),tableInd.getNtKwert(),pdfTableInd.getIgst()));
								}
								if (gstTable.getSgstRate() > 0) {
									pdfTableInd.setSgstRate(gstTable.getSgstRate());
									pdfTableInd.setSgst(rateCalculation(BigDecimal.valueOf(gstTable.getSgstRate()),
											tableInd.gettKwert()));
									// pdfTableInd.setTotalAmount1(totalCalculation(tableInd.gettKwert(),tableInd.getNtKwert(),pdfTableInd.getSgst()));
								}

							}
							pdfTableInd.setTotalAmount1(totalCalculation(tableInd.gettKwert(), tableInd.getNtKwert(),
									pdfTableInd.getCgst(), pdfTableInd.getIgst(), pdfTableInd.getSgst()));

							actualPdfData.add(pdfTableInd);
						}
					}
					/*
					 * if (tableData7 != null && !tableData7.isEmpty()) { for (int t = 0; t <
					 * tableData7.size(); t++) { ZsdSexInvDetailInd tableInd2 = tableData7.get(t);
					 * 
					 * if (tableInd2.getvText().equalsIgnoreCase("") &&
					 * tableInd2.getNtKwert().doubleValue() == 0.00) { // do not add to the list }
					 * else { freightInvoicePdfGeneration pdfTableInd2 = new
					 * freightInvoicePdfGeneration();
					 * pdfTableInd2.setContainerName2(tableInd2.getVhilm());
					 * pdfTableInd2.setContainerSize2(tableInd2.getWgbez60()); if
					 * (tableInd2.getvText().equalsIgnoreCase("Total")) {
					 * pdfTableInd2.setDescription2("SUB-TOTAL");
					 * 
					 * } else { pdfTableInd2.setDescription2(tableInd2.getvText()); }
					 * pdfTableInd2.setSac_code2(tableInd2.getSacCode());
					 * pdfTableInd2.setRate2(tableInd2.getKbetr() + " " + tableInd2.getWaers());
					 * pdfTableInd2.setConversionI2(tableInd2.getConv());
					 * pdfTableInd2.setUom2(tableInd2.getTdText());
					 * pdfTableInd2.setTaxable2(tableInd2.gettKwert());
					 * pdfTableInd2.setNontaxable2(tableInd2.getNtKwert());
					 * actualPdfData5.add(pdfTableInd2); }
					 * 
					 * } }
					 */

					/*
					 * List<ZsdSexInvDetail> gst = new ArrayList<ZsdSexInvDetail>(); ZsdSexInvDetail
					 * nTable = new ZsdSexInvDetail();
					 * 
					 * nTable.setKbetr("0"); nTable.setWaers("%");
					 * nTable.setKwert(BigDecimal.valueOf(0.000)); nTable.setvText("null");
					 * 
					 * gst.add(0, nTable); gst.add(1, nTable); gst.add(2, nTable);
					 * 
					 * if (tableData2 != null && !tableData2.isEmpty()) { for (int s = 0; s <
					 * tableData2.size(); s++) { ZsdSexInvDetail table = tableData2.get(s); String
					 * des = table.getvText(); if (table.getKschl().equals("JOCG") ||
					 * table.getKschl().equals("JOSG") || table.getKschl().equals("JOIG")) { counter
					 * = 1;
					 * 
					 * if (des.equals("IN:Central GST-OP 18")) { gst.remove(0); gst.add(0, table); }
					 * else if (des.equals("IN: StateGST�OP18")) { gst.remove(1); gst.add(1, table);
					 * } else if (des.equals("IN:IntegraGST-OP18")) { gst.remove(2); gst.add(2,
					 * table); } } else if (table.getKschl().equals("JSER") ||
					 * table.getKschl().equals("JKKR") || table.getKschl().equals("JSB2")) { counter
					 * = 2; if (des.equals("Service Charg Output")) { gst.remove(0); gst.add(0,
					 * table); } else if (des.equals("SBCess A/R")) { gst.remove(1); gst.add(1,
					 * table); } else if (des.equals("KK Cess A/R")) { gst.remove(2); gst.add(2,
					 * table); }
					 * 
					 * }
					 * 
					 * } } if (counter == 0) { for (int o = 0; o < gst.size(); o++) {
					 * ZsdSexInvDetail nTable2 = gst.get(o); nTable2.setKbetr("null");
					 * nTable2.setWaers("null"); nTable2.setKwert(null); } } for (int l = 0; l <
					 * gst.size(); l++) { freightInvoicePdfGeneration gstTableInd = new
					 * freightInvoicePdfGeneration(); ZsdSexInvDetail table = gst.get(l); if
					 * (table.getvText().equals("null")) { if (counter == 1) { if (l == 0) {
					 * gstTableInd.setCgst1("CGST"); } else if (l == 1) {
					 * gstTableInd.setCgst1("SGST"); } else if (l == 2) {
					 * gstTableInd.setCgst1("IGST"); } } else if (counter == 2) { if (l == 0) {
					 * gstTableInd.setCgst1("Service Charg Output"); } else if (l == 1) {
					 * gstTableInd.setCgst1("SBCess A/R"); } else if (l == 2) {
					 * gstTableInd.setCgst1("KK Cess A/R"); }
					 * 
					 * } } else { // gstTableInd.setCgst1(table.getvText()); if (l == 0) {
					 * gstTableInd.setCgst1("CGST"); } else if (l == 1) {
					 * gstTableInd.setCgst1("SGST"); } else if (l == 2) {
					 * gstTableInd.setCgst1("IGST"); } }
					 * gstTableInd.setCgst2(table.getKbetr().trim() + table.getWaers());
					 * gstTableInd.setCgst3(table.getKwert()); actualPdfData3.add(gstTableInd); }
					 */
					freightInvoicePdfGeneration pdfData2 = new freightInvoicePdfGeneration();
					pdfData2.setGstNo(sexInvoiceJsonData.getGstNo());
					pdfData2.setTax1("GST No.:");
					/*
					 * if (counter == 1) { pdfData2.setGstNo(sexInvoiceJsonData.getGstNo());
					 * pdfData2.setTax1("GST No.:"); } if (counter == 2) {
					 * pdfData2.setGstNo(sexInvoiceJsonData.getGvPanNo());
					 * pdfData2.setTax1("Service Tax No.:"); }
					 */
					actualPdfData4.add(pdfData2);
					JRDataSource pdfDataSource = new JRBeanCollectionDataSource(actualPdfData);
					JRDataSource pdfDataSource2 = new JRBeanCollectionDataSource(actualPdfData2);
					JRDataSource pdfDataSource3 = new JRBeanCollectionDataSource(actualPdfData3);
					JRDataSource pdfDataSource4 = new JRBeanCollectionDataSource(actualPdfData4);
					JRDataSource pdfDataSource6 = new JRBeanCollectionDataSource(actualPdfData5);

					parameterMap.put("datasource2", pdfDataSource2);
					parameterMap.put("datasource", pdfDataSource);
					parameterMap.put("datasource3", pdfDataSource3);
					parameterMap.put("datasource4", pdfDataSource4);
					parameterMap.put("datasource6", pdfDataSource6);
					ServletContext context = request.getSession().getServletContext();
					String path = context.getRealPath("/") + "";
					parameterMap.put("Context", path);
					modelAndView = new ModelAndView(reportType, parameterMap);

					return modelAndView;

				} else {
					if (tableData2 != null && !tableData2.isEmpty()) {
						for (int z = 0; z < tableData2.size(); z++) {
							ZsdSexInvDetail table = tableData2.get(z);
							freightInvoicePdfGeneration pdfTable = new freightInvoicePdfGeneration();
							pdfTable.setDescription(table.getvText());
							pdfTable.setRate(table.getKbetr().trim() + " " + table.getWaers());
							pdfTable.setConversion(table.getConv());
							if (table.getKpein().compareTo(BigDecimal.ZERO) == 0) {
								pdfTable.setUom("FIXED");
							} else {
								pdfTable.setUom(table.getKpein() + table.getKmein());
							}
							pdfTable.setTotal(table.getKwert());
							actualPdfData.add(pdfTable);

						}
					}

					if (tableDataCon != null && !tableDataCon.isEmpty()) {
						for (int x = 0; x < tableDataCon.size(); x++) {
							ZsdSexInvDetail tableCon = tableDataCon.get(x);

							if (tableCon.getvText().equalsIgnoreCase("") && tableCon.getKwert().doubleValue() == 0.00) {
								// do not add to the list
							} else {
								freightInvoicePdfGeneration pdfTable2 = new freightInvoicePdfGeneration();
								pdfTable2.setContainerName3(tableCon.getVhilm());
								pdfTable2.setContainerSize3(tableCon.getWgbez60());
								pdfTable2.setDescription3(tableCon.getvText());
								pdfTable2.setRate3(tableCon.getKbetr() + tableCon.getWaers());
								pdfTable2.setConversion3(tableCon.getConv());
								pdfTable2.setUom3(tableCon.getTdText());
								pdfTable2.setTotal3(tableCon.getKwert());
								actualPdfData6.add(pdfTable2);

							}

						}
					}
					// for(int t=0;t<tableData7.size();t++)
					// {
					// ZsdSexInvDetailInd tableInd2=tableData7.get(t);
					// freightInvoicePdfGeneration pdfTableInd2= new freightInvoicePdfGeneration();
					// pdfTableInd2.setContainerName2(tableInd2.getVhilm());
					// pdfTableInd2.setContainerSize2(tableInd2.getWgbez60());
					// if(tableInd2.getvText()!=null)
					// {
					// pdfTableInd2.setDescription2(tableInd2.getvText());
					// pdfTableInd2.setRate2(tableInd2.getKbetr()+" "+tableInd2.getWaers());
					// pdfTableInd2.setConversion2(tableInd2.getConv());
					// pdfTableInd2.setUom2(tableInd2.getTdText());
					// pdfTableInd2.setTotal2(tableInd2.gettKwert());
					//
					// }
					// actualPdfData5.add(pdfTableInd2);
					// }

					JRDataSource pdfDataSource = new JRBeanCollectionDataSource(actualPdfData);
					JRDataSource pdfDataSource2 = new JRBeanCollectionDataSource(actualPdfData2);
					JRDataSource pdfDataSource3 = new JRBeanCollectionDataSource(actualPdfData6);

					parameterMap.put("datasource2", pdfDataSource2);
					parameterMap.put("datasource7", pdfDataSource3);
					parameterMap.put("datasource", pdfDataSource);
					ServletContext context = request.getSession().getServletContext();
					String path = context.getRealPath("/") + "";
					parameterMap.put("Context", path);
					modelAndView = new ModelAndView(reportType, parameterMap);

					return modelAndView;
				}
			} else {
				//
				modelAndView.setViewName("redirect:/getSeaExportInvoice");
				return modelAndView;
			}
		} catch (IndexOutOfBoundsException ex) {
			// TODO Auto-generated catch block
			// modelAndView.setViewName("redirect://getSeaExportInvoice");
			// redirecAttributes.addFlashAttribute("message","HBL number is not valid!!
			// Please enter a valid HBL number");
			// return modelAndView;
			return null;
		}

	}

	// hamid
	// 22.03.2017
	@RequestMapping(value = "/getSeaExportInvoiceListFromUrl", method = RequestMethod.GET)
	public String getSeaExportInvoiceListFromUrl(@ModelAttribute("req") InvoiceTrackingParams req, Model model,
			HttpSession session, @RequestParam("searchString") String searchValue) {

		logger.debug(this.getClass().getName() + "getSeaExportInvoiceList():  Start ");
		LoginDTO loginDto = (LoginDTO) session.getAttribute("loginDetails");
		String customerNo = null;
		String distChannel = null;
		String division = null;
		String salesOrg = null;
		if (loginDto != null) {
			customerNo = loginDto.getLoggedInUserName();
			distChannel = loginDto.getDisChnlSelected();
			division = loginDto.getDivisionSelected();
			salesOrg = loginDto.getSalesOrgSelected();
			logger.info("Customer No found as : " + customerNo);
		}

		req.setHblNumber(searchValue);
		req.setSaleOrg(salesOrg);
		req.setDistChannel(distChannel);
		req.setDivision(division);
		req.setCustomer(customerNo);

		logger.debug(this.getClass().getName() + "getSeaExportInvoiceList():  Call Service with Parameter:"
				+ req.getCustomer() + ":" + req.getHblNumber());
		InvoiceListJsonData invoiceListJsonData = null;
		invoiceListJsonData = seaExportInvoiceListService.getSeaExportInvoiceList(req);
		model.addAttribute("invoiceListJsonData", invoiceListJsonData);
		return "seaExportInvoiceList";
	}

	@SuppressWarnings("unused")
	private List<ZsdSexInvDetail> prepareChargeData1(SexInvJsonData sexInvoiceJsonData) {

		List<ZsdSexInvDetail> chargeLst = new ArrayList<ZsdSexInvDetail>();
		List<String> identifiedChargeLst = new ArrayList<String>();
		List<String> vTextZsdSexInvDetailInd = new ArrayList<String>();
		List<String> vTextZsdSexInvDetail = new ArrayList<String>();

		List<ZsdSexInvDetailInd> zsdSexInvDetailInd = sexInvoiceJsonData.gettFHeaderList();
		List<ZsdSexInvDetail> zsdSexInvDetail = sexInvoiceJsonData.gettHeaderList();

		for (ZsdSexInvDetailInd zsdInvDetailInd : zsdSexInvDetailInd) {
			vTextZsdSexInvDetailInd.add(zsdInvDetailInd.getvText());
		}

		for (ZsdSexInvDetail zsdInvDetail : zsdSexInvDetail) {
			vTextZsdSexInvDetail.add(zsdInvDetail.getvText());
		}

		for (String vText : vTextZsdSexInvDetail) {
			if (vTextZsdSexInvDetailInd.contains(vText)) {
				continue;
			} else {
				identifiedChargeLst.add(vText);
			}
		}

		for (ZsdSexInvDetail zsdSexInvDetail1 : zsdSexInvDetail) {
			if (identifiedChargeLst.contains(zsdSexInvDetail1.getvText())) {
				chargeLst.add(zsdSexInvDetail1);
			}
		}

		return chargeLst;
	}

	public BigDecimal rateCalculation(BigDecimal rate, BigDecimal amount) {
		BigDecimal rateAmount = amount.multiply(rate.divide(new BigDecimal("100"))).setScale(2, RoundingMode.CEILING);
		return rateAmount;

	}

	public BigDecimal totalCalculation(BigDecimal taxable, BigDecimal nonTaxable, BigDecimal cgst, BigDecimal igst,
			BigDecimal sgst) {
		if (igst == null) {
			igst = BigDecimal.valueOf(0);
		}
		if (sgst == null) {
			sgst = BigDecimal.valueOf(0);
		}
		if (cgst == null) {
			cgst = BigDecimal.valueOf(0);
		}
		BigDecimal total = taxable.add(nonTaxable.add(cgst.add(igst.add(sgst)))).setScale(2, RoundingMode.CEILING);
		return total;
	}
}
