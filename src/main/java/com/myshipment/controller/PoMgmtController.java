package com.myshipment.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.myshipment.dto.ApprovedPoDTO;
import com.myshipment.dto.LoginDTO;
import com.myshipment.dto.PurchaseOrderDTO;
import com.myshipment.service.IPoMgmtService;
import com.myshipment.util.ServiceLocator;
import com.myshipment.util.SessionUtil;

@Controller
public class PoMgmtController {
	private IPoMgmtService poMgmtService;
	
	
	//load daash board data
	@RequestMapping("/loadpodashboard")
	public String loadPoDashBoardDatas(Model model,HttpServletRequest request) {
		poMgmtService=(IPoMgmtService)ServiceLocator.findService("poMgmtService");
		List<PurchaseOrderDTO> lstPo=poMgmtService.getPoDashBoardData();
		if(null!=lstPo && lstPo.size()>0)
			model.addAttribute("purchaseOrder", lstPo);
		return "podashboard";
	}

	// load approve po search page
	@RequestMapping(value="/approvepo",method=RequestMethod.GET)
	public String approvePo(HttpServletRequest request)
	{
		return "approvepo";
		
	}
	
	@RequestMapping(value="/updatepo",method=RequestMethod.POST)
	@ResponseStatus(value=HttpStatus.OK)
	public void approve(@RequestBody List<Map<String,Object>> lstmpObj,ModelMap model)
	{
		poMgmtService=(IPoMgmtService)ServiceLocator.findService("poMgmtService");
		int success=poMgmtService.approvePo(lstmpObj);
	
	}
	
	
@RequestMapping(value="/assigncarrier",method=RequestMethod.GET)
	public String assignCarrier(@RequestParam("poId") Long poId,Model model,HttpServletRequest request)
	{
		poMgmtService=(IPoMgmtService)ServiceLocator.findService("poMgmtService");
		ApprovedPoDTO approvedPoDTO=poMgmtService.loadApprovedPoLineItemById(poId);
		model.addAttribute("purchaseOrder", approvedPoDTO);
		return "assigncarrier";
		
	}

// Load assign Carrier Search Page
@RequestMapping(value="/getassignsearch",method=RequestMethod.GET)
public String getSearchAssignCarrier()
{
	return "assigncarrier";
}

@RequestMapping(value="/updateassigncarrier",method=RequestMethod.POST)
@ResponseStatus(value=HttpStatus.OK)
public void UpdateAssignCarrier(@RequestBody List<Map<String,Object>> assCarrLst,Model model)
{
	poMgmtService=(IPoMgmtService)ServiceLocator.findService("poMgmtService");
	boolean success=poMgmtService.assignCarrier(assCarrLst);

}

// Load Search Data for approve
@RequestMapping(value="/posearchapprove",method=RequestMethod.GET)
@ResponseBody
public List<PurchaseOrderDTO> poSearchApprove(@RequestParam("po_no") String po_no,@RequestParam("from_date") String from_date,@RequestParam("to_date") String to_date,@RequestParam("supplier_code") String supplier_code,HttpServletRequest request)
{
	poMgmtService=(IPoMgmtService)ServiceLocator.findService("poMgmtService");
	
	HttpSession session=SessionUtil.getHttpSession(request);
	LoginDTO loginDto = (LoginDTO)session.getAttribute(SessionUtil.LOGIN_DETAILS);
	
	List<PurchaseOrderDTO> lstPoDTO=poMgmtService.searchPo(po_no, from_date, to_date, supplier_code,loginDto.getLoggedInUserName());
	return lstPoDTO;
	
}

//Load Search data for Assign Carrier
@RequestMapping(value="/searchasgncrr",method=RequestMethod.GET)
@ResponseBody
public List<PurchaseOrderDTO> searchApproveCarr(@RequestParam("po_no") String po_no,@RequestParam("from_date") String from_date,@RequestParam("to_date") String to_date,@RequestParam("supplier_code") String supplier_code,HttpServletRequest request)
{
	poMgmtService=(IPoMgmtService)ServiceLocator.findService("poMgmtService");
	LoginDTO loginDto = (LoginDTO)request.getSession().getAttribute("loginDetails");
	//List<ApprovedPoDTO> lstPoDTO=poMgmtService.searchApprovedPo(po_no, from_date, to_date, supplier_code,loginDto.getLoggedInUserName());
	List<PurchaseOrderDTO> lstPoDTO=poMgmtService.searchPo(po_no, from_date, to_date, supplier_code,loginDto.getLoggedInUserName());
	
	return lstPoDTO;
	
}

}

