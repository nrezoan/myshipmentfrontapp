package com.myshipment.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.myshipment.dto.LoginDTO;
import com.myshipment.model.CommercialInvoiceJasper;
import com.myshipment.model.CommercialInvoiceJsonOutputData;
import com.myshipment.model.CommercialInvoiceParams;
import com.myshipment.model.ZbapiPacListHDTO;
import com.myshipment.model.ZbapiPacListIDTO;
import com.myshipment.util.DateUtil;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/*
 * @ Nusrat Momtahana
 * @ Hamid Bin Zaman
 */
@Controller
public class CommercialInvoiceController extends BaseController {

	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(CommercialInvoiceController.class);

	@Autowired
	private RestService restService;

	/*
	 * @RequestMapping(value = "/findCommercialInvoice", method =
	 * RequestMethod.POST) public String
	 * getCommercialInvoice(@ModelAttribute("commercialInvoice")
	 * CommercialInvoiceParams commercialInvoiceParams, Model model, HttpSession
	 * session) { logger.
	 * info("Method CommercialInvoiceController.getCommercialInvoice starts.");
	 * LoginDTO loginDto = (LoginDTO)session.getAttribute("loginDetails");
	 * String customerNo = null; if(loginDto != null){ customerNo =
	 * loginDto.getLoggedInUserName(); logger.info("Customer No found as : "+
	 * customerNo); } if(commercialInvoiceParams != null) {
	 * 
	 * commercialInvoiceParams.setKunnr(customerNo);
	 * //commercialInvoiceParams.setHblnumber(customerNo); StringBuffer
	 * webServiceUrl = new StringBuffer(RestUtil.COMMERCIAL_INVOICE);
	 * CommercialInvoiceJsonOutputData commercialInvoiceJsonOutputData =
	 * restService.postForObject(RestUtil.prepareUrlForService(webServiceUrl).
	 * toString(), commercialInvoiceParams,
	 * CommercialInvoiceJsonOutputData.class); //CommercialInvoiceDataForPage
	 * commercialInvoiceDataForPage =
	 * iCommercialInvoiceService.prepareData(commercialInvoiceJsonOutputData);
	 * //ZbapiPacListHDTO headerData =
	 * commercialInvoiceDataForPage.getZbapiPacListHDTO();
	 * //List<ZbapiPacListIDTO> itemDataLst =
	 * commercialInvoiceDataForPage.getZbapiPacListIDTOLst();
	 * model.addAttribute("commercialInvoiceJsonOutputData",
	 * commercialInvoiceJsonOutputData);
	 * 
	 * //model.addAttribute("itemDataLst", itemDataLst);
	 * 
	 * logger.
	 * info("Method CommercialInvoiceController.getCommercialInvoice ends.");
	 * 
	 * return "commercialInvoice"; }
	 * 
	 * return null; }
	 */

	@RequestMapping(value = "/findCommercialInvoice", method = RequestMethod.POST)
	public ModelAndView getCommercialInvoice(
			@ModelAttribute("commercialInvoice") CommercialInvoiceParams commercialInvoiceParams, Model model,
			HttpSession session, HttpServletResponse response, HttpServletRequest request, ModelAndView modelAndView,
			RedirectAttributes redirecAttributes, BindingResult result) {
		try {
			logger.info("Method CommercialInvoiceController.getCommercialInvoice starts.");
			LoginDTO loginDto = (LoginDTO) session.getAttribute("loginDetails");
			String customerNo = null;
			if (loginDto != null) {
				customerNo = loginDto.getLoggedInUserName();
				logger.info("Customer No found as : " + customerNo);
			}
			if (commercialInvoiceParams != null) {

				commercialInvoiceParams.setKunnr(customerNo);
				StringBuffer webServiceUrl = new StringBuffer(RestUtil.COMMERCIAL_INVOICE);
				CommercialInvoiceJsonOutputData commercialInvoiceJsonOutputData = restService.postForObject(
						RestUtil.prepareUrlForService(webServiceUrl).toString(), commercialInvoiceParams,
						CommercialInvoiceJsonOutputData.class);

				CommercialInvoiceJasper invoice = new CommercialInvoiceJasper();
				List<CommercialInvoiceJasper> actualPDFData = new ArrayList<CommercialInvoiceJasper>();

				String reportType = request.getParameter("type");
				reportType = reportType == null ? "commercialInvoice" : reportType;

				Map<String, Object> parameterMap = new HashMap<String, Object>();
				
				//check if commercialInvoiceJsonOutputData is not null
				if(commercialInvoiceJsonOutputData != null) {
					List<ZbapiPacListHDTO> waHeaderLst = commercialInvoiceJsonOutputData.getWaHeader();
					List<ZbapiPacListHDTO> countryOfOrg = commercialInvoiceJsonOutputData.getItCntryorig();
					List<ZbapiPacListHDTO> countryOfDest = commercialInvoiceJsonOutputData.getItCntrydest();
					List<ZbapiPacListHDTO> vendorLst = commercialInvoiceJsonOutputData.getItVendor();
					List<ZbapiPacListHDTO> vendorCntryLst = commercialInvoiceJsonOutputData.getItVendorcountry();
					List<ZbapiPacListHDTO> buyerLst = commercialInvoiceJsonOutputData.getItBuyer();
					List<ZbapiPacListHDTO> buyerCntryLst = commercialInvoiceJsonOutputData.getItBuyercountry();
					List<ZbapiPacListHDTO> polLst = commercialInvoiceJsonOutputData.getItPol();
					List<ZbapiPacListHDTO> podLst = commercialInvoiceJsonOutputData.getItPod();
					List<ZbapiPacListHDTO> podelLst = commercialInvoiceJsonOutputData.getItPodel();
					List<ZbapiPacListIDTO> waItemLst = commercialInvoiceJsonOutputData.getWaItem();
					List<ZbapiPacListHDTO> vbkdLst = commercialInvoiceJsonOutputData.getItVbkd();
					List<ZbapiPacListHDTO> issueBankLst = commercialInvoiceJsonOutputData.getItIssueBank();
					List<ZbapiPacListHDTO> makerLst = commercialInvoiceJsonOutputData.getItMaker();
					List<ZbapiPacListHDTO> consigneeLst = commercialInvoiceJsonOutputData.getItConsignee();
					List<ZbapiPacListHDTO> makerCntryLst = commercialInvoiceJsonOutputData.getItMakercountry();
					List<ZbapiPacListHDTO> consigneeCntryLst = commercialInvoiceJsonOutputData.getItConsigneecountry();
					List<ZbapiPacListHDTO> suppBankLst = commercialInvoiceJsonOutputData.getItSupplierBank();
					List<ZbapiPacListHDTO> sbcountryLst = commercialInvoiceJsonOutputData.getItSbcountry();
					List<ZbapiPacListHDTO> costUnitLst = commercialInvoiceJsonOutputData.getItCostUnit();
					
					ZbapiPacListHDTO header, orig, dest, vendor, vendorCountry, buyer, buyerCountry, pol, pod, podel, waItem, vbkd, issueBank,
						maker, consignee, makerCountry, consigneeCountry, suppBank, sbCountry, costUnit;
					
					header = (ZbapiPacListHDTO) (!waHeaderLst.isEmpty() ? waHeaderLst.get(0) : new ZbapiPacListHDTO());
					orig = (ZbapiPacListHDTO) (!countryOfOrg.isEmpty() ? countryOfOrg.get(0) : new ZbapiPacListHDTO());
					dest = (ZbapiPacListHDTO) (!countryOfDest.isEmpty() ? countryOfDest.get(0) : new ZbapiPacListHDTO());
					vendor = (ZbapiPacListHDTO) (!vendorLst.isEmpty() ? vendorLst.get(0) : new ZbapiPacListHDTO());
					vendorCountry = (ZbapiPacListHDTO) (!vendorCntryLst.isEmpty() ? vendorCntryLst.get(0) : new ZbapiPacListHDTO());
					buyer = (ZbapiPacListHDTO) (!consigneeLst.isEmpty() ? consigneeLst.get(0) : new ZbapiPacListHDTO());
					buyerCountry = (ZbapiPacListHDTO) (!consigneeCntryLst.isEmpty() ? consigneeCntryLst.get(0) : new ZbapiPacListHDTO());
					pol = (ZbapiPacListHDTO) (!polLst.isEmpty() ? polLst.get(0) : new ZbapiPacListHDTO());
					pod = (ZbapiPacListHDTO) (!podLst.isEmpty() ? podLst.get(0) : new ZbapiPacListHDTO());
					podel = (ZbapiPacListHDTO) (!podelLst.isEmpty() ? podelLst.get(0) : new ZbapiPacListHDTO());
					//waItem = (ZbapiPacListHDTO) (waItemLst.get(0) != null ? waItemLst.get(0) : "");
					vbkd = (ZbapiPacListHDTO) (!vbkdLst.isEmpty() ? vbkdLst.get(0) : new ZbapiPacListHDTO());
					issueBank = (ZbapiPacListHDTO) (!issueBankLst.isEmpty() ? issueBankLst.get(0) : new ZbapiPacListHDTO());
					maker = (ZbapiPacListHDTO) (!makerLst.isEmpty() ? makerLst.get(0) : new ZbapiPacListHDTO());
					consignee = (ZbapiPacListHDTO) (!buyerLst.isEmpty() ? buyerLst.get(0) : new ZbapiPacListHDTO());
					makerCountry = (ZbapiPacListHDTO) (!makerCntryLst.isEmpty() ? makerCntryLst.get(0) : new ZbapiPacListHDTO());
					consigneeCountry = (ZbapiPacListHDTO) (!buyerCntryLst.isEmpty() ? buyerCntryLst.get(0) : new ZbapiPacListHDTO());
					suppBank = (ZbapiPacListHDTO) (!suppBankLst.isEmpty() ? suppBankLst.get(0) : new ZbapiPacListHDTO());
					sbCountry = (ZbapiPacListHDTO) (!sbcountryLst.isEmpty() ? sbcountryLst.get(0) : new ZbapiPacListHDTO());
					costUnit = (ZbapiPacListHDTO) (!costUnitLst.isEmpty() ? costUnitLst.get(0) : new ZbapiPacListHDTO());
							
					invoice.setVendorAdd1(vendor.getName1());
					invoice.setVendorAdd2(vendor.getName2());
					invoice.setVendorAdd3(vendor.getName3());
					invoice.setVendorAdd4(vendor.getName4());
					invoice.setVendorCity(vendor.getCity1());
					invoice.setVendorCountry(vendorCountry.getLandx());
					
					invoice.setMakerAdd1(maker.getName1());
					invoice.setMakerAdd2(maker.getName2());
					invoice.setMakerAdd3(maker.getName3());
					invoice.setMakerAdd4(maker.getName4());
					invoice.setMakerCity(maker.getCity1());
					invoice.setMakerCountry(makerCountry.getLandx());
					
					invoice.setBankAdd1(suppBank.getName1());
					invoice.setBankAdd2(suppBank.getName2());
					invoice.setBankAdd3(suppBank.getName3());
					invoice.setBankCity(suppBank.getCity1());
					invoice.setBankCountry(suppBank.getLandx());
					
					invoice.setBuyerAdd1(buyer.getName1());
					invoice.setBuyerAdd2(buyer.getName2());
					invoice.setBuyerAdd3(buyer.getName3());
					invoice.setBuyerAdd4(buyer.getName4());
					invoice.setBuyerCity(buyer.getCity1());
					invoice.setBuyerCountry(buyerCountry.getLandx());
					
					invoice.setConsigneeAdd1(consignee.getName1());
					invoice.setConsigneeAdd2(consignee.getName2());
					invoice.setConsigneeAdd3(consignee.getName3());
					invoice.setConsigneeAdd4(consignee.getName4());
					invoice.setConsigneeCity(consignee.getCity1());
					invoice.setConsigneeCountry(consigneeCountry.getLandx());
					
					invoice.setInvoiceNo(header.getZzcomminvno());
					invoice.setInvoiceDt(DateUtil.formatDateToString(header.getZzcomminvdt()));
					invoice.setSo(header.getVbeln());
					invoice.setVendorRef(header.getDesc_z028());
					invoice.setHbl(header.getZzhblhawbno());
					invoice.setCountryOfOrg(orig.getLandx());
					invoice.setCountryOfDest(dest.getLandx());
					invoice.setIncoterm(vbkd.getInco1() + "," + vbkd.getInco2());
					invoice.setPlaceOfReceipt(header.getZzplacereceipt());
					invoice.setLcNo(header.getZzlcpottno());
					invoice.setLcDt(DateUtil.formatDateToString(header.getZzlcdt()));
					invoice.setLcExpDt(DateUtil.formatDateToString(header.getZzlcexpdt()));
					invoice.setIssuingBank(issueBank.getName1());
					invoice.setInvoiceCurrency(header.getZzunit_cost_unit());
					invoice.setBankCode(header.getDesc_z029());
					invoice.setExpNo(header.getZzexpno());
					invoice.setExpDt(DateUtil.formatDateToString(header.getZzexpdt()));
					invoice.setAccNumber(header.getDesc_z030());
					invoice.setSwiftTransfer(header.getDesc_z032());
					invoice.setRemarks(header.getDesc_z005());
					
					actualPDFData.add(invoice);
					
					for (int i = 0; i < waItemLst.size(); i++) {
						CommercialInvoiceJasper itemData = new CommercialInvoiceJasper();
						ZbapiPacListIDTO item = waItemLst.get(i);
						itemData.setOrderNo(item.getZzponumber());
						itemData.setItemRef(item.getZzstyleno());
						itemData.setItemDesc(((header.getDesc_z023().replaceAll("\\t+", "")).replaceAll("\\n", "")).replaceAll("\\r", ""));
						itemData.setHsCode(item.getZzhscode());
						itemData.setQty(item.getZztotalnopcs());
						itemData.setUnitPrice(item.getZzunitCost());
						itemData.setAmount(item.getZamount());
						actualPDFData.add(itemData);
					}
					JRDataSource pdfDataSource = new JRBeanCollectionDataSource(actualPDFData);
					parameterMap.put("datasource", pdfDataSource);
					modelAndView = new ModelAndView(reportType, parameterMap);

					logger.info("Method CommercialInvoiceController.getCommercialInvoice ends.");

					return modelAndView;
				}
				else {
					//commercialInvoiceJsonOutputData null
					modelAndView.setViewName("redirect:/getCommInvoicePage");
					redirecAttributes.addFlashAttribute("message", "No Data Found Due to Invalid Input or Network Error");
					return modelAndView;
				}
			}
			else {
				//commInvParams null
				modelAndView.setViewName("redirect:/getCommInvoicePage");
				redirecAttributes.addFlashAttribute("message", "No Data Found Due to Invalid Input");
				return modelAndView;
			}

		} catch (IndexOutOfBoundsException ex) {
			modelAndView.setViewName("redirect:/getCommInvoicePage");
			redirecAttributes.addFlashAttribute("message", "No Records Found");
			return modelAndView;
		}

	}

	@RequestMapping(value = "/getCommInvoicePage", method = RequestMethod.GET)
	public String getCommercialInvoiceSearchPage(ModelMap model) {
		logger.info("Method CommercialInvoiceController.getCommercialInvoiceSearchPage starts.");

		CommercialInvoiceParams commercialInvoiceParams = new CommercialInvoiceParams();
		model.addAttribute("commercialInvoice", commercialInvoiceParams);

		logger.info("Method CommercialInvoiceController.getCommercialInvoiceSearchPage ends.");

		return "commInvoiceSearch";
	}
}
