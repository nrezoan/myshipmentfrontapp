package com.myshipment.controller;

import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.myshipment.dto.LoginDTO;
import com.myshipment.model.BapiReturn3;
import com.myshipment.model.BookingDisplayJsonOutputData;
import com.myshipment.model.BookingDisplayParams;
import com.myshipment.model.CRMLogger;
import com.myshipment.model.DirectBookingParams;
import com.myshipment.model.DirectBookingUpdateJsonData;
import com.myshipment.model.MaterialsDto;
import com.myshipment.model.OrderHeader;
import com.myshipment.model.OrderItem;
import com.myshipment.service.IBookingUpdateService;
import com.myshipment.service.IOrderService;
import com.myshipment.service.IUpdateLogger;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;
import com.myshipment.util.SessionUtil;
import com.myshipment.util.StringUtil;
import com.myshipment.util.SessionUtil;
import com.myshipment.util.DateUtil;

/*
 * @Ranjeet Kumar
 */
@Controller
public class BookingUpdateController {

	private static final Logger logger = Logger.getLogger(BookingUpdateController.class);

	@Autowired
	private IBookingUpdateService bookingUpdateService;

	@Autowired
	private RestService restService;
	
	@Autowired
	private IOrderService orderService;
	
	@Autowired
	private IUpdateLogger updateLoggerService;

	@RequestMapping(value = "/doBookingUpdate", method = RequestMethod.POST)
	public @ResponseBody DirectBookingUpdateJsonData doBookingUpdate(
			@RequestBody DirectBookingParams directBookingParams, Model model, HttpServletRequest request) {

		logger.info("Method BookingUpdateController.doBookingUpdate starts.");
		try {
			if (directBookingParams != null) {

				HttpSession session = SessionUtil.getHttpSession(request);
				LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
				OrderHeader orderHeader = directBookingParams.getOrderHeader();
				orderHeader.setSalesOrg(loginDTO.getSalesOrgSelected());
				orderHeader.setDistChannel(loginDTO.getDisChnlSelected());
				orderHeader.setDivision(loginDTO.getDivisionSelected());
				List<CRMLogger> updateLoggerList = directBookingParams.getLoggerUpdate();
				for(CRMLogger updateLogger : updateLoggerList ) {
					updateLogger.setHblNumber(orderHeader.getHblNumber());
					updateLogger.setShipper(loginDTO.getLoggedInUserName());
					updateLogger.setDistChannel(loginDTO.getDisChnlSelected());
					updateLogger.setDivision(loginDTO.getDivisionSelected());	
					updateLogger.setSalesOrg(loginDTO.getSalesOrgSelected());
					updateLogger.setUpdatedDate(new Date());
					updateLogger.setBuyer(orderHeader.getBuyer());
				}
			

				DirectBookingUpdateJsonData bookingDisplayJsonOutputData = bookingUpdateService
						.doBookingUpdate(directBookingParams);
				bookingDisplayJsonOutputData.setHblnumber(orderHeader.getHblNumber());
				bookingDisplayJsonOutputData.setZsalesdocument(orderHeader.getDocNumber());
				try {
				int result = updateLoggerService.getUpdateLogger(updateLoggerList);
				}catch (Exception e) {
					
				}
				if (bookingDisplayJsonOutputData != null) {
					// model.addAttribute("bookingDisplayJsonOutputData",
					// bookingDisplayJsonOutputData);

					session.setAttribute("directBookingUpdateJsonData", bookingDisplayJsonOutputData);
					return bookingDisplayJsonOutputData;// need to replace with
														// the actual jsp name
				} else {
					logger.info("Value of the bookingDisplayJsonOutputData is : " + bookingDisplayJsonOutputData);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("Some exception occurred in the method BookingUpdateController.doBookingUpdate : " + e);
		}

		return null;
	}

	@RequestMapping(value = "/bookingupdateresponse", method = RequestMethod.GET)
	public String bookingresponse(HttpServletRequest request, Model model) {
		logger.debug(this.getClass().getName() + "In bookingresponse( )  --start");
		HttpSession session = SessionUtil.getHttpSession(request);
		DirectBookingUpdateJsonData directBookingJsonData = (DirectBookingUpdateJsonData) session
				.getAttribute("directBookingUpdateJsonData");
		if (null != directBookingJsonData) {
			boolean success = true;
			for (Iterator<BapiReturn3> iterator = directBookingJsonData.getBapiReturn3().iterator(); iterator.hasNext();) {
				BapiReturn3 type = (BapiReturn3) iterator.next();
				if (type.getType().equals("E")) {
					success = false;
					break;
				}
			}
			model.addAttribute("jsonResponse", directBookingJsonData);
			model.addAttribute("success", success);
		}
		return "booking-update-response";
	}
	
	@RequestMapping(value = "/deleteitem", method = { RequestMethod.GET, RequestMethod.POST })
	public String deleteItem(HttpServletRequest httpServletRequest, @RequestParam("itemId") Integer param, Model model)
			throws IllegalAccessException, InvocationTargetException {

		logger.debug(this.getClass().getName() + "in  deleteItem( ) --start    itemid= :" + param);
		HttpSession httpSession = SessionUtil.getHttpSession(httpServletRequest);
		DirectBookingParams bookingParams = (DirectBookingParams) httpSession
				.getAttribute(SessionUtil.BOOKING_UPDATE_ATTRIBUTE);
		logger.debug(this.getClass().getName() + " deleteItem( ) bookingParams= " + bookingParams.toString());
		if (param != null && bookingParams != null) {
			List<OrderItem> orderItemLst = bookingParams.getOrderItemLst();
			OrderItem orderItem = null;
			for (OrderItem orderItem2 : orderItemLst) {

				if (Integer.parseInt(orderItem2.getItemNumber()) == param) {
					orderItem = orderItem2;
					logger.debug(this.getClass().getName() + " deleteItem( ) removed item is+" + orderItem);
				}
			}
			orderItemLst.remove(orderItem);
			bookingParams.setOrderItemLst(orderItemLst);
		}

		HttpSession session = SessionUtil.getHttpSession(httpServletRequest);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		OrderHeader orderHeader = bookingParams.getOrderHeader();
		orderHeader.setSalesOrg(loginDTO.getSalesOrgSelected());
		orderHeader.setDistChannel(loginDTO.getDisChnlSelected());
		orderHeader.setDivision(loginDTO.getDivisionSelected());

		DirectBookingUpdateJsonData bookingDisplayJsonOutputData = bookingUpdateService.doBookingUpdate(bookingParams);
		boolean success = true;
		if (bookingDisplayJsonOutputData != null) {

			List<BapiReturn3> lstBapiReturn3 = bookingDisplayJsonOutputData.getBapiReturn3();
			for (Iterator<BapiReturn3> iterator = lstBapiReturn3.iterator(); iterator.hasNext();) {
				BapiReturn3 bapiReturn3 = (BapiReturn3) iterator.next();
				if (bapiReturn3.getType().equals("E") || bapiReturn3.getType().equals("A")) {
					success = false;
					break;
				}
			}
		}
		ObjectMapper objectMapper = new ObjectMapper();
		String jSON = null;

		BookingDisplayParams bookingDisplayParams = new BookingDisplayParams();
		bookingDisplayParams.setSalesOrderNumber(bookingParams.getOrderHeader().getDocNumber());
		StringBuffer webServiceUrl = new StringBuffer(RestUtil.BOOKING_DISPLAY);
		BookingDisplayJsonOutputData bookingDisplayJsonOutputData2 = restService.postForObject(
				RestUtil.prepareUrlForService(webServiceUrl).toString(), bookingDisplayParams,
				BookingDisplayJsonOutputData.class);

		if (bookingDisplayJsonOutputData2 != null) {

			bookingDisplayJsonOutputData2.getHeaderDisplayDTO();
			bookingDisplayJsonOutputData2.getHeaderTextDtoLst();
			bookingDisplayJsonOutputData2.getItemDtoLst();
			bookingDisplayJsonOutputData2.getItemTextDtoLst();
			bookingDisplayJsonOutputData2.getPartnerDtoLst();
			bookingParams = BookingDisplayController.createBookingObject(bookingDisplayJsonOutputData2);
			orderHeader = bookingParams.getOrderHeader();
			orderHeader.setDocNumber(bookingDisplayParams.getSalesOrderNumber());
			bookingParams.setOrderHeader(orderHeader);
			try {
				jSON = objectMapper.writeValueAsString(bookingParams);
			} catch (JsonProcessingException e) {

				e.printStackTrace();
			}
		}
		if (success) {

			model.addAttribute("updateStatus", success);
			model.addAttribute("updateMessage", "Updated Successfully !");
			httpSession.setAttribute(SessionUtil.BOOKING_UPDATE_ATTRIBUTE, bookingParams);

			model.addAttribute("directBookingParamsJson", jSON);
			model.addAttribute("directBookingParams", bookingParams);
		}

		else {
			StringBuffer message = new StringBuffer();
			for (Iterator<BapiReturn3> iterator = bookingDisplayJsonOutputData.getBapiReturn3().iterator(); iterator.hasNext();) {
				BapiReturn3 type = (BapiReturn3) iterator.next();
				if (type.getType().equalsIgnoreCase("E") || type.getType().equalsIgnoreCase("A"))
					message.append(type.getMessage()).append(". ");

			}

			model.addAttribute("updateStatus", success);
			model.addAttribute("updateMessage", message);
			httpSession.setAttribute(SessionUtil.BOOKING_UPDATE_ATTRIBUTE, bookingParams);

			model.addAttribute("directBookingParamsJson", jSON);
			model.addAttribute("directBookingParams", bookingParams);
		}

		//return "redirect:/bookingupdateresponse";
		
		//hamid//material passing
		List<MaterialsDto> lstMaterialsDto=orderService.getMaterialService("");
		model.addAttribute("materials",lstMaterialsDto);
		//declaring variables
		List<OrderItem> lstOrderItemForDisplay = bookingParams.getOrderItemLst();
		String grDone = "";
		Integer checkSum = 0;
		Integer curtonQuantity = 0;
		Double totalVolume = 0.0;
		Double grossWeight = 0.0;					
		for (OrderItem orderItem2 : lstOrderItemForDisplay) {
			curtonQuantity = curtonQuantity + orderItem2.getCurtonQuantity();
			grossWeight = grossWeight + orderItem2.getGrossWeight();
			totalVolume = totalVolume + orderItem2.getTotalVolume();
			if(orderItem2.getGrFlag()!=null && orderItem2.getGrFlag().equals("x"))
				checkSum++;
			else
				orderItem2.setGrFlag("");
		}
		if(checkSum == 0){
			//goods not received for any line item
			grDone = "show";
		}
		else {
			//goods received for all/some line items
			grDone = "hide";
		}
		grossWeight = Math.round(grossWeight * 100.0) / 100.0;	
		totalVolume = Math.round(totalVolume * 100.0) / 100.0;
		
		model.addAttribute("totalQuantity", curtonQuantity);
		model.addAttribute("grossWeight", grossWeight);
		model.addAttribute("totalCBM", totalVolume);
		model.addAttribute("grDone", grDone);	
		
		return "booking-update";
	}
	//BOOKING DELETE ENDS


	@RequestMapping(value = "/additem", method = { RequestMethod.GET, RequestMethod.POST })
	public @ResponseBody DirectBookingUpdateJsonData addItem(HttpServletRequest httpServletRequest,
			@RequestBody OrderItem orderItem, Model model) throws IllegalAccessException, InvocationTargetException
	// public String addItem(HttpServletRequest httpServletRequest, @RequestBody
	// OrderItem orderItem, Model model) throws IllegalAccessException,
	// InvocationTargetException
	{
		logger.debug(this.getClass().getName() + "in  addItem( ) --start");
		HttpSession httpSession = SessionUtil.getHttpSession(httpServletRequest);
		DirectBookingParams bookingParams = (DirectBookingParams) httpSession
				.getAttribute(SessionUtil.BOOKING_UPDATE_ATTRIBUTE);
		logger.debug(this.getClass().getName() + " addItem( ) bookingParams= " + bookingParams.toString());
		DirectBookingUpdateJsonData bookingDisplayJsonOutputData = null;

		if (bookingParams != null && orderItem != null) {
			HttpSession session = SessionUtil.getHttpSession(httpServletRequest);
			LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
			OrderHeader orderHeader = bookingParams.getOrderHeader();
			orderHeader.setSalesOrg(loginDTO.getSalesOrgSelected());
			orderHeader.setDistChannel(loginDTO.getDisChnlSelected());
			orderHeader.setDivision(loginDTO.getDivisionSelected());
			List<OrderItem> lstOrderItem = bookingParams.getOrderItemLst();
			lstOrderItem = prepareLineItem(lstOrderItem, orderItem);
			// done upto this part

			// bookingParams.setOrderHeader(orderHeader);
			bookingParams.setOrderItemLst(lstOrderItem);
			bookingDisplayJsonOutputData = bookingUpdateService.doBookingUpdate(bookingParams);
		}
		return bookingDisplayJsonOutputData;
		// return "booking-update";
	}

	private List<OrderItem> prepareLineItem(List<OrderItem> lstOrderItem, OrderItem orderItem) {
		Integer max = 0;
		for (OrderItem orderItem2 : lstOrderItem) {
			if (Integer.parseInt(orderItem2.getItemNumber()) > max)
				max = Integer.parseInt(orderItem2.getItemNumber());
		}
		max = max + 10;
		orderItem.setItemNumber(StringUtil.prepareItemNumberForSAP(max.toString()));
		lstOrderItem.add(orderItem);
		return lstOrderItem;
	}

}
