package com.myshipment.controller;

import java.util.ArrayList;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.myshipment.dto.LoginDTO;
import com.myshipment.dto.MyshipmentTrackContainerBeanDto;
import com.myshipment.dto.OrderHeaderDTO;
import com.myshipment.model.AirExportHAWBData;
import com.myshipment.model.AirImportHAWBData;
import com.myshipment.model.DSRParam;
import com.myshipment.model.DSRformParamsAir;
import com.myshipment.model.DSRformParamsSea;
import com.myshipment.model.MyshipmentTrackContainerBean;
import com.myshipment.model.MyshipmentTrackItemBean;
import com.myshipment.model.MyshipmentTrackJsonOutputData;
import com.myshipment.model.MyshipmentTrackParams;
import com.myshipment.model.SeaExportHBLData;
import com.myshipment.model.SeaImportHBLData;
import com.myshipment.model.TrackingRequestParam;
import com.myshipment.service.ICargoAckService;
import com.myshipment.util.CommonConstant;
import com.myshipment.util.DateUtil;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;
import com.myshipment.util.SessionUtil;

/**
 * @author Nusrat Momtahana
 *
 */
@Controller
public class DSRController extends BaseController {

	@Autowired
	private ICargoAckService cargoAckService;

	@Autowired
	private RestService restService;

	private Logger logger = Logger.getLogger(DSRController.class);

	@RequestMapping(value = "/getDSRForm", method = RequestMethod.GET)
	public String getDSRform(ModelMap model) {
		logger.info(this.getClass().getName() + " Returning Login Page");
		TrackingRequestParam trackParams = new TrackingRequestParam();
		model.addAttribute("trackParams", trackParams);
		return "dsrForm";

	}

	@RequestMapping(value = "/salesReportUpdate", method = RequestMethod.POST)
	public String getTrackingInfo(@ModelAttribute("trackingRequestParam") TrackingRequestParam trackingRequestParam,
			ModelMap model, HttpSession session) {
		logger.info("Method DSRController.getHawbInfo starts");

		try {
			MyshipmentTrackParams myshipmentTrackParams = new MyshipmentTrackParams();
			LoginDTO loginDto = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
			if ((loginDto.getDisChnlSelected().equalsIgnoreCase("EX"))
					&& (loginDto.getDivisionSelected().equalsIgnoreCase("SE"))) {
				myshipmentTrackParams.setAction("01");
			} else if ((loginDto.getDisChnlSelected().equalsIgnoreCase("EX"))
					&& (loginDto.getDivisionSelected().equalsIgnoreCase("AR"))) {
				myshipmentTrackParams.setAction("03");
			} else if ((loginDto.getDisChnlSelected().equalsIgnoreCase("IM"))
					&& (loginDto.getDivisionSelected().equalsIgnoreCase("SE"))) {
				myshipmentTrackParams.setAction("02");
			} else if ((loginDto.getDisChnlSelected().equalsIgnoreCase("IM"))
					&& (loginDto.getDivisionSelected().equalsIgnoreCase("AR"))) {
				myshipmentTrackParams.setAction("04");
			}
			myshipmentTrackParams.setDoc_no(trackingRequestParam.getBlNo().toUpperCase());
			myshipmentTrackParams.setCustomer(loginDto.getLoggedInUserName());
			myshipmentTrackParams.setDistChan(loginDto.getDisChnlSelected());
			myshipmentTrackParams.setDivision(loginDto.getDivisionSelected());
			myshipmentTrackParams.setSalesOrg(loginDto.getSalesOrgSelected());

			// StringBuffer webServiceUrl = new StringBuffer(RestUtil.OPEN_TRACKING_DETAIL);
			StringBuffer webServiceUrl = new StringBuffer(RestUtil.MYSHIPMENT_COMPLETE_TRACK);
			MyshipmentTrackJsonOutputData myshipmentTrackJsonOutputData = restService.postForObject(
					RestUtil.prepareUrlForService(webServiceUrl).toString(), myshipmentTrackParams,
					MyshipmentTrackJsonOutputData.class);

			if (myshipmentTrackParams != null
					&& myshipmentTrackParams.getAction().equalsIgnoreCase(CommonConstant.SEA_EXPORT_HBL)) {
				if (myshipmentTrackJsonOutputData.getDialogue().equalsIgnoreCase("success")) {
					DSRformParamsSea seaExportHBLData = prepareDataForSeaExportHBL(myshipmentTrackJsonOutputData);
					model.addAttribute("seaExportHBLData", seaExportHBLData);
					return "seaSalesReportUpdate";
				}
				return "trackSearchNoResult";
			}
			/*
			 * if(trackingRequestParam != null &&
			 * trackingRequestParam.getAction().equalsIgnoreCase(CommonConstant.
			 * SEA_IMPORT_HBL)) { SeaImportHBLData seaImportHBLData =
			 * prepareDataForSeaImportHBL(myshipmentTrackJsonOutputData);
			 * model.addAttribute("seaImportHBLData", seaImportHBLData); return
			 * "seaImportHBL"; }
			 */
			if (myshipmentTrackParams != null
					&& myshipmentTrackParams.getAction().equalsIgnoreCase(CommonConstant.AIR_EXPORT_HAWB)) {
				if (myshipmentTrackJsonOutputData.getDialogue().equalsIgnoreCase("success")) {
					DSRformParamsAir airExportHAWBData = prepareDataForAirExportHAWB(myshipmentTrackJsonOutputData);
					model.addAttribute("airExportHAWBData", airExportHAWBData);
					return "airSalesReportUpdate";
				}
				return "trackSearchNoResult";
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			return "redirect:/";
		}

		return "error";

	}

	@RequestMapping(value = "/dsrFormDetails", method = RequestMethod.POST)
	public String saveDSRinfoAir(@ModelAttribute("airExportHAWBData") DSRformParamsAir airExportHAWBData,
			HttpSession session, BindingResult bindingResult, HttpServletRequest request, Model model) {
		logger.info("Method DSRController.saveDSRinfo starts");
		return null;

	}

	@RequestMapping(value = "/dsrFormDetailsSea", method = RequestMethod.POST)
	public String saveDSRinfoSea(@ModelAttribute("seaExportHBLData") DSRformParamsSea seaExportHBLData,
			HttpSession session, BindingResult bindingResult, HttpServletRequest request, Model model) {
		logger.info("Method DSRController.saveDSRinfoSea starts");
		return null;

	}

	@RequestMapping(value = "/getDSRpage", method = RequestMethod.GET)
	public String getDSRpage(ModelMap model) {
		logger.info(this.getClass().getName() + " Returning Login Page");
		DSRParam dsrParam = new DSRParam();
		model.addAttribute("dsrParam", dsrParam);
		return "dsrReport";

	}

	@RequestMapping(value = "/getDSRdetails", method = RequestMethod.POST)
	public String getDSRDetails(@ModelAttribute("dsrParam") DSRParam dsrParam, Model model, HttpSession session,
			HttpServletResponse response, HttpServletRequest request, ModelAndView modelAndView,
			RedirectAttributes redirecAttributes, BindingResult result) {
		

		return "dsrReport";

	}

	@RequestMapping(value = "/getWHRpage", method = RequestMethod.GET)
	public String getWHRpage(ModelMap model) {
		logger.info(this.getClass().getName() + " Returning Login Page");
		DSRParam whrParam = new DSRParam();
		model.addAttribute("whrParam", whrParam);
		return "warehouseReport";

	}

	@RequestMapping(value = "/getWHRdetails", method = RequestMethod.POST)
	public String getWHRDetails(@ModelAttribute("whrParam") DSRParam whrParam, Model model, HttpSession session,
			HttpServletResponse response, HttpServletRequest request, ModelAndView modelAndView,
			RedirectAttributes redirecAttributes, BindingResult result) {

		return "warehouseReport";

	}

	private DSRformParamsSea prepareDataForSeaExportHBL(MyshipmentTrackJsonOutputData myshipmentTrackJsonOutputData) {
		List<MyshipmentTrackContainerBeanDto> containerTrackDtoList = new ArrayList<MyshipmentTrackContainerBeanDto>();
		DSRformParamsSea seaExportHBLData = new DSRformParamsSea();
		// seaExportHBLData.setMyshipmentTrackContainer(myshipmentTrackJsonOutputData.getMyshipmentTrackContainer());
		seaExportHBLData.setMyshipmentTrackHeader(myshipmentTrackJsonOutputData.getMyshipmentTrackHeader());
		seaExportHBLData.setMyshipmentTrackItem(myshipmentTrackJsonOutputData.getMyshipmentTrackItem());
		seaExportHBLData.setMyshipmentTrackSchedule(myshipmentTrackJsonOutputData.getMyshipmentTrackSchedule());
		if (myshipmentTrackJsonOutputData.getMyshipmentTrackContainer() != null
				&& !myshipmentTrackJsonOutputData.getMyshipmentTrackContainer().isEmpty()) {

			for (MyshipmentTrackContainerBean mtc : myshipmentTrackJsonOutputData.getMyshipmentTrackContainer()) {
				MyshipmentTrackContainerBeanDto mtcbdto = new MyshipmentTrackContainerBeanDto();
				mtcbdto.setBl_no(mtc.getBl_no());
				mtcbdto.setCont_mode(mtc.getCont_mode());
				mtcbdto.setCont_no(mtc.getCont_no());
				mtcbdto.setCont_size(mtc.getCont_size());
				mtcbdto.setItem_no(mtc.getItem_no());
				mtcbdto.setPacd_pcs(mtc.getPacd_pcs());
				mtcbdto.setPacd_qty(mtc.getPacd_qty());
				mtcbdto.setPacd_vol(mtc.getPacd_vol());
				mtcbdto.setPacd_wt(mtc.getPacd_wt());
				mtcbdto.setSeal_no(mtc.getSeal_no());
				mtcbdto.setSeq_no(mtc.getSeq_no());
				mtcbdto.setSo_no(mtc.getSo_no());
				containerTrackDtoList.add(mtcbdto);
			}
		}
		seaExportHBLData.setMyshipmentTrackContainer(containerTrackDtoList);
		// Setting the Po and style
		if (seaExportHBLData.getMyshipmentTrackContainer() != null
				&& !seaExportHBLData.getMyshipmentTrackContainer().isEmpty()) {
			for (MyshipmentTrackContainerBeanDto myshipmentTrackContainerBean : seaExportHBLData
					.getMyshipmentTrackContainer()) {
				for (MyshipmentTrackItemBean myshipmentTrackItemBean : seaExportHBLData.getMyshipmentTrackItem()) {
					if ((myshipmentTrackContainerBean.getSo_no().equalsIgnoreCase(myshipmentTrackItemBean.getSo_no()))
							&& (myshipmentTrackContainerBean.getItem_no()
									.equalsIgnoreCase(myshipmentTrackItemBean.getItem_no()))) {
						myshipmentTrackContainerBean.setPo_no(myshipmentTrackItemBean.getPo_no());
						myshipmentTrackContainerBean.setStyle(myshipmentTrackItemBean.getStyle());
					}
				}
			}
		}
		return seaExportHBLData;
	}

	private DSRformParamsAir prepareDataForAirExportHAWB(MyshipmentTrackJsonOutputData myshipmentTrackJsonOutputData) {

		DSRformParamsAir airExportHAWBData = new DSRformParamsAir();
		airExportHAWBData.setMyshipmentTrackHeader(myshipmentTrackJsonOutputData.getMyshipmentTrackHeader());
		airExportHAWBData.setMyshipmentTrackItem(myshipmentTrackJsonOutputData.getMyshipmentTrackItem());
		airExportHAWBData.setMyshipmentTrackSchedule(myshipmentTrackJsonOutputData.getMyshipmentTrackSchedule());

		return airExportHAWBData;
	}
}
