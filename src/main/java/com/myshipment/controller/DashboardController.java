package com.myshipment.controller;

//import java.util.stream.Collectors;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.myshipment.dto.LoginDTO;
import com.myshipment.model.BuyerWiseCBMJson;
import com.myshipment.model.BuyerWiseCBMdetails;
import com.myshipment.model.BuyerWiseGWTJson;
import com.myshipment.model.BuyerWiseGWTdetails;
import com.myshipment.model.BuyerWiseShipDetails;
import com.myshipment.model.BuyerWiseShipDetailsbyPOD;
import com.myshipment.model.BuywiseShipDetailsbyBLstatus;
import com.myshipment.model.DashboardInfoBox;
import com.myshipment.model.LastNShipmentsJsonOutputData;
import com.myshipment.model.MyshipmentTrackHeaderBean;
import com.myshipment.model.SOWiseShipmentSummaryJson;
import com.myshipment.model.ShipmentDetailsBuyerJson;
import com.myshipment.model.ShipmentDetailsShipperJson;
import com.myshipment.model.ShipmentStatusDetails;
import com.myshipment.model.SowiseShipDetailsBuyer;
import com.myshipment.model.SowiseShipDetailsShipper;
import com.myshipment.model.SuppWiseShipSummaryJson;
import com.myshipment.model.SupplierWiseCBMDetailJson;
import com.myshipment.model.SupplierWiseCBMJson;
import com.myshipment.model.SupplierWiseGWTDetailJson;
import com.myshipment.model.SupplierWiseGWTJson;
import com.myshipment.model.SupplierWiseShipmentDetailJson;
import com.myshipment.model.SupplierWiseShipmentJson;
import com.myshipment.model.SuppwiseShipDetailsbyBLstatus;
import com.myshipment.model.TopFiveShipBuyerJson;
import com.myshipment.model.TopFiveShipment;
import com.myshipment.model.TopFiveShipmentBuyer;
import com.myshipment.model.TotalGWT;
import com.myshipment.model.TotalShipment;
import com.myshipment.service.IDashboardService;
import com.myshipment.util.CommonConstant;
import com.myshipment.util.DateUtil;
import com.myshipment.util.MyShipApplicationException;
import com.myshipment.util.SessionUtil;

/**
 * @author Gufranur Rahman
 *
 */
@Controller
public class DashboardController extends BaseController {

	@Autowired
	IDashboardService dashboardService;
	// private ILoginService loginService = (ILoginService)
	// ServiceLocator.findService("loginService");

	private Logger logger = Logger.getLogger(DashboardController.class);

	@RequestMapping("/getSalesWiseShipmentPage")
	public String getSalesWiseShipmentPage() {
		logger.info(this.getClass().getName() + " Returning Login Page");
		return "saleswiseshipment";

	}

	@RequestMapping(value = "/shipmentStatusDetails", method = RequestMethod.GET)
	public String getShipmentStatusDetails(@RequestParam("status") String status, HttpServletRequest servletRequest,
			HttpServletResponse servletResponse, Model model) {
		HttpSession session = SessionUtil.getHttpSession(servletRequest);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		DashboardInfoBox dashboardInfoBox = (DashboardInfoBox) session.getAttribute(SessionUtil.DASHBOARD_INFO_DETAILS);

		List<MyshipmentTrackHeaderBean> shipmentList = null;

		if (status.equals("openOrder")) {
			shipmentList = dashboardInfoBox.getOpenOrderList();
			model.addAttribute("status", "Order Booked");
		} else if (status.equals("goodsReceived")) {
			shipmentList = dashboardInfoBox.getGoodsReceivedList();
			model.addAttribute("status", "Goods Received");
		} else if (status.equals("stuffingDone")) {
			shipmentList = dashboardInfoBox.getStuffingDoneList();
			model.addAttribute("status", "Stuffing Done");
		} else if (status.equals("inTransit")) {
			shipmentList = dashboardInfoBox.getInTransitList();
			model.addAttribute("status", "In Transit");
		} else if (status.equals("delivered")) {
			shipmentList = dashboardInfoBox.getDeliveredList();
			model.addAttribute("status", "Delivered");
		}

		List<ShipmentStatusDetails> statusWiseList = getStatusWiseShipmentList(shipmentList, loginDTO);

		if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE1)
				|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE2)) {
			model.addAttribute("nameBuyerSupplier", "Buyer Name");
			model.addAttribute("nameAgentSupplier", "Destination Agent");
		} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE1)
				|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE2)) {
			model.addAttribute("nameBuyerSupplier", "Supplier Name");
			model.addAttribute("nameAgentSupplier", "Destination Agent");
		} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.DA_CODE1)
				|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.DA_CODE2)) {
			model.addAttribute("nameBuyerSupplier", "Supplier Name");
			model.addAttribute("nameAgentSupplier", "Buyer Name");
		}

		if (loginDTO.getDivisionSelected().equals("SE")) {
			model.addAttribute("weightHeader", "Gross Weight");
			model.addAttribute("shippingLineHeader", "Shipping Line");
		} else if (loginDTO.getDivisionSelected().equals("AR")) {
			model.addAttribute("weightHeader", "Charge Weight");
			model.addAttribute("shippingLineHeader", "Airline");
		}

		model.addAttribute("statusWiseList", statusWiseList);

		return "shipmentStatusDetails";
	}

	public List<ShipmentStatusDetails> getStatusWiseShipmentList(List<MyshipmentTrackHeaderBean> shipmentList,
			LoginDTO loginDTO) {
		List<ShipmentStatusDetails> statusWiseList = new ArrayList<ShipmentStatusDetails>();
		if (shipmentList != null) {
			for (MyshipmentTrackHeaderBean myshipmentTrackHeaderBean : shipmentList) {
				ShipmentStatusDetails shipmentStatusDetails = new ShipmentStatusDetails();

				if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE1)
						|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE2)) {
					shipmentStatusDetails.setNameBuyerSupplier(myshipmentTrackHeaderBean.getBuyer());
					shipmentStatusDetails.setNameAgentSupplier(myshipmentTrackHeaderBean.getAgent());
				} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE1)
						|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE2)) {
					shipmentStatusDetails.setNameBuyerSupplier(myshipmentTrackHeaderBean.getShipper());
					shipmentStatusDetails.setNameAgentSupplier(myshipmentTrackHeaderBean.getAgent());
				} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.DA_CODE1)
						|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.DA_CODE2)) {
					shipmentStatusDetails.setNameBuyerSupplier(myshipmentTrackHeaderBean.getShipper());
					shipmentStatusDetails.setNameAgentSupplier(myshipmentTrackHeaderBean.getBuyer());
				}

				shipmentStatusDetails.setBlNo(myshipmentTrackHeaderBean.getBl_no());

				shipmentStatusDetails.setMblNo(myshipmentTrackHeaderBean.getMbl_no());
				shipmentStatusDetails.setShippingLine(myshipmentTrackHeaderBean.getCarrier());
				shipmentStatusDetails.setPlaceOfReceipt(myshipmentTrackHeaderBean.getPor());

				shipmentStatusDetails.setTotalCBM(myshipmentTrackHeaderBean.getVolume());
				if (loginDTO.getDivisionSelected().equals("SE")) {
					shipmentStatusDetails.setTotalWeight(myshipmentTrackHeaderBean.getGrs_wt());
				} else if (loginDTO.getDivisionSelected().equals("AR")) {
					shipmentStatusDetails.setTotalWeight(myshipmentTrackHeaderBean.getCrg_wt());
				}
				shipmentStatusDetails.setTotalQuantity(myshipmentTrackHeaderBean.getTot_qty());

				shipmentStatusDetails
						.setBlDate(DateUtil.formatStringToDatenew(myshipmentTrackHeaderBean.getBl_bt(), "dd-MM-yyyy"));
				shipmentStatusDetails
						.setEtd(DateUtil.formatStringToDatenew(myshipmentTrackHeaderBean.getDep_dt(), "dd-MM-yyyy"));
				shipmentStatusDetails
						.setEta(DateUtil.formatStringToDatenew(myshipmentTrackHeaderBean.getEta_dt(), "dd-MM-yyyy"));
				shipmentStatusDetails
						.setGr_date(DateUtil.formatStringToDatenew(myshipmentTrackHeaderBean.getGr_dt(), "dd-MM-yyyy"));
				shipmentStatusDetails.setStuffingDate(
						DateUtil.formatStringToDatenew(myshipmentTrackHeaderBean.getStuff_dt(), "dd-MM-yyyy"));
				shipmentStatusDetails.setShipmentDate(
						DateUtil.formatStringToDatenew(myshipmentTrackHeaderBean.getDep_dt(), "dd-MM-yyyy"));

				shipmentStatusDetails.setPol(myshipmentTrackHeaderBean.getPol());
				shipmentStatusDetails.setPod(myshipmentTrackHeaderBean.getPod());

				statusWiseList.add(shipmentStatusDetails);
			}
		}

		return statusWiseList;
	}

	public Date convertStringToDate_YYYYMMDD(String dateString) {
		Date date = null;
		if (dateString != null && dateString.equals("")) {
			date = DateUtil.formatStringToDate(dateString);
		}
		return date;
	}

	@RequestMapping(value = "/topShipment", method = RequestMethod.GET)
	public String getTopShipment(@RequestParam("port") String port, HttpServletRequest servletRequest,
			HttpServletResponse servletResponse, Model model) {
		HttpSession session = SessionUtil.getHttpSession(servletRequest);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		DashboardInfoBox dashboardInfoBox = (DashboardInfoBox) session.getAttribute(SessionUtil.DASHBOARD_INFO_DETAILS);

		List<MyshipmentTrackHeaderBean> totalShipmentList = dashboardInfoBox.getTotalShipmentList();
		List<MyshipmentTrackHeaderBean> portWiseShipmentList = new ArrayList<MyshipmentTrackHeaderBean>();

		for (MyshipmentTrackHeaderBean myshipmentTrackHeaderBean : totalShipmentList) {
			if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE1)
					|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE2)) {
				if (myshipmentTrackHeaderBean.getPod().equals(port)) {
					portWiseShipmentList.add(myshipmentTrackHeaderBean);
				}
			} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE1)
					|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE2)
					|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.DA_CODE1)
					|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.DA_CODE2)) {
				if (myshipmentTrackHeaderBean.getPol().equals(port)) {
					portWiseShipmentList.add(myshipmentTrackHeaderBean);
				}
			}
		}

		model.addAttribute("portWiseShipmentList", portWiseShipmentList);
		model.addAttribute("port", port);
		if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE1)
				|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE2)) {
			model.addAttribute("portTitle", "Port of Discharge");
			model.addAttribute("portTable", "Port of Loading");
			model.addAttribute("accountGroup", "Buyer");
		} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE1)
				|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE2)
				|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.DA_CODE1)
				|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.DA_CODE2)) {
			model.addAttribute("portTitle", "Port of Loading");
			model.addAttribute("portTable", "Port of Discharge");
			model.addAttribute("accountGroup", "Supplier");
		}

		return "topShipment";
	}

	@RequestMapping(value = "/dashboardInfoDetails", method = RequestMethod.GET)
	public String getDashboardDetails(@RequestParam("details") String details, HttpServletRequest servletRequest,
			HttpServletResponse servletResponse, Model model) {

		HttpSession session = SessionUtil.getHttpSession(servletRequest);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		DashboardInfoBox dashboardInfoBox = (DashboardInfoBox) session.getAttribute(SessionUtil.DASHBOARD_INFO_DETAILS);

		if (dashboardInfoBox != null) {

			if (details.equals("totalBooking")) {

				List<TotalShipment> totalShipment = getDashboardShipmentList(dashboardInfoBox.getTotalShipmentList(),
						loginDTO);

				if (totalShipment.size() == 0) {
					model.addAttribute("error", "No Data Found");
				} else {
					model.addAttribute("totalShipmentList", totalShipment);
					model.addAttribute("totalShipmentListJson", new Gson().toJson(totalShipment));
				}

				model.addAttribute("status", "Total Booking");

			} else if (details.equals("openBooking")) {

				List<TotalShipment> openOrder = getDashboardShipmentList(dashboardInfoBox.getOpenOrderList(), loginDTO);

				if (openOrder.size() == 0) {
					model.addAttribute("error", "No Data Found");
				} else {
					model.addAttribute("totalShipmentList", openOrder);
					model.addAttribute("totalShipmentListJson", new Gson().toJson(openOrder));
				}

				model.addAttribute("status", "Open Booking");

			} else if (details.equals("goodsReceived")) {

				List<TotalShipment> openOrder = getDashboardShipmentList(dashboardInfoBox.getGoodsReceivedList(),
						loginDTO);

				if (openOrder.size() == 0) {
					model.addAttribute("error", "No Data Found");
				} else {
					model.addAttribute("totalShipmentList", openOrder);
					model.addAttribute("totalShipmentListJson", new Gson().toJson(openOrder));
				}

				model.addAttribute("status", "Goods Handover");

			} else if (details.equals("stuffingDone")) {

				List<TotalShipment> openOrder = getDashboardShipmentList(dashboardInfoBox.getStuffingDoneList(),
						loginDTO);

				if (openOrder.size() == 0) {
					model.addAttribute("error", "No Data Found");
				} else {
					model.addAttribute("totalShipmentList", openOrder);
					model.addAttribute("totalShipmentListJson", new Gson().toJson(openOrder));
				}

				if (loginDTO.getDivisionSelected().equals("SE")) {
					model.addAttribute("status", "Stuffing Done");
				} else if (loginDTO.getDivisionSelected().equals("AR")) {
					model.addAttribute("status", "MAWB Issued");
				}

			} else if (details.equals("inTransit")) {

				List<TotalShipment> inTransit = getDashboardShipmentList(dashboardInfoBox.getInTransitList(), loginDTO);

				if (inTransit.size() == 0) {
					model.addAttribute("error", "No Data Found");
				} else {
					model.addAttribute("totalShipmentList", inTransit);
					model.addAttribute("totalShipmentListJson", new Gson().toJson(inTransit));
				}

				model.addAttribute("status", "In Transit (Estimated)");

			} else if (details.equals("arrived")) {

				List<TotalShipment> openOrder = getDashboardShipmentList(dashboardInfoBox.getDeliveredList(), loginDTO);

				if (openOrder.size() == 0) {
					model.addAttribute("error", "No Data Found");
				} else {
					model.addAttribute("totalShipmentList", openOrder);
					model.addAttribute("totalShipmentListJson", new Gson().toJson(openOrder));
				}

				model.addAttribute("status", "Delivered (Estimated)");

			} else if (details.equals("totalGWTCBM")) {

				Map<String, TotalGWT> totalShipmentCountMap = new HashMap<String, TotalGWT>();

				for (MyshipmentTrackHeaderBean myshipmentTrackHeaderBean : dashboardInfoBox.getTotalShipmentList()) {

					String compName = null;
					String id = null;
					if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE1)
							|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE2)) {
						compName = myshipmentTrackHeaderBean.getBuyer();
						id = myshipmentTrackHeaderBean.getBuyer_no();
					} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE1)
							|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE2)) {
						compName = myshipmentTrackHeaderBean.getShipper();
						id = myshipmentTrackHeaderBean.getShipper_no();
					} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.DA_CODE1)
							|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.DA_CODE2)) {
						compName = myshipmentTrackHeaderBean.getShipper();
						id = myshipmentTrackHeaderBean.getShipper_no();
					}
					if (totalShipmentCountMap.containsKey(compName)) {
						TotalGWT totalGWT = totalShipmentCountMap.get(compName);

						double grossWeight = myshipmentTrackHeaderBean.getGrs_wt() != null
								? myshipmentTrackHeaderBean.getGrs_wt().doubleValue()
								: 0.0;
						double cbmCrg = 0.0;

						if (loginDTO.getDivisionSelected().equals("SE")) {
							cbmCrg = myshipmentTrackHeaderBean.getVolume() != null
									? myshipmentTrackHeaderBean.getVolume().doubleValue()
									: 0.0;
						} else if (loginDTO.getDivisionSelected().equals("AR")) {
							cbmCrg = myshipmentTrackHeaderBean.getCrg_wt() != null
									? myshipmentTrackHeaderBean.getCrg_wt().doubleValue()
									: 0.0;
						}

						totalGWT.setTotalGWT(totalGWT.getTotalGWT() + grossWeight);
						totalGWT.setTotalCBMCRG(totalGWT.getTotalCBMCRG() + cbmCrg);

						double gwtPercentage = (totalGWT.getTotalGWT() / dashboardInfoBox.getTotalGWT()) * 100;
						double cbmCrgPercentage = 0.0;

						if (loginDTO.getDivisionSelected().equals("SE")) {
							cbmCrgPercentage = (totalGWT.getTotalCBMCRG() / dashboardInfoBox.getTotalCBM()) * 100;
						} else if (loginDTO.getDivisionSelected().equals("AR")) {
							cbmCrgPercentage = (totalGWT.getTotalCBMCRG() / dashboardInfoBox.getTotalCRGWT()) * 100;
						}

						totalGWT.setTotalGWTPercentage(gwtPercentage);
						totalGWT.setTotalCBMCRGPercentage(cbmCrgPercentage);

						totalShipmentCountMap.put(compName, totalGWT);
					} else {
						TotalGWT totalGWT = new TotalGWT();
						totalGWT.setName(compName);
						totalGWT.setId(id);

						double grossWeight = myshipmentTrackHeaderBean.getGrs_wt() != null
								? myshipmentTrackHeaderBean.getGrs_wt().doubleValue()
								: 0.0;
						double cbmCrg = myshipmentTrackHeaderBean.getVolume() != null
								? myshipmentTrackHeaderBean.getVolume().doubleValue()
								: 0.0;

						if (loginDTO.getDivisionSelected().equals("SE")) {
							cbmCrg = myshipmentTrackHeaderBean.getVolume() != null
									? myshipmentTrackHeaderBean.getVolume().doubleValue()
									: 0.0;
						} else if (loginDTO.getDivisionSelected().equals("AR")) {
							cbmCrg = myshipmentTrackHeaderBean.getCrg_wt() != null
									? myshipmentTrackHeaderBean.getCrg_wt().doubleValue()
									: 0.0;
						}

						totalGWT.setTotalGWT(grossWeight);
						totalGWT.setTotalCBMCRG(cbmCrg);

						double gwtPercentage = (totalGWT.getTotalGWT() / dashboardInfoBox.getTotalGWT()) * 100;
						double cbmCrgPercentage = 0.0;

						if (loginDTO.getDivisionSelected().equals("SE")) {
							cbmCrgPercentage = (totalGWT.getTotalCBMCRG() / dashboardInfoBox.getTotalCBM()) * 100;
						} else if (loginDTO.getDivisionSelected().equals("AR")) {
							cbmCrgPercentage = (totalGWT.getTotalCBMCRG() / dashboardInfoBox.getTotalCRGWT()) * 100;
						}

						totalGWT.setTotalGWTPercentage(gwtPercentage);
						totalGWT.setTotalCBMCRGPercentage(cbmCrgPercentage);

						totalShipmentCountMap.put(compName, totalGWT);
					}
					// buyerSet.add(buyer);
				}
				List<TotalGWT> totalGWTList = new ArrayList<TotalGWT>(totalShipmentCountMap.values());

				if (totalGWTList.size() == 0) {
					model.addAttribute("error", "No Data Found");
				} else {
					model.addAttribute("totalGWTList", totalGWTList);
					model.addAttribute("totalGWTListJson", new Gson().toJson(totalGWTList));
				}

				if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE1)
						|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE2)) {
					model.addAttribute("chartHeaderGWT", "Buyer wise Gross Weight");
					model.addAttribute("name", "Buyer Name");
					model.addAttribute("listHeader", "Top Buyers");
					model.addAttribute("status", "Total Volume (CBM)");
					if (loginDTO.getDivisionSelected().equals("SE")) {
						model.addAttribute("chartHeaderCBMCRG", "Buyer wise Volume (CBM)");
						model.addAttribute("status", "Total Gross Weight");
					} else if (loginDTO.getDivisionSelected().equals("AR")) {
						model.addAttribute("chartHeaderCBMCRG", "Buyer wise Charge Weight");
						model.addAttribute("status", "Total Charge Weight");
					}
				} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE1)
						|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE2)) {
					model.addAttribute("chartHeaderGWT", "Shipper wise Gross Weight");
					model.addAttribute("name", "Supplier Name");
					model.addAttribute("listHeader", "Top Suppliers");
					model.addAttribute("status", "Total Volume (CBM)");
					if (loginDTO.getDivisionSelected().equals("SE")) {
						model.addAttribute("chartHeaderCBMCRG", "Shipper wise Volume (CBM)");
						model.addAttribute("status", "Total Gross Weight");
					} else if (loginDTO.getDivisionSelected().equals("AR")) {
						model.addAttribute("chartHeaderCBMCRG", "Shipper wise Charge Weight");
						model.addAttribute("status", "Total Charge Weight");
					}
				} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.DA_CODE1)
						|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.DA_CODE2)) {
					model.addAttribute("chartHeaderGWT", "Shipper wise Gross Weight");
					model.addAttribute("name", "Supplier Name");
					model.addAttribute("listHeader", "Top Suppliers");
					model.addAttribute("status", "Total Volume (CBM)");
					if (loginDTO.getDivisionSelected().equals("SE")) {
						model.addAttribute("chartHeaderCBMCRG", "Shipper wise Volume (CBM)");
						model.addAttribute("status", "Total Gross Weight");
					} else if (loginDTO.getDivisionSelected().equals("AR")) {
						model.addAttribute("chartHeaderCBMCRG", "Shipper wise Charge Weight");
					}
				}
			} else if (details.equals("totalGWT")) {

				List<TotalGWT> totalGWTList = getTotalGWTList(dashboardInfoBox, loginDTO, details);

				if (totalGWTList.size() == 0) {
					model.addAttribute("error", "No Data Found");
				} else {
					model.addAttribute("totalGWTList", totalGWTList);
					model.addAttribute("totalGWTListJson", new Gson().toJson(totalGWTList));
				}

			} else if (details.equals("totalCBM")) {

				List<TotalGWT> totalGWTList = getTotalGWTList(dashboardInfoBox, loginDTO, details);

				if (totalGWTList.size() == 0) {
					model.addAttribute("error", "No Data Found");
				} else {
					model.addAttribute("totalGWTList", totalGWTList);
					model.addAttribute("totalGWTListJson", new Gson().toJson(totalGWTList));
				}

			}

			if (details.equals("totalGWT") || details.equals("totalCBM")) {
				if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE1)
						|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE2)) {
					if (details.equals("totalGWT")) {
						model.addAttribute("chartHeader", "Buyer wise Gross Weight");
						model.addAttribute("value", "Gross Weight");
						model.addAttribute("percentage", "% of Gross Weight");
						model.addAttribute("status", "Total Gross Weight");
					} else if (details.equals("totalCBM")) {
						if (loginDTO.getDivisionSelected().equals("SE")) {
							model.addAttribute("chartHeader", "Buyer wise Volume (CBM)");
							model.addAttribute("value", "Volume (CBM)");
							model.addAttribute("percentage", "% of Volume (CBM)");
							model.addAttribute("status", "Total Volume (CBM)");
						} else if (loginDTO.getDivisionSelected().equals("AR")) {
							model.addAttribute("chartHeader", "Buyer wise Charge Weight");
							model.addAttribute("value", "Charge Weight");
							model.addAttribute("percentage", "% of Charge Weight");
							model.addAttribute("status", "Total Charge Weight");
						}
					}

					model.addAttribute("name", "Buyer Name");
					model.addAttribute("listHeader", "Buyers");
				} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE1)
						|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE2)) {
					if (details.equals("totalGWT")) {
						model.addAttribute("chartHeader", "Supplier wise Gross Weight");
						model.addAttribute("value", "Gross Weight");
						model.addAttribute("percentage", "% of Gross Weight");
						model.addAttribute("status", "Total Gross Weight");
					} else if (details.equals("totalCBM")) {
						if (loginDTO.getDivisionSelected().equals("SE")) {
							model.addAttribute("chartHeader", "Supplier wise Volume (CBM)");
							model.addAttribute("value", "Volume (CBM)");
							model.addAttribute("percentage", "% of Volume (CBM)");
							model.addAttribute("status", "Total Volume (CBM)");
						} else if (loginDTO.getDivisionSelected().equals("AR")) {
							model.addAttribute("chartHeader", "Supplier wise Charge Weight");
							model.addAttribute("value", "Charge Weight");
							model.addAttribute("percentage", "% of Charge Weight");
							model.addAttribute("status", "Total Charge Weight");
						}
					}

					model.addAttribute("name", "Supplier Name");
					model.addAttribute("listHeader", "Suppliers");
				} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.DA_CODE1)
						|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.DA_CODE2)) {
					if (details.equals("totalGWT")) {
						model.addAttribute("chartHeader", "Supplier wise Gross Weight");
						model.addAttribute("value", "Gross Weight");
						model.addAttribute("percentage", "% of Gross Weight");
					} else if (details.equals("totalCBM")) {
						if (loginDTO.getDivisionSelected().equals("SE")) {
							model.addAttribute("chartHeader", "Supplier wise Volume (CBM)");
						} else if (loginDTO.getDivisionSelected().equals("AR")) {
							model.addAttribute("chartHeader", "Supplier wise Charge Weight");
						}
					}

					model.addAttribute("name", "Supplier Name");
					model.addAttribute("listHeader", "Suppliers");
				}
			}

			if (details.equals("totalBooking") || details.equals("openBooking") || details.equals("goodsReceived")
					|| details.equals("stuffingDone") || details.equals("inTransit") || details.equals("arrived")) {
				if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE1)
						|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE2)) {
					model.addAttribute("chartHeader", "Buyer wise Shipment");
					model.addAttribute("name", "Buyer Name");
					model.addAttribute("listHeader", "Top Buyers");
				} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE1)
						|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE2)) {
					model.addAttribute("chartHeader", "Supplier wise Shipment");
					model.addAttribute("name", "Supplier Name");
					model.addAttribute("listHeader", "Top Suppliers");
				} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.DA_CODE1)
						|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.DA_CODE2)) {
					model.addAttribute("chartHeader", "Supplier wise Shipment");
					model.addAttribute("name", "Supplier Name");
					model.addAttribute("listHeader", "Top Suppliers");
				}
			}

		}

		logger.info(this.getClass().getName() + " Returning Login Page");
		return "dashboardInfoDetails";

	}

	public List<TotalGWT> getTotalGWTList(DashboardInfoBox dashboardInfoBox, LoginDTO loginDTO, String details) {

		Map<String, TotalGWT> totalShipmentCountMap = new HashMap<String, TotalGWT>();

		for (MyshipmentTrackHeaderBean myshipmentTrackHeaderBean : dashboardInfoBox.getTotalShipmentList()) {

			String compName = null;
			String id = null;
			if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE1)
					|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE2)) {
				compName = myshipmentTrackHeaderBean.getBuyer();
				id = myshipmentTrackHeaderBean.getBuyer_no();
			} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE1)
					|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE2)) {
				compName = myshipmentTrackHeaderBean.getShipper();
				id = myshipmentTrackHeaderBean.getShipper_no();
			} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.DA_CODE1)
					|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.DA_CODE2)) {
				compName = myshipmentTrackHeaderBean.getShipper();
				id = myshipmentTrackHeaderBean.getShipper_no();
			}
			if (totalShipmentCountMap.containsKey(compName)) {
				TotalGWT totalGWT = totalShipmentCountMap.get(compName);

				double totalValue = 0.0;

				if (details.equals("totalGWT")) {
					totalValue = myshipmentTrackHeaderBean.getGrs_wt() != null
							? myshipmentTrackHeaderBean.getGrs_wt().doubleValue()
							: 0.0;
				} else if (details.equals("totalCBM")) {
					if (loginDTO.getDivisionSelected().equals("SE")) {
						totalValue = myshipmentTrackHeaderBean.getVolume() != null
								? myshipmentTrackHeaderBean.getVolume().doubleValue()
								: 0.0;
					} else if (loginDTO.getDivisionSelected().equals("AR")) {
						totalValue = myshipmentTrackHeaderBean.getCrg_wt() != null
								? myshipmentTrackHeaderBean.getCrg_wt().doubleValue()
								: 0.0;
					}
				}

				totalGWT.setTotalValue(totalGWT.getTotalValue() + totalValue);

				double totalValuePercentage = 0.0;

				if (details.equals("totalGWT")) {
					totalValuePercentage = (totalGWT.getTotalValue() / dashboardInfoBox.getTotalGWT()) * 100;
				} else if (details.equals("totalCBM")) {
					if (loginDTO.getDivisionSelected().equals("SE")) {
						totalValuePercentage = (totalGWT.getTotalValue() / dashboardInfoBox.getTotalCBM()) * 100;
					} else if (loginDTO.getDivisionSelected().equals("AR")) {
						totalValuePercentage = (totalGWT.getTotalValue() / dashboardInfoBox.getTotalCRGWT()) * 100;
					}
				}

				totalGWT.setValuePercentage(totalValuePercentage);

				totalShipmentCountMap.put(compName, totalGWT);
			} else {
				TotalGWT totalGWT = new TotalGWT();
				totalGWT.setName(compName);
				totalGWT.setId(id);

				double totalValue = 0.0;

				if (details.equals("totalGWT")) {
					totalValue = myshipmentTrackHeaderBean.getGrs_wt() != null
							? myshipmentTrackHeaderBean.getGrs_wt().doubleValue()
							: 0.0;
				} else if (details.equals("totalCBM")) {
					if (loginDTO.getDivisionSelected().equals("SE")) {
						totalValue = myshipmentTrackHeaderBean.getVolume() != null
								? myshipmentTrackHeaderBean.getVolume().doubleValue()
								: 0.0;
					} else if (loginDTO.getDivisionSelected().equals("AR")) {
						totalValue = myshipmentTrackHeaderBean.getCrg_wt() != null
								? myshipmentTrackHeaderBean.getCrg_wt().doubleValue()
								: 0.0;
					}
				}

				totalGWT.setTotalValue(totalValue);

				double totalValuePercentage = 0.0;

				if (details.equals("totalGWT")) {
					totalValuePercentage = (totalGWT.getTotalValue() / dashboardInfoBox.getTotalGWT()) * 100;
				} else if (details.equals("totalCBM")) {
					if (loginDTO.getDivisionSelected().equals("SE")) {
						totalValuePercentage = (totalGWT.getTotalValue() / dashboardInfoBox.getTotalCBM()) * 100;
					} else if (loginDTO.getDivisionSelected().equals("AR")) {
						totalValuePercentage = (totalGWT.getTotalValue() / dashboardInfoBox.getTotalCRGWT()) * 100;
					}
				}

				totalGWT.setValuePercentage(totalValuePercentage);

				totalShipmentCountMap.put(compName, totalGWT);
			}
			// buyerSet.add(buyer);
		}
		List<TotalGWT> totalGWTList = new ArrayList<TotalGWT>(totalShipmentCountMap.values());

		return totalGWTList;
	}

	public List<TotalShipment> getDashboardShipmentList(List<MyshipmentTrackHeaderBean> shipmentList,
			LoginDTO loginDTO) {
		Map<String, TotalShipment> shipmentCountMap = new HashMap<String, TotalShipment>();
		int shipmentCount = shipmentList.size();

		for (MyshipmentTrackHeaderBean myshipmentTrackHeaderBean : shipmentList) {
			String compName = null;
			String id = null;
			if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE1)
					|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE2)) {
				compName = myshipmentTrackHeaderBean.getBuyer();
				id = myshipmentTrackHeaderBean.getBuyer_no();
			} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE1)
					|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE2)) {
				compName = myshipmentTrackHeaderBean.getShipper();
				id = myshipmentTrackHeaderBean.getShipper_no();
			} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.DA_CODE1)
					|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.DA_CODE2)) {
				compName = myshipmentTrackHeaderBean.getShipper();
				id = myshipmentTrackHeaderBean.getShipper_no();
			}

			if (shipmentCountMap.containsKey(compName)) {
				TotalShipment shipment = shipmentCountMap.get(compName);
				shipment.setShipment(shipment.getShipment() + 1);
				double percentage = (shipment.getShipment() / shipmentCount) * 100;
				shipment.setShipmentPercentage(percentage);
				shipmentCountMap.put(compName, shipment);
			} else {
				TotalShipment shipment = new TotalShipment();
				shipment.setName(compName);
				shipment.setId(id);
				shipment.setShipment(1);
				double percentage = (shipment.getShipment() / shipmentCount) * 100;
				shipment.setShipmentPercentage(percentage);
				shipmentCountMap.put(compName, shipment);
			}
			// buyerSet.add(buyer);
		}
		List<TotalShipment> totalShipment = new ArrayList<TotalShipment>(shipmentCountMap.values());

		return totalShipment;
	}

	@RequestMapping(value = "/shipmentChartDetails", method = RequestMethod.GET)
	public String getShipmentChartDetails(@RequestParam("id") String id, @RequestParam("details") String details,
			HttpServletRequest servletRequest, HttpServletResponse servletResponse, Model model) {
		logger.debug(this.getClass().getName() + "getShipmentChartDetails():  Start ");

		HttpSession session = SessionUtil.getHttpSession(servletRequest);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		DashboardInfoBox dashboardInfoBox = (DashboardInfoBox) session.getAttribute(SessionUtil.DASHBOARD_INFO_DETAILS);

		List<MyshipmentTrackHeaderBean> list = null;

		if (details.equals("totalBooking")) {
			list = dashboardInfoBox.getTotalShipmentList();
			model.addAttribute("requestStatus", "Total Booking");
		} else if (details.equals("openBooking")) {
			list = dashboardInfoBox.getOpenOrderList();
			model.addAttribute("requestStatus", "Open Booking");
		} else if (details.equals("goodsReceived")) {
			list = dashboardInfoBox.getGoodsReceivedList();
			model.addAttribute("requestStatus", "Goods Handover");
		} else if (details.equals("stuffingDone")) {
			list = dashboardInfoBox.getStuffingDoneList();
			if (loginDTO.getDivisionSelected().equals("SE")) {
				model.addAttribute("requestStatus", "Stuffing Done");
			} else if (loginDTO.getDivisionSelected().equals("AR")) {
				model.addAttribute("requestStatus", "MAWB Issued");
			}
		} else if (details.equals("inTransit")) {
			list = dashboardInfoBox.getInTransitList();
			model.addAttribute("requestStatus", "In Transit");
		} else if (details.equals("arrived")) {
			list = dashboardInfoBox.getDeliveredList();
			model.addAttribute("requestStatus", "Arrived");
		} else if (details.equals("totalGWT") || details.equals("totalCBM")) {
			list = dashboardInfoBox.getTotalShipmentList();
			if (details.equals("totalGWT")) {
				model.addAttribute("requestStatus", "Gross Weight");
			} else if (details.equals("totalCBM")) {
				if (loginDTO.getDivisionSelected().equals("SE")) {
					model.addAttribute("requestStatus", "CBM");
				} else if (loginDTO.getDivisionSelected().equals("AR")) {
					model.addAttribute("requestStatus", "Charge Weight");
				}

			}
		}

		List<MyshipmentTrackHeaderBean> outputList = new ArrayList<MyshipmentTrackHeaderBean>();

		String name = "";
		for (MyshipmentTrackHeaderBean myshipmentTrackHeaderBean : list) {

			if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE1)
					|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE2)) {
				if (myshipmentTrackHeaderBean.getBuyer_no().equals(id)) {
					outputList.add(myshipmentTrackHeaderBean);
					if (name.equals("") && (myshipmentTrackHeaderBean.getBuyer() != null
							|| !myshipmentTrackHeaderBean.getBuyer().equals(""))) {
						name = myshipmentTrackHeaderBean.getBuyer();
					}
				}
			} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE1)
					|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE2)) {
				if (myshipmentTrackHeaderBean.getShipper_no().equals(id)) {
					outputList.add(myshipmentTrackHeaderBean);
					if (name.equals("") && (myshipmentTrackHeaderBean.getShipper() != null
							|| !myshipmentTrackHeaderBean.getShipper().equals(""))) {
						name = myshipmentTrackHeaderBean.getShipper();
					}
				}
			} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.DA_CODE1)
					|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.DA_CODE2)) {
				if (myshipmentTrackHeaderBean.getShipper_no().equals(id)) {
					outputList.add(myshipmentTrackHeaderBean);
					if (name.equals("") && (myshipmentTrackHeaderBean.getShipper() != null
							|| !myshipmentTrackHeaderBean.getShipper().equals(""))) {
						name = myshipmentTrackHeaderBean.getShipper();
					}
				}
			}
		}

		List<ShipmentStatusDetails> shipmentChartDetailsList = getStatusWiseShipmentList(outputList, loginDTO);

		if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE1)
				|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE2)) {
			model.addAttribute("nameBuyerSupplier", "Buyer Name");
			model.addAttribute("nameAgentSupplier", "Destination Agent");
		} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE1)
				|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE2)) {
			model.addAttribute("nameBuyerSupplier", "Supplier Name");
			model.addAttribute("nameAgentSupplier", "Destination Agent");
		} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.DA_CODE1)
				|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.DA_CODE2)) {
			model.addAttribute("nameBuyerSupplier", "Supplier Name");
			model.addAttribute("nameAgentSupplier", "Buyer Name");
		}

		if (loginDTO.getDivisionSelected().equals("SE")) {
			model.addAttribute("weightHeader", "Gross Weight");
			model.addAttribute("shippingLineHeader", "Shipping Line");
		} else if (loginDTO.getDivisionSelected().equals("AR")) {
			model.addAttribute("weightHeader", "Charge Weight");
			model.addAttribute("shippingLineHeader", "Airline");
		}

		model.addAttribute("name", name);
		model.addAttribute("status", details);
		model.addAttribute("shipmentChartDetailsList", shipmentChartDetailsList);
		model.addAttribute("shipmentList", outputList);
		return "shipmentChartDetails";

	}

	@RequestMapping(value = "/buyerDashboard", method = RequestMethod.GET)
	public String getDashboardDate(HttpServletRequest request, HttpServletResponse response, Model model) {
		logger.debug(this.getClass().getName() + "getDashboard():  Start ");

		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDto = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		String fromDate = "";
		String toDate = "";

		if ((loginDto.getDashBoardFromDate() != null || loginDto.getDashBoardFromDate().equalsIgnoreCase(""))
				&& (loginDto.getDashBoardToDate() != null || loginDto.getDashBoardToDate().equalsIgnoreCase(""))) {
			fromDate = loginDto.getDashBoardFromDate();
			toDate = loginDto.getDashBoardToDate();
		} else {
			Date tempDate = new Date();
			tempDate.setMonth(tempDate.getMonth() - 1);
			fromDate = DateUtil.formatDateToString(tempDate);
			toDate = DateUtil.formatDateToString(new Date());
		}

		// supplier wise shipper summary service call
		logger.info(this.getClass().getName() + "Calling Supplier Wise Shipment Summary for Buyer...");

		SuppWiseShipSummaryJson suppWiseShipSummaryJson = null;
		try {
			suppWiseShipSummaryJson = dashboardService.suppWiseShipSummaryForBuyerService(fromDate, toDate, loginDto);
		} catch (MyShipApplicationException e) {
			handleMyShipApplicationException(e, response);
		}
		logger.debug(this.getClass().getName() + "Supplier Wise Shipment Summary for Buyer Call Ends");

		// top five shipment service call
		logger.info(this.getClass().getName() + "Calling Top Five Shipment service...");

		TopFiveShipBuyerJson topFiveShipment = null;
		try {
			topFiveShipment = dashboardService.topFiveShipmentServiceBuyer(fromDate, toDate, loginDto);
			Collections.sort(topFiveShipment.getLstTopFiveShipmentBuyer(), new Comparator<TopFiveShipmentBuyer>() {
				public int compare(TopFiveShipmentBuyer o1, TopFiveShipmentBuyer o2) {
					if (o1.getTotalShipment() == o2.getTotalShipment())
						return 0;
					return o1.getTotalShipment() > o2.getTotalShipment() ? -1 : 1;
				}

			});
		} catch (MyShipApplicationException e) {
			handleMyShipApplicationException(e, response);
		}
		logger.debug(this.getClass().getName() + "Top Five Shipment service Call Ends");

		// recent shipment
		int noOfRecords = 5;
		LastNShipmentsJsonOutputData lastNShipmentsJsonOutputData = null;
		try {
			lastNShipmentsJsonOutputData = dashboardService.getRecentShipmentShipper(loginDto, noOfRecords);
		} catch (Exception e) {
			logger.error("Error occured while fetching Recent Shipment Shipper Details : " + e);
		}

		return null;
	}

	@RequestMapping(value = "/sowiseshipsummaryshipper", method = RequestMethod.GET)
	@ResponseBody
	public SOWiseShipmentSummaryJson soWiseShipmentSummary(@RequestParam("toDate") String todate,
			@RequestParam("fromDate") String fromDate, HttpServletRequest request, HttpServletResponse response) {
		logger.debug(this.getClass().getName() + "soWiseShipmentSummary():  Start ");

		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDto = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		loginDto.setDashBoardFromDate(fromDate);
		loginDto.setDashBoardToDate(todate);
		session.setAttribute(SessionUtil.LOGIN_DETAILS, loginDto);
		SOWiseShipmentSummaryJson sowiseShipmentSummary = null;
		try {
			sowiseShipmentSummary = dashboardService.soWiseShipSummaryForShipperService(fromDate, todate, loginDto);
		} catch (MyShipApplicationException e) {
			handleMyShipApplicationException(e, response);
		}
		logger.debug(this.getClass().getName() + "performLogin():  After call  with Parameter:" + loginDto);
		return sowiseShipmentSummary;
	}

	@RequestMapping(value = "/top5shipmentshipper", method = RequestMethod.GET)
	@ResponseBody
	public TopFiveShipment topFiveShipment(@RequestParam("toDate") String todate,
			@RequestParam("fromDate") String fromDate, HttpServletRequest request, HttpServletResponse response) {
		logger.debug(this.getClass().getName() + "soWiseShipmentSummary():  Start ");

		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDto = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		loginDto.setDashBoardFromDate(fromDate);
		loginDto.setDashBoardToDate(todate);
		session.setAttribute(SessionUtil.LOGIN_DETAILS, loginDto);
		TopFiveShipment topFiveShipment = null;
		try {
			topFiveShipment = dashboardService.topFiveShipmentService(fromDate, todate, loginDto);
		} catch (MyShipApplicationException e) {
			handleMyShipApplicationException(e, response);
		}
		logger.debug(this.getClass().getName() + "performLogin():  After call  with Parameter:" + loginDto);
		return topFiveShipment;
	}

	@RequestMapping(value = "/blstatusbuyer", method = RequestMethod.GET)

	public String blStatusDetails(@RequestParam("fromDate") String fromDate, @RequestParam("toDate") String toDate,
			@RequestParam("blStatus") String blStatus, HttpServletRequest servletRequest,
			HttpServletResponse servletResponse, Model model) {
		logger.debug(this.getClass().getName() + "blStatusDetails():  Start ");

		HttpSession session = SessionUtil.getHttpSession(servletRequest);
		LoginDTO loginDto = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		SuppwiseShipDetailsbyBLstatus suppwiseShipDetailsbyBLstatus = null;
		try {
			suppwiseShipDetailsbyBLstatus = dashboardService.blStatusBuyer(fromDate, toDate, blStatus, loginDto);
		} catch (MyShipApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.addAttribute("shipdetails", suppwiseShipDetailsbyBLstatus);
		return "shipementdetailbuyer";

	}

	@RequestMapping(value = "/blstatusshipper", method = RequestMethod.GET)

	public String blStatusDetailsShipper(@RequestParam("fromDate") String fromDate,
			@RequestParam("toDate") String toDate, @RequestParam("blStatus") String blStatus,
			HttpServletRequest servletRequest, HttpServletResponse servletResponse, Model model) {
		logger.debug(this.getClass().getName() + "blStatusDetails():  Start ");

		HttpSession session = SessionUtil.getHttpSession(servletRequest);
		LoginDTO loginDto = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		BuywiseShipDetailsbyBLstatus buywiseShipDetailsbyBLstatus = null;
		try {
			buywiseShipDetailsbyBLstatus = dashboardService.blStatusShipper(fromDate, toDate, blStatus, loginDto);
		} catch (MyShipApplicationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.addAttribute("shipdetails", buywiseShipDetailsbyBLstatus);
		return "shipmentdetailshipper";

	}

	@RequestMapping(value = "/shipdetshipper", method = RequestMethod.GET)

	public String shipDetailsShipper(@RequestParam("buyerNo") String buyerNo, HttpServletRequest servletRequest,
			HttpServletResponse servletResponse, Model model) {
		logger.debug(this.getClass().getName() + "shipDetailsShipper():  Start ");

		HttpSession session = SessionUtil.getHttpSession(servletRequest);
		LoginDTO loginDto = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		ShipmentDetailsShipperJson shipmentDetailsShipperJson = null;
		shipmentDetailsShipperJson = dashboardService.shipDetailsShipperService(buyerNo, loginDto);
		model.addAttribute("shipdetails", shipmentDetailsShipperJson);
		return "shipmentdetailshipper";

	}

	@RequestMapping(value = "/suppwiseshipsummarybuyer", method = RequestMethod.GET)
	@ResponseBody
	public SuppWiseShipSummaryJson supplWiseShipmentSummary(@RequestParam("toDate") String todate,
			@RequestParam("fromDate") String fromDate, HttpServletRequest request, HttpServletResponse response) {
		logger.debug(this.getClass().getName() + "soWiseShipmentSummary():  Start ");

		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDto = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		loginDto.setDashBoardFromDate(fromDate);
		loginDto.setDashBoardToDate(todate);
		session.setAttribute(SessionUtil.LOGIN_DETAILS, loginDto);
		SuppWiseShipSummaryJson suppWiseShipSummaryJson = null;
		try {
			suppWiseShipSummaryJson = dashboardService.suppWiseShipSummaryForBuyerService(fromDate, todate, loginDto);
		} catch (MyShipApplicationException e) {
			handleMyShipApplicationException(e, response);
		}
		logger.debug(this.getClass().getName() + "performLogin():  After call  with Parameter:" + loginDto);
		return suppWiseShipSummaryJson;
	}

	@RequestMapping(value = "/top5shipmentbuyer", method = RequestMethod.GET)
	@ResponseBody
	public TopFiveShipBuyerJson topFiveShipmentBuyer(@RequestParam("toDate") String todate,
			@RequestParam("fromDate") String fromDate, HttpServletRequest request, HttpServletResponse response) {
		logger.debug(this.getClass().getName() + "soWiseShipmentSummary():  Start ");

		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDto = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		loginDto.setDashBoardFromDate(fromDate);
		loginDto.setDashBoardToDate(todate);
		session.setAttribute(SessionUtil.LOGIN_DETAILS, loginDto);
		TopFiveShipBuyerJson topFiveShipment = null;
		try {
			topFiveShipment = dashboardService.topFiveShipmentServiceBuyer(fromDate, todate, loginDto);
			Collections.sort(topFiveShipment.getLstTopFiveShipmentBuyer(), new Comparator<TopFiveShipmentBuyer>() {
				public int compare(TopFiveShipmentBuyer o1, TopFiveShipmentBuyer o2) {
					if (o1.getTotalShipment() == o2.getTotalShipment())
						return 0;
					return o1.getTotalShipment() > o2.getTotalShipment() ? -1 : 1;
				}

			});

		} catch (MyShipApplicationException e) {
			handleMyShipApplicationException(e, response);
		}
		logger.debug(this.getClass().getName() + "performLogin():  After call  with Parameter:" + loginDto);
		return topFiveShipment;
	}

	@RequestMapping(value = "/buyerwiseshipdetails", method = RequestMethod.GET)
	public String totalShipmentDetShipper(@RequestParam("toDate") String toDate,
			@RequestParam("fromDate") String fromDate, HttpServletRequest request, HttpServletResponse response,
			Model model) {
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		BuyerWiseShipDetails buyerWiseShipDetails = dashboardService.totalShipDetShipperService(fromDate, toDate,
				loginDTO);
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonString = null;
		try {
			jsonString = objectMapper.writeValueAsString(buyerWiseShipDetails);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.addAttribute("shipdetails", jsonString);
		return "buyerwiseshipment";
	}

	@RequestMapping(value = "/buyerwisecbmdetails", method = RequestMethod.GET)
	public String totalCBMDetShipper(@RequestParam("toDate") String toDate, @RequestParam("fromDate") String fromDate,
			HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		BuyerWiseCBMdetails buyerWiseShipDetails = dashboardService.totCbmDetailsShipperService(fromDate, toDate,
				loginDTO);

		Collections.sort(buyerWiseShipDetails.getLstBuyerWiseCBMJson(), new Comparator<BuyerWiseCBMJson>() {
			public int compare(BuyerWiseCBMJson o1, BuyerWiseCBMJson o2) {
				if (o1.getTotalCBM() == o2.getTotalCBM())
					return 0;
				return o1.getTotalCBM() > o2.getTotalCBM() ? -1 : 1;
			}

		});
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonString = null;
		try {
			jsonString = objectMapper.writeValueAsString(buyerWiseShipDetails);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.addAttribute("shipdetails", jsonString);
		return "buyerwisecbm";
	}

	@RequestMapping(value = "/buyerwisegwtdetails", method = RequestMethod.GET)
	public String totalGWTDetShipper(@RequestParam("toDate") String toDate, @RequestParam("fromDate") String fromDate,
			HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		BuyerWiseGWTdetails buyerWiseShipDetails = dashboardService.totGwtDetailsShipperService(fromDate, toDate,
				loginDTO);

		Collections.sort(buyerWiseShipDetails.getLstBuyerWiseGWTJson(), new Comparator<BuyerWiseGWTJson>() {
			public int compare(BuyerWiseGWTJson o1, BuyerWiseGWTJson o2) {
				if (o1.getTotalGWT() == o2.getTotalGWT())
					return 0;
				return o1.getTotalGWT() > o2.getTotalGWT() ? -1 : 1;
			}

		});

		ObjectMapper objectMapper = new ObjectMapper();
		String jsonString = null;
		try {
			jsonString = objectMapper.writeValueAsString(buyerWiseShipDetails);
		} catch (JsonProcessingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		model.addAttribute("shipdetails", jsonString);
		return "buyerwisegwt";
	}

	@RequestMapping(value = "/suppwiseshipdetails", method = RequestMethod.GET)
	public String totalShipmentDetBuyer(@RequestParam("toDate") String toDate,
			@RequestParam("fromDate") String fromDate, HttpServletRequest request, HttpServletResponse response,
			Model model) {
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		SupplierWiseShipmentDetailJson buyerWiseShipDetails = dashboardService.totalShipmentDetBuyerService(fromDate,
				toDate, loginDTO);

		Collections.sort(buyerWiseShipDetails.getLstSupplierWiseShipmentJson(),
				new Comparator<SupplierWiseShipmentJson>() {
					public int compare(SupplierWiseShipmentJson o1, SupplierWiseShipmentJson o2) {
						if (o1.getTotalShipment() == o2.getTotalShipment())
							return 0;
						return o1.getTotalShipment() > o2.getTotalShipment() ? -1 : 1;
					}

				});
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonString = null;
		try {
			jsonString = objectMapper.writeValueAsString(buyerWiseShipDetails);
		} catch (JsonProcessingException e) {

			e.printStackTrace();
		}
		model.addAttribute("shipdetails", jsonString);
		return "supplierwiseshipment";
	}

	@RequestMapping(value = "/suppwisecbmdetails", method = RequestMethod.GET)
	public String totalCBMDetBuyer(@RequestParam("toDate") String toDate, @RequestParam("fromDate") String fromDate,
			HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		SupplierWiseCBMDetailJson buyerWiseShipDetails = dashboardService.totalCBMDetBuyerService(fromDate, toDate,
				loginDTO);

		Collections.sort(buyerWiseShipDetails.getLstSupplierWiseCBMJson(), new Comparator<SupplierWiseCBMJson>() {
			public int compare(SupplierWiseCBMJson o1, SupplierWiseCBMJson o2) {
				if (o1.getTotalCBM() == o2.getTotalCBM())
					return 0;
				return o1.getTotalCBM() > o2.getTotalCBM() ? -1 : 1;
			}

		});
		ObjectMapper objectMapper = new ObjectMapper();
		String jsonString = null;
		try {
			jsonString = objectMapper.writeValueAsString(buyerWiseShipDetails);
		} catch (JsonProcessingException e) {

			e.printStackTrace();
		}
		model.addAttribute("shipdetails", jsonString);
		return "supplierwisecbm";
	}

	@RequestMapping(value = "/suppwisegwtdetails", method = RequestMethod.GET)
	public String totalGWTDetBuyer(@RequestParam("toDate") String toDate, @RequestParam("fromDate") String fromDate,
			HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		SupplierWiseGWTDetailJson buyerWiseShipDetails = dashboardService.totalGWTDetBuyerService(fromDate, toDate,
				loginDTO);

		Collections.sort(buyerWiseShipDetails.getLstSupplierWiseGWTJson(), new Comparator<SupplierWiseGWTJson>() {
			public int compare(SupplierWiseGWTJson o1, SupplierWiseGWTJson o2) {
				if (o1.getTotalGWT() == o2.getTotalGWT())
					return 0;
				return o1.getTotalGWT() > o2.getTotalGWT() ? -1 : 1;
			}

		});

		ObjectMapper objectMapper = new ObjectMapper();
		String jsonString = null;
		try {
			jsonString = objectMapper.writeValueAsString(buyerWiseShipDetails);
		} catch (JsonProcessingException e) {

			e.printStackTrace();
		}
		model.addAttribute("shipdetails", jsonString);
		return "supplierwisegwt";
	}

	@RequestMapping(value = "/sowiseshipdetailsshipper", method = RequestMethod.GET)
	public String sowiseShipDetailsShipper(@RequestParam("toDate") String toDate,
			@RequestParam("fromDate") String fromDate, @RequestParam("salesOrg") String salesOrg,
			HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		SowiseShipDetailsShipper SowiseShipDetailsShipper = dashboardService.sowiseShipDetailsShipperService(fromDate,
				toDate, salesOrg, loginDTO);
		model.addAttribute("shipdetails", SowiseShipDetailsShipper);
		return "shipmentdetailshipper";

	}

	@RequestMapping(value = "/buyerwiseshipdetailsbypod", method = RequestMethod.GET)
	public String buyerWiseShipDetailsByPod(@RequestParam("toDate") String toDate,
			@RequestParam("fromDate") String fromDate, @RequestParam("pod") String pod, HttpServletRequest request,
			HttpServletResponse response, Model model) {
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		BuyerWiseShipDetailsbyPOD buyerWiseShipDetailsbyPOD = dashboardService
				.buyerWiseShipDetailsByPodService(fromDate, toDate, pod, loginDTO);
		model.addAttribute("shipdetails", buyerWiseShipDetailsbyPOD);
		return "shipmentdetailshipper";

	}

	@RequestMapping(value = "/sowiseshipdetailsbuyer", method = RequestMethod.GET)
	public String soWiseShipDetailsBuyer(@RequestParam("toDate") String toDate,
			@RequestParam("fromDate") String fromDate, @RequestParam("shipperNo") String shipperNo,
			HttpServletRequest request, HttpServletResponse response, Model model) {
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		SowiseShipDetailsBuyer sowiseShipDetailsBuyer = dashboardService.soWiseShipDetailsBuyerService(fromDate, toDate,
				shipperNo, loginDTO);
		model.addAttribute("shipdetails", sowiseShipDetailsBuyer);
		return "shipementdetailbuyer";
	}

	@RequestMapping(value = "/suppwiseshipdetailsbypoo", method = RequestMethod.GET)
	public String suppWiseShipDetailsByPOO(@RequestParam("toDate") String toDate,
			@RequestParam("fromDate") String fromDate, @RequestParam("poo") String poo, HttpServletRequest request,
			HttpServletResponse response, Model model) {
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		SuppwiseShipDetailsbyBLstatus suppwiseShipDetailsbyBLstatus = dashboardService
				.suppWiseShipDetailsByPOOService(fromDate, toDate, poo, loginDTO);
		model.addAttribute("shipdetails", suppwiseShipDetailsbyBLstatus);
		return "shipementdetailbuyer";
	}

	@RequestMapping(value = "/shipdetbuyer", method = RequestMethod.GET)

	public String shipDetailsBuyer(@RequestParam("shipperNo") String shipperNo, HttpServletRequest servletRequest,
			HttpServletResponse servletResponse, Model model) {
		logger.debug(this.getClass().getName() + "shipDetailsBuyer():  Start ");

		HttpSession session = SessionUtil.getHttpSession(servletRequest);
		LoginDTO loginDto = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		ShipmentDetailsBuyerJson shipmentDetailsBuyerJson = null;
		shipmentDetailsBuyerJson = dashboardService.shipDetailsBuyerService(shipperNo, loginDto);
		model.addAttribute("shipdetails", shipmentDetailsBuyerJson);
		return "shipementdetailbuyer";

	}

	@RequestMapping(value = "/recentshipment", method = RequestMethod.GET)
	public @ResponseBody LastNShipmentsJsonOutputData getRecentShipmentShipper(
			@RequestParam("NoOfRecords") int noOfRecords, HttpServletRequest httpServletRequest) {
		HttpSession session = SessionUtil.getHttpSession(httpServletRequest);

		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		LastNShipmentsJsonOutputData lastNShipmentsJsonOutputData = dashboardService.getRecentShipmentShipper(loginDTO,
				noOfRecords);

		return lastNShipmentsJsonOutputData;

	}
}
