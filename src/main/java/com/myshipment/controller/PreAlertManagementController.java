package com.myshipment.controller;

import java.io.File;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.myshipment.dto.LoginDTO;
import com.myshipment.model.AirBillOfLandingJsonData;
import com.myshipment.model.BapiRet1;
import com.myshipment.model.DashboardInfoBox;
import com.myshipment.model.MyshipmentTrackHeaderBean;
import com.myshipment.model.NegoBillLanding;
import com.myshipment.model.NegoBillLandingI;
import com.myshipment.model.PreAlertBL;
import com.myshipment.model.PreAlertHBL;
import com.myshipment.model.PreAlertHBLDetails;
import com.myshipment.model.PreAlertSaveBLStatus;
import com.myshipment.model.RequestParams;
import com.myshipment.model.ShippingReport;
import com.myshipment.model.airBillOfLadingPDFGeneration;
import com.myshipment.model.seaBillOfLadingPdfGeneration;
import com.myshipment.service.IPreAlertManagementService;
import com.myshipment.util.CommonConstant;
import com.myshipment.util.DateUtil;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;
import com.myshipment.util.SessionUtil;
import com.myshipment.util.ZipFiles;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRLoader;

/**
*
*@author Ahmad Naquib
*/

@Controller
public class PreAlertManagementController {

	private Logger logger = Logger.getLogger(PreAlertManagementController.class);

	@Autowired
	private IPreAlertManagementService preAlertManagementService;
	@Autowired
	private RestService restService;

	@RequestMapping(value = "/preAlertManagement", method = RequestMethod.GET)
	public String getPreAlertManagement(@RequestParam("status") String status, Model model,
			HttpServletRequest request) {

		logger.info(this.getClass() + ": Pre Alert management panel...");

		HttpSession session = SessionUtil.getHttpSession(request);

		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		DashboardInfoBox dashboardInfoBox = (DashboardInfoBox) session.getAttribute(SessionUtil.DASHBOARD_INFO_DETAILS);

		List<MyshipmentTrackHeaderBean> dashboardSupplierWiseHBL = new ArrayList<MyshipmentTrackHeaderBean>();

		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getOpenOrderList());
		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getGoodsReceivedList());
		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getStuffingDoneList());
		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getInTransitList());

		List<PreAlertHBL> preAlertHblBySupplierList = new ArrayList<PreAlertHBL>();
		String myshipmentUid = "";

		try {
			logger.info(this.getClass() + ": Fetching pre-alert details HBL-wise from database...");
			if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.CRM_CODE1)
					|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.CRM_CODE2)) {
				if (loginDTO.getCrmDashboardBuyerId() != null) {
					myshipmentUid = loginDTO.getCrmDashboardBuyerId();
					PreAlertHBL preAlertHbl = new PreAlertHBL();
					preAlertHbl.setBuyerId(myshipmentUid);
					preAlertHbl.setSalesOrg(loginDTO.getSalesOrgSelected());
					preAlertHbl.setDistributionChannel(loginDTO.getDisChnlSelected());
					preAlertHbl.setDivision(loginDTO.getDivisionSelected());
					preAlertHblBySupplierList = preAlertManagementService.getPreAlertBuyerWiseList(preAlertHbl, "ALL");
				} else {
					myshipmentUid = loginDTO.getLoggedInUserName();
					preAlertHblBySupplierList = preAlertManagementService.getPreAlertByHBL(myshipmentUid);
				}
			} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE1)
					|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE2)) {
				// Map<String, List<PreAlertHBLDetails>> preAlertDetailsMap =
				// getDashboardPreAlertList(dashboardInfoBox, loginDTO);
				preAlertHblBySupplierList = getDashboardPreAlertList(dashboardInfoBox, loginDTO);
				model.addAttribute("preAlertDetailsPending", preAlertHblBySupplierList);
			}

			logger.info(this.getClass() + ": Successfull database transaction for pre-alert details HBL-wise...");
		} catch (Exception e) {
			logger.info(this.getClass() + ": Error occured while fetching pre-alert details HBL-wise from database...");
			e.printStackTrace();
		}

		List<PreAlertHBLDetails> preAlertHblDetailsBySupplierList = new ArrayList<PreAlertHBLDetails>();

		if (status.equals("all")) {
			for (MyshipmentTrackHeaderBean myshipmentTrackHeaderBean : dashboardSupplierWiseHBL) {
				for (PreAlertHBL preAlertHblBySupplier : preAlertHblBySupplierList) {
					if (myshipmentTrackHeaderBean.getBl_no().equals(preAlertHblBySupplier.getHblNumber())) {
						PreAlertHBLDetails preAlertHBLDetails = new PreAlertHBLDetails();

						preAlertHBLDetails.setTrackHeaderBean(myshipmentTrackHeaderBean);
						preAlertHBLDetails.setPreAlertHbl(preAlertHblBySupplier);

						preAlertHblDetailsBySupplierList.add(preAlertHBLDetails);
					}
				}
			}
		} else if (status.equals("pending") || status.equals("completed")) {
			String statusCode = status.equals("pending") ? "0" : "1";
			for (MyshipmentTrackHeaderBean myshipmentTrackHeaderBean : dashboardSupplierWiseHBL) {
				for (PreAlertHBL preAlertHblBySupplier : preAlertHblBySupplierList) {
					if (myshipmentTrackHeaderBean.getBl_no().equals(preAlertHblBySupplier.getHblNumber())) {
						if (preAlertHblBySupplier.getIsPreAlertDoc().equals(statusCode)) {
							PreAlertHBLDetails preAlertHBLDetails = new PreAlertHBLDetails();

							preAlertHBLDetails.setTrackHeaderBean(myshipmentTrackHeaderBean);
							preAlertHBLDetails.setPreAlertHbl(preAlertHblBySupplier);

							preAlertHblDetailsBySupplierList.add(preAlertHBLDetails);
						}
					}
				}
			}
		}

		model.addAttribute("preAlertHblDetailsBySupplierList", preAlertHblDetailsBySupplierList);
		model.addAttribute("preAlertHblDetailsBySupplierListJSON", new Gson().toJson(preAlertHblDetailsBySupplierList));

		model.addAttribute("dashboardSupplierWiseHBL", dashboardSupplierWiseHBL);
		model.addAttribute("openOrderList", dashboardInfoBox.getOpenOrderList());
		model.addAttribute("goodsReceivedList", dashboardInfoBox.getGoodsReceivedList());
		model.addAttribute("stuffingDoneList", dashboardInfoBox.getStuffingDoneList());
		model.addAttribute("inTransitList", dashboardInfoBox.getInTransitList());

		return "preAlertManagement";
	}

	@RequestMapping(value = "/getPreAlertDetailsHBLwise", method = RequestMethod.POST)
	public ResponseEntity<?> searchPreAlertDocumentHBL(@RequestParam("blNumber") String hblNumber,
			HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = SessionUtil.getHttpSession(request);

		ResponseEntity<?> responseEntity = null;

		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		DashboardInfoBox dashboardInfoBox = (DashboardInfoBox) session.getAttribute(SessionUtil.DASHBOARD_INFO_DETAILS);

		List<MyshipmentTrackHeaderBean> dashboardSupplierWiseHBL = new ArrayList<MyshipmentTrackHeaderBean>();

		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getOpenOrderList());
		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getGoodsReceivedList());
		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getStuffingDoneList());
		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getInTransitList());

		PreAlertHBL preAlertHblSearchParameter = new PreAlertHBL();
		preAlertHblSearchParameter.setHblNumber(hblNumber);
		preAlertHblSearchParameter.setSalesOrg(loginDTO.getSalesOrgSelected());
		preAlertHblSearchParameter.setDistributionChannel(loginDTO.getDisChnlSelected());
		preAlertHblSearchParameter.setDivision(loginDTO.getDivisionSelected());

		String myshipmentUid = "";

		if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.CRM_CODE1)
				|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.CRM_CODE2)) {
			myshipmentUid = loginDTO.getCrmDashboardBuyerId();
		} else {
			myshipmentUid = loginDTO.getLoggedInUserName();
		}

		PreAlertHBL preAlertHblBySupplierAndHBL = new PreAlertHBL();

		try {
			preAlertHblBySupplierAndHBL = preAlertManagementService.getPreAlertBySingleHBL(preAlertHblSearchParameter,
					myshipmentUid);
		} catch (Exception e) {
			e.printStackTrace();
			responseEntity = new ResponseEntity("Error Occured while searching HBL/HAWB. Please try again",
					new HttpHeaders(), HttpStatus.BAD_REQUEST);
			return responseEntity;
		}

		if (preAlertHblBySupplierAndHBL == null) {
			responseEntity = new ResponseEntity("HBL/HAWB Not Found.", new HttpHeaders(), HttpStatus.BAD_REQUEST);
			return responseEntity;
		}

		List<PreAlertHBLDetails> preAlertHblDetailsBySupplierAndHBL = new ArrayList<PreAlertHBLDetails>();

		for (MyshipmentTrackHeaderBean myshipmentTrackHeaderBean : dashboardSupplierWiseHBL) {
			if (myshipmentTrackHeaderBean.getBl_no().equals(preAlertHblBySupplierAndHBL.getHblNumber())) {
				PreAlertHBLDetails preAlertHBLDetails = new PreAlertHBLDetails();

				preAlertHBLDetails.setTrackHeaderBean(myshipmentTrackHeaderBean);
				preAlertHBLDetails.setPreAlertHbl(preAlertHblBySupplierAndHBL);

				preAlertHblDetailsBySupplierAndHBL.add(preAlertHBLDetails);
			}
		}

		responseEntity = new ResponseEntity<List<PreAlertHBLDetails>>(preAlertHblDetailsBySupplierAndHBL,
				new HttpHeaders(), HttpStatus.OK);

		return responseEntity;
	}

	// uploading pre alert documents
	// used to upload both invoice packing list and MBL/MAWB
	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/uploadPreAlertDocs", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> upload(MultipartHttpServletRequest request, HttpServletResponse response) {

		HttpSession session = SessionUtil.getHttpSession(request);
		
		String fileSeparator=System.getProperty("file.separator");

		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		DashboardInfoBox dashboardInfoBox = (DashboardInfoBox) session.getAttribute(SessionUtil.DASHBOARD_INFO_DETAILS);

		ResponseEntity<?> responseEntity = null;

		Iterator<String> itr = request.getFileNames();

		HttpHeaders header = request.getRequestHeaders();

		// bl-number and file-type sent from client side request
		ArrayList blNumber = (ArrayList) header.get("bl-number");
		ArrayList fileType = (ArrayList) header.get("file-type");
		ArrayList agentId = (ArrayList) header.get("agent-id");

		String blNumberString = blNumber.get(0).toString();
		//hamid
		String agentIdString = agentId.get(0).toString();

		// current myshipment server path where ipl and mawb are uploaded
		// ex: pre-alerts/inv-pack-lists/MLL000001/MLL000001-IPL.pdf
		/*String UPLOADED_FOLDER = request.getServletContext()
				.getRealPath(CommonConstant.FILE_SEPERATOR + "WEB-INF" + CommonConstant.FILE_SEPERATOR + "classes"
						+ CommonConstant.FILE_SEPERATOR + "pre-alerts" + CommonConstant.FILE_SEPERATOR);*/
		String UPLOADED_FOLDER = request.getServletContext()
				.getRealPath(CommonConstant.FILE_SEPERATOR + "WEB-INF" + CommonConstant.FILE_SEPERATOR + "classes"
						+ CommonConstant.FILE_SEPERATOR + "pre-alerts");

		if (blNumber.get(0) == null || blNumber.get(0).equals("")) {
			responseEntity = new ResponseEntity("No HBL/HAWB Found", new HttpHeaders(), HttpStatus.BAD_REQUEST);
			return responseEntity;
		}

		String fileTypeString = fileType.get(0).toString();

		// String location = UPLOADED_FOLDER + "\\" + blNumber.get(0) + "\\";
		String location = "";
		String fileName = "";
		if (fileTypeString.equals("IPL")) {
			fileName = blNumber.get(0) + "-IPL";
			location = UPLOADED_FOLDER + fileSeparator + "inv-pack-lists" + fileSeparator + blNumber.get(0) + fileSeparator;
		} else if (fileTypeString.equals("MST")) {
			fileName = blNumber.get(0) + "-MST";
			location = UPLOADED_FOLDER + fileSeparator + "mawbs" + fileSeparator + blNumber.get(0) + fileSeparator;
		} else {
			responseEntity = new ResponseEntity("File Type Not Found. Please try again", new HttpHeaders(),
					HttpStatus.BAD_REQUEST);
			return responseEntity;
		}

		// search-parameters
		PreAlertHBL preAlertHblSearchParameter = new PreAlertHBL();

		if (fileTypeString.equals("IPL")) {
			preAlertHblSearchParameter.setHblNumber(blNumber.get(0).toString());
		} else if (fileTypeString.equals("MST")) {
			preAlertHblSearchParameter.setMblNumber(blNumber.get(0).toString());
		}

		preAlertHblSearchParameter.setSalesOrg(loginDTO.getSalesOrgSelected());
		preAlertHblSearchParameter.setDistributionChannel(loginDTO.getDisChnlSelected());
		preAlertHblSearchParameter.setDivision(loginDTO.getDivisionSelected());

		// setting user id according to logged in state
		// in case of crm ->authentication will be verified with his/her assigned buyer
		if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.CRM_CODE1)
				|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.CRM_CODE2)) {
			preAlertHblSearchParameter.setCrmId(loginDTO.getLoggedInUserName());
			preAlertHblSearchParameter.setBuyerId(loginDTO.getCrmDashboardBuyerId());
			preAlertHblSearchParameter.setSupplierId("");
		} else if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE1)
				|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE2)) {
			preAlertHblSearchParameter.setSupplierId(loginDTO.getLoggedInUserName());
			preAlertHblSearchParameter.setBuyerId("");
			preAlertHblSearchParameter.setCrmId("");
		}
		PreAlertHBL preAlertHblResult = null;
		if (fileTypeString.equals("IPL")) {
			try {
				preAlertHblResult = preAlertManagementService.checkPreAlertAuth(preAlertHblSearchParameter);
				if (preAlertHblResult == null) {
					responseEntity = new ResponseEntity("You are not Authorized to update this HBL/HAWB",
							new HttpHeaders(), HttpStatus.BAD_REQUEST);
					return responseEntity;
				}
			} catch (Exception e1) {
				logger.error(this.getClass() + ": Error occured while authenticating user for pre-alert update");
				e1.printStackTrace();
				responseEntity = new ResponseEntity("Error Occured while Authenticating. Please Try Again.",
						new HttpHeaders(), HttpStatus.BAD_REQUEST);
				return responseEntity;
			}
		}

		// generating file location
		File directory = new File(location);
		if (!directory.exists()) {
			if (directory.mkdir()) {
				logger.info(this.getClass() + ": Directory is created!");
			} else {
				logger.error(this.getClass() + ": Failed to create directory!");
				responseEntity = new ResponseEntity("Error while creating directory", new HttpHeaders(),
						HttpStatus.BAD_REQUEST);
				return responseEntity;
			}
		}

		// uploading file in the allocated location
		MultipartFile mpf = request.getFile(itr.next());
		logger.info(this.getClass() + ": " + mpf.getOriginalFilename() + " uploaded!");

		String extension = FilenameUtils.getExtension(mpf.getOriginalFilename());
		String filePath = "";
		if (extension != null && !extension.equals("")) {
			filePath = location + fileName + "." + extension;
		} else {
			responseEntity = new ResponseEntity("Invalid File Extension", new HttpHeaders(), HttpStatus.BAD_REQUEST);
			return responseEntity;
		}

		try {

			byte[] bytes = mpf.getBytes();
			Path path = Paths.get(filePath);

			Files.write(path, bytes);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			responseEntity = new ResponseEntity("Error Occured while file uploading", new HttpHeaders(),
					HttpStatus.BAD_REQUEST);
			return responseEntity;
		}

		// updating file location in the database
		int updateFilesDatabaseEntryStatus = 0;
		try {
			if (fileType.get(0).toString().equals("IPL")) {
				preAlertHblSearchParameter.setFilePathPreAlertDoc(filePath);
				updateFilesDatabaseEntryStatus = preAlertManagementService
						.updatePreAlertDocument(preAlertHblSearchParameter);
			} else if (fileType.get(0).toString().equals("MST")) {

				List<MyshipmentTrackHeaderBean> trackHeaderBeanList = new ArrayList<MyshipmentTrackHeaderBean>();

				trackHeaderBeanList.addAll(dashboardInfoBox.getOpenOrderList());
				trackHeaderBeanList.addAll(dashboardInfoBox.getGoodsReceivedList());
				trackHeaderBeanList.addAll(dashboardInfoBox.getStuffingDoneList());
				trackHeaderBeanList.addAll(dashboardInfoBox.getInTransitList());
				trackHeaderBeanList.addAll(dashboardInfoBox.getDeliveredList());

				preAlertHblSearchParameter.setFileMBL(filePath);
				preAlertHblSearchParameter.setIsMBL("1");
				preAlertHblSearchParameter.setAgentId(agentIdString);
				updateFilesDatabaseEntryStatus = preAlertManagementService
						.updatePreAlertMBLDocument(preAlertHblSearchParameter, trackHeaderBeanList);
				
				try {
					//fetching all hawbs associated with one mawb 
					Set<String> hblNumberSet = new HashSet<String>();
					hblNumberSet = preAlertManagementService.getListOfHBLToGenerateNonNego(preAlertHblSearchParameter, trackHeaderBeanList);
					//now hblNumberSet has all hawb listed under that mawb
					for (String s : hblNumberSet) {
						RequestParams reqparam = new RequestParams();
						reqparam.setReq1(s.toUpperCase());
						reqparam.setReq2(loginDTO.getSalesOrgSelected());
						reqparam.setReq3(loginDTO.getDisChnlSelected());
						reqparam.setReq4(loginDTO.getDivisionSelected());
						reqparam.setReq5(loginDTO.getCrmDashboardBuyerId());
						
						if(reqparam.getReq4().equalsIgnoreCase("SE"))
							getSeaNonNegoHouseDetail(reqparam, session, request);
						else if(reqparam.getReq4().equalsIgnoreCase("AR")) 
							getAirNonNegoHouseDetail(reqparam, session, request);
						else 
							logger.debug("Division selected is not valid");						
					}					
				}
				catch(Exception ex) {
					ex.printStackTrace();
				}
			}
			if (updateFilesDatabaseEntryStatus == 0) {
				responseEntity = new ResponseEntity(
						"Error occured while storing file. Please check if booking is placed properly.",
						new HttpHeaders(), HttpStatus.BAD_REQUEST);
				return responseEntity;
			} else {

				PreAlertSaveBLStatus preAlertSaveBLStatus = (PreAlertSaveBLStatus) session
						.getAttribute("preAlertSaveBLStatus");
				if (preAlertSaveBLStatus != null) {
					if (preAlertSaveBLStatus.getHblNumber().equals(blNumberString)) {
						preAlertSaveBLStatus.setIsPreAlertDoc("1");
						session.setAttribute("preAlertSaveBLStatus", preAlertSaveBLStatus);
					}
				}
				if (fileTypeString.equals("IPL")) {
					PreAlertHBL preAlertHBL = new PreAlertHBL();
					preAlertHBL.setIsPreAlertDoc("1");
					preAlertHBL.setHblNumber(blNumberString);
					responseEntity = new ResponseEntity<PreAlertHBL>(preAlertHBL, new HttpHeaders(), HttpStatus.OK);
				} else if (fileTypeString.equals("MST")) {
					PreAlertHBL preAlertHBL = new PreAlertHBL();
					preAlertHBL.setIsPreAlertDoc("1");
					preAlertHBL.setMblNumber(blNumberString);
					responseEntity = new ResponseEntity<PreAlertHBL>(preAlertHBL, new HttpHeaders(), HttpStatus.OK);
				}

			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return responseEntity;

	}

	/*
	 * @RequestMapping(value = "/downloadMBL", method = RequestMethod.GET) public
	 * void downloadMBlBuyerAgetn(@RequestParam("mblNumber") String mblNumber,
	 * HttpServletRequest request, HttpServletResponse response) {
	 * 
	 * logger.debug(this.getClass().getName() + " Downloading Mbl: " + mblNumber);
	 * 
	 * HttpSession session = SessionUtil.getHttpSession(request);
	 * 
	 * LoginDTO loginDTO = (LoginDTO)
	 * session.getAttribute(SessionUtil.LOGIN_DETAILS);
	 * 
	 * String fileSeparator = System.getProperty("file.separator"); String
	 * dataDirectory = request.getServletContext().getRealPath(fileSeparator +
	 * "WEB-INF" + fileSeparator + "classes" + fileSeparator + "pre-alerts" +
	 * fileSeparator + "mawb" + fileSeparator); logger.debug("File path: " +
	 * dataDirectory);
	 * 
	 * String fileName = fileSeparator + mblNumber + fileSeparator + mblNumber +
	 * "-MST.pdf"; Path path = Paths.get(dataDirectory, fileName);
	 * 
	 * if (Files.exists(path)) { response.setContentType("application/pdf");
	 * 
	 * try { Files.copy(path, response.getOutputStream());
	 * response.getOutputStream().flush(); } catch (IOException ex) {
	 * ex.printStackTrace(); } } else {
	 * 
	 * } }
	 */

	@RequestMapping(value = "/downloadMBL", method = RequestMethod.GET)
	public void showPreAlertZipDownload(@RequestParam("mblNumber") String mblNumber, ModelMap model,
			HttpSession session, HttpServletRequest request, HttpServletResponse response,
			RedirectAttributes redirectAttributes) throws IOException {

		logger.info("Method TestController.showPreAlertZipDownload starts");
		String fileSeparator = System.getProperty("file.separator");
		ZipFiles zipFiles = new ZipFiles();
		String mawb = mblNumber;
		boolean success = false;
		// should come thru form submission
		// list hawbs under mawb

		List<String> listedHawbs = generatePreAlertHBLlist(mblNumber,request);
		for (String node : listedHawbs) {
			// we have primary folder directory
			String PRIMARY_FOLDER = request.getServletContext()
					.getRealPath(fileSeparator + "WEB-INF" + fileSeparator + "classes" + fileSeparator + "pre-alerts"
							+ fileSeparator + "inv-pack-lists" + fileSeparator + node);
			List<String> filesInPrimary = new ArrayList<String>();
			File[] files = new File(PRIMARY_FOLDER).listFiles();
			// If this pathname does not denote a directory, then listFiles() will return
			// null
			for (File file : files) {
				if (file.isFile()) {
					filesInPrimary.add(file.getName());
				}
			}

			String TARGET_FOLDER = request.getServletContext()
					.getRealPath(fileSeparator + "WEB-INF" + fileSeparator + "classes" + fileSeparator + "pre-alerts"
							+ fileSeparator + "mawbs" + fileSeparator + mawb + fileSeparator + node);
			// check if target folder already exists
			File dir_trgt = new File(TARGET_FOLDER);
			boolean exists = dir_trgt.exists();
			if (!exists) {
				success = (new File(TARGET_FOLDER)).mkdir();
				if (success) {
					for (String pfilename : filesInPrimary) {
						try {
							Path FROM = Paths.get(PRIMARY_FOLDER + fileSeparator + pfilename);
							Path TO = Paths.get(TARGET_FOLDER + fileSeparator + pfilename);
							CopyOption[] options = new CopyOption[] { StandardCopyOption.REPLACE_EXISTING,
									StandardCopyOption.COPY_ATTRIBUTES };
							java.nio.file.Files.copy(FROM, TO, options);
						} catch (IOException ioex) {
							ioex.printStackTrace();
						}
					}
				} else {
					logger.debug("file path= " + PRIMARY_FOLDER + ", could not be resolved");
				}
			} else {
				for (String pfilename : filesInPrimary) {
					try {
						Path FROM = Paths.get(PRIMARY_FOLDER + fileSeparator + node + fileSeparator + pfilename);
						Path TO = Paths.get(TARGET_FOLDER + fileSeparator + node + fileSeparator + pfilename);
						CopyOption[] options = new CopyOption[] { StandardCopyOption.REPLACE_EXISTING,
								StandardCopyOption.COPY_ATTRIBUTES };
						java.nio.file.Files.copy(FROM, TO, options);
					} catch (IOException ioex) {
						ioex.printStackTrace();
					}
				}
			}
		}
		String SOURCE_FOLDER = request.getServletContext().getRealPath(fileSeparator + "WEB-INF" + fileSeparator
				+ "classes" + fileSeparator + "pre-alerts" + fileSeparator + "mawbs" + fileSeparator + mawb);
		String OUTPUT_ZIP_FILE = request.getServletContext().getRealPath(fileSeparator + "WEB-INF" + fileSeparator
				+ "classes" + fileSeparator + "pre-alerts" + fileSeparator + "mawbs" + fileSeparator + mawb + ".zip");
		// zipFiles.zipDirectory(dir, zipDirName);
		File dir = new File(SOURCE_FOLDER);
		zipFiles.zipDirectory(dir, OUTPUT_ZIP_FILE);
		model.addAttribute("zipdownloadlink", mawb + ".zip");
		logger.info("Method TestController.showPreAlertZipDownload ends");
		String fileName= mawb + ".zip";
		String dataDirectory = request.getServletContext().getRealPath(fileSeparator + "WEB-INF" + fileSeparator
				+ "classes" + fileSeparator + "pre-alerts" + fileSeparator + "mawbs" + fileSeparator);
		logger.debug("file path= " + dataDirectory);
		Path file = Paths.get(dataDirectory, fileName);
		if (Files.exists(file)) {
			response.setContentType("application/zip");
			response.addHeader("Content-Disposition", "attachment; filename=" + fileName);
			try {
				Files.copy(file, response.getOutputStream());
				response.getOutputStream().flush();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		
		PreAlertHBL preAlertSearchParameter =  new PreAlertHBL();
		int updateStatus = 0;
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		
		preAlertSearchParameter.setSalesOrg(loginDTO.getSalesOrgSelected());
		preAlertSearchParameter.setDistributionChannel(loginDTO.getDisChnlSelected());
		preAlertSearchParameter.setDivision(loginDTO.getDivisionSelected());
		preAlertSearchParameter.setMblNumber(mblNumber);
		try {
			updateStatus =preAlertManagementService
					.updateMblDownloadStatus(preAlertSearchParameter);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	/*	return "testprealertzipdownload";*/
	}

	@RequestMapping(value = "/showPdfIPL", method = RequestMethod.GET)
	public/* @ResponseBody ResponseEntity<byte[]> */ void upload(@RequestParam("blNumber") String hblNumber,
			HttpServletRequest request, HttpServletResponse response) {

		logger.debug(this.getClass().getName() + " showing Invoice PackList for HBL: " + hblNumber);

		HttpSession session = SessionUtil.getHttpSession(request);

		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		String fileSeparator = System.getProperty("file.separator");
		String dataDirectory = request.getServletContext().getRealPath(fileSeparator + "WEB-INF" + fileSeparator
				+ "classes" + fileSeparator + "pre-alerts" + fileSeparator + "inv-pack-lists" + fileSeparator);
		logger.debug("File path: " + dataDirectory);

		/*
		 * PreAlertHBL preAlertHblBySupplierAndHBL = new PreAlertHBL();
		 * 
		 * try { preAlertHblBySupplierAndHBL =
		 * preAlertManagementService.getPreAlertBySingleHBL(hblNumber,
		 * loginDTO.getLoggedInUserName(), loginDTO.getDisChnlSelected(),
		 * loginDTO.getDivisionSelected(), loginDTO.getSalesOrgSelected()); } catch
		 * (Exception e) { logger.error(this.getClass() +
		 * ": Error Occured while searching in database for hbl-" + hblNumber);
		 * e.printStackTrace(); } String fileName = ""; if(preAlertHblBySupplierAndHBL
		 * != null) { fileName = preAlertHblBySupplierAndHBL.getFilePathPreAlertDoc(); }
		 * else { logger.error(this.getClass() + ": No record found for HBL-"+
		 * hblNumber); }
		 */

		String fileName = fileSeparator + hblNumber + fileSeparator + hblNumber + "-IPL.pdf";
		Path path = Paths.get(dataDirectory, fileName);

		/*
		 * ResponseEntity<byte[]> responseEntity = null; HttpHeaders httpHeaders = new
		 * HttpHeaders();
		 * 
		 * byte[] contents = null; try { contents = Files.readAllBytes(path); } catch
		 * (IOException e) { // TODO Auto-generated catch block e.printStackTrace(); }
		 */

		if (Files.exists(path)) {
			response.setContentType("application/pdf");
			// response.addHeader("Content-Disposition", "attachment; filename="+fileName);

			/*
			 * httpHeaders.setContentType(MediaType.parseMediaType("application/pdf"));
			 * httpHeaders.setContentDispositionFormData("fileIPL", path.toString());
			 * httpHeaders.setCacheControl("must-revalidate, post-check=0, pre-check=0");
			 * responseEntity = new ResponseEntity<byte[]>(contents, httpHeaders,
			 * HttpStatus.OK);
			 */

			try {
				Files.copy(path, response.getOutputStream());
				response.getOutputStream().flush();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		} else {
			// responseEntity = new ResponseEntity<byte[]>(contents, httpHeaders,
			// HttpStatus.NO_CONTENT);
		}
		// return responseEntity;
	}

	@RequestMapping(value = "/preAlertBLwise", method = RequestMethod.GET)
	public String getPreAlertBLwise(@RequestParam("status") String status, Model model, HttpServletRequest request) {
		HttpSession session = SessionUtil.getHttpSession(request);

		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		DashboardInfoBox dashboardInfoBox = (DashboardInfoBox) session.getAttribute(SessionUtil.DASHBOARD_INFO_DETAILS);

		PreAlertHBL preAlertSearchParameter = new PreAlertHBL();
		if (loginDTO.getAccgroup().equals(CommonConstant.CRM_CODE1)
				|| loginDTO.getAccgroup().equals(CommonConstant.CRM_CODE2)) {
			preAlertSearchParameter.setBuyerId(loginDTO.getCrmDashboardBuyerId());
		} else {
			preAlertSearchParameter.setBuyerId(loginDTO.getLoggedInUserName());
		}
		preAlertSearchParameter.setSalesOrg(loginDTO.getSalesOrgSelected());
		preAlertSearchParameter.setDistributionChannel(loginDTO.getDisChnlSelected());
		preAlertSearchParameter.setDivision(loginDTO.getDivisionSelected());

		List<PreAlertHBL> preAlertBuyerWise = new ArrayList<PreAlertHBL>();

		try {
			preAlertBuyerWise = preAlertManagementService.getPreAlertBuyerWiseList(preAlertSearchParameter, status);
		} catch (Exception e) {
			logger.error(this.getClass() + ": Error Occured while fetching data from database...");
			e.printStackTrace();
		}

		PreAlertBL preAlertBl = null;
		try {
			preAlertBl = preAlertManagementService.getPreAlertInfo(loginDTO, dashboardInfoBox, preAlertBuyerWise);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Map<String, List<PreAlertHBLDetails>> mblWiseHbl = preAlertBl.getMbl();

		model.addAttribute("preAlertMblListJSON", new Gson().toJson(mblWiseHbl));

		return "preAlertMasterUpload";
	}

	@RequestMapping(value = "/getPreAlertDetailsMBLwise", method = RequestMethod.POST)
	public ResponseEntity<?> searchPreAlertDocumentMBL(@RequestParam("blNumber") String mblNumber,
			HttpServletRequest request, HttpServletResponse response) {
		HttpSession session = SessionUtil.getHttpSession(request);

		ResponseEntity<?> responseEntity = null;

		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		DashboardInfoBox dashboardInfoBox = (DashboardInfoBox) session.getAttribute(SessionUtil.DASHBOARD_INFO_DETAILS);

		List<MyshipmentTrackHeaderBean> dashboardSupplierWiseHBL = new ArrayList<MyshipmentTrackHeaderBean>();

		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getInTransitList());
		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getDeliveredList());

		List<PreAlertHBL> preAlertHblByBuyerAgentList = new ArrayList<PreAlertHBL>();
		PreAlertHBL preAlertSearchParameter = new PreAlertHBL();

		preAlertSearchParameter.setSalesOrg(loginDTO.getSalesOrgSelected());
		preAlertSearchParameter.setDistributionChannel(loginDTO.getDisChnlSelected());
		preAlertSearchParameter.setDivision(loginDTO.getDivisionSelected());

		String myshipmentUID = "";

		if (loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.CRM_CODE1)
				|| loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.CRM_CODE2)) {
			myshipmentUID = loginDTO.getCrmDashboardBuyerId();
		} else {
			myshipmentUID = loginDTO.getLoggedInUserName();
		}

		if (mblNumber == null || mblNumber.equals("")) {
			responseEntity = new ResponseEntity("Error Occured while searching MBL/MAWB. Please try again",
					new HttpHeaders(), HttpStatus.BAD_REQUEST);
			return responseEntity;
		} else {
			preAlertSearchParameter.setMblNumber(mblNumber);
		}

		try {
			preAlertHblByBuyerAgentList = preAlertManagementService.getMBlWisePreAlert(myshipmentUID,
					preAlertSearchParameter);

		} catch (Exception e) {
			e.printStackTrace();
			responseEntity = new ResponseEntity("Error Occured while searching MBL/MAWB. Please try again",
					new HttpHeaders(), HttpStatus.BAD_REQUEST);
			return responseEntity;
		}

		if (preAlertHblByBuyerAgentList == null) {
			responseEntity = new ResponseEntity("HBL/HAWB Not Found.", new HttpHeaders(), HttpStatus.BAD_REQUEST);
			return responseEntity;
		}

		List<PreAlertHBLDetails> preAlertHblDetailsByBuyerAgentList = new ArrayList<PreAlertHBLDetails>();

		Map<String, List<PreAlertHBLDetails>> mblWiseHbl = new HashMap<String, List<PreAlertHBLDetails>>();

		for (MyshipmentTrackHeaderBean myshipmentTrackHeaderBean : dashboardSupplierWiseHBL) {
			for (PreAlertHBL preAlertHblByBuyerAgent : preAlertHblByBuyerAgentList) {
				if (myshipmentTrackHeaderBean.getBl_no().equals(preAlertHblByBuyerAgent.getHblNumber())) {
					PreAlertHBLDetails preAlertHBLDetails = new PreAlertHBLDetails();

					preAlertHBLDetails.setPreAlertHbl(preAlertHblByBuyerAgent);
					preAlertHBLDetails.setTrackHeaderBean(myshipmentTrackHeaderBean);

					if (myshipmentTrackHeaderBean.getMbl_no().equals(preAlertHblByBuyerAgent.getMblNumber())) {
						if (mblWiseHbl.containsKey(preAlertHblByBuyerAgent.getMblNumber())) {
							List<PreAlertHBLDetails> tempPreAlertList = mblWiseHbl
									.get(preAlertHblByBuyerAgent.getMblNumber());
							tempPreAlertList.add(preAlertHBLDetails);
							mblWiseHbl.put(preAlertHblByBuyerAgent.getMblNumber(), tempPreAlertList);
						} else {
							List<PreAlertHBLDetails> tempPreAlertList = new ArrayList<PreAlertHBLDetails>();
							tempPreAlertList.add(preAlertHBLDetails);

							mblWiseHbl.put(preAlertHblByBuyerAgent.getMblNumber(), tempPreAlertList);
						}
					}

					preAlertHBLDetails.setTrackHeaderBean(myshipmentTrackHeaderBean);
					preAlertHBLDetails.setPreAlertHbl(preAlertHblByBuyerAgent);

					preAlertHblDetailsByBuyerAgentList.add(preAlertHBLDetails);
				}
			}
		}

		responseEntity = new ResponseEntity<Map<String, List<PreAlertHBLDetails>>>(mblWiseHbl, new HttpHeaders(),
				HttpStatus.OK);

		return responseEntity;
	}

	@RequestMapping(value = "/preAlertBLtoDownload", method = RequestMethod.GET)
	public String getPreAlertBLtoDownload(Model model, HttpServletRequest request) {

		logger.info(this.getClass() + ": Generating Pre-Alert MBL/MAWB List...");

		HttpSession session = SessionUtil.getHttpSession(request);

		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		DashboardInfoBox dashboardInfoBox = (DashboardInfoBox) session.getAttribute(SessionUtil.DASHBOARD_INFO_DETAILS);

		List<MyshipmentTrackHeaderBean> dashboardSupplierWiseHBL = new ArrayList<MyshipmentTrackHeaderBean>();

		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getInTransitList());
		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getDeliveredList());
		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getStuffingDoneList());

		List<PreAlertHBL> preAlertHblByBuyerAgentList = new ArrayList<PreAlertHBL>();
		PreAlertHBL preAlertSearchParameter = new PreAlertHBL();

		preAlertSearchParameter.setSalesOrg(loginDTO.getSalesOrgSelected());
		preAlertSearchParameter.setDistributionChannel(loginDTO.getDisChnlSelected());
		preAlertSearchParameter.setDivision(loginDTO.getDivisionSelected());

		String myshipmentUID = loginDTO.getLoggedInUserName();

		try {
			logger.info(this.getClass() + ": Calling Service for PreAlert MBL/MAWB...");
			preAlertHblByBuyerAgentList = preAlertManagementService.getPreAlertForDownload(myshipmentUID,
					preAlertSearchParameter);
		} catch (Exception e) {
			logger.error(this.getClass() + ": Error Occured while fetching Data for PreAlert MBL/MAWB...");
			e.printStackTrace();
		}

		List<PreAlertHBLDetails> preAlertHblDetailsByBuyerAgentList = new ArrayList<PreAlertHBLDetails>();

		Map<String, List<PreAlertHBLDetails>> mblWiseHbl = new HashMap<String, List<PreAlertHBLDetails>>();

		for (MyshipmentTrackHeaderBean myshipmentTrackHeaderBean : dashboardSupplierWiseHBL) {
			for (PreAlertHBL preAlertHblByBuyerAgent : preAlertHblByBuyerAgentList) {
				if (myshipmentTrackHeaderBean.getBl_no().equals(preAlertHblByBuyerAgent.getHblNumber())) {
					PreAlertHBLDetails preAlertHBLDetails = new PreAlertHBLDetails();

					preAlertHBLDetails.setPreAlertHbl(preAlertHblByBuyerAgent);
					preAlertHBLDetails.setTrackHeaderBean(myshipmentTrackHeaderBean);

					if (myshipmentTrackHeaderBean.getMbl_no().equals(preAlertHblByBuyerAgent.getMblNumber())) {
						if (mblWiseHbl.containsKey(preAlertHblByBuyerAgent.getMblNumber())) {
							List<PreAlertHBLDetails> tempPreAlertList = mblWiseHbl
									.get(preAlertHblByBuyerAgent.getMblNumber());
							tempPreAlertList.add(preAlertHBLDetails);

							mblWiseHbl.put(preAlertHblByBuyerAgent.getMblNumber(), tempPreAlertList);
						} else {
							List<PreAlertHBLDetails> tempPreAlertList = new ArrayList<PreAlertHBLDetails>();
							tempPreAlertList.add(preAlertHBLDetails);

							mblWiseHbl.put(preAlertHblByBuyerAgent.getMblNumber(), tempPreAlertList);
						}
					}

					preAlertHBLDetails.setTrackHeaderBean(myshipmentTrackHeaderBean);
					preAlertHBLDetails.setPreAlertHbl(preAlertHblByBuyerAgent);

					preAlertHblDetailsByBuyerAgentList.add(preAlertHBLDetails);

				}
			}
		}
		model.addAttribute("preAlertMblListJSON", new Gson().toJson(mblWiseHbl));
		return "preAlertMasterDownload";

	}

	public List<String> generatePreAlertHBLlist(String mblNo, HttpServletRequest request) {
		logger.info(this.getClass() + ": Generating Pre-Alert MBL/MAWB List...");

		HttpSession session = SessionUtil.getHttpSession(request);

		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);

		DashboardInfoBox dashboardInfoBox = (DashboardInfoBox) session.getAttribute(SessionUtil.DASHBOARD_INFO_DETAILS);

		List<MyshipmentTrackHeaderBean> dashboardSupplierWiseHBL = new ArrayList<MyshipmentTrackHeaderBean>();
		List<String> hblNoList = new ArrayList<String>();

		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getInTransitList());
		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getDeliveredList());
		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getStuffingDoneList());

		List<PreAlertHBL> preAlertHblByBuyerAgentList = new ArrayList<PreAlertHBL>();
		PreAlertHBL preAlertSearchParameter = new PreAlertHBL();

		preAlertSearchParameter.setSalesOrg(loginDTO.getSalesOrgSelected());
		preAlertSearchParameter.setDistributionChannel(loginDTO.getDisChnlSelected());
		preAlertSearchParameter.setDivision(loginDTO.getDivisionSelected());

		String myshipmentUID = loginDTO.getLoggedInUserName();

		try {
			logger.info(this.getClass() + ": Calling Service for PreAlert MBL/MAWB...");
			preAlertHblByBuyerAgentList = preAlertManagementService.getPreAlertForDownload(myshipmentUID,
					preAlertSearchParameter);
		} catch (Exception e) {
			logger.error(this.getClass() + ": Error Occured while fetching Data for PreAlert MBL/MAWB...");
			e.printStackTrace();
		}

		List<PreAlertHBLDetails> preAlertHblDetailsByBuyerAgentList = new ArrayList<PreAlertHBLDetails>();

		for (MyshipmentTrackHeaderBean myshipmentTrackHeaderBean : dashboardSupplierWiseHBL) {
			for (PreAlertHBL preAlertHblByBuyerAgent : preAlertHblByBuyerAgentList) {
				if (myshipmentTrackHeaderBean.getBl_no().equals(preAlertHblByBuyerAgent.getHblNumber())) {

					if (myshipmentTrackHeaderBean.getMbl_no().equals(preAlertHblByBuyerAgent.getMblNumber())) {
						if (myshipmentTrackHeaderBean.getMbl_no().equals(mblNo)) {

							hblNoList.add(preAlertHblByBuyerAgent.getHblNumber());

						}
					}
				}
			}
			
		}
		

		
		return hblNoList;
	}
	
	public List<PreAlertHBL> getDashboardPreAlertList(DashboardInfoBox dashboardInfoBox,
			LoginDTO loginDTO) {

		List<MyshipmentTrackHeaderBean> dashboardSupplierWiseHBL = new ArrayList<MyshipmentTrackHeaderBean>();

		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getOpenOrderList());
		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getGoodsReceivedList());
		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getStuffingDoneList());
		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getInTransitList());

		List<PreAlertHBL> preAlertHblBySupplierList = new ArrayList<PreAlertHBL>();

		try {
			preAlertHblBySupplierList = preAlertManagementService.getPreAlertByHBL(loginDTO.getLoggedInUserName());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		/*List<PreAlertHBLDetails> preAlertHblDetailsBySupplierListAll = new ArrayList<PreAlertHBLDetails>();
		List<PreAlertHBLDetails> preAlertHblDetailsBySupplierListPending = new ArrayList<PreAlertHBLDetails>();
		List<PreAlertHBLDetails> preAlertHblDetailsBySupplierListCompleted = new ArrayList<PreAlertHBLDetails>();

		for (MyshipmentTrackHeaderBean myshipmentTrackHeaderBean : dashboardSupplierWiseHBL) {
			for (PreAlertHBL preAlertHblBySupplier : preAlertHblBySupplierList) {
				if (myshipmentTrackHeaderBean.getBl_no().equals(preAlertHblBySupplier.getHblNumber())) {
					PreAlertHBLDetails preAlertHBLDetails = new PreAlertHBLDetails();

					preAlertHBLDetails.setTrackHeaderBean(myshipmentTrackHeaderBean);
					preAlertHBLDetails.setPreAlertHbl(preAlertHblBySupplier);
					if (preAlertHblBySupplier.getIsPreAlertDoc().equals("0")) {
						preAlertHblDetailsBySupplierListPending.add(preAlertHBLDetails);
					} else if (preAlertHblBySupplier.getIsPreAlertDoc().equals("1")) {
						preAlertHblDetailsBySupplierListCompleted.add(preAlertHBLDetails);
					} else {
						preAlertHblDetailsBySupplierListAll.add(preAlertHBLDetails);
					}
				}
			}
		}

		Map<String, List<PreAlertHBLDetails>> preAlertDetailsMap = new HashMap<String, List<PreAlertHBLDetails>>();

		preAlertDetailsMap.put("pre-alert-all", preAlertHblDetailsBySupplierListAll);
		preAlertDetailsMap.put("pre-alert-pending", preAlertHblDetailsBySupplierListPending);
		preAlertDetailsMap.put("pre-alert-completed", preAlertHblDetailsBySupplierListCompleted);*/

		return preAlertHblBySupplierList;
	}
	
	public void getSeaNonNegoHouseDetail(RequestParams reqParams, HttpSession session, HttpServletRequest request) throws Exception {
		try {
			logger.info("Method SeaBillOfLandingController.getSeaBillOfLandingDetail starts.");
			if (reqParams != null) {
				StringBuffer webServiceUrl = new StringBuffer(RestUtil.SEA_BILL_OF_LANDING);
				AirBillOfLandingJsonData seaBillOfLandingJsonData = restService.postForObject(
						RestUtil.prepareUrlForService(webServiceUrl).toString(), reqParams,
						AirBillOfLandingJsonData.class);

				if (seaBillOfLandingJsonData.getBapiReturn1() != null) {
					if (seaBillOfLandingJsonData.getBapiReturn1().getType().equalsIgnoreCase("E")) {
						//return seaBillOfLandingJsonData.getBapiReturn1().getType();
					}
				}

				ObjectMapper mapper = new ObjectMapper();
				try {
					mapper.writeValueAsString(seaBillOfLandingJsonData);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				String reportType = request.getParameter("type");
				reportType = reportType == null ? "seaNNB" : reportType;
				seaBillOfLadingPdfGeneration pdfData = new seaBillOfLadingPdfGeneration();
				List<seaBillOfLadingPdfGeneration> actualPdfData = new ArrayList<seaBillOfLadingPdfGeneration>();
				Map<String, Object> parameterMap = new HashMap<String, Object>();

				List<NegoBillLanding> tabledata = seaBillOfLandingJsonData.getItHeader();
				List<NegoBillLandingI> tabledata2 = seaBillOfLandingJsonData.getItItems();
				List<ShippingReport> tabledata3 = seaBillOfLandingJsonData.getItAddress();
				List<NegoBillLanding> tabledata4 = seaBillOfLandingJsonData.getItShipper();
				List<NegoBillLanding> tabledata5 = seaBillOfLandingJsonData.getItConsignee();
				List<NegoBillLanding> tabledata6 = seaBillOfLandingJsonData.getItShipperCountry();
				List<NegoBillLanding> tabledata7 = seaBillOfLandingJsonData.getItConsigneeCountry();
				List<NegoBillLanding> tabledata8 = seaBillOfLandingJsonData.getItNotify();
				List<NegoBillLanding> tabledata9 = seaBillOfLandingJsonData.getItNotifyCountry();
				List<NegoBillLanding> tabledata10 = seaBillOfLandingJsonData.getItPor();
				List<NegoBillLanding> tabledata11 = seaBillOfLandingJsonData.getItPod();
				List<NegoBillLanding> tabledata12 = seaBillOfLandingJsonData.getItPol();
				List<NegoBillLanding> tabledata13 = seaBillOfLandingJsonData.getItPolDel();
				List<NegoBillLanding> tabledata14 = seaBillOfLandingJsonData.getItCountryOfOrig();
				List<NegoBillLanding> tabledata15 = seaBillOfLandingJsonData.getItShipperCountry();
				List<NegoBillLanding> tabledata16 = seaBillOfLandingJsonData.getItDelevery();
				List<NegoBillLanding> tabledata17 = seaBillOfLandingJsonData.getItCompCountry();
				List<NegoBillLanding> tabledata18 = seaBillOfLandingJsonData.getItDeleveryCountry();
				List<NegoBillLanding> tabledata19 = seaBillOfLandingJsonData.getItPlaceOfIssue();
				List<NegoBillLanding> tabledata20 = seaBillOfLandingJsonData.getItTsp();

				String text_shipmark, text_desc;
				NegoBillLanding a, a1, a2, a3, a4, a5, a6, a7, a8, a9, b, b1, b2, b3, b4, b6, b9, b20;
				ShippingReport b5;

				if (tabledata != null && !tabledata.isEmpty()) {
					a = tabledata.get(0);// header
					text_shipmark = a.getDesc_Z112().replaceAll("\\t+", "");
					text_desc = a.getDesc_Z023().replaceAll("\\t+", "");
					text_shipmark = text_shipmark.replaceAll("\\n+", " ");
					text_desc = text_desc.replaceAll("\\n+", " ");
					text_shipmark = text_shipmark.replaceAll("\\r+", " ");
					text_desc = text_desc.replaceAll("\\r+", " ");
					pdfData.setMarksNnumber(text_shipmark);
					pdfData.setDescription(text_desc);
					pdfData.setPreCarrige(a.getZzAirlineName1());
					pdfData.setVessel(a.getZzAirlineNo1());
					pdfData.setBillOfLadingNo(a.getZzHblHawbNo());
					pdfData.setInvoiceNo(a.getZzCommInvNo());
					pdfData.setType(a.getZzLcpottNoType());
					pdfData.setNo(a.getZzLcpotNo());
					pdfData.setExpNo(a.getZzExpNo());
					pdfData.setShippingBilNo(a.getZzShipping_Bl_No());
					pdfData.setDepartureDate(epochTimeToDate(a.getZzDepartureDt()));
					pdfData.setShippingAirlineName(a.getZzAirlineName1());
					pdfData.setShippingAirlineNo(a.getZzAirlineNo1()); // description
					pdfData.setMvsl(a.getZzAirline());
					pdfData.setMvsl1(a.getZzAirlineNo2());
					pdfData.setFreightMode(a.getZzFreightModes());
					pdfData.setDate1(epochTimeToDate(a.getZzCommInvDt()));
					pdfData.setDate2(epochTimeToDate(a.getZzLcdt()));
					pdfData.setDate3(epochTimeToDate(a.getZzExpDt()));
					pdfData.setDate4(DateUtil.formatDateToString(a.getZzShipping_Bl_Dt()));
					pdfData.setDateOfissue(epochTimeToDate(a.getZzDepartureDt()));
				}
				if (tabledata4 != null && !tabledata4.isEmpty()) {
					a1 = tabledata4.get(0);// shipper
					pdfData.setShipperName(a1.getName1());
					pdfData.setAddress1(a1.getName2());
					pdfData.setAddress2(a1.getName3());
					pdfData.setAddress3(a1.getName4());
					pdfData.setShipperCity(a1.getCity1());
				}
				if (tabledata5 != null && !tabledata5.isEmpty()) {
					a2 = tabledata5.get(0);// consignee
					pdfData.setConsigneeName(a2.getName1());
					pdfData.setConsigneeAd1(a2.getName2());
					pdfData.setConsigneeAd2(a2.getName3());
					pdfData.setConsigneeAd3(a2.getName4());
					pdfData.setCongineeCity(a2.getCity1());
				}
				if (tabledata6 != null && !tabledata6.isEmpty()) {
					a3 = tabledata6.get(0);// shippercountry
					pdfData.setCountry(a3.getLandX());
				}
				if (tabledata7 != null && !tabledata7.isEmpty()) {
					a4 = tabledata7.get(0);// consignercountry
					pdfData.setConsigneeCountry(a4.getLandX());
				}
				if (tabledata8 != null && !tabledata8.isEmpty()) {
					a5 = tabledata8.get(0);// notify
					pdfData.setNotify1(a5.getName1());
					pdfData.setNotify2(a5.getName2());
					pdfData.setNotify3(a5.getName3());
					pdfData.setNotify4(a5.getName4());
					pdfData.setNotifyCity(a5.getCity1());
				}
				if (!tabledata9.isEmpty()) {
					a6 = tabledata9.get(0);// notifycountry
					pdfData.setNotifyCountry(a6.getLandX());
				}
				if (tabledata10 != null && !tabledata10.isEmpty()) {
					a7 = tabledata10.get(0);// por
					pdfData.setPlaceOfReceipt(a7.getlGobe());
				}
				if (tabledata11 != null && !tabledata11.isEmpty()) {
					a8 = tabledata11.get(0);// pod
					pdfData.setPortOfDischarge(a8.getZzPortName());
				}
				if (tabledata12 != null && !tabledata12.isEmpty()) {
					a9 = tabledata12.get(0);// pol
					pdfData.setShippingPortName(a9.getZzPortName());
					pdfData.setPortOfLoading(a9.getZzPortName());
				}
				if (tabledata13 != null && !tabledata13.isEmpty()) {
					b = tabledata13.get(0);// poldel
					pdfData.setPlaceOfDelivery(b.getZzPortName());
				}
				if (tabledata14 != null && !tabledata14.isEmpty()) {
					b1 = tabledata14.get(0);// countryOforigin
					pdfData.setCountryOfOrigin(b1.getLandX());
				}
				if (tabledata15 != null && !tabledata15.isEmpty()) {
					b2 = tabledata15.get(0);// shippercountry
					pdfData.setShippingPortCountry(b2.getLandX());
				}
				if (tabledata16 != null && !tabledata16.isEmpty()) {
					b3 = tabledata16.get(0);// delivery
					pdfData.setDelName1(b3.getName1());
					pdfData.setDelName2(b3.getName2());
					pdfData.setDelName3(b3.getName3());
					pdfData.setDelName4(b3.getName4());
					pdfData.setDelCity(b3.getCity1());
				}
				if (tabledata18 != null && !tabledata18.isEmpty()) {
					b4 = tabledata18.get(0);// deliverycountry
					pdfData.setDelCountry(b4.getLandX());
				}
				if (tabledata17 != null && !tabledata17.isEmpty()) {
					b6 = tabledata17.get(0);// companycountry
					pdfData.setComCountry(b6.getLandX());
				}
				if (tabledata19 != null && !tabledata19.isEmpty()) {
					b9 = tabledata19.get(0);// placeofissue
					pdfData.setPlaceOfissue(b9.getCity1());
				}
				if (tabledata3 != null && !tabledata3.isEmpty()) {
					b5 = tabledata3.get(0);// address
					pdfData.setCompany(b5.getName1());
					pdfData.setComAdd1(b5.getName2());
					pdfData.setComAdd2(b5.getName3());
					pdfData.setComAdd3(b5.getName4());
					pdfData.setComCity(b5.getCity1());
				}
				if (tabledata20 != null && !tabledata20.isEmpty()) {
					b20 = tabledata20.get(0);
					pdfData.setTransPort(b20.getZzPortName());
				}

				String s = seaBillOfLandingJsonData.getTotalQuantity();
				Double i1 = Double.parseDouble(s);
				pdfData.setPacakgeNo(i1.intValue());
				pdfData.setGrossWeight(round(seaBillOfLandingJsonData.getTotalAmount()));
				pdfData.setMeasurment(seaBillOfLandingJsonData.getTotalCbm());
				actualPdfData.add(pdfData);

				NegoBillLanding headertable = new NegoBillLanding();
				if (tabledata != null && !tabledata.isEmpty()) {
					headertable = tabledata.get(0);// header
				} else
					headertable.setZzMode_Shipment("");

				if (tabledata2 != null && !tabledata2.isEmpty()) {
					for (int w = 0; w < tabledata2.size(); w++) {
						NegoBillLandingI table = tabledata2.get(w);
						seaBillOfLadingPdfGeneration pdfTable = new seaBillOfLadingPdfGeneration();
						pdfTable.setMode(headertable.getZzMode_Shipment());
						pdfTable.setContainerNo(table.getVhilm());
						pdfTable.setSealNo(table.getVhilm_ku());
						Double i = Double.parseDouble(table.getVemng());
						pdfTable.setQty(i.intValue());
						pdfTable.setSize(table.getWgbez60());
						// pdfTable.setCbm(round(table.getNtvol()));
						pdfTable.setCbm(table.getNtvol());
						pdfTable.setKgs(round(table.getNtGew()));
						actualPdfData.add(pdfTable);
					}
				}
				
				String inputFileName = "NNB";
				ServletContext context = request.getServletContext();
				String fileSeparator=System.getProperty("file.separator");
				//String jrxmlReportPath = request.getServletContext().getRealPath(fileSeparator+"WEB-INF"+fileSeparator+"classes"+fileSeparator+ inputFileName + ".jrxml");
				String jrxmlReportPath = request.getServletContext().getRealPath(fileSeparator+"WEB-INF"+fileSeparator+"classes"+fileSeparator+ inputFileName + ".jasper");
				//JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlReportPath);
				JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(jrxmlReportPath);
				
				JRDataSource pdfDataSource = new JRBeanCollectionDataSource(actualPdfData);
				//JRBeanCollectionDataSource jrbcds = new JRBeanCollectionDataSource(actualPdfData);
				
				parameterMap.put("datasource", pdfDataSource);
				String path = context.getRealPath("/") + "";
				parameterMap.put("Context", path);
				//JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameterMap, jrbcds);
				JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameterMap, pdfDataSource);

				String outputFile = request.getServletContext()
						.getRealPath(fileSeparator + "WEB-INF" + fileSeparator + "classes" + fileSeparator
								+ "pre-alerts" + fileSeparator + "inv-pack-lists" + fileSeparator + reqParams.getReq1()
								+ fileSeparator + reqParams.getReq1() + "-HOUSE" + ".pdf");
				// Creation of the HTML Jasper Reports
		        JasperExportManager.exportReportToPdfFile(jasperPrint, outputFile);
				
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void getAirNonNegoHouseDetail(RequestParams reqParams, HttpSession session, HttpServletRequest request) throws Exception {
		try {
			if (reqParams != null) {
				StringBuffer webServiceUrl = new StringBuffer(RestUtil.AIR_BILL_OF_LANDING);
				AirBillOfLandingJsonData airBillOfLandingJsonData = restService.postForObject(
						RestUtil.prepareUrlForService(webServiceUrl).toString(), reqParams,
						AirBillOfLandingJsonData.class);

				if (airBillOfLandingJsonData.getBapiReturn1() != null) {
					if (airBillOfLandingJsonData.getBapiReturn1().getType().equalsIgnoreCase("E")) {
						//return airBillOfLandingJsonData.getBapiReturn1().getType();
					}
				}

				ObjectMapper mapper = new ObjectMapper();
				try {
					System.out.println(mapper.writeValueAsString(airBillOfLandingJsonData));
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				if (airBillOfLandingJsonData != null) {
					String reportType = request.getParameter("type");
					reportType = reportType == null ? "airNNB" : reportType;
					airBillOfLadingPDFGeneration pdfData = new airBillOfLadingPDFGeneration();
					List<airBillOfLadingPDFGeneration> actualPdfData = new ArrayList<airBillOfLadingPDFGeneration>();
					List<airBillOfLadingPDFGeneration> actualPdfData2 = new ArrayList<airBillOfLadingPDFGeneration>();
					Map<String, Object> parameterMap = new HashMap<String, Object>();

					List<NegoBillLanding> tabledata = airBillOfLandingJsonData.getItShipperBank();
					List<NegoBillLanding> tabledata1 = airBillOfLandingJsonData.getItShipper();
					List<NegoBillLanding> tabledata2 = airBillOfLandingJsonData.getItConsigneeBank();
					List<NegoBillLanding> tabledata3 = airBillOfLandingJsonData.getItConsignee();
					List<NegoBillLanding> tabledata4 = airBillOfLandingJsonData.getItNotify();
					List<NegoBillLanding> tabledata5 = airBillOfLandingJsonData.getItNotifyCountry();
					List<NegoBillLanding> tabledata6 = airBillOfLandingJsonData.getItHeader();
					List<NegoBillLanding> tabledata7 = airBillOfLandingJsonData.getItPol();
					List<NegoBillLanding> tabledata8 = airBillOfLandingJsonData.getItPod();
					List<NegoBillLanding> tabledata9 = airBillOfLandingJsonData.getItPolDel();
					List<NegoBillLanding> tabledata10 = airBillOfLandingJsonData.getItPor();
					List<ShippingReport> tabledata11 = airBillOfLandingJsonData.getItAddress();
					List<NegoBillLandingI> tabledata12 = airBillOfLandingJsonData.getItVbap();
					List<NegoBillLanding> tabledata13 = airBillOfLandingJsonData.getItPlaceOfIssue();
					List<NegoBillLanding> tabledata14 = airBillOfLandingJsonData.getItShipperCountry();
					List<NegoBillLanding> tabledata15 = airBillOfLandingJsonData.getItConsigneeCountry();
					List<NegoBillLanding> tabledata16 = airBillOfLandingJsonData.getItCompCountry();
					List<NegoBillLanding> tabledata17 = airBillOfLandingJsonData.getItShbCountry();
					List<NegoBillLanding> tabledata18 = airBillOfLandingJsonData.getItCbCountry();

					NegoBillLanding a, a1, a2, a3, a4, a5, a6, a7, a8, a9, a13, a14, a15, a16, a17, a18;
					ShippingReport a11;
					NegoBillLandingI a12;

					if (tabledata != null && !tabledata.isEmpty()) {
						a = tabledata.get(0);// shipperbank
						pdfData.setShipperName1(a.getName1());
						pdfData.setShipperName2(a.getName2());
						pdfData.setShipperName3(a.getName3());
						pdfData.setShipperName4(a.getName4());
						pdfData.setShipperCity1(a.getCity1());
					}
					if (tabledata1 != null && !tabledata1.isEmpty()) {
						a1 = tabledata1.get(0);// shipper
						pdfData.setShipper2Name1(a1.getName1());
						pdfData.setShipper2Name2(a1.getName2());
						pdfData.setShipper2Name3(a1.getName3());
						pdfData.setShipper2Name4(a1.getName4());
						pdfData.setShipper2City1(a1.getCity1());
					}
					if (tabledata2 != null && !tabledata2.isEmpty()) {
						a2 = tabledata2.get(0);// consigneeBank
						pdfData.setConsigneeName1(a2.getName1());
						pdfData.setConsigneeName2(a2.getName2());
						pdfData.setConsigneeName3(a2.getName3());
						pdfData.setConsigneeName4(a2.getName4());
						pdfData.setConsigneeCity1(a2.getCity1());
					}
					if (tabledata18 != null && !tabledata18.isEmpty()) {
						a18 = tabledata18.get(0);
						pdfData.setConsigneeCountry(a18.getLandX());
					}
					if (tabledata4 != null && !tabledata4.isEmpty()) {
						a4 = tabledata4.get(0);// notify
						pdfData.setNotifyName1(a4.getName1());
						pdfData.setNotifyName2(a4.getName2());
						pdfData.setNotifyName3(a4.getName3());
						pdfData.setNotifyName4(a4.getName4());
						pdfData.setNotifyCity1(a4.getCity1());
					}
					if (tabledata5 != null && !tabledata5.isEmpty()) {
						a5 = tabledata5.get(0);// notifyCountry
						pdfData.setNotifyCountry(a5.getLandX());
					}
					if (tabledata3 != null && !tabledata3.isEmpty()) {
						a3 = tabledata3.get(0);// consignee
						pdfData.setConsignee2Name1(a3.getName1());
						pdfData.setConsignee2Name2(a3.getName2());
						pdfData.setConsignee2Name3(a3.getName3());
						pdfData.setConsignee2Name4(a3.getName4());
						pdfData.setConsignee2City1(a3.getCity1());
					}
					if (tabledata6 != null && !tabledata6.isEmpty()) {
						a6 = tabledata6.get(0);// header
						String text_shipmark = a6.getDesc_Z112().replaceAll("\\t+", "");
						String text_desc = a6.getDesc_Z023().replaceAll("\\t+", "");
						text_shipmark = text_shipmark.replaceAll("\\n+", " ");
						text_desc = text_desc.replaceAll("\\n+", " ");
						text_shipmark = text_shipmark.replaceAll("\\r+", " ");
						text_desc = text_desc.replaceAll("\\r+", " ");
						pdfData.setDescription(text_desc);
						pdfData.setShippingMark(text_shipmark);
						pdfData.setFlightNo(a6.getZzAirlineNo1());
						pdfData.setFlightDt(DateUtil.formatDateToString(a6.getZzLoadingDt()));
						pdfData.setMasterBil(a6.getZzMblMabwNO());
						pdfData.setBiilNo(a6.getZzHblHawbNo());
						pdfData.setAccountInfo(a6.getZzFreightModes());
						pdfData.setInvoice(a6.getZzCommInvNo());
						pdfData.setType(a6.getZzLcpottNoType());
						pdfData.setNo(a6.getZzLcpotNo());
						pdfData.setExpNo(a6.getZzExpNo());
						pdfData.setScbNo(a6.getZzShipping_Bl_No());
						pdfData.setDate1(epochTimeToDate(a6.getZzCommInvDt()));
						pdfData.setDate2(epochTimeToDate(a6.getZzLcdt()));
						pdfData.setDate3(epochTimeToDate(a6.getZzExpDt()));
						pdfData.setDate4(DateUtil.formatDateToString(a6.getZzShipping_Bl_Dt()));
						pdfData.setExcuted(DateUtil.formatDateToString(a6.getZzHblHawBdt()));
					}
					if (tabledata7 != null && !tabledata7.isEmpty()) {
						a7 = tabledata7.get(0);// pol
						pdfData.setAirportOfDep(a7.getZzPortName());
					}
					if (tabledata8 != null && !tabledata8.isEmpty()) {
						a8 = tabledata8.get(0);// pod
						pdfData.setRequestedRouting(a8.getZzPortName());
					}
					if (tabledata9 != null && !tabledata9.isEmpty()) {
						a9 = tabledata9.get(0);// poldel
						pdfData.setAirportOfDes(a9.getZzPortName());
					}
					if (tabledata10 != null && !tabledata10.isEmpty()) {
					}
					if (tabledata11 != null && !tabledata11.isEmpty()) {
						a11 = tabledata11.get(0);// address
						pdfData.setAddressName1(a11.getName1());
						pdfData.setAddressName2(a11.getName2());
						pdfData.setAddressName3(a11.getName3());
						pdfData.setAddressName4(a11.getName4());
						pdfData.setAddressCity1(a11.getCity1());
					}
					if (tabledata13 != null && !tabledata13.isEmpty()) {
						a13 = tabledata13.get(0);// placeofissue
						pdfData.setAt(a13.getCity1());
					}
					if (tabledata14 != null && !tabledata14.isEmpty()) {
						a14 = tabledata14.get(0);// shippercountry
						pdfData.setShipper2Country(a14.getLandX());
					}
					if (tabledata15 != null && !tabledata15.isEmpty()) {
						a15 = tabledata15.get(0);// consigneecountry
						pdfData.setConsignee2Country(a15.getLandX());
					}
					if (tabledata16 != null && !tabledata16.isEmpty()) {
						a16 = tabledata16.get(0);// comcountry
						pdfData.setAddressCountry(a16.getLandX());
					}
					if (tabledata17 != null && !tabledata17.isEmpty()) {
						a17 = tabledata17.get(0);// shbcountry
						pdfData.setShipperCountry(a17.getLandX());
					}

					pdfData.setIata("");
					actualPdfData.add(pdfData);

					if (tabledata6 != null && !tabledata6.isEmpty()) {
						for (int w = 0; w < tabledata6.size(); w++) {
							NegoBillLanding table = tabledata6.get(w);
							airBillOfLadingPDFGeneration pdfTable = new airBillOfLadingPDFGeneration();
							Double i = Double.parseDouble(table.getZzSoquantity());
							pdfTable.setNoOfPcs(i.intValue());
							pdfTable.setGw(round(String.valueOf(table.getZzGrossWeigth().doubleValue())));
							pdfTable.setKg(round(table.getNtGew()));
							pdfTable.setCw(round(table.getZzTot_Chrg_Wt()));
							pdfTable.setRate("As Arranged");
							pdfTable.setTotal(round(table.getZzTotalVolWt()));
							actualPdfData.add(pdfTable);
						}
					}

					if (tabledata12 != null && !tabledata12.isEmpty()) {
						for (int z = 0; z < tabledata12.size(); z++) {
							a12 = tabledata12.get(z);// itvap
							airBillOfLadingPDFGeneration pdfTable2 = new airBillOfLadingPDFGeneration();
							pdfTable2.setDimnlength((int) Double.parseDouble(a12.getZzLenght()));
							pdfTable2.setDimnHeight((int) Double.parseDouble(a12.getZzHeight()));
							pdfTable2.setDimnWidth((int) Double.parseDouble(a12.getZzWidth()));
							pdfTable2.setDimn1((int) Double.parseDouble(a12.getZzTotalNoPcs()));
							pdfTable2.setDimn2(a12.getZzQuantityUom());
							actualPdfData2.add(pdfTable2);
						}
					}
					String inputFileName = "nnblair";
					ServletContext context = request.getServletContext();
					String fileSeparator=System.getProperty("file.separator");
					//String jrxmlReportPath = request.getServletContext().getRealPath(fileSeparator+"WEB-INF"+fileSeparator+"classes"+fileSeparator+ inputFileName + ".jrxml");
					String jrxmlReportPath = request.getServletContext().getRealPath(fileSeparator+"WEB-INF"+fileSeparator+"classes"+fileSeparator+ inputFileName + ".jasper");
					//JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlReportPath);
					JasperReport jasperReport = (JasperReport) JRLoader.loadObjectFromFile(jrxmlReportPath);
					
					JRDataSource pdfDataSource = new JRBeanCollectionDataSource(actualPdfData);
					//JRBeanCollectionDataSource jrbcds = new JRBeanCollectionDataSource(actualPdfData);
					
					parameterMap.put("datasource", pdfDataSource);
					String path = context.getRealPath("/") + "";
					parameterMap.put("Context", path);
					//JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameterMap, jrbcds);
					JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameterMap, pdfDataSource);

					String outputFile = request.getServletContext()
							.getRealPath(fileSeparator + "WEB-INF" + fileSeparator + "classes" + fileSeparator
									+ "pre-alerts" + fileSeparator + "inv-pack-lists" + fileSeparator + reqParams.getReq1()
									+ fileSeparator + reqParams.getReq1() + "-HOUSE" + ".pdf");
					// Creation of the HTML Jasper Reports
			        JasperExportManager.exportReportToPdfFile(jasperPrint, outputFile);
					
				}
			}
			//return null;
		} catch (Exception ex) {
			/*modelAndView.setViewName("redirect:/getAirBillOfLandingPage");
			redirecAttributes.addFlashAttribute("message", "HAWB number is not valid! Please enter a valid HBL number");
			return modelAndView;*/
			ex.printStackTrace();
		}
	}
	
	private String epochTimeToDate(String epochString) {
		String dateReturn = "";
		long epoch;
		if(epochString != null) {
			epoch = Long.parseLong(epochString);
        	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        	dateReturn = sdf.format(new Date(epoch));
		}
		return dateReturn;
	}
	private String round(String df)
	{
		DecimalFormat r = new DecimalFormat("0.00");
		Double d=Double.valueOf(df);
		String s=r.format(d);
		return s;
	}
	
}
