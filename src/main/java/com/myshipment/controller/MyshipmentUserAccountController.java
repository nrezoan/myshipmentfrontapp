/**
*
*@author Ahmad Naquib
*/
package com.myshipment.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.myshipment.service.IAccountEdit;
import com.myshipment.service.ICompanySelectionService;
import com.myshipment.util.EncryptDecryptUtil;
import com.myshipment.util.SessionUtil;

import com.google.gson.Gson;
import com.myshipment.dto.DistributionChannel;
import com.myshipment.dto.Division;
import com.myshipment.dto.LoginDTO;
import com.myshipment.dto.SalesOrgTree;
import com.myshipment.dto.SupplierDetailsDTO;
import com.myshipment.model.BapiRet1;
import com.myshipment.model.DashboardInfoBox;
import com.myshipment.model.DefaultCompany;
import com.myshipment.model.DetailsEditParam;
import com.myshipment.model.IntelligentCompanyPercentageUsage;
import com.myshipment.model.IntelligentCompanyUsage;
import com.myshipment.model.SalesorgListBean;
import com.myshipment.model.SupRequestParams;

@Controller
public class MyshipmentUserAccountController {

	private Logger logger = Logger.getLogger(MyshipmentUserAccountController.class);
	
	@Autowired
	private ICompanySelectionService companySelectionService;
	@Autowired
	private IAccountEdit editDetailService;
	
	@RequestMapping("/myAccount")
	public String getMyAccount(Model model, @ModelAttribute("error") String message, HttpServletRequest request,
			HttpServletResponse response,RedirectAttributes redirectAttributes) {
		
		logger.info("getMyAccount() called...");
		
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		
		List<IntelligentCompanyPercentageUsage> companyUsagePercentageList = companySelectionService.getCompanyUsagePercentageList(loginDTO);
		DefaultCompany defaultCompany = companySelectionService.getDefaultCompanyResult(loginDTO);
		
		DetailsEditParam editparam = new DetailsEditParam();
		model.addAttribute("editparam", editparam);
		
		model.addAttribute("companyUsagePercentageList", companyUsagePercentageList);
		model.addAttribute("defaultCompanyJson", new Gson().toJson(defaultCompany));
		
		return "myshipmentMyAccount";
	}
	
	@RequestMapping(value="/saveDefaultCompany", method=RequestMethod.POST)
	public ResponseEntity<?> saveDefaultCompany(UriComponentsBuilder bd, HttpServletRequest request,
			@RequestParam("salesOrg") String salesOrg, @RequestParam("distChannel") String distChannel,
			@RequestParam("division") String division) {
		
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		
		int status = 0;
		
		DefaultCompany defaultCompany = new DefaultCompany();
		
		defaultCompany.setUserId(loginDTO.getLoggedInUserName());
		defaultCompany.setPreferredCompanyId(salesOrg);
		defaultCompany.setDistributionChannel(distChannel);
		defaultCompany.setDivision(division);
		defaultCompany.setActive("1");
		defaultCompany.setUpdatedDate("");
		
		try {
			status = companySelectionService.saveDefaultCompany(defaultCompany);
		} catch (Exception e) {
			logger.info(this.getClass() + " : Error Occured While saving default company information");
			e.printStackTrace();
		}
		ResponseEntity<?> saveResponse = null;
		UriComponents uriComponents = bd.path("/homepage").build();
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(uriComponents.toUri());
		
		if(status == 1) {
			
			loginDTO.setSalesOrgSelected(salesOrg);
			loginDTO.setDisChnlSelected(distChannel);
			loginDTO.setDivisionSelected(division);
			
			session.setAttribute(SessionUtil.LOGIN_DETAILS, loginDTO);
			
			/*saveResponse = new ResponseEntity<Void>(headers, HttpStatus.OK);*/
			saveResponse = ResponseEntity.ok(loginDTO);
		} else {
			saveResponse = new ResponseEntity<Void>(headers, HttpStatus.NO_CONTENT);
		}
		
		
		return saveResponse;
	}
	
	@RequestMapping(value="/activationDefaultCompany", method=RequestMethod.POST)
	public ResponseEntity<?> activationDefaultCompany(UriComponentsBuilder bd, HttpServletRequest request,
			@RequestParam("active") String active) {
		//1->activated
		//0->deactivated
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		
		int status = 0;
		if(active != null) {
			DefaultCompany defaultCompany = new DefaultCompany();
			
			defaultCompany.setUserId(loginDTO.getLoggedInUserName());
			defaultCompany.setActive(active);
			defaultCompany.setUpdatedDate("");
			
			try {
				status = companySelectionService.activateDefaultCompany(defaultCompany);
			} catch (Exception e) {
				logger.info(this.getClass() + " : Error Occured While saving default company information");
				e.printStackTrace();
			}
		}
		
		ResponseEntity<?> saveResponse = null;
		UriComponents uriComponents = bd.path("/homepage").build();
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(uriComponents.toUri());
		
		if(status == 1) {
			
			/*loginDTO.setSalesOrgSelected(salesOrg);
			loginDTO.setDisChnlSelected(distChannel);
			loginDTO.setDivisionSelected(division);
			
			session.setAttribute(SessionUtil.LOGIN_DETAILS, loginDTO);*/
			
			saveResponse = new ResponseEntity<Void>(headers, HttpStatus.OK);
		} else {
			saveResponse = new ResponseEntity<Void>(headers, HttpStatus.NO_CONTENT);
		}
		
		
		return saveResponse;
	}
	
	@RequestMapping(value = "/changeOldPassword", method = RequestMethod.POST)
	public ResponseEntity<?> changeOldPassword(@RequestParam("oldPasswordParam") String oldPasswordParam, 
			@RequestParam("newPasswordParam") String newPasswordParam, 
			@RequestParam("verifyPasswordParam") String verifyPasswordParam,
			Model model, HttpSession session, ModelAndView modelAndView) {
		
		logger.info("Method AccountEditController.setNewPasswordInput starts.");
		LoginDTO loginDto = (LoginDTO) session.getAttribute("loginDetails");
		
		DetailsEditParam editparam = new DetailsEditParam();
		editparam.setPassword(oldPasswordParam);
		editparam.setNewPassword(newPasswordParam);
		editparam.setVerifyPassword(verifyPasswordParam);
		
		
		EncryptDecryptUtil encryptDecrypt = new EncryptDecryptUtil();
		String customerNo = null;
		String password = null;
		
		//ResponseEntity<?> saveResponse = null;
		
		if (loginDto != null) {
			customerNo = loginDto.getLoggedInUserName();
			password = encryptDecrypt.decrypt(loginDto.getLoggedInUserPassword());
			logger.info("Customer No found as : " + customerNo);
		}

		if (password.equals(editparam.getPassword())) {
			if (editparam != null) {
				editparam.setCustomerNo(customerNo);
				
				BapiRet1 editJsonData = null;
				editJsonData = editDetailService.getAccountEditData(editparam);
				if(editJsonData.getType().equals("S")) {
					String encryptedPassword = encryptDecrypt.encrypt(editparam.getNewPassword());
					if(encryptedPassword != null) {
						loginDto.setLoggedInUserPassword(encryptedPassword);
					}
			    	 //return ResponseEntity.ok("Password Successfully Changed!");
					return ResponseEntity.ok("CHANGED");
					
				}
				else if(editJsonData.getType().equals("E")) {
			    	 //return ResponseEntity.ok("Error Occured While changing password. Please Try Again");
					return ResponseEntity.ok("ERROR");
					
				}
				
			}
		}
		else {
	    	 //return ResponseEntity.ok("You have entered wrong password in the Old password field");
			return ResponseEntity.ok("WRONG");
		}
		return null;

	}
	
}
