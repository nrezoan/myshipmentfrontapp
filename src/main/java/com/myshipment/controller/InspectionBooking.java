package com.myshipment.controller;

import java.io.File;
import java.io.InputStream;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.myshipment.model.InspectionBookingBean;
import com.myshipment.model.InspectionTrackingParams;
import com.myshipment.model.InspectionTrackingResultBean;
import com.myshipment.dto.LoginDTO;
import com.myshipment.mappers.inspectionMapper;
import com.myshipment.util.DatabaseComponent;
import com.myshipment.util.SendMail;
import com.myshipment.util.SessionUtil;

@Controller
public class InspectionBooking{
	/**
	 * 
	 */
	@Autowired
	public inspectionMapper inspectMapper;
	private static final Logger logger = Logger.getLogger(InspectionBooking.class);
	
	//private static final long serialVersionUID = 3241557202361645759L;
	private String supplierId="";
	private InputStream inStream;
	private String buyingHouse="";
	private String poNumber="";
	private String inspectionDate="";
	private int inspectionQuantity=0;
	private String packMethod="";
	private String cargoDeliveryDate="";
	private String location="";
	private String customerName="";
	private String emailList="";
	private String remarks="";
	private String deliveryDate="";
	private int quantity=0;
	private String sapNo="";
	private String merchantDiser="";
	private String advOrder="";
	private String salesOrg="";
	private String division="";
	private String distChannel="";
	private String mode="";
	//private String remakrs="";
	//private String entryTime="";
	private String status="";
	private List<HashMap<String, String>> inspectionList;
	private List<Object> insBookingList;
	private List<Object> inspectorCodeList;
	//private List<Object> insTrackingStatusList;
	//private HttpServletRequest servletRequest;
	
	protected HttpServletRequest request;
    protected HttpServletResponse response;
    private String type="";
	private String scotaFileSubmission="N";
	private String scotaFileSubmissionDate;
	private double bookingNo=0;
	//private HashMap inspectorCodeMap=new HashMap();
	private String inspectorCode="";
	private String[] poNos;
	private String[] sapNos;
	private String[] inpsStatus;
	private String[] bookingNos;
	private String receiveDateFrom;
	private String receiveDateTo;
	private String inspectionDateFrom;
	private String inspectionDateTo;
	private String orderNo;
	private File packingList;
	private String packingListContentType;
	private String packingListFileName;
	
	/*@RequestMapping(value = "/doinspectionbooking", method = RequestMethod.GET)
	public String doInspectionBooking(ModelMap model, HttpServletRequest request){
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO)session.getAttribute(SessionUtil.LOGIN_DETAILS);
		if(loginDTO!=null)
			model.addAttribute("disChannel", loginDTO.getDisChnlSelected());
			model.addAttribute("supplierId", loginDTO.getLoggedInUserName());
			model.addAttribute("division", loginDTO.getDivisionSelected());
			model.addAttribute("salesOrg", loginDTO.getSalesOrgSelected());
		InspectionBookingBean insBookingData = new InspectionBookingBean();
		insBookingData.setDistChannel(loginDTO.getDisChnlSelected());
		insBookingData.setSupplierId(loginDTO.getLoggedInUserName());
		insBookingData.setDivision(loginDTO.getDivisionSelected());
		insBookingData.setSalesOrg(loginDTO.getSalesOrgSelected());
		model.addAttribute("insBookingData", insBookingData);		
		
		return "inspectionBooking";
	}*/
	

	
	/*@RequestMapping(value = "/populatepo", method = RequestMethod.GET)
	@ResponseBody
	public List<String> getPopulatePO(@RequestParam("param") String param) {

		DatabaseComponent ds = null;
		List<String> lstPortList = new ArrayList<String>();
		try {
			// POAutoCompleter db = new POAutoCompleter();
			ds = new DatabaseComponent();
			System.out.println("Query String: ");
			String query = param;
			System.out.println("Query String:" + query);
			if (query.length() > 6) {
				//convert to batis
				String selectQuery = "select order_no from orders where order_no like '" + query + "%' and FLAG='P'";
				System.out.println("select Query: " + selectQuery);
				ResultSet rs = ds.retrieve(selectQuery);

				while (rs.next()) {
					String poNumber = rs.getString("order_no");
					lstPortList.add(poNumber);
					// poNumbers.add(poNumber);
					// out.println(poNumber);
				}
				ds.closeConn();
			}
			/*
			 * List poNumbers = db.getData(query);
			 * 
			 * Iterator iterator = poNumbers.iterator();
			 * while(iterator.hasNext()) { String poNumber =
			 * (String)iterator.next(); out.println(poNumber); }
			 */
		/*} catch (Exception ex) {
			try{
				ds.closeConn();
			}
			catch(Exception e){
				e.printStackTrace();
			}
			ex.printStackTrace();
		}
		return lstPortList;

	}*/
	
//	public String doPOStatusUpdate(){
//		DatabaseComponent db=null;
//		try{
//			db=new DatabaseComponent();
//			
//			
//			db.update("update orders set FLAG='"+getStatus()+"' where order_no='"+getPoNumber()+"'");
//			db.commitTrans();
//			
//			db.closeConn();
//			addActionMessage("PO Updated Successfully");
//		}catch(Exception e){
//			try{
//			db.closeConn();
//			e.printStackTrace();
//			addActionMessage("PO Updated Error, Please Contact Myshipment Support");
//			}catch (Exception ex) {
//				// TODO: handle exception
//			}
//		}
//		return SUCCESS;
//	}
//	
//	public String doInspectionTrackingForSupplier(){
//		System.out.println("Come into Supplier");
//		return SUCCESS;
//	}
//	public String doInspectionTrackingForBuyer(){
//		System.out.println("Come into Buyer");
//		return SUCCESS;
//	}
//	public String getInspectionBookingSearchForm(){
//		System.out.println("Come into Buyer");
//		return SUCCESS;
//	}
//	
//	public String getPoEntryForm(){
//		System.out.println("Come into PO Entry Form");
//		DatabaseComponent db=null;
//		inspectorCodeList=new ArrayList<Object>();
//		try{
//			db=new DatabaseComponent();
//			String query="select distinct inspector_code from mst_merchndiser";
//			ResultSet rs=db.retrieve(query);
//			while(rs.next()){
//				inspectorCodeList.add(rs.getString("inspector_code").trim().toString());
//				System.out.println("code: "+ rs.getString("inspector_code").trim().toString());
//			}
//			System.out.println("size in servelt: "+inspectorCodeList.size());
//			
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//		return SUCCESS;
//	}
//	
//	public String doPoEntry(){
//		
//		DatabaseComponent db=null;
//		String query="";
//		inspectorCodeList=new ArrayList<Object>();
//		try{
//			db=new DatabaseComponent();
//			query="insert into orders (ORDER_NO,SAP_NO,INSPECTOR_CODE,SHIPMENT_MODE,QUANTITY,DELIVERY_DATE,ADV_ORDER,ENTRY_DATE,FLAG,CUSTOMER_NAME) values" +
//					      "('"+getPoNumber()+"','"+getSapNo()+"','"+getMerchantDiser()+"','"+getMode()+"',"+getQuantity()+",to_date('"+getDeliveryDate()+"','mm/dd/yyyy'),'"+getAdvOrder() +"',sysdate,'P','"+getCustomerName()+"')";
//			
//			System.out.println("Query:"+ query);
//			db.insert(query);
//			addActionMessage("Order Saved Successfully");
//			
//			
//			
//		}catch(Exception e){
//			e.printStackTrace();
//			addActionMessage("This order already exist, For Re-Inspection, kindly do PO Update");
//			/*query="select distinct inspector_code from mst_merchndiser";
//			try{
//			ResultSet rs=db.retrieve(query);
//			while(rs.next()){
//				inspectorCodeList.add(rs.getString("inspector_code").trim().toString());
//				System.out.println("code: "+ rs.getString("inspector_code").trim().toString());
//			}
//			}catch(Exception ex){
//				
//			}*/ 
//			
//		}
//		finally{
//		query="select distinct inspector_code from mst_merchndiser";
//		try{
//		ResultSet rs=db.retrieve(query);
//		while(rs.next()){
//			inspectorCodeList.add(rs.getString("inspector_code").trim().toString());
//			System.out.println("code: "+ rs.getString("inspector_code").trim().toString());
//		}
//		db.closeConn();
//		}catch(Exception e){
//			e.printStackTrace();
//		}
//		}
//		return SUCCESS;
//	}
//	public String updateInspectionBooking(){
//		DatabaseComponent db=null;
//		try{
//			db=new DatabaseComponent();
//			
//			StringBuffer updateQuery=new StringBuffer();
//			 updateQuery.append("update booking set " +
//					"INSPECTION_DATE=to_date('"+getInspectionDate()+"','mm/dd/yyyy'),  " +
//					"REMARKS='"+getRemarks()+"',  " +
//					            
//					"INSPECTION_QTY="+getInspectionQuantity()+", " +
//					"LOCATION='"+getLocation()+"'," +
//					"PACK_METHOD='"+getPackMethod()+"'") ;
//					if(!getCargoDeliveryDate().equalsIgnoreCase("")){
//						updateQuery.append(", CARGO_DELIVERY_DATE =to_date('"+getCargoDeliveryDate()+"','mm/dd/yyyy')" );
//					}
//					updateQuery.append("	where" +
//					" 		BOOKING_NO="+getBookingNo()+"" +
//					" and PO_NUMBER='"+getPoNumber()+"'");
//			System.out.println(updateQuery.toString());
//			db.update(updateQuery.toString());
//			db.commitTrans();
//			
//			db.closeConn();
//			addActionMessage("Inspection Booking Updated Successfully");
//		}catch(Exception e){
//			try{
//			db.closeConn();
//			e.printStackTrace();
//			addActionMessage("Inspection Booking Update Failed, Please Contact Myshipment Support");
//			}catch (Exception ex) {
//				// TODO: handle exception
//			}
//		}
//		return SUCCESS;
//	}
//	
//	public String doInspectionTrackingResult(){
//		DatabaseComponent db=null;
//		inspectionList=new ArrayList<HashMap<String,String>>();
//		insBookingList=new ArrayList<Object>();
//		try{
//			
//			db=new DatabaseComponent();
//			String selectQuery="";
//			if(getType().equalsIgnoreCase("supplier")){
//			selectQuery="SELECT BOOKING_NO, SALES_ORG, DIST, DIVISION, SUPPLIER_CODE, BUYING_CODE, INSPECTOR_CODE, to_char(INSPECTION_DATE, 'dd-Mon-yyyy') as INSPECTION_DATE, PO_NUMBER, to_char(DELIVERY_DATE,'dd-Mon-yyyy') as DELIVERY_DATE, ORDER_QTY, SHIPMENT_MODE, REMARKS, ENTRY_TIME, SAP_NO, INSPECTION_QTY, LOCATION, ADV_ORDER,PACK_METHOD,CUSTOMER_NAME,CARGO_DELIVERY_DATE FROM BOOKING WHERE INSPECTION_DATE=TO_DATE('"+getInspectionDate()+"','mm/dd/yyyy') " +
//					"and SUPPLIER_CODE='"+getSupplierId()+"'";
//			
//			}else{
//				selectQuery="SELECT BOOKING_NO, SALES_ORG, DIST, DIVISION, SUPPLIER_CODE, BUYING_CODE, INSPECTOR_CODE, to_char(INSPECTION_DATE, 'dd-Mon-yyyy') as INSPECTION_DATE, PO_NUMBER, to_char(DELIVERY_DATE,'dd-Mon-yyyy') as DELIVERY_DATE, ORDER_QTY, SHIPMENT_MODE, REMARKS, ENTRY_TIME, SAP_NO, INSPECTION_QTY, LOCATION, ADV_ORDER,PACK_METHOD,CUSTOMER_NAME,CARGO_DELIVERY_DATE FROM BOOKING WHERE INSPECTION_DATE=TO_DATE('"+getInspectionDate()+"','mm/dd/yyyy') and BUYING_CODE='"+getSupplierId()+"'";
//					
//			}
//			System.out.println("selectQuery: "+ selectQuery);
//			ResultSet rs=db.retrieve(selectQuery);
//			while(rs.next()){
//				HashMap<String, String> inspectionBooking=new HashMap<String, String>();
//				InspectionBookingBean insBookingBean=new InspectionBookingBean();
//				
//				String poNumber=rs.getString("PO_NUMBER");
//				String sapNo=rs.getString("SAP_NO");
//				String inspectionQty=rs.getString("INSPECTION_QTY");
//				String orderQty=rs.getString("ORDER_QTY");
//				String merchanDiser=rs.getString("INSPECTOR_CODE");
//				String mode=rs.getString("SHIPMENT_MODE");
//				String location=rs.getString("LOCATION");
//				String advOrder=rs.getString("ADV_ORDER");
//				String remarks=rs.getString("REMARKS");
//				String inspectionDate=rs.getString("INSPECTION_DATE");
//				String deliveryDate=rs.getString("DELIVERY_DATE");
//				String entryTime = rs.getString("ENTRY_TIME");
//				
//				insBookingBean.setPoNumber(poNumber);
//				insBookingBean.setSapNo(sapNo);
//				insBookingBean.setInspectionQuantity(Integer.parseInt(inspectionQty));
//				insBookingBean.setQuantity(Integer.parseInt(orderQty));
//				insBookingBean.setMerchantDiser(merchanDiser);
//				insBookingBean.setMode(mode);
//				insBookingBean.setAdvOrder(advOrder);
//				insBookingBean.setRemarks(remarks);
//				insBookingBean.setLocation(location);
//				insBookingBean.setInspectionDate(inspectionDate);
//				insBookingBean.setDeliveryDate(deliveryDate);
//				insBookingBean.setEntryTime(entryTime);
//				
//				inspectionBooking.put("poNumber", poNumber);
//				inspectionBooking.put("sapNo", sapNo);
//				inspectionBooking.put("inspectionQty", inspectionQty);
//				inspectionBooking.put("orderQty", orderQty);
//				inspectionBooking.put("merchanDiser", merchanDiser);
//				inspectionBooking.put("mode", mode);
//				inspectionBooking.put("advOrder", advOrder);
//				inspectionBooking.put("remarks", remarks);
//				inspectionBooking.put("location", location);
//				inspectionBooking.put("inspectionDate", inspectionDate);
//				inspectionBooking.put("deliveryDate", deliveryDate);
//				inspectionBooking.put("entryTime", entryTime);
//				insBookingList.add(insBookingBean);
//				inspectionList.add(inspectionBooking);
//				
//			}
//			request.setAttribute("inspectionList", inspectionList);
//			db.closeConn();
//			
//		}catch(Exception e)
//		{
//		e.printStackTrace();
//		try{
//			db.closeConn();
//			
//		}catch(Exception ex){
//			
//		}
//		}
//		return SUCCESS;
//	}
//	
//	
//	public String getInspectionBookingList(){
//		DatabaseComponent db=null;
//		inspectionList=new ArrayList<HashMap<String,String>>();
//		
//		try{
//			
//			db=new DatabaseComponent();
//			String selectQuery="";
//			
//			
//			selectQuery="SELECT BOOKING_NO, SALES_ORG, DIST, DIVISION, SUPPLIER_CODE, BUYING_CODE, INSPECTOR_CODE, to_char(INSPECTION_DATE, 'dd-Mon-yyyy') as INSPECTION_DATE, PO_NUMBER, to_char(DELIVERY_DATE,'dd-Mon-yyyy') as DELIVERY_DATE, ORDER_QTY, SHIPMENT_MODE, REMARKS, ENTRY_TIME, SAP_NO, INSPECTION_QTY, LOCATION, ADV_ORDER,PACK_METHOD,CUSTOMER_NAME,CARGO_DELIVERY_DATE FROM BOOKING WHERE INSPECTION_DATE=TO_DATE('"+getInspectionDate()+"','mm/dd/yyyy')" +
//					"and SUPPLIER_CODE='"+getSupplierId()+"'";
//			// and SUPPLIER_CODE='"+getSupplierId()+"'
//			System.out.println("selectQuery: "+ selectQuery);
//			ResultSet rs=db.retrieve(selectQuery);
//			while(rs.next()){
//				HashMap<String, String> inspectionBooking=new HashMap<String, String>();
//				
//				String bookingNo=rs.getString("BOOKING_NO");
//				String poNumber=rs.getString("PO_NUMBER");
//				String sapNo=rs.getString("SAP_NO");
//				String inspectionQty=rs.getString("INSPECTION_QTY");
//				String location=rs.getString("LOCATION");
//				String packMethod=rs.getString("PACK_METHOD");
//				String remarks=rs.getString("REMARKS");
//				String inspectionDate=rs.getString("INSPECTION_DATE");
//				String cargoDeliveryDate=rs.getString("CARGO_DELIVERY_DATE");
//				
//				inspectionBooking.put("bookingNo", bookingNo);
//				inspectionBooking.put("poNumber", poNumber);
//				inspectionBooking.put("sapNo", sapNo);
//				inspectionBooking.put("inspectionQty", inspectionQty);
//				inspectionBooking.put("remarks", remarks);
//				inspectionBooking.put("location", location);
//				inspectionBooking.put("inspectionDate", inspectionDate);
//				inspectionBooking.put("cargoDeliveryDate", cargoDeliveryDate);
//				inspectionBooking.put("packMethod", packMethod);
//				
//				inspectionList.add(inspectionBooking);
//				
//			}
//			request.setAttribute("inspectionList", inspectionList);
//			db.closeConn();
//			
//		}catch(Exception e)
//		{
//		e.printStackTrace();
//		try{
//			db.closeConn();
//			
//		}catch(Exception ex){
//			
//		}
//		}
//		return SUCCESS;
//	}
//	
//	public String getInspectionAccountInformation(){
//		DatabaseComponent db=null;
//		inspectionList=new ArrayList<HashMap<String,String>>();
//		
//		try{
//			
//			db=new DatabaseComponent();
//			String selectQuery="";
//			
//			
//			selectQuery="SELECT SUPPLIER_CODE,SUPPLIER_EMAIL,SUPPLIER_NAME from MST_SUPPLIER Where SUPPLIER_CODE='"+getSupplierId()+"'";
//			System.out.println("selectQuery: "+ selectQuery);
//			ResultSet rs=db.retrieve(selectQuery);
//			while(rs.next()){
//				customerName=rs.getString("SUPPLIER_NAME");
//				emailList=rs.getString("SUPPLIER_EMAIL");
//			}
//			db.closeConn();
//			
//		}catch(Exception e)
//		{
//		e.printStackTrace();
//		try{
//			db.closeConn();			
//		   }catch(Exception ex){}		
//		}
//		
//		System.out.println("Supplier Name:"+customerName);
//		System.out.println("Email Address:"+emailList);
//		return SUCCESS;
//	}
//	
//	public String updateInspectionAccountInfo()
//	{
//		DatabaseComponent db=null;
//		try{
//			db=new DatabaseComponent();
//			db.update("update MST_SUPPLIER set SUPPLIER_EMAIL='"+getEmailList()+"' where SUPPLIER_CODE='"+getSupplierId()+"'");
//			db.commitTrans();
//			db.closeConn();
//			inStream = new StringBufferInputStream("success");
//		}catch(Exception e){
//			try{
//			inStream = new StringBufferInputStream("error");
//			db.closeConn();
//			e.printStackTrace();
//			
//			}catch (Exception ex) {
//				// TODO: handle exception
//			}
//		}
//		
//		return "success";
//		
//		
//	}
//	
//	public String searchInspectionPo()
//	{
//		DatabaseComponent db=null;
//		String responseString="";
//		responseString="<table width='96%' border='1' cellpadding='0' cellspacing='0' style='border-collapse:collapse; border:1px solid #408080;'> " +
//				       " <tr>" +
//				       "  <td align='center' bgcolor='#6699CC' width='10%' height='20'><b>Srl</b></td> " +
//				       "  <td align='center' bgcolor='#A3C1E0' width='20%'><b>Order No</b></td> " +
//				       "  <td align='center' bgcolor='#6699CC' width='10%'><b>SAP No</b></td> " +
//				       "  <td align='center' bgcolor='#A3C1E0' width='10%'><b>Inspector Code</b></td> " +
//				       "  <td align='center' bgcolor='#6699CC' width='10%'><b>Quantity</b></td> " +
//				       "  <td align='center' bgcolor='#A3C1E0' width='15%'><b>Entry Date</b></td>" +
//				       "  <td align='center' bgcolor='#6699CC' width='5%'><b>Mode</b></td>" +
//				       "  <td align='center' bgcolor='#A3C1E0' width='5%'><b>Advert</b></td>" +
//				       "  <td align='center' bgcolor='#6699CC' width='15%'><b>Delivery Date</b></td>" +
//				       "  </tr>";
//		String color1="#F2F2F2";
//		String color2="#F7FBFB";
//		String color="";
//		try{
//			
//			db=new DatabaseComponent();
//			String selectQuery="";			
//			selectQuery="select orders.*,to_char(delivery_date,'dd-MM-YYYY') d_date,to_char(entry_date,'dd-MM-YYYY') e_date  from mondial.orders where order_no like '%"+getPoNumber()+"%' and flag='P'";			
//			System.out.println("selectQuery: "+ selectQuery);
//			ResultSet rs=db.retrieve(selectQuery);
//			
//			int count=1;
//			while(rs.next()){
//				
//				if(count%2==0)
//					color=color1;
//				else
//					color=color2;
//				
//				responseString+="  <tr bgcolor='"+color+"'> " +
//								"    <td align='center' height='23'>"+count+".</td>" +
//								"    <td align='left' style='padding-left:5px;'>"+rs.getString("ORDER_NO")+"</td>" +
//								"    <td align='center' >"+rs.getString("SAP_NO")+"</td> " +
//								"    <td align='center'>"+rs.getString("INSPECTOR_CODE")+"</td>" +
//								"    <td align='center'>"+rs.getString("QUANTITY")+"</td> " +
//								"    <td align='center'>"+rs.getString("E_DATE")+"</td>" +
//								"    <td align='center'>"+rs.getString("SHIPMENT_MODE")+"</td> " +
//								"    <td align='center'>"+rs.getString("ADV_ORDER")+"</td>" +
//								"    <td align='center'>"+rs.getString("D_DATE")+"</td>" +
//								"  </tr>";
//			 count++;
//			}
//			db.closeConn();
//			
//			if(count>1)
//				responseString+="</table>";
//			else
//				responseString="<img src='/MyshipmentWT/images/info.png' border='0' /><br/><b>No Inspection available with the specified criterea.</b>";
//		}catch(Exception e)
//		{
//		e.printStackTrace();
//		try{
//			db.closeConn();
//			
//		}catch(Exception ex){
//			
//		}
//		}
//		
//		inStream = new StringBufferInputStream(responseString);	
//		return "success";
//	}
//	public String getInspectionBookingForEdit(){
//		DatabaseComponent db=null;
//		inspectionList=new ArrayList<HashMap<String,String>>();
//		try{
//			
//			db=new DatabaseComponent();
//			String selectQuery="";
//			
//			selectQuery="SELECT BOOKING_NO, SALES_ORG, DIST, DIVISION, SUPPLIER_CODE, BUYING_CODE, INSPECTOR_CODE, to_char(INSPECTION_DATE, 'mm/dd/yyyy') as INSPECTION_DATE, PO_NUMBER, to_char(DELIVERY_DATE,'mm/dd/yyyy') as DELIVERY_DATE, ORDER_QTY, SHIPMENT_MODE, REMARKS, ENTRY_TIME, SAP_NO, INSPECTION_QTY, LOCATION, ADV_ORDER,PACK_METHOD,CUSTOMER_NAME,to_char(CARGO_DELIVERY_DATE,'mm/dd/yyyy') as CARGO_DELIVERY_DATE FROM BOOKING WHERE INSPECTION_DATE=TO_DATE('"+getInspectionDate()+"','mm/dd/yyyy') " +
//				//	"and SUPPLIER_CODE='"+getSupplierId()+"' " +
//							"and BOOKING_NO="+getBookingNo()+"";
//			
//			System.out.println("selectQuery: "+ selectQuery);
//			ResultSet rs=db.retrieve(selectQuery);
//			while(rs.next()){
//				HashMap<String, String> inspectionBooking=new HashMap<String, String>();
//				
//				String bookingNo=rs.getString("BOOKING_NO");
//				String poNumber=rs.getString("PO_NUMBER");
//				String sapNo=rs.getString("SAP_NO");
//				String inspectionQty=rs.getString("INSPECTION_QTY");
//				String orderQty=rs.getString("ORDER_QTY");
//				String merchanDiser=rs.getString("INSPECTOR_CODE");
//				String mode=rs.getString("SHIPMENT_MODE");
//				String location=rs.getString("LOCATION");
//				String advOrder=rs.getString("ADV_ORDER");
//				String remarks=rs.getString("REMARKS");
//				String inspectionDate=rs.getString("INSPECTION_DATE");
//				String cargoDeliveryDate=rs.getString("CARGO_DELIVERY_DATE");
//				String packMethod = rs.getString("PACK_METHOD");
//				
//				inspectionBooking.put("bookingNo", bookingNo);
//				inspectionBooking.put("poNumber", poNumber);
//				inspectionBooking.put("sapNo", sapNo);
//				inspectionBooking.put("inspectionQty", inspectionQty);
//				inspectionBooking.put("orderQty", orderQty);
//				inspectionBooking.put("merchanDiser", merchanDiser);
//				inspectionBooking.put("mode", mode);
//				inspectionBooking.put("advOrder", advOrder);
//				inspectionBooking.put("remarks", remarks);
//				inspectionBooking.put("location", location);
//				inspectionBooking.put("inspectionDate", inspectionDate);
//				inspectionBooking.put("cargoDeliveryDate", cargoDeliveryDate);
//				inspectionBooking.put("packMethod", packMethod);
//				inspectionList.add(inspectionBooking);
//				
//			}
//			request.setAttribute("inspectionList", inspectionList);
//			db.closeConn();
//			
//		}catch(Exception e)
//		{
//		e.printStackTrace();
//		try{
//			db.closeConn();
//			
//		}catch(Exception ex){
//			
//		}
//		}
//		return SUCCESS;
//	}
//	
//	public String getInspectionTrackingResult(){
//		DatabaseComponent db=null;
//		inspectionList=new ArrayList<HashMap<String,String>>();
//		insBookingList=new ArrayList<Object>();
//		try{
//			
//			db=new DatabaseComponent();
//			String selectQuery="";
//			
//			selectQuery="SELECT BOOKING_NO, SALES_ORG, DIST, DIVISION, SUPPLIER_CODE, BUYING_CODE, INSPECTOR_CODE, to_char(INSPECTION_DATE, 'dd-Mon-yyyy') as INSPECTION_DATE, PO_NUMBER, to_char(DELIVERY_DATE,'dd-Mon-yyyy') as DELIVERY_DATE, ORDER_QTY, SHIPMENT_MODE, REMARKS, ENTRY_TIME, SAP_NO, INSPECTION_QTY, LOCATION, ADV_ORDER,PACK_METHOD,CUSTOMER_NAME,CARGO_DELIVERY_DATE,SCOTA_FILE_SUBMISSION FROM BOOKING WHERE INSPECTION_DATE=TO_DATE('"+getInspectionDate()+"','mm/dd/yyyy') " +
//					"and INSPECTOR_CODE='"+getInspectorCode()+"' AND  INSPECTION_BOOKING_STATUS='P'";
//			
//			if(getInspectionDate().trim().length()==0){
//				selectQuery="SELECT BOOKING_NO, SALES_ORG, DIST, DIVISION, SUPPLIER_CODE, BUYING_CODE, INSPECTOR_CODE, to_char(INSPECTION_DATE, 'dd-Mon-yyyy') as INSPECTION_DATE, PO_NUMBER, to_char(DELIVERY_DATE,'dd-Mon-yyyy') as DELIVERY_DATE, ORDER_QTY, SHIPMENT_MODE, REMARKS, ENTRY_TIME, SAP_NO, INSPECTION_QTY, LOCATION, ADV_ORDER,PACK_METHOD,CUSTOMER_NAME,CARGO_DELIVERY_DATE,SCOTA_FILE_SUBMISSION FROM BOOKING WHERE "+
//								//INSPECTION_DATE=TO_DATE('"+getInspectionDate()+"','mm/dd/yyyy') and " +
//						" INSPECTOR_CODE='"+getInspectorCode()+"' AND  INSPECTION_BOOKING_STATUS='P'";
//				
//			}
//			System.out.println("selectQuery: "+ selectQuery);
//			ResultSet rs=db.retrieve(selectQuery);
//			while(rs.next()){
//				//HashMap<String, String> inspectionBooking=new HashMap<String, String>();
//				InspectionBookingBean insBookingBean=new InspectionBookingBean();
//				
//				String bookingNo=rs.getString("BOOKING_NO");
//				String poNumber=rs.getString("PO_NUMBER");
//				String sapNo=rs.getString("SAP_NO");
//				String inspectionQty=rs.getString("INSPECTION_QTY");
//				String orderQty=rs.getString("ORDER_QTY");
//				String merchanDiser=rs.getString("INSPECTOR_CODE");
//				String mode=rs.getString("SHIPMENT_MODE");
//				String location=rs.getString("LOCATION");
//				String advOrder=rs.getString("ADV_ORDER");
//				String remarks=rs.getString("REMARKS");
//				String inspectionDate=rs.getString("INSPECTION_DATE");
//				String deliveryDate=rs.getString("DELIVERY_DATE");
//				String entryTime = rs.getString("ENTRY_TIME");
//				String scotaFileSubmission=rs.getString("SCOTA_FILE_SUBMISSION");
//				
//				insBookingBean.setBookingNo(Double.parseDouble(bookingNo));
//				insBookingBean.setPoNumber(poNumber);
//				insBookingBean.setSapNo(sapNo);
//				insBookingBean.setInspectionQuantity(Integer.parseInt(inspectionQty));
//				insBookingBean.setQuantity(Integer.parseInt(orderQty));
//				insBookingBean.setMerchantDiser(merchanDiser);
//				insBookingBean.setMode(mode);
//				insBookingBean.setAdvOrder(advOrder);
//				insBookingBean.setRemarks(remarks);
//				insBookingBean.setLocation(location);
//				insBookingBean.setInspectionDate(inspectionDate);
//				insBookingBean.setDeliveryDate(deliveryDate);
//				insBookingBean.setEntryTime(entryTime);
//				insBookingBean.setScotaFileSubmission(scotaFileSubmission);
//				//inspectionBooking.put("poNumber", poNumber);
//				//inspectionBooking.put("sapNo", sapNo);
//				//inspectionBooking.put("inspectionQty", inspectionQty);
//				//inspectionBooking.put("orderQty", orderQty);
//				//inspectionBooking.put("merchanDiser", merchanDiser);
//				//inspectionBooking.put("mode", mode);
//				//inspectionBooking.put("advOrder", advOrder);
//				//inspectionBooking.put("remarks", remarks);
//				//inspectionBooking.put("location", location);
//				//inspectionBooking.put("inspectionDate", inspectionDate);
//				//inspectionBooking.put("deliveryDate", deliveryDate);
//				//inspectionBooking.put("entryTime", entryTime);
//				insBookingList.add(insBookingBean);
//				//inspectionList.add(inspectionBooking);
//				
//			}
//			request.setAttribute("inspectionList", inspectionList);
//			db.closeConn();
//			
//		}catch(Exception e)
//		{
//		e.printStackTrace();
//		try{
//			db.closeConn();
//			
//		}catch(Exception ex){
//			
//		}
//		}
//		inspectorCodeMap=new HashMap();
//		getInspectionUpdate();
//		return SUCCESS;
//	}
//	
//	
//	public String updateInspectionStatus(){
//		System.out.println("poNos: "+ getPoNos().length);
//		DatabaseComponent db=null;
//		String pos="";
//		String supplierName="";
//		try{
//			db=new DatabaseComponent();
//			db.setAutoCommit();
//		if(getPoNos().length>0){
//			for(int i=0;i<getPoNos().length;i++){
//				String poNo=getPoNos()[i];
//				String sapNo=getSapNos()[i];
//				String bookingNo=getBookingNos()[i];
//				String inspStatus=getInpsStatus()[i]==null?"N":getInpsStatus()[i];
//				System.out.println("inspStatus: "+inspStatus);
//				if(inspStatus.trim().equalsIgnoreCase("Y")){
//					String updateQuery="update booking set INSPECTION_BOOKING_STATUS='A' WHERE BOOKING_NO='"+bookingNo+"' AND SAP_NO='"+sapNo+"' AND PO_NUMBER='"+poNo+"'";
//					pos=poNo;
//					db.update(updateQuery);
//			
//				String selectQuery="";
//				
//				selectQuery="SELECT BOOKING_NO, SALES_ORG, DIST, DIVISION, BOOKING.SUPPLIER_CODE, BUYING_CODE, INSPECTOR_CODE, to_char(INSPECTION_DATE, 'dd-Mon-yyyy') as INSPECTION_DATE, PO_NUMBER, to_char(DELIVERY_DATE,'dd-Mon-yyyy') as DELIVERY_DATE, ORDER_QTY, SHIPMENT_MODE, REMARKS, ENTRY_TIME, SAP_NO, INSPECTION_QTY, LOCATION, ADV_ORDER,PACK_METHOD,CUSTOMER_NAME,CARGO_DELIVERY_DATE,SCOTA_FILE_SUBMISSION,MST_SUPPLIER.SUPPLIER_NAME FROM BOOKING, MST_SUPPLIER WHERE " +
//						"BOOKING_NO='"+bookingNo+"' AND SAP_NO='"+sapNo+"' AND PO_NUMBER='"+poNo+"' and BOOKING.SUPPLIER_CODE=MST_SUPPLIER.SUPPLIER_CODE";
//				
//				InspectionBookingBean insBookingBean=new InspectionBookingBean();
//				System.out.println("selectQuery: "+ selectQuery);
//				ResultSet rs=db.retrieve(selectQuery);
//				while(rs.next()){
//					//HashMap<String, String> inspectionBooking=new HashMap<String, String>();
//					
//					
//					 	bookingNo=rs.getString("BOOKING_NO");
//					String poNumber=rs.getString("PO_NUMBER");
//						sapNo=rs.getString("SAP_NO");
//					String inspectionQty=rs.getString("INSPECTION_QTY");
//					String orderQty=rs.getString("ORDER_QTY");
//					String merchanDiser=rs.getString("INSPECTOR_CODE");
//					String mode=rs.getString("SHIPMENT_MODE");
//					String location=rs.getString("LOCATION");
//					String advOrder=rs.getString("ADV_ORDER");
//					String remarks=rs.getString("REMARKS");
//					String inspectionDate=rs.getString("INSPECTION_DATE");
//					String deliveryDate=rs.getString("DELIVERY_DATE");
//					String entryTime = rs.getString("ENTRY_TIME");
//					String scotaFileSubmission=rs.getString("SCOTA_FILE_SUBMISSION");
//					supplierName=rs.getString("SUPPLIER_NAME");
//					insBookingBean.setSupplierId(rs.getString("SUPPLIER_CODE"));
//					insBookingBean.setBookingNo(Double.parseDouble(bookingNo));
//					insBookingBean.setPoNumber(poNumber);
//					insBookingBean.setSapNo(sapNo);
//					insBookingBean.setInspectionQuantity(Integer.parseInt(inspectionQty));
//					insBookingBean.setQuantity(Integer.parseInt(orderQty));
//					insBookingBean.setMerchantDiser(merchanDiser);
//					insBookingBean.setMode(mode);
//					insBookingBean.setAdvOrder(advOrder);
//					insBookingBean.setRemarks(remarks);
//					insBookingBean.setLocation(location);
//					insBookingBean.setInspectionDate(inspectionDate);
//					insBookingBean.setDeliveryDate(deliveryDate);
//					insBookingBean.setEntryTime(entryTime);
//					insBookingBean.setScotaFileSubmission(scotaFileSubmission);
//					
//					String selecQuery="select inspector_email,inspector_name from mst_merchndiser where inspector_code='"+getMerchantDiser()+"'";
//					System.out.println("merchandiser: "+getMerchantDiser());
//					System.out.println("selecQuery: "+selecQuery);
//					ResultSet rset=db.retrieve(selecQuery);
//					String emailAddresses="";
//					String inspectorName="";
//					while(rset.next()){
//						emailAddresses=rset.getString("inspector_email");
//						inspectorName=rset.getString("inspector_name");
//						
//						System.out.println("emailAddress: "+emailAddresses);
//					}
//					
//					selecQuery="select supplier_email from mst_supplier where supplier_code='"+insBookingBean.getSupplierId()+"'";
//					
//					System.out.println("selecQuery: "+selecQuery);
//					rset=db.retrieve(selecQuery);
//					String supplierEmailAddress="";
//					while(rset.next()){
//						supplierEmailAddress=rset.getString("supplier_email");
//						
//						
//						System.out.println("supplierEmailAddress: "+supplierEmailAddress);
//					}
//					if(emailAddresses.trim().length()>0 && supplierEmailAddress.trim().length()>0)
//						emailAddresses=emailAddresses+","+supplierEmailAddress;
//					else
//						emailAddresses=emailAddresses.trim()+supplierEmailAddress.trim();
//					if(emailAddresses.trim().length()>0){
//						Map hm = null;
//						  // System.out.println("Usage: ReportGenerator ....");
//						String pdfFileName ="";
//						  try {
//						   System.out.println("Start ....");
//						   // Get jasper report
//						   String path=getServletRequest().getRealPath("report");
//						   path=path+File.separator+"C_A.jpg";
//						   System.out.println("path: "+path);
//						   String jrxmlFileName =getServletRequest().getRealPath("WEB-INF/report/InspectionCertificate.jrxml");//"C:\\Tomcat5\\webapps\\MyshipmentWT\\WEB-INF\\lib\\InspectionCertificate.jrxml";
//						   String jasperFileName = getServletRequest().getRealPath("WEB-INF/report/InspectionCertificate.jasper");//"C:\\Tomcat5\\webapps\\MyshipmentWT\\WEB-INF\\lib\\InspectionCertificate.jasper";
//						   pdfFileName = getServletRequest().getRealPath("WEB-INF/report/InspectionCertificate_"+insBookingBean.getSupplierId()+".pdf"); //"C:\\Tomcat5\\webapps\\MyshipmentWT\\WEB-INF\\lib\\InspectionCertificate_"+insBookingBean.getSupplierId()+".pdf";
//						   System.out.println("jrxmlFileName: "+jrxmlFileName);
//						   System.out.println("jasperFileName: "+jasperFileName);
//						   System.out.println("pdfFileName: "+pdfFileName);
//						  // JasperCompileManager.compileReportToFile(jrxmlFileName, jasperFileName);
//						   
//						  // JasperCompileManager jcm=new Jas
//						  
//						   JasperReport report = JasperCompileManager.compileReport(jrxmlFileName);
//						   System.out.println("report.getName()"+report.getName());
//						   JasperCompileManager.compileReportToFile(jrxmlFileName, jasperFileName);
//						   
//						   JRParameter[] param=report.getParameters();
//						   for(int index=0;index<param.length;index++){
//							   System.out.println("param[0].getName()"+param[index].getName());
//						   }
//						   
//						   
//						  
//						   // JasperReport report = JasperManager.compileReport(design);
//
//
//						   // Create arguments
//						   // Map params = new HashMap();
//						   SimpleDateFormat sdf=new SimpleDateFormat("dd-MM-yyyy");
//						   Date todate=new Date();
//						   String generatedDate=sdf.format(todate);
//							
//							
//						   hm = new HashMap();
//						   hm.put("generateDate", generatedDate);
//						   hm.put("supplierName", supplierName);
//						   hm.put("inspectionDate",insBookingBean.getInspectionDate());
//						   hm.put("poNumber", insBookingBean.getPoNumber());
//						  // hm.put("noOfCartoon", "4");
//						   hm.put("OrderQty", Integer.toString(insBookingBean.getQuantity()));
//						   hm.put("offerQty", Integer.toString(insBookingBean.getInspectionQuantity()));
//						   hm.put("status", "YES");
//						   hm.put("merchantName", inspectorName);
//						   hm.put("imgLocation", "C_A.jpg");
//						   
//						  
//						   // Generate jasper print
//						   JasperPrint jprint =  JasperFillManager.fillReport(jasperFileName, hm, new JREmptyDataSource());
//
//						   // Export pdf file
//						   JasperExportManager.exportReportToPdfFile(jprint, pdfFileName);
//						   
//						   System.out.println("Done exporting reports to pdf");
//						   
//						  } catch (Exception e) {
//							  e.printStackTrace();
//						   System.out.print("Exceptiion" + e);
//						  }
//						SendMail sendMail=new SendMail();
//						sendMail.javaSendInspectionConfirmationMailWithAttachent("itsupport@mghgroup.com", emailAddresses, "", "INSPECTION BOOKING APPROVED", "Dear Sir,<br></br>Inspection Booking Approved for Following PO<br></br>"+pos.toString()+"<br></br><br></br>NB: This is System Generated Email, Please don't Reply<br></br>Thanks",pdfFileName);
//						sendMail=null;
//						File pdfFile=new File(pdfFileName);
//						if(pdfFile.exists())
//							pdfFile.delete();
//					}
//
//				}
//			}
//			}
//			db.commitTrans();
//		}
//		db.closeConn();
//		addActionMessage("Inspection Status Updated Successfully");
//		}catch(Exception ex){
//			ex.printStackTrace();
//			addActionError("Inspection Status Updated Failed");
//			try{
//				
//				db.rollbackTRans();
//				db.closeConn();
//				
//				
//			}catch(Exception exx){
//				exx.printStackTrace();
//			}
//			
//		}
//		
//		getInspectionUpdate();
//		return SUCCESS;
//	}
//	
//	
//	public String doInspectionStatusTrackingResult(){
//		DatabaseComponent db=null;
//		
//		insTrackingStatusList=new ArrayList<Object>();
//		try{
//			
//			db=new DatabaseComponent();
//			String selectQuery="";
//			if(getInspectionDateFrom().trim().length()>0 && getInspectionDateTo().trim().length()>0)
//			{
//				selectQuery=" SELECT VC_RECIEPT_NO,"+
//								" VC_ORDER_NO,"+
//								" DT_RECIEVE_DATE,"+
//								" SUPPLIER_NAME,"+
//								" VC_MODE,"+
//								" VC_ADVERT,"+
//								" VC_INSPECTOR_CODE,"+
//								" TOTAL_CARTON,"+
//								" TOTAL_PIECES,"+
//								" STATUS,"+
//								" VC_LOC_CODE,"+
//								" INSPECTION_DATE"+
//								" FROM INSPECTION_STATUS"+
//								" WHERE  INSPECTION_DATE between TO_DATE('"+getInspectionDateFrom()+"','mm/dd/yyyy') and TO_DATE('"+getInspectionDateTo()+"','mm/dd/yyyy') "+
//								" and VC_ORDER_NO like '%"+getOrderNo()+"%' "+
//								" order by "+
//								" VC_INSPECTOR_CODE,INSPECTION_DATE"	;
//			
//			}else{
//				selectQuery=" SELECT VC_RECIEPT_NO,"+
//						" VC_ORDER_NO,"+
//						" DT_RECIEVE_DATE,"+
//						" SUPPLIER_NAME,"+
//						" VC_MODE,"+
//						" VC_ADVERT,"+
//						" VC_INSPECTOR_CODE,"+
//						" TOTAL_CARTON,"+
//						" TOTAL_PIECES,"+
//						" STATUS,"+
//						" VC_LOC_CODE,"+
//						" INSPECTION_DATE"+
//						" FROM INSPECTION_STATUS"+
//						//" WHERE  INSPECTION_DATE between TO_DATE('"+getInspectionDate()+"','mm/dd/yyyy') and TO_DATE('"+getInspectionDate()+"','mm/dd/yyyy') "+
//						" WHERE VC_ORDER_NO like '%"+getOrderNo()+"%' "+
//						" order by "+
//						" VC_INSPECTOR_CODE,INSPECTION_DATE"	;
//			
//				
//			}
//			//if(getType().equalsIgnoreCase("supplier")){
//			//selectQuery="SELECT BOOKING_NO, SALES_ORG, DIST, DIVISION, SUPPLIER_CODE, BUYING_CODE, INSPECTOR_CODE, to_char(INSPECTION_DATE, 'dd-Mon-yyyy') as INSPECTION_DATE, PO_NUMBER, to_char(DELIVERY_DATE,'dd-Mon-yyyy') as DELIVERY_DATE, ORDER_QTY, SHIPMENT_MODE, REMARKS, ENTRY_TIME, SAP_NO, INSPECTION_QTY, LOCATION, ADV_ORDER,PACK_METHOD,CUSTOMER_NAME,CARGO_DELIVERY_DATE FROM BOOKING WHERE INSPECTION_DATE=TO_DATE('"+getInspectionDate()+"','mm/dd/yyyy') " +
//				//	"and SUPPLIER_CODE='"+getSupplierId()+"'";
//			
//		//	}else{
//			//	selectQuery="SELECT BOOKING_NO, SALES_ORG, DIST, DIVISION, SUPPLIER_CODE, BUYING_CODE, INSPECTOR_CODE, to_char(INSPECTION_DATE, 'dd-Mon-yyyy') as INSPECTION_DATE, PO_NUMBER, to_char(DELIVERY_DATE,'dd-Mon-yyyy') as DELIVERY_DATE, ORDER_QTY, SHIPMENT_MODE, REMARKS, ENTRY_TIME, SAP_NO, INSPECTION_QTY, LOCATION, ADV_ORDER,PACK_METHOD,CUSTOMER_NAME,CARGO_DELIVERY_DATE FROM BOOKING WHERE INSPECTION_DATE=TO_DATE('"+getInspectionDate()+"','mm/dd/yyyy') and BUYING_CODE='"+getSupplierId()+"'";
//					
//			//}
//			System.out.println("selectQuery: "+ selectQuery);
//			ResultSet rs=db.retrieve(selectQuery);
//			while(rs.next()){
//				
//				InspectionStatusTrackingBean insStatusBean=new InspectionStatusTrackingBean();
//				
//				String receiptNo=rs.getString("VC_RECIEPT_NO");
//				String orderNo=rs.getString("VC_ORDER_NO");
//				String receiveDate=rs.getString("DT_RECIEVE_DATE");
//				String supplierName=rs.getString("SUPPLIER_NAME");
//				String mode=rs.getString("VC_MODE");
//				String advert=rs.getString("VC_ADVERT");
//				String inspectorCode=rs.getString("VC_INSPECTOR_CODE");
//				String totalCartoon=rs.getString("TOTAL_CARTON");
//				String totalPieces=rs.getString("TOTAL_PIECES");
//				String status=rs.getString("STATUS");
//				String locCode=rs.getString("VC_LOC_CODE");
//				String inspectionDate=rs.getString("INSPECTION_DATE");
//			
//				
//				
//				
//				
//				insStatusBean.setOrderNo(orderNo);
//				insStatusBean.setReceiptNo(receiptNo);
//				insStatusBean.setReceiveDate(receiveDate);
//				insStatusBean.setSupplierName(supplierName);
//				insStatusBean.setMode(mode);
//				insStatusBean.setAdvOrder(advert);
//				insStatusBean.setInspectionCode(inspectorCode);
//				insStatusBean.setTotalCartoon(totalCartoon);
//				insStatusBean.setTotalPieces(totalPieces);
//				insStatusBean.setStatus(status);
//				insStatusBean.setLocation(locCode);
//				insStatusBean.setInspectionDate(inspectionDate);
//				insTrackingStatusList.add(insStatusBean);
//				
//			}
//			request.setAttribute("insTrackingStatusList", insTrackingStatusList);
//			db.closeConn();
//			
//		}catch(Exception e)
//		{
//		e.printStackTrace();
//		try{
//			db.closeConn();
//			
//		}catch(Exception ex){
//			
//		}
//		}
//		return SUCCESS;
//	}
	
	//inspection tracking
	@RequestMapping(value = "/getInspectionTrackingDetail", method = RequestMethod.POST)
	public String getInspectionTrackingDetail(@ModelAttribute("insTrackingParams") InspectionTrackingParams insTrackingParams, Model model, HttpServletRequest request){

		logger.info("Method InspectionBooking.getInspectionTrackingDetail starts.");
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO)session.getAttribute(SessionUtil.LOGIN_DETAILS);
		String selectQuery = "";
		
		//System.out.println(loginDTO.getAccgroup());
		SimpleDateFormat formatter = null;
		Date date = null;
		String queryDate = "";
		DatabaseComponent ds = null;
		InspectionTrackingResultBean insTrackResBean;
		List<InspectionTrackingResultBean>  insTrackingResultBeanLst = new ArrayList<InspectionTrackingResultBean>();
		//PoTrackingJsonOutputData poTrackingJsonOutputData = new PoTrackingJsonOutputData();
		try{
			/*formatter = new SimpleDateFormat("dd-MM-yyyy");
			//System.out.println(insTrackingParams.getFromDate());
			try {
				date = formatter.parse(insTrackingParams.getFromDate());
				formatter = new SimpleDateFormat("dd-MMM-yy");
				queryDate = formatter.format(date);
				//System.out.println(queryDate);
			}
			catch(ParseException e){
				e.printStackTrace();
			}*/
			
			queryDate = insTrackingParams.getFromDate();
			//List<String> lstPortList = new ArrayList<String>();
			if(loginDTO.getAccgroup().equals("ZMSP")||loginDTO.getAccgroup().equals("ZMFF")){
				//selectQuery = "SELECT * FROM BOOKING WHERE INSPECTION_DATE='"+queryDate+"'";
				selectQuery = "SELECT * FROM BOOKING WHERE SUPPLIER_CODE='"+loginDTO.getLoggedInUserName()+"' AND SALES_ORG='"+loginDTO.getSalesOrgSelected()+"' AND INSPECTION_DATE='"+queryDate+"'";
				System.out.println("select Query: " + selectQuery);
			}
			if(loginDTO.getAccgroup().equals("ZMBY")||loginDTO.getAccgroup().equals("ZMBH")){
				selectQuery = "SELECT * FROM BOOKING WHERE INSPECTION_DATE='"+queryDate+"'";
				System.out.println("select Query: " + selectQuery);
			}
			try {
				// POAutoCompleter db = new POAutoCompleter();
				ds = new DatabaseComponent();
				//String selectQuery = "SELECT * FROM BOOKING WHERE INSPECTION_DATE='"+queryDate+"'";
				//System.out.println("select Query: " + selectQuery);
				ResultSet rs = ds.retrieve(selectQuery);

				while (rs.next()) {
					//System.out.println(rs.getString("PO_NUMBER"));
					insTrackResBean = new InspectionTrackingResultBean();
					insTrackResBean.setPO_NUMBER(rs.getString("PO_NUMBER"));
					insTrackResBean.setSAP_NO(rs.getString("SAP_NO"));
					insTrackResBean.setINSPECTION_QTY(rs.getInt("INSPECTION_QTY"));
					insTrackResBean.setORDER_QTY(rs.getInt("ORDER_QTY"));
					insTrackResBean.setINSPECTOR_CODE(rs.getString("INSPECTOR_CODE"));
					insTrackResBean.setSHIPMENT_MODE(rs.getString("SHIPMENT_MODE"));
					insTrackResBean.setLOCATION(rs.getString("LOCATION"));
					insTrackResBean.setADV_ORDER(rs.getString("ADV_ORDER"));
					insTrackResBean.setREMARKS(rs.getString("REMARKS"));
					insTrackResBean.setINSPECTION_DATE(rs.getDate("INSPECTION_DATE"));
					insTrackResBean.setDELIVERY_DATE(rs.getDate("DELIVERY_DATE"));
					insTrackResBean.setENTRY_TIME(rs.getTimestamp("ENTRY_TIME"));
					insTrackingResultBeanLst.add(insTrackResBean);
				}
					ds.closeConn();
				
			} catch (Exception ex) {
				try{
					ds.closeConn();
				}
				catch(Exception e){
					e.printStackTrace();
				}
				ex.printStackTrace();
			}
			if(insTrackingResultBeanLst != null && insTrackingResultBeanLst.size() > 0){
				model.addAttribute("insTrackingResultBeanLst", insTrackingResultBeanLst);
				model.addAttribute("insTrackingParams",insTrackingParams);
				return "inspectionTracking";
			}
			else{
				logger.info("poTrackingResultBeanLst size is either null or zero. " );
				model.addAttribute("insTrackingParams",insTrackingParams);
				
				return "inspectionTracking";
			}
			
			
			
/*			if(poTrackingParams != null){
				
				LoginDTO loginDto = (LoginDTO)request.getSession().getAttribute("loginDetails");
				if(loginDto.getAccgroup().equals("ZMSP")||loginDto.getAccgroup().equals("ZMFF")){
					poTrackingParams.setShipper_no(loginDto.getLoggedInUserName());
					poTrackingParams.setBuyer_no("");
				}
				if(loginDto.getAccgroup().equals("ZMBY")||loginDto.getAccgroup().equals("ZMBH")){
					poTrackingParams.setBuyer_no(loginDto.getLoggedInUserName());
					poTrackingParams.setShipper_no("");
				}
				if(loginDto.getAccgroup().equals("ZMDA")){
					poTrackingParams.setBuyer_no("1010034827");
					poTrackingParams.setShipper_no("");
				}
				
				if(poTrackingParams.getStatusType().equalsIgnoreCase(CommonConstant.STATUS_TYPE_NONE)){
					poTrackingParams.setStatusType("");
				}
				poTrackingParams.setDivision(loginDto.getDivisionSelected());
				
				StringBuffer webServiceUrl = new StringBuffer(RestUtil.PO_TRACKING_INFO);

				//poTrackingJsonOutputData = restService.postForObject(RestUtil.prepareUrlForService(webServiceUrl).toString(), poTrackingParams, PoTrackingJsonOutputData.class);

				List<PoTrackingResultBean>  poTrackingResultBeanLst = poTrackingJsonOutputData.getPoTrackingResultData();
				
				//poTrackingResultBeanLst=getPoTrackingStatus(poTrackingResultBeanLst);
				
				if(poTrackingResultBeanLst != null && poTrackingResultBeanLst.size() > 0){
					model.addAttribute("poTrackingResultBeanLst", poTrackingResultBeanLst);
					model.addAttribute("poTrackingParams",poTrackingParams);
					return "poTracking";
				}
				else{
					logger.info("poTrackingResultBeanLst size is either null or zero. " );
					model.addAttribute("poTrackingParams",poTrackingParams);
					
					return "poTracking";
				}
			}*/
		}catch(Exception e){
			e.printStackTrace();
			logger.info("Some exception occurred : "+ e);
		}

		logger.info("Method PoTrackingController.getPoTrackingDetail ends.");
		
		return "inspectionTracking";
	}


	@RequestMapping(value = "/getInspectionTrackingPage", method = RequestMethod.GET)
	public String getInspectionTrackingPage(Model model){

		try{
			InspectionTrackingParams insTrackingParams = new InspectionTrackingParams();

			model.addAttribute("insTrackingParams", insTrackingParams);
		}catch(Exception e){
			e.printStackTrace();
			logger.debug("Some exception occurred : ", e);
		}
		return "inspectionTracking";
	}
	
	
	
	
//	public String getInspectionTrackingStatusPage(){
//		return SUCCESS;
//	}
	public void setSupplierId(String supplierId){
		this.supplierId=supplierId;
	}
	public String getSupplierId(){
		return supplierId;
	}
	public void setBuyingHouse(String buyingHouse){
		this.buyingHouse=buyingHouse;
	}
	public String getBuyingHouse(){
		return buyingHouse;
	}
	public void setPoNumber(String poNumber){
		this.poNumber=poNumber;
	}
	public String getPoNumber(){
		return poNumber;
	}
	public void setInspectionDate(String inspectionDate){
		this.inspectionDate=inspectionDate;
	}
	public String getInspectionDate(){
		return inspectionDate;
	}
	public void setInspectionQuantity(int inspectionQuantity){
		this.inspectionQuantity=inspectionQuantity;
	}
	public int getInspectionQuantity(){
		return inspectionQuantity;
	}
	public void setPackMethod(String packMethod){
		this.packMethod=packMethod;
	}
	public String getPackMethod(){
		return packMethod;
	}
	public void setCargoDeliveryDate(String cargoDeliveryDate){
		this.cargoDeliveryDate=cargoDeliveryDate;
	}
	public String getCargoDeliveryDate(){
		return cargoDeliveryDate;
	}
	public void setLocation(String location){
		this.location=location;
	}
	public String getLocation(){
		return location;
	}
	public void setCustomerName(String customerName){
		this.customerName=customerName;
	}
	public String getCustomerName(){
		return customerName;
	}
	public void setRemarks(String remarks){
		this.remarks=remarks;
	}
	public String getRemarks(){
		return remarks;
	}public void setDeliveryDate(String deliveryDate){
		this.deliveryDate=deliveryDate;
	}
	public String getDeliveryDate(){
		return deliveryDate;
	}
	public void setQuantity(int quantity){
		this.quantity=quantity;
	}
	public int getQuantity(){
		return quantity;
	}
	public void setSapNo(String sapNo){
		this.sapNo=sapNo;
	}
	public String getSapNo(){
		return sapNo;
	}
	public void setMerchantDiser(String merchantDiser){
		this.merchantDiser=merchantDiser;
	}
	public String getMerchantDiser(){
		return merchantDiser;
	}
	public void setAdvOrder(String advOrder){
		this.advOrder=advOrder;
	}
	public String getAdvOrder(){
		return advOrder;
	}
	
	
	public void setSalesOrg(String salesOrg){
		this.salesOrg=salesOrg;
	}
	public String getSalesOrg(){
		return salesOrg;
	}
	
	public void setDivision(String division){
		this.division=division;
	}
	public String getDivision(){
		return division;
	}
	
	public void setDistChannel(String distChannel){
		this.distChannel=distChannel;
	}
	public String getDistChannel(){
		return distChannel;
	}
	

	public void setMode(String mode){
		this.mode=mode;
	}
	public String getMode(){
		return mode;
	}
	
	public void setStatus(String status){
		this.status=status;
	}
	public String getStatus(){
		return status;
	}
	public void setInspectionList(List<HashMap<String, String>> inspectionList){
		this.inspectionList=inspectionList;
	}
	public List<HashMap<String, String>> getInspectionList(){
		return inspectionList;
	}
	
	
	public void setInsBookingList(List<Object> insBookingList){
		this.insBookingList=insBookingList;
	}
	public List<Object> getInsBookingList(){
		return insBookingList;
	}
	
	
	public void setInspectorCodeList(List<Object> inspectorCodeList){
		this.inspectorCodeList=inspectorCodeList;
	}
	public List<Object> getInspectorCodeList(){
		return inspectorCodeList;
	}
	public HttpServletRequest getServletRequest() {
        return request;
    }
 
    public void setServletRequest(HttpServletRequest req){
        this.request = req;
    }
 
    public HttpServletResponse getServletResponse() {
        return response;
    }
 
    public void setServletResponse(HttpServletResponse resp) {
        this.response = resp;
    }
    
    public void setType(String type){
		this.type=type;
	}
	public String getType(){
		return type;
	}
	public void setBookingNo(double bookingNo){
		this.bookingNo=bookingNo;
	}
	public double getBookingNo(){
		return bookingNo;
	}
	public String getEmailList() {
		return emailList;
	}
	public void setEmailList(String emailList) {
		this.emailList = emailList;
	}

	public InputStream getInStream() {
		return inStream;
	}
	public void setInStream(InputStream inStream) {
		this.inStream = inStream;
	}



	public String getScotaFileSubmission() {
		return scotaFileSubmission;
	}



	public void setScotaFileSubmission(String scotaFileSubmission) {
		this.scotaFileSubmission = scotaFileSubmission;
	}



	public String getScotaFileSubmissionDate() {
		return scotaFileSubmissionDate;
	}



	public void setScotaFileSubmissionDate(String scotaFileSubmissionDate) {
		this.scotaFileSubmissionDate = scotaFileSubmissionDate;
	}
	
	public String[] getPoNos() {
		return poNos;
	}
	public void setPoNos(String[] poNos) {
		this.poNos = poNos;
	}
	public String[] getSapNos() {
		return sapNos;
	}
	public void setSapNos(String[] sapNos) {
		this.sapNos = sapNos;
	}
	public String[] getInpsStatus() {
		return inpsStatus;
	}
	public void setInpsStatus(String[] inpsStatus) {
		this.inpsStatus = inpsStatus;
	}
/*	public HashMap getInspectorCodeMap() {
		return inspectorCodeMap;
	}
	public void setInspectorCodeMap(HashMap inspectorCodeMap) {
		this.inspectorCodeMap = inspectorCodeMap;
	}*/
	public String getInspectorCode() {
		return inspectorCode;
	}
	public void setInspectorCode(String inspectorCode) {
		this.inspectorCode = inspectorCode;
	}
	public String[] getBookingNos() {
		return bookingNos;
	}
	public void setBookingNos(String[] bookingNos) {
		this.bookingNos = bookingNos;
	}
	public String getReceiveDateFrom() {
		return receiveDateFrom;
	}
	public void setReceiveDateFrom(String receiveDateFrom) {
		this.receiveDateFrom = receiveDateFrom;
	}
	public String getReceiveDateTo() {
		return receiveDateTo;
	}
	public void setReceiveDateTo(String receiveDateTo) {
		this.receiveDateTo = receiveDateTo;
	}
	public String getOrderNo() {
		return orderNo;
	}
	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}
	public File getPackingList() {
		return packingList;
	}
	public void setPackingList(File packingList) {
		this.packingList = packingList;
	}
	public String getPackingListContentType() {
		return packingListContentType;
	}
	public void setPackingListContentType(String packingListContentType) {
		this.packingListContentType = packingListContentType;
	}
	public String getPackingListFileName() {
		return packingListFileName;
	}
	public void setPackingListFileName(String packingListFileName) {
		this.packingListFileName = packingListFileName;
	}
	public String getInspectionDateFrom() {
		return inspectionDateFrom;
	}
	public void setInspectionDateFrom(String inspectionDateFrom) {
		this.inspectionDateFrom = inspectionDateFrom;
	}
	public String getInspectionDateTo() {
		return inspectionDateTo;
	}
	public void setInspectionDateTo(String inspectionDateTo) {
		this.inspectionDateTo = inspectionDateTo;
	}
	
}
