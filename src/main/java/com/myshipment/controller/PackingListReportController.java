package com.myshipment.controller;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.myshipment.dto.LoginDTO;
import com.myshipment.model.PackListParams;
import com.myshipment.model.PackingListReportOutputDto;
import com.myshipment.service.IPackingListReport;
/*
 * @Mohammad Salahuddin
 */
@Controller
public class PackingListReportController extends BaseController{
	
	private static final long serialVersionUID = 1L;
	final static Logger logger = Logger.getLogger(PackingListReportController.class);
	
	@Autowired
	private IPackingListReport packingListReportService;
	
	@RequestMapping(value = "/getPackingListReport", method = RequestMethod.POST)
	public String getPackingListReport(@ModelAttribute("packingListReportParam") PackListParams packListParams, Model model, HttpSession session)
	{
		logger.info("Method PackingListReportController.getPackingListReport starts.");
		LoginDTO loginDto = (LoginDTO)session.getAttribute("loginDetails");
		String customerNo = null;
		if(loginDto != null){
			customerNo = loginDto.getLoggedInUserName();
			logger.info("Customer No found as : "+ customerNo);
		}
		if(packListParams != null)
		{
			
			PackingListReportOutputDto packingListReportOutputDto=packingListReportService.getPackingListReportDataService(packListParams);
			
			
			
			//TODO: do your packing
			
			model.addAttribute("packingListData", packingListReportOutputDto);
			
			//model.addAttribute("itemDataLst", itemDataLst);
			
			logger.info("Method CommercialInvoiceController.getCommercialInvoice ends.");
			
			return "packinglistreport";
		}

		return null;
	}
	
	@RequestMapping(value = "/getPackingListReportPage", method = RequestMethod.GET)
	public String getPackingListReportPage(ModelMap model)
	{
		logger.info("Method PackingListReportController.getPackingListReportPage starts.");
		
		PackListParams packingListReportParam = new PackListParams();
		model.addAttribute("packingListReportParam", packingListReportParam);
		
		logger.info("Method PackingListReportController.getPackingListReportPage ends.");
		
		return "packinglistreportinput";
	}
}
