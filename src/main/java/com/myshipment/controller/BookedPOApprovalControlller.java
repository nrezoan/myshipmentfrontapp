package com.myshipment.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.apache.log4j.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.myshipment.dto.LoginDTO;
import com.myshipment.model.BookedPOApproval;
import com.myshipment.model.BookedPOApprovalBean;
import com.myshipment.model.BuSupListBean;
import com.myshipment.model.MyshipmentTrackContainerBean;
import com.myshipment.model.MyshipmentTrackHeaderBean;
import com.myshipment.model.MyshipmentTrackItemBean;
import com.myshipment.model.MyshipmentTrackItemBeanDto;
import com.myshipment.model.MyshipmentTrackJsonOutputData;
import com.myshipment.model.MyshipmentTrackParams;
import com.myshipment.model.MyshipmentTrackScheduleBean;
import com.myshipment.model.SAPPOSearchParams;
import com.myshipment.model.SupRequestParams;
import com.myshipment.model.SupplierPreLoadJsonData;
import com.myshipment.service.IBookedPOApprovalService;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;
import com.myshipment.util.SessionUtil;

/*
 * @Hamid
 */
@Controller
public class BookedPOApprovalControlller extends BaseController{
	
	private static final long serialVersionUID = 1988302078474267886L;
	final static Logger logger = Logger.getLogger(BookedPOApprovalControlller.class);
	
	@Autowired
	private RestService restService;
	@Autowired
	private IBookedPOApprovalService bookedPOApprovalService;
		
	@RequestMapping(value = "/approvebookedpo", method = RequestMethod.GET)
	public String getApprovedBookedPoPage(ModelMap model, HttpServletRequest request) {

		logger.info("Method MyshipmentTrackController.getTrackShipmentByDate starts");
		LoginDTO loginDto = (LoginDTO) request.getSession().getAttribute("loginDetails");
		Map<String, String> buyersList = new LinkedHashMap<String, String>();
		buyersList = getBuyersList(loginDto);
		
		//MyshipmentTrackParams myshipmentTrackParams = new MyshipmentTrackParams();
		SAPPOSearchParams sapposearchparams = new SAPPOSearchParams();
		model.addAttribute("sapposearchparams", sapposearchparams);
		model.addAttribute("buyersList", buyersList);
		
		logger.info("Method MyshipmentTrackController.getTrackShipmentByDate ends");

		return "myshipmentTrackByDatePage";
	}
	
	@RequestMapping(value = "/approvebookedporeport", method = RequestMethod.GET)
	public String getApprovedBookedPoReportPage(ModelMap model, HttpServletRequest request) {

		logger.info("Method MyshipmentTrackController.getApprovedBookedPoReportPage starts");
		LoginDTO loginDto = (LoginDTO) request.getSession().getAttribute("loginDetails");
		Map<String, String> buyersList = new LinkedHashMap<String, String>();
		buyersList = getBuyersList(loginDto);
		
		//MyshipmentTrackParams myshipmentTrackParams = new MyshipmentTrackParams();
		SAPPOSearchParams sapposearchparams = new SAPPOSearchParams();
		model.addAttribute("sapposearchparams", sapposearchparams);
		model.addAttribute("buyersList", buyersList);
		
		logger.info("Method MyshipmentTrackController.getApprovedBookedPoReportPage ends");

		return "bookedPOApprovalReport";
	}
	
	@RequestMapping(value = "/approvebookedporeportdetail", method = RequestMethod.POST)
	public String showApprovedBookedPoReport(@ModelAttribute("sapposearchparams") SAPPOSearchParams sapposearchparams, Model model, HttpServletRequest request) {

		logger.info("Method MyshipmentTrackController.showApprovedBookedPoReport starts");		
		List<BookedPOApproval> listApprovedBookedPoByStatus = new ArrayList<BookedPOApproval>(); 
		if((sapposearchparams.getStatus().equalsIgnoreCase("APPROVED")) || (sapposearchparams.getStatus().equalsIgnoreCase("REJECTED"))) {			
			try{
				listApprovedBookedPoByStatus = bookedPOApprovalService.getBookedPoApprovalDataByStatus((convertStringToDate(sapposearchparams.getFromDate())), (convertStringToDate(sapposearchparams.getToDate())),sapposearchparams.getStatus(), sapposearchparams.getBuyer());
			}
			catch(Exception ex) {
				ex.printStackTrace();
			}
			LoginDTO loginDto = (LoginDTO) request.getSession().getAttribute("loginDetails");
			model.addAttribute("myshipTrackItemListOracle", listApprovedBookedPoByStatus);
			model.addAttribute("sapposearchparams", sapposearchparams);					
			model.addAttribute("companyCode", loginDto.getSalesOrgSelected());
			model.addAttribute("loggedinuser", loginDto.getLoggedInUserName());
			Map<String, String> buyersList = new LinkedHashMap<String, String>();
			buyersList = getBuyersList(loginDto);
			model.addAttribute("buyersList", buyersList);
			
			logger.info("Method MyshipmentTrackController.showShipmentDetailDataByDate (Status:Approved/Rejected) ends");
			return "bookedPOApprovalReport";
		}
		LoginDTO loginDto = (LoginDTO) request.getSession().getAttribute("loginDetails");
		model.addAttribute("myshipTrackItemListOracle", listApprovedBookedPoByStatus);
		model.addAttribute("sapposearchparams", sapposearchparams);					
		model.addAttribute("companyCode", loginDto.getSalesOrgSelected());
		model.addAttribute("loggedinuser", loginDto.getLoggedInUserName());
		Map<String, String> buyersList = new LinkedHashMap<String, String>();
		buyersList = getBuyersList(loginDto);
		model.addAttribute("buyersList", buyersList);
		
		logger.info("Method MyshipmentTrackController.showShipmentDetailDataByDate (Status:Approved/Rejected) ends");
		return "bookedPOApprovalReport";
	}
	
	@RequestMapping(value = "/shipmentdetaildatabydate", method = RequestMethod.POST)
	public String showShipmentDetailDataByDate(@ModelAttribute("sapposearchparams") SAPPOSearchParams sapposearchparams, Model model, HttpServletRequest request) {

		logger.info("Method MyshipmentTrackController.showShipmentDetailDataByDate starts");		
		
		if((sapposearchparams.getStatus().equalsIgnoreCase("APPROVED")) || (sapposearchparams.getStatus().equalsIgnoreCase("REJECTED"))) {
			List<BookedPOApproval> listApprovedBookedPoByStatus = new ArrayList<BookedPOApproval>(); 
			try{
				listApprovedBookedPoByStatus = bookedPOApprovalService.getBookedPoApprovalDataByStatus((convertStringToDate(sapposearchparams.getFromDate())), (convertStringToDate(sapposearchparams.getToDate())),sapposearchparams.getStatus(), sapposearchparams.getBuyer());
			}
			catch(Exception ex) {
				ex.printStackTrace();
			}
			LoginDTO loginDto = (LoginDTO) request.getSession().getAttribute("loginDetails");
			model.addAttribute("myshipTrackItemListOracle", listApprovedBookedPoByStatus);
			model.addAttribute("sapposearchparams", sapposearchparams);					
			model.addAttribute("companyCode", loginDto.getSalesOrgSelected());
			model.addAttribute("loggedinuser", loginDto.getLoggedInUserName());
			Map<String, String> buyersList = new LinkedHashMap<String, String>();
			buyersList = getBuyersList(loginDto);
			model.addAttribute("buyersList", buyersList);
			
			logger.info("Method MyshipmentTrackController.showShipmentDetailDataByDate (Status:Approved/Rejected) ends");
			return "myshipmentTrackByDatePage";
		}else {
			MyshipmentTrackJsonOutputData myshipmentTrackJsonOutputData;
			MyshipmentTrackParams myshipmentTrackParams;
			if (sapposearchparams != null) {
				myshipmentTrackJsonOutputData = new MyshipmentTrackJsonOutputData();
				myshipmentTrackParams = new MyshipmentTrackParams();
				LoginDTO loginDto = (LoginDTO) request.getSession().getAttribute("loginDetails");
				myshipmentTrackParams.setDivision(loginDto.getDivisionSelected());
				myshipmentTrackParams.setSalesOrg(loginDto.getSalesOrgSelected());
				myshipmentTrackParams.setDistChan(loginDto.getDisChnlSelected());
				myshipmentTrackParams.setCustomer(sapposearchparams.getBuyer());
				//myshipmentTrackParams.setErdat1(sapposearchparams.getFromDate());
				//myshipmentTrackParams.setErdat2(sapposearchparams.getToDate());				
				try {
					myshipmentTrackParams.setErdat1(new SimpleDateFormat("dd.MM.yyyy").parse(sapposearchparams.getFromDate()));
					myshipmentTrackParams.setErdat2(new SimpleDateFormat("dd.MM.yyyy").parse(sapposearchparams.getToDate()));
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				StringBuffer webServiceUrl = new StringBuffer(RestUtil.MYSHIPMENT_COMPLETE_TRACK);
				try {
					myshipmentTrackJsonOutputData = restService.postForObject(
							RestUtil.prepareUrlForService(webServiceUrl).toString(), myshipmentTrackParams,
							MyshipmentTrackJsonOutputData.class);
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				if (myshipmentTrackJsonOutputData.getDialogue().equalsIgnoreCase("FAILURE")) {
					logger.info("myshipmentTrackJsonOutputData was returned null, MyshipmentTrackController.getMyshipmentTrackDetail Method");
					model.addAttribute("errorDialogue", myshipmentTrackJsonOutputData.getDialogue());
					Map<String, String> buyersList = new LinkedHashMap<String, String>();
					buyersList = getBuyersList(loginDto);
					model.addAttribute("buyersList", buyersList);
					return "myshipmentTrackByDatePage";
				} 
				else if (myshipmentTrackJsonOutputData.getDialogue().equalsIgnoreCase("SUCCESS")) {
					// initialization
					List<MyshipmentTrackHeaderBean> myshipTrackHeaderList = new ArrayList<MyshipmentTrackHeaderBean>();
					List<MyshipmentTrackItemBean> myshipTrackItemList = new ArrayList<MyshipmentTrackItemBean>();
					List<MyshipmentTrackContainerBean> myshipTrackContainerList = new ArrayList<MyshipmentTrackContainerBean>();
					List<MyshipmentTrackScheduleBean> myshipTrackScheduleList = new ArrayList<MyshipmentTrackScheduleBean>();
					if (myshipmentTrackJsonOutputData != null) {
						if (myshipmentTrackJsonOutputData.getMyshipmentTrackHeader() != null) {
							myshipTrackHeaderList = myshipmentTrackJsonOutputData.getMyshipmentTrackHeader();
						}
						if (myshipmentTrackJsonOutputData.getMyshipmentTrackItem() != null) {
							myshipTrackItemList = myshipmentTrackJsonOutputData.getMyshipmentTrackItem();
						}
						if (myshipmentTrackJsonOutputData.getMyshipmentTrackContainer() != null) {
							myshipTrackContainerList = myshipmentTrackJsonOutputData.getMyshipmentTrackContainer();
						}
						if (myshipmentTrackJsonOutputData.getMyshipmentTrackSchedule() != null) {
							myshipTrackScheduleList = myshipmentTrackJsonOutputData.getMyshipmentTrackSchedule();
						}
						
						//add so wise new fields
						//i am here
						List<MyshipmentTrackItemBeanDto> myshipTrackItemDtoList = addHeaderItemstoItemBean(myshipTrackHeaderList, myshipTrackItemList);
						
						List<BookedPOApproval> listBookedPoApproval = new ArrayList<BookedPOApproval>(); 
						try{
							listBookedPoApproval = bookedPOApprovalService.getBookedPoApprovalDataByDate((convertStringToDate(sapposearchparams.getFromDate())), (convertStringToDate(sapposearchparams.getToDate())), sapposearchparams.getBuyer());
						}						
						catch(Exception ex) {
							ex.printStackTrace();
						}
						if(listBookedPoApproval.size() > 0) {
							//List<MyshipmentTrackItemBean> myshipTrackItemListOld = myshipTrackItemList;
							//List<MyshipmentTrackItemBean> myshipTrackItemListDuplicatesRemoved = removeDuplicatePOItems(myshipTrackItemList, listBookedPoApproval);
							//List<MyshipmentTrackItemBeanDto> myshipTrackItemDtoListOld = myshipTrackItemDtoList;
							List<MyshipmentTrackItemBeanDto> myshipTrackItemListDuplicatesRemoved = removeDuplicatePOItems(myshipTrackItemDtoList, listBookedPoApproval);							
							//System.out.println(myshipTrackItemListDuplicatesRemoved.size());
							myshipTrackItemDtoList = myshipTrackItemListDuplicatesRemoved;
						}
						model.addAttribute("myshipmentTrackParams", myshipmentTrackParams);
						model.addAttribute("myshipTrackHeaderList", myshipTrackHeaderList);
						//model.addAttribute("myshipTrackItemList", myshipTrackItemList);
						model.addAttribute("myshipTrackItemList", myshipTrackItemDtoList);
						model.addAttribute("myshipTrackContainerList", myshipTrackContainerList);
						model.addAttribute("myshipTrackScheduleList", myshipTrackScheduleList);
						model.addAttribute("sapposearchparams", sapposearchparams);					
						model.addAttribute("companyCode", loginDto.getSalesOrgSelected());
						model.addAttribute("loggedinuser", loginDto.getLoggedInUserName());
						Map<String, String> buyersList = new LinkedHashMap<String, String>();
						buyersList = getBuyersList(loginDto);
						model.addAttribute("buyersList", buyersList);
						
						logger.info("Method MyshipmentTrackController.showShipmentDetailDataByDate ends");
						return "myshipmentTrackByDatePage";
					}
				}else {
					logger.info("myshipmentTrackJsonOutputData was returned null, MyshipmentTrackController.getMyshipmentTrackDetail Method");
					Map<String, String> buyersList = new LinkedHashMap<String, String>();
					buyersList = getBuyersList(loginDto);
					model.addAttribute("buyersList", buyersList);
					return "myshipmentTrackByDatePage";
				}
			}
		}
		return "myshipmentTrackByDatePage";		
	}
	
	private List<MyshipmentTrackItemBeanDto> addHeaderItemstoItemBean(
			List<MyshipmentTrackHeaderBean> myshipTrackHeaderList, List<MyshipmentTrackItemBean> myshipTrackItemList) {
		// TODO Auto-generated method stub
		List<MyshipmentTrackItemBeanDto> itemBeanDtoList = new ArrayList<MyshipmentTrackItemBeanDto>();
		MyshipmentTrackItemBeanDto trckItmBeanDto;
		Iterator<MyshipmentTrackItemBean> iter = myshipTrackItemList.iterator();
		try {
			while (iter.hasNext()) {
				MyshipmentTrackItemBean tib = iter.next();
				trckItmBeanDto = new MyshipmentTrackItemBeanDto();
				trckItmBeanDto.setSo_no(tib.getSo_no());
				trckItmBeanDto.setBl_no(tib.getBl_no());
				trckItmBeanDto.setItem_no(tib.getItem_no());
				trckItmBeanDto.setPo_no(tib.getPo_no());											
				trckItmBeanDto.setItem_pcs(tib.getItem_pcs());
				trckItmBeanDto.setItem_qty(tib.getItem_qty());
				trckItmBeanDto.setStyle(tib.getStyle());
				trckItmBeanDto.setItem_vol(tib.getItem_vol());
				trckItmBeanDto.setItem_chdt(tib.getItem_chdt());
				trckItmBeanDto.setSize_no(tib.getSize());
				trckItmBeanDto.setColor(tib.getColor());
				trckItmBeanDto.setItem_grwt(tib.getItem_grwt());
				itemBeanDtoList.add(trckItmBeanDto);
			}
			Iterator<MyshipmentTrackItemBeanDto> iterDto = itemBeanDtoList.iterator();
			while(iterDto.hasNext()) {
				MyshipmentTrackItemBeanDto itmbdto = iterDto.next();
				for(MyshipmentTrackHeaderBean head : myshipTrackHeaderList) {
					if((head.getSo_no().equalsIgnoreCase(itmbdto.getSo_no())) && (head.getBl_no().equalsIgnoreCase(itmbdto.getBl_no()))) {
						itmbdto.setShipper_no(head.getShipper_no());
						itmbdto.setBuyer_no(head.getBuyer_no());
						itmbdto.setItem_bl_dt(head.getSo_dt());
						itmbdto.setBuyer_name(head.getBuyer());
						itmbdto.setShipper_name(head.getShipper());
					}
				}
			}
		}catch(Exception ex) {
			ex.printStackTrace();
		}
		return itemBeanDtoList;
	}

	private List<MyshipmentTrackItemBeanDto> removeDuplicatePOItems(List<MyshipmentTrackItemBeanDto> myshipTrackItemDtoList,	List<BookedPOApproval> listBookedPoApproval) {
		// TODO Auto-generated method stub
		//List<MyshipmentTrackItemBean> pendingPOItems = new ArrayList<MyshipmentTrackItemBean>();
		Iterator<MyshipmentTrackItemBeanDto> iter = myshipTrackItemDtoList.iterator();
		try {
			while (iter.hasNext()) {
				MyshipmentTrackItemBeanDto tib = iter.next();	
				for(BookedPOApproval bpa : listBookedPoApproval) {
					if((bpa.getSo_no().equalsIgnoreCase(tib.getSo_no())) && (bpa.getItem_no().equalsIgnoreCase(tib.getItem_no()))) {
						iter.remove();
						break;
					}
				}
			}
			//pendingPOItems = myshipTrackItemList;
			return myshipTrackItemDtoList;
		}
		catch(Exception ex) {
			ex.printStackTrace();
			return myshipTrackItemDtoList;
		}		
		//return pendingPOItems;
	}

	@RequestMapping(value = "/saveBookedPOApprovalData", method = RequestMethod.POST)
	@ResponseBody
	public List<BookedPOApproval> savePOApprovalDataFunction(HttpServletRequest request, Model model, @RequestBody List<BookedPOApprovalBean> bookedPoApprovalBeanList) {
		logger.info("Method MyshipmentTrackController.showShipmentDetailDataByDate starts");		
		logger.info("size of approved po list :" + bookedPoApprovalBeanList.size());
		HttpSession session = SessionUtil.getHttpSession(request);
		
		List<BookedPOApproval> bkdpoapplst = new ArrayList<BookedPOApproval>(); 
		for(BookedPOApprovalBean bpab : bookedPoApprovalBeanList) {
			BookedPOApproval approvedPo = new BookedPOApproval();
			approvedPo.setSo_no(bpab.getSono());
			approvedPo.setBl_no(bpab.getBlno());
			approvedPo.setItem_no(bpab.getItem_no());
			approvedPo.setPo_no(bpab.getPono());
			approvedPo.setItem_bl_dt(convertStringToDate(bpab.getItem_bl_dt()));
			approvedPo.setItem_pcs(Double.parseDouble(bpab.getItem_pcs()));
			approvedPo.setItem_qty(Double.parseDouble(bpab.getItem_qty()));
			approvedPo.setStyle(bpab.getStyle());
			approvedPo.setSize_no(bpab.getSize());
			approvedPo.setColor(bpab.getColor());
			approvedPo.setItem_vol(Double.parseDouble(bpab.getItem_vol()));
			approvedPo.setItem_grwt(Double.parseDouble(bpab.getItem_grwt()));
			approvedPo.setItem_chdt(convertStringToDate(bpab.getItem_chdt()));
			approvedPo.setBuyer_no(bpab.getBuyerno());
			approvedPo.setAction_user(bpab.getActionuser());
			approvedPo.setShipper_no(bpab.getShipperno());
			approvedPo.setShipper_name(bpab.getShippername());
			approvedPo.setBuyer_name(bpab.getBuyername());
			approvedPo.setIndc_dt(convertStringToDate(bpab.getIndcdt()));
			approvedPo.setPo_req_ship_dt(convertStringToDate(bpab.getPoreqdt()));
			approvedPo.setFvsl_name(bpab.getFvsl());
			approvedPo.setFetd(convertStringToDate(bpab.getFetd()));
			approvedPo.setMvsl_name(bpab.getMvsl());
			approvedPo.setMeta(convertStringToDate(bpab.getMeta()));
			approvedPo.setCarrier_name(bpab.getCar());
			approvedPo.setStatus(bpab.getSta());
			approvedPo.setRemarks(bpab.getRem());
			approvedPo.setAction_timestamp(new Date());
			if(approvedPo.getStatus().equalsIgnoreCase("APPROVED") || approvedPo.getStatus().equalsIgnoreCase("REJECTED"))
				bkdpoapplst.add(approvedPo);
		}
		if(bkdpoapplst != null && !bkdpoapplst.isEmpty())
			bookedPOApprovalService.saveBookedPoApprovalData(bkdpoapplst);
		//System.out.println("///////////////");
		
		session.setAttribute("approvedPOList", bkdpoapplst);
		return bkdpoapplst;
	}	
	
	
	//test buyer list populate
	public Map<String, String> getBuyersList(LoginDTO loginDto) {		
		Map<String, String> buyersList = new LinkedHashMap<String, String>();
		SupplierPreLoadJsonData supplierpreloadjsondata = new SupplierPreLoadJsonData();
		SupRequestParams supreqparams = new SupRequestParams();
		supreqparams.setCustomerId(loginDto.getLoggedInUserName());
		supreqparams.setDistChan(loginDto.getDisChnlSelected());
		supreqparams.setDivision(loginDto.getDivisionSelected());
		supreqparams.setSalesOrg(loginDto.getSalesOrgSelected());
		StringBuffer webServiceUrl = new StringBuffer(RestUtil.SUPPLIER_DETAILS);
		try {
			supplierpreloadjsondata = restService.postForObject(RestUtil.prepareUrlForService(webServiceUrl).toString(), 
					supreqparams, SupplierPreLoadJsonData.class);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if(supplierpreloadjsondata != null) {
			List<BuSupListBean> partnerList = supplierpreloadjsondata.getBuSupList();
			if((partnerList != null) && !partnerList.isEmpty()) {
				for(BuSupListBean bslb : partnerList) {
					if(bslb.getZzParvw().equalsIgnoreCase("WE"))
						buyersList.put(bslb.getZzCust(), bslb.getZzCustName());
				}
			}
		}
		return buyersList;
	}	
	
	public MyshipmentTrackHeaderBean getHeaderFromList(List<MyshipmentTrackHeaderBean> myshipTrackHeadList, String bl_no) {
		MyshipmentTrackHeaderBean myshipmentTrackHeaderBean = new MyshipmentTrackHeaderBean();
		for(MyshipmentTrackHeaderBean header : myshipTrackHeadList) {
			if(header.getBl_no().equalsIgnoreCase(bl_no)) {
				myshipmentTrackHeaderBean = header;
				break;
			}
		}
		return myshipmentTrackHeaderBean;
	}
	
	@ModelAttribute("statusTypes")
	public Map<String, String> getCompanyCodes() {
		
		Map<String, String> statusTypes = new LinkedHashMap<String, String>();
		statusTypes.put("PENDING", "PENDING");
		statusTypes.put("APPROVED", "APPROVED");
		statusTypes.put("REJECTED", "REJECTED");
		//statusTypes.put("ALL", "ALL");
		
		return statusTypes;
	}
	
	public Date convertStringToDate(String dateString) {
		SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        Date outputDate;
        try {
            outputDate = formatter.parse(dateString);
            return outputDate;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return new Date();
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/bookedpoapprovalsuccess",method=RequestMethod.GET)
	public String poApprovalResponse(HttpServletRequest request,Model model)
	{
		HttpSession session=SessionUtil.getHttpSession(request);
		try{
			List<BookedPOApprovalBean> bookedPoApprovalBeanList = (List<BookedPOApprovalBean>) session.getAttribute("approvedPOList");
			model.addAttribute("bookedPoApprovalBeanList", bookedPoApprovalBeanList);
			return "bookedPOApprovalResponse";
		}
		catch(Exception ex){
			ex.printStackTrace();
		}
		return "bookedPOApprovalResponse";		
	}
}
