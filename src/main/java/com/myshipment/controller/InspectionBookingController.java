package com.myshipment.controller;

import java.io.File;
import java.io.InputStream;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.myshipment.model.InsPODetails;
import com.myshipment.model.InspectionBookingBean;
import com.myshipment.model.InspectionTrackingParams;
import com.myshipment.model.InspectionTrackingResultBean;
import com.myshipment.model.POList;
import com.myshipment.service.InspectionBookingServiceImpl;

import com.myshipment.dto.LoginDTO;
import com.myshipment.mappers.POUploadMapper;
import com.myshipment.mappers.inspectionMapper;
import com.myshipment.util.DatabaseComponent;
import com.myshipment.util.SendMail;
import com.myshipment.util.SessionUtil;

@Controller
public class InspectionBookingController {
	/**
	 * Umme Tayaba
	 */
	@Autowired
	public inspectionMapper inspectMapper;

	@Autowired
	public InspectionBookingServiceImpl inspectionBookingService;
	private static final Logger logger = Logger.getLogger(InspectionBooking.class);

	// private static final long serialVersionUID = 3241557202361645759L;
	private String supplierId = "";
	private InputStream inStream;
	private String buyingHouse = "";
	private String poNumber = "";
	private String inspectionDate = "";
	private int inspectionQuantity = 0;
	private String packMethod = "";
	private String cargoDeliveryDate = "";
	private String location = "";
	private String customerName = "";
	private String emailList = "";
	private String remarks = "";
	private String deliveryDate = "";
	private int quantity = 0;
	private String sapNo = "";
	private String merchantDiser = "";
	private String advOrder = "";
	private String salesOrg = "";
	private String division = "";
	private String distChannel = "";
	private String mode = "";
	// private String remakrs="";
	// private String entryTime="";
	private String status = "";
	private List<HashMap<String, String>> inspectionList;
	private List<Object> insBookingList;
	private List<Object> inspectorCodeList;
	// private List<Object> insTrackingStatusList;
	// private HttpServletRequest servletRequest;

	protected HttpServletRequest request;
	protected HttpServletResponse response;
	private String type = "";
	private String scotaFileSubmission = "N";
	private String scotaFileSubmissionDate;
	private double bookingNo = 0;
	// private HashMap inspectorCodeMap=new HashMap();
	private String inspectorCode = "";
	private String[] poNos;
	private String[] sapNos;
	private String[] inpsStatus;
	private String[] bookingNos;
	private String receiveDateFrom;
	private String receiveDateTo;
	private String inspectionDateFrom;
	private String inspectionDateTo;
	private String orderNo;
	private File packingList;
	private String packingListContentType;
	private String packingListFileName;
	@Autowired
	private POUploadMapper poUploadMapper;


	@RequestMapping(value = "/api/searchPo/")
	public ResponseEntity<?> getSearchResultViaAjax(@RequestBody String json, BindingResult bindingResult,
			Errors errors, HttpServletRequest request) {
		logger.info(this.getClass().getName() + "ajax response starting");

		if (bindingResult.hasErrors()) {
			logger.info(this.getClass().getName() + "BINDING RESULT ERROR");

		}
		
		logger.info(this.getClass().getName() + json);

		json = json.toString().replaceAll("\"", "");
		List<InsPODetails> insPoDetails = new ArrayList<>();

		insPoDetails = inspectMapper.getOrdersPOWise(json);
		
		logger.info(this.getClass().getName() + insPoDetails.size());
		if (insPoDetails.isEmpty()) {
			logger.info(this.getClass().getName() + "list is empty");

		} else {
			logger.info(this.getClass().getName() + "list is not empty");
			for (InsPODetails insPoDetail:insPoDetails)
			{
				String status=insPoDetail.getFlag();
				if(status.equals("Y"))
				{
					logger.info(this.getClass().getName() + "flag is Y");
				}
			}

		}

		return ResponseEntity.ok(insPoDetails);

	}

	@RequestMapping(value = "/doinspectionbooking", method = RequestMethod.GET)
	public String doInspectionBooking(ModelMap model, HttpServletRequest request) {
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		if (loginDTO != null)
			model.addAttribute("disChannel", loginDTO.getDisChnlSelected());
		model.addAttribute("supplierId", loginDTO.getLoggedInUserName());
		model.addAttribute("division", loginDTO.getDivisionSelected());
		model.addAttribute("salesOrg", loginDTO.getSalesOrgSelected());
		InspectionBookingBean insBookingData = new InspectionBookingBean();
		insBookingData.setDistChannel(loginDTO.getDisChnlSelected());
		insBookingData.setSupplierId(loginDTO.getLoggedInUserName());
		insBookingData.setDivision(loginDTO.getDivisionSelected());
		insBookingData.setSalesOrg(loginDTO.getSalesOrgSelected());
		model.addAttribute("insBookingData", insBookingData);

		return "inspectionBooking";
	}

	@RequestMapping(value = "/submitinspectionbooking", method = RequestMethod.POST)
	public String submitInspectionBooking(@ModelAttribute("insBookingData") InspectionBookingBean insBookingData,
			ModelMap model, HttpServletRequest request, final RedirectAttributes redirectAttributes) {
		logger.info(this.getClass().getName() + " Submitting inspection booking");
		logger.info(this.getClass().getName() + " The buying house name is " + insBookingData.getBuyingHouse());
		logger.info(this.getClass().getName() + " The supplier id is " + insBookingData.getSupplierId());
		logger.info(this.getClass().getName() + " The marchandiser id is " + insBookingData.getMerchantDiser());
		HttpSession session = SessionUtil.getHttpSession(request);
		LoginDTO loginDTO = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
		insBookingData.setDistChannel(loginDTO.getDisChnlSelected());
		logger.info(this.getClass().getName() + " Selected distribution chanel " + loginDTO.getDisChnlSelected());
		insBookingData.setSupplierId(loginDTO.getLoggedInUserName());
		logger.info(this.getClass().getName() + " Selected user name " + loginDTO.getLoggedInUserName());
		insBookingData.setDivision(loginDTO.getDivisionSelected());
		logger.info(this.getClass().getName() + " Selected division " + loginDTO.getDivisionSelected());
		insBookingData.setSalesOrg(loginDTO.getSalesOrgSelected());
		logger.info(this.getClass().getName() + " Selected Sales org " + loginDTO.getSalesOrgSelected());
		List<InsPODetails> insPoDetails = new ArrayList<>();


		File fileToCreate = null;
		int checkAgain = 0;

		try {
			SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy");
			Date currentDate = new Date();
			if (getScotaFileSubmission().equals("Y"))
				this.scotaFileSubmissionDate = df.format(currentDate);
			else
				this.scotaFileSubmissionDate = "";
			String emailAddresses = "";
			inspectionBookingService.insertPOBooking(getScotaFileSubmission(), getScotaFileSubmissionDate(),
					insBookingData);

			try {
				checkAgain = inspectionBookingService.getCount(insBookingData.getPoNumber());
				logger.info(this.getClass().getName() + "the checkAgain1 is " + checkAgain);
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (checkAgain > 0) {
				inspectionBookingService.updateFlag(insBookingData.getPoNumber());
				inspectionBookingService.sendMailToSuppliersAndInspectors(fileToCreate, emailAddresses, checkAgain,
						insBookingData, redirectAttributes);

			} else {
				redirectAttributes.addFlashAttribute("message", "Inspection booking was not successful");
			}

		} catch (Exception e) {
			try {

				e.printStackTrace();

			} catch (Exception ex) {
			}
			e.printStackTrace();
		}

		return "redirect:/doinspectionbooking";
	}

	@RequestMapping(value = "/populatepo", method = RequestMethod.GET)
	@ResponseBody
	public List<String> getPopulatePO(@RequestParam("param") String param) {

		List<String> lstPortList = new ArrayList<String>();
		try {
			logger.info(this.getClass().getName() + "Query String: ");
			String query = param;
			logger.info(this.getClass().getName() + "Query String:" + query);
			if (query.length() > 6) {
				lstPortList = inspectionBookingService.getPO(query, lstPortList);
			}
		} catch (Exception ex) {
			try {
				;
			} catch (Exception e) {
				e.printStackTrace();
			}
			ex.printStackTrace();
		}
		return lstPortList;

	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public String getSupplierId() {
		return supplierId;
	}

	public void setBuyingHouse(String buyingHouse) {
		this.buyingHouse = buyingHouse;
	}

	public String getBuyingHouse() {
		return buyingHouse;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public String getPoNumber() {
		return poNumber;
	}

	public void setInspectionDate(String inspectionDate) {
		this.inspectionDate = inspectionDate;
	}

	public String getInspectionDate() {
		return inspectionDate;
	}

	public void setInspectionQuantity(int inspectionQuantity) {
		this.inspectionQuantity = inspectionQuantity;
	}

	public int getInspectionQuantity() {
		return inspectionQuantity;
	}

	public void setPackMethod(String packMethod) {
		this.packMethod = packMethod;
	}

	public String getPackMethod() {
		return packMethod;
	}

	public void setCargoDeliveryDate(String cargoDeliveryDate) {
		this.cargoDeliveryDate = cargoDeliveryDate;
	}

	public String getCargoDeliveryDate() {
		return cargoDeliveryDate;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getLocation() {
		return location;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public String getDeliveryDate() {
		return deliveryDate;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setSapNo(String sapNo) {
		this.sapNo = sapNo;
	}

	public String getSapNo() {
		return sapNo;
	}

	public void setMerchantDiser(String merchantDiser) {
		this.merchantDiser = merchantDiser;
	}

	public String getMerchantDiser() {
		return merchantDiser;
	}

	public void setAdvOrder(String advOrder) {
		this.advOrder = advOrder;
	}

	public String getAdvOrder() {
		return advOrder;
	}

	public void setSalesOrg(String salesOrg) {
		this.salesOrg = salesOrg;
	}

	public String getSalesOrg() {
		return salesOrg;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getDivision() {
		return division;
	}

	public void setDistChannel(String distChannel) {
		this.distChannel = distChannel;
	}

	public String getDistChannel() {
		return distChannel;
	}

	public void setMode(String mode) {
		this.mode = mode;
	}

	public String getMode() {
		return mode;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setInspectionList(List<HashMap<String, String>> inspectionList) {
		this.inspectionList = inspectionList;
	}

	public List<HashMap<String, String>> getInspectionList() {
		return inspectionList;
	}

	public void setInsBookingList(List<Object> insBookingList) {
		this.insBookingList = insBookingList;
	}

	public List<Object> getInsBookingList() {
		return insBookingList;
	}

	public void setInspectorCodeList(List<Object> inspectorCodeList) {
		this.inspectorCodeList = inspectorCodeList;
	}

	public List<Object> getInspectorCodeList() {
		return inspectorCodeList;
	}

	public HttpServletRequest getServletRequest() {
		return request;
	}

	public void setServletRequest(HttpServletRequest req) {
		this.request = req;
	}

	public HttpServletResponse getServletResponse() {
		return response;
	}

	public void setServletResponse(HttpServletResponse resp) {
		this.response = resp;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setBookingNo(double bookingNo) {
		this.bookingNo = bookingNo;
	}

	public double getBookingNo() {
		return bookingNo;
	}

	public String getEmailList() {
		return emailList;
	}

	public void setEmailList(String emailList) {
		this.emailList = emailList;
	}

	public InputStream getInStream() {
		return inStream;
	}

	public void setInStream(InputStream inStream) {
		this.inStream = inStream;
	}

	public String getScotaFileSubmission() {
		return scotaFileSubmission;
	}

	public void setScotaFileSubmission(String scotaFileSubmission) {
		this.scotaFileSubmission = scotaFileSubmission;
	}

	public String getScotaFileSubmissionDate() {
		return scotaFileSubmissionDate;
	}

	public void setScotaFileSubmissionDate(String scotaFileSubmissionDate) {
		this.scotaFileSubmissionDate = scotaFileSubmissionDate;
	}

	public String[] getPoNos() {
		return poNos;
	}

	public void setPoNos(String[] poNos) {
		this.poNos = poNos;
	}

	public String[] getSapNos() {
		return sapNos;
	}

	public void setSapNos(String[] sapNos) {
		this.sapNos = sapNos;
	}

	public String[] getInpsStatus() {
		return inpsStatus;
	}

	public void setInpsStatus(String[] inpsStatus) {
		this.inpsStatus = inpsStatus;
	}

	/*
	 * public HashMap getInspectorCodeMap() { return inspectorCodeMap; } public void
	 * setInspectorCodeMap(HashMap inspectorCodeMap) { this.inspectorCodeMap =
	 * inspectorCodeMap; }
	 */
	public String getInspectorCode() {
		return inspectorCode;
	}

	public void setInspectorCode(String inspectorCode) {
		this.inspectorCode = inspectorCode;
	}

	public String[] getBookingNos() {
		return bookingNos;
	}

	public void setBookingNos(String[] bookingNos) {
		this.bookingNos = bookingNos;
	}

	public String getReceiveDateFrom() {
		return receiveDateFrom;
	}

	public void setReceiveDateFrom(String receiveDateFrom) {
		this.receiveDateFrom = receiveDateFrom;
	}

	public String getReceiveDateTo() {
		return receiveDateTo;
	}

	public void setReceiveDateTo(String receiveDateTo) {
		this.receiveDateTo = receiveDateTo;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public File getPackingList() {
		return packingList;
	}

	public void setPackingList(File packingList) {
		this.packingList = packingList;
	}

	public String getPackingListContentType() {
		return packingListContentType;
	}

	public void setPackingListContentType(String packingListContentType) {
		this.packingListContentType = packingListContentType;
	}

	public String getPackingListFileName() {
		return packingListFileName;
	}

	public void setPackingListFileName(String packingListFileName) {
		this.packingListFileName = packingListFileName;
	}

	public String getInspectionDateFrom() {
		return inspectionDateFrom;
	}

	public void setInspectionDateFrom(String inspectionDateFrom) {
		this.inspectionDateFrom = inspectionDateFrom;
	}

	public String getInspectionDateTo() {
		return inspectionDateTo;
	}

	public void setInspectionDateTo(String inspectionDateTo) {
		this.inspectionDateTo = inspectionDateTo;
	}

}
