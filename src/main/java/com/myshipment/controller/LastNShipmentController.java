package com.myshipment.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.myshipment.dto.LoginDTO;
import com.myshipment.model.ItHeaderBean;
import com.myshipment.model.LastNShipmentParams;
import com.myshipment.model.LastNShipmentsJsonOutputData;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;
/*
 * @Ranjeet Kumar
 */
@Controller
public class LastNShipmentController extends BaseController{


	private static final long serialVersionUID = 1L;

	final static Logger logger = Logger.getLogger(LastNShipmentController.class);

	@Autowired
	private RestService restService;

	@RequestMapping(value = "/lastNShipmentData", method = RequestMethod.POST)
	public String getLastNShipmentDetail(@ModelAttribute("lastNShipmentParams") LastNShipmentParams lastNShipmentParams, Model model, HttpServletRequest request){

		logger.info("Method LastNShipmentController.getLastNShipmentDetail starts.");

		if(lastNShipmentParams != null){
			LoginDTO loginDto = (LoginDTO)request.getSession().getAttribute("loginDetails");
			lastNShipmentParams.setKunnr(loginDto.getLoggedInUserName());
			lastNShipmentParams.setSpart(loginDto.getDivisionSelected());
			lastNShipmentParams.setVkorg(loginDto.getSalesOrgSelected());
			lastNShipmentParams.setVtweg(loginDto.getDisChnlSelected());
			
			StringBuffer webServiceUrl = new StringBuffer(RestUtil.LAST_N_SHIPMENT);
			LastNShipmentsJsonOutputData lastNShipmentsJsonOutputData = restService.postForObject(RestUtil.prepareUrlForService(webServiceUrl).toString(), lastNShipmentParams, LastNShipmentsJsonOutputData.class);	
			
			if(lastNShipmentsJsonOutputData != null) {
				List<ItHeaderBean> itHeaderLst = lastNShipmentsJsonOutputData.getItHeaderBeanLst();
				model.addAttribute("itHeaderLst", itHeaderLst);
				model.addAttribute("lastNShipmentParams", lastNShipmentParams);
				logger.info("Method LastNShipmentController.getLastNShipmentDetail ends.");
				return "orderHistory";
			}
			else {
				logger.info("lastNShipmentsJsonOutputData was returned null, LastNShipmentController.getLastNShipmentDetail Method");
				return "orderHistory";
			}
		}
		return null;
	}

	@RequestMapping(value = "/getLastNShipmentPage", method = RequestMethod.GET)
	public String getLastNShipmentSearchPage(Model model, HttpServletRequest request)
	{
		logger.info("Method LastNShipmentController.getLastNShipmentSearchPage starts.");

		try{ 
			LastNShipmentParams lastNShipmentParams = new LastNShipmentParams();
			LoginDTO loginDto = (LoginDTO)request.getSession().getAttribute("loginDetails");			
			lastNShipmentParams.setKunnr(loginDto.getLoggedInUserName());
			lastNShipmentParams.setNumber(5);
			lastNShipmentParams.setSpart(loginDto.getDivisionSelected());
			lastNShipmentParams.setVkorg(loginDto.getSalesOrgSelected());
			lastNShipmentParams.setVtweg(loginDto.getDisChnlSelected());			
			logger.info("Method LastNShipmentController.getLastNShipmentSearchPage ends.");
			return getLastNShipmentDetail(lastNShipmentParams, model, request);
		}
		catch(Exception ex) {
			ex.printStackTrace();
			return "orderHistory";
		}
	}
}


