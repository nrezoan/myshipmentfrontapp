package com.myshipment.controller;

import org.springframework.stereotype.Controller;

import java.io.InputStream;
import java.util.Properties;
import javax.mail.MessagingException;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.myshipment.model.HelpEmailParams;
import com.myshipment.service.IEmailService;
import com.myshipment.util.CommonConstant;
import com.myshipment.util.ServiceLocator;
import com.sun.mail.util.MailSSLSocketFactory;

/**
 * 
 * @author Nusrat Momtahana
 *
 */

@Controller
public class HelpEmailController {
	@Autowired
	private IEmailService emailService;
	private ServiceLocator serviceLocator;

	private Logger logger = Logger.getLogger(HelpEmailController.class);

	@RequestMapping(value = "/helpEmail", method = RequestMethod.GET)
	public String showForm() {
		return "myshipment_landing";
	}

	@RequestMapping(value = "/helpEmail", method = RequestMethod.POST)
	public void help(@ModelAttribute("helpEmailParam") HelpEmailParams helpEmailParam, Model model, HttpSession session,
			ModelAndView modelAndView, RedirectAttributes redirecAttributes, BindingResult result) {
		logger.info("Method HelpEmailController.helpEmail starts.");

		if (helpEmailParam != null) {
			String mailTo = "itsupport@mghgroup.com";
			String text = "Dear Team," + "\n" + "\n" + helpEmailParam.getMessage2() + "\n" + "\nSender Name :"
					+ helpEmailParam.getName2() + "\nSender Email :" + helpEmailParam.getEmail2();
			String subject = "Myshipment Support request from-" + helpEmailParam.getEmail2();
			Properties properties = new Properties();
			InputStream inputStream = null;
			try {
				inputStream = this.getClass().getClassLoader().getResourceAsStream("email/emailconfig.properties");
				properties.load(inputStream);
				// properties.put(key, value);
				properties.put("mail.smtp.EnableSSL.enable", "true");

				MailSSLSocketFactory sf = new MailSSLSocketFactory();
				sf.setTrustAllHosts(true);

				properties.put("mail.smtp.ssl.socketFactory", sf);

				emailService = (IEmailService) serviceLocator.findService("emailService");
				try {
					emailService.recoverPasswordMail(mailTo,
							properties.getProperty(CommonConstant.DIRECT_BOOKING_MAIL_FROM), subject, text);
				} catch (MessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.debug("" + e.getMessage());
			}

		}

	}

}
