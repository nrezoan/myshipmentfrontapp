package com.myshipment.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myshipment.dto.LoginDTO;
import com.myshipment.dto.MyshipmentTrackContainerBeanDto;
import com.myshipment.model.AirExportHAWBData;
import com.myshipment.model.AirImportHAWBData;
import com.myshipment.model.FlightInformationModel;
import com.myshipment.model.FlightParamDetails;
import com.myshipment.model.FlightRoute;
import com.myshipment.model.ItDocumentHeaderBean;
import com.myshipment.model.MyshipmentTrackContainerBean;
import com.myshipment.model.MyshipmentTrackItemBean;
import com.myshipment.model.MyshipmentTrackJsonOutputData;
import com.myshipment.model.MyshipmentTrackParams;
import com.myshipment.model.MyshipmentTrackScheduleBean;
import com.myshipment.model.OAG;
import com.myshipment.model.OAGRequest;
import com.myshipment.model.OAGResponseDetails;
import com.myshipment.model.SeaExportHBLData;
import com.myshipment.model.SeaImportHBLData;
import com.myshipment.model.TrackingRequestParam;
import com.myshipment.model.TrackingStatusParams;
import com.myshipment.service.IOAGService;
import com.myshipment.util.CommonConstant;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;
import com.myshipment.util.SessionUtil;

/*
 * @Ranjeet Kumar
 */
@Controller
public class TrackingControllerNew {

	private static final Logger logger = Logger.getLogger(TrackingControllerNew.class);

	@Autowired
	private RestService restService;

	@Autowired
	private IOAGService oagService;

	@RequestMapping(value = "/getTrackingDetailInfo", method = RequestMethod.POST)
	public String getTrackingInfo(@ModelAttribute("trackingRequestParam") TrackingRequestParam trackingRequestParam,
			ModelMap model, HttpSession session) {
		logger.info("Method TrackingController.getTrackingInfo starts");

		OAGResponseDetails oagResponseDetails = new OAGResponseDetails();

		/*
		 * try{ if(trackingRequestParam != null) { LoginDTO loginDto =
		 * (LoginDTO)session.getAttribute(SessionUtil.LOGIN_DETAILS); String customerNo
		 * = null; String distChannel=null; String division=null; String salesOrg=null;
		 * if(loginDto != null){ System.out.println("loginDto: "+loginDto); customerNo =
		 * loginDto.getLoggedInUserName(); distChannel=loginDto.getDisChnlSelected();
		 * division=loginDto.getDivisionSelected();
		 * salesOrg=loginDto.getSalesOrgSelected();
		 * logger.info("Customer No found as : "+ customerNo);
		 * trackingRequestParam.setDistributionChannel(distChannel);
		 * trackingRequestParam.setDivision(division);
		 * trackingRequestParam.setSalesOrg(salesOrg);
		 * 
		 * }
		 * 
		 * List<ItDocumentHeaderBean> documentHeaderBeanLst = null;
		 * List<ItDocumentDetailBean> documentDetailBeanLst = null;
		 * 
		 * StringBuffer webServiceUrl = new StringBuffer(RestUtil.TRACKING_DETAIL);
		 * TrackingOutputJsonDataNew trackingOutputJsonData =
		 * restService.postForObject(RestUtil.prepareUrlForService(webServiceUrl).
		 * toString(), trackingRequestParam, TrackingOutputJsonDataNew.class);
		 * if(!trackingOutputJsonData.getResponse().equals("FAILED")){
		 * if(trackingOutputJsonData != null){ documentHeaderBeanLst =
		 * trackingOutputJsonData.getDocumentHeaderLst(); documentDetailBeanLst =
		 * trackingOutputJsonData.getDocumentDetailLst(); }
		 * 
		 * 
		 * TrackingStatusParams trackingStatusParams =
		 * prepareTrackingStatusParamsData(documentHeaderBeanLst);
		 * 
		 * model.addAttribute("documentHeaderBeanLst", documentHeaderBeanLst);
		 * model.addAttribute("documentDetailBeanLst", documentDetailBeanLst);
		 * model.addAttribute("trackingStatusParams", trackingStatusParams);
		 * model.addAttribute("trackingOutputJsonData", trackingOutputJsonData);
		 * logger.info("Method TrackingController.getTrackingInfo ends");
		 * 
		 * 
		 * return "tracking"; }else{ model.addAttribute("trackingOutputJsonData",
		 * trackingOutputJsonData); return "tracking"; } } }catch(Exception e){
		 * e.printStackTrace(); logger.debug("Some exception occurred : ", e); return
		 * "trackingSearch"; } return "trackingSearch";
		 */

		try {
			MyshipmentTrackParams myshipmentTrackParams = new MyshipmentTrackParams();
			LoginDTO loginDto = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
			if ((loginDto.getDisChnlSelected().equalsIgnoreCase("EX"))
					&& (loginDto.getDivisionSelected().equalsIgnoreCase("SE"))) {
				myshipmentTrackParams.setAction("01");
			} else if ((loginDto.getDisChnlSelected().equalsIgnoreCase("EX"))
					&& (loginDto.getDivisionSelected().equalsIgnoreCase("AR"))) {
				myshipmentTrackParams.setAction("03");
			} else if ((loginDto.getDisChnlSelected().equalsIgnoreCase("IM"))
					&& (loginDto.getDivisionSelected().equalsIgnoreCase("SE"))) {
				myshipmentTrackParams.setAction("02");
			} else if ((loginDto.getDisChnlSelected().equalsIgnoreCase("IM"))
					&& (loginDto.getDivisionSelected().equalsIgnoreCase("AR"))) {
				myshipmentTrackParams.setAction("04");
			}
			myshipmentTrackParams.setDoc_no(trackingRequestParam.getBlNo().toUpperCase());
			myshipmentTrackParams.setCustomer(loginDto.getLoggedInUserName());
			myshipmentTrackParams.setDistChan(loginDto.getDisChnlSelected());
			myshipmentTrackParams.setDivision(loginDto.getDivisionSelected());
			myshipmentTrackParams.setSalesOrg(loginDto.getSalesOrgSelected());

			// StringBuffer webServiceUrl = new StringBuffer(RestUtil.OPEN_TRACKING_DETAIL);
			StringBuffer webServiceUrl = new StringBuffer(RestUtil.MYSHIPMENT_COMPLETE_TRACK);
			MyshipmentTrackJsonOutputData myshipmentTrackJsonOutputData = restService.postForObject(
					RestUtil.prepareUrlForService(webServiceUrl).toString(), myshipmentTrackParams,
					MyshipmentTrackJsonOutputData.class);

			// prepareDataCorrespodningToThePage(myshipmentTrackJsonOutputData,
			// trackingRequestParam.getAction());

			if (myshipmentTrackParams != null
					&& myshipmentTrackParams.getAction().equalsIgnoreCase(CommonConstant.SEA_EXPORT_HBL)) {
				if (myshipmentTrackJsonOutputData.getDialogue().equalsIgnoreCase("success")) {
					SeaExportHBLData seaExportHBLData = prepareDataForSeaExportHBL(myshipmentTrackJsonOutputData);
					model.addAttribute("seaExportHBLData", seaExportHBLData);
					return "trackSeaExpHbl";
				}
				return "trackSearchNoResult";
			}
			/*
			 * if(trackingRequestParam != null &&
			 * trackingRequestParam.getAction().equalsIgnoreCase(CommonConstant.
			 * SEA_IMPORT_HBL)) { SeaImportHBLData seaImportHBLData =
			 * prepareDataForSeaImportHBL(myshipmentTrackJsonOutputData);
			 * model.addAttribute("seaImportHBLData", seaImportHBLData); return
			 * "seaImportHBL"; }
			 */
			String airports = "";
			if (myshipmentTrackParams != null
					&& myshipmentTrackParams.getAction().equalsIgnoreCase(CommonConstant.AIR_EXPORT_HAWB)) {

				if (myshipmentTrackJsonOutputData.getDialogue().equalsIgnoreCase("success")) {

					AirExportHAWBData airExportHAWBData = prepareDataForAirExportHAWB(myshipmentTrackJsonOutputData);

					oagResponseDetails = oagFlightRequestParamCreate(airExportHAWBData);
					FlightInformationModel flight = new FlightInformationModel();
					Gson gson = new GsonBuilder().setPrettyPrinting().create();

					if (oagResponseDetails != null) {
						
						
						for(OAG oag:oagResponseDetails.getOagResponse() ) {
							airports = airports+" "+oag.getVcFlightNo()+"("+ oag.getVcDepAirportCode()+" to "+ oag.getVcArrAirportCode()+")\n";
						}

						airExportHAWBData.setFlightDetails(oagResponseDetails.getOagResponse());

						OAG inAirDetails = oagResponseDetails.getInAirFlightDetails();

						airExportHAWBData.setInAirFlightDetails(inAirDetails);

						if (inAirDetails != null) {

							FlightRoute[] flightRoute = gson.fromJson(
									airExportHAWBData.getInAirFlightDetails().getVcRoute(), FlightRoute[].class);

							flight = flightData(inAirDetails, flightRoute);

						} else {

							List<OAG> flightDetails = airExportHAWBData.getFlightDetails();
							OAG lastLeg = flightDetails.get(flightDetails.size() - 1);
							FlightRoute[] flightRoute = gson.fromJson(lastLeg.getVcRoute(), FlightRoute[].class);
							flight = flightData(lastLeg, flightRoute);

						}
					}

					model.addAttribute("airExportHAWBData", airExportHAWBData);
					model.addAttribute("flight", flight);
					model.addAttribute("currentFlight", new Gson().toJson(flight));
					model.addAttribute("oagResponseDetails", new Gson().toJson(oagResponseDetails));
					model.addAttribute("airports", airports);
					return "trackAirExpHawb";
				}
				return "trackSearchNoResult";
			}
			/*
			 * if(trackingRequestParam != null &&
			 * trackingRequestParam.getAction().equalsIgnoreCase(CommonConstant.
			 * AIR_IMPORT_HAWB)) { AirImportHAWBData airImportHAWBData =
			 * prepareDataForAirImportHAWB(myshipmentTrackJsonOutputData);
			 * model.addAttribute("airImportHAWBData", airImportHAWBData); return
			 * "airImportHAWB"; }
			 */
			/*
			 * if(trackingRequestParam != null &&
			 * trackingRequestParam.getAction().equalsIgnoreCase(CommonConstant.
			 * SEA_EXPORT_MBL)) {
			 * 
			 * } if(trackingRequestParam != null &&
			 * trackingRequestParam.getAction().equalsIgnoreCase(CommonConstant.
			 * SEA_IMPORT_MBL)) {
			 * 
			 * } if(trackingRequestParam != null &&
			 * trackingRequestParam.getAction().equalsIgnoreCase(CommonConstant.
			 * AIR_EXPORT_MAWB)) {
			 * 
			 * } if(trackingRequestParam != null &&
			 * trackingRequestParam.getAction().equalsIgnoreCase(CommonConstant.
			 * AIR_IMPORT_MAWB)) {
			 * 
			 * }
			 */
		} catch (Exception ex) {
			ex.printStackTrace();
			return "redirect:/";
		}

		return "error";

	}

	@RequestMapping(value = "/getCommonTrackingDetailInfo", method = RequestMethod.POST)
	public String getCommonTrackingInfo(
			@ModelAttribute("trackingRequestParam") TrackingRequestParam trackingRequestParam, ModelMap model,
			HttpSession session) {
		logger.info("Method TrackingController.getTrackingInfo starts");

		try {
			MyshipmentTrackParams myshipmentTrackParams = new MyshipmentTrackParams();
			myshipmentTrackParams.setAction(trackingRequestParam.getAction());
			myshipmentTrackParams.setDoc_no(trackingRequestParam.getBlNo().toUpperCase());

			// StringBuffer webServiceUrl = new StringBuffer(RestUtil.OPEN_TRACKING_DETAIL);
			StringBuffer webServiceUrl = new StringBuffer(RestUtil.MYSHIPMENT_COMPLETE_TRACK);
			MyshipmentTrackJsonOutputData myshipmentTrackJsonOutputData = restService.postForObject(
					RestUtil.prepareUrlForService(webServiceUrl).toString(), myshipmentTrackParams,
					MyshipmentTrackJsonOutputData.class);

			// prepareDataCorrespodningToThePage(myshipmentTrackJsonOutputData,
			// trackingRequestParam.getAction());

			if (trackingRequestParam != null
					&& trackingRequestParam.getAction().equalsIgnoreCase(CommonConstant.SEA_EXPORT_HBL)) {
				SeaExportHBLData seaExportHBLData = prepareDataForSeaExportHBL(myshipmentTrackJsonOutputData);
				model.addAttribute("seaExportHBLData", seaExportHBLData);
				return "seaExportHBL";

			}
			if (trackingRequestParam != null
					&& trackingRequestParam.getAction().equalsIgnoreCase(CommonConstant.SEA_IMPORT_HBL)) {
				SeaImportHBLData seaImportHBLData = prepareDataForSeaImportHBL(myshipmentTrackJsonOutputData);
				model.addAttribute("seaImportHBLData", seaImportHBLData);
				return "seaImportHBL";
			}
			if (trackingRequestParam != null
					&& trackingRequestParam.getAction().equalsIgnoreCase(CommonConstant.AIR_EXPORT_HAWB)) {
				AirExportHAWBData airExportHAWBData = prepareDataForAirExportHAWB(myshipmentTrackJsonOutputData);
				model.addAttribute("airExportHAWBData", airExportHAWBData);
				return "airExportHAWB";
			}
			if (trackingRequestParam != null
					&& trackingRequestParam.getAction().equalsIgnoreCase(CommonConstant.AIR_IMPORT_HAWB)) {
				AirImportHAWBData airImportHAWBData = prepareDataForAirImportHAWB(myshipmentTrackJsonOutputData);
				model.addAttribute("airImportHAWBData", airImportHAWBData);
				return "airImportHAWB";
			}
			if (trackingRequestParam != null
					&& trackingRequestParam.getAction().equalsIgnoreCase(CommonConstant.SEA_EXPORT_MBL)) {

			}
			if (trackingRequestParam != null
					&& trackingRequestParam.getAction().equalsIgnoreCase(CommonConstant.SEA_IMPORT_MBL)) {

			}
			if (trackingRequestParam != null
					&& trackingRequestParam.getAction().equalsIgnoreCase(CommonConstant.AIR_EXPORT_MAWB)) {

			}
			if (trackingRequestParam != null
					&& trackingRequestParam.getAction().equalsIgnoreCase(CommonConstant.AIR_IMPORT_MAWB)) {

			}
		} catch (Exception ex) {
			ex.printStackTrace();
			return "landingpage_v2";
		}

		return "error";
		/*
		 * try{ if(trackingRequestParam != null) {
		 * 
		 * 
		 * List<ItDocumentHeaderBean> documentHeaderBeanLst = null;
		 * List<ItDocumentDetailBean> documentDetailBeanLst = null;
		 * 
		 * StringBuffer webServiceUrl = new
		 * StringBuffer(RestUtil.COMMON_TRACKING_DETAIL); TrackingOutputJsonDataNew
		 * trackingOutputJsonData =
		 * restService.postForObject(RestUtil.prepareUrlForService(webServiceUrl).
		 * toString(), trackingRequestParam, TrackingOutputJsonDataNew.class);
		 * 
		 * if(trackingOutputJsonData != null){ documentHeaderBeanLst =
		 * trackingOutputJsonData.getDocumentHeaderLst(); documentDetailBeanLst =
		 * trackingOutputJsonData.getDocumentDetailLst(); }
		 * 
		 * 
		 * TrackingStatusParams trackingStatusParams =
		 * prepareTrackingStatusParamsData(documentHeaderBeanLst);
		 * 
		 * model.addAttribute("documentHeaderBeanLst", documentHeaderBeanLst);
		 * model.addAttribute("documentDetailBeanLst", documentDetailBeanLst);
		 * model.addAttribute("trackingStatusParams", trackingStatusParams);
		 * 
		 * logger.info("Method TrackingController.getTrackingInfo ends");
		 * 
		 * 
		 * return "trackingCommon"; } }catch(Exception e){ e.printStackTrace();
		 * logger.debug("Some exception occurred : ", e); return "trackingCommon"; }
		 * return "trackingCommon";
		 */

	}

	@RequestMapping(value = "/getTrackingInfoFrmUrl", method = RequestMethod.GET)
	public String getTrackingInfoFrmUrl(@RequestParam("searchString") String searchValue, ModelMap model,
			HttpSession session) {
		logger.info("Method TrackingController.getTrackingInfo starts");

		OAGResponseDetails oagResponseDetails = new OAGResponseDetails();
		/*
		 * try{ if(searchValue != null) { LoginDTO loginDto =
		 * (LoginDTO)session.getAttribute(SessionUtil.LOGIN_DETAILS);
		 * TrackingRequestParam trackingRequestParam=new TrackingRequestParam(); String
		 * customerNo = null; String distChannel=null; String division=null; String
		 * salesOrg=null; if(loginDto != null){
		 * System.out.println("loginDto: "+loginDto); customerNo =
		 * loginDto.getLoggedInUserName(); distChannel=loginDto.getDisChnlSelected();
		 * division=loginDto.getDivisionSelected();
		 * salesOrg=loginDto.getSalesOrgSelected();
		 * logger.info("Customer No found as : "+ customerNo);
		 * trackingRequestParam.setDistributionChannel(distChannel);
		 * trackingRequestParam.setDivision(division);
		 * trackingRequestParam.setSalesOrg(salesOrg);
		 * trackingRequestParam.setBlNo(searchValue);
		 * 
		 * 
		 * }
		 * 
		 * List<ItDocumentHeaderBean> documentHeaderBeanLst = null;
		 * List<ItDocumentDetailBean> documentDetailBeanLst = null;
		 * 
		 * StringBuffer webServiceUrl = new StringBuffer(RestUtil.TRACKING_DETAIL);
		 * TrackingOutputJsonDataNew trackingOutputJsonData =
		 * restService.postForObject(RestUtil.prepareUrlForService(webServiceUrl).
		 * toString(), trackingRequestParam, TrackingOutputJsonDataNew.class);
		 * if(!trackingOutputJsonData.getResponse().equalsIgnoreCase("FAILED")){
		 * if(trackingOutputJsonData != null){ documentHeaderBeanLst =
		 * trackingOutputJsonData.getDocumentHeaderLst(); documentDetailBeanLst =
		 * trackingOutputJsonData.getDocumentDetailLst(); }
		 * 
		 * 
		 * TrackingStatusParams trackingStatusParams =
		 * prepareTrackingStatusParamsData(documentHeaderBeanLst);
		 * 
		 * model.addAttribute("documentHeaderBeanLst", documentHeaderBeanLst);
		 * model.addAttribute("documentDetailBeanLst", documentDetailBeanLst);
		 * model.addAttribute("trackingStatusParams", trackingStatusParams);
		 * model.addAttribute("trackingOutputJsonData", trackingOutputJsonData);
		 * logger.info("Method TrackingController.getTrackingInfo ends");
		 * 
		 * 
		 * return "tracking"; }else{ model.addAttribute("trackingOutputJsonData",
		 * trackingOutputJsonData); return "tracking"; } } }
		 */
		try {
			MyshipmentTrackParams myshipmentTrackParams = new MyshipmentTrackParams();
			LoginDTO loginDto = (LoginDTO) session.getAttribute(SessionUtil.LOGIN_DETAILS);
			if ((loginDto.getDisChnlSelected().equalsIgnoreCase("EX"))
					&& (loginDto.getDivisionSelected().equalsIgnoreCase("SE"))) {
				myshipmentTrackParams.setAction("01");
			} else if ((loginDto.getDisChnlSelected().equalsIgnoreCase("EX"))
					&& (loginDto.getDivisionSelected().equalsIgnoreCase("AR"))) {
				myshipmentTrackParams.setAction("03");
			} else if ((loginDto.getDisChnlSelected().equalsIgnoreCase("IM"))
					&& (loginDto.getDivisionSelected().equalsIgnoreCase("SE"))) {
				myshipmentTrackParams.setAction("02");
			} else if ((loginDto.getDisChnlSelected().equalsIgnoreCase("IM"))
					&& (loginDto.getDivisionSelected().equalsIgnoreCase("AR"))) {
				myshipmentTrackParams.setAction("04");
			}
			if (searchValue != null) {
				myshipmentTrackParams.setDoc_no(searchValue);
				myshipmentTrackParams.setCustomer(loginDto.getLoggedInUserName());
				myshipmentTrackParams.setDistChan(loginDto.getDisChnlSelected());
				myshipmentTrackParams.setDivision(loginDto.getDivisionSelected());
				myshipmentTrackParams.setSalesOrg(loginDto.getSalesOrgSelected());
			}

			StringBuffer webServiceUrl = new StringBuffer(RestUtil.MYSHIPMENT_COMPLETE_TRACK);
			MyshipmentTrackJsonOutputData myshipmentTrackJsonOutputData = restService.postForObject(
					RestUtil.prepareUrlForService(webServiceUrl).toString(), myshipmentTrackParams,
					MyshipmentTrackJsonOutputData.class);

			if (myshipmentTrackParams != null
					&& myshipmentTrackParams.getAction().equalsIgnoreCase(CommonConstant.SEA_EXPORT_HBL)) {
				if (myshipmentTrackJsonOutputData.getDialogue().equalsIgnoreCase("success")) {
					SeaExportHBLData seaExportHBLData = prepareDataForSeaExportHBL(myshipmentTrackJsonOutputData);
					model.addAttribute("seaExportHBLData", seaExportHBLData);
					return "trackSeaExpHbl";
				}
				return "trackSearchNoResult";
			}
			/*
			 * if(trackingRequestParam != null &&
			 * trackingRequestParam.getAction().equalsIgnoreCase(CommonConstant.
			 * SEA_IMPORT_HBL)) { SeaImportHBLData seaImportHBLData =
			 * prepareDataForSeaImportHBL(myshipmentTrackJsonOutputData);
			 * model.addAttribute("seaImportHBLData", seaImportHBLData); return
			 * "seaImportHBL"; }
			 */
			String airports  = "";
			if (myshipmentTrackParams != null
					&& myshipmentTrackParams.getAction().equalsIgnoreCase(CommonConstant.AIR_EXPORT_HAWB)) {
				if (myshipmentTrackJsonOutputData.getDialogue().equalsIgnoreCase("success")) {

					AirExportHAWBData airExportHAWBData = prepareDataForAirExportHAWB(myshipmentTrackJsonOutputData);
					FlightInformationModel flight = new FlightInformationModel();
					Gson gson = new GsonBuilder().setPrettyPrinting().create();

					oagResponseDetails = oagFlightRequestParamCreate(airExportHAWBData);

					if (oagResponseDetails != null) {
						
						for(OAG oag:oagResponseDetails.getOagResponse() ) {
							airports = airports+" "+oag.getVcFlightNo()+"("+ oag.getVcDepAirportCode()+" to "+ oag.getVcArrAirportCode()+")\n";
						}


						airExportHAWBData.setFlightDetails(oagResponseDetails.getOagResponse());
						OAG inAirDetails = oagResponseDetails.getInAirFlightDetails();

						airExportHAWBData.setInAirFlightDetails(inAirDetails);

						if (inAirDetails != null) {

							FlightRoute[] flightRoute = gson.fromJson(
									airExportHAWBData.getInAirFlightDetails().getVcRoute(), FlightRoute[].class);

							flight = flightData(inAirDetails, flightRoute);
						} else {

							List<OAG> flightDetails = airExportHAWBData.getFlightDetails();
							OAG lastLeg = flightDetails.get(flightDetails.size() - 1);
							FlightRoute[] flightRoute = gson.fromJson(lastLeg.getVcRoute(), FlightRoute[].class);
							flight = flightData(lastLeg, flightRoute);

						}
					}

					model.addAttribute("airExportHAWBData", airExportHAWBData);
					model.addAttribute("flight", flight);
					model.addAttribute("currentFlight", new Gson().toJson(flight));
					model.addAttribute("oagResponseDetails", new Gson().toJson(oagResponseDetails));
					model.addAttribute("airports", airports);
					return "trackAirExpHawb";
				}
				return "trackSearchNoResult";
			}
			/*
			 * if(trackingRequestParam != null &&
			 * trackingRequestParam.getAction().equalsIgnoreCase(CommonConstant.
			 * AIR_IMPORT_HAWB)) { AirImportHAWBData airImportHAWBData =
			 * prepareDataForAirImportHAWB(myshipmentTrackJsonOutputData);
			 * model.addAttribute("airImportHAWBData", airImportHAWBData); return
			 * "airImportHAWB"; }
			 */
			/*
			 * if(trackingRequestParam != null &&
			 * trackingRequestParam.getAction().equalsIgnoreCase(CommonConstant.
			 * SEA_EXPORT_MBL)) {
			 * 
			 * } if(trackingRequestParam != null &&
			 * trackingRequestParam.getAction().equalsIgnoreCase(CommonConstant.
			 * SEA_IMPORT_MBL)) {
			 * 
			 * } if(trackingRequestParam != null &&
			 * trackingRequestParam.getAction().equalsIgnoreCase(CommonConstant.
			 * AIR_EXPORT_MAWB)) {
			 * 
			 * } if(trackingRequestParam != null &&
			 * trackingRequestParam.getAction().equalsIgnoreCase(CommonConstant.
			 * AIR_IMPORT_MAWB)) {
			 * 
			 * }
			 */
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug("Some exception occurred : ", e);
			return "trackingSearch";
		}
		return "trackingSearch";
	}

	@RequestMapping(value = "/getTrackingSeaPage", method = RequestMethod.GET)
	public String getTrackingSearchPage(ModelMap model) {

		logger.info("Method TrackingController.getTrackingSearchPage starts");

		TrackingRequestParam trackingRequestParam = new TrackingRequestParam();
		model.addAttribute("trackingRequestParam", trackingRequestParam);

		logger.info("Method TrackingController.getTrackingSearchPage ends.");

		return "trackingSearch";
	}

	/*
	 * @RequestMapping(value="/getTrackingPage", method=RequestMethod.GET) public
	 * String getTrackingPage(Model model){ BapiTrackDTO trackingDto = new
	 * BapiTrackDTO(); TrackingStatusParams trackingStatusParams = new
	 * TrackingStatusParams(); trackingDto.setZzmblmawbno("abc");
	 * trackingDto.setZzmblmawbdt(new Date()); trackingDto.setBudat(new Date());
	 * trackingDto.setZstuffingdate(new Date());
	 * trackingStatusParams.setBookingDate(dateToStringConversion(trackingDto.
	 * getZzmblmawbdt()));
	 * trackingStatusParams.setGoodsReceivedDate(dateToStringConversion(trackingDto.
	 * getZzmblmawbdt()));
	 * trackingStatusParams.setStuffingDate(dateToStringConversion(trackingDto.
	 * getZzmblmawbdt())); //trackingStatusParams.s
	 * model.addAttribute("trackingDto", trackingDto);
	 * model.addAttribute("trackingStatusParams", trackingStatusParams);
	 * 
	 * return "tracking"; }
	 */

	@SuppressWarnings("unused")
	private String dateToStringConversion(Date dateToConvert) {
		String pattern = "ddMMyyyy";
		SimpleDateFormat sdf = new SimpleDateFormat(pattern);
		String convertedString = sdf.format(dateToConvert);
		return convertedString;
	}

	@SuppressWarnings("unused")
	private TrackingStatusParams prepareTrackingStatusParamsData(List<ItDocumentHeaderBean> headerBeanLst) {
		TrackingStatusParams trackingStatusParams = new TrackingStatusParams();
		for (ItDocumentHeaderBean itDocumentHeaderBean : headerBeanLst) {
			if (itDocumentHeaderBean.getBooking_date() != null) {
				// trackingStatusParams.setBookingDate(dateToStringConversion(itDocumentHeaderBean.getBooking_date()));
				trackingStatusParams.setBookingDate(itDocumentHeaderBean.getBooking_date());
			}
			if (itDocumentHeaderBean.getGr_date() != null) {
				// trackingStatusParams.setGoodsReceivedDate(dateToStringConversion(itDocumentHeaderBean.getGr_date()));
				trackingStatusParams.setGoodsReceivedDate(itDocumentHeaderBean.getGr_date());
			}
			if (itDocumentHeaderBean.getShipment_date() != null) {
				// trackingStatusParams.setStuffingDate(dateToStringConversion(itDocumentHeaderBean.getShipment_date()));
				trackingStatusParams.setStuffingDate(itDocumentHeaderBean.getShipment_date());
			}
			if (itDocumentHeaderBean.getShipment_date() != null) {
				// trackingStatusParams.setShippedOnboardDate(dateToStringConversion(itDocumentHeaderBean.getShipment_date()));
				trackingStatusParams.setShippedOnboardDate(itDocumentHeaderBean.getShipment_date());
			}

		}

		return trackingStatusParams;
	}

	/*
	 * private Object
	 * prepareDataCorrespodningToThePage(MyshipmentTrackJsonOutputData
	 * myshipmentTrackJsonOutputData, String action){
	 * 
	 * if(action.equalsIgnoreCase("01")){ SeaExportHBLData seaExportHBLData = new
	 * SeaExportHBLData();
	 * seaExportHBLData.setMyshipmentTrackContainer(myshipmentTrackJsonOutputData.
	 * getMyshipmentTrackContainer());
	 * seaExportHBLData.setMyshipmentTrackHeader(myshipmentTrackJsonOutputData.
	 * getMyshipmentTrackHeader());
	 * seaExportHBLData.setMyshipmentTrackItem(myshipmentTrackJsonOutputData.
	 * getMyshipmentTrackItem());
	 * seaExportHBLData.setMyshipmentTrackSchedule(myshipmentTrackJsonOutputData.
	 * getMyshipmentTrackSchedule());
	 * 
	 * return seaExportHBLData; }
	 * 
	 * if(action.equalsIgnoreCase("02")){ SeaImportHBLData seaImportHBLData = new
	 * SeaImportHBLData();
	 * seaImportHBLData.setMyshipmentTrackHeader(myshipmentTrackJsonOutputData.
	 * getMyshipmentTrackHeader());
	 * seaImportHBLData.setMyshipmentTrackItem(myshipmentTrackJsonOutputData.
	 * getMyshipmentTrackItem());
	 * seaImportHBLData.setMyshipmentTrackSchedule(myshipmentTrackJsonOutputData.
	 * getMyshipmentTrackSchedule());
	 * 
	 * return seaImportHBLData; }
	 * 
	 * return null; }
	 */
	private SeaExportHBLData prepareDataForSeaExportHBL(MyshipmentTrackJsonOutputData myshipmentTrackJsonOutputData) {
		List<MyshipmentTrackContainerBeanDto> containerTrackDtoList = new ArrayList<MyshipmentTrackContainerBeanDto>();
		SeaExportHBLData seaExportHBLData = new SeaExportHBLData();
		// seaExportHBLData.setMyshipmentTrackContainer(myshipmentTrackJsonOutputData.getMyshipmentTrackContainer());
		seaExportHBLData.setMyshipmentTrackHeader(myshipmentTrackJsonOutputData.getMyshipmentTrackHeader());
		seaExportHBLData.setMyshipmentTrackItem(myshipmentTrackJsonOutputData.getMyshipmentTrackItem());
		seaExportHBLData.setMyshipmentTrackSchedule(myshipmentTrackJsonOutputData.getMyshipmentTrackSchedule());
		if (myshipmentTrackJsonOutputData.getMyshipmentTrackContainer() != null
				&& !myshipmentTrackJsonOutputData.getMyshipmentTrackContainer().isEmpty()) {

			for (MyshipmentTrackContainerBean mtc : myshipmentTrackJsonOutputData.getMyshipmentTrackContainer()) {
				MyshipmentTrackContainerBeanDto mtcbdto = new MyshipmentTrackContainerBeanDto();
				mtcbdto.setBl_no(mtc.getBl_no());
				mtcbdto.setCont_mode(mtc.getCont_mode());
				mtcbdto.setCont_no(mtc.getCont_no());
				mtcbdto.setCont_size(mtc.getCont_size());
				mtcbdto.setItem_no(mtc.getItem_no());
				mtcbdto.setPacd_pcs(mtc.getPacd_pcs());
				mtcbdto.setPacd_qty(mtc.getPacd_qty());
				mtcbdto.setPacd_vol(mtc.getPacd_vol());
				mtcbdto.setPacd_wt(mtc.getPacd_wt());
				mtcbdto.setSeal_no(mtc.getSeal_no());
				mtcbdto.setSeq_no(mtc.getSeq_no());
				mtcbdto.setSo_no(mtc.getSo_no());
				containerTrackDtoList.add(mtcbdto);
			}
		}
		seaExportHBLData.setMyshipmentTrackContainer(containerTrackDtoList);
		// Setting the Po and style
		if (seaExportHBLData.getMyshipmentTrackContainer() != null
				&& !seaExportHBLData.getMyshipmentTrackContainer().isEmpty()) {
			for (MyshipmentTrackContainerBeanDto myshipmentTrackContainerBean : seaExportHBLData
					.getMyshipmentTrackContainer()) {
				for (MyshipmentTrackItemBean myshipmentTrackItemBean : seaExportHBLData.getMyshipmentTrackItem()) {
					if ((myshipmentTrackContainerBean.getSo_no().equalsIgnoreCase(myshipmentTrackItemBean.getSo_no()))
							&& (myshipmentTrackContainerBean.getItem_no()
									.equalsIgnoreCase(myshipmentTrackItemBean.getItem_no()))) {
						myshipmentTrackContainerBean.setPo_no(myshipmentTrackItemBean.getPo_no());
						myshipmentTrackContainerBean.setStyle(myshipmentTrackItemBean.getStyle());
					}
				}
			}
		}

		// preparePORelatedInfo(seaExportHBLData.getMyshipmentTrackContainer(),
		// seaExportHBLData.getMyshipmentTrackItem())
		return seaExportHBLData;
	}

	private SeaImportHBLData prepareDataForSeaImportHBL(MyshipmentTrackJsonOutputData myshipmentTrackJsonOutputData) {
		SeaImportHBLData seaImportHBLData = new SeaImportHBLData();
		seaImportHBLData.setMyshipmentTrackHeader(myshipmentTrackJsonOutputData.getMyshipmentTrackHeader());
		seaImportHBLData.setMyshipmentTrackItem(myshipmentTrackJsonOutputData.getMyshipmentTrackItem());
		seaImportHBLData.setMyshipmentTrackSchedule(myshipmentTrackJsonOutputData.getMyshipmentTrackSchedule());

		return seaImportHBLData;
	}

	private AirImportHAWBData prepareDataForAirImportHAWB(MyshipmentTrackJsonOutputData myshipmentTrackJsonOutputData) {

		AirImportHAWBData airImportHAWBData = new AirImportHAWBData();
		airImportHAWBData.setMyshipmentTrackHeader(myshipmentTrackJsonOutputData.getMyshipmentTrackHeader());
		airImportHAWBData.setMyshipmentTrackItem(myshipmentTrackJsonOutputData.getMyshipmentTrackItem());
		airImportHAWBData.setMyshipmentTrackSchedule(myshipmentTrackJsonOutputData.getMyshipmentTrackSchedule());

		return airImportHAWBData;
	}

	private AirExportHAWBData prepareDataForAirExportHAWB(MyshipmentTrackJsonOutputData myshipmentTrackJsonOutputData) {

		AirExportHAWBData airExportHAWBData = new AirExportHAWBData();
		airExportHAWBData.setMyshipmentTrackHeader(myshipmentTrackJsonOutputData.getMyshipmentTrackHeader());
		airExportHAWBData.setMyshipmentTrackItem(myshipmentTrackJsonOutputData.getMyshipmentTrackItem());
		airExportHAWBData.setMyshipmentTrackSchedule(myshipmentTrackJsonOutputData.getMyshipmentTrackSchedule());

		return airExportHAWBData;
	}

	public OAGResponseDetails oagFlightRequestParamCreate(AirExportHAWBData airExportHAWBData) {

		OAGResponseDetails oagResponseDetails = new OAGResponseDetails();

		List<MyshipmentTrackScheduleBean> myshipmentTrackScheduleList = airExportHAWBData.getMyshipmentTrackSchedule();
		OAGRequest oagreq = new OAGRequest();
		List<FlightParamDetails> oaglist = new ArrayList<FlightParamDetails>();
		int count = 1;

		Hashtable<String, Integer> flightLeg = new Hashtable<String, Integer>();

		for (MyshipmentTrackScheduleBean mstsb : myshipmentTrackScheduleList) {

			FlightParamDetails oag1 = new FlightParamDetails();

			if (!mstsb.getCar_no().equalsIgnoreCase("") && !mstsb.getEtd().equalsIgnoreCase("")) {
				oag1.setFlightNumber(mstsb.getCar_no().replaceAll("[\\-\\+\\.\\^:,\\s+]", ""));
				oag1.setFlightDate(changeDateFormat(mstsb.getEtd()));
				oag1.setLeg(count);
				oaglist.add(oag1);
				flightLeg.put(mstsb.getCar_no().replaceAll("[\\-\\+\\.\\^:,\\s+]", ""), count);
				count++;
				oag1 = new FlightParamDetails();
			}

		}

		if (oaglist.size() != 0) {
			oagreq.setFlightParamDetailsList(oaglist);
			List<OAG> oagResponses = oagService.getFlightDetails(oagreq);

			oagResponseDetails.setOagResponse(oagResponses);

			for (OAG or : oagResponses) {

				String flightNo = or.getVcFlightNo();
				int leg = flightLeg.get(flightNo);
				or.setLeg(leg);

				if (or.getVcFlightStatus() != null) {

					if (!or.getSystemMessage().equalsIgnoreCase(CommonConstant.ARRIVED)) {

						if (or.getVcFlightStatus().equalsIgnoreCase(CommonConstant.IN_AIR)
								|| or.getVcFlightStatus().equalsIgnoreCase(CommonConstant.OUT_GATE)
								|| or.getVcFlightStatus().equalsIgnoreCase(CommonConstant.SCHEDULED)) {

							oagResponseDetails.setInAirFlightDetails(or);
						}
					}
					else {
						or.setVcFlightStatus(CommonConstant.ARRIVED);
						or.setVcRelativeTime("");
					}
				}
			}

			if (oagResponses != null) {

				logger.info("------Flight details retrived-----");
				logger.info("Returning flight details");
			} else {

				logger.info("------Flight details did not retrived-------");
				logger.info("-----------Returning null----------------");
			}

			return oagResponseDetails;
		} else {

			return null;
		}

	}

	public String changeDateFormat(String date) {

		SimpleDateFormat dateFortmateInput = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat dateFormateOutput = new SimpleDateFormat("yyyy-MM-dd");
		Date dateObj;
		String convertedDate = "";

		try {

			dateObj = dateFortmateInput.parse(date);
			convertedDate = dateFormateOutput.format(dateObj);
		} catch (ParseException e) {

			logger.info("Error occured while parsing the date: " + e.getMessage());
			e.printStackTrace();
		}

		return convertedDate;

	}

	public FlightInformationModel flightData(OAG flightLeg, FlightRoute[] flightRoute) {

		FlightInformationModel flightDetails = new FlightInformationModel();
		flightDetails.setAircraft(flightLeg.getVcAircraftType());
		flightDetails.setAirlines(flightLeg.getVcAirlineName());
		flightDetails.setArrCode(flightLeg.getVcArrAirportCode());
		flightDetails.setArrAirport(flightLeg.getVcArrAirportName());
		flightDetails.setArrCity(flightLeg.getVcArrAirportCityName());
		flightDetails.setDepCode(flightLeg.getVcDepAirportCode());
		flightDetails.setDepAirport(flightLeg.getVcDepAirportName());
		flightDetails.setDepCity(flightLeg.getVcDepAirportCityName());

		if (flightRoute != null) {
			flightDetails.setDepLat(flightRoute[0].getLatitude());
			flightDetails.setDepLong(flightRoute[0].getLongitude());
			flightDetails.setArrLat(flightRoute[flightRoute.length - 1].getLatitude());
			flightDetails.setArrLong(flightRoute[flightRoute.length - 1].getLongitude());
		}
		flightDetails.setFlightLat(flightLeg.getVcAirporneLatitude() != null ? flightLeg.getVcAirporneLatitude() : "");
		flightDetails
				.setFlightLong(flightLeg.getVcAirporneLongitude() != null ? flightLeg.getVcAirporneLongitude() : "");

		flightDetails.setFlightNo(flightLeg.getVcFlightNo());
//		if (flightLeg.getSystemMessage().equalsIgnoreCase(CommonConstant.ARRIVED)) {
//			flightDetails.setFlightStatus(CommonConstant.ARRIVED);
//			flightDetails.setRemainingTime("");
//		} else {
			flightDetails.setFlightStatus(flightLeg.getVcFlightStatus());
			flightDetails.setRemainingTime(flightLeg.getVcRelativeTime());

	//	}
		if (flightLeg.getDtArrGtActDate() != null) {
			flightDetails.setArrGTDate(flightLeg.getDtArrGtActDate());
			flightDetails.setArrGTTime(flightLeg.getVcArrGtActTime());
		} else if (flightLeg.getDtArrGtSchDate() != null) {
			flightDetails.setArrGTDate(flightLeg.getDtArrGtSchDate());
			flightDetails.setArrGTTime(flightLeg.getVcArrGtSchTime());
		} else {
			flightDetails.setArrGTDate(flightLeg.getDtArrGtEstDate());
			flightDetails.setArrGTTime(flightLeg.getVcArrGtEstTime());

		}
		if (flightLeg.getDtDepGtActDate() != null) {
			flightDetails.setDepGTDate(flightLeg.getDtDepGtActDate());
			flightDetails.setDepGTTime(flightLeg.getVcDepGtActTime());
		} else if (flightLeg.getDtDepGtSchDate() != null) {
			flightDetails.setDepGTDate(flightLeg.getDtDepGtSchDate());
			flightDetails.setDepGTTime(flightLeg.getVcDepGtSchTime());
		} else {
			flightDetails.setDepGTDate(flightLeg.getDtDepGtEstDate());
			flightDetails.setDepGTTime(flightLeg.getVcDepGtEstTime());

		}
		if (flightLeg.getDtArrRtActDate() != null) {
			flightDetails.setArrRTDate(flightLeg.getDtArrRtActDate());
			flightDetails.setArrRTTime(flightLeg.getVcArrRtActTime());
		} else if (flightLeg.getDtArrRtSchDate() != null) {
			flightDetails.setArrRTDate(flightLeg.getDtArrRtSchDate());
			flightDetails.setArrRTTime(flightLeg.getVcArrRtSchTime());
		} else {
			flightDetails.setArrRTDate(flightLeg.getDtArrRtEstDate());
			flightDetails.setArrRTTime(flightLeg.getVcArrRtEstTime());

		}
		if (flightLeg.getDtDepRtActDate() != null) {
			flightDetails.setDepRTDate(flightLeg.getDtDepRtActDate());
			flightDetails.setDepRTTime(flightLeg.getVcDepRtActTime());
		} else if (flightLeg.getDtDepGtSchDate() != null) {
			flightDetails.setDepRTDate(flightLeg.getDtDepRtSchDate());
			flightDetails.setDepRTTime(flightLeg.getVcDepRtSchTime());
		} else {
			flightDetails.setDepRTDate(flightLeg.getDtDepRtEstDate());
			flightDetails.setDepRTTime(flightLeg.getVcDepRtEstTime());

		}
		// DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd
		// HH:mm:ss");
		DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
		if (flightLeg.getDtUpdatedAt() != null) {
			flightDetails.setUpdateTime(flightLeg.getDtUpdatedAt().toString(fmt) + "(BDT TIMEZONE)");
			// flightDetails.setUpdateTime(flightLeg.getDtUpdatedAt().format(formatter) +
			// "(BDT TIMEZONE)");
		}
		flightDetails.setTailNumber(flightLeg.getVcTailNumber());
		return flightDetails;

	}
}
