package com.myshipment.controller;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.myshipment.dto.LoginDTO;
import com.myshipment.model.AirBillOfLandingJsonData;
import com.myshipment.model.BapiRet1;
import com.myshipment.model.NegoBillLanding;
import com.myshipment.model.NegoBillLandingI;
import com.myshipment.model.RequestParams;
import com.myshipment.model.ShippingReport;
import com.myshipment.model.airBillOfLadingPDFGeneration;
import com.myshipment.util.DateUtil;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;
/*
 * @Ranjeet Kumar
 */
@Controller
public class AirBillOfLandingController {

	final static Logger logger = Logger.getLogger(AirBillOfLandingController.class);

	@Autowired
	private RestService restService;

//	@RequestMapping(value = "/getAirBillOfLandingData", method = RequestMethod.POST)
//	public String getAirBillOfLandingDetail(@ModelAttribute("reqParams") RequestParams reqParams, ModelMap model, HttpSession session){
//
//		logger.info("Method AirBillOfLandingController.getAirBillOfLandingDetail starts.");
//		if(reqParams != null){
//			StringBuffer webServiceUrl = new StringBuffer(RestUtil.AIR_BILL_OF_LANDING);
//			
//			LoginDTO loginDto = (LoginDTO)session.getAttribute("loginDetails");
//			String customerNo = null;
//			String distChannel=null;
//			String division=null;
//			String salesOrg=null;
//			if(loginDto != null){
//				customerNo = loginDto.getLoggedInUserName();
//				distChannel=loginDto.getDisChnlSelected();
//				division=loginDto.getDivisionSelected();
//				salesOrg=loginDto.getSalesOrgSelected();
//				logger.info("Customer No found as : "+ customerNo);
//			}
//			
//			reqParams.setReq2(salesOrg);
//			reqParams.setReq3(distChannel);
//			reqParams.setReq4(division);
//			reqParams.setReq5(customerNo);
//			
//			AirBillOfLandingJsonData airBillOfLandingJsonData = restService.postForObject(RestUtil.prepareUrlForService(webServiceUrl).toString(), reqParams, AirBillOfLandingJsonData.class);				
//			
//			ObjectMapper mapper=new ObjectMapper();
//			try{
//			System.out.println(mapper.writeValueAsString(airBillOfLandingJsonData));
//			}catch(Exception ex){
//				ex.printStackTrace();
//			}
//			if(airBillOfLandingJsonData != null){
//				model.addAttribute("airBillOfLandingJsonData", airBillOfLandingJsonData);
//				logger.info("Method AirBillOfLandingController.getAirBillOfLandingDetail ends.");
//
//				return "billOfLanding";
//			}
//			else{
//				logger.info("Value of airBillOfLandingJsonData is : " +airBillOfLandingJsonData);
//			}
//		}
//		return null;
//	}
	
	@RequestMapping(value = "/AirNonNegoBillOfLading", method = RequestMethod.POST)
	public ModelAndView getAirBillOfLandingDetail(@ModelAttribute("reqParams") RequestParams reqParams, ModelMap model,
			HttpSession session, HttpServletResponse response, HttpServletRequest request, ModelAndView modelAndView,
			RedirectAttributes redirecAttributes, BindingResult result) throws Exception {

		logger.info("Method AirBillOfLandingController.getAirBillOfLandingDetail starts.");
		try {
			if (reqParams != null) {
				StringBuffer webServiceUrl = new StringBuffer(RestUtil.AIR_BILL_OF_LANDING);

				LoginDTO loginDto = (LoginDTO) session.getAttribute("loginDetails");
				String customerNo = null;
				String distChannel = null;
				String division = null;
				String salesOrg = null;
				if (loginDto != null) {
					customerNo = loginDto.getLoggedInUserName();
					distChannel = loginDto.getDisChnlSelected();
					division = loginDto.getDivisionSelected();
					salesOrg = loginDto.getSalesOrgSelected();
					logger.info("Customer No found as : " + customerNo);
				}

				reqParams.setReq1(reqParams.getReq1().toUpperCase());
				reqParams.setReq2(salesOrg);
				reqParams.setReq3(distChannel);
				reqParams.setReq4(division);
				reqParams.setReq5(customerNo);

				AirBillOfLandingJsonData airBillOfLandingJsonData = restService.postForObject(
						RestUtil.prepareUrlForService(webServiceUrl).toString(), reqParams,
						AirBillOfLandingJsonData.class);
				
/*				if(airBillOfLandingJsonData.getItHeader().isEmpty() && airBillOfLandingJsonData.getItShipper().isEmpty() &&
						airBillOfLandingJsonData.getItConsignee().isEmpty() && airBillOfLandingJsonData.getItAddress().isEmpty()) {
					modelAndView.setViewName("redirect:/getAirBillOfLandingPage");
					redirecAttributes.addFlashAttribute("message", "HBL number is not valid! Please enter a valid HBL number");
					return modelAndView;
				}*/
				if (airBillOfLandingJsonData.getBapiReturn1() != null) {
					if (airBillOfLandingJsonData.getBapiReturn1().getType().equalsIgnoreCase("E")) {
						BapiRet1 bapiRetAirNonNego = airBillOfLandingJsonData.getBapiReturn1();
						modelAndView.setViewName("redirect:/getAirBillOfLandingPage");
						redirecAttributes.addFlashAttribute("message", bapiRetAirNonNego.getMessage());
						return modelAndView;
					}
				}

				ObjectMapper mapper = new ObjectMapper();
				try {
					System.out.println(mapper.writeValueAsString(airBillOfLandingJsonData));
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				if (airBillOfLandingJsonData != null) {
					String reportType = request.getParameter("type");
					if(salesOrg.equalsIgnoreCase("2200")){
						reportType = reportType == null ? "airNNBBNX" : reportType;
					}
					else {
						reportType = reportType == null ? "airNNB" : reportType;
					}
					airBillOfLadingPDFGeneration pdfData = new airBillOfLadingPDFGeneration();
					List<airBillOfLadingPDFGeneration> actualPdfData = new ArrayList<airBillOfLadingPDFGeneration>();
					List<airBillOfLadingPDFGeneration> actualPdfData2 = new ArrayList<airBillOfLadingPDFGeneration>();
					Map<String, Object> parameterMap = new HashMap<String, Object>();

					List<NegoBillLanding> tabledata = airBillOfLandingJsonData.getItShipperBank();
					List<NegoBillLanding> tabledata1 = airBillOfLandingJsonData.getItShipper();
					List<NegoBillLanding> tabledata2 = airBillOfLandingJsonData.getItConsigneeBank();
					List<NegoBillLanding> tabledata3 = airBillOfLandingJsonData.getItConsignee();
					List<NegoBillLanding> tabledata4 = airBillOfLandingJsonData.getItNotify();
					List<NegoBillLanding> tabledata5 = airBillOfLandingJsonData.getItNotifyCountry();
					List<NegoBillLanding> tabledata6 = airBillOfLandingJsonData.getItHeader();
					List<NegoBillLanding> tabledata7 = airBillOfLandingJsonData.getItPol();
					List<NegoBillLanding> tabledata8 = airBillOfLandingJsonData.getItPod();
					List<NegoBillLanding> tabledata9 = airBillOfLandingJsonData.getItPolDel();
					List<NegoBillLanding> tabledata10 = airBillOfLandingJsonData.getItPor();
					List<ShippingReport> tabledata11 = airBillOfLandingJsonData.getItAddress();
					List<NegoBillLandingI> tabledata12 = airBillOfLandingJsonData.getItVbap();
					List<NegoBillLanding> tabledata13 = airBillOfLandingJsonData.getItPlaceOfIssue();
					List<NegoBillLanding> tabledata14 = airBillOfLandingJsonData.getItShipperCountry();
					List<NegoBillLanding> tabledata15 = airBillOfLandingJsonData.getItConsigneeCountry();
					List<NegoBillLanding> tabledata16 = airBillOfLandingJsonData.getItCompCountry();
					List<NegoBillLanding> tabledata17 = airBillOfLandingJsonData.getItShbCountry();
					List<NegoBillLanding> tabledata18 = airBillOfLandingJsonData.getItCbCountry();

					NegoBillLanding a, a1, a2, a3, a4, a5, a6, a7, a8, a9, a13, a14, a15, a16, a17, a18;
					ShippingReport a11;
					NegoBillLandingI a12,hscodeTable;
					



					SimpleDateFormat isoFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					Date date = new Date();
					isoFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
					pdfData.setDate(isoFormat.format(date));

					if (tabledata != null && !tabledata.isEmpty()) {
						a = tabledata.get(0);// shipperbank
						pdfData.setShipperName1(a.getName1());
						pdfData.setShipperName2(a.getName2());
						pdfData.setShipperName3(a.getName3());
						pdfData.setShipperName4(a.getName4());
						pdfData.setShipperCity1(a.getCity1());
					}
					if (tabledata1 != null && !tabledata1.isEmpty()) {
						a1 = tabledata1.get(0);// shipper
						pdfData.setShipper2Name1(a1.getName1());
						pdfData.setShipper2Name2(a1.getName2());
						pdfData.setShipper2Name3(a1.getName3());
						pdfData.setShipper2Name4(a1.getName4());
						pdfData.setShipper2City1(a1.getCity1());
					}
					if (tabledata2 != null && !tabledata2.isEmpty()) {
						a2 = tabledata2.get(0);// consigneeBank
						pdfData.setConsigneeName1(a2.getName1());
						pdfData.setConsigneeName2(a2.getName2());
						pdfData.setConsigneeName3(a2.getName3());
						pdfData.setConsigneeName4(a2.getName4());
						pdfData.setConsigneeCity1(a2.getCity1());
					}
					if (tabledata18 != null && !tabledata18.isEmpty()) {
						a18 = tabledata18.get(0);
						pdfData.setConsigneeCountry(a18.getLandX());
					}
					if (tabledata4 != null && !tabledata4.isEmpty()) {
						a4 = tabledata4.get(0);// notify
						pdfData.setNotifyName1(a4.getName1());
						pdfData.setNotifyName2(a4.getName2());
						pdfData.setNotifyName3(a4.getName3());
						pdfData.setNotifyName4(a4.getName4());
						pdfData.setNotifyCity1(a4.getCity1());
					}
					if (tabledata5 != null && !tabledata5.isEmpty()) {
						a5 = tabledata5.get(0);// notifyCountry
						pdfData.setNotifyCountry(a5.getLandX());
					}
					if (tabledata3 != null && !tabledata3.isEmpty()) {
						a3 = tabledata3.get(0);// consignee
						pdfData.setConsignee2Name1(a3.getName1());
						pdfData.setConsignee2Name2(a3.getName2());
						pdfData.setConsignee2Name3(a3.getName3());
						pdfData.setConsignee2Name4(a3.getName4());
						pdfData.setConsignee2City1(a3.getCity1());
					}
					if (tabledata6 != null && !tabledata6.isEmpty()) {
						a6 = tabledata6.get(0);// header
						String text_shipmark = a6.getDesc_Z112().replaceAll("\\t+", "");
						String text_desc = a6.getDesc_Z023().replaceAll("\\t+", "");
						text_shipmark = text_shipmark.replaceAll("\\n+", " ");
						text_desc = text_desc.replaceAll("\\n+", " ");
						text_shipmark = text_shipmark.replaceAll("\\r+", " ");
						text_desc = text_desc.replaceAll("\\r+", " ");
						pdfData.setDescription(text_desc);
						pdfData.setShippingMark(text_shipmark);
						pdfData.setFlightNo(a6.getZzAirlineNo1());
						pdfData.setFlightDt(DateUtil.formatDateToString(a6.getZzLoadingDt()));
						pdfData.setMasterBil(a6.getZzMblMabwNO());
						pdfData.setBiilNo(a6.getZzHblHawbNo());
						pdfData.setAccountInfo(a6.getZzFreightModes());
						pdfData.setInvoice(a6.getZzCommInvNo());
						pdfData.setType(a6.getZzLcpottNoType());
						pdfData.setNo(a6.getZzLcpotNo());
						pdfData.setExpNo(a6.getZzExpNo());
						pdfData.setScbNo(a6.getZzShipping_Bl_No());
						pdfData.setDate1(epochTimeToDate(a6.getZzCommInvDt()));
						pdfData.setDate2(epochTimeToDate(a6.getZzLcdt()));
						pdfData.setDate3(epochTimeToDate(a6.getZzExpDt()));
						pdfData.setDate4(DateUtil.formatDateToString(a6.getZzShipping_Bl_Dt()));
						pdfData.setExcuted(DateUtil.formatDateToString(a6.getZzHblHawBdt()));
					}
					if (tabledata7 != null && !tabledata7.isEmpty()) {
						a7 = tabledata7.get(0);// pol
						pdfData.setAirportOfDep(a7.getZzPortName());
					}
					if (tabledata8 != null && !tabledata8.isEmpty()) {
						a8 = tabledata8.get(0);// pod
						pdfData.setRequestedRouting(a8.getZzPortName());
					}
					if (tabledata9 != null && !tabledata9.isEmpty()) {
						a9 = tabledata9.get(0);// poldel
						pdfData.setAirportOfDes(a9.getZzPortName());
					}
					if (tabledata10 != null && !tabledata10.isEmpty()) {
					}
					if (tabledata11 != null && !tabledata11.isEmpty()) {
						a11 = tabledata11.get(0);// address
						pdfData.setAddressName1(a11.getName1());
						pdfData.setAddressName2(a11.getName2());
						pdfData.setAddressName3(a11.getName3());
						pdfData.setAddressName4(a11.getName4());
						pdfData.setAddressCity1(a11.getCity1());
						pdfData.setStreeNo(a11.getStreetNo());
					}
					if (tabledata13 != null && !tabledata13.isEmpty()) {
						a13 = tabledata13.get(0);// placeofissue
						pdfData.setAt(a13.getCity1());
					}
					if (tabledata14 != null && !tabledata14.isEmpty()) {
						a14 = tabledata14.get(0);// shippercountry
						pdfData.setShipper2Country(a14.getLandX());
					}
					if (tabledata15 != null && !tabledata15.isEmpty()) {
						a15 = tabledata15.get(0);// consigneecountry
						pdfData.setConsignee2Country(a15.getLandX());
					}
					if (tabledata16 != null && !tabledata16.isEmpty()) {
						a16 = tabledata16.get(0);// comcountry
						pdfData.setAddressCountry(a16.getLandX());
					}
					if (tabledata17 != null && !tabledata17.isEmpty()) {
						a17 = tabledata17.get(0);// shbcountry
						pdfData.setShipperCountry(a17.getLandX());
					}
					
					String HSCODE=null;
					if(tabledata12 != null && !tabledata12.isEmpty()) {
						for(int c=0;c < tabledata12.size(); c++) {
							hscodeTable = tabledata12.get(c);
							if(c==0){
								HSCODE= hscodeTable.getZzHsCode();
							}
							else {
								HSCODE= HSCODE+ "," + hscodeTable.getZzHsCode();
							}
						}
						
						pdfData.setHsCode(HSCODE);
					}

					pdfData.setIata("");
					actualPdfData.add(pdfData);

					if (tabledata6 != null && !tabledata6.isEmpty()) {
						for (int w = 0; w < tabledata6.size(); w++) {
							NegoBillLanding table = tabledata6.get(w);
							airBillOfLadingPDFGeneration pdfTable = new airBillOfLadingPDFGeneration();
							Double i = Double.parseDouble(table.getZzSoquantity());
							pdfTable.setNoOfPcs(i.intValue());
							pdfTable.setGw(round(String.valueOf(table.getZzGrossWeigth().doubleValue())));
							pdfTable.setKg(round(table.getNtGew()));
							pdfTable.setCw(round(table.getZzTot_Chrg_Wt()));
							pdfTable.setRate("As Arranged");
							pdfTable.setTotal(round(table.getZzTotalVolWt()));
							actualPdfData.add(pdfTable);
						}
					}

					if (tabledata12 != null && !tabledata12.isEmpty()) {
						for (int z = 0; z < tabledata12.size(); z++) {
							a12 = tabledata12.get(z);// itvap
							airBillOfLadingPDFGeneration pdfTable2 = new airBillOfLadingPDFGeneration();
							pdfTable2.setDimnlength((int) Double.parseDouble(a12.getZzLenght()));
							pdfTable2.setDimnHeight((int) Double.parseDouble(a12.getZzHeight()));
							pdfTable2.setDimnWidth((int) Double.parseDouble(a12.getZzWidth()));
							pdfTable2.setDimn1((int) Double.parseDouble(a12.getZzTotalNoPcs()));
							pdfTable2.setDimn2(a12.getZzQuantityUom());
							actualPdfData2.add(pdfTable2);
						}
					}

					JRDataSource pdfDataSource = new JRBeanCollectionDataSource(actualPdfData);
					JRDataSource pdfDataSource2 = new JRBeanCollectionDataSource(actualPdfData2);
					
			

					parameterMap.put("datasource", pdfDataSource);
					parameterMap.put("datasource2", pdfDataSource2);

					ServletContext context = request.getSession().getServletContext();
					String path = context.getRealPath("/") + "";
					parameterMap.put("Context", path);
					modelAndView = new ModelAndView(reportType, parameterMap);

					return modelAndView;
				}
			}
			return null;
		} catch (Exception ex) {
			modelAndView.setViewName("redirect:/getAirBillOfLandingPage");
			redirecAttributes.addFlashAttribute("message", "HAWB number is not valid! Please enter a valid HBL number");
			return modelAndView;
		}
	}

	
	
	@RequestMapping(value="/getAirBillOfLandingPage", method = RequestMethod.GET )
	public String getAirBillOfLanding(ModelMap model, HttpSession session) {
		logger.info(this.getClass().getName() + " Returning Login Page");
		
		RequestParams reqParams = new RequestParams();
		model.addAttribute("reqParams", reqParams);
		
		LoginDTO loginDto = (LoginDTO) session.getAttribute("loginDetails");
		
		String division = loginDto.getDivisionSelected();
		
		if(division.equals("AR")) {
			return "airBillOfLandingSearch";
		} else if(division.equals("SE")) {
			return "redirect:/getSeaBillOfLandingPage";
		}
		
		return "airBillOfLandingSearch";

	}
	
	//hamid
	//22.03.2017
	@RequestMapping(value = "/getAirBillOfLandingFromUrl", method = RequestMethod.GET)
	public ModelAndView getAirBillOfLandingDetailFromUrl(@ModelAttribute("reqParams") RequestParams reqParams, ModelMap model, HttpSession session, @RequestParam("searchString") String searchValue,
			HttpServletResponse response,HttpServletRequest request,
			ModelAndView modelAndView, RedirectAttributes redirecAttributes, BindingResult result) throws Exception{

		logger.info("Method AirBillOfLandingController.getAirBillOfLandingDetail starts.");
		if(reqParams != null){
			LoginDTO loginDto = (LoginDTO)session.getAttribute("loginDetails");
			String customerNo = null;
			String distChannel=null;
			String division=null;
			String salesOrg=null;
			if(loginDto != null){
				customerNo = loginDto.getLoggedInUserName();
				distChannel=loginDto.getDisChnlSelected();
				division=loginDto.getDivisionSelected();
				salesOrg=loginDto.getSalesOrgSelected();
				logger.info("Customer No found as : "+ customerNo);
			}
			
			reqParams.setReq1(searchValue);
			reqParams.setReq2(salesOrg);
			reqParams.setReq3(distChannel);
			reqParams.setReq4(division);
			reqParams.setReq5(customerNo);
			return getAirBillOfLandingDetail(reqParams, model, session, response, request, modelAndView, redirecAttributes, result);
			
/*			AirBillOfLandingJsonData airBillOfLandingJsonData = restService.postForObject(RestUtil.prepareUrlForService(webServiceUrl).toString(), reqParams, AirBillOfLandingJsonData.class);				
			
			ObjectMapper mapper=new ObjectMapper();
			try{
			System.out.println(mapper.writeValueAsString(airBillOfLandingJsonData));
			}catch(Exception ex){
				ex.printStackTrace();
			}
			if(airBillOfLandingJsonData != null){
				model.addAttribute("airBillOfLandingJsonData", airBillOfLandingJsonData);
				logger.info("Method AirBillOfLandingController.getAirBillOfLandingDetail ends.");

				return "billOfLanding";
			}
			else{
				logger.info("Value of airBillOfLandingJsonData is : " +airBillOfLandingJsonData);
			}*/
		}
		return null;
	}
	private String epochTimeToDate(String epochString) {
		String dateReturn = "";
		long epoch;
		if(epochString != null) {
			epoch = Long.parseLong(epochString);
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			dateReturn = sdf.format(new Date(epoch));
		}
		return dateReturn;
	}
	private String round(String df)
	{
		DecimalFormat r = new DecimalFormat("0.00");
		Double d=Double.valueOf(df);
		String s=r.format(d);
		return s;
	}
	
}
