/**
*
*@author Ahmad Naquib
*/
package com.myshipment.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.myshipment.model.ContainerInformation;
import com.myshipment.model.ContainerLocationDetails;
import com.myshipment.model.ContainerTrackingParams;
import com.myshipment.model.ContainerTrackingSummary;
import com.myshipment.model.ContainerTypeInformation;
import com.myshipment.service.IContainerTrackingService;

@Controller
public class ContainerTrackingController {
	
	Logger logger = Logger.getLogger(ContainerTrackingController.class);

	@Autowired
	IContainerTrackingService containerTrackingService;
	
	@RequestMapping(value="/containerTracking", method=RequestMethod.GET)
	public String getContainerTrackingPage(Model model, HttpServletResponse response,HttpServletRequest request) {
		try {
			logger.info("Container Tracking controller called");
			ContainerTrackingParams params = new ContainerTrackingParams();
			model.addAttribute("containerTrackingParams", params);
			
		} catch (Exception e) {
			logger.error("Container Tracking initialization failed : " + e);
			e.printStackTrace();
		}
		return "containerTracking";
	}
	
	@RequestMapping(value="/getContainerDetails", method=RequestMethod.POST)
	public String getContainerDetails(@ModelAttribute("containerTrackingParams") ContainerTrackingParams containerTrackingParams, Model model, HttpServletRequest request) {
		try {
			//String containerNumber = "CAIU2578094";
			List<ContainerInformation> containerInfoList = containerTrackingService.getContainerInfo(containerTrackingParams.getContainerNumber());
			List<ContainerTrackingSummary> summaryList = containerTrackingService.getContainerSummary(containerTrackingParams.getContainerNumber());
			List<ContainerLocationDetails> detailsList = containerTrackingService.getContainerDetails(containerTrackingParams.getContainerNumber());
			List<ContainerTypeInformation> typeList = containerTrackingService.getContainerTypeInfo(containerTrackingParams.getContainerNumber());
			ContainerLocationDetails lastMovementDetails = getLastMovement(detailsList);
			if(detailsList != null) {
				model.addAttribute("containerInfo", containerInfoList.get(0));
				model.addAttribute("containerSummaryList", summaryList);
				model.addAttribute("containerDetailsList", detailsList);
				model.addAttribute("containerTypeList", typeList.get(0));
				model.addAttribute("lastMovement", lastMovementDetails);
				model.addAttribute("containerTrackingParams", containerTrackingParams);
				return "containerTracking";
			} else {
				model.addAttribute("containerTrackingParams", containerTrackingParams);
				return "containerTracking";
			}
			
		} catch (Exception e) {
			logger.error("Error occured : " + e);
			e.printStackTrace();
		}
		return null;
	}
	
	public ContainerLocationDetails getLastMovement(List<ContainerLocationDetails> detailsList) {
		int maxDate = 0;
		ContainerLocationDetails lastMovementDetails = null;
		for (ContainerLocationDetails containerLocationDetails : detailsList) {
			try{
				if(containerLocationDetails.getDtmCode().equals("145")) {
					String tempDate = containerLocationDetails.getActivityDate();
					int date = Integer.parseInt(tempDate.substring(4, tempDate.length()));
					maxDate = Math.max(maxDate, date);
					if(date == maxDate) {
						lastMovementDetails = containerLocationDetails;
					}
				}
			} catch(Exception e) {
				continue;
			}
			
		}
		String activity = lastMovementDetails.getLocDetails();
		if(activity != null || activity.equalsIgnoreCase("")) {
			String [] activityByWord = activity.split(" ");
			activity = activityByWord[activityByWord.length-1];
			lastMovementDetails.setLocDetails(activity);
		}
		
		
		return lastMovementDetails;
	}
	
}
