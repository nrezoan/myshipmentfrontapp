package com.myshipment.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.myshipment.dto.SearchPoDTO;
import com.myshipment.model.SegPurchaseOrder;
import com.myshipment.service.ISearchPOSerivce;
/*
 * @ Ranjeet Kumar
 */
@Controller
public class SearchPOController {

	private static final Logger logger = Logger.getLogger(SearchPOController.class);
	
	@Autowired
	private ISearchPOSerivce iSearchPOSerivce;
	
	@RequestMapping(value = "/getSearchPoPage", method = RequestMethod.GET)
	public String getPOSearchPage(Map<String, Object> model, HttpSession httpSession,@ModelAttribute("searchPoDTO") SearchPoDTO searchPoDto)
	{

		logger.info("Inside SearchPOController.getPOSearchPage method.");
		
		if(searchPoDto.getRedirectedRequest() != null && searchPoDto.getRedirectedRequest().equalsIgnoreCase("yes"))
		{
			model.put("searchPoDTO", searchPoDto);
			List<SegPurchaseOrder> purchaseOrderLst = iSearchPOSerivce.getLineItemPosBasedOnParameter(searchPoDto);
			model.put("purchaseOrderLst", purchaseOrderLst);
		}
		else
		{
			SearchPoDTO searchPoDTO = new SearchPoDTO();
			model.put("searchPoDTO", searchPoDTO);
		}

		logger.info("Method SearchPOController.getPOSearchPage ends.");
		
		return "poSearchLatest";
	}
	
	@RequestMapping(value="/searchPo", method = RequestMethod.POST)
	public String searchPOs(@ModelAttribute("searchPoDTO") SearchPoDTO searchPoDto, ModelMap model, HttpSession httpSession)
	{

		logger.info("Inside SearchPOController.searchPOs method.");

		//List<HeaderPOData> purchaseOrderLst = iSearchPOSerivce.getPosBasedOnParameter(searchPoDto);
		List<SegPurchaseOrder> purchaseOrderLst = iSearchPOSerivce.getLineItemPosBasedOnParameter(searchPoDto);

		model.put("purchaseOrderLst", purchaseOrderLst);
	
		logger.info("Method SearchPOController.searchPOs ends.");
		
		return "poSearchLatest";
	}
	
	public String getPoLstFromLineItemTable()
	{
		String poNo = "";
		iSearchPOSerivce.getAllLineItemPo(poNo);
		
		return null;
	}
	
	@SuppressWarnings("unused")
	private String changeDateData(String dateFormat)
	{
		String[] data = dateFormat.split("/");
		String changedDate = data[1]+ "/"+ data[0]+"/" +data[2];
		
		return changedDate;
	}

}
