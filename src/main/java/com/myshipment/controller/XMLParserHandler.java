package com.myshipment.controller;

import java.util.ArrayList;
import java.util.List;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.myshipment.xmlpobean.CarfourPOXMLBean;
import com.myshipment.xmlpobean.DateLivraiso;
import com.myshipment.xmlpobean.Documents;
import com.myshipment.xmlpobean.Ean;
import com.myshipment.xmlpobean.Fournisseur;
import com.myshipment.xmlpobean.LibLangue;
import com.myshipment.xmlpobean.Produit;
import com.myshipment.xmlpobean.QteProduit;
import com.myshipment.xmlpobean.RefCli2;
import com.myshipment.xmlpobean.Tranche;

/*
 * @Ranjeet Kumar
 */
public class XMLParserHandler extends DefaultHandler{

	//Main object
	private CarfourPOXMLBean carfourPOXMLBean;

	//Inner objects starts	
	private Fournisseur fournisseur;
	private List<Produit> produitLst = new ArrayList<Produit>();
	private Produit produit;
	private List<LibLangue> libLangueLst = new ArrayList<LibLangue>();
	private LibLangue libLangue;
	private List<Ean> eanLst = new ArrayList<Ean>();
	private Ean ean;
	private List<RefCli2> refCli2Lst = new ArrayList<RefCli2>();
	private RefCli2 refCli2;
	private List<Tranche> trancheLst = new ArrayList<Tranche>();
	private Tranche tranche;
	//private List<QteProduit> qteProduitLst = new ArrayList<QteProduit>();
	private List<QteProduit> qteProduitLst = null;
	private QteProduit qteProduit;
	private List<DateLivraiso> dateLivraisoLst = new ArrayList<DateLivraiso>();
	private DateLivraiso dateLivraiso;
	private List<Documents> documentsLst = new ArrayList<Documents>();
	private Documents documents;
	//Inner objects ends

	private boolean bCod_soc = false;
	private boolean bCod_eta = false;
	private boolean bNum_uat = false;
	private boolean bCod_sta = false;
	private boolean bCod_mod_tra = false;
	private boolean bCod_typ_tra = false;
	//fields for fournisseur tag starts
	private boolean bCod_trs_exp = false;
	private boolean bLib_trs1 = false;
	private boolean bLib_trs2 = false;
	private boolean bAdr1 = false;
	private boolean bAdr2 = false;
	private boolean bAdr3 = false;
	private boolean bCod_pay = false;
	private boolean bCod_ean_fou = false;
	//fields for fournisseur tag ends
	private boolean bCod_mod_reg = false;
	private boolean bCod_dev = false;
	private boolean bCod_trs_dst = false;
	private boolean bCod_trs_tra = false;
	private boolean bCod_trs_qua = false;
	private boolean bFlg_sta_cqu = false;
	private boolean bNum_int_tra = false;
	private boolean bCod_int = false;
	private boolean bCod_trs_agt = false;
	private boolean bFlg_ctr_mat = false;
	private boolean bCod_sai = false;
	private boolean bLib_sai = false;
	private boolean bCod_tys = false;
	private boolean bLib_tys = false;
	private boolean bNum_int_typ = false;
	private boolean bRef_cli_1 = false;
	private boolean bNum_cmd_cli = false;
	private boolean bCod_inc = false;
	private boolean bCod_inc_vte = false;
	private boolean bCod_vil_inc = false;
	private boolean bCod_vil_prc1 = false;
	private boolean bDat_con = false;
	//Start of lProducts_list tag
	//Start of produit tag
	private boolean bCod_pro = false;
	private boolean bCod_pro_pro = false;
	private boolean bLib_pro = false;
	private boolean bCod_emb = false;
	private boolean bPcb = false;
	private boolean bSou_pcb = false;
	private boolean bCod_pay_produit = false;
	private boolean bCod_ray = false;
	private boolean bMnt_ach = false;
	private boolean bTau_ach = false;
	private boolean bMnt_ces = false;
	private boolean bMnt_vte = false;
	private boolean bCod_dev_vte = false;
	private boolean bTau_vte = false;
	private boolean bMnt_vte_soc = false;
	private boolean bCol_lon = false;
	private boolean bCol_lar = false;
	private boolean bCol_hau = false;
	private boolean bCol_vol = false;
	private boolean bCol_pds = false;
	private boolean bRef_fou = false;
	private boolean bPct_dou = false;
	private boolean bTyp_pro = false;
	private boolean bLib_pro_lng = false;
	private boolean bDan_cla = false;
	private boolean bDan_onu = false;
	private boolean bCod_pal = false;
	private boolean bPal_lon = false;
	private boolean bPal_lar = false;
	private boolean bPal_hau = false;
	private boolean bPal_vol = false;
	private boolean bPal_pds = false;
	private boolean bCol_cou = false;
	private boolean bCou_pal = false;
	private boolean bCol_pal = false;
	private boolean bCod_unl_ach = false;
	//Start of tag Liste_lib_langue(lProducts_list->produit->Liste_lib_langue)
	//Start of tag lib_langue(lProducts_list->produit->Liste_lib_langue->lib_langue)
	private boolean bCod_lan = false;
	private boolean bLib_sds = false;
	private boolean bLib_dsc = false;
	private boolean bLib_dsc2 = false;
	//End of tag lib_langue
	private boolean bListe_desc_colis = false;
	//Start of tag Liste_EAN(lProducts_list->produit->Liste_EAN)
	//Start of tag EAN(lProducts_list->produit->Liste_EAN->EAN)
	private boolean bCod_cou = false;
	private boolean bCod_tai = false;
	private boolean bCod_ean = false;
	private boolean bCod_ean_mas = false;
	private boolean bCod_ean_inn = false;
	//End of EAN(lProducts_list->produit->Liste_EAN->EAN)
	//Start of tag Liste_NDP(lProducts_list->produit->Liste_NDP)
	//No values inside. just tag starts and ends
	//Start of tag Liste_ref_cli(lProducts_list->produit->Liste_ref_cli)
	//Start of tag ref_cli_2(lProducts_list->produit->Liste_ref_cli->ref_cli_2)
	//private String cod_cou_ref_cli_2;
	//private String cod_tai_ref_cli_2;
	private boolean bRef_cli_3 = false;
	//Start of tag Liste_box(lProducts_list->produit->Liste_box)
	//No values inside. just tag starts and ends
	//Start of tag liste_tranches
	//Start of tag tranche
	private boolean bNum_tra = false;
	private boolean bDat_etd = false;
	private boolean bNbr_jou_tra = false;
	private boolean bDat_eta = false;
	private boolean bCod_trs_cli = false;
	private boolean bCod_ean_cli = false;
	private boolean bCod_vil_prc2 = false;
	private boolean bCod_trs_dep = false;
	//private boolean cod_mod_tra_tranche;
	//private boolean cod_inc_tranche;
	//private boolean cod_vil_inc_tranche;
	//Start of the tag Liste_qte_produit(liste_tranches->tranche->Liste_qte_produit)
	//Start of the tag qte_produit(liste_tranches->tranche->Liste_qte_produit->qte_produit)
	//private String cod_pro_qte_produit;
	//private String cod_cou_qte_produit;
	//private String cod_tai_qte_produit;
	private boolean bNbr_col = false;
	private boolean bNbr_uvc = false;
	private boolean bVol = false;
	private boolean bPds_brt = false;
	private boolean bCod_prt = false;
	//End of the tag qte_produit(liste_tranches->tranche->Liste_qte_produit->qte_produit)
	//Start of the tag liste_liv_magasin
	//Start of the tag date_livraiso(liste_liv_magasin->date_livraiso)
	//private String cod_pro_date_livraiso;
	//private String num_tra_date_livraiso;
	//private String nbr_col_date_livraiso;
	private boolean bQte = false;
	private boolean bDat_etd_lim = false;
	private boolean bDat_ent_cal = false;
	private boolean bDat_ent_sou = false;
	private boolean bDat_mag_cal = false;
	private boolean bDat_mag_sou = false;
	private boolean bListe_catalogue = false;
	//End of the tag date_livraiso(liste_liv_magasin->date_livraiso)
	//Start of the tag liste_docu
	//Start of the tag documents(liste_docu->documents)
	//private String cod_pro_documents;
	private boolean bCod_ndp = false;
	private boolean bCod_amf = false;
	private boolean bCod_doc = false;
	private boolean bNbr_ori = false;
	private boolean bNbr_cop = false;
	//End of the tag documents(liste_docu->documents)

	private boolean bProduit = false;
	private boolean bFournisseur = false;
	private boolean bQte_produit = false;
	private boolean bEan = false;
	private boolean bCom = false;
	//Some temp value for duplicate variables
	private String tempCod_pay = null;
	private String tempCod_pro = null;
	private String tempCod_cou = null;
	private String tempCod_tai = null;
	private String tempNbr_uvc = null;
	private String tempCod_vil_inc = null;

	public CarfourPOXMLBean getParsedXMLObject(){

		//carfourPOXMLBean.setFournisseurList(fournisseurLst);

		carfourPOXMLBean.setFournisseur(fournisseur);
		carfourPOXMLBean.setProduitLst(produitLst);
		carfourPOXMLBean.setTrancheLst(trancheLst);
		carfourPOXMLBean.setDateLivraisoLst(dateLivraisoLst);
		carfourPOXMLBean.setDocumentsLst(documentsLst);
		return carfourPOXMLBean;
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {

		System.out.println("Start Element : "+qName);

		//Inner objects creation starts
		if(qName.equalsIgnoreCase("com")){
			bCom = true;
			carfourPOXMLBean = new CarfourPOXMLBean();
		}
		if(qName.equalsIgnoreCase("fournisseur")){
			//bFournisseur = true;
			fournisseur = new Fournisseur();
		}
		if(qName.equalsIgnoreCase("produit")){
			bProduit = true;
			produit = new Produit();
		}
		if(qName.equalsIgnoreCase("lib_langue")){
			libLangue = new LibLangue();
		}
		if(qName.equalsIgnoreCase("EAN")){
			bEan = true;
			ean = new Ean();
		}
		if(qName.equalsIgnoreCase("ref_cli_2")){
			refCli2 = new RefCli2();
		}
		if(qName.equalsIgnoreCase("tranche")){
			tranche = new Tranche();
			qteProduitLst = new ArrayList<QteProduit>();
		}
		if(qName.equalsIgnoreCase("qte_produit")){
			bQte_produit = true;
			qteProduit = new QteProduit();
		}
		if(qName.equalsIgnoreCase("date_livraiso")){
			dateLivraiso = new DateLivraiso();
		}
		if(qName.equalsIgnoreCase("documents")){
			documents = new Documents();
		}
		//Inner objects creation ends

		if(qName.equalsIgnoreCase("cod_soc")){
			bCod_soc = true;
		}
		if(qName.equalsIgnoreCase("cod_eta")){
			bCod_eta = true;
		}
		if(qName.equalsIgnoreCase("num_uat")){
			bNum_uat = true;
		}
		if(qName.equalsIgnoreCase("cod_sta")){
			bCod_sta = true;
		}
		if(qName.equalsIgnoreCase("cod_mod_tra")){
			bCod_mod_tra = true;
		}
		if(qName.equalsIgnoreCase("cod_typ_tra")){
			bCod_typ_tra = true;
		}
		if(qName.equalsIgnoreCase("cod_trs_exp")){
			bCod_trs_exp = true;
		}
		if(qName.equalsIgnoreCase("lib_trs1")){
			bLib_trs1 = true;
		}
		if(qName.equalsIgnoreCase("lib_trs2")){
			bLib_trs2 = true;
		}
		if(qName.equalsIgnoreCase("adr1")){
			bAdr1 = true;
		}
		if(qName.equalsIgnoreCase("adr2")){
			bAdr2 = true;
		}
		if(qName.equalsIgnoreCase("adr3")){
			bAdr3 = true;
		}
		if(qName.equalsIgnoreCase("cod_pay")){
			bCod_pay = true;
		}
		if(qName.equalsIgnoreCase("cod_ean_fou")){
			bCod_ean_fou = true;
		}
		if(qName.equalsIgnoreCase("cod_mod_reg")){
			bCod_mod_reg = true;
		}
		if(qName.equalsIgnoreCase("cod_dev")){
			bCod_dev = true;
		}
		if(qName.equalsIgnoreCase("cod_trs_dst")){
			bCod_trs_dst = true;
		}
		if(qName.equalsIgnoreCase("cod_trs_tra")){
			bCod_trs_tra = true;
		}
		if(qName.equalsIgnoreCase("cod_trs_qua")){
			bCod_trs_qua = true;
		}
		if(qName.equalsIgnoreCase("flg_sta_cqu")){
			bFlg_sta_cqu = true;
		}
		if(qName.equalsIgnoreCase("num_int_tra")){
			bNum_int_tra = true;
		}
		if(qName.equalsIgnoreCase("cod_int")){
			bCod_int = true;
		}
		if(qName.equalsIgnoreCase("cod_trs_agt")){
			bCod_trs_agt = true;
		}
		if(qName.equalsIgnoreCase("flg_ctr_mat")){
			bFlg_ctr_mat = true;
		}
		if(qName.equalsIgnoreCase("cod_sai")){
			bCod_sai = true;
		}
		if(qName.equalsIgnoreCase("lib_sai")){
			bLib_sai = true;
		}
		if(qName.equalsIgnoreCase("cod_tys")){
			bCod_tys = true;
		}
		if(qName.equalsIgnoreCase("lib_tys")){
			bLib_tys = true;
		}
		if(qName.equalsIgnoreCase("num_int_typ")){
			bNum_int_typ = true;
		}
		if(qName.equalsIgnoreCase("ref_cli_1")){
			bRef_cli_1 = true;
		}
		if(qName.equalsIgnoreCase("num_cmd_cli")){
			bNum_cmd_cli = true;
		}
		if(qName.equalsIgnoreCase("cod_inc")){
			bCod_inc = true;
		}
		if(qName.equalsIgnoreCase("cod_inc_vte")){
			bCod_inc_vte = true;
		}
		if(qName.equalsIgnoreCase("cod_vil_inc")){
			bCod_vil_inc = true;
		}
		if(qName.equalsIgnoreCase("cod_vil_prc1")){
			bCod_vil_prc1 = true;
		}
		if(qName.equalsIgnoreCase("dat_con")){
			bDat_con = true;
		}
		if(qName.equalsIgnoreCase("cod_pro")){
			bCod_pro = true;
		}
		if(qName.equalsIgnoreCase("cod_pro_pro")){
			bCod_pro_pro = true;
		}
		if(qName.equalsIgnoreCase("lib_pro")){
			bLib_pro = true;
		}
		if(qName.equalsIgnoreCase("cod_emb")){
			bCod_emb = true;
		}
		if(qName.equalsIgnoreCase("pcb")){
			bPcb = true;
		}
		if(qName.equalsIgnoreCase("sou_pcb")){
			bSou_pcb = true;
		}
		/*		if(qName.equalsIgnoreCase("cod_pay_produit")){
			bCod_pay_produit = true;
		}*/
		if(qName.equalsIgnoreCase("cod_ray")){
			bCod_ray = true;
		}
		if(qName.equalsIgnoreCase("mnt_ach")){
			bMnt_ach = true;
		}
		if(qName.equalsIgnoreCase("tau_ach")){
			bTau_ach = true;
		}
		if(qName.equalsIgnoreCase("mnt_ces")){
			bMnt_ces = true;
		}
		if(qName.equalsIgnoreCase("mnt_vte")){
			bMnt_vte = true;
		}
		if(qName.equalsIgnoreCase("cod_dev_vte")){
			bCod_dev_vte = true;
		}
		if(qName.equalsIgnoreCase("tau_vte")){
			bTau_vte = true;
		}
		if(qName.equalsIgnoreCase("mnt_vte_soc")){
			bMnt_vte_soc = true;
		}
		if(qName.equalsIgnoreCase("col_lon")){
			bCol_lon = true;
		}
		if(qName.equalsIgnoreCase("col_lar")){
			bCol_lar = true;
		}
		if(qName.equalsIgnoreCase("col_hau")){
			bCol_hau = true;
		}
		if(qName.equalsIgnoreCase("col_vol")){
			bCol_vol = true;
		}
		if(qName.equalsIgnoreCase("col_pds")){
			bCol_pds = true;
		}
		if(qName.equalsIgnoreCase("ref_fou")){
			bRef_fou = true;
		}
		if(qName.equalsIgnoreCase("pct_dou")){
			bPct_dou = true;
		}
		if(qName.equalsIgnoreCase("typ_pro")){
			bTyp_pro = true;
		}
		if(qName.equalsIgnoreCase("lib_pro_lng")){
			bLib_pro_lng = true;
		}
		if(qName.equalsIgnoreCase("dan_cla")){
			bDan_cla = true;
		}
		if(qName.equalsIgnoreCase("dan_onu")){
			bDan_onu = true;
		}
		if(qName.equalsIgnoreCase("cod_pal")){
			bCod_pal = true;
		}
		if(qName.equalsIgnoreCase("pal_lon")){
			bPal_lon = true;
		}
		if(qName.equalsIgnoreCase("pal_lar")){
			bPal_lar = true;
		}
		if(qName.equalsIgnoreCase("pal_hau")){
			bPal_hau = true;
		}
		if(qName.equalsIgnoreCase("pal_vol")){
			bPal_vol = true;
		}
		if(qName.equalsIgnoreCase("pal_pds")){
			bPal_pds = true;
		}
		if(qName.equalsIgnoreCase("col_cou")){
			bCol_cou = true;
		}
		if(qName.equalsIgnoreCase("cou_pal")){
			bCou_pal = true;
		}
		if(qName.equalsIgnoreCase("col_pal")){
			bCol_pal = true;
		}
		if(qName.equalsIgnoreCase("cod_unl_ach")){
			bCod_unl_ach = true;
		}
		if(qName.equalsIgnoreCase("cod_lan")){
			bCod_lan = true;
		}
		if(qName.equalsIgnoreCase("lib_sds")){
			bLib_sds = true;
		}
		if(qName.equalsIgnoreCase("lib_dsc")){
			bLib_dsc = true;
		}
		if(qName.equalsIgnoreCase("lib_dsc2")){
			bLib_dsc2 = true;
		}
		if(qName.equalsIgnoreCase("Liste_desc_colis")){
			bListe_desc_colis = true;
		}
		if(qName.equalsIgnoreCase("cod_cou")){
			bCod_cou = true;
		}
		if(qName.equalsIgnoreCase("cod_tai")){
			bCod_tai = true;
		}
		if(qName.equalsIgnoreCase("cod_ean")){
			bCod_ean = true;
		}
		if(qName.equalsIgnoreCase("cod_ean_mas")){
			bCod_ean_mas = true;
		}
		if(qName.equalsIgnoreCase("cod_ean_inn")){
			bCod_ean_inn = true;
		}
		if(qName.equalsIgnoreCase("ref_cli_3")){
			bRef_cli_3 = true;
		}
		if(qName.equalsIgnoreCase("num_tra")){
			bNum_tra = true;
		}
		if(qName.equalsIgnoreCase("dat_etd")){
			bDat_etd = true;
		}
		if(qName.equalsIgnoreCase("nbr_jou_tra")){
			bNbr_jou_tra = true;
		}
		if(qName.equalsIgnoreCase("dat_eta")){
			bDat_eta = true;
		}
		if(qName.equalsIgnoreCase("cod_trs_cli")){
			bCod_trs_cli = true;
		}
		if(qName.equalsIgnoreCase("cod_ean_cli")){
			bCod_ean_cli = true;
		}
		if(qName.equalsIgnoreCase("cod_vil_prc2")){
			bCod_vil_prc2 = true;
		}
		if(qName.equalsIgnoreCase("cod_trs_dep")){
			bCod_trs_dep = true;
		}
		if(qName.equalsIgnoreCase("nbr_col")){
			bNbr_col = true;	
		}
		if(qName.equalsIgnoreCase("nbr_uvc")){
			bNbr_uvc = true;
		}
		if(qName.equalsIgnoreCase("vol")){
			bVol = true;
		}
		if(qName.equalsIgnoreCase("pds_brt")){
			bPds_brt = true;
		}
		if(qName.equalsIgnoreCase("cod_prt")){
			bCod_prt = true;
		}
		if(qName.equalsIgnoreCase("qte")){
			bQte = true;
		}
		if(qName.equalsIgnoreCase("dat_etd_lim")){
			bDat_etd_lim = true;
		}
		if(qName.equalsIgnoreCase("dat_ent_cal")){
			bDat_ent_cal = true;
		}
		if(qName.equalsIgnoreCase("dat_ent_sou")){
			bDat_ent_sou = true;
		}
		if(qName.equalsIgnoreCase("dat_mag_cal")){
			bDat_mag_cal = true;
		}
		if(qName.equalsIgnoreCase("dat_mag_sou")){
			bDat_mag_sou = true;
		}
		if(qName.equalsIgnoreCase("Liste_catalogue")){
			bListe_catalogue = true;
		}
		if(qName.equalsIgnoreCase("cod_ndp")){
			bCod_ndp = true;
		}
		if(qName.equalsIgnoreCase("cod_amf")){
			bCod_amf = true;
		}
		if(qName.equalsIgnoreCase("cod_doc")){
			bCod_doc = true;
		}
		if(qName.equalsIgnoreCase("nbr_ori")){
			bNbr_ori = true;
		}
		if(qName.equalsIgnoreCase("nbr_cop")){
			bNbr_cop = true;
		}


	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {

		if(bCod_soc){
			carfourPOXMLBean.setCod_soc(getStringValue(ch, start, length));
			bCod_soc = false;
		}
		if(bCod_eta){
			carfourPOXMLBean.setCod_eta(getStringValue(ch, start, length));
			bCod_eta = false;
		}
		if(bNum_uat){
			carfourPOXMLBean.setNum_uat(getStringValue(ch, start, length));
			bNum_uat = false;
		}
		if(bCod_sta){
			carfourPOXMLBean.setCod_sta(getStringValue(ch, start, length));
			bCod_sta = false;
		}
		if(bCod_mod_tra){
			carfourPOXMLBean.setCod_mod_tra(getStringValue(ch, start, length));
			bCod_mod_tra = false;
		}
		if(bCod_typ_tra){
			carfourPOXMLBean.setCod_typ_tra(getStringValue(ch, start, length));
			bCod_typ_tra = false;
		}
		if(bCod_trs_exp){
			//carfourPOXMLBean.setCod_trs_exp(getStringValue(ch, start, length));
			fournisseur.setCod_trs_exp(getStringValue(ch, start, length));
			bCod_trs_exp = false;
		}
		if(bLib_trs1){
			//carfourPOXMLBean.setLib_trs1(getStringValue(ch, start, length));
			fournisseur.setLib_trs1(getStringValue(ch, start, length));
			bLib_trs1 = false;
		}
		if(bLib_trs2){
			//carfourPOXMLBean.setLib_trs2(getStringValue(ch, start, length));
			fournisseur.setLib_trs2(getStringValue(ch, start, length));
			bLib_trs2 = false;
		}
		if(bAdr1){
			//carfourPOXMLBean.setAdr1(getStringValue(ch, start, length));
			fournisseur.setAdr1(getStringValue(ch, start, length));
			bAdr1 = false;
		}
		if(bAdr2){
			//carfourPOXMLBean.setAdr2(getStringValue(ch, start, length));
			fournisseur.setAdr2(getStringValue(ch, start, length));
			bAdr2 = false;
		}
		if(bAdr3){
			//carfourPOXMLBean.setAdr3(getStringValue(ch, start, length));
			fournisseur.setAdr3(getStringValue(ch, start, length));
			bAdr3 = false;
		}
		if(bCod_pay){
			//carfourPOXMLBean.setCod_pay(getStringValue(ch, start, length));
			//fournisseur.setCod_pay(getStringValue(ch, start, length));
			tempCod_pay = getStringValue(ch, start, length);
			bCod_pay = false;
		}
		if(bCod_ean_fou){
			//carfourPOXMLBean.setCod_ean_fou(getStringValue(ch, start, length));
			fournisseur.setCod_ean_fou(getStringValue(ch, start, length));
			bCod_ean_fou = false;
		}
		if(bCod_mod_reg){
			carfourPOXMLBean.setCod_mod_reg(getStringValue(ch, start, length));
			bCod_mod_reg = false;
		}
		if(bCod_dev){
			carfourPOXMLBean.setCod_dev(getStringValue(ch, start, length));
			bCod_dev = false;
		}
		if(bCod_trs_dst){
			carfourPOXMLBean.setCod_trs_dst(getStringValue(ch, start, length));
		}
		if(bCod_trs_tra){
			carfourPOXMLBean.setCod_trs_tra(getStringValue(ch, start, length));
			bCod_trs_dst = false;
		}
		if(bCod_trs_qua){
			carfourPOXMLBean.setCod_trs_qua(getStringValue(ch, start, length));
			bCod_trs_qua = false;
		}
		if(bFlg_sta_cqu){
			carfourPOXMLBean.setFlg_sta_cqu(getStringValue(ch, start, length));
			bFlg_sta_cqu = false;
		}
		if(bNum_int_tra){
			carfourPOXMLBean.setNum_int_tra(getStringValue(ch, start, length));
		}
		if(bCod_int){
			carfourPOXMLBean.setCod_int(getStringValue(ch, start, length));
			bNum_int_tra = false;
		}
		if(bCod_trs_agt){
			carfourPOXMLBean.setCod_trs_agt(getStringValue(ch, start, length));
			bCod_trs_agt = false;
		}
		if(bFlg_ctr_mat){
			carfourPOXMLBean.setFlg_ctr_mat(getStringValue(ch, start, length));
		}
		if(bCod_sai){
			carfourPOXMLBean.setCod_sai(getStringValue(ch, start, length));
			bFlg_ctr_mat = false;
		}
		if(bLib_sai){
			carfourPOXMLBean.setLib_sai(getStringValue(ch, start, length));
		}
		if(bCod_tys){
			carfourPOXMLBean.setCod_tys(getStringValue(ch, start, length));
			bLib_sai = false;
		}
		if(bLib_tys){
			carfourPOXMLBean.setLib_tys(getStringValue(ch, start, length));
			bLib_tys = false;
		}
		if(bNum_int_typ){
			carfourPOXMLBean.setNum_int_typ(getStringValue(ch, start, length));
			bNum_int_typ = false;
		}
		if(bRef_cli_1){
			carfourPOXMLBean.setRef_cli_1(getStringValue(ch, start, length));
			bRef_cli_1 = false;
		}
		if(bNum_cmd_cli){
			carfourPOXMLBean.setNum_cmd_cli(getStringValue(ch, start, length));
			bNum_cmd_cli = false;
		}
		if(bCod_inc){
			carfourPOXMLBean.setCod_inc(getStringValue(ch, start, length));
			bCod_inc = false;
		}
		if(bCod_inc_vte){
			carfourPOXMLBean.setCod_inc_vte(getStringValue(ch, start, length));
			bCod_inc_vte = false;
		}
		if(bCod_vil_inc){
			//carfourPOXMLBean.setCod_vil_inc(getStringValue(ch, start, length));
			tempCod_vil_inc = getStringValue(ch, start, length);
			if(bCom){
				carfourPOXMLBean.setCod_vil_inc(tempCod_vil_inc);
				bCom = false;
			}
			bCod_vil_inc = false;
		}
		if(bCod_vil_prc1){
			carfourPOXMLBean.setCod_vil_prc1(getStringValue(ch, start, length));
			bCod_vil_prc1 = false;
		}
		if(bDat_con){
			carfourPOXMLBean.setDat_con(getStringValue(ch, start, length));
			bDat_con = false;
		}
		if(bCod_pro){
			//produit.setCod_pro(getStringValue(ch, start, length));
			tempCod_pro = getStringValue(ch, start, length);
			bCod_pro = false;
		}
		if(bCod_pro_pro){
			produit.setCod_pro_pro(getStringValue(ch, start, length));
			bCod_pro_pro = false;
		}
		if(bLib_pro){
			produit.setLib_pro(getStringValue(ch, start, length));
			bLib_pro = false;
		}
		if(bCod_emb){
			produit.setCod_emb(getStringValue(ch, start, length));
			bCod_emb = false;
		}
		if(bPcb){
			produit.setPcb(getStringValue(ch, start, length));
			bPcb = false;
		}
		if(bSou_pcb){
			produit.setSou_pcb(getStringValue(ch, start, length));
			bSou_pcb = false;
		}
		/*		if(bCod_pay){
			produit.setCod_pay(getStringValue(ch, start, length));
			bCod_pay = false;
		}
		 */		
		if(bCod_ray){
			produit.setCod_ray(getStringValue(ch, start, length));
			bCod_ray = false;
		}
		if(bMnt_ach){
			produit.setMnt_ach(getStringValue(ch, start, length));
			bMnt_ach = false;
		}
		if(bTau_ach){
			produit.setTau_ach(getStringValue(ch, start, length));
			bTau_ach = false;
		}
		if(bMnt_ces){
			produit.setMnt_ces(getStringValue(ch, start, length));
			bMnt_ces = false;
		}
		if(bMnt_vte){
			produit.setMnt_vte(getStringValue(ch, start, length));
			bMnt_vte = false;
		}
		if(bCod_dev_vte){
			produit.setCod_dev_vte(getStringValue(ch, start, length));
			bCod_dev_vte = false;
		}
		if(bTau_vte){
			produit.setTau_vte(getStringValue(ch, start, length));
			bTau_vte = false;
		}
		if(bMnt_vte_soc){
			produit.setMnt_vte_soc(getStringValue(ch, start, length));
			bMnt_vte_soc = false;
		}
		if(bCol_lon){
			produit.setCol_lon(getStringValue(ch, start, length));
			bCol_lon = false;
		}
		if(bCol_lar){
			produit.setCol_lar(getStringValue(ch, start, length));
			bCol_lar = false;
		}
		if(bCol_hau){
			produit.setCol_hau(getStringValue(ch, start, length));
			bCol_hau = false;
		}
		if(bCol_vol){
			produit.setCol_vol(getStringValue(ch, start, length));
			bCol_vol = false;
		}
		if(bCol_pds){
			produit.setCol_pds(getStringValue(ch, start, length));
			bCol_pds = false;
		}
		if(bRef_fou){
			produit.setRef_fou(getStringValue(ch, start, length));
			bRef_fou = false;
		}
		if(bPct_dou){
			produit.setPct_dou(getStringValue(ch, start, length));
			bPct_dou = false;
		}
		if(bTyp_pro){
			produit.setTyp_pro(getStringValue(ch, start, length));
			bTyp_pro = false;
		}
		if(bLib_pro_lng){
			produit.setLib_pro_lng(getStringValue(ch, start, length));
			bLib_pro_lng = false;
		}
		if(bDan_cla){
			produit.setDan_cla(getStringValue(ch, start, length));
			bDan_cla = false;
		}
		if(bDan_onu){
			produit.setDan_onu(getStringValue(ch, start, length));
			bDan_onu = false;
		}
		if(bCod_pal){
			produit.setCod_pal(getStringValue(ch, start, length));
			bCod_pal = false;
		}
		if(bPal_lon){
			produit.setPal_lon(getStringValue(ch, start, length));
		}
		if(bPal_lar){
			produit.setPal_lar(getStringValue(ch, start, length));
			bPal_lon = false;
		}
		if(bPal_hau){
			produit.setPal_hau(getStringValue(ch, start, length));
			bPal_hau = false;
		}
		if(bPal_vol){
			produit.setPal_vol(getStringValue(ch, start, length));
			bPal_vol = false;
		}
		if(bPal_pds){
			produit.setPal_pds(getStringValue(ch, start, length));
			bPal_pds = false;
		}
		if(bCol_cou){
			produit.setCol_cou(getStringValue(ch, start, length));
			bCol_cou = false;
		}
		if(bCou_pal){
			produit.setCou_pal(getStringValue(ch, start, length));
			bCou_pal = false;
		}
		if(bCol_pal){
			produit.setCol_pal(getStringValue(ch, start, length));
			bCol_pal = false;
		}
		if(bCod_unl_ach){
			produit.setCod_unl_ach(getStringValue(ch, start, length));
			bCod_unl_ach = false;
		}
		if(bCod_lan){
			libLangue.setCod_lan(getStringValue(ch, start, length));
			bCod_lan = false;
		}
		if(bLib_sds){
			libLangue.setLib_sds(getStringValue(ch, start, length));
			bLib_sds = false;
		}
		if(bLib_dsc){
			libLangue.setLib_dsc(getStringValue(ch, start, length));
			bLib_dsc = false;
		}
		if(bLib_dsc2){
			libLangue.setLib_dsc2(getStringValue(ch, start, length));
			bLib_dsc2 = false;
		}
		/*		if(bListe_desc_colis){
			carfourPOXMLBean.setListe_desc_colis(getStringValue(ch, start, length));
			bListe_desc_colis = false;
		}*/
		if(bCod_cou){
			//ean.setCod_cou(getStringValue(ch, start, length));
			tempCod_cou = getStringValue(ch, start, length);
			bCod_cou = false;
		}
		if(bCod_tai){
			//ean.setCod_tai(getStringValue(ch, start, length));
			tempCod_tai = getStringValue(ch, start, length);
			bCod_tai = false;
		}
		if(bCod_ean){
			ean.setCod_ean(getStringValue(ch, start, length));
			bCod_ean = false;
		}
		if(bCod_ean_mas){
			ean.setCod_ean_mas(getStringValue(ch, start, length));
			bCod_ean_mas = false;
		}
		if(bCod_ean_inn){
			ean.setCod_ean_inn(getStringValue(ch, start, length));
			bCod_ean_inn = false;
		}
		if(bRef_cli_3){
			refCli2.setRef_cli_3(getStringValue(ch, start, length));
			bRef_cli_3 = false;
		}
		if(bNum_tra){
			tranche.setNum_tra(getStringValue(ch, start, length));
			bNum_tra = false;
		}
		if(bDat_etd){
			tranche.setDat_etd(getStringValue(ch, start, length));
			bDat_etd = false;
		}
		if(bNbr_jou_tra){
			tranche.setNbr_jou_tra(getStringValue(ch, start, length));
			bNbr_jou_tra = false;
		}
		if(bDat_eta){
			tranche.setDat_eta(getStringValue(ch, start, length));
			bDat_eta = false;
		}
		if(bCod_trs_cli){
			tranche.setCod_trs_cli(getStringValue(ch, start, length));
			bCod_trs_cli = false;
		}
		if(bCod_ean_cli){
			tranche.setCod_ean_cli(getStringValue(ch, start, length));
			bCod_ean_cli = false;
		}
		if(bCod_vil_prc2){
			tranche.setCod_vil_prc2(getStringValue(ch, start, length));
			bCod_vil_prc2 = false;
		}
		if(bCod_trs_dep){
			tranche.setCod_trs_dep(getStringValue(ch, start, length));
			bCod_trs_dep = false;
		}
		if(bNbr_col){
			qteProduit.setNbr_col(getStringValue(ch, start, length));	
			bNbr_col = false;
		}
		if(bNbr_uvc){
			//qteProduit.setNbr_uvc(getStringValue(ch, start, length));
			tempNbr_uvc = getStringValue(ch, start, length);
			bNbr_uvc = false;
		}
		if(bVol){
			qteProduit.setVol(getStringValue(ch, start, length));
			bVol = false;
		}
		if(bPds_brt){
			qteProduit.setPds_brt(getStringValue(ch, start, length));
			bPds_brt = false;
		}
		if(bCod_prt){
			qteProduit.setCod_prt(getStringValue(ch, start, length));
			bCod_prt = false;
		}
		if(bQte){
			dateLivraiso.setQte(getStringValue(ch, start, length));
			bQte = false;
		}
		if(bDat_etd_lim){
			dateLivraiso.setDat_etd_lim(getStringValue(ch, start, length));
			bDat_etd_lim = false;
		}
		if(bDat_ent_cal){
			dateLivraiso.setDat_ent_cal(getStringValue(ch, start, length));
			bDat_ent_cal = false;
		}
		if(bDat_ent_sou){
			dateLivraiso.setDat_ent_sou(getStringValue(ch, start, length));
			bDat_ent_sou = false;
		}
		if(bDat_mag_cal){
			dateLivraiso.setDat_mag_cal(getStringValue(ch, start, length));
			bDat_mag_cal = false;
		}
		if(bDat_mag_sou){
			dateLivraiso.setDat_mag_sou(getStringValue(ch, start, length));
			bDat_mag_sou = false;
		}
		/*		if(bListe_catalogue){
			carfourPOXMLBean.setListe_catalogue(getStringValue(ch, start, length));
			bListe_catalogue = false;
		}*/

		//The following are commented as it is not being used as per us.
		/*		if(bCod_ndp){
			documents.setCod_ndp(getStringValue(ch, start, length));
			bCod_ndp = false;
		}

		if(bCod_amf){
			documents.setCod_amf(getStringValue(ch, start, length));
			bCod_amf = false;
		}
		if(bCod_doc){
			documents.setCod_doc(getStringValue(ch, start, length));
			bCod_doc = false;
		}
		if(bNbr_ori){
			documents.setNbr_ori(getStringValue(ch, start, length));
			bNbr_ori = false;
		}
		if(bNbr_cop){
			documents.setNbr_cop(getStringValue(ch, start, length));
			bNbr_cop = false;
		}*/
	}


	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {


		if(qName.equalsIgnoreCase("produit")){
			//Since produit contains others objects so setting those objects to produit
			produit.setLibLangueLst(libLangueLst);
			produit.setEanLst(eanLst);
			produit.setRefCli2Lst(refCli2Lst);
			produitLst.add(produit);
		}
		if(qName.equalsIgnoreCase("lib_langue")){
			libLangueLst.add(libLangue);
		}
		if(qName.equalsIgnoreCase("EAN")){
			eanLst.add(ean);
		}
		if(qName.equalsIgnoreCase("ref_cli_2")){
			refCli2Lst.add(refCli2);
		}
		if(qName.equalsIgnoreCase("tranche")){
			tranche.setQteProduit(qteProduitLst);
			trancheLst.add(tranche);
		}
		if(qName.equalsIgnoreCase("qte_produit")){
			qteProduitLst.add(qteProduit);
		}
		if(qName.equalsIgnoreCase("date_livraiso")){
			dateLivraisoLst.add(dateLivraiso);
		}
		if(qName.equalsIgnoreCase("documents")){
			documentsLst.add(documents);
		}

		if(qName.equalsIgnoreCase("cod_pro") && bProduit ){
			produit.setCod_pro(tempCod_pro);

		}
		if(qName.equalsIgnoreCase("cod_pro") && bQte_produit ){
			qteProduit.setCod_pro_qte_produit(tempCod_pro);

		}
		if(qName.equalsIgnoreCase("cod_cou") && bQte_produit ){
			qteProduit.setCod_cou_qte_produit(tempCod_cou);
		}
		if(qName.equalsIgnoreCase("cod_cou") && bEan ){
			ean.setCod_cou(tempCod_cou);
		}
		if(qName.equalsIgnoreCase("nbr_uvc") && bQte_produit ){
			qteProduit.setNbr_uvc(tempNbr_uvc);
		}
		if(qName.equalsIgnoreCase("cod_tai") && bEan ){
			ean.setCod_tai(tempCod_tai);
		}
		if(qName.equalsIgnoreCase("cod_tai") && bQte_produit ){
			qteProduit.setCod_tai_qte_produit(tempCod_tai);
		}
		/*		if(qName.equalsIgnoreCase("cod_vil_inc") && bCom ){
			carfourPOXMLBean.setCod_vil_inc(tempCod_vil_inc);
		}
		 */		
		if(qName.equalsIgnoreCase("produit")){
			bProduit = false;
		}
		if(qName.equalsIgnoreCase("qte_produit")){
			bQte_produit = false;
		}
		if(qName.equalsIgnoreCase("EAN")){
			bEan = false;
		}

	}

	private String getStringValue(char[] ch, int start, int length){

		String value = new String(ch, start, length);
		return value;
	}
}
