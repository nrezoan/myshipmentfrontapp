package com.myshipment.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.myshipment.util.DatabaseComponent;

public class PopulatePO extends HttpServlet {

	/**
	 * Constructor of the object.
	 */
	public PopulatePO() {
		super();
	}

	/**
	 * Destruction of the servlet. <br>
	 */
	public void destroy() {
		super.destroy(); // Just puts "destroy" string in log
		// Put your code here
	}

	/**
	 * The doGet method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to get.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		out
				.println("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\">");
		out.println("<HTML>");
		out.println("  <HEAD><TITLE>A Servlet</TITLE></HEAD>");
		out.println("  <BODY>");
		out.print("    This is ");
		out.print(this.getClass());
		out.println(", using the GET method");
		out.println("  </BODY>");
		out.println("</HTML>");
		out.flush();
		out.close();
	}

	/**
	 * The doPost method of the servlet. <br>
	 *
	 * This method is called when a form has its tag value method equals to post.
	 * 
	 * @param request the request send by the client to the server
	 * @param response the response send by the server to the client
	 * @throws ServletException if an error occurred
	 * @throws IOException if an error occurred
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		System.out.println("Come into post method");
		PrintWriter out = response.getWriter();
		String poNumber=request.getParameter("poNumber");
		String statusType=request.getParameter("statusUpdate")==null?"":request.getParameter("statusUpdate");
		String query="";
		try{
			DatabaseComponent db=new DatabaseComponent();
			if(statusType.equalsIgnoreCase("yes"))
				query="select ORDER_NO, SAP_NO, INSPECTOR_CODE, SHIPMENT_MODE, QUANTITY, to_char(DELIVERY_DATE,'mm/dd/yyyy') as DELIVERY_DATE, ADV_ORDER, ENTRY_DATE, FLAG,CUSTOMER_NAME from orders where order_no='"+poNumber+"'";
			
			else
				query="select ORDER_NO, SAP_NO, INSPECTOR_CODE, SHIPMENT_MODE, QUANTITY, to_char(DELIVERY_DATE,'mm/dd/yyyy') as DELIVERY_DATE, ADV_ORDER, ENTRY_DATE, FLAG,CUSTOMER_NAME from orders where order_no='"+poNumber+"' and flag='P'";
			
			System.out.println("query: "+query);
			ResultSet rs=db.retrieve(query);
			
			StringBuffer res=new StringBuffer();
			res.append("<?xml version='1.0'?>");
			res.append("<PO>");
			int count=0;
			while(rs.next()){
				if(count>0)
					break;
			String sapNo=rs.getString("SAP_NO");
			String merchantDiser=rs.getString("INSPECTOR_CODE");
			String shipmentMode=rs.getString("SHIPMENT_MODE");
			String quantity=rs.getString("QUANTITY");
			String deliveryDate=rs.getString("DELIVERY_DATE");
			String advOrder=rs.getString("ADV_ORDER");
			String customerName=rs.getString("CUSTOMER_NAME")==null?"null":rs.getString("CUSTOMER_NAME").toString();
			String status=rs.getString("FLAG");
			System.out.println("SAP NO: "+ sapNo);
			
			res.append("<sapNo>");
			res.append(sapNo);
			res.append("</sapNo>");
			res.append("\n");
			
			res.append("<merchantDiser>");
			res.append(merchantDiser);
			res.append("</merchantDiser>");
			res.append("\n");
			
			res.append("<shipmentMode>");
			res.append(shipmentMode);
			res.append("</shipmentMode>");
			res.append("\n");
			
			res.append("<deliveryDate>");
			res.append(deliveryDate);
			res.append("</deliveryDate>");
			res.append("\n");
			
			res.append("<quantity>");
			res.append(quantity);
			res.append("</quantity>");
			res.append("\n");
			
			res.append("<advOrder>");
			res.append(advOrder);
			res.append("</advOrder>");
			res.append("\n");
			
			res.append("<status>");
			res.append(status);
			res.append("</status>");
			res.append("\n");
			
			res.append("<customerName>");
			customerName=normalize(customerName.toString());
			res.append(customerName);
			res.append("</customerName>");
			res.append("\n");
			
			count++;
		

			
			}
			if(count==0){
				res.append("<message>");
				res.append("Invalid PO");
				res.append("</message>");

			}
			else{
				res.append("<message>");
				res.append("Success");
				res.append("</message>");
			}
			res.append("</PO>");
			
			//System.out.println("response: "+ res.toString());
			response.setContentType("text/xml");
			
			response.setContentLength(res.toString().length());
			
			System.out.println(res.toString());
			response.getWriter().write(res.toString());
		

			}
			catch(Exception ex)
			{
				ex.printStackTrace();
			}
			
	}
	
	public static String normalize(String s) {
		if (s == null) return null;
		StringBuffer ret = new StringBuffer();
		int len = s.length();
		for (int i = 0; i < len; i++) {
			char ch = s.charAt(i);
			
			switch (ch) {
				case '<':
					ret.append("&#060;");
					break;
				case '>':
					ret.append("&#062;");
					break;
				case '&':
					ret.append("&#038;");
					break;
				case '"':
					ret.append("&#034;");
					break;
				
				case '\'':
					ret.append("&#039;");
					break;
				
				case '\\':
					ret.append("&#092;");
				case '%':
					ret.append("&#037;");	
					break;
				case '+':
					ret.append("&#043;");	
					break;
					
				     
				       
				      /*
				       else if (character == '(') {
				         result.append("&#040;");
				       }
				       else if (character == ')') {
				         result.append("&#041;");
				       }
				       else if (character == '#') {
				         result.append("&#035;");
				       }
				       else if (character == '%') {
				         result.append("&#037;");
				       }
				       else if (character == ';') {
				         result.append("&#059;");
				       }
				       else if (character == '+') {
				         result.append("&#043;");
				       }
				       else if (character == '-') {
				         result.append("&#045;");
				       }
					*/
				default:
					ret.append(ch);
			}
		}
		return new String(ret);
	}

	/**
	 * Initialization of the servlet. <br>
	 *
	 * @throws ServletException if an error occurs
	 */
	public void init() throws ServletException {
		// Put your code here
	}

}
