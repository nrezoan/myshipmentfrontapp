package com.myshipment.mappers;

import org.apache.ibatis.annotations.*;

import com.myshipment.model.ApprovedPurchaseOrder;
import com.myshipment.model.POFileDTO;

/*
 * @ Ranjeet Kumar
 */
public interface UploadDataMapper {

	public final String insert = "INSERT INTO T_FILE_UPLOAD_DATA (UPLOAD_ID,VC_PO_NO,NU_CLIENT_CODE,VC_PRODUCT_NO,VC_SKU_NO,VC_STYLE_NO,VC_ARTICLE_NO,VC_COLOR,VC_SIZE,VC_POL,VC_POD,NU_NO_PCS_CTNS,VC_COMMODITY,DT_ETD,VC_TOT_PCS,VC_QUAN,VC_QUA_UOM,NU_LENGTH,NU_WIDTH,NU_HIEGHT,VC_IN_HCM,VC_CBM_SEA,VC_VOLUME,VC_GW_CAR,VC_GR_WT,VC_NW_CAR,VC_NT_WT,VC_REF_FIELD1,VC_REF_FIELD2,VC_REF_FIELD3,VC_REF_FIELD4,VC_REF_FIELD5,VC_HS_CODE,VC_QC_DT,VC_REL_DT,SALES_ORG,SAP_QUOTATION,FILE_UPLOAD_DATE,VC_BUYER,VC_BUY_HOUSE,FILE_NAME,VC_DIVISION ) VALUES (#{upload_id},#{vc_po_no},#{nu_client_code},#{vc_product_no},#{vc_sku_no},#{vc_style_no},#{vc_article_no},#{vc_color},#{vc_size},#{vc_pol},#{vc_pod},#{nu_no_pcs_ctns},#{vc_commodity},#{dt_etd},#{vc_tot_pcs},#{vc_quan},#{vc_qua_uom},#{nu_length},#{nu_width},#{nu_height},#{vc_in_hcm},#{vc_cbm_sea},#{vc_volume},#{vc_gw_car},#{vc_gr_wt},#{vc_nw_car},#{vc_nt_wt},#{vc_ref_field1},#{vc_ref_field2},#{vc_ref_field3},#{vc_ref_field4},#{vc_ref_field5},#{vc_hs_code},#{vc_qc_dt},#{vc_rel_dt},#{sales_org},#{sap_quotation},#{file_upload_date},#{vc_buyer},#{vc_buy_house},#{file_name},#{vc_division})";

	@Insert(insert)
	@SelectKey(keyProperty = "upload_id", before = true, resultType = Integer.class, statement = {
			"SELECT T_FILE_UPLOAD_DATA_SEQ.nextval AS upload_id FROM dual" })
	public void insert(POFileDTO poFileDTO);

	
	
	
	/*Sanjana Khan Shammi*/
	
	public final String insertPOGeneratedInfo = "insert into t_approved_purchase_order(APP_ID,VC_PO_NO,VC_SKU_NO,VC_ARTICLE_NO,VC_COLOR,VC_SIZE,VC_STYLE_NO,VC_COMMODITY,VC_TOT_PCS,VC_QUAN,NU_LENGTH,NU_WIDTH,NU_HIEGHT,VC_GR_WT,VC_NT_WT,VC_HS_CODE,VC_BUYER,VC_CBM_SEA,NU_CLIENT_CODE,VC_IN_HCM,SALES_ORG,BOOKING_STATUS,VC_DIVISION) values(#{app_id},#{vc_po_no},#{vc_sku_no},#{vc_article_no},#{vc_color},#{vc_size},#{vc_style_no},#{vc_commodity},#{vc_tot_pcs},#{vc_quan},#{nu_length},#{nu_width},#{nu_hieght},#{vc_gr_wt},#{vc_nt_wt},#{vc_hs_code},#{vc_buyer},#{vc_cbm_sea},#{nu_client_code},#{vc_in_hcm},#{sales_org},#{booking_status},#{vc_division})";

	@Insert(insertPOGeneratedInfo)
	@SelectKey(keyProperty = "app_id", before = true, resultType = Long.class, statement = {
			"SELECT t_approved_purchase_order_seq.nextval AS app_id FROM dual" })
	public Long insertPOGeneratedInfo(ApprovedPurchaseOrder appPO);

	

}
