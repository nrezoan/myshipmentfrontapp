package com.myshipment.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.myshipment.model.ApprovedPurchaseOrder;

public interface POMapperNew {

	@Select("SELECT VC_PO_NO AS vc_po_no,VC_STYLE_NO AS vc_style_no,VC_COLOR AS vc_color,VC_SIZE AS vc_size,VC_HS_CODE AS vc_hs_code,NU_NO_PCS_CTNS AS nu_no_pcs_ctns,VC_TOT_PCS AS vc_tot_pcs,VC_NT_WT AS vc_nt_wt,VC_GR_WT AS vc_gr_wt,VC_CBM_SEA AS vc_cbm_sea,APP_ID as app_id FROM T_APPROVED_PURCHASE_ORDER WHERE VC_BUYER =#{buyer} AND SALES_ORG=#{salesOrg} AND VC_DIVISION=#{division} AND NU_CLIENT_CODE=#{shipper} AND BOOKING_STATUS='P'")
	public List<ApprovedPurchaseOrder> getPOforBooking(@Param("buyer") String buyer,@Param("salesOrg") String salesOrg,@Param("division") String division,@Param("shipper") String shipper);


}
