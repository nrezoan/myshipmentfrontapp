package com.myshipment.mappers;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;

import com.myshipment.model.POFileInfo;
/*
 * @Ranjeet Kumar
 */
public interface POFileReaderMapper {

	public final String insert = "INSERT INTO T_PO_FILE_UPLOAD (FILE_ID,FILE_NAME,FILE_DIRECTORY,FILE_COMPLETE_PATH,FILE_CONTENT,FILE_UPLOAD_DATE ) VALUES (#{file_id},#{fileName},#{fileDirectory},#{completeFilePath},#{fileContent},to_date(#{created_on},'dd-mm-yyyy'))";
	
	
	@Insert(insert)
	@SelectKey(
            keyProperty = "file_id",
            before = true,
            resultType = Long.class,
            statement = { "SELECT T_PO_FILE_UPLOAD_SEQ.nextval AS file_id FROM dual" })
	public void insert(POFileInfo poFileInfo);
	
	@Select("SELECT FILE_ID AS file_id, FILE_NAME AS fileName, FILE_DIRECTORY AS fileDirectory, FILE_COMPLETE_PATH AS completeFilePath, FILE_UPLOAD_DATE AS created_on "
			+ "FROM T_PO_FILE_UPLOAD WHERE FILE_NAME=#{fileName}")
	public POFileInfo getFileBasedOnFileName(@Param("fileName") String fileName);
}
