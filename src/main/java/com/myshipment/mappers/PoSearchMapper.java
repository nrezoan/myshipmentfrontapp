package com.myshipment.mappers;


import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.myshipment.model.HeaderPOData;
import com.myshipment.model.SegPurchaseOrder;
/*
 * @ Ranjeet Kumar
 */
public interface PoSearchMapper {

	@Select("SELECT PO_ID AS po_id, VC_PO_NO AS vc_po_no, NU_CLIENT_CODE AS nu_client_code, SALES_ORG AS sales_org, "
			+ "VC_BUYER AS vc_buyer, VC_BUY_HOUSE AS vc_buy_house, COMPANY_CODE AS company_code, SUPPLIER_CODE AS supplier_code,"
			+ " PO_CREATION_DATE AS currentDate FROM T_PURCHASE_ORDER WHERE VC_PO_NO=#{poNo}")
	public List<HeaderPOData> selectPoBasedOnPoNo(String poNo);
	
	@Select("SELECT PO_ID AS po_id, VC_PO_NO AS vc_po_no, NU_CLIENT_CODE AS nu_client_code, SALES_ORG AS sales_org, "
			+ "VC_BUYER AS vc_buyer, VC_BUY_HOUSE AS vc_buy_house, COMPANY_CODE AS company_code, SUPPLIER_CODE AS supplier_code,"
			+ " PO_CREATION_DATE AS currentDate FROM T_PURCHASE_ORDER WHERE VC_PO_NO=#{poNo} AND PO_CREATION_DATE=TO_DATE(#{todate}, 'DD-MM-YY')")
	public List<HeaderPOData> getPosBasedOnPoNoAndDate(@Param("poNo") String poNo, @Param("todate") String todate);
/*	
	@Select("SELECT PO_ID AS po_id, VC_PO_NO AS vc_po_no, NU_CLIENT_CODE AS nu_client_code, SALES_ORG AS sales_org, "
			+ "VC_BUYER AS vc_buyer, VC_BUY_HOUSE AS vc_buy_house, COMPANY_CODE AS company_code, SUPPLIER_CODE AS supplier_code,"
			+ " PO_CREATION_DATE AS currentDate FROM T_PURCHASE_ORDER WHERE PO_CREATION_DATE BETWEEN TO_DATE(#{todate}, 'dd-mm-yyyy') AND TO_DATE(#{fromdate}, 'dd-mm-yyyy')")
	public List<HeaderPOData> getPosBasedOnTodateAndFromdate(String todate, String fromdate);
*/	
	@Select("SELECT SEG_ID AS seg_id, PO_ID AS po_id, VC_PO_NO AS vc_po_no, VC_PRODUCT_NO AS vc_product_no, VC_SKU_NO AS vc_sku_no, VC_STYLE_NO AS vc_style_no, VC_ARTICLE_NO AS vc_article_no,"
			+ "VC_COLOR AS vc_color, VC_SIZE AS vc_size, VC_POL AS vc_pol, VC_POD AS vc_pod, NU_NO_PCS_CTNS AS nu_no_pcs_ctns, VC_COMMODITY AS vc_commodity, DT_ETD AS dt_etd, VC_TOT_PCS AS vc_tot_pcs,"
			+ "VC_QUAN AS vc_quan, VC_QUA_UOM AS vc_qua_uom, NU_LENGTH AS nu_length, NU_WIDTH AS nu_width, NU_HIEGHT AS nu_height, VC_IN_HCM AS vc_in_hcm, VC_CBM_SEA AS vc_cbm_sea, VC_VOLUME AS vc_volume,"
			+ "VC_GW_CAR AS vc_gw_car,VC_GR_WT AS vc_gr_wt, VC_NW_CAR AS vc_nw_car, VC_NT_WT AS vc_nt_wt, VC_REF_FIELD1 AS vc_ref_field1, VC_REF_FIELD2 AS vc_ref_field2, VC_REF_FIELD3 AS vc_ref_field3,"
			+ "VC_REF_FIELD4 AS vc_ref_field4, VC_REF_FIELD5 AS vc_ref_field5, VC_HS_CODE AS vc_hs_code, VC_QC_DT AS vc_qc_dt, VC_REL_DT AS vc_rel_dt, SAP_QUOTATION AS sap_quotation, FILE_UPLOAD_DATE AS file_upload_date,"
			+ "FILE_NAME AS file_name, VC_DIVISION AS vc_division, TO_CHAR(PO_CREATION_DATE, 'DD-MM-YYYY') AS formattedPoCreationDate FROM T_SEG_PURCHASE_ORDER WHERE VC_PO_NO=#{vc_po_no} AND IS_ACTIVE != 'NO'")
	public List<SegPurchaseOrder> getAllLineItemPos(String poNo);
/*	
	@Select("SELECT PO_ID AS po_id, VC_PO_NO AS vc_po_no, NU_CLIENT_CODE AS nu_client_code, SALES_ORG AS sales_org, "
			+ "VC_BUYER AS vc_buyer, VC_BUY_HOUSE AS vc_buy_house, COMPANY_CODE AS company_code, SUPPLIER_CODE AS supplier_code,"
			+ " PO_CREATION_DATE AS currentDate FROM T_PURCHASE_ORDER WHERE PO_CREATION_DATE BETWEEN #{date} AND #{fromdate}")
	public List<HeaderPOData> getPosBasedOnTodateAndFromdate(@Param("date") Date todate, @Param("fromdate") Date fromdate);
*/	
	
	@Select("SELECT PO_ID AS po_id, VC_PO_NO AS vc_po_no, NU_CLIENT_CODE AS nu_client_code, SALES_ORG AS sales_org, "
			+ "VC_BUYER AS vc_buyer, VC_BUY_HOUSE AS vc_buy_house, COMPANY_CODE AS company_code, SUPPLIER_CODE AS supplier_code,"
			+ " PO_CREATION_DATE AS currentDate FROM T_PURCHASE_ORDER WHERE PO_CREATION_DATE >= TO_DATE(#{todate}, 'DD-MM-YY') AND PO_CREATION_DATE <= TO_DATE(#{fromdate}, 'DD-MM-YY')")
	public List<HeaderPOData> getPosBasedOnTodateAndFromdate(@Param("todate") String todate, @Param("fromdate") String fromdate);
	
	@Select("SELECT SEG_ID AS seg_id, PO_ID AS po_id, VC_PO_NO AS vc_po_no, VC_PRODUCT_NO AS vc_product_no, VC_SKU_NO AS vc_sku_no, VC_STYLE_NO AS vc_style_no, VC_ARTICLE_NO AS vc_article_no,"
			+ "VC_COLOR AS vc_color, VC_SIZE AS vc_size, VC_POL AS vc_pol, VC_POD AS vc_pod, NU_NO_PCS_CTNS AS nu_no_pcs_ctns, VC_COMMODITY AS vc_commodity, DT_ETD AS dt_etd, VC_TOT_PCS AS vc_tot_pcs,"
			+ "VC_QUAN AS vc_quan, VC_QUA_UOM AS vc_qua_uom, NU_LENGTH AS nu_length, NU_WIDTH AS nu_width, NU_HIEGHT AS nu_height, VC_IN_HCM AS vc_in_hcm, VC_CBM_SEA AS vc_cbm_sea, VC_VOLUME AS vc_volume,"
			+ "VC_GW_CAR AS vc_gw_car,VC_GR_WT AS vc_gr_wt, VC_NW_CAR AS vc_nw_car, VC_NT_WT AS vc_nt_wt, VC_REF_FIELD1 AS vc_ref_field1, VC_REF_FIELD2 AS vc_ref_field2, VC_REF_FIELD3 AS vc_ref_field3,"
			+ "VC_REF_FIELD4 AS vc_ref_field4, VC_REF_FIELD5 AS vc_ref_field5, VC_HS_CODE AS vc_hs_code, VC_QC_DT AS vc_qc_dt, VC_REL_DT AS vc_rel_dt, SAP_QUOTATION AS sap_quotation, FILE_UPLOAD_DATE AS file_upload_date,"
			+ "FILE_NAME AS file_name, VC_DIVISION AS vc_division, TO_CHAR(PO_CREATION_DATE, 'DD-MM-YYYY') AS formattedPoCreationDate, IS_ACTIVE AS isRecordActive, SEGREGATTED_AGAINST AS segregattedAgainst, PROJECT_NO AS projectNo, UNIT AS unit, COMMODITY AS commodity, PAY_CONDAY_COND AS payCondayCond,"
			+ "UNIT_PRIC_PCS AS unitPricePerPcs, CURRENCY_UNIT AS currencyUnit, PIECES_PER_CTN AS piecesPerCTN, GWEIGHT_PER_CARTON AS gweightPerCarton, NET_WEIGHT_PER_CARTON AS netWeightPerCarton, ITEM_DESCRIPTION AS itemDescription FROM T_SEG_PURCHASE_ORDER WHERE PO_CREATION_DATE >= TO_DATE(#{todate}, 'DD-MM-YY') AND IS_ACTIVE != 'NO'")
	public List<SegPurchaseOrder> getCurrentUploadedData(@Param("todate") String todate);

	/*
	 * new addition
	 */
	@Select("SELECT SEG_ID AS seg_id, PO_ID AS po_id, VC_PO_NO AS vc_po_no, VC_PRODUCT_NO AS vc_product_no, VC_SKU_NO AS vc_sku_no, VC_STYLE_NO AS vc_style_no, VC_ARTICLE_NO AS vc_article_no,"
			+ "VC_COLOR AS vc_color, VC_SIZE AS vc_size, VC_POL AS vc_pol, VC_POD AS vc_pod, NU_NO_PCS_CTNS AS nu_no_pcs_ctns, VC_COMMODITY AS vc_commodity, DT_ETD AS dt_etd, VC_TOT_PCS AS vc_tot_pcs,"
			+ "VC_QUAN AS vc_quan, VC_QUA_UOM AS vc_qua_uom, NU_LENGTH AS nu_length, NU_WIDTH AS nu_width, NU_HIEGHT AS nu_height, VC_IN_HCM AS vc_in_hcm, VC_CBM_SEA AS vc_cbm_sea, VC_VOLUME AS vc_volume,"
			+ "VC_GW_CAR AS vc_gw_car,VC_GR_WT AS vc_gr_wt, VC_NW_CAR AS vc_nw_car, VC_NT_WT AS vc_nt_wt, VC_REF_FIELD1 AS vc_ref_field1, VC_REF_FIELD2 AS vc_ref_field2, VC_REF_FIELD3 AS vc_ref_field3,"
			+ "VC_REF_FIELD4 AS vc_ref_field4, VC_REF_FIELD5 AS vc_ref_field5, VC_HS_CODE AS vc_hs_code, VC_QC_DT AS vc_qc_dt, VC_REL_DT AS vc_rel_dt, SAP_QUOTATION AS sap_quotation, FILE_UPLOAD_DATE AS file_upload_date,"
			+ "FILE_NAME AS file_name, VC_DIVISION AS vc_division, TO_CHAR(PO_CREATION_DATE, 'DD-MM-YYYY') AS formattedPoCreationDate FROM T_SEG_PURCHASE_ORDER WHERE PO_CREATION_DATE >= TO_DATE(#{fromdate}, 'DD-MM-YY') AND PO_CREATION_DATE <= TO_DATE(#{todate}, 'DD-MM-YY') AND IS_ACTIVE != 'NO'")
	public List<SegPurchaseOrder> getLineItemsBasedOnFromdateAndTodate(@Param("fromdate") String fromdate, @Param("todate") String todate);
	
	@Select("SELECT SEG_ID AS seg_id, PO_ID AS po_id, VC_PO_NO AS vc_po_no, VC_PRODUCT_NO AS vc_product_no, VC_SKU_NO AS vc_sku_no, VC_STYLE_NO AS vc_style_no, VC_ARTICLE_NO AS vc_article_no,"
			+ "VC_COLOR AS vc_color, VC_SIZE AS vc_size, VC_POL AS vc_pol, VC_POD AS vc_pod, NU_NO_PCS_CTNS AS nu_no_pcs_ctns, VC_COMMODITY AS vc_commodity, DT_ETD AS dt_etd, VC_TOT_PCS AS vc_tot_pcs,"
			+ "VC_QUAN AS vc_quan, VC_QUA_UOM AS vc_qua_uom, NU_LENGTH AS nu_length, NU_WIDTH AS nu_width, NU_HIEGHT AS nu_height, VC_IN_HCM AS vc_in_hcm, VC_CBM_SEA AS vc_cbm_sea, VC_VOLUME AS vc_volume,"
			+ "VC_GW_CAR AS vc_gw_car,VC_GR_WT AS vc_gr_wt, VC_NW_CAR AS vc_nw_car, VC_NT_WT AS vc_nt_wt, VC_REF_FIELD1 AS vc_ref_field1, VC_REF_FIELD2 AS vc_ref_field2, VC_REF_FIELD3 AS vc_ref_field3,"
			+ "VC_REF_FIELD4 AS vc_ref_field4, VC_REF_FIELD5 AS vc_ref_field5, VC_HS_CODE AS vc_hs_code, VC_QC_DT AS vc_qc_dt, VC_REL_DT AS vc_rel_dt, SAP_QUOTATION AS sap_quotation, FILE_UPLOAD_DATE AS file_upload_date,"
			+ "FILE_NAME AS file_name, VC_DIVISION AS vc_division, TO_CHAR(PO_CREATION_DATE, 'DD-MM-YYYY') AS formattedPoCreationDate FROM T_SEG_PURCHASE_ORDER WHERE PO_CREATION_DATE >= TO_DATE(#{fromdate}, 'DD-MM-YY') AND PO_CREATION_DATE <= TO_DATE(#{todate}, 'DD-MM-YY') AND VC_PO_NO=#{poNo} AND IS_ACTIVE != 'NO'")
	public List<SegPurchaseOrder> getLineItemsBasedOnFromdateAndTodateAndPoNo(@Param("fromdate") String fromdate, @Param("todate") String todate, @Param("poNo") String poNo);
	
	@Select("SELECT SEG_ID AS seg_id, PO_ID AS po_id, VC_PO_NO AS vc_po_no, VC_PRODUCT_NO AS vc_product_no, VC_SKU_NO AS vc_sku_no, VC_STYLE_NO AS vc_style_no, VC_ARTICLE_NO AS vc_article_no,"
			+ "VC_COLOR AS vc_color, VC_SIZE AS vc_size, VC_POL AS vc_pol, VC_POD AS vc_pod, NU_NO_PCS_CTNS AS nu_no_pcs_ctns, VC_COMMODITY AS vc_commodity, DT_ETD AS dt_etd, VC_TOT_PCS AS vc_tot_pcs,"
			+ "VC_QUAN AS vc_quan, VC_QUA_UOM AS vc_qua_uom, NU_LENGTH AS nu_length, NU_WIDTH AS nu_width, NU_HIEGHT AS nu_height, VC_IN_HCM AS vc_in_hcm, VC_CBM_SEA AS vc_cbm_sea, VC_VOLUME AS vc_volume,"
			+ "VC_GW_CAR AS vc_gw_car,VC_GR_WT AS vc_gr_wt, VC_NW_CAR AS vc_nw_car, VC_NT_WT AS vc_nt_wt, VC_REF_FIELD1 AS vc_ref_field1, VC_REF_FIELD2 AS vc_ref_field2, VC_REF_FIELD3 AS vc_ref_field3,"
			+ "VC_REF_FIELD4 AS vc_ref_field4, VC_REF_FIELD5 AS vc_ref_field5, VC_HS_CODE AS vc_hs_code, VC_QC_DT AS vc_qc_dt, VC_REL_DT AS vc_rel_dt, SAP_QUOTATION AS sap_quotation, FILE_UPLOAD_DATE AS file_upload_date,"
			+ "FILE_NAME AS file_name, VC_DIVISION AS vc_division, TO_CHAR(PO_CREATION_DATE, 'DD-MM-YYYY') AS formattedPoCreationDate FROM T_SEG_PURCHASE_ORDER WHERE AND IS_ACTIVE != 'NO'")
	public List<SegPurchaseOrder> getLineItemsDataIfNoInputProvided();
	
	@Select("SELECT SEG_ID AS seg_id, PO_ID AS po_id, VC_PO_NO AS vc_po_no, VC_PRODUCT_NO AS vc_product_no, VC_SKU_NO AS vc_sku_no, VC_STYLE_NO AS vc_style_no, VC_ARTICLE_NO AS vc_article_no,"
			+ "VC_COLOR AS vc_color, VC_SIZE AS vc_size, VC_POL AS vc_pol, VC_POD AS vc_pod, NU_NO_PCS_CTNS AS nu_no_pcs_ctns, VC_COMMODITY AS vc_commodity, DT_ETD AS dt_etd, VC_TOT_PCS AS vc_tot_pcs,"
			+ "VC_QUAN AS vc_quan, VC_QUA_UOM AS vc_qua_uom, NU_LENGTH AS nu_length, NU_WIDTH AS nu_width, NU_HIEGHT AS nu_height, VC_IN_HCM AS vc_in_hcm, VC_CBM_SEA AS vc_cbm_sea, VC_VOLUME AS vc_volume,"
			+ "VC_GW_CAR AS vc_gw_car,VC_GR_WT AS vc_gr_wt, VC_NW_CAR AS vc_nw_car, VC_NT_WT AS vc_nt_wt, VC_REF_FIELD1 AS vc_ref_field1, VC_REF_FIELD2 AS vc_ref_field2, VC_REF_FIELD3 AS vc_ref_field3,"
			+ "VC_REF_FIELD4 AS vc_ref_field4, VC_REF_FIELD5 AS vc_ref_field5, VC_HS_CODE AS vc_hs_code, VC_QC_DT AS vc_qc_dt, VC_REL_DT AS vc_rel_dt, SAP_QUOTATION AS sap_quotation, FILE_UPLOAD_DATE AS file_upload_date,"
			+ "FILE_NAME AS file_name, VC_DIVISION AS vc_division, TO_CHAR(PO_CREATION_DATE, 'DD-MM-YYYY') AS formattedPoCreationDate, IS_ACTIVE AS isRecordActive, SEGREGATTED_AGAINST AS segregattedAgainst, PROJECT_NO AS projectNo, UNIT AS unit, COMMODITY AS commodity, PAY_CONDAY_COND AS payCondayCond,"
			+ "UNIT_PRIC_PCS AS unitPricePerPcs, CURRENCY_UNIT AS currencyUnit, PIECES_PER_CTN AS piecesPerCTN, GWEIGHT_PER_CARTON AS gweightPerCarton, NET_WEIGHT_PER_CARTON AS netWeightPerCarton, ITEM_DESCRIPTION AS itemDescription FROM T_SEG_PURCHASE_ORDER WHERE PO_ID=#{poId}  AND IS_ACTIVE != 'NO'")
	public List<SegPurchaseOrder> getCurrentUploadedDataNew(@Param("poId") Long poId);
	
	@Update("UPDATE T_SEG_PURCHASE_ORDER SET VC_PRODUCT_NO=#{vc_product_no}, VC_SKU_NO=#{vc_sku_no}, VC_STYLE_NO=#{vc_style_no}, VC_ARTICLE_NO=#{vc_article_no}, VC_COLOR=#{vc_color}, VC_SIZE=#{vc_size}, VC_POL=#{vc_pol}, VC_POD=#{vc_pod},"
			+ "NU_NO_PCS_CTNS=#{nu_no_pcs_ctns}, VC_COMMODITY=#{vc_commodity}, VC_TOT_PCS=#{vc_tot_pcs}, VC_QUAN=#{vc_quan}, NU_LENGTH=#{nu_length}, NU_WIDTH=#{nu_width}, NU_HIEGHT=#{nu_height}, VC_CBM_SEA=#{vc_cbm_sea}, VC_GR_WT=#{vc_gr_wt}, VC_NT_WT=#{vc_nt_wt}, VC_REF_FIELD1=#{vc_ref_field1},"
			+ "VC_REF_FIELD2=#{vc_ref_field2}, VC_REF_FIELD3=#{vc_ref_field3}, VC_REF_FIELD4=#{vc_ref_field4}, VC_REF_FIELD5=#{vc_ref_field5}, VC_VOLUME=#{vc_volume}, VC_HS_CODE=#{vc_hs_code} "
			+ "WHERE SEG_ID=#{seg_id}")
	public Long updatePoOnSegId(SegPurchaseOrder segPurchaseOrder);
}






