package com.myshipment.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import com.myshipment.model.BuyerDTO;
import com.myshipment.model.DailySaleReportParams;
import com.myshipment.model.DailySalesReportHawbParams;
import com.myshipment.model.HMCountryList;
/*import com.myshipment.model.DailySaleReportParams;
import com.myshipment.model.DailySalesReportHawbParams;
import com.myshipment.model.HMCountryList;*/
import com.myshipment.model.WarehouseCode;

public interface HMMapper {

	public final String SELECT_COUNTRY_LIST = "SELECT CODE AS code,COUNTRY AS country,ZONE as zone FROM HM_COUNTRYLIST";
	public final String SELECT_MAWB_DETAILS = "SELECT MAWB AS mawb,GHA AS gha,BOOKED AS booked,ORIGIN AS origin,DESTINATION AS destination,TOTAL_PIECES AS totalPieces,WEIGHT AS weigth,CWT AS cwt,CBM AS cbm,TYPES_OF_SHIPMENT AS typesOfShipment,CARGO_HANDOVER_DATE AS grDate,PRE_ALERT_DATE AS preAlertDate,ETA AS eta,ETD AS etd,DIIF_ETA_ATA AS diff,ATA_AIRPORT AS ataPort,WH_DATE AS whDate,TYPE_OF_DELIVERY AS typeOfDelivery,PU_TIME AS puTime,SLOT_REQUEST_DATE AS slotReqDt,SLOT_RECEIVE_DATE AS slotRecDt,CARGO_STATUS AS cargoStatus,EXCEPTION AS exception,REMARKS AS remarks,WEEKEND_HOURS AS weekendHours,PRE_NOTICE_REQ AS preNoticeReq,DLV_AFTER_ARRIVAL AS dlvDate,ARRIVAL_TO_DLV AS arrToDlvDate,ARRIVAL_TO_TRUCK AS arrToTruckDep,TRUCK_TYPE AS truckType,TRUCK_BOOKING_DATE AS truckBookingDate,TRUCK_CANCELLATION_DATE AS truckCancelDate,LICENSE_PLATE AS licensePlate,DRIVER_DETAILS AS driverDetails,TOD_WEEK AS week,ATA_GHA_SOV AS ghaToSovAta,ATD_GHA_SOV AS ghaToSovAtd,YEAR AS year,TSP AS tsp,FREIGHT_SERVICE AS freightService,ATD_ORIGIN AS atd,ATA_DESTINATION AS ata,CUT_OFF_DATE AS cutOffDate,ETA_DEADLINE AS etaDeadline,P2P_TRANSIT AS portToPort,P2DC_TRANSIT AS portToDc,DIFF_DAYS AS dayDifference FROM T_MAWB_DATA WHERE MAWB=#{mawb}";
	public final String SELECT_HAWB_DETAILS = "SELECT HAWB AS hawb,MAWB AS mawb,ACTUAL_RECEIVER AS receiver,INCOTERMS AS incoterms,ORIGIN AS origin,DEST AS destination,GW AS grossWeight,VOULME AS volume,CW AS chargeableWeight FROM T_HAWB_DATA WHERE MAWB=#{mawb}";
	public final String SELECT_BUYER_LIST = "SELECT BUYER_ID AS buyerID,BUYER_NAME AS buyerName FROM HM_BUYERS WHERE BUYER_ID=#{buyer}";
	public final String SELECT_WC_LIST = "SELECT PM AS pm,COUNTRY_CODE AS countryCode,WAREHOUSE_CODE AS whCode FROM HM_WAREHOUSE WHERE COUNTRY_CODE=#{country}";

	
	@Select(SELECT_COUNTRY_LIST)
	public List<HMCountryList> allCountry();

	@Select(SELECT_MAWB_DETAILS)
	public DailySaleReportParams mawb(@Param("mawb") String mawb);
	
	@Select(SELECT_HAWB_DETAILS)
	public List<DailySalesReportHawbParams> hawbList(@Param("mawb") String mawb);

	@Select(SELECT_BUYER_LIST)
	public List<BuyerDTO> getBuyerList(@Param("buyer") String buyer,@Param("disChnlSelected") String disChnlSelected, @Param("divisionSelected") String divisionSelected,@Param("salesOrgSelected") String salesOrgSelected);

	@Select(SELECT_WC_LIST)
	public List<WarehouseCode> getWHList(@Param("country") String country);
	
}
