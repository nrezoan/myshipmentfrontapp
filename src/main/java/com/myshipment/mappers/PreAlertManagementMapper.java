/**
*
*@author Ahmad Naquib
*
*/
package com.myshipment.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.myshipment.model.PreAlertHBL;

public interface PreAlertManagementMapper {
	
	public final String INSERT_PRE_ALERT_HBL_AFTER_BOOKING = "INSERT INTO T_PRE_ALERT_HBL(HBL_NUMBER, SUPPLIER_ID, BUYER_ID, AGENT_ID, DISTRIBUTION_CHANNEL, DIVISION, SALES_ORG, IS_MBL, IS_PRE_ALERT_DOC, DOWNLOADED) "
			+ "VALUES(#{hbl_number}, #{supplier_id}, #{buyer_id}, #{agent_id}, #{distribution_channel}, #{division}, #{sales_org}, '0', '0', '0')";
	
	public final String SELECT_PRE_ALERT_HBL_SUPPLIER_WISE = "SELECT HBL_NUMBER AS hblNumber, SUPPLIER_ID AS supplierId, BUYER_ID AS buyerId, AGENT_ID AS agentId, "
			+ "FILE_PATH_PRE_ALERT_DOC AS filePathPreAlertDoc, "
			+ "FILE_PATH_MBL AS filePathMBL, IS_PRE_ALERT_DOC AS isPreAlertDoc, "
			+ "DISTRIBUTION_CHANNEL AS distributionChannel, DIVISION AS division, "
			+ "SALES_ORG AS salesOrg, CRM_ID AS crmId, "
			+ "IS_MBL AS isMBL, DOWNLOADED AS downloaded, FK_MBL_NUMBER AS mblNumber "
			+ "FROM T_PRE_ALERT_HBL WHERE SUPPLIER_ID=#{supplier_id} AND DOWNLOADED='0'";
	
	/*public final String SELECT_PRE_ALERT_HBL_BUYER_WISE = "SELECT HBL_NUMBER AS hblNumber, SUPPLIER_ID AS supplierId, BUYER_ID AS buyerId, AGENT_ID AS agentId, "
			+ "FILE_PATH_PRE_ALERT_DOC AS filePathPreAlertDoc, "
			+ "FILE_PATH_MBL AS filePathMBL, IS_PRE_ALERT_DOC AS isPreAlertDoc, "
			+ "DISTRIBUTION_CHANNEL AS distributionChannel, DIVISION AS division, "
			+ "SALES_ORG AS salesOrg, CRM_ID AS crmId, "
			+ "IS_MBL AS isMBL, DOWNLOADED AS downloaded, FK_MBL_NUMBER AS mblNumber "
			+ "FROM T_PRE_ALERT_HBL WHERE BUYER_ID=#{preAlertHbl.buyerId} AND DISTRIBUTION_CHANNEL=#{preAlertHbl.distributionChannel} "
			+ "AND DIVISION=#{preAlertHbl.division} AND SALES_ORG=#{preAlertHbl.salesOrg} AND DOWNLOADED='0'";*/
	
	public final String SELECT_PRE_ALERT_HBL_BUYER_WISE_ALL = "SELECT HBL.HBL_NUMBER AS hblNumber, "
			+ "HBL.SUPPLIER_ID AS supplierId, HBL.BUYER_ID AS buyerId, HBL.AGENT_ID AS agentId, "
			+ "HBL.FILE_PATH_PRE_ALERT_DOC AS filePathPreAlertDoc, " 
			+ "MBL.FILE_PATH_MBL AS filePathMBL, HBL.IS_PRE_ALERT_DOC AS isPreAlertDoc, " 
			+ "HBL.DISTRIBUTION_CHANNEL AS distributionChannel, HBL.DIVISION AS division, " 
			+ "HBL.SALES_ORG AS salesOrg, HBL.CRM_ID AS crmId, "
			+ "(CASE WHEN MBL.IS_MBL IS NULL THEN '0' ELSE MBL.IS_MBL END) as isMBL, " 
			+ "HBL.DOWNLOADED AS downloaded, MBL.MBL_NUMBER AS mblNumber "
			+ "FROM T_PRE_ALERT_HBL HBL "
			+ "LEFT OUTER JOIN T_PRE_ALERT_MBL MBL ON HBL.FK_MBL_NUMBER=MBL.MBL_NUMBER "
			+ "WHERE HBL.BUYER_ID=#{preAlertHbl.buyerId} "
			+ "AND HBL.SALES_ORG=#{preAlertHbl.salesOrg} "
			+ "AND HBL.DISTRIBUTION_CHANNEL=#{preAlertHbl.distributionChannel} "
			+ "AND HBL.DIVISION=#{preAlertHbl.division} "
			+ "AND HBL.DOWNLOADED='0'";
	
	public final String SELECT_PRE_ALERT_HBL_BUYER_WISE_PENDING = "SELECT HBL.HBL_NUMBER AS hblNumber, "
			+ "HBL.SUPPLIER_ID AS supplierId, HBL.BUYER_ID AS buyerId, HBL.AGENT_ID AS agentId, "
			+ "HBL.FILE_PATH_PRE_ALERT_DOC AS filePathPreAlertDoc, " 
			+ "MBL.FILE_PATH_MBL AS filePathMBL, HBL.IS_PRE_ALERT_DOC AS isPreAlertDoc, " 
			+ "HBL.DISTRIBUTION_CHANNEL AS distributionChannel, HBL.DIVISION AS division, " 
			+ "HBL.SALES_ORG AS salesOrg, HBL.CRM_ID AS crmId, "
			+ "(CASE WHEN MBL.IS_MBL IS NULL THEN '0' ELSE MBL.IS_MBL END) as isMBL, " 
			+ "HBL.DOWNLOADED AS downloaded, MBL.MBL_NUMBER AS mblNumber "
			+ "FROM T_PRE_ALERT_HBL HBL "
			+ "LEFT OUTER JOIN T_PRE_ALERT_MBL MBL ON HBL.FK_MBL_NUMBER=MBL.MBL_NUMBER "
			+ "WHERE HBL.BUYER_ID=#{preAlertHbl.buyerId} "
			+ "AND HBL.SALES_ORG=#{preAlertHbl.salesOrg} "
			+ "AND HBL.DISTRIBUTION_CHANNEL=#{preAlertHbl.distributionChannel} "
			+ "AND HBL.DIVISION=#{preAlertHbl.division} "
			+ "AND (MBL.IS_MBL IS NULL OR MBL.IS_MBL='0') "
			+ "AND HBL.DOWNLOADED='0'";
	
	public final String SELECT_PRE_ALERT_HBL_BUYER_WISE_COMPLETED = "SELECT HBL.HBL_NUMBER AS hblNumber, "
			+ "HBL.SUPPLIER_ID AS supplierId, HBL.BUYER_ID AS buyerId, HBL.AGENT_ID AS agentId, "
			+ "HBL.FILE_PATH_PRE_ALERT_DOC AS filePathPreAlertDoc, " 
			+ "MBL.FILE_PATH_MBL AS filePathMBL, HBL.IS_PRE_ALERT_DOC AS isPreAlertDoc, " 
			+ "HBL.DISTRIBUTION_CHANNEL AS distributionChannel, HBL.DIVISION AS division, " 
			+ "HBL.SALES_ORG AS salesOrg, HBL.CRM_ID AS crmId, "
			+ "(CASE WHEN MBL.IS_MBL IS NULL THEN '0' ELSE MBL.IS_MBL END) as isMBL, " 
			+ "HBL.DOWNLOADED AS downloaded, MBL.MBL_NUMBER AS mblNumber "
			+ "FROM T_PRE_ALERT_HBL HBL "
			+ "LEFT OUTER JOIN T_PRE_ALERT_MBL MBL ON HBL.FK_MBL_NUMBER=MBL.MBL_NUMBER "
			+ "WHERE HBL.BUYER_ID=#{preAlertHbl.buyerId} "
			+ "AND HBL.SALES_ORG=#{preAlertHbl.salesOrg} "
			+ "AND HBL.DISTRIBUTION_CHANNEL=#{preAlertHbl.distributionChannel} "
			+ "AND HBL.DIVISION=#{preAlertHbl.division} "
			+ "AND (MBL.IS_MBL='1') "
			+ "AND HBL.DOWNLOADED='0'";
	
	public final String SELECT_PRE_ALERT_HBL_SUPPLIER_AND_HBL_WISE = "SELECT HBL_NUMBER AS hblNumber, SUPPLIER_ID AS supplierId, BUYER_ID AS buyerId, AGENT_ID AS agentId, "
			+ "FILE_PATH_PRE_ALERT_DOC AS filePathPreAlertDoc, "
			+ "FILE_PATH_MBL AS filePathMBL, IS_PRE_ALERT_DOC AS isPreAlertDoc, "
			+ "DISTRIBUTION_CHANNEL AS distributionChannel, DIVISION AS division, "
			+ "SALES_ORG AS salesOrg, CRM_ID AS crmId, "
			+ "IS_MBL AS isMBL, DOWNLOADED AS downloaded, FK_MBL_NUMBER AS mblNumber "
			+ "FROM T_PRE_ALERT_HBL WHERE (SUPPLIER_ID=#{myshipment_uid} OR BUYER_ID=#{myshipment_uid}) AND HBL_NUMBER=#{preAlertHbl.hblNumber} "
			+ "AND DISTRIBUTION_CHANNEL=#{preAlertHbl.distributionChannel} AND DIVISION=#{preAlertHbl.division} "
			+ "AND SALES_ORG=#{preAlertHbl.salesOrg} AND DOWNLOADED='0'";
	
	public final String SELECT_AUTHORIZATION_CHECK_HBL = "SELECT HBL_NUMBER AS hblNumber, SUPPLIER_ID AS supplierId, BUYER_ID AS buyerId, AGENT_ID AS agentId, "
			+ "FILE_PATH_PRE_ALERT_DOC AS filePathPreAlertDoc, "
			+ "FILE_PATH_MBL AS filePathMBL, IS_PRE_ALERT_DOC AS isPreAlertDoc, "
			+ "DISTRIBUTION_CHANNEL AS distributionChannel, DIVISION AS division, "
			+ "SALES_ORG AS salesOrg, CRM_ID AS crmId, "
			+ "IS_MBL AS isMBL, DOWNLOADED AS downloaded, FK_MBL_NUMBER AS mblNumber "
			+ "FROM T_PRE_ALERT_HBL WHERE (SUPPLIER_ID=#{preAlertHbl.supplierId} OR BUYER_ID=#{preAlertHbl.buyerId}) "
			+ "AND HBL_NUMBER=#{preAlertHbl.hblNumber} "
			+ "AND DISTRIBUTION_CHANNEL=#{preAlertHbl.distributionChannel} AND DIVISION=#{preAlertHbl.division} "
			+ "AND SALES_ORG=#{preAlertHbl.salesOrg}";
	
	public final String UPDATE_PRE_ALERT_DOCUMENT_PATH = "UPDATE T_PRE_ALERT_HBL "
			+ "SET FILE_PATH_PRE_ALERT_DOC=#{preAlertHbl.filePathPreAlertDoc}, "
			+ "IS_PRE_ALERT_DOC='1', UPDATED_AT=systimestamp, "
			+ "CRM_ID=#{preAlertHbl.crmId} "
			+ "WHERE HBL_NUMBER=#{preAlertHbl.hblNumber} "
			+ "AND DISTRIBUTION_CHANNEL=#{preAlertHbl.distributionChannel} "
			+ "AND DIVISION=#{preAlertHbl.division} "
			+ "AND SALES_ORG=#{preAlertHbl.salesOrg}";
	
	public final String UPDATE_MBL_DOWNLAOD_STATUS = "UPDATE T_PRE_ALERT_HBL "
			+ "SET DOWNLOADED='1' "
			+ "WHERE FK_MBL_NUMBER=#{preAlertHbl.mblNumber} "
			+ "AND DISTRIBUTION_CHANNEL=#{preAlertHbl.distributionChannel} "
			+ "AND DIVISION=#{preAlertHbl.division} "
			+ "AND SALES_ORG=#{preAlertHbl.salesOrg}";
	
	public final String UPDATE_PRE_ALERT_PACKING_LIST = "UPDATE T_PRE_ALERT_HBL "
			+ "SET FILE_PATH_PACKING_LIST=#{packing_list_path}, IS_PACKING_LIST='1', UPDATED_AT=systimestamp "
			+ "WHERE HBL_NUMBER=#{hbl_number}";
	
/*	public final String SELECT_PRE_ALERT_READY_TO_DOWNLOAD = "SELECT HBL_NUMBER AS hblNumber, SUPPLIER_ID AS supplierId, BUYER_ID AS buyerId, AGENT_ID AS agentId, "
			+ "FILE_PATH_PRE_ALERT_DOC AS filePathPreAlertDoc, " 
			+ "FILE_PATH_MBL AS filePathMBL, IS_PRE_ALERT_DOC AS isPreAlertDoc, " 
			+ "DISTRIBUTION_CHANNEL AS distributionChannel, DIVISION AS division, " 
			+ "SALES_ORG AS salesOrg, CRM_ID AS crmId, " 
			+ "IS_MBL AS isMBL, DOWNLOADED AS downloaded, FK_MBL_NUMBER AS mblNumber "
			+ "FROM T_PRE_ALERT_HBL "
			+ "WHERE (AGENT_ID=#{myshipment_uid} OR BUYER_ID=#{myshipment_uid}) AND SALES_ORG=#{preAlertHbl.salesOrg} "
			+ "AND DISTRIBUTION_CHANNEL=#{preAlertHbl.distributionChannel} AND DIVISION=#{preAlertHbl.division} "
			+ "AND DOWNLOADED='0'";*/

	public final String SELECT_PRE_ALERT_READY_TO_DOWNLOAD = "SELECT HBL.HBL_NUMBER AS hblNumber, HBL.SUPPLIER_ID AS supplierId, HBL.BUYER_ID AS buyerId, HBL.AGENT_ID AS agentId, "
			+"HBL.FILE_PATH_PRE_ALERT_DOC AS filePathPreAlertDoc, "
			+"MBL.FILE_PATH_MBL AS filePathMBL, HBL.IS_PRE_ALERT_DOC AS isPreAlertDoc, "
			+"HBL.DISTRIBUTION_CHANNEL AS distributionChannel, HBL.DIVISION AS division, "
			+"HBL.SALES_ORG AS salesOrg, MBL.CRM_ID AS crmId, MBL.UPDATED_AT AS updatedAt,"
			+"MBL.IS_MBL AS isMBL, HBL.DOWNLOADED AS downloaded, HBL.FK_MBL_NUMBER AS mblNumber "
			+"FROM T_PRE_ALERT_HBL HBL "
			+"JOIN T_PRE_ALERT_MBL MBL ON HBL.FK_MBL_NUMBER=MBL.MBL_NUMBER "
			+"WHERE (AGENT_ID=#{myshipment_uid} OR BUYER_ID=#{myshipment_uid}) AND SALES_ORG=#{preAlertHbl.salesOrg} "
			+"AND DISTRIBUTION_CHANNEL=#{preAlertHbl.distributionChannel} AND DIVISION=#{preAlertHbl.division} "
			+"AND DOWNLOADED='0'";
	
	
	public final String SELECT_PRE_ALERT_MBL_WISE = "SELECT HBL.HBL_NUMBER AS hblNumber, "
			+ "HBL.SUPPLIER_ID AS supplierId, HBL.BUYER_ID AS buyerId, HBL.AGENT_ID AS agentId, "
			+ "HBL.FILE_PATH_PRE_ALERT_DOC AS filePathPreAlertDoc, " 
			+ "MBL.FILE_PATH_MBL AS filePathMBL, HBL.IS_PRE_ALERT_DOC AS isPreAlertDoc, " 
			+ "HBL.DISTRIBUTION_CHANNEL AS distributionChannel, HBL.DIVISION AS division, " 
			+ "HBL.SALES_ORG AS salesOrg, HBL.CRM_ID AS crmId, " 
			+ "MBL.IS_MBL AS isMBL, HBL.DOWNLOADED AS downloaded, MBL.MBL_NUMBER AS mblNumber "
			+ "FROM T_PRE_ALERT_HBL HBL "
			+ "JOIN T_PRE_ALERT_MBL MBL ON HBL.FK_MBL_NUMBER=MBL.MBL_NUMBER "
			+ "WHERE HBL.FK_MBL_NUMBER=#{preAlertHbl.mblNumber} "
			+ "AND (HBL.AGENT_ID=#{myshipment_uid} OR HBL.BUYER_ID=#{myshipment_uid}) "
			+ "AND HBL.SALES_ORG=#{preAlertHbl.salesOrg} "
			+ "AND HBL.DISTRIBUTION_CHANNEL=#{preAlertHbl.distributionChannel} "
			+ "AND HBL.DIVISION=#{preAlertHbl.division} "
			+ "AND HBL.DOWNLOADED='0'";
	
	@Insert(INSERT_PRE_ALERT_HBL_AFTER_BOOKING)
	public int insertPreAlertHBLafterBooking(@Param("hbl_number") String hblNumber, @Param("supplier_id") String supplierId, @Param("buyer_id") String buyerId, @Param("agent_id") String agentId, @Param("distribution_channel") String distributionChannel, @Param("division") String division, @Param("sales_org") String salesOrg);
	
	@Update(UPDATE_PRE_ALERT_DOCUMENT_PATH)
	public int updatePreAlertDocumentPath(@Param("preAlertHbl") PreAlertHBL preAlertHbl);
	
	@Update(UPDATE_MBL_DOWNLAOD_STATUS)
	public int updateMblDownloadStatus(@Param("preAlertHbl") PreAlertHBL preAlertHbl);

	@Update(UPDATE_PRE_ALERT_PACKING_LIST)
	public int updatePreAlertPackingList(@Param("packing_list_path") String packingListPath, @Param("hbl_number") String hblNumber);

	@Select(SELECT_PRE_ALERT_HBL_SUPPLIER_WISE)
	public List<PreAlertHBL> selectPreAlertHblList(@Param("supplier_id") String supplierId);
	
	//BUYER-WISE PREALERTS(MMBL/MAWB)
	@Select(SELECT_PRE_ALERT_HBL_BUYER_WISE_ALL)
	public List<PreAlertHBL> selectBuyerPreAlertHblListALL(@Param("preAlertHbl") PreAlertHBL preAlertHbl);
	
	@Select(SELECT_PRE_ALERT_HBL_BUYER_WISE_PENDING)
	public List<PreAlertHBL> selectBuyerPreAlertHblListPENDING(@Param("preAlertHbl") PreAlertHBL preAlertHbl);
	
	@Select(SELECT_PRE_ALERT_HBL_BUYER_WISE_COMPLETED)
	public List<PreAlertHBL> selectBuyerPreAlertHblListCOMPLETED(@Param("preAlertHbl") PreAlertHBL preAlertHbl);
	//----------
	
	@Select(SELECT_PRE_ALERT_HBL_SUPPLIER_AND_HBL_WISE)
	public PreAlertHBL selectPreAlertHbl(@Param("preAlertHbl") PreAlertHBL preAlertHbl, @Param("myshipment_uid") String myshipmentUid);
	
	@Select(SELECT_PRE_ALERT_READY_TO_DOWNLOAD)
	public List<PreAlertHBL> selectPreAlertReadyToDownload(@Param("myshipment_uid") String myshipmentUID, @Param("preAlertHbl")PreAlertHBL preAlertHbl);
	
	@Select(SELECT_AUTHORIZATION_CHECK_HBL)
	public PreAlertHBL checkAuthHBL(@Param("preAlertHbl") PreAlertHBL preAlertHbl);
	
	@Select(SELECT_PRE_ALERT_MBL_WISE)
	public List<PreAlertHBL> mblWisePreAlert(@Param("myshipment_uid") String myshipmentUID, @Param("preAlertHbl") PreAlertHBL preAlertHbl);
	
}
