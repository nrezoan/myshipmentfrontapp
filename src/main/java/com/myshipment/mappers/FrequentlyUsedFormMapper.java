package com.myshipment.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;

import com.myshipment.dto.LoginDTO;
import com.myshipment.model.FrequentUsedMenu;
import com.myshipment.model.FrequentlyUsedFormMdl;
/*
 * @ Gufranur Rahman
 */
public interface FrequentlyUsedFormMapper {

	
	@SelectKey(keyProperty="pageFeq_Id",before=true,resultType=Long.class,statement={"select T_PAGE_FREQUENCY_SEQ.nextval AS pageFeq_Id from dual"})
	@Insert("insert into T_PAGE_FREQUENCY(PAGE_FRQ_ID,USER_NAME,COMPANY,DIST_CHANNEL,DIVISION,PAGE_NAME,CONTEXT_PATH,COUNTER) values(#{pageFeq_Id},#{customerCode},#{company},#{distChannel},#{divison},#{pageName},#{context},#{counter})")
	public int saveFrequentlyUserForm(FrequentlyUsedFormMdl obj);
	
	@Select("SELECT PAGE_FRQ_ID AS pageFeq_Id, USER_NAME AS customerCode, COMPANY AS company, DIST_CHANNEL AS distChannel, DIVISION AS divison, PAGE_NAME AS pageName, CONTEXT_PATH AS context,"
			+ "COUNTER AS counter FROM T_PAGE_FREQUENCY WHERE USER_NAME = #{customerCode} AND COMPANY = #{company} AND DIST_CHANNEL=#{distChannel} AND DIVISION = #{divison} AND PAGE_NAME= #{pageName}")
	public List<FrequentlyUsedFormMdl> getFrequentlyUsedFormInfo(@Param("customerCode") String customerCode,@Param("company") String company, @Param("distChannel") String distChannel, @Param("divison") String divison,@Param("pageName") String pageName);
	

	@Update("update T_PAGE_FREQUENCY set COUNTER=#{counter} WHERE USER_NAME = #{customerCode} AND COMPANY = #{company} AND DIST_CHANNEL=#{distChannel} AND DIVISION = #{divison} AND PAGE_NAME= #{pageName}")
	public int updateCounterOfFrequentlyUsedMenu(FrequentlyUsedFormMdl obj);
	

	@Select("SELECT  PAGE_NAME AS menuName, CONTEXT_PATH AS url,COUNTER AS counter "
			+ "FROM T_PAGE_FREQUENCY WHERE USER_NAME = #{loggedInUserName} and dist_channel = #{disChnlSelected} and division = #{divisionSelected} and company = #{salesOrgSelected} and COUNTER>9  and  rownum <= 5 order by COUNTER desc")
	public List<FrequentUsedMenu> getFrequentlyUsedMenu(LoginDTO loginDTO);
	
	
}
