package com.myshipment.mappers;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.myshipment.model.BookedPOApproval;

public interface BookedPOApprovalMapper {
	
	public final String INSERT_BOOKED_PO_APPROVAL_DATA = "INSERT INTO T_BOOKED_PO_APPROVAL (SO_NO,BL_NO,ITEM_NO,PO_NO,ITEM_BL_DT,ITEM_PCS,ITEM_QTY,STYLE,ITEM_VOL,ITEM_CHDT,INDC_DT,PO_REQ_SHIP_DT,FVSL_NAME,FETD,MVSL_NAME,META,CARRIER_NAME,STATUS,REMARKS,BUYER_NO,SIZE_NO,COLOR,ITEM_GRWT,SHIPPER_NO,ACTION_USER,ACTION_TIMESTAMP,BUYER_NAME,SHIPPER_NAME) VALUES (#{so_no},#{bl_no},#{item_no},#{po_no},#{item_bl_dt},#{item_pcs},#{item_qty},#{style},#{item_vol},#{item_chdt},#{indc_dt},#{po_req_ship_dt},#{fvsl_name},#{fetd},#{mvsl_name},#{meta},#{carrier_name},#{status},#{remarks},#{buyer_no},#{size_no},#{color},#{item_grwt},#{shipper_no},#{action_user},#{action_timestamp},#{buyer_name},#{shipper_name})";
	public final String UPDATE_BOOKED_PO_APPROVAL_DATA = "UPDATE T_BOOKED_PO_APPROVAL SET INDC_DT=#{indc_dt},PO_REQ_SHIP_DT=#{po_req_ship_dt},FVSL_NAME=#{fvsl_name},FETD=#{fetd},MVSL_NAME=#{mvsl_name},META=#{meta},CARRIER_NAME=#{carrier_name},STATUS=#{status},REMARKS=#{remarks},ACTION_TIMESTAMP=#{action_timestamp} WHERE SO_NO=#{so_no} AND BL_NO=#{bl_no} AND ITEM_NO=#{item_no}";
	public final String COUNT_BOOKED_PO_APPROVAL_DATA = "SELECT COUNT(*) FROM T_BOOKED_PO_APPROVAL WHERE SO_NO=#{so_no} AND BL_NO=#{bl_no} AND ITEM_NO=#{item_no}";
	public final String FETCH_BOOKED_PO_DATA_DATE_PARAMS = "SELECT * FROM T_BOOKED_PO_APPROVAL WHERE BUYER_NO=#{param3} AND ITEM_BL_DT BETWEEN #{param1} AND #{param2}";
	public final String FETCH_BOOKED_PO_DATA_BY_STATUS = "SELECT * FROM T_BOOKED_PO_APPROVAL WHERE STATUS=#{param3} AND BUYER_NO=#{param4} AND ITEM_BL_DT BETWEEN #{param1} AND #{param2}";
	
	@Insert(INSERT_BOOKED_PO_APPROVAL_DATA)
	public void saveApprovedBookedPO(BookedPOApproval bookedPOApproval);
	
	@Select(COUNT_BOOKED_PO_APPROVAL_DATA)
	public int getApprovedBookedPOByPrimaryKeys(BookedPOApproval bookedPOApproval);

	@Update(UPDATE_BOOKED_PO_APPROVAL_DATA)
	public void updateApprovedBookedPO(BookedPOApproval bookedPOApproval);

	@Select(FETCH_BOOKED_PO_DATA_DATE_PARAMS)
	public List<BookedPOApproval> fetchDataByDateParams(Date startDate, Date endDate, String buyer);
	
	@Select(FETCH_BOOKED_PO_DATA_BY_STATUS)
	public List<BookedPOApproval> fetchDataByStatus(Date startDate, Date endDate, String status, String buyer);
}
