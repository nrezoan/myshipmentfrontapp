package com.myshipment.mappers;

import com.myshipment.model.ApproOrderCarrierDtl;
import com.myshipment.model.ApprovedPurchaseOrder;
import com.myshipment.model.CarrierMaster;
import com.myshipment.model.Materials;
import com.myshipment.model.PackListColor;
import com.myshipment.model.PackListSize;
import com.myshipment.model.PackingListMdl;
import com.myshipment.model.PurchaseOrderMdl;
import com.myshipment.model.SaveAsTemplateMdl;
import com.myshipment.model.SegPurchaseOrderMdl;
import com.myshipment.model.StorageLocation;
import com.myshipment.model.UpdateEvent;
import java.util.List;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;

public abstract interface PoMapper
{
  public abstract List<PurchaseOrderMdl> selectAllPO();
  
  public abstract PurchaseOrderMdl selectPoLineItemByPoId(Long paramLong);
  
  @SelectKey(keyProperty="app_id", before=true, resultType=Long.class, statement={"SELECT t_approved_purchase_order_seq.nextval AS app_id FROM dual"})
  @Insert({"insert into t_approved_purchase_order(APP_ID,SEG_ID,VC_PO_NO,NU_CLIENT_CODE,VC_PRODUCT_NO,VC_SKU_NO,VC_ARTICLE_NO,VC_COLOR,VC_SIZE,VC_STYLE_NO,VC_POL,VC_POD,NU_NO_PCS_CTNS,VC_COMMODITY,DT_ETD ,VC_TOT_PCS,VC_QUAN,VC_QUA_UOM,NU_LENGTH,NU_WIDTH,NU_HIEGHT,VC_IN_HCM,VC_VOLUME,VC_GW_CAR,VC_GR_WT,VC_NW_CAR,VC_NT_WT,VC_REF_FIELD1,VC_REF_FIELD2,VC_REF_FIELD3,VC_REF_FIELD4,VC_REF_FIELD5,VC_HS_CODE,VC_QC_DT,VC_REL_DT,SALES_ORG,SAP_QUOTATION,FILE_UPLOAD_DATE,VC_BUYER,VC_BUY_HOUSE,FILE_NAME,VC_DIVISION,BOOKING_STATUS,APPROVE_COMMENT,CARRIER_ID,CARRIER_SCHEDULE,VC_CBM_SEA)  values(#{app_id},#{seg_id},#{vc_po_no},#{nu_client_code},#{vc_product_no},#{vc_sku_no},#{vc_article_no},#{vc_color},#{vc_size},#{vc_style_no},#{vc_pol},#{vc_pod},#{nu_no_pcs_ctns},#{vc_commodity},#{dt_etd}.,#{vc_tot_pcs},#{vc_quan},#{vc_qua_uom},#{nu_length},#{nu_width},#{nu_hieght},#{vc_in_hcm},#{vc_volume},#{vc_gw_car},#{vc_gr_wt},#{vc_nw_car},#{vc_nt_wt},#{vc_ref_field1},#{vc_ref_field2},#{vc_ref_field3},#{vc_ref_field4},#{vc_ref_field5},#{vc_hs_code},#{vc_qc_dt},#{vc_rel_dt},#{sales_org},#{sap_quotation},#{file_upload_date},#{vc_buyer},#{vc_buy_house},#{file_name},#{vc_division},#{booking_status},#{approve_comment},#{carrier_id},to_date(#{carrier_schedule},'dd-mm-yyyy'),#{vc_cbm_sea})"})
  public abstract Long updateApprovedPo(ApprovedPurchaseOrder paramApprovedPurchaseOrder);
  
  public abstract SegPurchaseOrderMdl selectPoLineItembyLineItemId(Long paramLong);
  
  @SelectKey(keyProperty="up_ev_id", before=true, resultType=Long.class, statement={"SELECT T_UPDATE_EVENT_SEQ.nextval AS up_ev_id FROM dual"})
  @Insert({"insert into t_update_event(UP_EV_ID,VC_PO_NO,SEG_ID,ACTION,ITEMS) values(#{up_ev_id},#{vc_po_no},#{seg_id},#{action},#{items})"})
  public abstract void updatePoUpdateEvent(UpdateEvent paramUpdateEvent);
  
  public abstract List<ApprovedPurchaseOrder> selectAppPoLineItembyLineItemId(Long paramLong);
  
  public abstract List<CarrierMaster> getAllCarriers();
  
  public abstract List<PurchaseOrderMdl> searchPo(@Param("poNumber") String paramString1, @Param("fromDate") String paramString2, @Param("toDate") String paramString3, @Param("supplierCode") String paramString4, @Param("buyerCode") String paramString5);
  
  @Update({"update t_seg_purchase_order set VC_TOT_PCS=#{vc_tot_pcs}, CARRIER_ID=#{carrier_id}, CARRIER_SCHEDULE=to_date(#{carrier_schedule},'dd-mm-yyyy') where seg_id=#{seg_id}"})
  public abstract Long updateLineItemById(SegPurchaseOrderMdl paramSegPurchaseOrderMdl);
  
  @Update({"update t_seg_purchase_order set CARRIER_ID=#{carrier_id}, CARRIER_SCHEDULE=to_date(#{carrier_schedule},'dd-mm-yyyy') where seg_id=#{seg_id}"})
  public abstract Long updateLineItemCarrierIdAndScheduleById(SegPurchaseOrderMdl paramSegPurchaseOrderMdl);
  
  @SelectKey(keyProperty="app_carr_id", before=true, resultType=Long.class, statement={"SELECT t_app_order_carr_dtl_seq.nextval AS app_carr_id FROM dual"})
  @Insert({"insert into t_approved_order_carrier_dtl(APP_CARR_ID,APP_ID,NO_OF_ITEMS_APPROVED,CARRIER_NAME,CARRIER_SCHEDULE,VC_PO_NO,CARR_ASS_COMMENT) values(#{app_carr_id},#{app_id},#{no_of_items_approved},#{carrier_name},#{caarrier_schedule},#{vc_po_no},#{carr_ass_comment})"})
  public abstract Long insertAppCarrDtl(ApproOrderCarrierDtl paramApproOrderCarrierDtl);
  
  public abstract List<ApprovedPurchaseOrder> searchApprovedPo(@Param("poNumber") String paramString1, @Param("fromDate") String paramString2, @Param("toDate") String paramString3, @Param("supplierCode") String paramString4, @Param("buyerCode") String paramString5);
  
  public abstract ApprovedPurchaseOrder getApprovedItemById(Long paramLong);
  
  @Update({"update t_approved_purchase_order set VC_TOT_PCS=#{vc_tot_pcs} where APP_ID=#{app_id}"})
  public abstract void updateApprovedPOById(ApprovedPurchaseOrder paramApprovedPurchaseOrder);
  
  @Update({"update t_approved_purchase_order set BOOKING_STATUS=#{booking_status} where APP_ID=#{app_id}"})
  public abstract void updateApprovedPoBookingStatusById(ApprovedPurchaseOrder paramApprovedPurchaseOrder);
  
  public abstract List<ApprovedPurchaseOrder> getApprovedPoNotBookedList(@Param("poNumber") String paramString1, @Param("buyer") String paramString2, @Param("fromDate") String paramString3, @Param("toDate") String paramString4);
  
  @SelectKey(keyProperty="plId", before=true, resultType=Long.class, statement={"select S_Packing_Lst_Seq.nextval AS plId from dual"})
  @Insert({"insert into T_PO_BOOK_PACKLIST_DTLS(PL_ID,HBL_NUMBER,Z_SALES_DOCUMENT,CTN_SRL_NO,NO_OF_CTNS) values(#{plId},#{hblNumber},#{zSalesDocument},#{ctnSrlNo},#{noOfCtnls}) "})
  public abstract long savePackingList(PackingListMdl paramPackingListMdl);
  
  @SelectKey(keyProperty="colorId", before=true, resultType=Long.class, statement={"select S_packing_lst_color.nextval AS colorId from dual"})
  @Insert({"insert into T_PO_BOOK_PACKLIST_COLOR(COLOR_ID,COLOR_NAME,PCS_PER_CTN,TOTAL_PCS,PL_ID,REMARKS) values(#{colorId},#{name},#{pcsPerCartoon},#{totalPcs},#{plId},#{remarks})"})
  public abstract long savePackingListColor(PackListColor paramPackListColor);
  
  @SelectKey(keyProperty="sizeId", before=true, resultType=Long.class, statement={"select s_packing_lst_size.nextval AS sizeId from dual"})
  @Insert({"insert into T_PO_BOOK_PACKLIST_SIZE(SIZE_ID,SIZE_NAME,COLOR_ID,NO_OF_PCS) values(#{sizeId},#{name},#{colorId},#{noOfPcs})"})
  public abstract void savePackingListSize(PackListSize paramPackListSize);
  
  @SelectKey(keyProperty="template_Id", before=true, resultType=Long.class, statement={"select T_TEMPLATE_SEQ.nextval AS template_Id from dual"})
  @Insert({"insert into T_TEMPLATE(TEMPLATE_ID,CUSTOMER_CODE,COMPANY,OPERATION_TYPE,SEA_OR_AIR,TEMPLATE_NAME,TEMPLATE_DATA,CREATION_DATE,CREATED_BY,IS_ACTIVE) values(#{template_Id},#{customerCode},#{company},#{operationType},#{seaOrAir},#{templateName},#{templateData},to_date(#{createdDate},'dd-mm-yyyy'),#{createdBy},#{isActive})"})
  public abstract void saveBookingInfoAsTemp(SaveAsTemplateMdl paramSaveAsTemplateMdl);
  
  @Select({"SELECT TEMPLATE_ID AS template_Id, CUSTOMER_CODE AS customerCode, COMPANY AS company, OPERATION_TYPE AS operationType, SEA_OR_AIR AS seaOrAir, TEMPLATE_NAME AS templateName, TEMPLATE_DATA AS templateData,CREATION_DATE AS createdDate, CREATED_BY AS createdBy, IS_ACTIVE AS isActive FROM T_TEMPLATE WHERE CUSTOMER_CODE = #{customerCode} AND COMPANY = #{company} AND OPERATION_TYPE=#{operationType} AND SEA_OR_AIR = #{seaOrAir}"})
  public abstract List<SaveAsTemplateMdl> getTemplateInfoByLoginCred(SaveAsTemplateMdl paramSaveAsTemplateMdl);
  
  @Select({"SELECT TEMPLATE_ID AS template_Id, CUSTOMER_CODE AS customerCode, COMPANY AS company, OPERATION_TYPE AS operationType, SEA_OR_AIR AS seaOrAir, TEMPLATE_NAME AS templateName, TEMPLATE_DATA AS templateData,CREATION_DATE AS createdDate, CREATED_BY AS createdBy, IS_ACTIVE AS isActive FROM T_TEMPLATE WHERE TEMPLATE_NAME =#{templateId} AND CUSTOMER_CODE = #{customerCode} AND COMPANY = #{company} AND OPERATION_TYPE=#{operationType} AND SEA_OR_AIR = #{seaOrAir}"})
  public abstract SaveAsTemplateMdl getTemplateInfoByTemplateId(@Param("templateId") String paramString1, @Param("customerCode") String paramString2, @Param("company") String paramString3, @Param("operationType") String paramString4, @Param("seaOrAir") String paramString5);
  
  @Update({"UPDATE T_TEMPLATE set TEMPLATE_DATA=#{tempInfo} where TEMPLATE_ID=#{temp_Id}"})
  public abstract void updateTemplateInfo(@Param("temp_Id") String paramString1, @Param("tempInfo") String paramString2);
  
  @Select({"select ID as id, MATERIAL_CODE as materialCode, material_description as materialDescription from material_master"})
  public abstract List<Materials> getMaterial();
  
  @Select({"select rowid as id, STORAGE_ID as storageId, STORAGE_DESC as storageDescription from storage_loc_master"})
  public abstract List<StorageLocation> getStorageLocation();
}