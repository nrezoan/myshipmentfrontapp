package com.myshipment.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;

import com.myshipment.model.InsPODetails;
import com.myshipment.model.InspectionBookingBean;
import com.myshipment.model.POList;

public interface inspectionMapper {
	
	final String FETCH_PO = "SELECT ORDER_NO AS po_no  FROM ORDERS WHERE ORDER_NO LIKE '%${query}%' AND FLAG ='P'";
	final String INSERT_PO_BOOKING = "INSERT INTO BOOKING(BOOKING_NO, SALES_ORG, DIST, DIVISION, SUPPLIER_CODE, BUYING_CODE, INSPECTOR_CODE, INSPECTION_DATE, PO_NUMBER, DELIVERY_DATE, ORDER_QTY, SHIPMENT_MODE, REMARKS, ENTRY_TIME, SAP_NO, INSPECTION_QTY, LOCATION, ADV_ORDER,PACK_METHOD,CUSTOMER_NAME,CARGO_DELIVERY_DATE,INSPECTION_BOOKING_STATUS) values (#{booking.bookingNo},#{booking.salesOrg},#{booking.distChannel},#{booking.division},#{booking.supplierId},#{booking.buyingHouse},#{booking.merchantDiser},to_date(#{booking.inspectionDate},'mm/dd/yyyy'),#{booking.poNumber},to_date(#{booking.deliveryDate},'mm/dd/yyyy'),#{booking.quantity},#{booking.mode},#{booking.remarks},to_date(#{booking.entryTime},'mm/dd/yyyy'),#{booking.sapNo},#{booking.inspectionQuantity},#{booking.location},#{booking.advOrder},#{booking.packMethod},#{booking.customerName},to_date(#{booking.deliveryDate},'mm/dd/yyyy'),'P')";
	final String INSERT_PO_BOOKING_NEW ="INSERT INTO BOOKING (BOOKING_NO, SALES_ORG, DIST, DIVISION, SUPPLIER_CODE, BUYING_CODE, INSPECTOR_CODE, INSPECTION_DATE, PO_NUMBER, DELIVERY_DATE, ORDER_QTY, SHIPMENT_MODE, REMARKS, ENTRY_TIME, SAP_NO, INSPECTION_QTY, LOCATION, ADV_ORDER,PACK_METHOD,CUSTOMER_NAME,CARGO_DELIVERY_DATE,SCOTA_FILE_SUBMISSION, SCOTA_FILE_SUBMISSION_DATE,INSPECTION_BOOKING_STATUS) values (#{booking.bookingNo},#{booking.salesOrg},#{booking.distChannel},#{booking.division},#{booking.supplierId},#{booking.buyingHouse},#{booking.merchantDiser},to_date(#{booking.inspectionDate},'mm/dd/yyyy'),#{booking.poNumber},to_date(#{booking.deliveryDate},'mm/dd/yyyy'),#{booking.quantity},#{booking.mode},#{booking.remarks},to_date(#{booking.entryTime},'mm/dd/yyyy'),#{booking.sapNo},#{booking.inspectionQuantity},#{booking.location},#{booking.advOrder},#{booking.packMethod},#{booking.customerName},to_date(#{booking.deliveryDate},'mm/dd/yyyy'),#{scotaFileSubmission},to_date(#{scotaFileSubmissionDate},'mm/dd/yyyy'),'P')";
	//final String INSERT_PO_BOOKING_WITHFILE = "INSERT INTO BOOKING(BOOKING_NO, SALES_ORG, DIST, DIVISION, SUPPLIER_CODE, BUYING_CODE, INSPECTOR_CODE, INSPECTION_DATE, PO_NUMBER, DELIVERY_DATE, ORDER_QTY, SHIPMENT_MODE, REMARKS, ENTRY_TIME, SAP_NO, INSPECTION_QTY, LOCATION, ADV_ORDER,PACK_METHOD,CUSTOMER_NAME,CARGO_DELIVERY_DATE,SCOTA_FILE_SUBMISSION, SCOTA_FILE_SUBMISSION_DATE,INSPECTION_BOOKING_STATUS) values (#{booking.bookingNo},#{booking.salesOrg},#{booking.distChannel},#{booking.division},#{booking.supplierId},#{booking.buyingHouse},#{booking.merchantDiser},#{booking.inspectionDate},#{booking.poNumber},#{booking.deliveryDate},#{booking.quantity},#{booking.mode},#{booking.remarks},#{booking.entryTime},#{booking.sapNo},#{booking.inspectionQuantity},#{booking.location},#{booking.advOrder},#{booking.packMethod},#{booking.customerName},#{booking.deliveryDate},#{booking.bookingStatus})";
	final String INSERT_PO_BOOKING_NEW_ELSE ="INSERT INTO BOOKING (BOOKING_NO, SALES_ORG, DIST, DIVISION, SUPPLIER_CODE, BUYING_CODE, INSPECTOR_CODE, INSPECTION_DATE, PO_NUMBER, DELIVERY_DATE, ORDER_QTY, SHIPMENT_MODE, REMARKS, ENTRY_TIME, SAP_NO, INSPECTION_QTY, LOCATION, ADV_ORDER,PACK_METHOD,CUSTOMER_NAME,CARGO_DELIVERY_DATE,SCOTA_FILE_SUBMISSION,INSPECTION_BOOKING_STATUS) values (#{booking.bookingNo},#{booking.salesOrg},#{booking.distChannel},#{booking.division},#{booking.supplierId},#{booking.buyingHouse},#{booking.merchantDiser},to_date(#{booking.inspectionDate},'mm/dd/yyyy'),#{booking.poNumber},to_date(#{booking.deliveryDate},'mm/dd/yyyy'),#{booking.quantity},#{booking.mode},#{booking.remarks},to_date(#{booking.entryTime},'mm/dd/yyyy'),#{booking.sapNo},#{booking.inspectionQuantity},#{booking.location},#{booking.advOrder},#{booking.packMethod},#{booking.customerName},to_date(#{booking.deliveryDate},'mm/dd/yyyy'),#{scotaFileSubmission},#{booking.bookingStatus})";
	final String FLAG_UPDATE = "UPDATE ORDERS set FLAG='Y' where ORDER_NO=#{poNO}";
	final String SUPPLIER_MAIL = "SELECT SUPPLIER_EMAIL from MST_SUPPLIER where SUPPLIER_CODE=#{supplier}";
	final String INSPECTOR_MAIL = "SELECT INSPECTOR_EMAIL from MST_MERCHNDISER where INSPECTOR_CODE=#{inspector}";
	final String GET_COUNT ="SELECT COUNT(*) AS rowcount FROM BOOKING WHERE PO_NUMBER=#{po}";
	
	//final String PODETAILS= "SELECT ORDER_NO as order_no, SAP_NO as sap_no, INSPECTOR_CODE as inspector_code, SHIPMENT_MODE as shipment_mode, QUANTITY as quantity,  ADV_ORDER as advOrder, ENTRY_DATE as entry_date, FLAG as flag,CUSTOMER_NAME as customerName FROM ORDERS WHERE ORDER_NO = #{orderNo} and FLAG='P'";
	
	final String PODETAILS= "SELECT ORDER_NO as order_no FROM ORDERS WHERE (ORDER_NO=#{poNumber} AND FLAG='P')";
	final String GET_ORDERS_PO_NUMBERWISE="select ORDER_NO as order_no, SAP_NO as sap_no, INSPECTOR_CODE as inspector_code, SHIPMENT_MODE as shipment_mode, QUANTITY as quantity, to_char(DELIVERY_DATE,'mm/dd/yyyy') as delivery_date, ADV_ORDER as adv_order, ENTRY_DATE as entry_date, FLAG as flag,CUSTOMER_NAME as customer_name from ORDERS WHERE order_no=#{poNumber}";
	final String GET_ORDERS_PO_NUMBERWISE_ELSE="select ORDER_NO as order_no, SAP_NO as sap_no, INSPECTOR_CODE as inspector_code, SHIPMENT_MODE as shipment_mode, QUANTITY as quantity, to_char(DELIVERY_DATE,'mm/dd/yyyy') as delivery_date, ADV_ORDER as adv_order, ENTRY_DATE as entry_date, FLAG as flag,CUSTOMER_NAME as customer_name from ORDERS WHERE (order_no=#{poNumber} AND flag='P')";
	
	final String PODETAILSWITHOUTFLAG= "SELECT ORDER_NO as order_no, SAP_NO as sap_no, INSPECTOR_CODE as inspector_code, SHIPMENT_MODE as shipment_mode, QUANTITY as quantity, to_char(DELIVERY_DATE,'mm/dd/yyyy') as delivery_date, ADV_ORDER as advOrder, ENTRY_DATE as entry_date, FLAG as flag,CUSTOMER_NAME as customerName FROM ORDERS WHERE ORDER_NO = #{orderNo}";
	final String[] SELECT_PO_BOOKING = null;
	 
	@Select(FETCH_PO)
	public List<POList> getPO(@Param("query") String query);
	
	@Insert(INSERT_PO_BOOKING)
	@SelectKey(keyProperty = "booking.bookingNo", before = true, resultType = Long.class, statement = {"SELECT BOOKINGNO.nextval AS app_id FROM dual" })
	public int insertPOBooking(@Param("booking") InspectionBookingBean booking);
	
	@Insert(INSERT_PO_BOOKING_NEW)
	@SelectKey(keyProperty = "booking.bookingNo", before = true, resultType = Long.class, statement = {"SELECT BOOKINGNO.nextval AS app_id FROM dual" })
	public void insertPOBookingNew(@Param("scotaFileSubmission") String scotaFileSubmission,@Param("scotaFileSubmissionDate") String scotaFileSubmissionDate,@Param("booking") InspectionBookingBean booking);
	
	@Insert(INSERT_PO_BOOKING_NEW_ELSE)
	@SelectKey(keyProperty = "booking.bookingNo", before = true, resultType = Long.class, statement = {"SELECT BOOKINGNO.nextval AS app_id FROM dual" })
	public void insertPOBookingNewElse(@Param("scotaFileSubmission") String scotaFileSubmission,@Param("booking") InspectionBookingBean booking);
	
	
	@Select("SELECT BOOKINGNO.nextval AS app_id FROM dual" )
	public int selectPOBooking();
	
//	@Insert(INSERT_PO_BOOKING_WITHFILE)
//	@SelectKey(keyProperty = "booking.bookingNo", before = true, resultType = Long.class, statement = {"SELECT BOOKINGNO.nextval AS app_id FROM dual" })
//	public int insertPOBookingWithFile(@Param("booking") InspectionBookingBean booking);
	
	@Update(FLAG_UPDATE)
	public void updateFlag(@Param("poNO") String poNO);
	
	@Select(SUPPLIER_MAIL)
	public String getSupplierMail(@Param("supplier") String supplier);
	
	@Select(GET_ORDERS_PO_NUMBERWISE)
	public List<InsPODetails> getOrdersPOWise(@Param("poNumber") String poNumber);
	
	@Select(GET_ORDERS_PO_NUMBERWISE_ELSE)
	public List<InsPODetails> getOrdersPOWiseElse(@Param("poNumber") String poNumber);
	
	@Select(INSPECTOR_MAIL)
	public String getInspectorMail(@Param("inspector") String inspector);
	
	@Select(GET_COUNT)
	public int getCount(@Param("po") String po);
	
	@Select(PODETAILS)
	public List<InsPODetails> getPODetails(@Param("poNumber") String poNumber);
	
	@Select(PODETAILSWITHOUTFLAG)
	public List<InsPODetails> getPODetailsWitoutFlag(@Param("orderNo") String orderNo);


}
