package com.myshipment.mappers;

import java.util.List;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;

import com.myshipment.model.CRMLogger;
import com.myshipment.model.ReportParams;

public interface UpdateLoggerMapper {
	
	public final String INSERT_UPDATE_LOGGER = "INSERT INTO T_UPDATE_LOGGER(SHIPPER,DIST_CHANNEL,DIVISION,HBL,UPDATE_TYPE,ITEM_NUMBER,FIELD_NAME,PREVIOUS_VALUE,UPDATED_VALUE,UPDATED_TIME,SALES_ORG,BUYER,ID) values(#{updateLogger.shipper},#{updateLogger.distChannel},#{updateLogger.division},#{updateLogger.hblNumber},#{updateLogger.dataType},#{updateLogger.lineItemNo},#{updateLogger.fieldName},#{updateLogger.prev},#{updateLogger.updated},#{updateLogger.updatedDate},#{updateLogger.salesOrg},#{updateLogger.buyer},#{updateLogger.id})";
	public final String SELECT_LOGGER_HBLWISE = "SELECT HBL AS hblNumber,UPDATE_TYPE AS dataType,ITEM_NUMBER AS lineItemNo,FIELD_NAME AS fieldName,PREVIOUS_VALUE AS prev,UPDATED_VALUE AS updated,UPDATED_TIME as updatedDate FROM T_UPDATE_LOGGER WHERE (HBL=#{req.hblNumber} AND DIST_CHANNEL=#{req.distChannel} AND DIVISION=#{req.division} AND SALES_ORG=#{req.salesOrg} AND SHIPPER=#{req.shipper} AND BUYER=#{req.buyer})";
	public final String SELECT_LOGGER_ALL = "SELECT HBL AS hblNumber,UPDATE_TYPE AS dataType,ITEM_NUMBER AS lineItemNo,FIELD_NAME AS fieldName,PREVIOUS_VALUE AS prev,UPDATED_VALUE AS updated,UPDATED_TIME as updatedDate FROM T_UPDATE_LOGGER WHERE (DIST_CHANNEL=#{req.distChannel} AND DIVISION=#{req.division} AND SALES_ORG=#{req.salesOrg} AND SHIPPER=#{req.shipper} AND BUYER=#{req.buyer})";

	
	@Insert(INSERT_UPDATE_LOGGER)
	@SelectKey(
            keyProperty = "updateLogger.id",
            before = true,
            resultType = Long.class,
            statement = { "SELECT T_UPDATE_LOGGER_SEQ.nextval AS id FROM dual" })
	public int insertUpdateLogger(@Param("updateLogger") CRMLogger updateLogger);

	@Select(SELECT_LOGGER_HBLWISE)
	public List<CRMLogger> hblwiseLogger(@Param("req") CRMLogger req);
	
	@Select(SELECT_LOGGER_ALL)
	public List<CRMLogger> allLogger(@Param("req") CRMLogger req);
}
