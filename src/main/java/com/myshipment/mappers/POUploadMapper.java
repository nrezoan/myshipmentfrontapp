package com.myshipment.mappers;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;

import com.myshipment.model.InsPODetails;
import com.myshipment.model.PurchaseOrdersModel;
import com.myshipment.model.TemplateModel;

public interface POUploadMapper {

	final String ACTIVE_BUYER = "SELECT ACTIVE AS active FROM BUYER_TEMPLATE WHERE  (TEMPLATE_ID=#{buyer_id})";
	final String INSERT_PO_TEMPLATE = "INSERT INTO BUYER_TEMPLATE(TEMPLATE_ID,ACTIVE,TOTAL_GW,CARTON_LENGTH,CARTON_HEIGHT,CARTON_WIDTH,TOTAL_PIECES,CARTON_QUANTITY,CARTON_UNIT,TOTAL_CBM,HS_CODE,COMMODITY,STYLE_NO,WH_CODE,SIZE_NO,SKU,ARTICLE_NO,COLOR,TOTAL_NW,COMM_INV,COMM_INV_DATE,PIECES_CTN,CARTON_SR_NO,REFERENCE_1,REFERENCE_2,REFERENCE_3,TERMS_OF_SHIPMENT,FREIGHT_MODE,DIVISION,POL,POD,PLACE_OF_DELIVERY,PLACE_OF_DELIVERY_ADDRESS,CARGO_HANDOVER_DATE,PO_NUMBER,ENTRY_DATE,ENTRY_BY,SHIPPER,FVSL,MVSL_1,MVSL_2,MVSL_3,VOYAGE_1,VOYAGE_2,VOYAGE_3,DEPARTURE_DATE,ETA,ATA,ATD,TRANSHIPMENT_1_ETD,TRANSHIPMNET_2_ETD,TRANSHIPMENT_1_ETA,TRANSHIPMENT_2_ETA) values(#{poTemplate.template_id},#{poTemplate.active},#{poTemplate.total_gw},#{poTemplate.carton_length},#{poTemplate.carton_height},#{poTemplate.carton_width},#{poTemplate.total_pieces},#{poTemplate.carton_quantity},#{poTemplate.carton_unit},#{poTemplate.total_cbm},#{poTemplate.hs_code},#{poTemplate.commodity},#{poTemplate.style_no},#{poTemplate.wh_code},#{poTemplate.size_no},#{poTemplate.sku},#{poTemplate.article_no},#{poTemplate.color},#{poTemplate.total_nw},#{poTemplate.comm_inv},#{poTemplate.comm_inv_date},#{poTemplate.pieces_ctn},#{poTemplate.carton_sr_no},#{poTemplate.reference_1},#{poTemplate.reference_2},#{poTemplate.reference_3},#{poTemplate.terms_of_shipment},#{poTemplate.freight_mode},#{poTemplate.division},#{poTemplate.pol},#{poTemplate.pod},#{poTemplate.place_of_delivery},#{poTemplate.place_of_delivery_address},#{poTemplate.cargo_handover_date},#{poTemplate.po_number},#{poTemplate.entry_date},#{poTemplate.entry_by},#{poTemplate.shipper},#{poTemplate.fvsl},#{poTemplate.mvsl1},#{poTemplate.mvsl2},#{poTemplate.mvsl3},#{poTemplate.voyage1},#{poTemplate.voyage2},#{poTemplate.voyage3},#{poTemplate.etd},#{poTemplate.eta},#{poTemplate.ata},#{poTemplate.atd},#{poTemplate.transhipment1etd},#{poTemplate.transhipment2etd},#{poTemplate.transhipment1eta},#{poTemplate.transhipment2eta})";
	final String TEMPLATE_DETAILS = "SELECT TEMPLATE_ID as template_id,ACTIVE as active,TOTAL_GW as total_gw,CARTON_LENGTH as carton_length,CARTON_HEIGHT as carton_height,CARTON_WIDTH as carton_width,TOTAL_PIECES as total_pieces,CARTON_QUANTITY as carton_quantity,CARTON_UNIT as carton_unit,TOTAL_CBM as total_cbm,HS_CODE as hs_code,COMMODITY as commodity,STYLE_NO as style_no,WH_CODE as wh_code,SIZE_NO as size_no,SKU as sku,ARTICLE_NO as article_no,COLOR as color,TOTAL_NW as total_nw,COMM_INV as comm_inv,COMM_INV_DATE as comm_inv_date,PIECES_CTN as pieces_ctn,CARTON_SR_NO as carton_sr_no,REFERENCE_1 as reference_1,REFERENCE_2 as reference_2,REFERENCE_3 as reference_3,TERMS_OF_SHIPMENT as terms_of_shipment,FREIGHT_MODE as freight_mode,DIVISION as division,POL as pol,POD as pod,PLACE_OF_DELIVERY as place_of_delivery,PLACE_OF_DELIVERY_ADDRESS as place_of_delivery_address,CARGO_HANDOVER_DATE as cargo_handover_date,PO_NUMBER as po_number, SHIPPER as shipper FROM BUYER_TEMPLATE WHERE (TEMPLATE_ID=#{buyer_id})";
	final String INSERT_PO_DETAILS = "INSERT INTO PURCHASE_ORDERS(BUYER_ID,TOTAL_GW,CARTON_LENGTH,CARTON_HEIGHT,CARTON_WIDTH,TOTAL_PIECES,CARTON_QUANTITY,CARTON_UNIT,TOTAL_CBM,HS_CODE,COMMODITY,STYLE_NO,WH_CODE,SIZE_NO,SKU,ARTICLE_NO,COLOR,TOTAL_NW,COMM_INV,COMM_INV_DATE,PIECES_CTN,CARTON_SR_NO,REFERENCE_1,REFERENCE_2,REFERENCE_3,TERMS_OF_SHIPMENT,FREIGHT_MODE,DIVISION,POL,POD,PLACE_OF_DELIVERY,PLACE_OF_DELIVERY_ADDRESS,CARGO_HANDOVER_DATE,PO_NUMBER,ENTRY_DATE,ENTRY_BY,APP_ID,FLAG,SHIPPER,FVSL,MVSL_1,MVSL_2,MVSL_3,VOYAGE_1,VOYAGE_2,VOYAGE_3,DEPARTURE_DATE,ETA,ATA,ATD,TRANSHIPMENT_1_ETD,TRANSHIPMNET_2_ETD,TRANSHIPMENT_1_ETA,TRANSHIPMENT_2_ETA) values(#{poDetails.buyer_id},#{poDetails.total_gw},#{poDetails.carton_length},#{poDetails.carton_height},#{poDetails.carton_width},#{poDetails.total_pieces},#{poDetails.carton_quantity},#{poDetails.carton_unit},#{poDetails.total_cbm},#{poDetails.hs_code},#{poDetails.commodity},#{poDetails.style_no},#{poDetails.wh_code},#{poDetails.size_no},#{poDetails.sku},#{poDetails.article_no},#{poDetails.color},#{poDetails.total_nw},#{poDetails.comm_inv},#{poDetails.comm_inv_date},#{poDetails.pieces_ctn},#{poDetails.carton_sr_no},#{poDetails.reference_1},#{poDetails.reference_2},#{poDetails.reference_3},#{poDetails.terms_of_shipment},#{poDetails.freight_mode},#{poDetails.division},#{poDetails.pol},#{poDetails.pod},#{poDetails.place_of_delivery},#{poDetails.place_of_delivery_address},#{poDetails.cargo_handover_date},#{poDetails.po_no},#{poDetails.entry_date},#{poDetails.entry_by},#{poDetails.app_id},'P',#{poDetails.shipper},#{poDeatils.fvsl},#{poDeatils.mvsl1},#{poDeatils.mvsl2},#{poDeatils.mvsl3},#{poDeatils.voyage1},#{poDeatils.voyage2},#{poDeatils.voyage3},#{poDeatils.etd},#{poDeatils.eta},#{poDeatils.ata},#{poDeatils.atd},#{poDeatils.transhipment1etd},#{poDeatils.transhipment2etd},#{poDeatils.transhipment1eta},#{poDeatils.transhipment2eta})";
	final String DELETE_PO_TEMPLATE = "DELETE FROM BUYER_TEMPLATE WHERE (TEMPLATE_ID=#{buyer_id})";
	final String DELETE_PO = "DELETE FROM PURCHASE_ORDERS WHERE (APP_ID=#{app_id})";
	final String PO_FOR_BOOKING = "SELECT APP_ID as app_id,BUYER_ID as buyer_id,TOTAL_GW as total_gw,CARTON_LENGTH as carton_length,CARTON_HEIGHT as carton_height,CARTON_WIDTH as carton_width,TOTAL_PIECES as total_pieces,CARTON_QUANTITY as carton_quantity,CARTON_UNIT as carton_unit,TOTAL_CBM as total_cbm,HS_CODE as hs_code,COMMODITY as commodity,STYLE_NO as style_no,WH_CODE as wh_code,SIZE_NO as size_no,SKU as sku,ARTICLE_NO as article_no,COLOR as color,TOTAL_NW as total_nw,COMM_INV as comm_inv,COMM_INV_DATE as comm_inv_date,PIECES_CTN as pieces_ctn,CARTON_SR_NO as carton_sr_no,REFERENCE_1 as reference_1,REFERENCE_2 as reference_2,REFERENCE_3 as reference_3,TERMS_OF_SHIPMENT as terms_of_shipment,FREIGHT_MODE as freight_mode,DIVISION as division,POL as pol,POD as pod,PLACE_OF_DELIVERY as place_of_delivery,PLACE_OF_DELIVERY_ADDRESS as place_of_delivery_address,CARGO_HANDOVER_DATE as cargo_handover_date,PO_NUMBER as po_no FROM PURCHASE_ORDERS WHERE (BUYER_ID=#{buyer_id} AND FLAG='P')";
	final String PO_FOR_BOOKING_SHIPPERWISE = "SELECT APP_ID as app_id,BUYER_ID as buyer_id,TOTAL_GW as total_gw,CARTON_LENGTH as carton_length,CARTON_HEIGHT as carton_height,CARTON_WIDTH as carton_width,TOTAL_PIECES as total_pieces,CARTON_QUANTITY as carton_quantity,CARTON_UNIT as carton_unit,TOTAL_CBM as total_cbm,HS_CODE as hs_code,COMMODITY as commodity,STYLE_NO as style_no,WH_CODE as wh_code,SIZE_NO as size_no,SKU as sku,ARTICLE_NO as article_no,COLOR as color,TOTAL_NW as total_nw,COMM_INV as comm_inv,COMM_INV_DATE as comm_inv_date,PIECES_CTN as pieces_ctn,CARTON_SR_NO as carton_sr_no,REFERENCE_1 as reference_1,REFERENCE_2 as reference_2,REFERENCE_3 as reference_3,TERMS_OF_SHIPMENT as terms_of_shipment,FREIGHT_MODE as freight_mode,DIVISION as division,POL as pol,POD as pod,PLACE_OF_DELIVERY as place_of_delivery,PLACE_OF_DELIVERY_ADDRESS as place_of_delivery_address,CARGO_HANDOVER_DATE as cargo_handover_date,PO_NUMBER as po_no,SHIPPER as shipper,FVSL as fvsl,MVSL_1 as mvsl1,MVSL_2 as mvsl2,MVSL_3 as mvsl3,VOYAGE_1 as voyage1,VOYAGE_2 asvoyage2,VOYAGE_3 as voyage3,DEPARTURE_DATE as etd,ETA as eta,ATA as ata,ATD as atd,TRANSHIPMENT_1_ETD as transhipment1etd,TRANSHIPMENT_2_ETD as transhipment2etd,TRANSHIPMENT_1_ETA as transhipment1eta,TRANSHIPMENT_2_ETA as transhipment2eta FROM PURCHASE_ORDERS WHERE (BUYER_ID=#{buyer_id} AND FLAG='P' AND SHIPPER=#{shipper})";
	final String FLAG_UPDATE = "UPDATE PURCHASE_ORDERS set FLAG='Y',BOOKING_DATE=#{booking_date} where APP_ID=#{app_id}";
	final String PO_UPDATE = "SELECT APP_ID as app_id,BUYER_ID as buyer_id,TOTAL_GW as total_gw,CARTON_LENGTH as carton_length,CARTON_HEIGHT as carton_height,CARTON_WIDTH as carton_width,TOTAL_PIECES as total_pieces,CARTON_QUANTITY as carton_quantity,CARTON_UNIT as carton_unit,TOTAL_CBM as total_cbm,HS_CODE as hs_code,COMMODITY as commodity,STYLE_NO as style_no,WH_CODE as wh_code,SIZE_NO as size_no,SKU as sku,ARTICLE_NO as article_no,COLOR as color,TOTAL_NW as total_nw,COMM_INV as comm_inv,COMM_INV_DATE as comm_inv_date,PIECES_CTN as pieces_ctn,CARTON_SR_NO as carton_sr_no,REFERENCE_1 as reference_1,REFERENCE_2 as reference_2,REFERENCE_3 as reference_3,TERMS_OF_SHIPMENT as terms_of_shipment,FREIGHT_MODE as freight_mode,DIVISION as division,POL as pol,POD as pod,PLACE_OF_DELIVERY as place_of_delivery,PLACE_OF_DELIVERY_ADDRESS as place_of_delivery_address,CARGO_HANDOVER_DATE as cargo_handover_date,PO_NUMBER as po_no,SHIPPER as shipper,FVSL as fvsl,MVSL_1 as mvsl1,MVSL_2 as mvsl2,MVSL_3 as mvsl3,VOYAGE_1 as voyage1,VOYAGE_2 asvoyage2,VOYAGE_3 as voyage3,DEPARTURE_DATE as etd,ETA as eta,ATA as ata,ATD as atd,TRANSHIPMENT_1_ETD as transhipment1etd,TRANSHIPMENT_2_ETD as transhipment2etd,TRANSHIPMENT_1_ETA as transhipment1eta,TRANSHIPMENT_2_ETA as transhipment2eta FROM PURCHASE_ORDERS WHERE (BUYER_ID=#{buyer_id} AND PO_NUMBER=#{po_no} AND FLAG='P')";
	final String PO_DETAILS_UPDATE ="UPDATE PURCHASE_ORDERS set PO_NUMBER=#{poDetails.po_no},TOTAL_GW=#{poDetails.total_gw},CARTON_LENGTH=#{poDetails.carton_length},CARTON_HEIGHT=#{poDetails.carton_height},CARTON_WIDTH=#{poDetails.carton_width},TOTAL_PIECES=#{poDetails.total_pieces},CARTON_QUANTITY=#{poDetails.carton_quantity},CARTON_UNIT=#{poDetails.carton_unit},TOTAL_CBM=#{poDetails.total_cbm},HS_CODE=#{poDetails.hs_code},COMMODITY=#{poDetails.commodity},STYLE_NO=#{poDetails.style_no},WH_CODE=#{poDetails.wh_code},SIZE_NO=#{poDetails.size_no},SKU=#{poDetails.sku},ARTICLE_NO=#{poDetails.article_no},COLOR=#{poDetails.color},TOTAL_NW=#{poDetails.total_nw},COMM_INV=#{poDetails.comm_inv},COMM_INV_DATE=#{poDetails.comm_inv_date},REFERENCE_1=#{poDetails.reference_1},REFERENCE_2=#{poDetails.reference_2},REFERENCE_3=#{poDetails.reference_3},TERMS_OF_SHIPMENT=#{poDetails.terms_of_shipment},FREIGHT_MODE=#{poDetails.freight_mode},DIVISION=#{poDetails.division},CARGO_HANDOVER_DATE=#{poDetails.cargo_handover_date},SHIPPER=#{poDetails.shipper},FVSL=#{poDeatils.fvsl},MVSL_1=#{poDeatils.mvsl1},MVSL_2=#{poDeatils.mvsl2},MVSL_3=#{poDeatils.mvsl3},VOYAGE_1=#{poDeatils.voyage1},VOYAGE_2=#{poDeatils.voyage2},VOYAGE_3=#{poDeatils.voyage3},DEPARTURE_DATE=#{poDeatils.etd},ETA=#{poDeatils.eta},ATA=#{poDeatils.ata},ATD=#{poDeatils.atd},TRANSHIPMENT_1_ETD=#{poDeatils.transhipment1etd},TRANSHIPMENT_2_ETD=#{poDeatils.transhipment2etd},TRANSHIPMENT_1_ETA=#{poDeatils.transhipment1eta},TRANSHIPMENT_2_ETA=#{poDeatils.transhipment2eta} where APP_ID=#{poDetails.app_id}";

	@Insert(INSERT_PO_TEMPLATE)
	public int insertPOTemplate(@Param("poTemplate") TemplateModel poTemplate);

	@Delete(DELETE_PO_TEMPLATE)
	public int updatePOTemplate(@Param("buyer_id") String buyer);
	
	@Delete(DELETE_PO)
	public int deletePO(@Param("app_id") String app_id);

	@Insert(INSERT_PO_DETAILS)
	@SelectKey(keyProperty = "poDetails.app_id", before = true, resultType = Long.class, statement = {"SELECT PURCHASE_ORDERS_SEQ.nextval AS app_id FROM dual" })
	public int insertPODetails(@Param("poDetails") PurchaseOrdersModel poDetails);

	@Select(ACTIVE_BUYER)
	public TemplateModel checkTemplateExists(@Param("buyer_id") String buyer);

	@Select(TEMPLATE_DETAILS)
	public TemplateModel getTemplate(@Param("buyer_id") String buyer);

	@Select(PO_FOR_BOOKING)
	public List<PurchaseOrdersModel> getPendingPO(@Param("buyer_id") String buyer);


	@Select(PO_FOR_BOOKING_SHIPPERWISE)
	public List<PurchaseOrdersModel> getPendingPOShipperwise(@Param("buyer_id") String buyer,
			@Param("shipper") String shipper);

	@Update(FLAG_UPDATE)
	public void updateFlag(@Param("app_id") Long app_id, @Param("booking_date") Date booking_date);

	@Select(PO_UPDATE)
	public List<PurchaseOrdersModel> getPOforUpdate(@Param("buyer_id") String buyer, @Param("po_no") String po_no);

	@Update(PO_DETAILS_UPDATE)
	public void updatePODetails(@Param("poDetails") PurchaseOrdersModel poDetails);
}
