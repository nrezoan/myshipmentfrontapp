package com.myshipment.mappers;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectKey;
import org.apache.ibatis.annotations.Update;

import com.myshipment.model.SegPurchaseOrder;
/*
 * @ Ranjeet Kumar
 */
public interface SegregationMapper {

	public final String insert = "INSERT INTO T_SEG_PURCHASE_ORDER (SEG_ID,PO_ID,VC_PO_NO,NU_CLIENT_CODE,VC_PRODUCT_NO,VC_SKU_NO,VC_STYLE_NO,VC_ARTICLE_NO,VC_COLOR,VC_SIZE,VC_POL,VC_POD,NU_NO_PCS_CTNS,VC_COMMODITY,DT_ETD,VC_TOT_PCS,VC_QUAN,VC_QUA_UOM,NU_LENGTH,NU_WIDTH,NU_HIEGHT,VC_IN_HCM,VC_CBM_SEA,VC_VOLUME,VC_GW_CAR,VC_GR_WT,VC_NW_CAR,VC_NT_WT,VC_REF_FIELD1,VC_REF_FIELD2,VC_REF_FIELD3,VC_REF_FIELD4,VC_REF_FIELD5,VC_HS_CODE,VC_QC_DT,VC_REL_DT,SALES_ORG,SAP_QUOTATION,FILE_UPLOAD_DATE,VC_BUYER,VC_BUY_HOUSE,FILE_NAME,VC_DIVISION,PO_CREATION_DATE,IS_ACTIVE,SEGREGATTED_AGAINST ) VALUES (#{seg_id},#{po_id},#{vc_po_no},#{nu_client_code},#{vc_product_no},#{vc_sku_no},#{vc_style_no},#{vc_article_no},#{vc_color},#{vc_size},#{vc_pol},#{vc_pod},#{nu_no_pcs_ctns},#{vc_commodity},#{dt_etd},#{vc_tot_pcs},#{vc_quan},#{vc_qua_uom},#{nu_length},#{nu_width},#{nu_height},#{vc_in_hcm},#{vc_cbm_sea},#{vc_volume},#{vc_gw_car},#{vc_gr_wt},#{vc_nw_car},#{vc_nt_wt},#{vc_ref_field1},#{vc_ref_field2},#{vc_ref_field3},#{vc_ref_field4},#{vc_ref_field5},#{vc_hs_code},#{vc_qc_dt},#{vc_rel_dt},#{sales_org},#{sap_quotation},#{file_upload_date},#{vc_buyer},#{vc_buy_house},#{file_name},#{vc_division},#{currentDate},#{isRecordActive},#{segregattedAgainst})";
	
	@Select("SELECT SEG_ID AS seg_id, PO_ID AS po_id, VC_PO_NO AS vc_po_no, VC_PRODUCT_NO AS vc_product_no, VC_SKU_NO AS vc_sku_no, VC_STYLE_NO AS vc_style_no, VC_ARTICLE_NO AS vc_article_no,"
			+ "VC_COLOR AS vc_color, VC_SIZE AS vc_size, VC_POL AS vc_pol, VC_POD AS vc_pod, NU_NO_PCS_CTNS AS nu_no_pcs_ctns, VC_COMMODITY AS vc_commodity, DT_ETD AS dt_etd, VC_TOT_PCS AS vc_tot_pcs,"
			+ "VC_QUAN AS vc_quan, VC_QUA_UOM AS vc_qua_uom, NU_LENGTH AS nu_length, NU_WIDTH AS nu_width, NU_HIEGHT AS nu_height, VC_IN_HCM AS vc_in_hcm, VC_CBM_SEA AS vc_cbm_sea, VC_VOLUME AS vc_volume,"
			+ "VC_GW_CAR AS vc_gw_car,VC_GR_WT AS vc_gr_wt, VC_NW_CAR AS vc_nw_car, VC_NT_WT AS vc_nt_wt, VC_REF_FIELD1 AS vc_ref_field1, VC_REF_FIELD2 AS vc_ref_field2, VC_REF_FIELD3 AS vc_ref_field3,"
			+ "VC_REF_FIELD4 AS vc_ref_field4, VC_REF_FIELD5 AS vc_ref_field5, VC_HS_CODE AS vc_hs_code, VC_QC_DT AS vc_qc_dt, VC_REL_DT AS vc_rel_dt, SAP_QUOTATION AS sap_quotation, FILE_UPLOAD_DATE AS file_upload_date,"
			+ "FILE_NAME AS file_name, VC_DIVISION AS vc_division, TO_CHAR(PO_CREATION_DATE, 'DD-MM-YYYY') AS formattedPoCreationDate FROM T_SEG_PURCHASE_ORDER WHERE SEG_ID=#{segId}")
	public SegPurchaseOrder getSegregatePurchaseOrderBasedOnSegId(@Param("segId") int segId);
	
	@Insert(insert)
	@SelectKey(
            keyProperty = "seg_id",
            before = true,
            resultType = Integer.class,
            statement = { "SELECT T_SEG_PURCHASE_ORDER_SEQ.nextval AS upload_id FROM dual" })
	public int insertData(SegPurchaseOrder segPurchaseOrder);
	
	@Update("UPDATE t_seg_purchase_order"
			+ " SET IS_ACTIVE = 'NO' "
			+ "WHERE SEG_ID = #{seg_id}")
	public int updateData(@Param("seg_id") Long seg_id);
	
}
