package com.myshipment.dto;

import java.util.HashMap;
import java.util.Map;

import com.myshipment.model.PackingItem;

public class OrderHeaderDTO {
	private static final Long serialVersionUID = -5364574756756756l;
	private String appId;
	private String docNumber;
	private String docType;
	private String salesOrg;
	private String tos;
	private String tosDes;
	private String freightMode;
	private String buyer;
	private String buyersBank;
	private String localBuyingHouse;

	private String hblInit;
	private String shipperRefNo;
	private String shipperRefDate;
	private String comInvNo;
	// private Date comInvDate;
	private String comInvDate;
	private String expNo;
	// private Date expDate;
	private String expDate;
	private String lcTtPono;
	// private Date lcTtPoDate;
	private String lcTtPoDate;
	// private Date lcExpiryDate;
	private String lcExpiryDate;
	private String vendorRef;
	private String bankCode;
	private String accNumber;
	private String intermediateBank;
	private String swiftTransfer;
	private String hblNumber;
	private String transType;
	private Integer totalNetWeight;
	private Integer totalVolume;
	private Integer totalPcs;
	private Integer grossWeight;
	private String cargoStatus;

	private String portLink;
	// private Date cargoHandoverDate;
	private String cargoHandoverDate;
	private String portLoad;
	// private Date loadingDate;
	private String loadingDate;
	private String portOfDischarge;
	private String placeOfDelivery;
	private String placeOfDeliveryText;
	// private Date deptDate;
	private String deptDate;
	private String paymentTerm;
	// private Date shippingDate;
	private String shippingDate;
	private String shippingMark;
	private String sideMak;
	private String remarks1;
	private String remarks2;
	private String description;
	private Integer soQuantity;

	private String shippersBank;
	private String shippersBankName;
	private String shippersBankAddress1;
	private String shippersBankAddress2;
	private String shippersBankAddress3;
	private String shippersBankCity;
	private String shippersBankCountry;

	private String shipper;
	private String shipperName;
	private String shipperAddress1;
	private String shipperAddress2;
	private String shipperAddress3;
	private String shipperCity;
	private String shipperCountry;

	private String buyerName;
	private String buyerAddress1;
	private String buyerAddress2;
	private String buyerAddress3;
	private String buyerCity;
	private String buyerCountry;

	private String notifyParty;
	private String notifyPartyName;
	private String notifyPartyAddress1;
	private String notifyPartyAddress2;
	private String notifyPartyAddress3;
	private String notifyPartyCity;
	private String notifyPartyCountry;

	private String division;
	private String channel;

	private String localBuyingHouseText;
	private String buyerText;
	private String shippersBankText;
	private String notifyPartyText;
	private String portLinkText;
	private String portLoadText;
	private String portOfDischargeText;
	private String placeOfDeliveryText1;
	private Map<Integer, PackingItem> packingList = new HashMap<Integer, PackingItem>();
	private String docParserJson;
	
	

	public String getDocParserJson() {
		return docParserJson;
	}

	public void setDocParserJson(String docParserJson) {
		this.docParserJson = docParserJson;
	}

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getDocNumber() {
		return docNumber;
	}

	public void setDocNumber(String docNumber) {
		this.docNumber = docNumber;
	}

	public String getDocType() {
		return docType;
	}

	public void setDocType(String docType) {
		this.docType = docType;
	}

	public String getSalesOrg() {
		return salesOrg;
	}

	public void setSalesOrg(String salesOrg) {
		this.salesOrg = salesOrg;
	}

	public String getTos() {
		return tos;
	}

	public void setTos(String tos) {
		this.tos = tos;
	}

	public String getTosDes() {
		return tosDes;
	}

	public void setTosDes(String tosDes) {
		this.tosDes = tosDes;
	}

	public String getFreightMode() {
		return freightMode;
	}

	public void setFreightMode(String freightMode) {
		this.freightMode = freightMode;
	}

	public String getBuyer() {
		return buyer;
	}

	public void setBuyer(String buyer) {
		this.buyer = buyer;
	}

	public String getBuyersBank() {
		return buyersBank;
	}

	public void setBuyersBank(String buyersBank) {
		this.buyersBank = buyersBank;
	}

	public String getLocalBuyingHouse() {
		return localBuyingHouse;
	}

	public void setLocalBuyingHouse(String localBuyingHouse) {
		this.localBuyingHouse = localBuyingHouse;
	}

	public String getHblInit() {
		return hblInit;
	}

	public void setHblInit(String hblInit) {
		this.hblInit = hblInit;
	}

	public String getShipperRefNo() {
		return shipperRefNo;
	}

	public void setShipperRefNo(String shipperRefNo) {
		this.shipperRefNo = shipperRefNo;
	}

	public String getShipperRefDate() {
		return shipperRefDate;
	}

	public void setShipperRefDate(String shipperRefDate) {
		this.shipperRefDate = shipperRefDate;
	}

	public String getComInvNo() {
		return comInvNo;
	}

	public void setComInvNo(String comInvNo) {
		this.comInvNo = comInvNo;
	}

	public String getComInvDate() {
		return comInvDate;
	}

	public void setComInvDate(String comInvDate) {
		this.comInvDate = comInvDate;
	}

	public String getExpNo() {
		return expNo;
	}

	public void setExpNo(String expNo) {
		this.expNo = expNo;
	}

	public String getExpDate() {
		return expDate;
	}

	public void setExpDate(String expDate) {
		this.expDate = expDate;
	}

	public String getLcTtPono() {
		return lcTtPono;
	}

	public void setLcTtPono(String lcTtPono) {
		this.lcTtPono = lcTtPono;
	}

	public String getLcTtPoDate() {
		return lcTtPoDate;
	}

	public void setLcTtPoDate(String lcTtPoDate) {
		this.lcTtPoDate = lcTtPoDate;
	}

	public String getLcExpiryDate() {
		return lcExpiryDate;
	}

	public void setLcExpiryDate(String lcExpiryDate) {
		this.lcExpiryDate = lcExpiryDate;
	}

	public String getVendorRef() {
		return vendorRef;
	}

	public void setVendorRef(String vendorRef) {
		this.vendorRef = vendorRef;
	}

	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public String getAccNumber() {
		return accNumber;
	}

	public void setAccNumber(String accNumber) {
		this.accNumber = accNumber;
	}

	public String getIntermediateBank() {
		return intermediateBank;
	}

	public void setIntermediateBank(String intermediateBank) {
		this.intermediateBank = intermediateBank;
	}

	public String getSwiftTransfer() {
		return swiftTransfer;
	}

	public void setSwiftTransfer(String swiftTransfer) {
		this.swiftTransfer = swiftTransfer;
	}

	public String getHblNumber() {
		return hblNumber;
	}

	public void setHblNumber(String hblNumber) {
		this.hblNumber = hblNumber;
	}

	public String getTransType() {
		return transType;
	}

	public void setTransType(String transType) {
		this.transType = transType;
	}

	public Integer getTotalNetWeight() {
		return totalNetWeight;
	}

	public void setTotalNetWeight(Integer totalNetWeight) {
		this.totalNetWeight = totalNetWeight;
	}

	public Integer getTotalVolume() {
		return totalVolume;
	}

	public void setTotalVolume(Integer totalVolume) {
		this.totalVolume = totalVolume;
	}

	public Integer getTotalPcs() {
		return totalPcs;
	}

	public void setTotalPcs(Integer totalPcs) {
		this.totalPcs = totalPcs;
	}

	public Integer getGrossWeight() {
		return grossWeight;
	}

	public void setGrossWeight(Integer grossWeight) {
		this.grossWeight = grossWeight;
	}

	public String getCargoStatus() {
		return cargoStatus;
	}

	public void setCargoStatus(String cargoStatus) {
		this.cargoStatus = cargoStatus;
	}

	public String getPortLink() {
		return portLink;
	}

	public void setPortLink(String portLink) {
		this.portLink = portLink;
	}

	public String getCargoHandoverDate() {
		return cargoHandoverDate;
	}

	public void setCargoHandoverDate(String cargoHandoverDate) {
		this.cargoHandoverDate = cargoHandoverDate;
	}

	public String getPortLoad() {
		return portLoad;
	}

	public void setPortLoad(String portLoad) {
		this.portLoad = portLoad;
	}

	public String getLoadingDate() {
		return loadingDate;
	}

	public void setLoadingDate(String loadingDate) {
		this.loadingDate = loadingDate;
	}

	public String getPortOfDischarge() {
		return portOfDischarge;
	}

	public void setPortOfDischarge(String portOfDischarge) {
		this.portOfDischarge = portOfDischarge;
	}

	public String getPlaceOfDelivery() {
		return placeOfDelivery;
	}

	public void setPlaceOfDelivery(String placeOfDelivery) {
		this.placeOfDelivery = placeOfDelivery;
	}

	public String getPlaceOfDeliveryText() {
		return placeOfDeliveryText;
	}

	public void setPlaceOfDeliveryText(String placeOfDeliveryText) {
		this.placeOfDeliveryText = placeOfDeliveryText;
	}

	public String getDeptDate() {
		return deptDate;
	}

	public void setDeptDate(String deptDate) {
		this.deptDate = deptDate;
	}

	public String getPaymentTerm() {
		return paymentTerm;
	}

	public void setPaymentTerm(String paymentTerm) {
		this.paymentTerm = paymentTerm;
	}

	public String getShippingDate() {
		return shippingDate;
	}

	public void setShippingDate(String shippingDate) {
		this.shippingDate = shippingDate;
	}

	public String getShippingMark() {
		return shippingMark;
	}

	public void setShippingMark(String shippingMark) {
		this.shippingMark = shippingMark;
	}

	public String getSideMak() {
		return sideMak;
	}

	public void setSideMak(String sideMak) {
		this.sideMak = sideMak;
	}

	public String getRemarks1() {
		return remarks1;
	}

	public void setRemarks1(String remarks1) {
		this.remarks1 = remarks1;
	}

	public String getRemarks2() {
		return remarks2;
	}

	public void setRemarks2(String remarks2) {
		this.remarks2 = remarks2;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getSoQuantity() {
		return soQuantity;
	}

	public void setSoQuantity(Integer soQuantity) {
		this.soQuantity = soQuantity;
	}

	public String getShippersBank() {
		return shippersBank;
	}

	public void setShippersBank(String shippersBank) {
		this.shippersBank = shippersBank;
	}

	public String getShippersBankName() {
		return shippersBankName;
	}

	public void setShippersBankName(String shippersBankName) {
		this.shippersBankName = shippersBankName;
	}

	public String getShippersBankAddress1() {
		return shippersBankAddress1;
	}

	public void setShippersBankAddress1(String shippersBankAddress1) {
		this.shippersBankAddress1 = shippersBankAddress1;
	}

	public String getShippersBankAddress2() {
		return shippersBankAddress2;
	}

	public void setShippersBankAddress2(String shippersBankAddress2) {
		this.shippersBankAddress2 = shippersBankAddress2;
	}

	public String getShippersBankAddress3() {
		return shippersBankAddress3;
	}

	public void setShippersBankAddress3(String shippersBankAddress3) {
		this.shippersBankAddress3 = shippersBankAddress3;
	}

	public String getShippersBankCity() {
		return shippersBankCity;
	}

	public void setShippersBankCity(String shippersBankCity) {
		this.shippersBankCity = shippersBankCity;
	}

	public String getShippersBankCountry() {
		return shippersBankCountry;
	}

	public void setShippersBankCountry(String shippersBankCountry) {
		this.shippersBankCountry = shippersBankCountry;
	}

	public String getShipper() {
		return shipper;
	}

	public void setShipper(String shipper) {
		this.shipper = shipper;
	}

	public String getShipperName() {
		return shipperName;
	}

	public void setShipperName(String shipperName) {
		this.shipperName = shipperName;
	}

	public String getShipperAddress1() {
		return shipperAddress1;
	}

	public void setShipperAddress1(String shipperAddress1) {
		this.shipperAddress1 = shipperAddress1;
	}

	public String getShipperAddress2() {
		return shipperAddress2;
	}

	public void setShipperAddress2(String shipperAddress2) {
		this.shipperAddress2 = shipperAddress2;
	}

	public String getShipperAddress3() {
		return shipperAddress3;
	}

	public void setShipperAddress3(String shipperAddress3) {
		this.shipperAddress3 = shipperAddress3;
	}

	public String getShipperCity() {
		return shipperCity;
	}

	public void setShipperCity(String shipperCity) {
		this.shipperCity = shipperCity;
	}

	public String getShipperCountry() {
		return shipperCountry;
	}

	public void setShipperCountry(String shipperCountry) {
		this.shipperCountry = shipperCountry;
	}

	public String getBuyerName() {
		return buyerName;
	}

	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}

	public String getBuyerAddress1() {
		return buyerAddress1;
	}

	public void setBuyerAddress1(String buyerAddress1) {
		this.buyerAddress1 = buyerAddress1;
	}

	public String getBuyerAddress2() {
		return buyerAddress2;
	}

	public void setBuyerAddress2(String buyerAddress2) {
		this.buyerAddress2 = buyerAddress2;
	}

	public String getBuyerAddress3() {
		return buyerAddress3;
	}

	public void setBuyerAddress3(String buyerAddress3) {
		this.buyerAddress3 = buyerAddress3;
	}

	public String getBuyerCity() {
		return buyerCity;
	}

	public void setBuyerCity(String buyerCity) {
		this.buyerCity = buyerCity;
	}

	public String getBuyerCountry() {
		return buyerCountry;
	}

	public void setBuyerCountry(String buyerCountry) {
		this.buyerCountry = buyerCountry;
	}

	public String getNotifyParty() {
		return notifyParty;
	}

	public void setNotifyParty(String notifyParty) {
		this.notifyParty = notifyParty;
	}

	public String getNotifyPartyName() {
		return notifyPartyName;
	}

	public void setNotifyPartyName(String notifyPartyName) {
		this.notifyPartyName = notifyPartyName;
	}

	public String getNotifyPartyAddress1() {
		return notifyPartyAddress1;
	}

	public void setNotifyPartyAddress1(String notifyPartyAddress1) {
		this.notifyPartyAddress1 = notifyPartyAddress1;
	}

	public String getNotifyPartyAddress2() {
		return notifyPartyAddress2;
	}

	public void setNotifyPartyAddress2(String notifyPartyAddress2) {
		this.notifyPartyAddress2 = notifyPartyAddress2;
	}

	public String getNotifyPartyAddress3() {
		return notifyPartyAddress3;
	}

	public void setNotifyPartyAddress3(String notifyPartyAddress3) {
		this.notifyPartyAddress3 = notifyPartyAddress3;
	}

	public String getNotifyPartyCity() {
		return notifyPartyCity;
	}

	public void setNotifyPartyCity(String notifyPartyCity) {
		this.notifyPartyCity = notifyPartyCity;
	}

	public String getNotifyPartyCountry() {
		return notifyPartyCountry;
	}

	public void setNotifyPartyCountry(String notifyPartyCountry) {
		this.notifyPartyCountry = notifyPartyCountry;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	public String getLocalBuyingHouseText() {
		return localBuyingHouseText;
	}

	public void setLocalBuyingHouseText(String localBuyingHouseText) {
		this.localBuyingHouseText = localBuyingHouseText;
	}

	public String getBuyerText() {
		return buyerText;
	}

	public void setBuyerText(String buyerText) {
		this.buyerText = buyerText;
	}

	public String getShippersBankText() {
		return shippersBankText;
	}

	public void setShippersBankText(String shippersBankText) {
		this.shippersBankText = shippersBankText;
	}

	public String getNotifyPartyText() {
		return notifyPartyText;
	}

	public void setNotifyPartyText(String notifyPartyText) {
		this.notifyPartyText = notifyPartyText;
	}

	public String getPortLinkText() {
		return portLinkText;
	}

	public void setPortLinkText(String portLinkText) {
		this.portLinkText = portLinkText;
	}

	public String getPortLoadText() {
		return portLoadText;
	}

	public void setPortLoadText(String portLoadText) {
		this.portLoadText = portLoadText;
	}

	public String getPortOfDischargeText() {
		return portOfDischargeText;
	}

	public void setPortOfDischargeText(String portOfDischargeText) {
		this.portOfDischargeText = portOfDischargeText;
	}

	public String getPlaceOfDeliveryText1() {
		return placeOfDeliveryText1;
	}

	public void setPlaceOfDeliveryText1(String placeOfDeliveryText1) {
		this.placeOfDeliveryText1 = placeOfDeliveryText1;
	}

	public Map<Integer, PackingItem> getPackingList() {
		return packingList;
	}

	public void setPackingList(Map<Integer, PackingItem> packingList) {
		this.packingList = packingList;
	}

	@Override
	public String toString() {
		return "OrderHeaderDTO [appId=" + appId + ", docNumber=" + docNumber + ", docType=" + docType + ", salesOrg="
				+ salesOrg + ", tos=" + tos + ", tosDes=" + tosDes + ", freightMode=" + freightMode + ", buyer=" + buyer
				+ ", buyersBank=" + buyersBank + ", localBuyingHouse=" + localBuyingHouse + ", hblInit=" + hblInit
				+ ", shipperRefNo=" + shipperRefNo + ", shipperRefDate=" + shipperRefDate + ", comInvNo=" + comInvNo
				+ ", comInvDate=" + comInvDate + ", expNo=" + expNo + ", expDate=" + expDate + ", lcTtPono=" + lcTtPono
				+ ", lcTtPoDate=" + lcTtPoDate + ", lcExpiryDate=" + lcExpiryDate + ", vendorRef=" + vendorRef
				+ ", bankCode=" + bankCode + ", accNumber=" + accNumber + ", intermediateBank=" + intermediateBank
				+ ", swiftTransfer=" + swiftTransfer + ", hblNumber=" + hblNumber + ", transType=" + transType
				+ ", totalNetWeight=" + totalNetWeight + ", totalVolume=" + totalVolume + ", totalPcs=" + totalPcs
				+ ", grossWeight=" + grossWeight + ", cargoStatus=" + cargoStatus + ", portLink=" + portLink
				+ ", cargoHandoverDate=" + cargoHandoverDate + ", portLoad=" + portLoad + ", loadingDate=" + loadingDate
				+ ", portOfDischarge=" + portOfDischarge + ", placeOfDelivery=" + placeOfDelivery
				+ ", placeOfDeliveryText=" + placeOfDeliveryText + ", deptDate=" + deptDate + ", paymentTerm="
				+ paymentTerm + ", shippingDate=" + shippingDate + ", shippingMark=" + shippingMark + ", sideMak="
				+ sideMak + ", remarks1=" + remarks1 + ", remarks2=" + remarks2 + ", description=" + description
				+ ", soQuantity=" + soQuantity + ", shippersBank=" + shippersBank + ", shippersBankName="
				+ shippersBankName + ", shippersBankAddress1=" + shippersBankAddress1 + ", shippersBankAddress2="
				+ shippersBankAddress2 + ", shippersBankAddress3=" + shippersBankAddress3 + ", shippersBankCity="
				+ shippersBankCity + ", shippersBankCountry=" + shippersBankCountry + ", shipper=" + shipper
				+ ", shipperName=" + shipperName + ", shipperAddress1=" + shipperAddress1 + ", shipperAddress2="
				+ shipperAddress2 + ", shipperAddress3=" + shipperAddress3 + ", shipperCity=" + shipperCity
				+ ", shipperCountry=" + shipperCountry + ", buyerName=" + buyerName + ", buyerAddress1=" + buyerAddress1
				+ ", buyerAddress2=" + buyerAddress2 + ", buyerAddress3=" + buyerAddress3 + ", buyerCity=" + buyerCity
				+ ", buyerCountry=" + buyerCountry + ", notifyParty=" + notifyParty + ", notifyPartyName="
				+ notifyPartyName + ", notifyPartyAddress1=" + notifyPartyAddress1 + ", notifyPartyAddress2="
				+ notifyPartyAddress2 + ", notifyPartyAddress3=" + notifyPartyAddress3 + ", notifyPartyCity="
				+ notifyPartyCity + ", notifyPartyCountry=" + notifyPartyCountry + ", division=" + division
				+ ", channel=" + channel + ", localBuyingHouseText=" + localBuyingHouseText + ", buyerText=" + buyerText
				+ ", shippersBankText=" + shippersBankText + ", notifyPartyText=" + notifyPartyText + ", portLinkText="
				+ portLinkText + ", portLoadText=" + portLoadText + ", portOfDischargeText=" + portOfDischargeText
				+ ", placeOfDeliveryText1=" + placeOfDeliveryText1 + ", packingList=" + packingList + 
				", docParserJson=" + docParserJson +"]";
	}

}
