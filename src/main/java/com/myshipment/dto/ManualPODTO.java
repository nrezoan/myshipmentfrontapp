package com.myshipment.dto;

import java.util.List;

import com.myshipment.model.SegPurchaseOrder;

/*
 * @ Ranjeet Kumar
 */
public class ManualPODTO {

	private String vc_po_no = ""; 
	private String nu_client_code = "";
	private String vc_product_no = "";
	private String vc_sku_no = "";
	private String vc_style_no = "";
	private String vc_article_no = "";
	private String vc_color = "";
	private String vc_size = "";
	private String vc_pol = "";
	private String vc_pod = "";
	private double nu_no_pcs_ctns;
	private String vc_commodity = "";		
	private double vc_tot_pcs;
	private double vc_quan;
	private String vc_in_hcm = "";	
	private String vc_hs_code = "";
	private String sales_org = "";
	private String sap_quotation = "";
	private String vc_buyer = "";
	private String vc_buy_house = "";
	private String vc_division = "";
	private String company_code = "";
	private String supplier_code = "";
	
	private List<SegPurchaseOrder> dynamicData;
	
	public String getVc_po_no() {
		return vc_po_no;
	}
	public void setVc_po_no(String vc_po_no) {
		this.vc_po_no = vc_po_no;
	}
	public String getNu_client_code() {
		return nu_client_code;
	}
	public void setNu_client_code(String nu_client_code) {
		this.nu_client_code = nu_client_code;
	}
	public String getVc_product_no() {
		return vc_product_no;
	}
	public void setVc_product_no(String vc_product_no) {
		this.vc_product_no = vc_product_no;
	}
	public String getVc_sku_no() {
		return vc_sku_no;
	}
	public void setVc_sku_no(String vc_sku_no) {
		this.vc_sku_no = vc_sku_no;
	}
	public String getVc_style_no() {
		return vc_style_no;
	}
	public void setVc_style_no(String vc_style_no) {
		this.vc_style_no = vc_style_no;
	}
	public String getVc_article_no() {
		return vc_article_no;
	}
	public void setVc_article_no(String vc_article_no) {
		this.vc_article_no = vc_article_no;
	}
	public String getVc_color() {
		return vc_color;
	}
	public void setVc_color(String vc_color) {
		this.vc_color = vc_color;
	}
	public String getVc_size() {
		return vc_size;
	}
	public void setVc_size(String vc_size) {
		this.vc_size = vc_size;
	}
	public String getVc_pol() {
		return vc_pol;
	}
	public void setVc_pol(String vc_pol) {
		this.vc_pol = vc_pol;
	}
	public String getVc_pod() {
		return vc_pod;
	}
	public void setVc_pod(String vc_pod) {
		this.vc_pod = vc_pod;
	}
	public double getNu_no_pcs_ctns() {
		return nu_no_pcs_ctns;
	}
	public void setNu_no_pcs_ctns(double nu_no_pcs_ctns) {
		this.nu_no_pcs_ctns = nu_no_pcs_ctns;
	}
	public String getVc_commodity() {
		return vc_commodity;
	}
	public void setVc_commodity(String vc_commodity) {
		this.vc_commodity = vc_commodity;
	}
	public double getVc_tot_pcs() {
		return vc_tot_pcs;
	}
	public void setVc_tot_pcs(double vc_tot_pcs) {
		this.vc_tot_pcs = vc_tot_pcs;
	}
	public double getVc_quan() {
		return vc_quan;
	}
	public void setVc_quan(double vc_quan) {
		this.vc_quan = vc_quan;
	}
	public String getVc_in_hcm() {
		return vc_in_hcm;
	}
	public void setVc_in_hcm(String vc_in_hcm) {
		this.vc_in_hcm = vc_in_hcm;
	}
	public String getVc_hs_code() {
		return vc_hs_code;
	}
	public void setVc_hs_code(String vc_hs_code) {
		this.vc_hs_code = vc_hs_code;
	}
	public String getSales_org() {
		return sales_org;
	}
	public void setSales_org(String sales_org) {
		this.sales_org = sales_org;
	}
	public String getSap_quotation() {
		return sap_quotation;
	}
	public void setSap_quotation(String sap_quotation) {
		this.sap_quotation = sap_quotation;
	}
	public String getVc_buyer() {
		return vc_buyer;
	}
	public void setVc_buyer(String vc_buyer) {
		this.vc_buyer = vc_buyer;
	}
	public String getVc_buy_house() {
		return vc_buy_house;
	}
	public void setVc_buy_house(String vc_buy_house) {
		this.vc_buy_house = vc_buy_house;
	}
	public String getVc_division() {
		return vc_division;
	}
	public void setVc_division(String vc_division) {
		this.vc_division = vc_division;
	}
	public String getCompany_code() {
		return company_code;
	}
	public void setCompany_code(String company_code) {
		this.company_code = company_code;
	}
	public String getSupplier_code() {
		return supplier_code;
	}
	public void setSupplier_code(String supplier_code) {
		this.supplier_code = supplier_code;
	}
	public List<SegPurchaseOrder> getDynamicData() {
		return dynamicData;
	}
	public void setDynamicData(List<SegPurchaseOrder> dynamicData) {
		this.dynamicData = dynamicData;
	}
	
	
}

