package com.myshipment.dto;

import java.util.List;
import java.util.Map;

import com.myshipment.model.BapiReturn1;
import com.myshipment.model.BapiReturn2;
import com.myshipment.model.CompcodeListBean;
import com.myshipment.model.FrequentUsedMenu;
import com.myshipment.model.PeAddressBean;
import com.myshipment.model.SalesorgListBean;

/**
 * @author Virendra Kumar
 *
 */



public class LoginDTO {

	private static final long serialVersionUID = -171015544166201L;
	private BapiReturn1 bapiReturn1;
	private String partner;
	private String accgroup;
	private PeAddressBean peAddress;
	private String salesOrgSelected;
	private String divisionSelected;
	private String disChnlSelected;
	private BapiReturn2 bapiReturn2;
	private Map<String,SalesOrgTree> mpSalesOrgTree;
	private List<SalesorgListBean> salesorgList; 
	private List<CompcodeListBean> companycodeList; 
	private String loggedInUserName;
	private String loggedInUserPassword;
	private String IsUserLoggedIn;
	private String crmDashboardBuyerId;
	private String dashBoardFromDate;
	private String dashBoardToDate;
	private List<FrequentUsedMenu> frequentUsedMenuList;
	private Map<String,SalesOrgTree> salesOrgDivisionWise;
	private SupplierDetailsDTO supplierDetailsDTO;
	
	//sanjana
	private Integer pendingPO;
	
	
	public List<FrequentUsedMenu> getFrequentUsedMenuList() {
		return frequentUsedMenuList;
	}

	public void setFrequentUsedMenuList(List<FrequentUsedMenu> frequentUsedMenuList) {
		this.frequentUsedMenuList = frequentUsedMenuList;
	}

	public String getSalesOrgSelected() {
		return salesOrgSelected;
	}

	public void setSalesOrgSelected(String salesOrgSelected) {
		this.salesOrgSelected = salesOrgSelected;
	}

	public String getDivisionSelected() {
		return divisionSelected;
	}

	public void setDivisionSelected(String divisionSelected) {
		this.divisionSelected = divisionSelected;
	}

	public String getDisChnlSelected() {
		return disChnlSelected;
	}

	public void setDisChnlSelected(String disChnlSelected) {
		this.disChnlSelected = disChnlSelected;
	}

	
	public PeAddressBean getPeAddress() {
		return peAddress;
	}

	public void setPeAddress(PeAddressBean peAddress) {
		this.peAddress = peAddress;
	}

	public BapiReturn2 getBapiReturn2() {
		return bapiReturn2;
	}

	public void setBapiReturn2(BapiReturn2 bapiReturn2) {
		this.bapiReturn2 = bapiReturn2;
	}

	public Map<String, SalesOrgTree> getMpSalesOrgTree() {
		return mpSalesOrgTree;
	}

	public void setMpSalesOrgTree(Map<String, SalesOrgTree> mpSalesOrgTree) {
		this.mpSalesOrgTree = mpSalesOrgTree;
	}

	public List<SalesorgListBean> getSalesorgList() {
		return salesorgList;
	}

	public void setSalesorgList(List<SalesorgListBean> salesorgList) {
		this.salesorgList = salesorgList;
	}

	public List<CompcodeListBean> getCompanycodeList() {
		return companycodeList;
	}

	public void setCompanycodeList(List<CompcodeListBean> companycodeList) {
		this.companycodeList = companycodeList;
	}

	
	public String getAccgroup() {
		return accgroup;
	}

	public void setAccgroup(String accgroup) {
		this.accgroup = accgroup;
	}

	
	public BapiReturn1 getBapiReturn1() {
		return bapiReturn1;
	}

	public void setBapiReturn1(BapiReturn1 bapiReturn1) {
		this.bapiReturn1 = bapiReturn1;
	}

	public String getPartner() {
		return partner;
	}

	public void setPartner(String partner) {
		this.partner = partner;
	}

	

	public String getLoggedInUserName() {
		return loggedInUserName;
	}

	public void setLoggedInUserName(String loggedInUserName) {
		this.loggedInUserName = loggedInUserName;
	}

	public String getLoggedInUserPassword() {
		return loggedInUserPassword;
	}

	public void setLoggedInUserPassword(String loggedInUserPassword) {
		this.loggedInUserPassword = loggedInUserPassword;
	}

	public String getIsUserLoggedIn() {
		return IsUserLoggedIn;
	}

	public void setIsUserLoggedIn(String isUserLoggedIn) {
		IsUserLoggedIn = isUserLoggedIn;
	}

	public String getDashBoardFromDate() {
		return dashBoardFromDate;
	}

	public void setDashBoardFromDate(String dashBoardFromDate) {
		this.dashBoardFromDate = dashBoardFromDate;
	}

	public String getDashBoardToDate() {
		return dashBoardToDate;
	}

	public void setDashBoardToDate(String dashBoardToDate) {
		this.dashBoardToDate = dashBoardToDate;
	}

	@Override
	public String toString() {
		return "LoginDTO [bapiReturn1=" + bapiReturn1 + ", partner=" + partner + ", accgroup=" + accgroup
				+ ", peAddress=" + peAddress + ", salesOrgSelected=" + salesOrgSelected + ", divisionSelected="
				+ divisionSelected + ", disChnlSelected=" + disChnlSelected + ", bapiReturn2=" + bapiReturn2
				+ ", mpSalesOrgTree=" + mpSalesOrgTree + ", salesorgList=" + salesorgList + ", companycodeList="
				+ companycodeList + ", loggedInUserName=" + loggedInUserName + ", loggedInUserPassword="
				+ loggedInUserPassword + ", IsUserLoggedIn=" + IsUserLoggedIn + ", dashBoardFromDate="
				+ dashBoardFromDate + ", dashBoardToDate=" + dashBoardToDate + ", frequentUsedMenuList=" + frequentUsedMenuList + "]";
		
	}

	public Map<String,SalesOrgTree> getSalesOrgDivisionWise() {
		return salesOrgDivisionWise;
	}

	public void setSalesOrgDivisionWise(Map<String,SalesOrgTree> salesOrgDivisionWise) {
		this.salesOrgDivisionWise = salesOrgDivisionWise;
	}

	public String getCrmDashboardBuyerId() {
		return crmDashboardBuyerId;
	}

	public void setCrmDashboardBuyerId(String crmDashboardBuyerId) {
		this.crmDashboardBuyerId = crmDashboardBuyerId;
	}

	public SupplierDetailsDTO getSupplierDetailsDTO() {
		return supplierDetailsDTO;
	}

	public void setSupplierDetailsDTO(SupplierDetailsDTO supplierDetailsDTO) {
		this.supplierDetailsDTO = supplierDetailsDTO;
	}

	//Sanjana
	public Integer getPendingPO() {
		return pendingPO;
	}

	public void setPendingPO(Integer pendingPO) {
		this.pendingPO = pendingPO;
	}

	

	
}
