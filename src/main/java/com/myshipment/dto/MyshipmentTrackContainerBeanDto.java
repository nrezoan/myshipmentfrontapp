package com.myshipment.dto;

import java.math.BigDecimal;

public class MyshipmentTrackContainerBeanDto {
	//@Parameter("SO_NO")
	private String so_no;
	//@Parameter("BL_NO")
	private String bl_no;
	//@Parameter("ITEM_NO")
	private String item_no;
	//@Parameter("SEQ_NO")
	private String seq_no ;
	//@Parameter("CONT_NO")
	private String cont_no;
	//@Parameter("CONT_SIZE")
	private String cont_size;
	//@Parameter("SEAL_NO")
	private String seal_no;
	//@Parameter("CONT_MODE")
	private String cont_mode;
	//@Parameter("PACD_QTY")
	private BigDecimal pacd_qty;
	//@Parameter("PACD_PCS")
	private String pacd_pcs;
	//@Parameter("PACD_VOL")
	private BigDecimal pacd_vol;
	//@Parameter("PACD_WT")
	private BigDecimal pacd_wt;
	
	private String po_no;
	
	private String style;

	public String getSo_no() {
		return so_no;
	}

	public void setSo_no(String so_no) {
		this.so_no = so_no;
	}

	public String getBl_no() {
		return bl_no;
	}

	public void setBl_no(String bl_no) {
		this.bl_no = bl_no;
	}

	public String getItem_no() {
		return item_no;
	}

	public void setItem_no(String item_no) {
		this.item_no = item_no;
	}

	public String getSeq_no() {
		return seq_no;
	}

	public void setSeq_no(String seq_no) {
		this.seq_no = seq_no;
	}

	public String getCont_no() {
		return cont_no;
	}

	public void setCont_no(String cont_no) {
		this.cont_no = cont_no;
	}

	public String getCont_size() {
		return cont_size;
	}

	public void setCont_size(String cont_size) {
		this.cont_size = cont_size;
	}

	public String getSeal_no() {
		return seal_no;
	}

	public void setSeal_no(String seal_no) {
		this.seal_no = seal_no;
	}

	public String getCont_mode() {
		return cont_mode;
	}

	public void setCont_mode(String cont_mode) {
		this.cont_mode = cont_mode;
	}

	public BigDecimal getPacd_qty() {
		return pacd_qty;
	}

	public void setPacd_qty(BigDecimal pacd_qty) {
		this.pacd_qty = pacd_qty;
	}

	public String getPacd_pcs() {
		return pacd_pcs;
	}

	public void setPacd_pcs(String pacd_pcs) {
		this.pacd_pcs = pacd_pcs;
	}

	public BigDecimal getPacd_vol() {
		return pacd_vol;
	}

	public void setPacd_vol(BigDecimal pacd_vol) {
		this.pacd_vol = pacd_vol;
	}

	public BigDecimal getPacd_wt() {
		return pacd_wt;
	}

	public void setPacd_wt(BigDecimal pacd_wt) {
		this.pacd_wt = pacd_wt;
	}

	public String getPo_no() {
		return po_no;
	}

	public void setPo_no(String po_no) {
		this.po_no = po_no;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}
	
	
	
}
