package com.myshipment.dto;

import java.util.List;

import com.myshipment.model.BapiReturn1;
import com.myshipment.model.BuSupListBean;
import com.myshipment.model.DocDesListBean;
import com.myshipment.model.FrieghtModeBean;
import com.myshipment.model.HblListBean;
import com.myshipment.model.IncoListBean;
import com.myshipment.model.LoListBean;
import com.myshipment.model.TvBapiBean;
import com.myshipment.model.UomBean;

/**
 * @author virendra
 *
 */
public class SupplierDetailsDTO {
	private static final Long serialVersionUID=-2344565678989l;
	private BapiReturn1 bapiReturn1;
	private List<LoListBean> loList; 
	private List<HblListBean> hblList;
	private List<TvBapiBean> tvBapiList;
	private List<UomBean> uomList;
	private List<IncoListBean> incoList;
	private List<FrieghtModeBean> freightModeList;
	private List<DocDesListBean> docDesList;
	private List<BuSupListBean> buSupList;
	private BuyersSuppliersmap buyersSuppliersmap;
	public BapiReturn1 getBapiReturn1() {
		return bapiReturn1;
	}
	public void setBapiReturn1(BapiReturn1 bapiReturn1) {
		this.bapiReturn1 = bapiReturn1;
	}
	public List<LoListBean> getLoList() {
		return loList;
	}
	public void setLoList(List<LoListBean> loList) {
		this.loList = loList;
	}
	public List<HblListBean> getHblList() {
		return hblList;
	}
	public void setHblList(List<HblListBean> hblList) {
		this.hblList = hblList;
	}
	public List<TvBapiBean> getTvBapiList() {
		return tvBapiList;
	}
	public void setTvBapiList(List<TvBapiBean> tvBapiList) {
		this.tvBapiList = tvBapiList;
	}
	public List<UomBean> getUomList() {
		return uomList;
	}
	public void setUomList(List<UomBean> uomList) {
		this.uomList = uomList;
	}
	public List<IncoListBean> getIncoList() {
		return incoList;
	}
	public void setIncoList(List<IncoListBean> incoList) {
		this.incoList = incoList;
	}
	public List<FrieghtModeBean> getFreightModeList() {
		return freightModeList;
	}
	public void setFreightModeList(List<FrieghtModeBean> freightModeList) {
		this.freightModeList = freightModeList;
	}
	public List<DocDesListBean> getDocDesList() {
		return docDesList;
	}
	public void setDocDesList(List<DocDesListBean> docDesList) {
		this.docDesList = docDesList;
	}
	public List<BuSupListBean> getBuSupList() {
		return buSupList;
	}
	public void setBuSupList(List<BuSupListBean> buSupList) {
		this.buSupList = buSupList;
	}
	public BuyersSuppliersmap getBuyersSuppliersmap() {
		return buyersSuppliersmap;
	}
	public void setBuyersSuppliersmap(BuyersSuppliersmap buyersSuppliersmap) {
		this.buyersSuppliersmap = buyersSuppliersmap;
	}
	@Override
	public String toString() {
		return "SupplierDetailsDTO [bapiReturn1=" + bapiReturn1 + ", loList=" + loList + ", hblList=" + hblList
				+ ", tvBapiList=" + tvBapiList + ", uomList=" + uomList + ", incoList=" + incoList
				+ ", freightModeList=" + freightModeList + ", docDesList=" + docDesList + ", buSupList=" + buSupList
				+ ", buyersSuppliersmap=" + buyersSuppliersmap + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((bapiReturn1 == null) ? 0 : bapiReturn1.hashCode());
		result = prime * result + ((buSupList == null) ? 0 : buSupList.hashCode());
		result = prime * result + ((buyersSuppliersmap == null) ? 0 : buyersSuppliersmap.hashCode());
		result = prime * result + ((docDesList == null) ? 0 : docDesList.hashCode());
		result = prime * result + ((freightModeList == null) ? 0 : freightModeList.hashCode());
		result = prime * result + ((hblList == null) ? 0 : hblList.hashCode());
		result = prime * result + ((incoList == null) ? 0 : incoList.hashCode());
		result = prime * result + ((loList == null) ? 0 : loList.hashCode());
		result = prime * result + ((tvBapiList == null) ? 0 : tvBapiList.hashCode());
		result = prime * result + ((uomList == null) ? 0 : uomList.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SupplierDetailsDTO other = (SupplierDetailsDTO) obj;
		if (bapiReturn1 == null) {
			if (other.bapiReturn1 != null)
				return false;
		} else if (!bapiReturn1.equals(other.bapiReturn1))
			return false;
		if (buSupList == null) {
			if (other.buSupList != null)
				return false;
		} else if (!buSupList.equals(other.buSupList))
			return false;
		if (buyersSuppliersmap == null) {
			if (other.buyersSuppliersmap != null)
				return false;
		} else if (!buyersSuppliersmap.equals(other.buyersSuppliersmap))
			return false;
		if (docDesList == null) {
			if (other.docDesList != null)
				return false;
		} else if (!docDesList.equals(other.docDesList))
			return false;
		if (freightModeList == null) {
			if (other.freightModeList != null)
				return false;
		} else if (!freightModeList.equals(other.freightModeList))
			return false;
		if (hblList == null) {
			if (other.hblList != null)
				return false;
		} else if (!hblList.equals(other.hblList))
			return false;
		if (incoList == null) {
			if (other.incoList != null)
				return false;
		} else if (!incoList.equals(other.incoList))
			return false;
		if (loList == null) {
			if (other.loList != null)
				return false;
		} else if (!loList.equals(other.loList))
			return false;
		if (tvBapiList == null) {
			if (other.tvBapiList != null)
				return false;
		} else if (!tvBapiList.equals(other.tvBapiList))
			return false;
		if (uomList == null) {
			if (other.uomList != null)
				return false;
		} else if (!uomList.equals(other.uomList))
			return false;
		return true;
	}
	
	

}
