package com.myshipment.dto;

import java.util.Set;

public class DistributionChannel {
	private String distrChan;	
	private String distrChanText;
	private Set<Division> lstDivision;
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((distrChan == null) ? 0 : distrChan.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DistributionChannel other = (DistributionChannel) obj;
		if (distrChan == null) {
			if (other.distrChan != null)
				return false;
		} else if (!distrChan.equals(other.distrChan))
			return false;
		return true;
	}
	public Set<Division> getLstDivision() {
		return lstDivision;
	}
	public void setLstDivision(Set<Division> lstDivision) {
		this.lstDivision = lstDivision;
	}
	public String getDistrChan() {
		return distrChan;
	}
	public void setDistrChan(String distrChan) {
		this.distrChan = distrChan;
	}
	public String getDistrChanText() {
		return distrChanText;
	}
	public void setDistrChanText(String distrChanText) {
		this.distrChanText = distrChanText;
	}

}
