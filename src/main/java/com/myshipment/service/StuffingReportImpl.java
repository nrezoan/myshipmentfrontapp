package com.myshipment.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/*
 * @ Gufranur Rahman
 */
import com.myshipment.model.BillOfLandingDetails;
import com.myshipment.model.RequestParams;
import com.myshipment.model.StuffingReportData;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;

@Service
public class StuffingReportImpl implements IStuffingReport{

	private Logger logger = Logger.getLogger(AirBillOfLandingImpl.class);
	@Autowired
	private RestService restService;

	public RestService getRestService() {
		return restService;
	}

	public void setRestService(RestService restService) {
		this.restService = restService;
	}


	
	@Override
	public StuffingReportData getStuffingDetail(RequestParams req) {
		StuffingReportData stuffingReport = null;
		StringBuffer url=new StringBuffer(RestUtil.StuffingReport_DETAILS);
		stuffingReport = restService.postForObject(RestUtil.prepareUrlForService(url).toString(), req, StuffingReportData.class);
		return stuffingReport;
	}


}
