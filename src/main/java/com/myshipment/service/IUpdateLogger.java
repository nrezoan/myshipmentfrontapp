package com.myshipment.service;

import java.util.List;

import com.myshipment.model.CRMLogger;
import com.myshipment.model.ReportParams;

public interface IUpdateLogger {
	public int getUpdateLogger(List<CRMLogger> crmLogger);
	public List<CRMLogger> getLoggerHblWise(CRMLogger req);

}
