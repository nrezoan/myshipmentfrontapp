package com.myshipment.service;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import com.myshipment.controller.OrderController;
import com.myshipment.mappers.POUploadMapper;
import com.myshipment.model.PurchaseOrdersModel;
import com.myshipment.model.TemplateModel;

public class TemplateMappingService implements ITemplateMappingService {

	private Logger logger = Logger.getLogger(OrderController.class);
	@Autowired
	private POUploadMapper poUploadMapper;

	@Override
	public Boolean saveTemplateMapping(TemplateModel templateModel) {
		// TODO Auto-generated method stub
		TemplateModel template = poUploadMapper.checkTemplateExists(templateModel.getTemplate_id());
		Boolean check = false;
		if (template == null) {
			logger.info("Saving Template");
			poUploadMapper.insertPOTemplate(templateModel);
		} else {
			logger.info("Template Already exists");
			check = true;
		}
		return check;

	}

	@Override
	public void updateTemplateMapping(TemplateModel templateModel) {
		// TODO Auto-generated method stub
		logger.info("Updating Template for " + templateModel.getTemplate_id());
		poUploadMapper.updatePOTemplate(templateModel.getTemplate_id());
		poUploadMapper.insertPOTemplate(templateModel);
		logger.info("Template updated for " + templateModel.getTemplate_id());

	}

	@Override
	public TemplateModel getTemplate(String buyerId) {
		// TODO Auto-generated method stub
		return poUploadMapper.getTemplate(buyerId);
	}

	@Override
	public void savePoDetails(List<PurchaseOrdersModel> purchaseOrderList) {

		for (PurchaseOrdersModel purchaseOrdersModel : purchaseOrderList) {

			poUploadMapper.insertPODetails(purchaseOrdersModel);
		}

	}

	@Override
	public List<PurchaseOrdersModel> getPoDetails(String poNo, String buyer_id) {

		List<PurchaseOrdersModel> orderList = poUploadMapper.getPOforUpdate(buyer_id, poNo);
		return orderList;

	}

	@Override
	public void updatePoDetails(List<PurchaseOrdersModel> purchaseOrderList) {

		for (PurchaseOrdersModel purchaseOrdersModel : purchaseOrderList) {

			poUploadMapper.updatePODetails(purchaseOrdersModel);
		}

	}
	
	@Override
	public void deletePO(String app_id) {
		poUploadMapper.deletePO(app_id);
	}
}
