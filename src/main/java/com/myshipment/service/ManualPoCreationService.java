package com.myshipment.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myshipment.dao.IManualPoCreationDAO;
import com.myshipment.dto.ManualPODTO;
import com.myshipment.model.HeaderPOData;
import com.myshipment.model.SegPurchaseOrder;
/*
 * @ Ranjeet Kumar
 */
@Service
public class ManualPoCreationService implements IManualPoCreationService{

	@Autowired
	private IManualPoCreationDAO manualPoCreationDAO;
	
	@Override
	public Long createManualPO(ManualPODTO manualPODTO) {
		 
		HeaderPOData headerPOData = createHeaderData(manualPODTO);
		
		List<SegPurchaseOrder> egPurchaseOrder = manualPODTO.getDynamicData();
		Long poId = manualPoCreationDAO.createManualPO(headerPOData, egPurchaseOrder);
		
		return poId;
	}

	private HeaderPOData createHeaderData(ManualPODTO manualPODTO)
	{
		HeaderPOData headerPOData = new HeaderPOData();
		
		headerPOData.setVc_po_no(manualPODTO.getVc_po_no());
		headerPOData.setNu_client_code(null==manualPODTO.getNu_client_code()?"":manualPODTO.getNu_client_code());
		headerPOData.setCompany_code(null==manualPODTO.getCompany_code()?"":manualPODTO.getCompany_code());
		headerPOData.setSales_org(null==manualPODTO.getSales_org()?"":manualPODTO.getSales_org());
		headerPOData.setSupplier_code(null==manualPODTO.getSupplier_code()?"":manualPODTO.getSupplier_code());
		headerPOData.setVc_buy_house(null==manualPODTO.getVc_buy_house()?"":manualPODTO.getVc_buy_house());
		headerPOData.setVc_buyer(null==manualPODTO.getVc_buyer()?"":manualPODTO.getVc_buyer());
		//headerPOData.setVc_hs_code(manualPODTO.getVc_hs_code());
		headerPOData.setCurrentDate(getCurrentDate());
		
		return headerPOData;
	}
	
	private SegPurchaseOrder createLineItemData(ManualPODTO manualPODTO)
	{
		SegPurchaseOrder segPurchaseOrder = new SegPurchaseOrder();
		
		segPurchaseOrder.setVc_po_no(manualPODTO.getVc_po_no());
		segPurchaseOrder.setVc_product_no(manualPODTO.getVc_product_no());
		segPurchaseOrder.setVc_sku_no(manualPODTO.getVc_sku_no());
		segPurchaseOrder.setVc_style_no(manualPODTO.getVc_style_no());
		segPurchaseOrder.setVc_article_no(manualPODTO.getVc_article_no());
		segPurchaseOrder.setVc_color(manualPODTO.getVc_color());
		segPurchaseOrder.setVc_size(manualPODTO.getVc_size());
		segPurchaseOrder.setVc_pol(manualPODTO.getVc_pol());
		segPurchaseOrder.setVc_pod(manualPODTO.getVc_pod());
		segPurchaseOrder.setNu_no_pcs_ctns(manualPODTO.getNu_no_pcs_ctns());
		segPurchaseOrder.setVc_commodity(manualPODTO.getVc_commodity());
		segPurchaseOrder.setVc_tot_pcs(manualPODTO.getVc_tot_pcs());
		segPurchaseOrder.setVc_quan(manualPODTO.getVc_quan());
		segPurchaseOrder.setVc_in_hcm(manualPODTO.getVc_in_hcm());
		segPurchaseOrder.setSap_quotation(manualPODTO.getSap_quotation());
		segPurchaseOrder.setVc_division(manualPODTO.getVc_division());
		
		segPurchaseOrder.setCurrentDate(getCurrentDate());
		
		return segPurchaseOrder;
	}
	
	private Date getCurrentDate()
	{
		Date currentDate = new Date();
		
		return currentDate;
	}

	@Override
	public List<SegPurchaseOrder> getCurrentManuallyUploadedData(Long poId) {
		
		List<SegPurchaseOrder> uploadedRecords = manualPoCreationDAO.getCurrentlyManuallyUploadedRecords(poId);
		return uploadedRecords;
	}

}
