package com.myshipment.service;

import org.springframework.web.multipart.MultipartFile;

import com.myshipment.model.DirectBookingParams;

public interface IFileUploadService {
	
	public DirectBookingParams processFileUploaded(MultipartFile multipartFile,String filename,DirectBookingParams directBookingParams);

}
