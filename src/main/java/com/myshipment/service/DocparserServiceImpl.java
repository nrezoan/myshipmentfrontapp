package com.myshipment.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;

import org.apache.commons.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.myshipment.model.DocParserMetaDataModel;

@Service
public class DocparserServiceImpl implements IDocparserService {

	@Override
	public String convert(MultipartFile file) throws Exception {
		// TODO Auto-generated method stub
		File convFile = new File(file.getOriginalFilename());
		convFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		byte[] bytes = loadFile(convFile);
		byte[] encoded = Base64.encodeBase64(bytes);
		String encodedString = new String(encoded);
		if (convFile.delete())
			System.out.println("Delete file");
		return encodedString;
	}

	@Override
	public String docparserFileUpload(String file) {
		// TODO Auto-generated method stub
		String url = "https://api.docparser.com/v1/document/upload/wfrcgctvbdhn";
		// String url="https://api.docparser.com/v1/parsers";
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders httpHeaders = new HttpHeaders();

		httpHeaders.setAccept(Collections.singletonList(MediaType.ALL));
		httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);

		MultiValueMap<String, Object> body = new LinkedMultiValueMap<>();

		httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);
		httpHeaders.set("api_key", "9cfd72aca8439ff3f367c74d33731ecc4763d2b6");

		body.set("file_content", file);

		HttpEntity<?> entity = new HttpEntity<Object>(body, httpHeaders);
		ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);

		System.out.println(responseEntity.getBody());

		String documentInfo = responseEntity.getBody();
		System.out.println(documentInfo);
		System.out.println();
		System.out.println("Taking time for pre processing");
		System.out.println();
		System.out.println();
		System.out.println();

		return documentInfo;
	}

	@Override
	public String docparserFileRetrieve(String uploadedFileJson) throws JSONException {
		// TODO Auto-generated method stub
		String documentId = fetchDocumentInformation(uploadedFileJson);
		String url = "https://api.docparser.com/v1/results/wfrcgctvbdhn/" + documentId;
		System.out.println(url);
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders httpHeaders = new HttpHeaders();

		httpHeaders.setAccept(Collections.singletonList(MediaType.ALL));
		httpHeaders.setContentType(MediaType.MULTIPART_FORM_DATA);

		httpHeaders.set("api_key", "9cfd72aca8439ff3f367c74d33731ecc4763d2b6");

		HttpEntity<?> entity = new HttpEntity<Object>(httpHeaders);
		ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, entity, String.class);

		System.out.println();
		System.out.println();
		System.out.println();
		return responseEntity.getBody();
		// String
		// x=responseEntity.getBody().substring(1,responseEntity.getBody().length()-1);
		// System.out.println(x);
		// parseFile(x);
	}

	// Base64 converter
	private byte[] loadFile(File file) throws IOException {
		InputStream is = new FileInputStream(file);

		long length = file.length();
		if (length > Integer.MAX_VALUE) {
			// File is too large
		}
		byte[] bytes = new byte[(int) length];

		int offset = 0;
		int numRead = 0;
		while (offset < bytes.length && (numRead = is.read(bytes, offset, bytes.length - offset)) >= 0) {
			offset += numRead;
		}

		if (offset < bytes.length) {
			throw new IOException("Could not completely read file " + file.getName());
		}

		is.close();
		return bytes;
	}
	
	// parsing document id json
	private static String fetchDocumentInformation(String documentInfo) throws JSONException {
		JSONObject jsonObject = new JSONObject(documentInfo);
		Gson gson = new GsonBuilder().setPrettyPrinting().create();
		DocParserMetaDataModel documentId = gson.fromJson(jsonObject.toString(), DocParserMetaDataModel.class);
		System.out.println("Document Id " + documentId.getId());
		return documentId.getId();
	}


}
