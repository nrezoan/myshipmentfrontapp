package com.myshipment.service;

import java.net.ConnectException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.ResourceAccessException;

import com.myshipment.dao.IFrequentlyUsedFormDAO;
import com.myshipment.dao.IPoMgmtDAO;
import com.myshipment.dto.BuyersSuppliersmap;
import com.myshipment.dto.DistributionChannel;
import com.myshipment.dto.Division;
import com.myshipment.dto.LoginDTO;
import com.myshipment.dto.SalesGroup;
import com.myshipment.dto.SalesOrgTree;
import com.myshipment.dto.SupplierDetailsDTO;
import com.myshipment.model.BuSupListBean;
import com.myshipment.model.DocDesListBean;
import com.myshipment.model.FrequentUsedMenu;
import com.myshipment.model.FrequentlyUsedFormMdl;
import com.myshipment.model.FrieghtModeBean;
import com.myshipment.model.HblListBean;
import com.myshipment.model.IncoListBean;
import com.myshipment.model.Login;
import com.myshipment.model.SalesorgListBean;
import com.myshipment.model.SaveAsTemplateMdl;
import com.myshipment.model.SupRequestParams;
import com.myshipment.model.SupplierPreLoadJsonData;
import com.myshipment.model.UserDetails;
import com.myshipment.util.CommonConstant;
import com.myshipment.util.ErrorCode;
import com.myshipment.util.MyShipApplicationException;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;

/**
 * @author Virendra Kumar
 *
 */
@Service
public class LoginServiceImpl implements ILoginService {
	// @Autowired
	private Logger logger = Logger.getLogger(LoginServiceImpl.class);
	@Autowired
	private RestService restService;
	
	@Autowired
	private IFrequentlyUsedFormDAO frequntlyUsedformDao;


	public RestService getRestService() {
		return restService;
	}

	public void setRestService(RestService restService) {
		this.restService = restService;
	}
	
	public IFrequentlyUsedFormDAO getPoMgmtDao() {
		return frequntlyUsedformDao;
	}

	public void setPoMgmtDao(IFrequentlyUsedFormDAO frequntlyUsedformDao) {
		this.frequntlyUsedformDao = frequntlyUsedformDao;
	}

	@Override
	public LoginDTO authenticateAndGetLoginDetails(Login login) throws MyShipApplicationException {

		logger.debug(this.getClass().getName() + " getLoginDetails()  start-- ");
		LoginDTO loginDto = null;
		/*logger.debug(this.getClass().getName() + " getLoginDetails()  calling the service:  URL="
				+ (RestUtil.prepareUrlForService(RestUtil.LOGIN_DETAILS)));*/
		UserDetails userDetails = null;
		StringBuffer url=new StringBuffer(RestUtil.LOGIN_DETAILS);
		try {
			userDetails = restService.postForObject(RestUtil.prepareUrlForService(url).toString(), login,
					UserDetails.class);
		} catch (Exception e) {
			if (e instanceof ConnectException)
				throw new MyShipApplicationException(ErrorCode.SERVICE_CONNECTION_ERROR, e.getLocalizedMessage());
			if (e instanceof ResourceAccessException)
				throw new MyShipApplicationException(ErrorCode.SERVICE_CONNECTION_ERROR, e.getLocalizedMessage());
		}
		if(null!=userDetails)
		{
			loginDto=new LoginDTO();
		BeanUtils.copyProperties(userDetails, loginDto);
		if(userDetails.getBapiReturn2().getMessage().trim().equalsIgnoreCase("Login successfull!!".trim()))
				{
		SalesOrgTree salesOrgTree = null;
		Map<String, SalesOrgTree> salesMap = new HashMap<String, SalesOrgTree>();
		
		List<SalesorgListBean> salesList = userDetails.getSalesorgList();
		Set<DistributionChannel> lstDistChnl = null;
		Set<Division> lstDivision = new HashSet<>();
		Set<SalesGroup> lstSalesGrp = new HashSet<>();

		for (SalesorgListBean salesorgListBean : salesList) {

			if (salesorgListBean.getSalesOrg() != null) {

				if (!salesMap.containsKey(salesorgListBean.getSalesOrg())) {
					salesOrgTree = new SalesOrgTree();
					salesOrgTree.setSalesOrg(salesorgListBean.getSalesOrg());
					salesOrgTree.setSalesOrgText(salesorgListBean.getSalesOrgText());
					DistributionChannel distChn = new DistributionChannel();
					distChn.setDistrChan(salesorgListBean.getDistrChan());
					distChn.setDistrChanText(salesorgListBean.getDistrChanText());
					Division division = new Division();
					division.setDivision(salesorgListBean.getDivision());
					division.setDivisionText(salesorgListBean.getDivisionText());
					lstDivision = new HashSet<>();
					lstDivision.add(division);
					distChn.setLstDivision(lstDivision);
					lstDistChnl = new HashSet<>();
					lstDistChnl.add(distChn);

					salesOrgTree.setLstDistChnl(lstDistChnl);

					salesMap.put(salesOrgTree.getSalesOrg(), salesOrgTree);
				} else {
					salesOrgTree = salesMap.get(salesorgListBean.getSalesOrg());
					lstDistChnl = salesOrgTree.getLstDistChnl();

					for (Iterator iterator = lstDistChnl.iterator(); iterator.hasNext();) {
						DistributionChannel distributionChannel = (DistributionChannel) iterator.next();
						if (distributionChannel.getDistrChan().equals(salesorgListBean.getDistrChan())) {
							Set<Division> setDivision = distributionChannel.getLstDivision();
							Division division = new Division();
							division.setDivision(salesorgListBean.getDivision());
							division.setDivisionText(salesorgListBean.getDivisionText());
							setDivision.add(division);
							distributionChannel.setLstDivision(setDivision);
						} else {
							Division division = new Division();
							division.setDivision(salesorgListBean.getDivision());
							division.setDivisionText(salesorgListBean.getDivisionText());
							Set<Division> setDivision = new HashSet<>();
							setDivision.add(division);
							DistributionChannel dstChnl = null;
							for (DistributionChannel distributionChannel1 : lstDistChnl) {
								if (distributionChannel1.getDistrChan() == salesorgListBean.getDistrChan()) {
									dstChnl = distributionChannel1;
									break;
								}
							}
							if (dstChnl == null)
								dstChnl = new DistributionChannel();
							dstChnl.setDistrChan(salesorgListBean.getDistrChan());
							dstChnl.setDistrChanText(salesorgListBean.getDistrChanText());
							dstChnl.setLstDivision(setDivision);
							lstDistChnl.add(dstChnl);
						}

					}
					salesOrgTree.setLstDistChnl(lstDistChnl);

				}

			}

		}
		loginDto.setMpSalesOrgTree(salesMap);
		//loginDto.setLoggedInUserName(login.getUserCode());
		loginDto.setLoggedInUserName(userDetails.getSalesAreas().get(0).getCustomer());
		loginDto.setLoggedInUserPassword(login.getPassword());
		logger.debug(this.getClass().getName() + " getLoginDetails()  called the service:  " + loginDto);
				}
		}
		return loginDto;
	}

	public SupplierDetailsDTO getSupplierPreLoadedData(SupRequestParams supplierRequestParams, HttpSession session) {
		StringBuffer url=new StringBuffer(RestUtil.SUPPLIER_DETAILS);
		SupplierPreLoadJsonData supplierPreLoadJsonData = restService.postForObject(
				RestUtil.prepareUrlForService(url).toString(), supplierRequestParams, SupplierPreLoadJsonData.class);
		SupplierDetailsDTO supDto = new SupplierDetailsDTO();
		Map<String, String> docTypes = new HashMap<String, String>();
		Map<String, String> buyers = new HashMap<String, String>();
		Map<String, String> buyersBank = new HashMap<String, String>();
		Map<String, String> shippersBank = new HashMap<String, String>();
		Map<String, String> shippersAddress = new HashMap<String, String>();
		Map<String, String> localBuyingHouse = new HashMap<String, String>();
		Map<String, String> tos = new HashMap<String, String>();
		Map<String, String> freightMode = new HashMap<String, String>();
		Map<String, String> transType = new HashMap<String, String>();
		Map<String, String> hblInit = new HashMap<String, String>();
		Map<String, String> portLink = new HashMap<String, String>();
		Map<String, String> portLoad = new HashMap<String, String>();
		Map<String, String> unit = new HashMap<String, String>();
		Map<String, String> cartonUnit = new HashMap<String, String>();
		Map<String, String> shipppingLine = new HashMap<String, String>();
		Map<String, String> shipper = new HashMap<String, String>();
		Map<String, String> consignee = new HashMap<String, String>();
		Map<String, String> notifyParty = new HashMap<String, String>();
		Map<String, String> shipmentMode = new HashMap<String, String>();
		Map<String, String> containerMode = new HashMap<String, String>();
		if (null != supplierPreLoadJsonData) {
			BeanUtils.copyProperties(supplierPreLoadJsonData, supDto);
			List<BuSupListBean> lstBuySup = null;
			lstBuySup = supplierPreLoadJsonData.getBuSupList();
			for (Iterator iterator = lstBuySup.iterator(); iterator.hasNext();) {
				BuSupListBean buSupListBean = (BuSupListBean) iterator.next();
				if (buSupListBean.getZzParvw().equals("WE") || buSupListBean.getZzParvw().equals("ZD"))
					buyers.put(buSupListBean.getZzCust(), buSupListBean.getZzCustName());
				if (buSupListBean.getZzParvw().equals("ZB"))
					localBuyingHouse.put(buSupListBean.getZzCust(), buSupListBean.getZzCustName());
				if (buSupListBean.getZzParvw().equals("ZY"))
					shippersBank.put(buSupListBean.getZzCust(), buSupListBean.getZzCustName());
				if (buSupListBean.getZzParvw().equals("ZX"))
					buyersBank.put(buSupListBean.getZzCust(), buSupListBean.getZzCustName());
				if (buSupListBean.getZzParvw().equals("ZO"))
					notifyParty.put(buSupListBean.getZzCust(), buSupListBean.getZzCustName());
				if (buSupListBean.getZzParvw().equals("AG")||buSupListBean.getZzParvw().equals("ZT"))
					shipper.put(buSupListBean.getZzCust(), buSupListBean.getZzCustName());
				if (buSupListBean.getZzParvw().equals("PY"))
					notifyParty.put(buSupListBean.getZzCust(), buSupListBean.getZzCustName());
				if (buSupListBean.getZzParvw().equals("ZS"))
					shipppingLine.put(buSupListBean.getZzCust(), buSupListBean.getZzCustName());
				if (buSupListBean.getZzParvw().equals("ZA"))
					shipppingLine.put(buSupListBean.getZzCust(), buSupListBean.getZzCustName());

			}
			for (Iterator iterator = supplierPreLoadJsonData.getFreightModeList().iterator(); iterator.hasNext();) {
				FrieghtModeBean frieghtModeBean = (FrieghtModeBean) iterator.next();
				freightMode.put(frieghtModeBean.getZzfreightMode(), frieghtModeBean.getZzfreightModes());
				
			}
			for (Iterator iterator = supplierPreLoadJsonData.getIncoList().iterator(); iterator.hasNext();) {
				IncoListBean incoListBean = (IncoListBean) iterator.next();
				tos.put(incoListBean.getInco(), incoListBean.getInco());
				
			}
			
			for (Iterator iterator = supplierPreLoadJsonData.getDocDesList().iterator(); iterator.hasNext();) {
				DocDesListBean docDesListBean = (DocDesListBean) iterator.next();
				docTypes.put(docDesListBean.getAuart(), docDesListBean.getBezei());
			}
			for (Iterator iterator = supplierPreLoadJsonData.getHblList().iterator(); iterator.hasNext();) {
				HblListBean hblListBean = (HblListBean) iterator.next();
				hblInit.put(hblListBean.getCompIni(),hblListBean.getCompIni());
				
			}
			BuyersSuppliersmap buyersSuppliersmap = new BuyersSuppliersmap();
			buyersSuppliersmap.setBuyers(buyers);
			buyersSuppliersmap.setBuyersBank(buyersBank);
			buyersSuppliersmap.setCartonUnit(cartonUnit);
			buyersSuppliersmap.setConsignee(consignee);
			buyersSuppliersmap.setContainerMode(containerMode);
			buyersSuppliersmap.setDocTypes(docTypes);
			buyersSuppliersmap.setFreightMode(freightMode);
			buyersSuppliersmap.setHblInit(hblInit);
			buyersSuppliersmap.setLocalBuyingHouse(localBuyingHouse);
			buyersSuppliersmap.setNotifyParty(notifyParty);
			buyersSuppliersmap.setPortLink(portLink);
			buyersSuppliersmap.setPortLoad(portLoad);
			buyersSuppliersmap.setShipmentMode(shipmentMode);
			buyersSuppliersmap.setShipper(shipper);
			buyersSuppliersmap.setShippersAddress(shippersAddress);
			buyersSuppliersmap.setShippersBank(shippersBank);
			buyersSuppliersmap.setShipppingLine(shipppingLine);
			buyersSuppliersmap.setTos(tos);
			buyersSuppliersmap.setTransType(transType);
			buyersSuppliersmap.setUnit(unit);

			supDto.setBuyersSuppliersmap(buyersSuppliersmap);
		}

		return supDto;

	}
	public void saveUserFavouriteForm(FrequentlyUsedFormMdl obj)
	{	
		frequntlyUsedformDao.insetFrequentlyUsedFormIntoTable(obj); 
	}
	public List <FrequentUsedMenu> getFrequentUsedMenuById(LoginDTO loginDTO){
		List <FrequentUsedMenu> menuList = null;
		menuList= frequntlyUsedformDao.getFrequentlyUsedMenu(loginDTO);
		return menuList;
	}
}
