package com.myshipment.service;

import java.util.List;

import com.myshipment.dto.ManualPODTO;
import com.myshipment.model.SegPurchaseOrder;
/*
 * @ Ranjeet Kumar
 */
public interface IManualPoCreationService {

	public Long createManualPO(ManualPODTO manualPODTO);
	
	public List<SegPurchaseOrder> getCurrentManuallyUploadedData(Long poId);
}
