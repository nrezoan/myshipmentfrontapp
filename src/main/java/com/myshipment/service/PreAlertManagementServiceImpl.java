/**
*
*@author Ahmad Naquib
*/
package com.myshipment.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myshipment.dao.IPreAlertManagementDAO;
import com.myshipment.dto.LoginDTO;
import com.myshipment.model.DashboardInfoBox;
import com.myshipment.model.MyshipmentTrackHeaderBean;
import com.myshipment.model.PreAlertBL;
import com.myshipment.model.PreAlertHBL;
import com.myshipment.model.PreAlertHBLDetails;
import com.myshipment.model.PreAlertMBL;
import com.myshipment.util.CommonConstant;


@Service
public class PreAlertManagementServiceImpl implements IPreAlertManagementService {
	
	private Logger logger = Logger.getLogger(PreAlertManagementServiceImpl.class);

	@Autowired
	private IPreAlertManagementDAO preAlertManagementDAO;
	
	@Override
	public int updatePreAlertDocument(PreAlertHBL preAlertHBL) throws Exception {
		return preAlertManagementDAO.updatePreAlertDocumentPath(preAlertHBL);
	}
	
	@Override
	public int updatePreAlertMBLDocument(PreAlertHBL preAlertHbl, List<MyshipmentTrackHeaderBean> trackHeaderBeanList) throws Exception {
		PreAlertMBL mblExists = preAlertManagementDAO.checkMBLExist(preAlertHbl);
		int databaseTransactionStatus = 0;
		/*if(mblExists == null) {
			databaseTransactionStatus = preAlertManagementDAO.saveMBLDocument(preAlertHbl);
		} else {
			databaseTransactionStatus = preAlertManagementDAO.updateMBLDocument(preAlertHbl);
		}*/
		
		Set<String> hblNumberSet = new HashSet<String>();
		String mblNumber = preAlertHbl.getMblNumber();
		for(MyshipmentTrackHeaderBean trackHeaderBean : trackHeaderBeanList) {
			if(mblNumber.equals(trackHeaderBean.getMbl_no())) {
				hblNumberSet.add(trackHeaderBean.getBl_no());
			}
		}
		
		String[] hblNumber = hblNumberSet.toArray(new String[hblNumberSet.size()]);
		databaseTransactionStatus = preAlertManagementDAO.saveMBLDocument(preAlertHbl, hblNumber);
		
		return databaseTransactionStatus;
	}

	@Override
	public List<PreAlertHBL> getPreAlertByHBL(String supplierId) throws Exception {
		return preAlertManagementDAO.getPreAlertByHBL(supplierId);
	}

	@Override
	public int savePreAlertHBL(String hblNumber, String supplierId, String buyerId, String agentId, String distributionChannel, String division, String salesOrg) throws Exception {
		return preAlertManagementDAO.insertPreAlertHBL(hblNumber, supplierId, buyerId, agentId, distributionChannel, division, salesOrg);
	}

	@Override
	public PreAlertHBL getPreAlertBySingleHBL(PreAlertHBL preAlertHbl, String myshipmentUid) throws Exception {
		return preAlertManagementDAO.getPreAlertBySingleHBL(preAlertHbl, myshipmentUid);
	}

	@Override
	public List<PreAlertHBL> getPreAlertForDownload(String myshipmentUID, PreAlertHBL preAlertHbl) throws Exception {
		return preAlertManagementDAO.getPreAlertToDownload(myshipmentUID, preAlertHbl);
	}
	
	@Override
	public List<PreAlertHBL> getPreAlertBuyerWiseList(PreAlertHBL preAlertHbl, String status) throws Exception {
		return preAlertManagementDAO.getPreAlertBuyerWiseList(preAlertHbl, status);
	}

	@Override
	public PreAlertHBL checkPreAlertAuth(PreAlertHBL preAlertHBL) throws Exception {
		return preAlertManagementDAO.checkAuthForHBL(preAlertHBL);
	}

	@Override
	public List<PreAlertHBL> getMBlWisePreAlert(String myshipmentUid, PreAlertHBL preAlertHbl) throws Exception {
		return preAlertManagementDAO.getMblwisePreAlertList(myshipmentUid, preAlertHbl);
	}

	@Override
	public PreAlertBL getPreAlertInfo(LoginDTO loginDTO, DashboardInfoBox dashboardInfoBox, List<PreAlertHBL>preAlertHblByBuyerAgentList) throws Exception {
		
		List<MyshipmentTrackHeaderBean> dashboardSupplierWiseHBL = new ArrayList<MyshipmentTrackHeaderBean>();
		
		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getOpenOrderList());
		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getGoodsReceivedList());
		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getStuffingDoneList());
		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getInTransitList());
		dashboardSupplierWiseHBL.addAll(dashboardInfoBox.getDeliveredList());
		
		
		List<PreAlertHBLDetails> preAlertHblDetailsByBuyerList = new ArrayList<PreAlertHBLDetails>();
		
		Map<String, List<PreAlertHBLDetails>> mblWiseHbl = new HashMap<String, List<PreAlertHBLDetails>>();
		
		for(MyshipmentTrackHeaderBean myshipmentTrackHeaderBean : dashboardSupplierWiseHBL) {
			for(PreAlertHBL preAlertHblByBuyerAgent : preAlertHblByBuyerAgentList) {
				if(myshipmentTrackHeaderBean.getBl_no().equals(preAlertHblByBuyerAgent.getHblNumber())) {
					PreAlertHBLDetails preAlertHBLDetails = new PreAlertHBLDetails();
					
					/*preAlertHBLDetails.setPreAlertHbl(preAlertHblByBuyerAgent);
					preAlertHBLDetails.setTrackHeaderBean(myshipmentTrackHeaderBean);*/
					
					/*if(myshipmentTrackHeaderBean.getMbl_no().equals(preAlertHblByBuyerAgent.getMblNumber())) {
						
					}*/
					if(!myshipmentTrackHeaderBean.getMbl_no().equals("")) {
						if(mblWiseHbl.containsKey(myshipmentTrackHeaderBean.getMbl_no())) {
							List<PreAlertHBLDetails> tempPreAlertList = mblWiseHbl.get(myshipmentTrackHeaderBean.getMbl_no());
							tempPreAlertList.add(preAlertHBLDetails);
							
							mblWiseHbl.put(myshipmentTrackHeaderBean.getMbl_no(), tempPreAlertList);
						} else {
							List<PreAlertHBLDetails> tempPreAlertList = new ArrayList<PreAlertHBLDetails>();
							tempPreAlertList.add(preAlertHBLDetails);
							
							mblWiseHbl.put(myshipmentTrackHeaderBean.getMbl_no(), tempPreAlertList);
						}
					}
					
					
					preAlertHBLDetails.setTrackHeaderBean(myshipmentTrackHeaderBean);
					preAlertHBLDetails.setPreAlertHbl(preAlertHblByBuyerAgent);
					
					preAlertHblDetailsByBuyerList.add(preAlertHBLDetails);
				}
			}
		}
		
		PreAlertBL preAlertBl = new PreAlertBL();
		
		preAlertBl.setHbl(preAlertHblDetailsByBuyerList);
		preAlertBl.setMbl(mblWiseHbl);
		
		return preAlertBl;
	}

	@Override
	public Set<String> getListOfHBLToGenerateNonNego(PreAlertHBL preAlertHbl, List<MyshipmentTrackHeaderBean> trackHeaderBeanList) throws Exception {
						
			Set<String> hblNumberSet = new HashSet<String>();
			String mblNumber = preAlertHbl.getMblNumber();
			for(MyshipmentTrackHeaderBean trackHeaderBean : trackHeaderBeanList) {
				if(mblNumber.equals(trackHeaderBean.getMbl_no())) {
					hblNumberSet.add(trackHeaderBean.getBl_no());
				}
			}
			
			return hblNumberSet;
		}	
	
	@Override
	public int updateMblDownloadStatus(PreAlertHBL preAlertHBL) throws Exception {
		// TODO Auto-generated method stub
		return preAlertManagementDAO.updateMblDownloadStatus(preAlertHBL);
	}

}
