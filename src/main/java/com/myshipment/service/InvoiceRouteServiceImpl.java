package com.myshipment.service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.stereotype.Service;

import com.myshipment.model.InvoiceRouteModel;

@Service
public class InvoiceRouteServiceImpl implements IinvoiceRouteService {

	/*
	 * this method replicates the original excel file and creates this fake data
	 * holder. later on this option will be replaced by one dao implementation
	 */
	public List<InvoiceRouteModel> InvoiceRouteServiceImpl() {
		List<InvoiceRouteModel> invoiceRouteModelList = new ArrayList<InvoiceRouteModel>();

		InvoiceRouteModel invoiceRoute1 = new InvoiceRouteModel("BELGIUM", "Dhaka Airport WH", "DHAKA-BDDAC",
				"BRUSSEL-BEBRU", "BRUSSEL-BEBRU", "BELGIUM");
		invoiceRouteModelList.add(invoiceRoute1);
		invoiceRoute1 = new InvoiceRouteModel("POLAND", "Dhaka Airport WH", "DHAKA-BDDAC", "GADKI-PLGDK", "GADKI-PLGDK",
				"POLAND");
		invoiceRouteModelList.add(invoiceRoute1);
		invoiceRoute1 = new InvoiceRouteModel("GERMANY", "Dhaka Airport WH", "DHAKA-BDDAC", "HAMBURG-DEHAM",
				"HAMBURG-DEHAM", "GERMANY");
		invoiceRouteModelList.add(invoiceRoute1);
		invoiceRoute1 = new InvoiceRouteModel("TURKEY", "Dhaka Airport WH", "DHAKA-BDDAC", "ISTANBUL-TRIST",
				"ISTANBUL-TRIST", "TURKEY");
		invoiceRouteModelList.add(invoiceRoute1);
		invoiceRoute1 = new InvoiceRouteModel("GREAT BRITAIN", "Dhaka Airport WH", "DHAKA-BDDAC", "RUGBY-GBRUG",
				"RUGBY-GBRUG", "GREAT BRITAIN");
		invoiceRouteModelList.add(invoiceRoute1);
		invoiceRoute1 = new InvoiceRouteModel("SPAIN", "Dhaka Airport WH", "DHAKA-BDDAC", "MADRID-ESMAD",
				"MADRID-ESMAD", "SPAIN");
		invoiceRouteModelList.add(invoiceRoute1);
		invoiceRoute1 = new InvoiceRouteModel("NORWAY", "Dhaka Airport WH", "DHAKA-BDDAC", "OSLO-NOOSL", "OSLO-NOOSL",
				"NORWAY");
		invoiceRouteModelList.add(invoiceRoute1);
		invoiceRoute1 = new InvoiceRouteModel("CROATIA", "Dhaka Airport WH", "DHAKA-BDDAC", "ZAGREB-HRZAG",
				"ZAGREB-HRZAG", "CROATIA");
		invoiceRouteModelList.add(invoiceRoute1);
		invoiceRoute1 = new InvoiceRouteModel("SWITZERLAND", "Dhaka Airport WH", "DHAKA-BDDAC", "ZURICH-CHZRH",
				"ZURICH-CHZRH", "SWITZERLAND");
		invoiceRouteModelList.add(invoiceRoute1);
		invoiceRoute1 = new InvoiceRouteModel("RUSSIA", "Dhaka Airport WH", "DHAKA-BDDAC",
				"SAINT PETERSBURG PETROLESPORT-RUSPP", "SAINT PETERSBURG PETROLESPORT-RUSPP", "RUSSIA");
		invoiceRouteModelList.add(invoiceRoute1);
		invoiceRoute1 = new InvoiceRouteModel("DENMARK", "Dhaka Airport WH", "DHAKA-BDDAC", "COPENHAGEN-DKCPH",
				"COPENHAGEN-DKCPH", "DENMARK");
		invoiceRouteModelList.add(invoiceRoute1);
		invoiceRoute1 = new InvoiceRouteModel("SWEDEN", "Dhaka Airport WH", "DHAKA-BDDAC", "ARLANDA APT/STOCKHOL-SEARN",
				"ARLANDA APT/STOCKHOL-SEARN", "SWEDEN (ARN)");
		invoiceRouteModelList.add(invoiceRoute1);
		/* working area */
		invoiceRoute1 = new InvoiceRouteModel("SWEDEN", "Dhaka Airport WH", "DHAKA-BDDAC", "GOTHENBURG-SEGOT",
				"GOTHENBURG-SEGOT", "SWEDEN (GOT)");
		invoiceRouteModelList.add(invoiceRoute1);
		/* working area ends */
		invoiceRoute1 = new InvoiceRouteModel("SOUTH AFRICA", "Dhaka Airport WH", "DHAKA-BDDAC", "JOHANNESBURG-ZAJNB",
				"JOHANNESBURG-ZAJNB", "SOUTH AFRICA");
		invoiceRouteModelList.add(invoiceRoute1);
		invoiceRoute1 = new InvoiceRouteModel("NETHERLANDS", "Dhaka Airport WH", "DHAKA-BDDAC", "AMSTERDAM-NLAMS",
				"AMSTERDAM-NLAMS", "NETHERLANDS");
		invoiceRouteModelList.add(invoiceRoute1);
		invoiceRoute1 = new InvoiceRouteModel("ITALY", "Dhaka Airport WH", "DHAKA-BDDAC", "MILANO-ITMIL",
				"MILANO-ITMIL", "ITALY");
		invoiceRouteModelList.add(invoiceRoute1);
		invoiceRoute1 = new InvoiceRouteModel("SERBIA", "Dhaka Airport WH", "DHAKA-BDDAC", "BELGRADE (BEOGRAD)-RSBEG",
				"BELGRADE (BEOGRAD)-RSBEG", "SERBIA");
		invoiceRouteModelList.add(invoiceRoute1);
		invoiceRoute1 = new InvoiceRouteModel("MIDDLE-EAST", "Dhaka Airport WH", "DHAKA-BDDAC", "DUBAI-AEDXB",
				"DUBAI-AEDXB", "MIDDLE-EAST");
		invoiceRouteModelList.add(invoiceRoute1);
		return invoiceRouteModelList;
	}

	@Override
	public InvoiceRouteModel routeInformation(String destination, String orderNumber) throws Exception {
		// TODO Auto-generated method stub
		List<InvoiceRouteModel> invoiceRouteModelList = InvoiceRouteServiceImpl();
		for (Iterator<InvoiceRouteModel> iterator = invoiceRouteModelList.iterator(); iterator.hasNext();) {
			InvoiceRouteModel invoiceRouteModel = (InvoiceRouteModel) iterator.next();
			if (invoiceRouteModel.getDestination().equalsIgnoreCase(destination)) {
				if (invoiceRouteModel.getDestination().equalsIgnoreCase("SWEDEN")) {
					/*
					 * if the destination is sweden and if the ordernumber starts with 1 then port
					 * of discharge will be GOTHENBURG-SEGOT
					 */
					if (orderNumber.charAt(0) == '1'
							&& invoiceRouteModel.getPortOfDischarge().equalsIgnoreCase("GOTHENBURG-SEGOT")) {
						System.out.println("in gothenburg block");
						return invoiceRouteModel;
					} else if (orderNumber.charAt(0) != '1'
							&& invoiceRouteModel.getPortOfDischarge().equalsIgnoreCase("ARLANDA APT/STOCKHOL-SEARN")) {
						System.out.println("in Arlanda Block");
						return invoiceRouteModel;
					}

				} else {
					return invoiceRouteModel;
				}

			}
		}
		return null;
	}

}
