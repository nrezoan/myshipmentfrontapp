package com.myshipment.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/*
 * @ Gufranur Rahman
 */
import com.myshipment.model.BillOfLandingDetails;
import com.myshipment.model.RequestParams;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;

@Service
public class SeaBillOfLandingImpl implements ISeaBillOfLanding{

	private Logger logger = Logger.getLogger(AirBillOfLandingImpl.class);
	@Autowired
	private RestService restService;

	public RestService getRestService() {
		return restService;
	}

	public void setRestService(RestService restService) {
		this.restService = restService;
	}


	
	@Override
	public BillOfLandingDetails getSeaBillLandingDetail(RequestParams req ) {
		BillOfLandingDetails seaBillDetail = null;
		StringBuffer url=new StringBuffer(RestUtil.SeaBillOfLanding_DETAILS);
		seaBillDetail = restService.postForObject(RestUtil.prepareUrlForService(url).toString(), req, BillOfLandingDetails.class);
		return seaBillDetail;
	}


}
