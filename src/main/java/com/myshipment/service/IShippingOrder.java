package com.myshipment.service;

import com.myshipment.model.ReportParams;
import com.myshipment.model.RequestParams;
import com.myshipment.model.ShippingOrderReportJsonData;


public interface IShippingOrder {

	public ShippingOrderReportJsonData getShippingOrderDetail(ReportParams req);
}
