package com.myshipment.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.myshipment.model.DocParserDataModel;
import com.myshipment.model.LineItemObjectModel;

@Service
public class DocparserLineItemServiceImpl implements IDocparserLineItemService{

	
	//working area nazib
	//demo purpose block
	
	 /*
     * return this  method will take the  returned json and convert it into normal java object
     */
	@Override
	public LineItemObjectModel[] fetchDocument(String documentInfo) throws Exception {
		// TODO Auto-generated method stub
    	LineItemObjectModel itemObject[]=null;
    	try {
            //JSONObject jsonObject = new JSONObject(documentInfo);
    	       Gson gson = new GsonBuilder().setPrettyPrinting().create();
    	       List<DocParserDataModel> finalDataList = gson.fromJson(documentInfo, new TypeToken<List<DocParserDataModel>>(){}.getType());        
    	       String[] lineItemCount = finalDataList.get(0).getHeight_width_length().split("[\n]");
    	       itemObject=new LineItemObjectModel[lineItemCount.length];
    	       //one loop will be here which will generate all the line item here 
    	       //for the index 0 grossWeight and TotalPieces will have the direct values 
    	       //for other index grossWeight and TotalPieces will have the value 0
    	       for(int i=0;i<lineItemCount.length;i++) {
    	    	   System.out.println(makingLineItemObject(finalDataList.get(0),i));
    	    	   itemObject[i]=makingLineItemObject(finalDataList.get(0),i);
    	       }
    	       return itemObject;
    	}catch(Exception e) {
    		throw e;
    	}

	}
	 /*
     * return this  method will take the  returned json and convert it into normal java object
     * @docParserData which will have the pojo of whole returned json
     * @index says which number of line element will be executed 
     */
	private LineItemObjectModel makingLineItemObject(DocParserDataModel docParserDataModel,int index) {
    	LineItemObjectModel itemObject=new LineItemObjectModel();
    	itemObject.setPoNo(docParserDataModel.getPo_no());
    	//only the first line item will have the value of these corresponding 0
    	if(index==0) {
    		itemObject.setTotalGrossWeight(docParserDataModel.getGross_weight());
    		itemObject.setTotalPieces(docParserDataModel.getTotal_pieces());
    	}else {
    		itemObject.setTotalGrossWeight("0");
    		itemObject.setTotalPieces("0");
    	}
    	String[] cartoonQuantity = docParserDataModel.getCarton_quantity().split("[*|\n]");
    	itemObject.setCartonQuantity(cartoonQuantity[index]);
    	String[] heightWeightLengthArray = docParserDataModel.getHeight_width_length().split("[\n]");
    	String[] heightWeightLengthSignle=heightWeightLengthArray[index].split("[*]");
    	itemObject.setCartonLength(heightWeightLengthSignle[0]);
    	itemObject.setCartonWidth(heightWeightLengthSignle[1]);
    	itemObject.setCartonHeight(heightWeightLengthSignle[2]);
    	itemObject.sethSCode(docParserDataModel.getHs_code());
    	return itemObject;
    }
	

}
