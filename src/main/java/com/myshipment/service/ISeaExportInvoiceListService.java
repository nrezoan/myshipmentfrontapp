package com.myshipment.service;

import com.myshipment.model.InvoiceListJsonData;
import com.myshipment.model.InvoiceTrackingParams;
import com.myshipment.model.ReportParams;



public interface ISeaExportInvoiceListService {

	public InvoiceListJsonData getSeaExportInvoiceList(InvoiceTrackingParams req);
}
