/*
 * @author nazib
 */
package com.myshipment.service;
import com.myshipment.model.InvoiceRouteModel;

public interface IinvoiceRouteService {
	
	public InvoiceRouteModel routeInformation(String destination, String orderNumber) throws Exception;

}
