package com.myshipment.service;

import com.myshipment.model.RequestParams;
import com.myshipment.model.StuffingReportData;


public interface IStuffingReport {

	public StuffingReportData getStuffingDetail(RequestParams req);
}
