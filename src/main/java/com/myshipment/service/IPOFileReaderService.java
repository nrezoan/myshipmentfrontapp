package com.myshipment.service;
/*
 * @Ranjeet Kumar
 */
import java.util.List;

import com.myshipment.model.POFileInfo;

public interface IPOFileReaderService {

	public void processTheFileData(List<POFileInfo> fileInfoLst);
	public POFileInfo checkForDuplicateFile(String fileName);
}
