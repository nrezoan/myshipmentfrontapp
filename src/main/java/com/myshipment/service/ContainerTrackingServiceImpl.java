/**
*
*@author Ahmad Naquib
*/
package com.myshipment.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myshipment.dao.IContainerTrackingDAO;
import com.myshipment.model.ContainerInformation;
import com.myshipment.model.ContainerLocationDetails;
import com.myshipment.model.ContainerTrackingSummary;
import com.myshipment.model.ContainerTypeInformation;
import com.myshipment.util.DateUtil;

@Service
public class ContainerTrackingServiceImpl implements IContainerTrackingService {
	
	Logger logger = Logger.getLogger(ContainerTrackingServiceImpl.class);

	@Autowired
	IContainerTrackingDAO containerTrackingDao;
	@Override
	public List<ContainerLocationDetails> getContainerDetails(String containerNumber) throws Exception {
		
		List<ContainerLocationDetails> result = containerTrackingDao.getContainerDetailsData(containerNumber);
		List<ContainerLocationDetails> list = new ArrayList<ContainerLocationDetails>();
		
		for (ContainerLocationDetails containerLocationDetails : result) {
			containerLocationDetails.setDateTime(DateUtil.stringToDateTime(containerLocationDetails.getActivityDate()));
			list.add(containerLocationDetails);
		}
		return list;
	}

	@Override
	public List<ContainerTrackingSummary> getContainerSummary(String containerNumber) throws Exception {
		List<ContainerTrackingSummary> result = containerTrackingDao.getContainerSummaryData(containerNumber);
		List<ContainerTrackingSummary> list = new ArrayList<ContainerTrackingSummary>();
		
		for (ContainerTrackingSummary containerTrackingSummary : result) {
			containerTrackingSummary.setDateTime(DateUtil.stringToDateTime(containerTrackingSummary.getActivityDate()));
			list.add(containerTrackingSummary);
		}
		return list;
	}

	@Override
	public List<ContainerInformation> getContainerInfo(String containerNumber)
			throws Exception {
		List<ContainerInformation> result = containerTrackingDao.getContainerInfoData(containerNumber);
		return result;
	}

	@Override
	public List<ContainerTypeInformation> getContainerTypeInfo(String containerNumber) throws Exception {
		logger.info("Container Type info service Called...");
		List<ContainerTypeInformation> result = containerTrackingDao.getContainerTypeInfo(containerNumber);
		return result;
	}

}
