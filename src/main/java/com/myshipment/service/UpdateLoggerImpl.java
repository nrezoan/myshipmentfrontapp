package com.myshipment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myshipment.dao.IUpdateLoggerDAO;
import com.myshipment.model.CRMLogger;
import com.myshipment.model.ReportParams;

@Service
public class UpdateLoggerImpl implements IUpdateLogger {
	@Autowired
	IUpdateLoggerDAO updateLoggerDao;

	public IUpdateLoggerDAO getUpdateLoggerDao() {
		return updateLoggerDao;
	}

	public void setUpdateLoggerDao(IUpdateLoggerDAO updateLoggerDao) {
		this.updateLoggerDao = updateLoggerDao;
	}

	@Override
	public int getUpdateLogger(List<CRMLogger> crmLogger) {

		updateLoggerDao.getUpdateLogger(crmLogger);
		return 0;

	}

	@Override
	public List<CRMLogger> getLoggerHblWise(CRMLogger req) {

		return updateLoggerDao.getLoggerHblWise(req);
	}

}
