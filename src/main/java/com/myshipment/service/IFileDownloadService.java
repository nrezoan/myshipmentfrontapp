package com.myshipment.service;

import com.myshipment.dto.LoginDTO;
import com.myshipment.model.DirectBookingParams;

public interface IFileDownloadService {
	public void createAndDownloadExcel(DirectBookingParams bookingParams,String fileName,String dataDirectory);
	public void createAndDownloadDashboardExcel(LoginDTO loginDTO,String fileName,String dataDirectory);
}
