package com.myshipment.service;

import com.myshipment.model.InvoiceParam;
import com.myshipment.model.SexInvJsonData;



public interface IAirExportInvoiceDetailService {

	public SexInvJsonData getAirExportInvoiceDetail(InvoiceParam param);
}
