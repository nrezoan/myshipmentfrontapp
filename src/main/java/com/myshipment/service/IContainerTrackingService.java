/**
*
*@author Ahmad Naquib
*/
package com.myshipment.service;

import java.util.List;

import com.myshipment.model.ContainerInformation;
import com.myshipment.model.ContainerLocationDetails;
import com.myshipment.model.ContainerTrackingSummary;
import com.myshipment.model.ContainerTypeInformation;

public interface IContainerTrackingService {

	public List<ContainerInformation> getContainerInfo(String containerNumber) throws Exception;
	public List<ContainerLocationDetails> getContainerDetails(String containerNumber) throws Exception;
	public List<ContainerTrackingSummary> getContainerSummary(String containerNumber) throws Exception;
	public List<ContainerTypeInformation> getContainerTypeInfo(String containerNumber) throws Exception;
	
}
