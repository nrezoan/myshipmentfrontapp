package com.myshipment.service;

import com.myshipment.dto.LoginDTO;
import com.myshipment.model.BuyerWiseCBMdetails;
import com.myshipment.model.BuyerWiseGWTdetails;
import com.myshipment.model.BuyerWiseShipDetails;
import com.myshipment.model.BuyerWiseShipDetailsbyPOD;
import com.myshipment.model.BuywiseShipDetailsbyBLstatus;
import com.myshipment.model.LastNShipmentsJsonOutputData;
import com.myshipment.model.SOWiseShipmentSummaryJson;
import com.myshipment.model.ShipmentDetailsBuyerJson;
import com.myshipment.model.ShipmentDetailsShipperJson;
import com.myshipment.model.SowiseShipDetailsBuyer;
import com.myshipment.model.SowiseShipDetailsShipper;
import com.myshipment.model.SuppWiseShipSummaryJson;
import com.myshipment.model.SupplierWiseCBMDetailJson;
import com.myshipment.model.SupplierWiseGWTDetailJson;
import com.myshipment.model.SupplierWiseShipmentDetailJson;
import com.myshipment.model.SuppwiseShipDetailsbyBLstatus;
import com.myshipment.model.TopFiveShipBuyerJson;
import com.myshipment.model.TopFiveShipment;
import com.myshipment.util.MyShipApplicationException;


/**
 * @author Gufranur Rahman
 *
 */
public interface IDashboardService {
	
	//For Supplier
	public SOWiseShipmentSummaryJson soWiseShipSummaryForShipperService(String fromDate,String toDate,LoginDTO loginDto ) throws MyShipApplicationException;
	public TopFiveShipment topFiveShipmentService(String fromDate,String toDate,LoginDTO loginDTO) throws MyShipApplicationException;	
	public SuppwiseShipDetailsbyBLstatus blStatusBuyer(String fromDate,String toDate,String blStatus,LoginDTO loginDTO) throws MyShipApplicationException;
	public SuppWiseShipSummaryJson suppWiseShipSummaryForBuyerService(String fromDate,String toDate,LoginDTO loginDto ) throws MyShipApplicationException;
	public TopFiveShipBuyerJson topFiveShipmentServiceBuyer(String fromDate, String toDate, LoginDTO loginDTO) throws MyShipApplicationException;
	public BuywiseShipDetailsbyBLstatus blStatusShipper(String fromDate, String toDate, String blStatus,LoginDTO loginDTO) throws MyShipApplicationException;
	public BuyerWiseShipDetails totalShipDetShipperService(String fromDate, String toDate,LoginDTO loginDTO);
	public BuyerWiseCBMdetails totCbmDetailsShipperService(String fromDate, String toDate,LoginDTO loginDTO);
	public BuyerWiseGWTdetails totGwtDetailsShipperService(String fromDate, String toDate,LoginDTO loginDTO);
	public SupplierWiseShipmentDetailJson totalShipmentDetBuyerService(String fromDate, String toDate,LoginDTO loginDTO);
	public SupplierWiseCBMDetailJson totalCBMDetBuyerService(String fromDate, String toDate,LoginDTO loginDTO);
	public SupplierWiseGWTDetailJson totalGWTDetBuyerService(String fromDate, String toDate,LoginDTO loginDTO);
	public SowiseShipDetailsShipper sowiseShipDetailsShipperService(String fromDate,String toDate,String salesOrg,LoginDTO loginDTO);
	public BuyerWiseShipDetailsbyPOD buyerWiseShipDetailsByPodService(String fromDate,String toDate,String pod,LoginDTO loginDTO);
	public SowiseShipDetailsBuyer soWiseShipDetailsBuyerService(String fromDate,String toDate,String shipperNo,LoginDTO loginDTO);
	public SuppwiseShipDetailsbyBLstatus suppWiseShipDetailsByPOOService(String fromDate,String toDate,String poo,LoginDTO loginDTO);
	public ShipmentDetailsShipperJson shipDetailsShipperService(String buyerNo,LoginDTO loginDTO);
	public ShipmentDetailsBuyerJson shipDetailsBuyerService(String shipperNo,LoginDTO loginDTO);
	public LastNShipmentsJsonOutputData getRecentShipmentShipper(LoginDTO loginDTO,int noOfRecords);
	
	
}
