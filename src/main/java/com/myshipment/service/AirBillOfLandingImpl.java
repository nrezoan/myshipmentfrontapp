package com.myshipment.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myshipment.model.BillOfLandingDetails;
import com.myshipment.model.RequestParams;
import com.myshipment.model.UserDetails;
/*
 * @ Gufranur Rahman
 */
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;

@Service
public class AirBillOfLandingImpl implements IAirBillOfLanding{

	private Logger logger = Logger.getLogger(AirBillOfLandingImpl.class);
	@Autowired
	private RestService restService;

	public RestService getRestService() {
		return restService;
	}

	public void setRestService(RestService restService) {
		this.restService = restService;
	}


	
	@Override
	public BillOfLandingDetails getAirBillLandingDetail(RequestParams req ) {
		BillOfLandingDetails airBillDetail = null;
		StringBuffer url=new StringBuffer(RestUtil.AirBillOfLanding_DETAILS);
		airBillDetail = restService.postForObject(RestUtil.prepareUrlForService(url).toString(), req, BillOfLandingDetails.class);
		return airBillDetail;
	}

}
