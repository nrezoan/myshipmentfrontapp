package com.myshipment.service;

import com.myshipment.model.InvoiceListJsonData;
import com.myshipment.model.InvoiceTrackingParams;



public interface IAirExportInvoiceListService {

	public InvoiceListJsonData getAirExportInvoiceList(InvoiceTrackingParams req);
}
