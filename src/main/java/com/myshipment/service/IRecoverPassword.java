package com.myshipment.service;

import com.myshipment.model.BapiRet1;
import com.myshipment.model.DetailsEditParam;

public interface IRecoverPassword {

	public BapiRet1 getRecoveredPass(DetailsEditParam editParam);
}
