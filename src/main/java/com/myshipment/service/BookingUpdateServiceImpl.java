package com.myshipment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myshipment.model.DirectBookingParams;
import com.myshipment.model.DirectBookingUpdateJsonData;
import com.myshipment.model.OrderHeader;
import com.myshipment.model.OrderItem;
import com.myshipment.util.DateUtil;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;

@Service
public class BookingUpdateServiceImpl implements IBookingUpdateService{

	@Autowired
	private RestService restService;
	@Override
	public DirectBookingUpdateJsonData doBookingUpdate(DirectBookingParams directBookingParams) {
		
		OrderHeader orderHeader=directBookingParams.getOrderHeader();
		orderHeader.setCargoHandoverDate(orderHeader.getCargoHandoverDate()!=null && !orderHeader.getCargoHandoverDate().isEmpty()?DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getCargoHandoverDate()):"");
		orderHeader.setComInvDate(orderHeader.getComInvDate()!=null && !orderHeader.getComInvDate().isEmpty()?DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getComInvDate()):"");
		orderHeader.setDeptDate(orderHeader.getDeptDate()!=null && !orderHeader.getDeptDate().isEmpty()?DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getDeptDate()):"");
		orderHeader.setExpDate(orderHeader.getExpDate()!=null && !orderHeader.getExpDate().isEmpty()?DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getExpDate()):"");
		orderHeader.setLcExpiryDate(orderHeader.getLcExpiryDate()!=null &&!orderHeader.getLcExpiryDate().isEmpty()?DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getLcExpiryDate()):"");
		orderHeader.setLcTtPoDate(orderHeader.getLcTtPoDate()!=null && !orderHeader.getLcTtPoDate().isEmpty()?DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getLcTtPoDate()):"");
		orderHeader.setLoadingDate(orderHeader.getLoadingDate()!=null && !orderHeader.getLoadingDate().isEmpty()?DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getLoadingDate()):"");
		orderHeader.setShippingDate(orderHeader.getShippingDate()!=null && !orderHeader.getShippingDate().isEmpty()?DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderHeader.getShippingDate()):"");
		directBookingParams.setOrderHeader(orderHeader);
		List<OrderItem> lstOrderItem=directBookingParams.getOrderItemLst();
		
		for (OrderItem orderItem : lstOrderItem) {
			orderItem.setCommInvoiceDate(orderItem.getCommInvoiceDate()!=null && !orderItem.getCommInvoiceDate().isEmpty()?DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderItem.getCommInvoiceDate()):"");
			orderItem.setEtdDate(orderItem.getEtdDate()!=null && !orderItem.getEtdDate().isEmpty()?DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderItem.getEtdDate()):"");
			orderItem.setInDate(orderItem.getInDate()!=null && !orderItem.getInDate().isEmpty()?DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderItem.getInDate()):"");
			orderItem.setOutDate(orderItem.getOutDate()!=null && !orderItem.getOutDate().isEmpty()?DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderItem.getOutDate()):"");
			orderItem.setQcDate(orderItem.getQcDate()!=null && !orderItem.getQcDate().isEmpty()?DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderItem.getQcDate()):"");
			orderItem.setReceiveDate(orderItem.getReceiveDate()!=null && !orderItem.getReceiveDate().isEmpty()?DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderItem.getReceiveDate()):"");			
			orderItem.setReleaseDate(orderItem.getReleaseDate()!=null && !orderItem.getReleaseDate().isEmpty()?DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(orderItem.getReleaseDate()):"");
			orderItem.setVolumeWeight(orderItem.getTotalVolume()*166.67);
		}
		directBookingParams.setOrderItemLst(lstOrderItem);
		StringBuffer webServiceUrl = new StringBuffer(RestUtil.BOOKING_UPDATE);
		DirectBookingUpdateJsonData bookingDisplayJsonOutputData = restService.postForObject(RestUtil.prepareUrlForService(webServiceUrl).toString(), directBookingParams, DirectBookingUpdateJsonData.class);
		return bookingDisplayJsonOutputData;
	}	

}
