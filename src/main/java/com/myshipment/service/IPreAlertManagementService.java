/**
*
*@author Ahmad Naquib
*/
package com.myshipment.service;

import java.util.List;
import java.util.Set;

import com.myshipment.dto.LoginDTO;
import com.myshipment.model.DashboardInfoBox;
import com.myshipment.model.MyshipmentTrackHeaderBean;
import com.myshipment.model.PreAlertBL;
import com.myshipment.model.PreAlertHBL;

public interface IPreAlertManagementService {

	/**
	 * Returns the status of database transaction (failed-0, success-1)
	 *
	 * @param 
	 *            (Saves HBL(after a successful booking) with related information)
	 * @return
	 */
	public int savePreAlertHBL(String hblNumber, String supplierId, String buyerId, String agentId, String distributionChannel, String division, String salesOrg) throws Exception;
	
	/**
	 * Returns the status of database transaction (failed-0, success-1)
	 *
	 * @param 
	 *            (Updates invoice packing list (stores file path and status of IPL))
	 * @return
	 */
	public int updatePreAlertDocument(PreAlertHBL preAlertHBL) throws Exception;
	
	/**
	 * Returns the status of database transaction (failed-0, success-1)
	 *
	 * @param 
	 *            (Save MBL and file path or updates MBL file location path)
	 * @return
	 */
	public int updatePreAlertMBLDocument(PreAlertHBL preAlertHBL, List<MyshipmentTrackHeaderBean> trackHeaderBean) throws Exception;
	
	/**
	 * Returns List of Pre-Alerts(HBL)
	 *
	 * @param Supplier Id----------INCOMPLETE-----------search with other parameters(dist, div and org)
	 *            
	 * @return
	 */
	
	public int updateMblDownloadStatus(PreAlertHBL preAlertHBL) throws Exception;
	
	public List<PreAlertHBL> getPreAlertByHBL(String supplierId) throws Exception;
	//Search by HBL number(Single HBL/HAWB)
	public PreAlertHBL getPreAlertBySingleHBL(PreAlertHBL preAlertHbl, String myshipmentUid) throws Exception;
	public List<PreAlertHBL> getPreAlertForDownload(String myshipmentUID, PreAlertHBL preAlertHbl) throws Exception;
	public List<PreAlertHBL> getPreAlertBuyerWiseList(PreAlertHBL preAlertHbl, String status) throws Exception;
	public PreAlertHBL checkPreAlertAuth(PreAlertHBL preAlertHBL) throws Exception;
	public List<PreAlertHBL> getMBlWisePreAlert(String myshipmentUid, PreAlertHBL preAlertHbl) throws Exception;
	
	public PreAlertBL getPreAlertInfo(LoginDTO loginDTO, DashboardInfoBox dashboardInfoBox, List<PreAlertHBL>preAlertHblByBuyerAgentList) throws Exception;
	
	public Set<String> getListOfHBLToGenerateNonNego(PreAlertHBL preAlertHBL, List<MyshipmentTrackHeaderBean> trackHeaderBean) throws Exception;

}
