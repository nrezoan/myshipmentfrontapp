/**
*
*@author Ahmad Naquib
*/
package com.myshipment.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myshipment.dao.ICompanySelectionDAO;
import com.myshipment.dto.LoginDTO;
import com.myshipment.dto.SalesOrgTree;
import com.myshipment.model.CompcodeListBean;
import com.myshipment.model.DefaultCompany;
import com.myshipment.model.IntelligentCompanyPercentageUsage;
import com.myshipment.model.IntelligentCompanyUsage;

@Service
public class CompanySelectionServiceImpl implements ICompanySelectionService {

	private Logger logger = Logger.getLogger(CompanySelectionServiceImpl.class);
	
	@Autowired
	private ICompanySelectionDAO companySelectionDAO;
	
	@Override
	public List<IntelligentCompanyUsage> getCompanyUsageList(String userCode) throws Exception {
		logger.info(this.getClass() + ": Service for Company Usage List called");
		return companySelectionDAO.getCompanyUsageList(userCode);
	}

	@Override
	public List<IntelligentCompanyPercentageUsage> getCompanyUsagePercentageList(LoginDTO loginDTO) {
		logger.info(this.getClass() + ": Company Percentage List called");
		List<IntelligentCompanyUsage> companyUsageList = new ArrayList<IntelligentCompanyUsage>();
		String userCode = loginDTO.getLoggedInUserName();
		try {
			companyUsageList = getCompanyUsageList(userCode);
		} catch (Exception e) {
			logger.info(this.getClass() + ": Error Occured fetching company usage list from database");
			e.printStackTrace();
		}
		double sum = 0;
		List<IntelligentCompanyPercentageUsage> companyUsagePercentageListTemp = new ArrayList<IntelligentCompanyPercentageUsage>();
		Map<String, SalesOrgTree> salesOrgMap = loginDTO.getMpSalesOrgTree();
		for(IntelligentCompanyUsage intelligentCompanyUsage : companyUsageList) {
			IntelligentCompanyPercentageUsage companyPercentageUsage = new IntelligentCompanyPercentageUsage();
			
			SalesOrgTree salesOrgTree = salesOrgMap.get(intelligentCompanyUsage.getCompany());
			if(salesOrgTree != null) {
				sum = sum + Integer.parseInt(intelligentCompanyUsage.getTotalHit());
				companyPercentageUsage.setCompanyName(salesOrgTree.getSalesOrgText());
				companyPercentageUsage.setCompanyCode(intelligentCompanyUsage.getCompany());
				companyPercentageUsage.setDistributionChannel(intelligentCompanyUsage.getDistributionChannel());
				companyPercentageUsage.setDivision(intelligentCompanyUsage.getDivision());
				companyPercentageUsage.setTotalHit(intelligentCompanyUsage.getTotalHit());
				
				companyUsagePercentageListTemp.add(companyPercentageUsage);
			}
			
			
		}
		List<IntelligentCompanyPercentageUsage> companyUsagePercentageList = new ArrayList<IntelligentCompanyPercentageUsage>();
		for (IntelligentCompanyPercentageUsage intelligentCompanyPercentageUsage : companyUsagePercentageListTemp) {
			double percentage = (Integer.parseInt(intelligentCompanyPercentageUsage.getTotalHit()) / sum) * 100;
			
			intelligentCompanyPercentageUsage.setTotalHitPercentage(percentage);
			companyUsagePercentageList.add(intelligentCompanyPercentageUsage);
		}
		
		return companyUsagePercentageList;
	}
	
	@Override
	public DefaultCompany getDefaultCompany(String userCode) throws Exception {
		
		logger.info(this.getClass() + " : Get selected Default Company Service called");
		
		return companySelectionDAO.getDefaultCompany(userCode);
	}

	@Override
	public DefaultCompany getActiveDefaultCompany(String userCode) throws Exception {
		
		logger.info(this.getClass() + " : Get selected Default Company Service called");
		DefaultCompany defaultCompany = getDefaultCompany(userCode);
		if(defaultCompany != null) {
			if(defaultCompany.getActive().equals("0")) {
				logger.info(this.getClass() + " : No active default company for " + userCode);
				defaultCompany = null;
			} else if(defaultCompany.getActive().equals("1")) {
				logger.info(this.getClass() + " : FOUND - Active default company for " + userCode);
			} else {
				logger.error(this.getClass() + " : Error occured, NULL(neither 0 / nor 1) active state for " + userCode);
			}
		} else {
			logger.error(this.getClass() + " : Default Company Result NULL for " + userCode);
		}
		return defaultCompany;
	}

	@Override
	public DefaultCompany getDefaultCompanyResult(LoginDTO loginDTO) {
		String userCode = loginDTO.getLoggedInUserName();
		DefaultCompany defaultCompany = null;
		try {
			defaultCompany = getDefaultCompany(userCode);
		} catch (Exception e) {
			logger.info(this.getClass() + " : Error occured while fetching default company data from database");
			e.printStackTrace();
		}
		if(defaultCompany == null) {
			defaultCompany = new DefaultCompany();
		}
		Map<String, SalesOrgTree> salesOrgMap = loginDTO.getSalesOrgDivisionWise();
		SalesOrgTree salesOrgTree = salesOrgMap.get(defaultCompany.getPreferredCompanyId());
		if(salesOrgTree != null) {
			defaultCompany.setPreferredCompanyName(salesOrgTree.getSalesOrgText());
		}
		
		return defaultCompany;
	}

	@Override
	public int saveDefaultCompany(DefaultCompany defaultCompany) throws Exception {
		DefaultCompany defaultCompanyResult = getDefaultCompany(defaultCompany.getUserId());
		int status = 0;
		if(defaultCompanyResult == null) {
			status = companySelectionDAO.saveDefaultCompany(defaultCompany);
		} else {
			status = companySelectionDAO.updateDefaultCompany(defaultCompany);
		}
		return status;
	}

	@Override
	public int activateDefaultCompany(DefaultCompany defaultCompany) throws Exception {
		return companySelectionDAO.setActiveDefaultCompany(defaultCompany);
	}

}
