package com.myshipment.service;

import com.myshipment.model.LineItemObjectModel;

public interface IDocparserLineItemService {
	public LineItemObjectModel[] fetchDocument(String documentInfo) throws Exception;

}
