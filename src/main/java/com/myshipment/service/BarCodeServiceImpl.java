package com.myshipment.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.myshipment.model.BarCodeImage;

@Service
public class BarCodeServiceImpl implements IBarCodeService {

	private Logger logger = Logger.getLogger(BarCodeServiceImpl.class);
	private static final String CODE_128_OUTPUT_IMAGE_NAME = "code128";

	BitMatrix bitMatrix;

	@Override
	public BarCodeImage generateCode128toImage(String codeToGenerate, String pathOfImage, String imageType, int width,
			int height) throws WriterException, FileNotFoundException, IOException {
		logger.debug("generateCode128toImage()start  ");
		bitMatrix = new QRCodeWriter().encode(codeToGenerate, BarcodeFormat.QR_CODE, width, height);
		// QRCodeWriter qrCodeWriter = new QRCodeWriter();
		// bitMatrix = qrCodeWriter.encode(QRCodeName, BarcodeFormat.QR_CODE,
		// fileHeight, fileWidth, hashMap);
		OutputStream outputStream;
		File file = new File(BarCodeServiceImpl.CODE_128_OUTPUT_IMAGE_NAME + "." + imageType);
		outputStream = new FileOutputStream(file);

		MatrixToImageWriter.writeToStream(bitMatrix, imageType, outputStream);
		outputStream.close();
		InputStream inputStream = new FileInputStream(file);

		byte[] bytes = new byte[(int) file.length()];
		inputStream.read(bytes);
		inputStream.close();
		BarCodeImage barCodeImage = new BarCodeImage();
		barCodeImage.setFileName(file.getName());
		barCodeImage.setImageFile(bytes);

		byte[] encodeBase64 = Base64.encodeBase64(bytes);
		String base64Encoded = new String(encodeBase64, "UTF-8");
		barCodeImage.setBase64Image(base64Encoded);
		return barCodeImage;

	}

	/*
	 * private String getConfigPropertyValue(String key) { String value=null;
	 * Properties properties=new Properties(); try { InputStream inputStream=new
	 * FileInputStream(new File(BarCodeServiceImpl.CONFIG_FILE_NAME));
	 * 
	 * properties.load(inputStream); value= properties.getProperty(key); } catch
	 * (FileNotFoundException e) { // TODO Auto-generated catch block
	 * e.printStackTrace(); } catch (IOException e) { // TODO Auto-generated
	 * catch block e.printStackTrace(); } return value; }
	 */

}
