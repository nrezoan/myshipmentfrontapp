package com.myshipment.service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.myshipment.dao.IUploadPODataDAO;
import com.myshipment.dto.FileDTO;
import com.myshipment.dto.LoginDTO;
import com.myshipment.dto.SupplierDetailsDTO;
import com.myshipment.model.ApprovedPurchaseOrder;
import com.myshipment.model.HeaderAndLineitemData;
import com.myshipment.model.HeaderPOData;
import com.myshipment.model.MaterialsDto;
import com.myshipment.model.POFileDTO;
import com.myshipment.model.SegPurchaseOrder;
import com.myshipment.model.ZemailJsonData;
import com.myshipment.model.ZemailParams;
import com.myshipment.util.CommonConstant;
import com.myshipment.util.RestUtil;
import com.myshipment.util.SessionUtil;
import com.sun.mail.util.MailSSLSocketFactory;

@Service
public class UploadPODataService implements IUploadPODataService {

	final static Logger logger = Logger.getLogger(UploadPODataService.class);

	@Autowired
	private IUploadPODataDAO uploadPODataDAO;
	
	String supplierName = null;

	@Override
	public List<SegPurchaseOrder> processData(MultipartFile file, FileDTO fileData) {
		logger.info("Inside method UploadPODataService.processData");

		List<Long> poIdLst = null;
		List<SegPurchaseOrder> segPurchaseOrderList = null;
		List<POFileDTO> poFileDTOLst = new ArrayList<POFileDTO>();

		Set<String> poNoSet = new HashSet<String>();
		List<String> poNoLst = new ArrayList<String>();

		try {
			ByteArrayInputStream bis = new ByteArrayInputStream(file.getBytes());

			// Create Workbook instance for xlsx/xls file input stream
			Workbook workbook = null;
			if (file.getOriginalFilename().toLowerCase().endsWith("xlsx")) {
				logger.info("File type is xlsx.");
				workbook = new XSSFWorkbook(bis);
			} else if (file.getOriginalFilename().toLowerCase().endsWith("xls")) {
				logger.info("File type is xls.");
				workbook = new HSSFWorkbook(bis);
			}

			// Get the number of sheets in the xlsx file
			int numberOfSheets = workbook.getNumberOfSheets();

			for (int i = 0; i < numberOfSheets; i++) {

				// Get the nth sheet from the workbook
				Sheet sheet = workbook.getSheetAt(i);

				// every sheet has rows, iterate over them
				Iterator<Row> rowIterator = sheet.iterator();
				while (rowIterator.hasNext()) {

					// Get the row object
					Row row = rowIterator.next();
					if (row.getRowNum() == 0)
						continue;
					POFileDTO poDto = new POFileDTO();

					// Every row has columns, get the column iterator and iterate over them
					Iterator<Cell> cellIterator = row.cellIterator();

					while (cellIterator.hasNext()) {
						// Get the Cell object
						Cell nextCell = cellIterator.next();
						int columnIndex = (nextCell.getColumnIndex() + 1);
						if (columnIndex == 1) {
							if ((getCellValue(nextCell) == null) || (getCellValue(nextCell).toString().equals(""))) {
								break;
							}
						}
						// check the cell type and process accordingly
						switch (columnIndex) {
						case 1:
							poDto.setVc_po_no((String) getCellValue(nextCell));
							// Adding code for change
							poNoSet.add(poDto.getVc_po_no());
							poNoLst.add(poDto.getVc_po_no());
							// End of code change
							break;
						case 2:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							// DataFormatter df = new DataFormatter();
							// String value = df.formatCellValue(cell);
							poDto.setNu_client_code((String) getStringCellValue(nextCell));
							break;
						case 3:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setVc_product_no((String) getCellValue(nextCell));
							break;
						case 4:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setVc_sku_no((String) getCellValue(nextCell));
							break;
						case 5:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setVc_style_no((String) getCellValue(nextCell));
							break;
						case 6:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setVc_article_no((String) getCellValue(nextCell));
							break;
						case 7:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setVc_color((String) getCellValue(nextCell));
							break;
						case 8:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setVc_size((String) getCellValue(nextCell));
							break;
						case 9:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setVc_pol((String) getCellValue(nextCell));
							break;
						case 10:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setVc_pod((String) getCellValue(nextCell));
							break;
						case 11:
							// nextCell.setCellType(Cell.CELL_TYPE_STRING);
							try {
								if (nextCell.getCellType() == nextCell.CELL_TYPE_NUMERIC) {
									poDto.setNu_no_pcs_ctns(nextCell.getNumericCellValue());
								}
							} catch (Exception ex) {
								poDto.setNu_no_pcs_ctns(nextCell.getNumericCellValue());
							}
							break;
						case 12:
							try {
								nextCell.setCellType(Cell.CELL_TYPE_STRING);
								if ((String) getCellValue(nextCell) == null || (String) getCellValue(nextCell) == "") {
									return null;
								}
								poDto.setVc_commodity((String) getCellValue(nextCell));
							} catch (Exception ex) {

							}
							break;
						case 13:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setDt_etd(getCellValue(nextCell).toString());
							break;
						case 14:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setVc_tot_pcs((getCellValue(nextCell).equals("") ? 0.0
									: Double.parseDouble((String) getCellValue(nextCell))));
							break;
						case 15:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setVc_quan((getCellValue(nextCell).equals("") ? 0.0
									: Double.parseDouble((String) getCellValue(nextCell))));
							break;
						case 16:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setVc_qua_uom((String) getCellValue(nextCell));
							break;
						case 17:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setNu_length((getCellValue(nextCell).equals("") ? 0.0
									: Double.parseDouble((String) getCellValue(nextCell))));
							break;
						case 18:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setNu_width((getCellValue(nextCell).equals("") ? 0.0
									: Double.parseDouble((String) getCellValue(nextCell))));
							break;
						case 19:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setNu_height((getCellValue(nextCell).equals("") ? 0.0
									: Double.parseDouble((String) getCellValue(nextCell))));
							break;
						case 20:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setVc_in_hcm((String) getCellValue(nextCell));
							break;
						case 21:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setVc_cbm_sea((String) getCellValue(nextCell));
							break;
						case 22:
							// nextCell.setCellType(Cell.CELL_TYPE_STRING);
							try {
								poDto.setVc_volume(Double.toString((double) getCellValue(nextCell)));
							} catch (Exception ex) {
								poDto.setVc_volume("0");
							}
							break;
						case 23:
							try {
								nextCell.setCellType(Cell.CELL_TYPE_STRING);
								poDto.setVc_gw_car((String) getCellValue(nextCell));
							} catch (Exception ex) {
								poDto.setVc_gw_car("0");
							}
							break;
						case 24:
							// nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setVc_gr_wt((String) getCellValue(nextCell));
							break;
						case 25:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setVc_nw_car((String) getCellValue(nextCell));
							break;
						case 26:
							// nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setVc_nt_wt((String) getCellValue(nextCell));
							break;
						case 27:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setVc_ref_field1((String) getCellValue(nextCell));
							break;
						case 28:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setVc_ref_field2((String) getCellValue(nextCell));
							break;
						case 29:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setVc_ref_field3((String) getCellValue(nextCell));
							break;
						case 30:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setVc_ref_field4((String) getCellValue(nextCell));
							break;
						case 31:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setVc_ref_field5((String) getCellValue(nextCell));
							break;
						case 32:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setVc_hs_code((String) getCellValue(nextCell));
							break;
						case 33:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setVc_qc_dt((String) getCellValue(nextCell));
							break;
						case 34:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setVc_rel_dt((String) getCellValue(nextCell));
							break;
						case 35:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setSales_org((String) getCellValue(nextCell));
							break;
						case 36:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setSap_quotation((String) getCellValue(nextCell));
							break;
						case 37:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setFile_upload_date((String) getCellValue(nextCell));
							break;
						case 38:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setVc_buyer(getCellValue(nextCell).toString());
							break;
						case 39:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setVc_buy_house(getCellValue(nextCell).toString());
							break;
						case 40:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setFile_name((String) getCellValue(nextCell));
							break;
						case 41:
							nextCell.setCellType(Cell.CELL_TYPE_STRING);
							poDto.setVc_division((String) getCellValue(nextCell));
							break;
						}
					}
					if (poDto.getVc_po_no() != null && !poDto.getVc_po_no().equals(""))
						poFileDTOLst.add(poDto);
				}

			}

			HeaderAndLineitemData headerAndLineitemData = prepareDataForHeaderAndLineitems(poNoSet, poFileDTOLst,
					poNoLst);
			headerAndLineitemData.setUniquePoNo(poNoSet);
			segPurchaseOrderList = uploadPODataDAO.insertDataInPurchaseOrderAndSegregatedTable(headerAndLineitemData,
					fileData);

			logger.info("Method UploadPODataService.processData ends.");

			return segPurchaseOrderList;
		} catch (Exception ex) {
			ex.printStackTrace();

			logger.debug("Exception Occurred : ", ex);
		} finally {
			try {
				return segPurchaseOrderList;
			} catch (Exception e) {
				e.printStackTrace();
				logger.debug("Exception Occurred : ", e);
			}
		}
		return segPurchaseOrderList;

	}

	private Object getCellValue(Cell cell) {
		if (cell == null)
			return "";
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_STRING: {
			System.out.println("Cell value: " + cell.getStringCellValue());
			return cell.getStringCellValue() == null ? "" : cell.getStringCellValue();
		}
		case Cell.CELL_TYPE_BOOLEAN:
			return cell.getBooleanCellValue();

		case Cell.CELL_TYPE_NUMERIC:
			double val = cell.getNumericCellValue();
			return Double.toString(val) == null ? "0" : Double.toString(val);

		case Cell.CELL_TYPE_BLANK:
			return cell.getStringCellValue() == null ? "0" : cell.getStringCellValue();
		}

		return null;
	}

	private Object getStringCellValue(Cell cell) {
		if (cell == null)
			return "";
		System.out.println("Cell value: " + cell.getStringCellValue());
		return cell.getStringCellValue() == null ? "" : cell.getStringCellValue();
	}

	private HeaderAndLineitemData prepareDataForHeaderAndLineitems(Set<String> poNos, List<POFileDTO> poFileDTOLst,
			List<String> poNoLst) {
		HeaderAndLineitemData headerAndLineitemData = new HeaderAndLineitemData();
		Map<String, POFileDTO> lineItems = new HashMap<String, POFileDTO>();
		// Map<String, List<SegPurchaseOrder>> lineItems = new HashMap<String,
		// List<SegPurchaseOrder>>();
		// Map<String, List<HeaderPOData>> headerItems = new HashMap<String,
		// List<HeaderPOData>>();
		Map<String, HeaderPOData> headerItem = new HashMap<String, HeaderPOData>();
		// List<POFileDTO> lineItemsOfAPo = new ArrayList<POFileDTO>();
		// List<SegPurchaseOrder> lineItemsOfAPo = new ArrayList<SegPurchaseOrder>();

		for (String poNo : poNos) {

			HeaderPOData headerPOData = createHeaderTableData(poNo, poFileDTOLst);
			headerItem.put(poNo, headerPOData);
		}

		headerAndLineitemData.setHeaderItem(headerItem);
		headerAndLineitemData.setLineItems(poFileDTOLst);

		return headerAndLineitemData;
	}

	private int noOfOccurrenceOfThePo(List<String> posNo, String po) {
		return Collections.frequency(posNo, po);
	}

	private HeaderPOData createHeaderTableData(String poNo, List<POFileDTO> lineItemsOfAPo) {
		HeaderPOData headerPOData = new HeaderPOData();
		if (lineItemsOfAPo != null && lineItemsOfAPo.size() > 0) {
			for (POFileDTO lineItem : lineItemsOfAPo) {
				if (poNo.equalsIgnoreCase(lineItem.getVc_po_no())) {
					headerPOData.setVc_po_no(lineItem.getVc_po_no());
					headerPOData.setNu_client_code(lineItem.getNu_client_code());
					// headerPOData.setCompany_code(lineItemsOfAPo.get(0));
					headerPOData.setVc_hs_code(lineItem.getVc_hs_code());
					headerPOData.setVc_buyer(lineItem.getVc_buyer());
					headerPOData.setVc_buy_house(lineItem.getVc_buy_house());
					headerPOData.setSales_org(lineItem.getSales_org());
					headerPOData.setSupplier_code(lineItem.getNu_client_code());
					headerPOData.setCurrentDate(getCurrentDate());
				}

			}
		}

		return headerPOData;
	}

	private Date getCurrentDate() {
		Date currentDate = new Date();

		return currentDate;
	}

	@Override
	public List<SegPurchaseOrder> getUploadedData() {
		Date date = new Date();
		DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		String currentDate = dateFormat.format(date);
		List<SegPurchaseOrder> uploadedData = uploadPODataDAO.getUploadedRecords(currentDate);

		return uploadedData;
	}

	@Override
	public List<List<SegPurchaseOrder>> getCurrentlyUploadedDataThroughFileUpload(List<Long> poIdLst) {

		List<List<SegPurchaseOrder>> uploadedDataLst = uploadPODataDAO
				.getCurrentlyUploadedRecordThroughFileUpload(poIdLst);
		return uploadedDataLst;
	}
	
	
	/*Sanjana Khan*/

	@Override
	public List<ApprovedPurchaseOrder> savePOGenerationInfo(List<Map<String, Object>> POGenerationInfo,
			Map<String, String> shipper, LoginDTO loginDto) {

		List<ApprovedPurchaseOrder> appPOList = new ArrayList<ApprovedPurchaseOrder>();
		List<ApprovedPurchaseOrder> approvedPOList = new ArrayList<ApprovedPurchaseOrder>();
		if (null != POGenerationInfo) {
			// List<Object> lstPackingList=null;
			for (Iterator iterator = POGenerationInfo.iterator(); iterator.hasNext();) {
				// OrderItem item = null;
				ApprovedPurchaseOrder appPO = null;

				Map<String, Object> map = (Map<String, Object>) iterator.next();

				List<Object> lstObject = (List) map.get("itemDetail");

				/*
				 * List<Object> lstExtraObject = (List) map.get("itemExtra");
				 * lstPackingList=(List)map.get("packinklist");
				 */
				/*
				 * if(null!=lstObject && null!=lstExtraObject) item=new OrderItem();
				 */

				if (null != lstObject)
					appPO = new ApprovedPurchaseOrder();
				if (null != lstObject)
					for (Iterator iterator2 = lstObject.iterator(); iterator2.hasNext();) {
						Map<String, Object> itemMap = (Map<String, Object>) iterator2.next(); // per line item
						// appPO = null;

						for (Map.Entry<String, String> shp : shipper.entrySet()) {

							if (shp.getValue().equals(itemMap.get("SupplierNo"))) {
								supplierName = (String) shp.getValue();
								appPO.setNu_client_code((String) shp.getKey());
								break;
							}

						}

						appPO.setVc_po_no((String) itemMap.get("PONumber"));
						appPO.setVc_gr_wt(
								(String) itemMap.get("GrossWeight") == null || itemMap.get("GrossWeight").equals("")
										? "0"
										: (String) itemMap.get("GrossWeight"));
						appPO.setNu_length((Double.parseDouble(
								(String) itemMap.get("CartonLength") == null || itemMap.get("CartonLength").equals("")
										? "0"
										: (String) itemMap.get("CartonLength"))));
						appPO.setNu_hieght(Double.parseDouble(
								(String) itemMap.get("CartonHeight") == null || itemMap.get("CartonHeight").equals("")
										? "0"
										: (String) itemMap.get("CartonHeight")));
						appPO.setNu_width(Double.parseDouble(
								(String) itemMap.get("CartonWidth") == null || itemMap.get("CartonWidth").equals("")
										? "0"
										: (String) itemMap.get("CartonWidth")));
						appPO.setVc_tot_pcs(Integer.parseInt(
								(String) itemMap.get("TotalPcs") == null || itemMap.get("TotalPcs").equals("") ? "0"
										: (String) itemMap.get("TotalPcs")));
						appPO.setVc_quan(Integer.parseInt(
								(String) itemMap.get("TotalCarton") == null || itemMap.get("TotalCarton").equals("")
										? "0"
										: (String) itemMap.get("TotalCarton")));
						appPO.setVc_cbm_sea(
								(String) itemMap.get("TotalCBM") == null || itemMap.get("TotalCBM").equals("") ? ("0")
										: (String) itemMap.get("TotalCBM"));
						appPO.setVc_hs_code((String) itemMap.get("HSCode"));
						appPO.setVc_style_no((String) itemMap.get("Style"));
						appPO.setVc_size((String) itemMap.get("Size"));
						appPO.setVc_sku_no((String) itemMap.get("ProductCode"));
						appPO.setVc_article_no((String) itemMap.get("ProjectNo"));
						appPO.setVc_color((String) itemMap.get("Color"));
						appPO.setVc_nt_wt(
								(String) itemMap.get("NetWeight") == null || itemMap.get("NetWeight").equals("") ? "0"
										: (String) itemMap.get("NetWeight"));
						appPO.setVc_commodity((String) itemMap.get("MaterialNo"));
						appPO.setVc_buyer(loginDto.getLoggedInUserName());

						appPO.setVc_in_hcm((String) itemMap.get("CartonMeasurementUnit"));
						appPO.setSales_org(loginDto.getSalesOrgSelected());
						appPO.setBooking_status("P");
						appPO.setVc_division(loginDto.getDivisionSelected());

						System.out.println("POmgmtService SavePOGenerationInfo");

					}

				if (appPO != null) {
					System.out.println("lineitems");
					appPOList.add(appPO);
				}

			}
		}
		// have to create object of dao
		approvedPOList = uploadPODataDAO.savePOGeneratedInfo(appPOList);
		
		//email send for notification 
		
		
		
		
		return approvedPOList;
	}

	@Override
	public String supplierName() {
		
		return supplierName;
	}

}
