package com.myshipment.service;

import com.myshipment.model.BillOfLandingDetails;
import com.myshipment.model.RequestParams;


public interface ISeaBillOfLanding {

	public BillOfLandingDetails getSeaBillLandingDetail(RequestParams req);
}
