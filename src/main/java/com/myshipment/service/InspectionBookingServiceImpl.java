package com.myshipment.service;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.myshipment.controller.InspectionBooking;
import com.myshipment.mappers.inspectionMapper;
import com.myshipment.model.InspectionBookingBean;
import com.myshipment.model.POList;
import com.myshipment.util.DatabaseComponent;
import com.myshipment.util.SendMail;
@Service
public class InspectionBookingServiceImpl {
	@Autowired
   private inspectionMapper inspectMapper;
   private static final Logger logger = Logger.getLogger(InspectionBooking.class);

public void insertPOBooking(String scotaFileSubmission, String scotaFileSubmissionDate,
		InspectionBookingBean insBookingData) {
	if(scotaFileSubmission.equals("Y")){
		inspectMapper.insertPOBookingNew(scotaFileSubmission,scotaFileSubmissionDate,insBookingData);
	}
	else{
		inspectMapper.insertPOBookingNewElse(scotaFileSubmission,insBookingData);
		}
	
}

public int getCount(String poNumber) {
	return inspectMapper.getCount(poNumber);
}

public void updateFlag(String poNumber) {
	inspectMapper.updateFlag(poNumber);
}
public void sendMailToSuppliersAndInspectors(File fileToCreate,String emailAddresses,int checkAgain,InspectionBookingBean insBookingData,RedirectAttributes redirectAttributes) throws Exception {
		
		emailAddresses=inspectMapper.getInspectorMail(insBookingData.getMerchantDiser());
		System.out.println("the merchantdiser is"+emailAddresses);

		String email=inspectMapper.getSupplierMail(insBookingData.getSupplierId());
		logger.info(this.getClass().getName() +insBookingData.getSupplierId());
		logger.info(this.getClass().getName() +"the new email is "+email);
		if (email!=null) {
		emailAddresses=emailAddresses+(email.trim().length() > 0? "," + email : "");
		logger.info(this.getClass().getName() + "the merchantdiser is"+ emailAddresses);
		}
		
		if (emailAddresses.trim().length() > 0) {
			SendMail sendMail = new SendMail();

			sendMail.javaSendMail("itsupport@mghgroup.com", emailAddresses, "", "INSPECTION BOOKING", "Dear Sir,<br></br>Inspection Booking Done For following PO:" + insBookingData.getPoNumber() + ".<br></br> <br></br><br></br>NB: This is System Generated Email, Please don't Reply<br></br>Thanks,<br></br> MGH IT");
		}
		
		if (fileToCreate != null)
			fileToCreate.delete();
		redirectAttributes.addFlashAttribute("message", "Inspection booking placed successfully");
				
	
	
}
public List<String> getPO(String query, List<String> lstPortList)
{
	List<POList> POLists=inspectMapper.getPO(query);
	for(POList poList:POLists) {
		lstPortList.add(poList.getPo_no());
	}
return lstPortList;
}
	

}
