package com.myshipment.service;

import com.myshipment.model.BapiRet1;
import com.myshipment.model.DetailsEditParam;



public interface IAccountEdit {

	public BapiRet1 getAccountEditData(DetailsEditParam editParam);
}
