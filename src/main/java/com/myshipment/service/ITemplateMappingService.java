package com.myshipment.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.myshipment.model.PurchaseOrdersModel;
import com.myshipment.model.TemplateModel;

public interface ITemplateMappingService {

	public Boolean saveTemplateMapping(TemplateModel templateModel);
	public void updateTemplateMapping(TemplateModel templateModel);
	public TemplateModel getTemplate(String buyerId);
	public void savePoDetails(List<PurchaseOrdersModel> purchaseOrderList);
	public List<PurchaseOrdersModel> getPoDetails(String poNo,String buyer_id);
	public void updatePoDetails(List<PurchaseOrdersModel> purchaseOrderList);
	public void deletePO(String app_id);
}
