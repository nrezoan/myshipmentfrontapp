package com.myshipment.service;

import org.json.JSONException;
import org.springframework.web.multipart.MultipartFile;

public interface IDocparserService {
	public String convert(MultipartFile file) throws Exception;
	public String docparserFileUpload(String file);
	public String docparserFileRetrieve(String uploadedFileJson) throws JSONException;
}
