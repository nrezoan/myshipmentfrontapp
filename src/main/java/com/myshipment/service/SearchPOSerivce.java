package com.myshipment.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myshipment.dao.ISearchPoDAO;
import com.myshipment.dto.SearchPoDTO;
import com.myshipment.model.HeaderPOData;
import com.myshipment.model.SegPurchaseOrder;
/*
 * @ Ranjeet Kumar
 */
@Service
public class SearchPOSerivce implements ISearchPOSerivce{

	final static Logger logger = Logger.getLogger(SearchPOSerivce.class);
	
	@Autowired
	private ISearchPoDAO searchPoDAO;
	
	@Override
	public List<HeaderPOData> getPosBasedOnParameter(SearchPoDTO searchPoDto) {

	/*String pattern = "dd-MM-yyyy";
	    SimpleDateFormat format = new SimpleDateFormat(pattern);
	    try{
	    Date fromDate = format.parse(searchPoDto.getFromDate());
	    Date toDate = format.parse(searchPoDto.getToDate());
	    String fDate = format.format(fromDate);
	    String tDate = format.format(toDate);
	    searchPoDto.setFromDate(fDate);
	    searchPoDto.setToDate(tDate);
	    }catch(Exception e)
	    {
	    	e.printStackTrace();
	    }*/
		List<HeaderPOData> headerPoLst = new ArrayList<HeaderPOData>();
		
		
		if((searchPoDto.getPoNo() != null) && (searchPoDto.getFromDate() == null || searchPoDto.getFromDate().isEmpty()) && (searchPoDto.getToDate() == null) || searchPoDto.getToDate().isEmpty())
		{
			 headerPoLst = searchPoDAO.getPosBasedOnPoNo(searchPoDto);
			 
			 return headerPoLst;
		}

		if(((searchPoDto.getPoNo() != null) || (!searchPoDto.getPoNo().isEmpty())) && (searchPoDto.getFromDate() != null) && (searchPoDto.getToDate() == null || searchPoDto.getToDate().isEmpty()))
		{
			 headerPoLst = searchPoDAO.getPosBasedOnPoNoAndDate(searchPoDto);
			 
			 return headerPoLst;
		}
/*
		if((searchPoDto.getPoNo() == null) && (searchPoDto.getFromDate() != null) && (searchPoDto.getToDate() != null))
		{
			 headerPoLst = searchPoDAO.getPosBasedOnTodateAndFromdate(searchPoDto);
			 
			 return headerPoLst;
		}
*/
/*		if((searchPoDto.getPoNo() != null) && (searchPoDto.getFromDate() != null) && (searchPoDto.getToDate() != null))
		{
			 headerPoLst = searchPoDAO.getPosBasedOnTodateAndFromdate(searchPoDto);
			 
			 return headerPoLst;
		}
	
		if((searchPoDto.getPoNo() == null || searchPoDto.getPoNo().isEmpty()) && (searchPoDto.getFromDate() != null) && (searchPoDto.getToDate() != null))
		{
			 headerPoLst = searchPoDAO.getPosBasedOnTodateAndFromdate(searchPoDto);
			 
			 return headerPoLst;
		}
*/		
		
		
		return headerPoLst;
	}

	@Override
	public List<SegPurchaseOrder> getAllLineItemPo(String poNo) {
		
		List<SegPurchaseOrder> segOrders = searchPoDAO.getAllLineItemPoData(poNo);
		
		return segOrders;
	}

	@Override
	public List<SegPurchaseOrder> getLineItemPosBasedOnParameter(SearchPoDTO searchPoDto) 
	{
			
		List<SegPurchaseOrder> lineItems = new ArrayList<SegPurchaseOrder>();
		
		if((searchPoDto.getPoNo().isEmpty()) && (searchPoDto.getFromDate().isEmpty()) && (searchPoDto.getToDate().isEmpty()))
		{
			List<SegPurchaseOrder> segOrders = searchPoDAO.getAllDataForNoInput();
			 
			 return segOrders;
		}
		
		if((searchPoDto.getPoNo() == null || searchPoDto.getPoNo().isEmpty()) && (searchPoDto.getFromDate() != null || !searchPoDto.getFromDate().isEmpty()) && (searchPoDto.getToDate() != null || !searchPoDto.getToDate().isEmpty()))
		{
			lineItems = searchPoDAO.getPosBasedOnTodateAndFromdate(searchPoDto);
			 
			 return lineItems;
		}
		
		if((searchPoDto.getPoNo() != null && !searchPoDto.getPoNo().isEmpty()) && (searchPoDto.getFromDate() == null || searchPoDto.getFromDate().isEmpty()) && (searchPoDto.getToDate() == null) || searchPoDto.getToDate().isEmpty())
		{
			List<SegPurchaseOrder> segOrders = searchPoDAO.getAllLineItemPoData(searchPoDto.getPoNo());
			 
			 return segOrders;
		}
		
		if((searchPoDto.getPoNo() != null && !searchPoDto.getPoNo().isEmpty()) && (searchPoDto.getFromDate() != null && !searchPoDto.getFromDate().isEmpty()) && (searchPoDto.getToDate() != null) && !searchPoDto.getToDate().isEmpty())
		{
			//List<SegPurchaseOrder> segOrders = searchPoDAO.getAllLineItemPoData(searchPoDto.getPoNo());
			List<SegPurchaseOrder> segOrders = searchPoDAO.getPosBasedOnTodateAndFromdateAndPoNo(searchPoDto);
			 return segOrders;
		}
		
/*		if((searchPoDto.getPoNo() == null || searchPoDto.getPoNo().isEmpty()) && (searchPoDto.getFromDate() == null || searchPoDto.getFromDate().isEmpty()) && (searchPoDto.getToDate() == null) || searchPoDto.getToDate().isEmpty())
		{
			List<SegPurchaseOrder> segOrders = searchPoDAO.getAllDataForNoInput();
			 
			 return segOrders;
		}
*/		
		return lineItems;
	}

	@Override
	public Long updatePo(SegPurchaseOrder segPurchaseOrder) {
		
		Long id = searchPoDAO.updatePoBasedOnSegId(segPurchaseOrder);
		return id;
	}

}
