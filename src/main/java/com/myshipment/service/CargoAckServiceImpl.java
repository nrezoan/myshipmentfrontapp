package com.myshipment.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/*
 * @ Gufranur Rahman
 */
import com.myshipment.model.BillOfLandingDetails;
import com.myshipment.model.CargoAckReportJsonData;
import com.myshipment.model.ReportParams;
import com.myshipment.model.RequestParams;
import com.myshipment.model.ShippingOrderReportJsonData;
import com.myshipment.model.StuffingReportData;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;

@Service
public class CargoAckServiceImpl implements ICargoAckService{

	private Logger logger = Logger.getLogger(CargoAckServiceImpl.class);
	@Autowired
	private RestService restService;

	public RestService getRestService() {
		return restService;
	}

	public void setRestService(RestService restService) {
		this.restService = restService;
	}


	
	@Override
	public CargoAckReportJsonData getCargoAckDetail(ReportParams req) {
		CargoAckReportJsonData cargoAckData = null;
		StringBuffer url=new StringBuffer(RestUtil.CARGO_ACK);
		cargoAckData = restService.postForObject(RestUtil.prepareUrlForService(url).toString(), req, CargoAckReportJsonData.class);
		return cargoAckData;
	}


}
