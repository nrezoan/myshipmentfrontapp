package com.myshipment.service;

import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.myshipment.dto.FileDTO;
import com.myshipment.dto.LoginDTO;
import com.myshipment.model.ApprovedPurchaseOrder;
import com.myshipment.model.SegPurchaseOrder;
/*
 * @ Ranjeet Kumar
 */
public interface IUploadPODataService {

	public List<SegPurchaseOrder> processData(MultipartFile file, FileDTO fileData);
	
	//public List<Long> processBookedPOData(MultipartFile file, FileDTO fileData);
	
	public List<SegPurchaseOrder> getUploadedData();

	public List<List<SegPurchaseOrder>> getCurrentlyUploadedDataThroughFileUpload(List<Long> poIdLst);
	
	
	/* shammi */
	public List<ApprovedPurchaseOrder> savePOGenerationInfo(List<Map<String,Object>> listOfLineItem,Map<String, String> shipper, LoginDTO loginDto);
	
	public String supplierName();
}
