package com.myshipment.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myshipment.dto.LoginDTO;
import com.myshipment.model.DirectBookingParams;
import com.myshipment.model.OrderItem;
import com.myshipment.model.PoBookingPacklistColorBean;
import com.myshipment.model.PoBookingPacklistDetailBean;
import com.myshipment.model.PoBookingPacklistSizeBean;
import com.myshipment.model.ShipDetailsToExportParams;
import com.myshipment.model.ShipmentDetails;
import com.myshipment.model.ShipmentDetailsJson;
import com.myshipment.util.CommonConstant;
import com.myshipment.util.DateUtil;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;

@Service
public class FileDownloadService implements IFileDownloadService{

	@Autowired
	private RestService restService;
	
	public RestService getRestService() {
		return restService;
	}


	public void setRestService(RestService restService) {
		this.restService = restService;
	}


	@Override
	public void createAndDownloadExcel(DirectBookingParams directBookingParams,String fileName,String dataDirectory) {
		XSSFWorkbook xssfWorkbook=new XSSFWorkbook();
		
		
		List<OrderItem> lstOrderItem=directBookingParams.getOrderItemLst();
		createLineItem(lstOrderItem, xssfWorkbook);
		PoBookingPacklistDetailBean poBookingPacklistDetailBean=directBookingParams.getPoBookingPacklistDetailBean();
		if(null!=poBookingPacklistDetailBean)
			createPackingList(xssfWorkbook,poBookingPacklistDetailBean);
			try {
				FileOutputStream outputStream=new  FileOutputStream(new File(dataDirectory+File.separator+fileName));
				xssfWorkbook.write(outputStream);
				outputStream.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
		
	}
	
	private void createLineItem(List<OrderItem> lstOrderItem, XSSFWorkbook xSSFWorkbook)
	{
		XSSFSheet lineItem=xSSFWorkbook.createSheet("item_detail");
		lineItem.setDefaultColumnWidth(18);
		XSSFRow row=lineItem.createRow(0);
		
		//hamid
		CellStyle style = xSSFWorkbook.createCellStyle();
	    //style.setFillForegroundColor(IndexedColors.GREEN.getIndex());
	    //style.setFillPattern(CellStyle.SOLID_FOREGROUND);
	    Font font = xSSFWorkbook.createFont();
        font.setColor(IndexedColors.RED.getIndex());
        style.setFont(font);
        //end
		
		XSSFCell cell=row.createCell(0, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("PONumber");
		 cell.setCellStyle(style);
		 cell=row.createCell(1, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("TotalCarton");
		 cell.setCellStyle(style);
		 cell=row.createCell(2, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("TotalPcs");
		 cell.setCellStyle(style);
		 cell=row.createCell(3, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("CartonLength");
		 cell.setCellStyle(style);
		 cell=row.createCell(4, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("CartonWidth");
		 cell.setCellStyle(style);
		 cell=row.createCell(5, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("CartonHeight");
		 cell.setCellStyle(style);
		 cell=row.createCell(6, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("CartonMeasurementUnit");
		 cell.setCellStyle(style);
		 cell=row.createCell(7, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("TotalCBM");
		 cell.setCellStyle(style);
		 cell=row.createCell(8, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("GrossWeight");
		 cell.setCellStyle(style);
		 cell=row.createCell(9, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("HSCode");
		 cell.setCellStyle(style);
		 cell=row.createCell(10, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("MaterialNo");
		 cell.setCellStyle(style);
		 cell=row.createCell(11, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("ItemUoM");
		 cell=row.createCell(12, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Style");
		 cell=row.createCell(13, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Color");
		 cell=row.createCell(14, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Size");
		 cell=row.createCell(15, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Line_Pcs/Carton");
		 cell=row.createCell(16, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Line_GrossWt/Carton");
		 cell=row.createCell(17, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Line_NetWt/Carton");
		 cell=row.createCell(18, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("NetWeight");
		 cell=row.createCell(19, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("ProductCode");
		 cell=row.createCell(20, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("ProjectNo");
		 cell=row.createCell(21, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Line_CartonSlNo");
		 cell=row.createCell(22, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Line_CommInvNo");
		 cell=row.createCell(23, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Line_CommInvDate");
		 cell=row.createCell(24, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Line_QCDate");
		 cell=row.createCell(25, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Line_ReleaseDate");
		 cell=row.createCell(26, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Line_Description");
		 cell=row.createCell(27, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Line_ExtraDescription");
		 cell=row.createCell(28, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Line_ShippingMarks");
		 cell=row.createCell(29, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("PaymentCondition");
		 cell=row.createCell(30, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("OrderCurrency");
		 cell=row.createCell(31, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("UnitPrice");
		 cell=row.createCell(32, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Line_NetCost");
		 cell=row.createCell(33, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("ReferenceNo1");
		 cell=row.createCell(34, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Line_ReferenceNo2");
		 cell=row.createCell(35, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Line_ReferenceNo3");
		 cell=row.createCell(36, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Line_ReferenceNo4");
		 cell=row.createCell(37, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Line_ReferenceNo5");
		 
/*		 cell=row.createCell(38, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("VGBEL");
		 cell=row.createCell(39, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Release Date"); 
		 cell=row.createCell(40, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("QC Date");
		 cell=row.createCell(41, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Carton Ser. No");*/
		
		 
		 
		int rowCount = 1;
		for (Iterator iterator = lstOrderItem.iterator(); iterator.hasNext();) {
			OrderItem orderItem = (OrderItem) iterator.next();
			row = lineItem.createRow(rowCount);
			cell = row.createCell(0, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getPoNumber()); 				//0
			cell = row.createCell(1, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getCurtonQuantity());			//1
			cell = row.createCell(2, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getTotalPieces());				//2
			cell = row.createCell(3, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getCartonLength());				//3
			cell = row.createCell(4, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getCartonWidth());				//4
			cell = row.createCell(5, Cell.CELL_TYPE_STRING);	
			cell.setCellValue(orderItem.getCartonHeight());				//5
			cell = row.createCell(6, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getCartonUnit());				//6
			cell = row.createCell(7, Cell.CELL_TYPE_STRING);
			//cell.setCellValue(orderItem.getTotalCbm());			
			cell.setCellValue(orderItem.getTotalVolume());				//7
			cell = row.createCell(8, Cell.CELL_TYPE_STRING);	
			cell.setCellValue(orderItem.getGrossWeight());				//8
			cell = row.createCell(9, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getHsCode());					//9
			cell = row.createCell(10, Cell.CELL_TYPE_STRING);	
			cell.setCellValue(orderItem.getMaterial());					//10
			cell = row.createCell(11, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getMaterialUnit());				//11
			cell = row.createCell(12, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getStyleNo());					//12
			cell = row.createCell(13, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getColor());					//13
			cell = row.createCell(14, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getSizeNo());					//14
			cell = row.createCell(15, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getPcsPerCarton());				//15
			cell = row.createCell(16, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getGrossWeightPerCarton());		//16
			cell = row.createCell(17, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getNetWeightPerCarton());		//17
			cell = row.createCell(18, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getNetWeight());				//18
			cell = row.createCell(19, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getProductCode());				//19
			cell = row.createCell(20, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getProjectNo());				//20
			cell = row.createCell(21, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getCartonSerNo());				//21
			cell = row.createCell(22, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getCommInvoice());				//22
			cell = row.createCell(23, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getCommInvoiceDate());			//23
			cell = row.createCell(24, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getQcDate());					//24
			cell = row.createCell(25, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getReleaseDate());				//25
			cell = row.createCell(26, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getDescription());				//26
			cell = row.createCell(27, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getItemDescription());			//27
			cell = row.createCell(28, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getShippingMark());				//28
			cell = row.createCell(29, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getPayCond());					//29
			cell = row.createCell(30, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getCurrency());					//30
			cell = row.createCell(31, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getUnitPrice());				//31
			cell = row.createCell(32, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getNetCost());					//32
			cell = row.createCell(33, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getRefNo());					//33
			cell = row.createCell(34, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getReference1());				//34
			cell = row.createCell(35, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getReference3());				//35
			cell = row.createCell(36, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getReference4());				//36
			cell = row.createCell(39, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getReference5());				//37
			
/*			cell = row.createCell(40, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getQcDate());
			cell = row.createCell(41, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getCartonSerNo());	
			cell.setCellValue(orderItem.getPoNumber());*/
			
			rowCount++;
		}
	}
	
	
/*	private void createLineItem(List<OrderItem> lstOrderItem,XSSFWorkbook xSSFWorkbook)
	{
		XSSFSheet lineItem=xSSFWorkbook.createSheet("Line Item");
		lineItem.setDefaultColumnWidth(15);
		XSSFRow row=lineItem.createRow(0);
		XSSFCell cell=row.createCell(0, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("PO. Number");
		 cell=row.createCell(1, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Style Number");
		 cell=row.createCell(2, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Ref. No.");
		 cell=row.createCell(3, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Project No.");
		 cell=row.createCell(4, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Product Code");
		 cell=row.createCell(5, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Color.");
		 cell=row.createCell(6, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Size No.");
		 cell=row.createCell(7, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Total Pieces");
		 cell=row.createCell(8, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Carton Length");
		 cell=row.createCell(9, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Carton Width");
		 cell=row.createCell(10, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Carton Height");
		 cell=row.createCell(11, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Carton Unit");
		 cell=row.createCell(12, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Carton Quantity");
		 cell=row.createCell(13, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Total Gross Weight");
		 cell=row.createCell(14, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Total Net Weight");
		 cell=row.createCell(15, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Total CBM");
		 cell=row.createCell(16, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Unit");
		 cell=row.createCell(17, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Commodity");
		 cell=row.createCell(18, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Pay Conday Cond.");
		 cell=row.createCell(19, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Unit Price/Pcs");
		 cell=row.createCell(20, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Currency Unit");
		 cell=row.createCell(21, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Commercial Invoice.");
		 cell=row.createCell(22, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Commerical Invoice Date");
		 cell=row.createCell(23, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Net Cost");
		 cell=row.createCell(24, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Pcs/Carton");
		 cell=row.createCell(25, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Gross Weight/Carton");
		 cell=row.createCell(26, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Net Weight/Carton");
		 cell=row.createCell(27, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Reference 1");
		 cell=row.createCell(28, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Reference 2");
		 cell=row.createCell(29, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Reference 3");
		 cell=row.createCell(30, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Reference 4");
		 cell=row.createCell(31, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Reference 5");
		 cell=row.createCell(32, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Shipping Mark Short Description");
		 cell=row.createCell(33, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Description");
		 cell=row.createCell(34, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Item Description");
		 cell=row.createCell(35, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("HS Code.");
		 cell=row.createCell(36, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Posnr");
		 cell=row.createCell(37, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("VBELN");
		 cell=row.createCell(38, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("VGBEL");
		 cell=row.createCell(39, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Release Date"); 
		 cell=row.createCell(40, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("QC Date");
		 cell=row.createCell(41, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Carton Ser. No");
		
		 
		 
		int rowCount = 1;
		for (Iterator iterator = lstOrderItem.iterator(); iterator.hasNext();) {
			OrderItem orderItem = (OrderItem) iterator.next();
			row = lineItem.createRow(rowCount);
			cell = row.createCell(0, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getPoNumber());
			cell = row.createCell(1, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getStyleNo());
			cell = row.createCell(2, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getRefNo());
			cell = row.createCell(3, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getProjectNo());
			cell = row.createCell(4, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getProductCode());
			cell = row.createCell(5, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getColor());
			cell = row.createCell(6, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getSizeNo());
			cell = row.createCell(7, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getTotalPieces());
			cell = row.createCell(8, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getCartonLength());
			cell = row.createCell(9, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getCartonWidth());
			cell = row.createCell(10, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getCartonHeight());
			cell = row.createCell(11, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getCartonUnit());
			cell = row.createCell(12, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getCurtonQuantity());
			cell = row.createCell(13, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getGrossWeight());
			cell = row.createCell(14, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getNetWeight());
			cell = row.createCell(15, Cell.CELL_TYPE_STRING);
			//cell.setCellValue(orderItem.getTotalCbm());
			cell.setCellValue(orderItem.getTotalVolume());
			cell = row.createCell(16, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getUnit());
			cell = row.createCell(17, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getMaterial());
			cell = row.createCell(18, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getPayCond());
			cell = row.createCell(19, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getUnitPrice());
			cell = row.createCell(20, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getCurrency());
			cell = row.createCell(21, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getCommInvoice());
			cell = row.createCell(22, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getCommInvoiceDate());
			cell = row.createCell(23, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getNetCost());
			cell = row.createCell(24, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getPcsPerCarton());

			cell = row.createCell(25, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getGrossWeightPerCarton());
			cell = row.createCell(26, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getNetWeightPerCarton());
			cell = row.createCell(27, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getReference1());
			cell = row.createCell(28, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getReference2());
			cell = row.createCell(29, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getReference3());
			cell = row.createCell(30, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getReference4());
			cell = row.createCell(31, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getReference5());
			cell = row.createCell(32, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getShippingMark());
			cell = row.createCell(33, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getDescription());
			cell = row.createCell(34, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getItemDescription());
			cell = row.createCell(35, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getHsCode());
			cell = row.createCell(36, Cell.CELL_TYPE_STRING);
			if(null!=orderItem.getPosnerNumber())
			cell.setCellValue(orderItem.getPosnerNumber());
			cell = row.createCell(39, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getReleaseDate());
			cell = row.createCell(40, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getQcDate());
			cell = row.createCell(41, Cell.CELL_TYPE_STRING);
			cell.setCellValue(orderItem.getCartonSerNo());	
			cell.setCellValue(orderItem.getPoNumber());
			rowCount++;
		}
	}*/
	
	private void createPackingList(XSSFWorkbook xSSFWorkbook,PoBookingPacklistDetailBean poBookingPacklistDetailBean)
	{
		XSSFSheet packingList=xSSFWorkbook.createSheet("Packing List");
		
		packingList.setDefaultColumnWidth(15);
		Set<PoBookingPacklistColorBean> packlistColorBeans=new LinkedHashSet<PoBookingPacklistColorBean>(poBookingPacklistDetailBean.getPacklistColors()) ;
		PoBookingPacklistColorBean  poBookingPacklistColorBean=null;
		for (PoBookingPacklistColorBean poBookingPacklistColorBean1 : packlistColorBeans) {
			poBookingPacklistColorBean=poBookingPacklistColorBean1;
			break;
		}
		try {
			Set<PoBookingPacklistSizeBean> setPoBookingPacklistSizeBean = new LinkedHashSet<PoBookingPacklistSizeBean>(
					poBookingPacklistColorBean.getPacklistSizes());
			XSSFRow row = packingList.createRow(0);
			int i = 0;
			XSSFCell cell = row.createCell(i, Cell.CELL_TYPE_STRING);
			cell.setCellValue("Carton No");
			i++;
			cell = row.createCell(i, Cell.CELL_TYPE_STRING);
			cell.setCellValue("No. of Containers");
			i++;
			cell = row.createCell(i, Cell.CELL_TYPE_STRING);
			cell.setCellValue("Color");
			i++;
			for (Iterator iterator = setPoBookingPacklistSizeBean.iterator(); iterator.hasNext();) {
				PoBookingPacklistSizeBean poBookingPacklistSizeBean = (PoBookingPacklistSizeBean) iterator.next();
				cell = row.createCell(i, Cell.CELL_TYPE_STRING);
				cell.setCellValue(poBookingPacklistSizeBean.getSizeName());
				i++;
			}

			cell = row.createCell(i, Cell.CELL_TYPE_STRING);
			cell.setCellValue("Pcs per Carton.");
			i++;
			cell = row.createCell(i, Cell.CELL_TYPE_STRING);
			cell.setCellValue("Total Pcs.");
			i++;
			cell = row.createCell(i, Cell.CELL_TYPE_STRING);
			cell.setCellValue("Remarks");

			int rowNumber = 1;
			for (Iterator iterator = packlistColorBeans.iterator(); iterator.hasNext();) {
				PoBookingPacklistColorBean poBookingPacklistColorBean2 = (PoBookingPacklistColorBean) iterator.next();
				row = packingList.createRow(rowNumber);
				i = 0;
				if (rowNumber == 1) {
					cell = row.createCell(i, Cell.CELL_TYPE_STRING);
					if (poBookingPacklistDetailBean.getCtnSrlNo() != null)
						cell.setCellValue(poBookingPacklistDetailBean.getCtnSrlNo());
					else
						cell.setCellValue("");
					i++;
					cell = row.createCell(i, Cell.CELL_TYPE_STRING);
					if (poBookingPacklistDetailBean.getNoOfContainers() != null)
						cell.setCellValue(poBookingPacklistDetailBean.getNoOfContainers());
					else
						cell.setCellValue("");
					i++;
				} else {
					cell = row.createCell(i, Cell.CELL_TYPE_STRING);
					i++;
					cell = row.createCell(i, Cell.CELL_TYPE_STRING);
					i++;
				}
				cell = row.createCell(i, Cell.CELL_TYPE_STRING);
				cell.setCellValue(poBookingPacklistColorBean2.getColorName());
				i++;
				for (Iterator iterator2 = setPoBookingPacklistSizeBean.iterator(); iterator2.hasNext();) {
					PoBookingPacklistSizeBean poBookingPacklistSizeBean = (PoBookingPacklistSizeBean) iterator2.next();
					cell = row.createCell(i, Cell.CELL_TYPE_STRING);
					if (poBookingPacklistSizeBean.getNoOfPcs() != null)
						cell.setCellValue(poBookingPacklistSizeBean.getNoOfPcs());
					else
						cell.setCellValue("");
					i++;
				}
				cell = row.createCell(i, Cell.CELL_TYPE_STRING);
				if (poBookingPacklistColorBean2.getPcsPerCtn() != null)
					cell.setCellValue(poBookingPacklistColorBean2.getPcsPerCtn());
				else
					cell.setCellValue("");
				i++;
				cell = row.createCell(i, Cell.CELL_TYPE_STRING);
				if (poBookingPacklistColorBean2.getTotalPcs() != null)
					cell.setCellValue(poBookingPacklistColorBean2.getTotalPcs());
				else
					cell.setCellValue("");
				i++;
				cell = row.createCell(i, Cell.CELL_TYPE_STRING);
				if (poBookingPacklistColorBean2.getRemarks() != null)
					cell.setCellValue(poBookingPacklistColorBean2.getRemarks());
				else
					cell.setCellValue("");
				rowNumber++;
			}
			packingList.addMergedRegion(new CellRangeAddress(1, packlistColorBeans.size(), 0, 0));
			packingList.addMergedRegion(new CellRangeAddress(1, packlistColorBeans.size(), 1, 1));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}


	@Override
	public void createAndDownloadDashboardExcel(LoginDTO loginDTO, String fileName, String dataDirectory) {

		ShipDetailsToExportParams detailsToExportParams=new ShipDetailsToExportParams();
		detailsToExportParams.setBookingDateFrom(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(loginDTO.getDashBoardFromDate()));
		detailsToExportParams.setBookingDateTo(DateUtil.formatDD_MM_YYYY_to_YYYYMMDD(loginDTO.getDashBoardToDate()));
		detailsToExportParams.setDistChannel(loginDTO.getDisChnlSelected());
		detailsToExportParams.setDivision(loginDTO.getDivisionSelected());
		if(loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE1) || loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE2))
			detailsToExportParams.setBuyerNo(loginDTO.getLoggedInUserName());
		if(loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE1) || loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE2))
			detailsToExportParams.setShipperNo(loginDTO.getLoggedInUserName());
		StringBuffer serviceUrl=new StringBuffer(RestUtil.SHIP_DETAILS_TO_EXPORT);
		ShipmentDetailsJson shipmentDetailsJson =restService.postForObject(RestUtil.prepareUrlForService(serviceUrl).toString(), detailsToExportParams, ShipmentDetailsJson.class);
		XSSFWorkbook xssfWorkbook=new XSSFWorkbook();
		if(loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE1) || loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.BUYER_CODE2))
		{
			createExcelForBuyer(xssfWorkbook, shipmentDetailsJson);
		}
		else
			if(loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE1) || loginDTO.getAccgroup().equalsIgnoreCase(CommonConstant.SUPPLIER_CODE2))
			{
				createExcelForShipper(xssfWorkbook, shipmentDetailsJson);
			}
		
		
		
			try {
				FileOutputStream outputStream=new  FileOutputStream(new File(dataDirectory+File.separator+fileName));
				xssfWorkbook.write(outputStream);
				outputStream.close();
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
	}
	private void createExcelForBuyer(XSSFWorkbook xssfWorkbook,ShipmentDetailsJson shipmentDetailsJson)
	{
		XSSFSheet xssfSheet=xssfWorkbook.createSheet("Sheet 1");
		xssfSheet.setDefaultColumnWidth(15);
		XSSFRow row=xssfSheet.createRow(0);
		XSSFCell cell=row.createCell(0, Cell.CELL_TYPE_STRING);
		cell.setCellValue("BL.Number");
		 cell=row.createCell(1, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("BL Date");
		 cell=row.createCell(2, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Booking Date");
		 cell=row.createCell(3, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Shipper No.");
		 cell=row.createCell(4, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Shipper Name");
		 cell=row.createCell(5, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("POL. Code");
		 cell=row.createCell(6, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("POD. Code");
		 cell=row.createCell(7, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Gross Weight");
		 cell=row.createCell(8, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Charge Weight");
		 cell=row.createCell(9, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Total Volume");
		 cell=row.createCell(10, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("GR. Date");
		 cell=row.createCell(11, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Shipment Date");
		  
		 
		int rowCount = 1;
		List<ShipmentDetails> lstShipmentDetails=shipmentDetailsJson.getLstShipmentDetails();
		for (Iterator iterator = lstShipmentDetails.iterator(); iterator.hasNext();) {
			ShipmentDetails shipmentDetails = (ShipmentDetails) iterator.next();
			row = xssfSheet.createRow(rowCount);
			 cell=row.createCell(0, Cell.CELL_TYPE_STRING);
			cell.setCellValue(shipmentDetails.getBlNo());
			 cell=row.createCell(1, Cell.CELL_TYPE_STRING);
			 cell.setCellValue(DateUtil.YYYYMMDDToDD_MM_YYYY(shipmentDetails.getBldate()));
			 cell=row.createCell(2, Cell.CELL_TYPE_STRING);
			 cell.setCellValue(DateUtil.YYYYMMDDToDD_MM_YYYY(shipmentDetails.getBookingDate()));
			 cell=row.createCell(3, Cell.CELL_TYPE_STRING);
			 cell.setCellValue(shipmentDetails.getShipperNo());
			 cell=row.createCell(4, Cell.CELL_TYPE_STRING);
			 cell.setCellValue(shipmentDetails.getShipperName());
			 cell=row.createCell(5, Cell.CELL_TYPE_STRING);
			 cell.setCellValue(shipmentDetails.getPolCode());
			 cell=row.createCell(6, Cell.CELL_TYPE_STRING);
			 cell.setCellValue(shipmentDetails.getPodCode());
			 cell=row.createCell(7, Cell.CELL_TYPE_STRING);
			 cell.setCellValue(shipmentDetails.getGrossWeight());
			 cell=row.createCell(8, Cell.CELL_TYPE_STRING);
			 cell.setCellValue(shipmentDetails.getChargeWeight());
			 cell=row.createCell(9, Cell.CELL_TYPE_STRING);
			 cell.setCellValue(shipmentDetails.getTotVolume());
			 cell=row.createCell(10, Cell.CELL_TYPE_STRING);
			 cell.setCellValue(DateUtil.YYYYMMDDToDD_MM_YYYY(shipmentDetails.getGrDate()));
			 cell=row.createCell(11, Cell.CELL_TYPE_STRING);
			 cell.setCellValue(DateUtil.YYYYMMDDToDD_MM_YYYY(shipmentDetails.getShipmentDate()));
			rowCount++;
		}
		
	}
	private void createExcelForShipper(XSSFWorkbook xssfWorkbook,ShipmentDetailsJson shipmentDetailsJson)
	{
		XSSFSheet xssfSheet=xssfWorkbook.createSheet("Sheet 1");
		xssfSheet.setDefaultColumnWidth(15);
		XSSFRow row=xssfSheet.createRow(0);
		XSSFCell cell=row.createCell(0, Cell.CELL_TYPE_STRING);
		cell.setCellValue("BL.Number");
		 cell=row.createCell(1, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("BL Date");
		 cell=row.createCell(2, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Booking Date");
		 cell=row.createCell(3, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Buyer No.");
		 cell=row.createCell(4, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Buyer Name");
		 cell=row.createCell(5, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("POL. Code");
		 cell=row.createCell(6, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("POD. Code");
		 cell=row.createCell(7, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Gross Weight");
		 cell=row.createCell(8, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Charge Weight");
		 cell=row.createCell(9, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Total Volume");
		 cell=row.createCell(10, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("GR. Date");
		 cell=row.createCell(11, Cell.CELL_TYPE_STRING);
		 cell.setCellValue("Shipment Date");
		  
		 
		int rowCount = 1;
		List<ShipmentDetails> lstShipmentDetails=shipmentDetailsJson.getLstShipmentDetails();
		for (Iterator iterator = lstShipmentDetails.iterator(); iterator.hasNext();) {
			ShipmentDetails shipmentDetails = (ShipmentDetails) iterator.next();
			row = xssfSheet.createRow(rowCount);
			 cell=row.createCell(0, Cell.CELL_TYPE_STRING);
			cell.setCellValue(shipmentDetails.getBlNo());
			 cell=row.createCell(1, Cell.CELL_TYPE_STRING);
			 cell.setCellValue(DateUtil.YYYYMMDDToDD_MM_YYYY(shipmentDetails.getBldate()));
			 cell=row.createCell(2, Cell.CELL_TYPE_STRING);
			 cell.setCellValue(DateUtil.YYYYMMDDToDD_MM_YYYY(shipmentDetails.getBookingDate()));
			 cell=row.createCell(3, Cell.CELL_TYPE_STRING);
			 cell.setCellValue(shipmentDetails.getBuyerNo());
			 cell=row.createCell(4, Cell.CELL_TYPE_STRING);
			 cell.setCellValue(shipmentDetails.getBuyerName());
			 cell=row.createCell(5, Cell.CELL_TYPE_STRING);
			 cell.setCellValue(shipmentDetails.getPolCode());
			 cell=row.createCell(6, Cell.CELL_TYPE_STRING);
			 cell.setCellValue(shipmentDetails.getPodCode());
			 cell=row.createCell(7, Cell.CELL_TYPE_STRING);
			 cell.setCellValue(shipmentDetails.getGrossWeight());
			 cell=row.createCell(8, Cell.CELL_TYPE_STRING);
			 cell.setCellValue(shipmentDetails.getChargeWeight());
			 cell=row.createCell(9, Cell.CELL_TYPE_STRING);
			 cell.setCellValue(shipmentDetails.getTotVolume());
			 cell=row.createCell(10, Cell.CELL_TYPE_STRING);
			 cell.setCellValue(DateUtil.YYYYMMDDToDD_MM_YYYY(shipmentDetails.getGrDate()));
			 cell=row.createCell(11, Cell.CELL_TYPE_STRING);
			 cell.setCellValue(DateUtil.YYYYMMDDToDD_MM_YYYY(shipmentDetails.getShipmentDate()));
			rowCount++;
		}
	}

}
