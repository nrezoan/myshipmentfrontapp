package com.myshipment.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myshipment.model.BapiRet1;
import com.myshipment.model.DetailsEditParam;
import com.myshipment.util.RestService;
import com.myshipment.util.RestUtil;

@Service
public class AccountEditImpl implements IAccountEdit {
	
	private Logger logger = Logger.getLogger(AccountEditImpl.class);
	@Autowired
	private RestService restService;

	public RestService getRestService() {
		return restService;
	}

	public void setRestService(RestService restService) {
		this.restService = restService;
	}
	
	@Override
	public BapiRet1 getAccountEditData(DetailsEditParam editParam) {
		BapiRet1 editDetail = null;
		StringBuffer url=new StringBuffer(RestUtil.PASSWORD_CHANGE);
		editDetail = restService.postForObject(RestUtil.prepareUrlForService(url).toString(), editParam, BapiRet1.class);
		return editDetail;
	}


}
