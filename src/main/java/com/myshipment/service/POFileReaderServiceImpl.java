package com.myshipment.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.myshipment.dao.IPOFileReaderDAO;
import com.myshipment.model.POFileInfo;
/*
 * @Ranjeet Kumar
 */
@Service
public class POFileReaderServiceImpl implements IPOFileReaderService{

	@Autowired
	private IPOFileReaderDAO iPOFileReaderDAO;

	@Override
	public void processTheFileData(List<POFileInfo> fileInfoLst) {

		iPOFileReaderDAO.savePOFileData(fileInfoLst);
	}

	@Override
	public POFileInfo checkForDuplicateFile(String fileName) {
		if(fileName != null){
			POFileInfo poFileInfo = iPOFileReaderDAO.getFileInfoForDuplicateCheck(fileName);
			return poFileInfo;
		}

		return null;
	}

}
