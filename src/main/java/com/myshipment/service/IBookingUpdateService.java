package com.myshipment.service;

import com.myshipment.model.DirectBookingParams;
import com.myshipment.model.DirectBookingUpdateJsonData;

public interface IBookingUpdateService {
	
	public DirectBookingUpdateJsonData doBookingUpdate(DirectBookingParams directBookingParams);

}
