package com.myshipment.xmlpobean;

/*
 * @Ranjeet Kumar
 */
public class QteProduit {

	private String cod_pro_qte_produit;
	private String cod_cou_qte_produit;
	private String cod_tai_qte_produit;
	private String nbr_col;
	private String nbr_uvc;
	private String vol;
	private String pds_brt;
	private String cod_prt;
	
	public String getCod_pro_qte_produit() {
		return cod_pro_qte_produit;
	}
	public void setCod_pro_qte_produit(String cod_pro_qte_produit) {
		this.cod_pro_qte_produit = cod_pro_qte_produit;
	}
	public String getCod_cou_qte_produit() {
		return cod_cou_qte_produit;
	}
	public void setCod_cou_qte_produit(String cod_cou_qte_produit) {
		this.cod_cou_qte_produit = cod_cou_qte_produit;
	}
	public String getCod_tai_qte_produit() {
		return cod_tai_qte_produit;
	}
	public void setCod_tai_qte_produit(String cod_tai_qte_produit) {
		this.cod_tai_qte_produit = cod_tai_qte_produit;
	}
	public String getNbr_col() {
		return nbr_col;
	}
	public void setNbr_col(String nbr_col) {
		this.nbr_col = nbr_col;
	}
	public String getNbr_uvc() {
		return nbr_uvc;
	}
	public void setNbr_uvc(String nbr_uvc) {
		this.nbr_uvc = nbr_uvc;
	}
	public String getVol() {
		return vol;
	}
	public void setVol(String vol) {
		this.vol = vol;
	}
	public String getPds_brt() {
		return pds_brt;
	}
	public void setPds_brt(String pds_brt) {
		this.pds_brt = pds_brt;
	}
	public String getCod_prt() {
		return cod_prt;
	}
	public void setCod_prt(String cod_prt) {
		this.cod_prt = cod_prt;
	}
	
	
}
