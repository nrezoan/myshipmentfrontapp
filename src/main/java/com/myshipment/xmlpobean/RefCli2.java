package com.myshipment.xmlpobean;

/*
 * @Ranjeet Kumar
 */
public class RefCli2 {

	private String cod_cou_ref_cli_2;
	private String cod_tai_ref_cli_2;
	private String ref_cli_3;
	
	public String getCod_cou_ref_cli_2() {
		return cod_cou_ref_cli_2;
	}
	public void setCod_cou_ref_cli_2(String cod_cou_ref_cli_2) {
		this.cod_cou_ref_cli_2 = cod_cou_ref_cli_2;
	}
	public String getCod_tai_ref_cli_2() {
		return cod_tai_ref_cli_2;
	}
	public void setCod_tai_ref_cli_2(String cod_tai_ref_cli_2) {
		this.cod_tai_ref_cli_2 = cod_tai_ref_cli_2;
	}
	public String getRef_cli_3() {
		return ref_cli_3;
	}
	public void setRef_cli_3(String ref_cli_3) {
		this.ref_cli_3 = ref_cli_3;
	}
	
	
}
