package com.myshipment.xmlpobean;

/*
 * @Ranjeet Kumar
 */
public class LibLangue {

	private String cod_lan;
	private String lib_sds;
	private String lib_dsc;
	private String lib_dsc2;
	
	public String getCod_lan() {
		return cod_lan;
	}
	public void setCod_lan(String cod_lan) {
		this.cod_lan = cod_lan;
	}
	public String getLib_sds() {
		return lib_sds;
	}
	public void setLib_sds(String lib_sds) {
		this.lib_sds = lib_sds;
	}
	public String getLib_dsc() {
		return lib_dsc;
	}
	public void setLib_dsc(String lib_dsc) {
		this.lib_dsc = lib_dsc;
	}
	public String getLib_dsc2() {
		return lib_dsc2;
	}
	public void setLib_dsc2(String lib_dsc2) {
		this.lib_dsc2 = lib_dsc2;
	}
	
	
}
