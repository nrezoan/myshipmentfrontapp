package com.myshipment.xmlpobean;

import java.util.ArrayList;
import java.util.List;

/*
 * @Ranjeet Kumar
 */
public class Tranche {

	private String num_tra;
	private String dat_etd;
	private String nbr_jou_tra;
	private String dat_eta;
	private String cod_trs_cli;
	private String cod_ean_cli;
	private String cod_vil_prc2;
	private String cod_trs_dep;
	private String cod_mod_tra_tranche;
	private String cod_inc_tranche;
	private String cod_vil_inc_tranche;
	private List<QteProduit> qteProduit = new ArrayList<QteProduit>();
	
	public String getNum_tra() {
		return num_tra;
	}
	public void setNum_tra(String num_tra) {
		this.num_tra = num_tra;
	}
	public String getDat_etd() {
		return dat_etd;
	}
	public void setDat_etd(String dat_etd) {
		this.dat_etd = dat_etd;
	}
	public String getNbr_jou_tra() {
		return nbr_jou_tra;
	}
	public void setNbr_jou_tra(String nbr_jou_tra) {
		this.nbr_jou_tra = nbr_jou_tra;
	}
	public String getDat_eta() {
		return dat_eta;
	}
	public void setDat_eta(String dat_eta) {
		this.dat_eta = dat_eta;
	}
	public String getCod_trs_cli() {
		return cod_trs_cli;
	}
	public void setCod_trs_cli(String cod_trs_cli) {
		this.cod_trs_cli = cod_trs_cli;
	}
	public String getCod_ean_cli() {
		return cod_ean_cli;
	}
	public void setCod_ean_cli(String cod_ean_cli) {
		this.cod_ean_cli = cod_ean_cli;
	}
	public String getCod_vil_prc2() {
		return cod_vil_prc2;
	}
	public void setCod_vil_prc2(String cod_vil_prc2) {
		this.cod_vil_prc2 = cod_vil_prc2;
	}
	public String getCod_trs_dep() {
		return cod_trs_dep;
	}
	public void setCod_trs_dep(String cod_trs_dep) {
		this.cod_trs_dep = cod_trs_dep;
	}
	public String getCod_mod_tra_tranche() {
		return cod_mod_tra_tranche;
	}
	public void setCod_mod_tra_tranche(String cod_mod_tra_tranche) {
		this.cod_mod_tra_tranche = cod_mod_tra_tranche;
	}
	public String getCod_inc_tranche() {
		return cod_inc_tranche;
	}
	public void setCod_inc_tranche(String cod_inc_tranche) {
		this.cod_inc_tranche = cod_inc_tranche;
	}
	public String getCod_vil_inc_tranche() {
		return cod_vil_inc_tranche;
	}
	public void setCod_vil_inc_tranche(String cod_vil_inc_tranche) {
		this.cod_vil_inc_tranche = cod_vil_inc_tranche;
	}
	public List<QteProduit> getQteProduit() {
		return qteProduit;
	}
	public void setQteProduit(List<QteProduit> qteProduit) {
		this.qteProduit = qteProduit;
	}
	
	
}
