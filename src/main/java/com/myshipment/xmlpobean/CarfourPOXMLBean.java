package com.myshipment.xmlpobean;

import java.util.ArrayList;
import java.util.List;

/*
 * @Ranjeet Kumar
 */
public class CarfourPOXMLBean {

	private String cod_soc;
	private String cod_eta;
	private String num_uat;
	private String cod_sta;
	private String cod_mod_tra;
	private String cod_typ_tra;
	//fields for fournisseur tag starts
	//private List<Fournisseur> fournisseurList = new ArrayList<Fournisseur>();
	Fournisseur fournisseur;
/*	private String cod_trs_exp;
	private String lib_trs1;
	private String lib_trs2;
	private String adr1;
	private String adr2;
	private String adr3;
	private String cod_pay;
	private String cod_ean_fou;*/
	//fields for fournisseur tag ends
	private String cod_mod_reg;
	private String cod_dev;
	private String cod_trs_dst;
	private String cod_trs_tra;
	private String cod_trs_qua;
	private String flg_sta_cqu;
	private String num_int_tra;
	private String cod_int;
	private String cod_trs_agt;
	private String flg_ctr_mat;
	private String cod_sai;
	private String lib_sai;
	private String cod_tys;
	private String lib_tys;
	private String num_int_typ;
	private String ref_cli_1;
	private String num_cmd_cli;
	private String cod_inc;
	private String cod_inc_vte;
	private String cod_vil_inc;
	private String cod_vil_prc1;
	private String dat_con;
	//Start of lProducts_list tag
	//Start of produit tag
	private List<Produit> produitLst = new ArrayList<Produit>();
/*	private String cod_pro;
	private String cod_pro_pro;
	private String lib_pro;
	private String cod_emb;
	private String pcb;
	private String sou_pcb;
	private String cod_pay_produit;
	private String cod_ray;
	private String mnt_ach;
	private String tau_ach;
	private String mnt_ces;
	private String mnt_vte;
	private String cod_dev_vte;
	private String tau_vte;
	private String mnt_vte_soc;
	private String col_lon;
	private String col_lar;
	private String col_hau;
	private String col_vol;
	private String col_pds;
	private String ref_fou;
	private String pct_dou;
	private String typ_pro;
	private String lib_pro_lng;
	private String dan_cla;
	private String dan_onu;
	private String cod_pal;
	private String pal_lon;
	private String pal_lar;
	private String pal_hau;
	private String pal_vol;
	private String pal_pds;
	private String col_cou;
	private String cou_pal;
	private String col_pal;
	private String cod_unl_ach;*/
	//Start of tag Liste_lib_langue(lProducts_list->produit->Liste_lib_langue)
	//Start of tag lib_langue(lProducts_list->produit->Liste_lib_langue->lib_langue)
/*	private String cod_lan;
	private String lib_sds;
	private String lib_dsc;
	private String lib_dsc2;*/
	//End of tag lib_langue
//	private String Liste_desc_colis;
	//Start of tag Liste_EAN(lProducts_list->produit->Liste_EAN)
	//Start of tag EAN(lProducts_list->produit->Liste_EAN->EAN)
/*	private String cod_cou;
	private String cod_tai;
	private String cod_ean;
	private String cod_ean_mas;
	private String cod_ean_inn;*/
	//End of EAN(lProducts_list->produit->Liste_EAN->EAN)
	//Start of tag Liste_NDP(lProducts_list->produit->Liste_NDP)
	//No values inside. just tag starts and ends
	//Start of tag Liste_ref_cli(lProducts_list->produit->Liste_ref_cli)
	//Start of tag ref_cli_2(lProducts_list->produit->Liste_ref_cli->ref_cli_2)
/*	private String cod_cou_ref_cli_2;
	private String cod_tai_ref_cli_2;
	private String ref_cli_3;*/
	//Start of tag Liste_box(lProducts_list->produit->Liste_box)
	//No values inside. just tag starts and ends
	//Start of tag liste_tranches
	//Start of tag tranche
	private List<Tranche> trancheLst = new ArrayList<Tranche>();
/*	private String num_tra;
	private String dat_etd;
	private String nbr_jou_tra;
	private String dat_eta;
	private String cod_trs_cli;
	private String cod_ean_cli;
	private String cod_vil_prc2;
	private String cod_trs_dep;
	private String cod_mod_tra_tranche;
	private String cod_inc_tranche;
	private String cod_vil_inc_tranche;*/
	//Start of the tag Liste_qte_produit(liste_tranches->tranche->Liste_qte_produit)
	//Start of the tag qte_produit(liste_tranches->tranche->Liste_qte_produit->qte_produit)
/*	private String cod_pro_qte_produit;
	private String cod_cou_qte_produit;
	private String cod_tai_qte_produit;
	private String nbr_col;
	private String nbr_uvc;
	private String vol;
	private String pds_brt;
	private String cod_prt;*/
	//End of the tag qte_produit(liste_tranches->tranche->Liste_qte_produit->qte_produit)
	//Start of the tag liste_liv_magasin
	//Start of the tag date_livraiso(liste_liv_magasin->date_livraiso)
	private List<DateLivraiso> dateLivraisoLst = new ArrayList<DateLivraiso>();
/*	private String cod_pro_date_livraiso;
	private String num_tra_date_livraiso;
	private String nbr_col_date_livraiso;
	private String qte;
	private String dat_etd_lim;
	private String 	dat_ent_cal;
	private String dat_ent_sou;
	private String dat_mag_cal;
	private String dat_mag_sou;
	private String Liste_catalogue;*/
	//End of the tag date_livraiso(liste_liv_magasin->date_livraiso)
	//Start of the tag liste_docu
	//Start of the tag documents(liste_docu->documents)
	private List<Documents> DocumentsLst = new ArrayList<Documents>();
/*	private String cod_pro_documents;
	private String cod_ndp;
	private String cod_amf;
	private String cod_doc;
	private String nbr_ori;
	private String nbr_cop;*/
	//End of the tag documents(liste_docu->documents)
	
	
	public String getCod_soc() {
		return cod_soc;
	}
	public void setCod_soc(String cod_soc) {
		this.cod_soc = cod_soc;
	}
	public String getCod_eta() {
		return cod_eta;
	}
	public void setCod_eta(String cod_eta) {
		this.cod_eta = cod_eta;
	}
	public String getNum_uat() {
		return num_uat;
	}
	public void setNum_uat(String num_uat) {
		this.num_uat = num_uat;
	}
	public String getCod_sta() {
		return cod_sta;
	}
	public void setCod_sta(String cod_sta) {
		this.cod_sta = cod_sta;
	}
	public String getCod_mod_tra() {
		return cod_mod_tra;
	}
	public void setCod_mod_tra(String cod_mod_tra) {
		this.cod_mod_tra = cod_mod_tra;
	}
	public String getCod_typ_tra() {
		return cod_typ_tra;
	}
	public void setCod_typ_tra(String cod_typ_tra) {
		this.cod_typ_tra = cod_typ_tra;
	}
/*	public String getCod_trs_exp() {
		return cod_trs_exp;
	}
	public void setCod_trs_exp(String cod_trs_exp) {
		this.cod_trs_exp = cod_trs_exp;
	}
	public String getLib_trs1() {
		return lib_trs1;
	}
	public void setLib_trs1(String lib_trs1) {
		this.lib_trs1 = lib_trs1;
	}
	public String getLib_trs2() {
		return lib_trs2;
	}
	public void setLib_trs2(String lib_trs2) {
		this.lib_trs2 = lib_trs2;
	}
	public String getAdr1() {
		return adr1;
	}
	public void setAdr1(String adr1) {
		this.adr1 = adr1;
	}
	public String getAdr2() {
		return adr2;
	}
	public void setAdr2(String adr2) {
		this.adr2 = adr2;
	}
	public String getAdr3() {
		return adr3;
	}
	public void setAdr3(String adr3) {
		this.adr3 = adr3;
	}
	public String getCod_pay() {
		return cod_pay;
	}
	public void setCod_pay(String cod_pay) {
		this.cod_pay = cod_pay;
	}
	public String getCod_ean_fou() {
		return cod_ean_fou;
	}
	public void setCod_ean_fou(String cod_ean_fou) {
		this.cod_ean_fou = cod_ean_fou;
	}*/
	public String getCod_mod_reg() {
		return cod_mod_reg;
	}
	public void setCod_mod_reg(String cod_mod_reg) {
		this.cod_mod_reg = cod_mod_reg;
	}
	public String getCod_dev() {
		return cod_dev;
	}
	public void setCod_dev(String cod_dev) {
		this.cod_dev = cod_dev;
	}
	public String getCod_trs_dst() {
		return cod_trs_dst;
	}
	public void setCod_trs_dst(String cod_trs_dst) {
		this.cod_trs_dst = cod_trs_dst;
	}
	public String getCod_trs_tra() {
		return cod_trs_tra;
	}
	public void setCod_trs_tra(String cod_trs_tra) {
		this.cod_trs_tra = cod_trs_tra;
	}
	public String getCod_trs_qua() {
		return cod_trs_qua;
	}
	public void setCod_trs_qua(String cod_trs_qua) {
		this.cod_trs_qua = cod_trs_qua;
	}
	public String getFlg_sta_cqu() {
		return flg_sta_cqu;
	}
	public void setFlg_sta_cqu(String flg_sta_cqu) {
		this.flg_sta_cqu = flg_sta_cqu;
	}
	public String getNum_int_tra() {
		return num_int_tra;
	}
	public void setNum_int_tra(String num_int_tra) {
		this.num_int_tra = num_int_tra;
	}
	public String getCod_int() {
		return cod_int;
	}
	public void setCod_int(String cod_int) {
		this.cod_int = cod_int;
	}
	public String getCod_trs_agt() {
		return cod_trs_agt;
	}
	public void setCod_trs_agt(String cod_trs_agt) {
		this.cod_trs_agt = cod_trs_agt;
	}
	public String getFlg_ctr_mat() {
		return flg_ctr_mat;
	}
	public void setFlg_ctr_mat(String flg_ctr_mat) {
		this.flg_ctr_mat = flg_ctr_mat;
	}
	public String getCod_sai() {
		return cod_sai;
	}
	public void setCod_sai(String cod_sai) {
		this.cod_sai = cod_sai;
	}
	public String getLib_sai() {
		return lib_sai;
	}
	public void setLib_sai(String lib_sai) {
		this.lib_sai = lib_sai;
	}
	public String getCod_tys() {
		return cod_tys;
	}
	public void setCod_tys(String cod_tys) {
		this.cod_tys = cod_tys;
	}
	public String getLib_tys() {
		return lib_tys;
	}
	public void setLib_tys(String lib_tys) {
		this.lib_tys = lib_tys;
	}
	public String getNum_int_typ() {
		return num_int_typ;
	}
	public void setNum_int_typ(String num_int_typ) {
		this.num_int_typ = num_int_typ;
	}
	public String getRef_cli_1() {
		return ref_cli_1;
	}
	public void setRef_cli_1(String ref_cli_1) {
		this.ref_cli_1 = ref_cli_1;
	}
	public String getNum_cmd_cli() {
		return num_cmd_cli;
	}
	public void setNum_cmd_cli(String num_cmd_cli) {
		this.num_cmd_cli = num_cmd_cli;
	}
	public String getCod_inc() {
		return cod_inc;
	}
	public void setCod_inc(String cod_inc) {
		this.cod_inc = cod_inc;
	}
	public String getCod_inc_vte() {
		return cod_inc_vte;
	}
	public void setCod_inc_vte(String cod_inc_vte) {
		this.cod_inc_vte = cod_inc_vte;
	}
	public String getCod_vil_inc() {
		return cod_vil_inc;
	}
	public void setCod_vil_inc(String cod_vil_inc) {
		this.cod_vil_inc = cod_vil_inc;
	}
	public String getCod_vil_prc1() {
		return cod_vil_prc1;
	}
	public void setCod_vil_prc1(String cod_vil_prc1) {
		this.cod_vil_prc1 = cod_vil_prc1;
	}
	public String getDat_con() {
		return dat_con;
	}
	public void setDat_con(String dat_con) {
		this.dat_con = dat_con;
	}
/*	public String getCod_pro() {
		return cod_pro;
	}
	public void setCod_pro(String cod_pro) {
		this.cod_pro = cod_pro;
	}
	public String getCod_pro_pro() {
		return cod_pro_pro;
	}
	public void setCod_pro_pro(String cod_pro_pro) {
		this.cod_pro_pro = cod_pro_pro;
	}
	public String getLib_pro() {
		return lib_pro;
	}
	public void setLib_pro(String lib_pro) {
		this.lib_pro = lib_pro;
	}
	public String getCod_emb() {
		return cod_emb;
	}
	public void setCod_emb(String cod_emb) {
		this.cod_emb = cod_emb;
	}
	public String getPcb() {
		return pcb;
	}
	public void setPcb(String pcb) {
		this.pcb = pcb;
	}
	public String getSou_pcb() {
		return sou_pcb;
	}
	public void setSou_pcb(String sou_pcb) {
		this.sou_pcb = sou_pcb;
	}
	public String getCod_pay_produit() {
		return cod_pay_produit;
	}
	public void setCod_pay_produit(String cod_pay_produit) {
		this.cod_pay_produit = cod_pay_produit;
	}
	public String getCod_ray() {
		return cod_ray;
	}
	public void setCod_ray(String cod_ray) {
		this.cod_ray = cod_ray;
	}
	public String getMnt_ach() {
		return mnt_ach;
	}
	public void setMnt_ach(String mnt_ach) {
		this.mnt_ach = mnt_ach;
	}
	public String getTau_ach() {
		return tau_ach;
	}
	public void setTau_ach(String tau_ach) {
		this.tau_ach = tau_ach;
	}
	public String getMnt_ces() {
		return mnt_ces;
	}
	public void setMnt_ces(String mnt_ces) {
		this.mnt_ces = mnt_ces;
	}
	public String getMnt_vte() {
		return mnt_vte;
	}
	public void setMnt_vte(String mnt_vte) {
		this.mnt_vte = mnt_vte;
	}
	public String getCod_dev_vte() {
		return cod_dev_vte;
	}
	public void setCod_dev_vte(String cod_dev_vte) {
		this.cod_dev_vte = cod_dev_vte;
	}
	public String getTau_vte() {
		return tau_vte;
	}
	public void setTau_vte(String tau_vte) {
		this.tau_vte = tau_vte;
	}
	public String getMnt_vte_soc() {
		return mnt_vte_soc;
	}
	public void setMnt_vte_soc(String mnt_vte_soc) {
		this.mnt_vte_soc = mnt_vte_soc;
	}
	public String getCol_lon() {
		return col_lon;
	}
	public void setCol_lon(String col_lon) {
		this.col_lon = col_lon;
	}
	public String getCol_lar() {
		return col_lar;
	}
	public void setCol_lar(String col_lar) {
		this.col_lar = col_lar;
	}
	public String getCol_hau() {
		return col_hau;
	}
	public void setCol_hau(String col_hau) {
		this.col_hau = col_hau;
	}
	public String getCol_vol() {
		return col_vol;
	}
	public void setCol_vol(String col_vol) {
		this.col_vol = col_vol;
	}
	public String getCol_pds() {
		return col_pds;
	}
	public void setCol_pds(String col_pds) {
		this.col_pds = col_pds;
	}
	public String getRef_fou() {
		return ref_fou;
	}
	public void setRef_fou(String ref_fou) {
		this.ref_fou = ref_fou;
	}
	public String getPct_dou() {
		return pct_dou;
	}
	public void setPct_dou(String pct_dou) {
		this.pct_dou = pct_dou;
	}
	public String getTyp_pro() {
		return typ_pro;
	}
	public void setTyp_pro(String typ_pro) {
		this.typ_pro = typ_pro;
	}
	public String getLib_pro_lng() {
		return lib_pro_lng;
	}
	public void setLib_pro_lng(String lib_pro_lng) {
		this.lib_pro_lng = lib_pro_lng;
	}
	public String getDan_cla() {
		return dan_cla;
	}
	public void setDan_cla(String dan_cla) {
		this.dan_cla = dan_cla;
	}
	public String getDan_onu() {
		return dan_onu;
	}
	public void setDan_onu(String dan_onu) {
		this.dan_onu = dan_onu;
	}
	public String getCod_pal() {
		return cod_pal;
	}
	public void setCod_pal(String cod_pal) {
		this.cod_pal = cod_pal;
	}
	public String getPal_lon() {
		return pal_lon;
	}
	public void setPal_lon(String pal_lon) {
		this.pal_lon = pal_lon;
	}
	public String getPal_lar() {
		return pal_lar;
	}
	public void setPal_lar(String pal_lar) {
		this.pal_lar = pal_lar;
	}
	public String getPal_hau() {
		return pal_hau;
	}
	public void setPal_hau(String pal_hau) {
		this.pal_hau = pal_hau;
	}
	public String getPal_vol() {
		return pal_vol;
	}
	public void setPal_vol(String pal_vol) {
		this.pal_vol = pal_vol;
	}
	public String getPal_pds() {
		return pal_pds;
	}
	public void setPal_pds(String pal_pds) {
		this.pal_pds = pal_pds;
	}
	public String getCol_cou() {
		return col_cou;
	}
	public void setCol_cou(String col_cou) {
		this.col_cou = col_cou;
	}
	public String getCou_pal() {
		return cou_pal;
	}
	public void setCou_pal(String cou_pal) {
		this.cou_pal = cou_pal;
	}
	public String getCol_pal() {
		return col_pal;
	}
	public void setCol_pal(String col_pal) {
		this.col_pal = col_pal;
	}
	public String getCod_unl_ach() {
		return cod_unl_ach;
	}
	public void setCod_unl_ach(String cod_unl_ach) {
		this.cod_unl_ach = cod_unl_ach;
	}
	public String getCod_lan() {
		return cod_lan;
	}
	public void setCod_lan(String cod_lan) {
		this.cod_lan = cod_lan;
	}
	public String getLib_sds() {
		return lib_sds;
	}
	public void setLib_sds(String lib_sds) {
		this.lib_sds = lib_sds;
	}
	public String getLib_dsc() {
		return lib_dsc;
	}
	public void setLib_dsc(String lib_dsc) {
		this.lib_dsc = lib_dsc;
	}
	public String getLib_dsc2() {
		return lib_dsc2;
	}
	public void setLib_dsc2(String lib_dsc2) {
		this.lib_dsc2 = lib_dsc2;
	}
	public String getListe_desc_colis() {
		return Liste_desc_colis;
	}
	public void setListe_desc_colis(String liste_desc_colis) {
		Liste_desc_colis = liste_desc_colis;
	}
	public String getCod_cou() {
		return cod_cou;
	}
	public void setCod_cou(String cod_cou) {
		this.cod_cou = cod_cou;
	}
	public String getCod_tai() {
		return cod_tai;
	}
	public void setCod_tai(String cod_tai) {
		this.cod_tai = cod_tai;
	}
	public String getCod_ean() {
		return cod_ean;
	}
	public void setCod_ean(String cod_ean) {
		this.cod_ean = cod_ean;
	}
	public String getCod_ean_mas() {
		return cod_ean_mas;
	}
	public void setCod_ean_mas(String cod_ean_mas) {
		this.cod_ean_mas = cod_ean_mas;
	}
	public String getCod_ean_inn() {
		return cod_ean_inn;
	}
	public void setCod_ean_inn(String cod_ean_inn) {
		this.cod_ean_inn = cod_ean_inn;
	}
	public String getCod_cou_ref_cli_2() {
		return cod_cou_ref_cli_2;
	}
	public void setCod_cou_ref_cli_2(String cod_cou_ref_cli_2) {
		this.cod_cou_ref_cli_2 = cod_cou_ref_cli_2;
	}
	public String getCod_tai_ref_cli_2() {
		return cod_tai_ref_cli_2;
	}
	public void setCod_tai_ref_cli_2(String cod_tai_ref_cli_2) {
		this.cod_tai_ref_cli_2 = cod_tai_ref_cli_2;
	}
	public String getRef_cli_3() {
		return ref_cli_3;
	}
	public void setRef_cli_3(String ref_cli_3) {
		this.ref_cli_3 = ref_cli_3;
	}
	public String getNum_tra() {
		return num_tra;
	}
	public void setNum_tra(String num_tra) {
		this.num_tra = num_tra;
	}
	public String getDat_etd() {
		return dat_etd;
	}
	public void setDat_etd(String dat_etd) {
		this.dat_etd = dat_etd;
	}
	public String getNbr_jou_tra() {
		return nbr_jou_tra;
	}
	public void setNbr_jou_tra(String nbr_jou_tra) {
		this.nbr_jou_tra = nbr_jou_tra;
	}
	public String getDat_eta() {
		return dat_eta;
	}
	public void setDat_eta(String dat_eta) {
		this.dat_eta = dat_eta;
	}
	public String getCod_trs_cli() {
		return cod_trs_cli;
	}
	public void setCod_trs_cli(String cod_trs_cli) {
		this.cod_trs_cli = cod_trs_cli;
	}
	public String getCod_ean_cli() {
		return cod_ean_cli;
	}
	public void setCod_ean_cli(String cod_ean_cli) {
		this.cod_ean_cli = cod_ean_cli;
	}
	public String getCod_vil_prc2() {
		return cod_vil_prc2;
	}
	public void setCod_vil_prc2(String cod_vil_prc2) {
		this.cod_vil_prc2 = cod_vil_prc2;
	}
	public String getCod_trs_dep() {
		return cod_trs_dep;
	}
	public void setCod_trs_dep(String cod_trs_dep) {
		this.cod_trs_dep = cod_trs_dep;
	}
	public String getCod_mod_tra_tranche() {
		return cod_mod_tra_tranche;
	}
	public void setCod_mod_tra_tranche(String cod_mod_tra_tranche) {
		this.cod_mod_tra_tranche = cod_mod_tra_tranche;
	}
	public String getCod_inc_tranche() {
		return cod_inc_tranche;
	}
	public void setCod_inc_tranche(String cod_inc_tranche) {
		this.cod_inc_tranche = cod_inc_tranche;
	}
	public String getCod_vil_inc_tranche() {
		return cod_vil_inc_tranche;
	}
	public void setCod_vil_inc_tranche(String cod_vil_inc_tranche) {
		this.cod_vil_inc_tranche = cod_vil_inc_tranche;
	}
	public String getCod_pro_qte_produit() {
		return cod_pro_qte_produit;
	}
	public void setCod_pro_qte_produit(String cod_pro_qte_produit) {
		this.cod_pro_qte_produit = cod_pro_qte_produit;
	}
	public String getCod_cou_qte_produit() {
		return cod_cou_qte_produit;
	}
	public void setCod_cou_qte_produit(String cod_cou_qte_produit) {
		this.cod_cou_qte_produit = cod_cou_qte_produit;
	}
	public String getCod_tai_qte_produit() {
		return cod_tai_qte_produit;
	}
	public void setCod_tai_qte_produit(String cod_tai_qte_produit) {
		this.cod_tai_qte_produit = cod_tai_qte_produit;
	}
	public String getNbr_col() {
		return nbr_col;
	}
	public void setNbr_col(String nbr_col) {
		this.nbr_col = nbr_col;
	}
	public String getNbr_uvc() {
		return nbr_uvc;
	}
	public void setNbr_uvc(String nbr_uvc) {
		this.nbr_uvc = nbr_uvc;
	}
	public String getVol() {
		return vol;
	}
	public void setVol(String vol) {
		this.vol = vol;
	}
	public String getPds_brt() {
		return pds_brt;
	}
	public void setPds_brt(String pds_brt) {
		this.pds_brt = pds_brt;
	}
	public String getCod_prt() {
		return cod_prt;
	}
	public void setCod_prt(String cod_prt) {
		this.cod_prt = cod_prt;
	}
	public String getCod_pro_date_livraiso() {
		return cod_pro_date_livraiso;
	}
	public void setCod_pro_date_livraiso(String cod_pro_date_livraiso) {
		this.cod_pro_date_livraiso = cod_pro_date_livraiso;
	}
	public String getNum_tra_date_livraiso() {
		return num_tra_date_livraiso;
	}
	public void setNum_tra_date_livraiso(String num_tra_date_livraiso) {
		this.num_tra_date_livraiso = num_tra_date_livraiso;
	}
	public String getNbr_col_date_livraiso() {
		return nbr_col_date_livraiso;
	}
	public void setNbr_col_date_livraiso(String nbr_col_date_livraiso) {
		this.nbr_col_date_livraiso = nbr_col_date_livraiso;
	}
	public String getQte() {
		return qte;
	}
	public void setQte(String qte) {
		this.qte = qte;
	}
	public String getDat_etd_lim() {
		return dat_etd_lim;
	}
	public void setDat_etd_lim(String dat_etd_lim) {
		this.dat_etd_lim = dat_etd_lim;
	}
	public String getDat_ent_cal() {
		return dat_ent_cal;
	}
	public void setDat_ent_cal(String dat_ent_cal) {
		this.dat_ent_cal = dat_ent_cal;
	}
	public String getDat_ent_sou() {
		return dat_ent_sou;
	}
	public void setDat_ent_sou(String dat_ent_sou) {
		this.dat_ent_sou = dat_ent_sou;
	}
	public String getDat_mag_cal() {
		return dat_mag_cal;
	}
	public void setDat_mag_cal(String dat_mag_cal) {
		this.dat_mag_cal = dat_mag_cal;
	}
	public String getDat_mag_sou() {
		return dat_mag_sou;
	}
	public void setDat_mag_sou(String dat_mag_sou) {
		this.dat_mag_sou = dat_mag_sou;
	}
	public String getListe_catalogue() {
		return Liste_catalogue;
	}
	public void setListe_catalogue(String liste_catalogue) {
		Liste_catalogue = liste_catalogue;
	}
	public String getCod_pro_documents() {
		return cod_pro_documents;
	}
	public void setCod_pro_documents(String cod_pro_documents) {
		this.cod_pro_documents = cod_pro_documents;
	}
	public String getCod_ndp() {
		return cod_ndp;
	}
	public void setCod_ndp(String cod_ndp) {
		this.cod_ndp = cod_ndp;
	}
	public String getCod_amf() {
		return cod_amf;
	}
	public void setCod_amf(String cod_amf) {
		this.cod_amf = cod_amf;
	}
	public String getCod_doc() {
		return cod_doc;
	}
	public void setCod_doc(String cod_doc) {
		this.cod_doc = cod_doc;
	}
	public String getNbr_ori() {
		return nbr_ori;
	}
	public void setNbr_ori(String nbr_ori) {
		this.nbr_ori = nbr_ori;
	}
	public String getNbr_cop() {
		return nbr_cop;
	}
	public void setNbr_cop(String nbr_cop) {
		this.nbr_cop = nbr_cop;
	}*/
/*	public List<Fournisseur> getFournisseurList() {
		return fournisseurList;
	}
	public void setFournisseurList(List<Fournisseur> fournisseurList) {
		this.fournisseurList = fournisseurList;
	}*/
	
	public List<Produit> getProduitLst() {
		return produitLst;
	}
	public Fournisseur getFournisseur() {
		return fournisseur;
	}
	public void setFournisseur(Fournisseur fournisseur) {
		this.fournisseur = fournisseur;
	}
	public void setProduitLst(List<Produit> produitLst) {
		this.produitLst = produitLst;
	}
	public List<Tranche> getTrancheLst() {
		return trancheLst;
	}
	public void setTrancheLst(List<Tranche> trancheLst) {
		this.trancheLst = trancheLst;
	}
	public List<DateLivraiso> getDateLivraisoLst() {
		return dateLivraisoLst;
	}
	public void setDateLivraisoLst(List<DateLivraiso> dateLivraisoLst) {
		this.dateLivraisoLst = dateLivraisoLst;
	}
	public List<Documents> getDocumentsLst() {
		return DocumentsLst;
	}
	public void setDocumentsLst(List<Documents> documentsLst) {
		DocumentsLst = documentsLst;
	}
	
	
}

