package com.myshipment.model;

public class ShippingOrderPdfGeneration {
	private String companyName;
	private String companyCode;
	private String address1;
	private String address2;
	private String address3;
	private String telno;
	private String soNo;
	private String hblNo;
	private String shipper;
	private String cnf;
	private String notify;
	private String phone1;
	private String phone2;
	private String phone3;
	private String placeOfReceipt;
	private String destination;
	private String expectedDate;
	private String po;
	private String comodity;
	private String styleNo;
	private String sku;
	private int cartonQty;
	private int totalPcs;
	private Double netWeight;
	private Double grossWeight;
	private Double totalCbm;
	private String vesselNo;
	private String vesselName;
	private String motherVesselNo;
	private String motherVesselName;
	private String carrier;
	private String salesOrg;
	private String qrString;
	private String totalGrossW;
	private String orderType;

	public String getOrderType() {
		return orderType;
	}

	public void setOrderType(String orderType) {
		this.orderType = orderType;
	}

	public String getTotalGrossW() {
		return totalGrossW;
	}

	public void setTotalGrossW(String totalGrossW) {
		this.totalGrossW = totalGrossW;
	}

	public String getQrString() {
		return qrString;
	}

	public void setQrString(String qrString) {
		this.qrString = qrString;
	}

	public String getSalesOrg() {
		return salesOrg;
	}

	public void setSalesOrg(String salesOrg) {
		this.salesOrg = salesOrg;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getVesselNo() {
		return vesselNo;
	}

	public void setVesselNo(String vesselNo) {
		this.vesselNo = vesselNo;
	}

	public String getVesselName() {
		return vesselName;
	}

	public void setVesselName(String vesselName) {
		this.vesselName = vesselName;
	}

	public String getMotherVesselNo() {
		return motherVesselNo;
	}

	public void setMotherVesselNo(String motherVesselNo) {
		this.motherVesselNo = motherVesselNo;
	}

	public String getMotherVesselName() {
		return motherVesselName;
	}

	public void setMotherVesselName(String motherVesselName) {
		this.motherVesselName = motherVesselName;
	}

	public String getCarrier() {
		return carrier;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getTelno() {
		return telno;
	}

	public void setTelno(String telno) {
		this.telno = telno;
	}

	public String getSoNo() {
		return soNo;
	}

	public void setSoNo(String soNo) {
		this.soNo = soNo;
	}

	public String getHblNo() {
		return hblNo;
	}

	public void setHblNo(String hblNo) {
		this.hblNo = hblNo;
	}

	public String getShipper() {
		return shipper;
	}

	public void setShipper(String shipper) {
		this.shipper = shipper;
	}

	public String getCnf() {
		return cnf;
	}

	public void setCnf(String cnf) {
		this.cnf = cnf;
	}

	public String getNotify() {
		return notify;
	}

	public void setNotify(String notify) {
		this.notify = notify;
	}

	public String getPhone1() {
		return phone1;
	}

	public void setPhone1(String phone1) {
		this.phone1 = phone1;
	}

	public String getPhone2() {
		return phone2;
	}

	public void setPhone2(String phone2) {
		this.phone2 = phone2;
	}

	public String getPhone3() {
		return phone3;
	}

	public void setPhone3(String phone3) {
		this.phone3 = phone3;
	}

	public String getPlaceOfReceipt() {
		return placeOfReceipt;
	}

	public void setPlaceOfReceipt(String placeOfReceipt) {
		this.placeOfReceipt = placeOfReceipt;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public String getExpectedDate() {
		return expectedDate;
	}

	public void setExpectedDate(String expectedDate) {
		this.expectedDate = expectedDate;
	}

	public String getPo() {
		return po;
	}

	public void setPo(String po) {
		this.po = po;
	}

	public String getComodity() {
		return comodity;
	}

	public void setComodity(String comodity) {
		this.comodity = comodity;
	}

	public String getStyleNo() {
		return styleNo;
	}

	public void setStyleNo(String styleNo) {
		this.styleNo = styleNo;
	}

	public String getSku() {
		return sku;
	}

	public void setSku(String sku) {
		this.sku = sku;
	}

	public int getCartonQty() {
		return cartonQty;
	}

	public void setCartonQty(int cartonQty) {
		this.cartonQty = cartonQty;
	}

	public int getTotalPcs() {
		return totalPcs;
	}

	public void setTotalPcs(int totalPcs) {
		this.totalPcs = totalPcs;
	}

	public Double getNetWeight() {
		return netWeight;
	}

	public void setNetWeight(Double netWeight) {
		this.netWeight = netWeight;
	}

	public Double getGrossWeight() {
		return grossWeight;
	}

	public void setGrossWeight(Double grossWeight) {
		this.grossWeight = grossWeight;
	}

	public Double getTotalCbm() {
		return totalCbm;
	}

	public void setTotalCbm(Double totalCbm) {
		this.totalCbm = totalCbm;
	}

}
