/**
*
*@author Ahmad Naquib
*/
package com.myshipment.model;

import java.util.Date;

public class ContainerLocationDetails {
	
	private String locCode;
	private String dtmCode;
	private String dtmDetails;
	private String activityDate;
	private String locDetails;
	private String activityLocation;
	private String voyageNo;
	
	private Date dateTime;
	
	public String getDtmDetails() {
		return dtmDetails;
	}
	public void setDtmDetails(String dtmDetails) {
		this.dtmDetails = dtmDetails;
	}
	public String getActivityDate() {
		return activityDate;
	}
	public void setActivityDate(String activityDate) {
		this.activityDate = activityDate;
	}
	public String getLocDetails() {
		return locDetails;
	}
	public void setLocDetails(String locDetails) {
		this.locDetails = locDetails;
	}
	public String getActivityLocation() {
		return activityLocation;
	}
	public void setActivityLocation(String activityLocation) {
		this.activityLocation = activityLocation;
	}
	public Date getDateTime() {
		return dateTime;
	}
	public void setDateTime(Date dateTime) {
		this.dateTime = dateTime;
	}
	public String getVoyageNo() {
		return voyageNo;
	}
	public void setVoyageNo(String voyageNo) {
		this.voyageNo = voyageNo;
	}
	public String getLocCode() {
		return locCode;
	}
	public void setLocCode(String locCode) {
		this.locCode = locCode;
	}
	public String getDtmCode() {
		return dtmCode;
	}
	public void setDtmCode(String dtmCode) {
		this.dtmCode = dtmCode;
	}
	
}
