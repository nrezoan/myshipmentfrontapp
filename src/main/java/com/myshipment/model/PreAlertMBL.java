/**
*
*@author Ahmad Naquib
*/
package com.myshipment.model;

import java.sql.Timestamp;
import java.util.Date;

public class PreAlertMBL {

	private String mblNumber;
	private String filePathMbl;
	private String isMbl;
	private String isReadyForPreAlert;
	private Date createdAt;
	private Timestamp updatedAt;
	private String crmId;
	
	public String getMblNumber() {
		return mblNumber;
	}
	public void setMblNumber(String mblNumber) {
		this.mblNumber = mblNumber;
	}
	public String getFilePathMbl() {
		return filePathMbl;
	}
	public void setFilePathMbl(String filePathMbl) {
		this.filePathMbl = filePathMbl;
	}
	public String getIsMbl() {
		return isMbl;
	}
	public void setIsMbl(String isMbl) {
		this.isMbl = isMbl;
	}
	public String getIsReadyForPreAlert() {
		return isReadyForPreAlert;
	}
	public void setIsReadyForPreAlert(String isReadyForPreAlert) {
		this.isReadyForPreAlert = isReadyForPreAlert;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Timestamp getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}
	public String getCrmId() {
		return crmId;
	}
	public void setCrmId(String crmId) {
		this.crmId = crmId;
	}
	
}
