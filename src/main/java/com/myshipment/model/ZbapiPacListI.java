package com.myshipment.model;



public class ZbapiPacListI {


	private String vbeln;

	private Double zzgr_wt;

	private Double zzntWt;
	
	private String zzponumber;

	private String descZ001;

	private String descZ003;
	
	private String zzstyleno;
	
	private String zzhscode;
	
	private Integer zztotalnopcs;
	
	private String zzunitCost;
	
	private String zzquantity;
	
	private String zzedisize;
	
	private String zzcolour;
	
	private String zzpcspercarton;
	
	private String zzlength;
	
	private String zzwidth;
	
	private String zzheight;
	
	private String zzcartserlno;
	
	private Double zzvolume;
	
	private Double zznetwtpercarton;
	
	private String zzwtpercartoon;
	
	private Double zamount;
	
	private Double zunitVol;
	
	private Integer posnr;
	
	private String zzskuno;
	
	private Double zzcbmSea;
	
	private String descZ002;
	
	private String descZ005;
	
	private Double zzunitCostUnit;
	
	private String zzcomminvno;
	
	private Integer index;
	public String getVbeln() {
		return vbeln;
	}
	public void setVbeln(String vbeln) {
		this.vbeln = vbeln;
	}
	public Double getZzgr_wt() {
		return zzgr_wt;
	}
	public void setZzgr_wt(Double zzgr_wt) {
		this.zzgr_wt = zzgr_wt;
	}
	public Double getZzntWt() {
		return zzntWt;
	}
	public void setZzntWt(Double zzntWt) {
		this.zzntWt = zzntWt;
	}
	public String getZzponumber() {
		return zzponumber;
	}
	public void setZzponumber(String zzponumber) {
		this.zzponumber = zzponumber;
	}
	public String getDescZ001() {
		return descZ001;
	}
	public void setDescZ001(String descZ001) {
		this.descZ001 = descZ001;
	}
	public String getDescZ003() {
		return descZ003;
	}
	public void setDescZ003(String descZ003) {
		this.descZ003 = descZ003;
	}
	public String getZzstyleno() {
		return zzstyleno;
	}
	public void setZzstyleno(String zzstyleno) {
		this.zzstyleno = zzstyleno;
	}
	public String getZzhscode() {
		return zzhscode;
	}
	public void setZzhscode(String zzhscode) {
		this.zzhscode = zzhscode;
	}
	public Integer getZztotalnopcs() {
		return zztotalnopcs;
	}
	public void setZztotalnopcs(Integer zztotalnopcs) {
		this.zztotalnopcs = zztotalnopcs;
	}
	public String getZzunitCost() {
		return zzunitCost;
	}
	public void setZzunitCost(String zzunitCost) {
		this.zzunitCost = zzunitCost;
	}
	public String getZzquantity() {
		return zzquantity;
	}
	public void setZzquantity(String zzquantity) {
		this.zzquantity = zzquantity;
	}
	public String getZzedisize() {
		return zzedisize;
	}
	public void setZzedisize(String zzedisize) {
		this.zzedisize = zzedisize;
	}
	public String getZzcolour() {
		return zzcolour;
	}
	public void setZzcolour(String zzcolour) {
		this.zzcolour = zzcolour;
	}
	public String getZzpcspercarton() {
		return zzpcspercarton;
	}
	public void setZzpcspercarton(String zzpcspercarton) {
		this.zzpcspercarton = zzpcspercarton;
	}
	public String getZzlength() {
		return zzlength;
	}
	public void setZzlength(String zzlength) {
		this.zzlength = zzlength;
	}
	public String getZzwidth() {
		return zzwidth;
	}
	public void setZzwidth(String zzwidth) {
		this.zzwidth = zzwidth;
	}
	public String getZzheight() {
		return zzheight;
	}
	public void setZzheight(String zzheight) {
		this.zzheight = zzheight;
	}
	public String getZzcartserlno() {
		return zzcartserlno;
	}
	public void setZzcartserlno(String zzcartserlno) {
		this.zzcartserlno = zzcartserlno;
	}
	public Double getZzvolume() {
		return zzvolume;
	}
	public void setZzvolume(Double zzvolume) {
		this.zzvolume = zzvolume;
	}
	public Double getZznetwtpercarton() {
		return zznetwtpercarton;
	}
	public void setZznetwtpercarton(Double zznetwtpercarton) {
		this.zznetwtpercarton = zznetwtpercarton;
	}
	public String getZzwtpercartoon() {
		return zzwtpercartoon;
	}
	public void setZzwtpercartoon(String zzwtpercartoon) {
		this.zzwtpercartoon = zzwtpercartoon;
	}
	public Double getZamount() {
		return zamount;
	}
	public void setZamount(Double zamount) {
		this.zamount = zamount;
	}
	public Double getZunitVol() {
		return zunitVol;
	}
	public void setZunitVol(Double zunitVol) {
		this.zunitVol = zunitVol;
	}
	public Integer getPosnr() {
		return posnr;
	}
	public void setPosnr(Integer posnr) {
		this.posnr = posnr;
	}
	public String getZzskuno() {
		return zzskuno;
	}
	public void setZzskuno(String zzskuno) {
		this.zzskuno = zzskuno;
	}
	public Double getZzcbmSea() {
		return zzcbmSea;
	}
	public void setZzcbmSea(Double zzcbmSea) {
		this.zzcbmSea = zzcbmSea;
	}
	public String getDescZ002() {
		return descZ002;
	}
	public void setDescZ002(String descZ002) {
		this.descZ002 = descZ002;
	}
	public String getDescZ005() {
		return descZ005;
	}
	public void setDescZ005(String descZ005) {
		this.descZ005 = descZ005;
	}
	public Double getZzunitCostUnit() {
		return zzunitCostUnit;
	}
	public void setZzunitCostUnit(Double zzunitCostUnit) {
		this.zzunitCostUnit = zzunitCostUnit;
	}
	public String getZzcomminvno() {
		return zzcomminvno;
	}
	public void setZzcomminvno(String zzcomminvno) {
		this.zzcomminvno = zzcomminvno;
	}
	public Integer getIndex() {
		return index;
	}
	public void setIndex(Integer index) {
		this.index = index;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
