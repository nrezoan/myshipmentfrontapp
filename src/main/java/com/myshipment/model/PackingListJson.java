package com.myshipment.model;

import java.util.List;

public class PackingListJson {

	List<PoBookingPacklistDetailBean> poBookingPacklistDetailBeans;

	public List<PoBookingPacklistDetailBean> getPoBookingPacklistDetailBeans() {
		return poBookingPacklistDetailBeans;
	}

	public void setPoBookingPacklistDetailBeans(List<PoBookingPacklistDetailBean> poBookingPacklistDetailBeans) {
		this.poBookingPacklistDetailBeans = poBookingPacklistDetailBeans;
	}
	
}
