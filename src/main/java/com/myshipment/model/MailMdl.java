package com.myshipment.model;

public class MailMdl {
	private String mailFrom;
	private String[] mailto;
	private String[] mailCc;
	private String[] mailBcc;
	private String mailSubject;
	private String mailContent;
	private String templateName;
	private String contentType;
	private String inlineImagePath;
	private String inlineImageContentId;
	private String attachmentName;
	private String attachmentPath;
	public MailMdl()
	{
		contentType="text/html";
	}
	public String getMailFrom() {
		return mailFrom;
	}
	public void setMailFrom(String mailFrom) {
		this.mailFrom = mailFrom;
	}
	public String[] getMailto() {
		return mailto;
	}
	public void setMailto(String[] mailto) {
		this.mailto = mailto;
	}
	public String[] getMailCc() {
		return mailCc;
	}
	public void setMailCc(String[] mailCc) {
		this.mailCc = mailCc;
	}
	public String[] getMailBcc() {
		return mailBcc;
	}
	public void setMailBcc(String[] mailBcc) {
		this.mailBcc = mailBcc;
	}
	public String getMailSubject() {
		return mailSubject;
	}
	public void setMailSubject(String mailSubject) {
		this.mailSubject = mailSubject;
	}
	public String getMailContent() {
		return mailContent;
	}
	public void setMailContent(String mailContent) {
		this.mailContent = mailContent;
	}
	public String getTemplateName() {
		return templateName;
	}
	public void setTemplateName(String templateName) {
		this.templateName = templateName;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getInlineImagePath() {
		return inlineImagePath;
	}
	public void setInlineImagePath(String inlineImagePath) {
		this.inlineImagePath = inlineImagePath;
	}
	
	public String getAttachmentName() {
		return attachmentName;
	}
	public void setAttachmentName(String attachmentName) {
		this.attachmentName = attachmentName;
	}
	public String getAttachmentPath() {
		return attachmentPath;
	}
	public void setAttachmentPath(String attachmentPath) {
		this.attachmentPath = attachmentPath;
	}
	public String getInlineImageContentId() {
		return inlineImageContentId;
	}
	public void setInlineImageContentId(String inlineImageContentId) {
		this.inlineImageContentId = inlineImageContentId;
	}
	

}
