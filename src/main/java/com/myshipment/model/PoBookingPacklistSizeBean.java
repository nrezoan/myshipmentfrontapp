package com.myshipment.model;

/*
 * @Ranjeet Kumar
 */
public class PoBookingPacklistSizeBean {

	private Integer sizeId;	
	private String sizeName;	
	private Integer colorId;	
	private Integer noOfPcs;
	private PoBookingPacklistColorBean packlistColor;
	
	public Integer getSizeId() {
		return sizeId;
	}
	public void setSizeId(Integer sizeId) {
		this.sizeId = sizeId;
	}
	public String getSizeName() {
		return sizeName;
	}
	public void setSizeName(String sizeName) {
		this.sizeName = sizeName;
	}
	public Integer getColorId() {
		return colorId;
	}
	public void setColorId(Integer colorId) {
		this.colorId = colorId;
	}
	public Integer getNoOfPcs() {
		return noOfPcs;
	}
	public void setNoOfPcs(Integer noOfPcs) {
		this.noOfPcs = noOfPcs;
	}
	public PoBookingPacklistColorBean getPacklistColor() {
		return packlistColor;
	}
	public void setPacklistColor(PoBookingPacklistColorBean packlistColor) {
		this.packlistColor = packlistColor;
	}
	
	
	
}
