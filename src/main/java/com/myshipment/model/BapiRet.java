package com.myshipment.model;


public class BapiRet {

	
	
	private char type;	
	
    private String id;
    
    private String number;
   
    private String message; 
   
    private char logNo;
   
    private String log_msg_no;
    
    private String messageV1;
    
    private String messageV2;
  
    private String messageV3;
   
    private String messageV4;
    
	public char getType() {
		return type;
	}
	public String getId() {
		return id;
	}
	public String getNumber() {
		return number;
	}
	public String getMessage() {
		return message;
	}
	public char getLogNo() {
		return logNo;
	}
	public String getLog_msg_no() {
		return log_msg_no;
	}
	public String getMessageV1() {
		return messageV1;
	}
	public String getMessageV2() {
		return messageV2;
	}
	public String getMessageV3() {
		return messageV3;
	}
	public String getMessageV4() {
		return messageV4;
	}
    
}
