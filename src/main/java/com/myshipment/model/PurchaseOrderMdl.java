package com.myshipment.model;

import java.util.Date;
import java.util.List;

public class PurchaseOrderMdl{
	

	private static final long serialVersionUID = -4356643246891l;
	private Long po_id;
	private String vc_po_no;
	private String nu_client_code;
	private String sales_org;
	private String vc_buyer;
	private String vc_buy_house;
	private String company_code;
	private String supplier_code;
	private String vc_product_no;
	private Date po_creation_date;
	private List<SegPurchaseOrderMdl> lineItems;
	public Date getPo_creation_date() {
		return po_creation_date;
	}

	public void setPo_creation_date(Date po_creation_date) {
		this.po_creation_date = po_creation_date;
	}

	
	public String getVc_product_no() {
		return vc_product_no;
	}

	public void setVc_product_no(String vc_product_no) {
		this.vc_product_no = vc_product_no;
	}

	public Long getPo_id() {
		return po_id;
	}

	public void setPo_id(Long po_id) {
		this.po_id = po_id;
	}

	public String getVc_po_no() {
		return vc_po_no;
	}

	public void setVc_po_no(String vc_po_no) {
		this.vc_po_no = vc_po_no;
	}

	public String getNu_client_code() {
		return nu_client_code;
	}

	public void setNu_client_code(String nu_client_code) {
		this.nu_client_code = nu_client_code;
	}

	public String getSales_org() {
		return sales_org;
	}

	public void setSales_org(String sales_org) {
		this.sales_org = sales_org;
	}

	public String getVc_buyer() {
		return vc_buyer;
	}

	public void setVc_buyer(String vc_buyer) {
		this.vc_buyer = vc_buyer;
	}

	public String getVc_buy_house() {
		return vc_buy_house;
	}

	public void setVc_buy_house(String vc_buy_house) {
		this.vc_buy_house = vc_buy_house;
	}

	public String getCompany_code() {
		return company_code;
	}

	public void setCompany_code(String company_code) {
		this.company_code = company_code;
	}

	public String getSupplier_code() {
		return supplier_code;
	}

	public void setSupplier_code(String supplier_code) {
		this.supplier_code = supplier_code;
	}

	public List<SegPurchaseOrderMdl> getLineItems() {
		return lineItems;
	}

	public void setLineItems(List<SegPurchaseOrderMdl> lineItems) {
		this.lineItems = lineItems;
	}

	/*@Override
	public String toString() {
		return "PurchaseOrderMdl [po_id=" + po_id + ", vc_po_no=" + vc_po_no + ", nu_client_code=" + nu_client_code
				+ ", sales_org=" + sales_org + ", vc_buyer=" + vc_buyer + ", vc_buy_house=" + vc_buy_house
				+ ", company_code=" + company_code + ", supplier_code=" + supplier_code + ", vc_product_no="
				+ vc_product_no + ", po_creation_date=" + po_creation_date + ", lineItems=" + lineItems + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((company_code == null) ? 0 : company_code.hashCode());
		result = prime * result + ((lineItems == null) ? 0 : lineItems.hashCode());
		result = prime * result + ((nu_client_code == null) ? 0 : nu_client_code.hashCode());
		result = prime * result + ((po_creation_date == null) ? 0 : po_creation_date.hashCode());
		result = prime * result + ((po_id == null) ? 0 : po_id.hashCode());
		result = prime * result + ((sales_org == null) ? 0 : sales_org.hashCode());
		result = prime * result + ((supplier_code == null) ? 0 : supplier_code.hashCode());
		result = prime * result + ((vc_buy_house == null) ? 0 : vc_buy_house.hashCode());
		result = prime * result + ((vc_buyer == null) ? 0 : vc_buyer.hashCode());
		result = prime * result + ((vc_po_no == null) ? 0 : vc_po_no.hashCode());
		result = prime * result + ((vc_product_no == null) ? 0 : vc_product_no.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PurchaseOrderMdl other = (PurchaseOrderMdl) obj;
		if (company_code == null) {
			if (other.company_code != null)
				return false;
		} else if (!company_code.equals(other.company_code))
			return false;
		if (lineItems == null) {
			if (other.lineItems != null)
				return false;
		} else if (!lineItems.equals(other.lineItems))
			return false;
		if (nu_client_code == null) {
			if (other.nu_client_code != null)
				return false;
		} else if (!nu_client_code.equals(other.nu_client_code))
			return false;
		if (po_creation_date == null) {
			if (other.po_creation_date != null)
				return false;
		} else if (!po_creation_date.equals(other.po_creation_date))
			return false;
		if (po_id == null) {
			if (other.po_id != null)
				return false;
		} else if (!po_id.equals(other.po_id))
			return false;
		if (sales_org == null) {
			if (other.sales_org != null)
				return false;
		} else if (!sales_org.equals(other.sales_org))
			return false;
		if (supplier_code == null) {
			if (other.supplier_code != null)
				return false;
		} else if (!supplier_code.equals(other.supplier_code))
			return false;
		if (vc_buy_house == null) {
			if (other.vc_buy_house != null)
				return false;
		} else if (!vc_buy_house.equals(other.vc_buy_house))
			return false;
		if (vc_buyer == null) {
			if (other.vc_buyer != null)
				return false;
		} else if (!vc_buyer.equals(other.vc_buyer))
			return false;
		if (vc_po_no == null) {
			if (other.vc_po_no != null)
				return false;
		} else if (!vc_po_no.equals(other.vc_po_no))
			return false;
		if (vc_product_no == null) {
			if (other.vc_product_no != null)
				return false;
		} else if (!vc_product_no.equals(other.vc_product_no))
			return false;
		return true;
	}*/

	
}
