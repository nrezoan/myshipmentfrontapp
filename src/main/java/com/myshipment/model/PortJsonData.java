package com.myshipment.model;

import java.util.List;


public class PortJsonData {

	
	
	private List<PortListBean> portList;

	public List<PortListBean> getPortList() {
		return portList;
	}

	public void setPortList(List<PortListBean> portList) {
		this.portList = portList;
	} 
	
	
	
}
