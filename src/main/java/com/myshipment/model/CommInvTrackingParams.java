package com.myshipment.model;
/*
 * @Ranjeet Kumar
 */
public class CommInvTrackingParams {

	private String commInvNo;
	private String fromDate;
	private String toDate;
	private String statusType;
	private String shipper_no;
	private String buyer_no;
	
	
	public String getCommInvNo() {
		return commInvNo;
	}
	public void setCommInvNo(String commInvNo) {
		this.commInvNo = commInvNo;
	}
	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getStatusType() {
		return statusType;
	}
	public void setStatusType(String statusType) {
		this.statusType = statusType;
	}
	public String getShipper_no() {
		return shipper_no;
	}
	public void setShipper_no(String shipper_no) {
		this.shipper_no = shipper_no;
	}
	public String getBuyer_no() {
		return buyer_no;
	}
	public void setBuyer_no(String buyer_no) {
		this.buyer_no = buyer_no;
	}
	
	
}
