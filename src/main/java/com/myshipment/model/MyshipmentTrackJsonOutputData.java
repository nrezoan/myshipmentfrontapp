package com.myshipment.model;

import java.util.List;

public class MyshipmentTrackJsonOutputData {
	
		private BapiReturn1 bapiReturn1;		
		private List<MyshipmentTrackHeaderBean> myshipmentTrackHeader;		
		private List<MyshipmentTrackItemBean> myshipmentTrackItem;		
		private List<MyshipmentTrackScheduleBean> myshipmentTrackSchedule;
		private List<MyshipmentTrackContainerBean> myshipmentTrackContainer;
		private String dialogue;
		
		public BapiReturn1 getBapiReturn1() {
			return bapiReturn1;
		}
		public void setBapiReturn1(BapiReturn1 bapiReturn1) {
			this.bapiReturn1 = bapiReturn1;
		}
		public List<MyshipmentTrackHeaderBean> getMyshipmentTrackHeader() {
			return myshipmentTrackHeader;
		}
		public void setMyshipmentTrackHeader(List<MyshipmentTrackHeaderBean> myshipmentTrackHeader) {
			this.myshipmentTrackHeader = myshipmentTrackHeader;
		}
		public List<MyshipmentTrackItemBean> getMyshipmentTrackItem() {
			return myshipmentTrackItem;
		}
		public void setMyshipmentTrackItem(List<MyshipmentTrackItemBean> myshipmentTrackItem) {
			this.myshipmentTrackItem = myshipmentTrackItem;
		}
		public List<MyshipmentTrackScheduleBean> getMyshipmentTrackSchedule() {
			return myshipmentTrackSchedule;
		}
		public void setMyshipmentTrackSchedule(List<MyshipmentTrackScheduleBean> myshipmentTrackSchedule) {
			this.myshipmentTrackSchedule = myshipmentTrackSchedule;
		}
		public List<MyshipmentTrackContainerBean> getMyshipmentTrackContainer() {
			return myshipmentTrackContainer;
		}
		public void setMyshipmentTrackContainer(List<MyshipmentTrackContainerBean> myshipmentTrackContainer) {
			this.myshipmentTrackContainer = myshipmentTrackContainer;
		}
		public String getDialogue() {
			return dialogue;
		}
		public void setDialogue(String dialogue) {
			this.dialogue = dialogue;
		}
		
}
