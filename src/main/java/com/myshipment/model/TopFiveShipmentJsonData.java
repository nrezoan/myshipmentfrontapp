package com.myshipment.model;

import java.util.Comparator;

public class TopFiveShipmentJsonData {
	private String portOfDischarge;
	private Double totalShipment;
	private Double totalCBM;
	private Double totalGWT;
	
	//hamid
	private String portOfDischargeDesc;
	public String getPortOfDischargeDesc() {
		return portOfDischargeDesc;
	}

	public void setPortOfDischargeDesc(String portOfDischargeDesc) {
		this.portOfDischargeDesc = portOfDischargeDesc;
	}
	
	public String getPortOfDischarge() {
		return portOfDischarge;
	}
	public void setPortOfDischarge(String portOfDischarge) {
		this.portOfDischarge = portOfDischarge;
	}
	public Double getTotalShipment() {
		return totalShipment;
	}
	public void setTotalShipment(Double totalShipment) {
		this.totalShipment = totalShipment;
	}
	public Double getTotalCBM() {
		return totalCBM;
	}
	public void setTotalCBM(Double totalCBM) {
		this.totalCBM = totalCBM;
	}
	public Double getTotalGWT() {
		return totalGWT;
	}
	public void setTotalGWT(Double totalGWT) {
		this.totalGWT = totalGWT;
	}
	public static Comparator<TopFiveShipmentJsonData> TopFiveShipJsonDataComparatorByPOD=new Comparator<TopFiveShipmentJsonData>() {
		
		@Override
		public int compare(TopFiveShipmentJsonData o1, TopFiveShipmentJsonData o2) {
			return o1.portOfDischarge.compareTo(o2.portOfDischarge);
			
		}
	};

public static Comparator<TopFiveShipmentJsonData> TopFiveShipJsonDataComparatorByTotShip=new Comparator<TopFiveShipmentJsonData>() {
		
		@Override
		public int compare(TopFiveShipmentJsonData o1, TopFiveShipmentJsonData o2) {
			 Double d1=o1.totalShipment;
			 Double d2=o2.totalShipment;
			 return d1.compareTo(d2);
			
		}
	};
	
	
public static Comparator<TopFiveShipmentJsonData> TopFiveShipJsonDataComparatorByTotCBM=new Comparator<TopFiveShipmentJsonData>() {
		
		@Override
		public int compare(TopFiveShipmentJsonData o1, TopFiveShipmentJsonData o2) {
			 Double d1=o1.totalCBM;
			 Double d2=o2.totalCBM;
			 return d1.compareTo(d2);
			
		}
	};
public static Comparator<TopFiveShipmentJsonData> TopFiveShipJsonDataComparatorByTotGWT=new Comparator<TopFiveShipmentJsonData>() {
		
		@Override
		public int compare(TopFiveShipmentJsonData o1, TopFiveShipmentJsonData o2) {
			 Double d1=o1.totalGWT;
			 Double d2=o2.totalGWT;
			 return d1.compareTo(d2);
			
		}
	};
@Override
public String toString() {
	return "TopFiveShipmentJsonData [portOfDischarge=" + portOfDischarge + ", totalShipment=" + totalShipment
			+ ", totalCBM=" + totalCBM + ", totalGWT=" + totalGWT + "]";
}
	
}
