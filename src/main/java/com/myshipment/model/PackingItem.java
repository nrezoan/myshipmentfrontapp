package com.myshipment.model;

import java.io.Serializable;

import com.myshipment.model.PackingItem;
  
public class PackingItem implements Serializable{

	
	@Override
	public String toString() {
		return "PackingItem [index=" + index + ", column1=" + column1 + ", column2=" + column2 + ", column3=" + column3
				+ ", column4=" + column4 + ", quantity1=" + quantity1 + ", quantity2=" + quantity2 + ", quantity3="
				+ quantity3 + ", quantity4=" + quantity4 + ", quantity5=" + quantity5 + ", quantity6=" + quantity6
				+ ", quantity7=" + quantity7 + ", quantity8=" + quantity8 + ", quantity9=" + quantity9 + ", quantity10="
				+ quantity10 + ", quantity11=" + quantity11 + ", quantity12=" + quantity12 + ", quantity13="
				+ quantity13 + ", quantity14=" + quantity14 + "]";
	}
	private static final long serialVersionUID = 1L;
	
	private Integer index;
	private String column1;
	private String column2;
	private String column3;
	private String column4;
	private Double quantity1;
	private Double quantity2;
	private Double quantity3;

	private Double quantity4;
	private Double quantity5;
	private Double quantity6;
	private Double quantity7;
	private Double quantity8;
	private Double quantity9;
	private Double quantity10;
	private Double quantity11;
	private Double quantity12;
	private Double quantity13;
	private Double quantity14;

	public Double getQuantity13() {
		return quantity13;
	}

	public void setQuantity13(Double quantity13) {
		this.quantity13 = quantity13;
	}

	public Double getQuantity14() {
		return quantity14;
	}

	public void setQuantity14(Double quantity14) {
		this.quantity14 = quantity14;
	}



	public Double getQuantity4() {
		return quantity4;
	}
	public void setQuantity4(Double quantity4) {
		this.quantity4 = quantity4;
	}
	public Double getQuantity5() {
		return quantity5;
	}
	public void setQuantity5(Double quantity5) {
		this.quantity5 = quantity5;
	}
	public Double getQuantity6() {
		return quantity6;
	}
	public void setQuantity6(Double quantity6) {
		this.quantity6 = quantity6;
	}
	public Double getQuantity7() {
		return quantity7;
	}
	public void setQuantity7(Double quantity7) {
		this.quantity7 = quantity7;
	}
	public Double getQuantity8() {
		return quantity8;
	}
	public void setQuantity8(Double quantity8) {
		this.quantity8 = quantity8;
	}
	public Double getQuantity9() {
		return quantity9;
	}
	public void setQuantity9(Double quantity9) {
		this.quantity9 = quantity9;
	}
	public Double getQuantity10() {
		return quantity10;
	}
	public void setQuantity10(Double quantity10) {
		this.quantity10 = quantity10;
	}
	public Double getQuantity11() {
		return quantity11;
	}
	public void setQuantity11(Double quantity11) {
		this.quantity11 = quantity11;
	}
	public Double getQuantity12() {
		return quantity12;
	}
	public void setQuantity12(Double quantity12) {
		this.quantity12 = quantity12;
	}
	public Integer getIndex() {
		return index;
	}
	public void setIndex(Integer index) {
		this.index = index;
	}
	public String getColumn1() {
		return column1;
	}
	public void setColumn1(String column1) {
		this.column1 = column1;
	}
	public String getColumn2() {
		return column2;
	}
	public void setColumn2(String column2) {
		this.column2 = column2;
	}
	public String getColumn3() {
		return column3;
	}
	public void setColumn3(String column3) {
		this.column3 = column3;
	}
	public String getColumn4() {
		return column4;
	}
	public void setColumn4(String column4) {
		this.column4 = column4;
	}
	public Double getQuantity1() {
		return quantity1;
	}
	public void setQuantity1(Double quantity1) {
		this.quantity1 = quantity1;
	}
	public Double getQuantity2() {
		return quantity2;
	}
	public void setQuantity2(Double quantity2) {
		this.quantity2 = quantity2;
	}
	public Double getQuantity3() {
		return quantity3;
	}
	public void setQuantity3(Double quantity3) {
		this.quantity3 = quantity3;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((column1 == null) ? 0 : column1.hashCode());
		result = prime * result + ((column2 == null) ? 0 : column2.hashCode());
		result = prime * result + ((column3 == null) ? 0 : column3.hashCode());
		result = prime * result + ((column4 == null) ? 0 : column4.hashCode());
		result = prime * result + ((quantity1 == null) ? 0 : quantity1.hashCode());
		result = prime * result + ((quantity2 == null) ? 0 : quantity2.hashCode());
		result = prime * result + ((quantity3 == null) ? 0 : quantity3.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final PackingItem other = (PackingItem) obj;
		if (column1 == null) {
			if (other.column1 != null)
				return false;
		} else if (!column1.equals(other.column1))
			return false;
		if (column2 == null) {
			if (other.column2 != null)
				return false;
		} else if (!column2.equals(other.column2))
			return false;
		if (column3 == null) {
			if (other.column3 != null)
				return false;
		} else if (!column3.equals(other.column3))
			return false;
		if (column4 == null) {
			if (other.column4 != null)
				return false;
		} else if (!column4.equals(other.column4))
			return false;
		if (quantity1 == null) {
			if (other.quantity1 != null)
				return false;
		} else if (!quantity1.equals(other.quantity1))
			return false;
		if (quantity2 == null) {
			if (other.quantity2 != null)
				return false;
		} else if (!quantity2.equals(other.quantity2))
			return false;
		if (quantity3 == null) {
			if (other.quantity3 != null)
				return false;
		} else if (!quantity3.equals(other.quantity3))
			return false;
		return true;
	}
}
