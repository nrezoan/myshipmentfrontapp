package com.myshipment.model;

import java.util.List;

/*
 * @Ranjeet Kumar
 */
public class RedirectParams {

	private Long poId;
	private List<SegPurchaseOrder> poIdLst;
	
	public List<SegPurchaseOrder> getPoIdLst() {
		return poIdLst;
	}

	public void setPoIdLst(List<SegPurchaseOrder> poIdLst) {
		this.poIdLst = poIdLst;
	}

	public Long getPoId() {
		return poId;
	}

	public void setPoId(Long poId) {
		this.poId = poId;
	}


	
}
