package com.myshipment.model;

import java.util.List;

/**
 * @author virendra
 *
 */
public class DirectBookingParams {
	private static final Long serialVersionUID=-43534563645634l;

	private OrderHeader orderHeader;
	private List<OrderItem> orderItemLst;
	private List<OrderItem> oldOrderItemList;
	private List<CRMLogger> loggerUpdate;
	@Override
	public String toString() {
		return "DirectBookingParams [orderHeader=" + orderHeader + ", orderItemLst=" + orderItemLst + ", goodsDesc="
				+ goodsDesc + ", shippingMark=" + shippingMark + ", poBookingPacklistDetailBean="
				+ poBookingPacklistDetailBean + "]";
	}
	private String goodsDesc;
	private String shippingMark;
	private PoBookingPacklistDetailBean poBookingPacklistDetailBean;
	
	public PoBookingPacklistDetailBean getPoBookingPacklistDetailBean() {
		return poBookingPacklistDetailBean;
	}
	public void setPoBookingPacklistDetailBean(PoBookingPacklistDetailBean poBookingPacklistDetailBean) {
		this.poBookingPacklistDetailBean = poBookingPacklistDetailBean;
	}
	public OrderHeader getOrderHeader() {
		return orderHeader;
	}
	public void setOrderHeader(OrderHeader orderHeader) {
		this.orderHeader = orderHeader;
	}
	public List<OrderItem> getOrderItemLst() {
		return orderItemLst;
	}
	public void setOrderItemLst(List<OrderItem> orderItemLst) {
		this.orderItemLst = orderItemLst;
	}
	public List<CRMLogger> getLoggerUpdate() {
		return loggerUpdate;
	}
	public void setLoggerUpdate(List<CRMLogger> loggerUpdate) {
		this.loggerUpdate = loggerUpdate;
	}
	public List<OrderItem> getOldOrderItemList() {
		return oldOrderItemList;
	}
	public void setOldOrderItemList(List<OrderItem> oldOrderItemList) {
		this.oldOrderItemList = oldOrderItemList;
	}
	public String getGoodsDesc() {
		return goodsDesc;
	}
	public void setGoodsDesc(String goodsDesc) {
		this.goodsDesc = goodsDesc;
	}
	public String getShippingMark() {
		return shippingMark;
	}
	public void setShippingMark(String shippingMark) {
		this.shippingMark = shippingMark;
	}
	
	
}
