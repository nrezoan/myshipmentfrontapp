package com.myshipment.model;

public class CarrierMaster {
	private Long carrier_id;
	private String  carrier_name;
	private String carrier_code;
	private String address;
	private String phone;
	private String active;
	public Long getCarrier_id() {
		return carrier_id;
	}
	public void setCarrier_id(Long carrier_id) {
		this.carrier_id = carrier_id;
	}
	public String getCarrier_name() {
		return carrier_name;
	}
	public void setCarrier_name(String carrier_name) {
		this.carrier_name = carrier_name;
	}
	public String getCarrier_code() {
		return carrier_code;
	}
	public void setCarrier_code(String carrier_code) {
		this.carrier_code = carrier_code;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	@Override
	public String toString() {
		return "CarrierMaster [carrier_id=" + carrier_id + ", carrier_name=" + carrier_name + ", carrier_code="
				+ carrier_code + ", address=" + address + ", phone=" + phone + ", active=" + active + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((active == null) ? 0 : active.hashCode());
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((carrier_code == null) ? 0 : carrier_code.hashCode());
		result = prime * result + ((carrier_id == null) ? 0 : carrier_id.hashCode());
		result = prime * result + ((carrier_name == null) ? 0 : carrier_name.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CarrierMaster other = (CarrierMaster) obj;
		if (active == null) {
			if (other.active != null)
				return false;
		} else if (!active.equals(other.active))
			return false;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (carrier_code == null) {
			if (other.carrier_code != null)
				return false;
		} else if (!carrier_code.equals(other.carrier_code))
			return false;
		if (carrier_id == null) {
			if (other.carrier_id != null)
				return false;
		} else if (!carrier_id.equals(other.carrier_id))
			return false;
		if (carrier_name == null) {
			if (other.carrier_name != null)
				return false;
		} else if (!carrier_name.equals(other.carrier_name))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		return true;
	}
	

}
