package com.myshipment.model;


public class Salesareas {

	private String customer;
	private String salesorg;
	private String distrchn;
	private String division;
	
	
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public void setSalesorg(String salesorg) {
		this.salesorg = salesorg;
	}
	public void setDistrchn(String distrchn) {
		this.distrchn = distrchn;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getSalesorg() {
		return salesorg;
	}
	public String getDistrchn() {
		return distrchn;
	}
	public String getDivision() {
		return division;
	}
	
	
}
