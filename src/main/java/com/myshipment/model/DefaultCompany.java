/**
*
*@author Ahmad Naquib
*/
package com.myshipment.model;

public class DefaultCompany {

	private String userId;
	private String preferredCompanyId;
	private String preferredCompanyName;
	private String distributionChannel;
	private String division;
	private String active;
	private String updatedDate;
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getActive() {
		return active;
	}
	public void setActive(String active) {
		this.active = active;
	}
	public String getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(String updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getPreferredCompanyId() {
		return preferredCompanyId;
	}
	public void setPreferredCompanyId(String preferredCompanyId) {
		this.preferredCompanyId = preferredCompanyId;
	}
	public String getPreferredCompanyName() {
		return preferredCompanyName;
	}
	public void setPreferredCompanyName(String preferredCompanyName) {
		this.preferredCompanyName = preferredCompanyName;
	}
	public String getDistributionChannel() {
		return distributionChannel;
	}
	public void setDistributionChannel(String distributionChannel) {
		this.distributionChannel = distributionChannel;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	
}
