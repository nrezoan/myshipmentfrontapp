package com.myshipment.model;
/*
 * @ Gufranur Rahman
 */


public class UomBean {
	
	private String spRas;
	private String msEhi;
	private String msEh3;
	private String msEh6;
	private String msEht;
	private String msEhl;
	public String getSpRas() {
		return spRas;
	}
	public void setSpRas(String spRas) {
		this.spRas = spRas;
	}
	public String getMsEhi() {
		return msEhi;
	}
	public void setMsEhi(String msEhi) {
		this.msEhi = msEhi;
	}
	public String getMsEh3() {
		return msEh3;
	}
	public void setMsEh3(String msEh3) {
		this.msEh3 = msEh3;
	}
	public String getMsEh6() {
		return msEh6;
	}
	public void setMsEh6(String msEh6) {
		this.msEh6 = msEh6;
	}
	public String getMsEht() {
		return msEht;
	}
	public void setMsEht(String msEht) {
		this.msEht = msEht;
	}
	public String getMsEhl() {
		return msEhl;
	}
	public void setMsEhl(String msEhl) {
		this.msEhl = msEhl;
	}
	


	
}
