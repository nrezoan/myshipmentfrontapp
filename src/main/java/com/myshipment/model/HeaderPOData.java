package com.myshipment.model;

import java.util.Date;
/*
 * @ Ranjeet Kumar
 */
public class HeaderPOData {
	
	private Long po_id;
	private int upload_id;
	private String vc_po_no;
	private String nu_client_code;
	private String vc_hs_code;
	private String sales_org;
	private String vc_buyer;
	private String vc_buy_house;
	private String company_code;
	private String supplier_code;
	private Date currentDate;
	

	public int getUpload_id() {
		return upload_id;
	}
	public void setUpload_id(int upload_id) {
		this.upload_id = upload_id;
	}
	public String getVc_po_no() {
		return vc_po_no;
	}
	public void setVc_po_no(String vc_po_no) {
		this.vc_po_no = vc_po_no;
	}
	public String getNu_client_code() {
		return nu_client_code;
	}
	public void setNu_client_code(String nu_client_code) {
		this.nu_client_code = nu_client_code;
	}
	public String getVc_hs_code() {
		return vc_hs_code;
	}
	public void setVc_hs_code(String vc_hs_code) {
		this.vc_hs_code = vc_hs_code;
	}
	public String getSales_org() {
		return sales_org;
	}
	public void setSales_org(String sales_org) {
		this.sales_org = sales_org;
	}
	public String getVc_buyer() {
		return vc_buyer;
	}
	public void setVc_buyer(String vc_buyer) {
		this.vc_buyer = vc_buyer;
	}
	public String getVc_buy_house() {
		return vc_buy_house;
	}
	public void setVc_buy_house(String vc_buy_house) {
		this.vc_buy_house = vc_buy_house;
	}
	public String getCompany_code() {
		return company_code;
	}
	public void setCompany_code(String company_code) {
		this.company_code = company_code;
	}
	public String getSupplier_code() {
		return supplier_code;
	}
	public void setSupplier_code(String supplier_code) {
		this.supplier_code = supplier_code;
	}
	public Date getCurrentDate() {
		return currentDate;
	}
	public void setCurrentDate(Date currentDate) {
		this.currentDate = currentDate;
	}
	public Long getPo_id() {
		return po_id;
	}
	public void setPo_id(Long po_id) {
		this.po_id = po_id;
	}
	
	
}
