package com.myshipment.model;

import java.util.List;

public class InvoiceListJsonData {

	private List<Invoice> invoice;
	private List<InvoiceList> invoiceList;

	public List<InvoiceList> getInvoiceList() {
		return invoiceList;
	}

	public void setInvoiceList(List<InvoiceList> invoiceList) {
		this.invoiceList = invoiceList;
	}

	public List<Invoice> getInvoice() {
		return invoice;
	}

	public void setInvoice(List<Invoice> invoice) {
		this.invoice = invoice;
	}

}
