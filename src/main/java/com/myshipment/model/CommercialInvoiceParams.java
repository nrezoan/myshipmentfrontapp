package com.myshipment.model;
/*
 * @Ranjeet Kumar
 */


public class CommercialInvoiceParams {

	private String kunnr;	
	private String hblnumber;	
	private String ponumber;	
/*	private String kna1Kunnr;	
	private String vbakZzhblhawbno;	
	private String vbapZzponumber;*/
	public String getKunnr() {
		return kunnr;
	}
	public void setKunnr(String kunnr) {
		this.kunnr = kunnr;
	}
	public String getHblnumber() {
		return hblnumber;
	}
	public void setHblnumber(String hblnumber) {
		this.hblnumber = hblnumber;
	}
	public String getPonumber() {
		return ponumber;
	}
	public void setPonumber(String ponumber) {
		this.ponumber = ponumber;
	}
/*	public String getKna1Kunnr() {
		return kna1Kunnr;
	}
	public void setKna1Kunnr(String kna1Kunnr) {
		this.kna1Kunnr = kna1Kunnr;
	}
	public String getVbakZzhblhawbno() {
		return vbakZzhblhawbno;
	}
	public void setVbakZzhblhawbno(String vbakZzhblhawbno) {
		this.vbakZzhblhawbno = vbakZzhblhawbno;
	}
	public String getVbapZzponumber() {
		return vbapZzponumber;
	}
	public void setVbapZzponumber(String vbapZzponumber) {
		this.vbapZzponumber = vbapZzponumber;
	}
*/	
	
}
