package com.myshipment.model;
/*
 * @Ranjeet Kumar
 */
public class POFileInfo {

	private Long file_id;
	private String fileDirectory;
	private String fileName;
	private String completeFilePath;
	private String fileContent;
	private String created_on;
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}	
	public String getFileContent() {
		return fileContent;
	}
	public void setFileContent(String fileContent) {
		this.fileContent = fileContent;
	}
	public String getFileDirectory() {
		return fileDirectory;
	}
	public void setFileDirectory(String fileDirectory) {
		this.fileDirectory = fileDirectory;
	}
	public String getCompleteFilePath() {
		return completeFilePath;
	}
	public void setCompleteFilePath(String completeFilePath) {
		this.completeFilePath = completeFilePath;
	}
	public String getCreated_on() {
		return created_on;
	}
	public void setCreated_on(String created_on) {
		this.created_on = created_on;
	}
	public Long getFile_id() {
		return file_id;
	}
	public void setFile_id(Long file_id) {
		this.file_id = file_id;
	}
	
	
	
	
}
