package com.myshipment.model;

public class BuyerWiseGWTJson {
	private String buyerNo;
	private String buyerName;
	private double totalGWT;
	private double totalGWTPerc;
	public String getBuyerNo() {
		return buyerNo;
	}
	public void setBuyerNo(String buyerNo) {
		this.buyerNo = buyerNo;
	}
	public String getBuyerName() {
		return buyerName;
	}
	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}
	public double getTotalGWT() {
		return totalGWT;
	}
	public void setTotalGWT(double totalGWT) {
		this.totalGWT = totalGWT;
	}
	public double getTotalGWTPerc() {
		return totalGWTPerc;
	}
	public void setTotalGWTPerc(double totalGWTPerc) {
		this.totalGWTPerc = totalGWTPerc;
	}

}
