package com.myshipment.model;

import java.util.List;

/*
 * @Sanjana Khan
 */
public class RedirectParamsPOForm {

	private Long poId;
	private List<ApprovedPurchaseOrder> poIdLst;
	private String supplierName;
	
	
	public String getSupplierName() {
		return supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public List<ApprovedPurchaseOrder> getPoIdLst() {
		return poIdLst;
	}

	public void setPoIdLst(List<ApprovedPurchaseOrder> poIdLst) {
		this.poIdLst = poIdLst;
	}

	public Long getPoId() {
		return poId;
	}

	public void setPoId(Long poId) {
		this.poId = poId;
	}


	
}
