package com.myshipment.model;

import java.util.List;

public class ShipmentDetailsJson {
	private List<ShipmentDetails> lstShipmentDetails;

	public List<ShipmentDetails> getLstShipmentDetails() {
		return lstShipmentDetails;
	}

	public void setLstShipmentDetails(List<ShipmentDetails> lstShipmentDetails) {
		this.lstShipmentDetails = lstShipmentDetails;
	}
	

}
