package com.myshipment.model;

public class TLines {
	private String tdFormat;
	private String tdLine;

	public String getTdFormat() {
		return tdFormat;
	}

	public void setTdFormat(String tdFormat) {
		this.tdFormat = tdFormat;
	}

	public String getTdLine() {
		return tdLine;
	}

	public void setTdLine(String tdLine) {
		this.tdLine = tdLine;
	}

	
	
}