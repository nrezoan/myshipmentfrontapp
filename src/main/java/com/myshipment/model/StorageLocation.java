package com.myshipment.model;

public class StorageLocation {
	private String id;
	private String storageId;
	private String storageDescription;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStorageId() {
		return storageId;
	}
	public void setStorageId(String storageId) {
		this.storageId = storageId;
	}
	public String getStorageDescription() {
		return storageDescription;
	}
	public void setStorageDescription(String storageDescription) {
		this.storageDescription = storageDescription;
	}
	
	

}
