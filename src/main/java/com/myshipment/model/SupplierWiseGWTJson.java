package com.myshipment.model;

public class SupplierWiseGWTJson {
	private String shipperNo;
	private String shipperName;
	private double totalGWT;
	private double totalGWTPerc;
	public String getShipperNo() {
		return shipperNo;
	}
	public void setShipperNo(String shipperNo) {
		this.shipperNo = shipperNo;
	}
	public String getShipperName() {
		return shipperName;
	}
	public void setShipperName(String shipperName) {
		this.shipperName = shipperName;
	}
	public double getTotalGWT() {
		return totalGWT;
	}
	public void setTotalGWT(double totalGWT) {
		this.totalGWT = totalGWT;
	}
	public double getTotalGWTPerc() {
		return totalGWTPerc;
	}
	public void setTotalGWTPerc(double totalGWTPerc) {
		this.totalGWTPerc = totalGWTPerc;
	}
	

}
