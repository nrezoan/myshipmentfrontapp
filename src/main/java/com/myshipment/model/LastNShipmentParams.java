package com.myshipment.model;
/*
 * @Ranjeet Kumar
 */
public class LastNShipmentParams {

	//Customer Number1
	private String kunnr;	
	//Natural Number
	private Integer number;
	//private String number;	
	//Sales Organization
	private String vkorg;	
	//Distibution Channel
	private String vtweg;	
	//Division
	private String spart;
	public String getKunnr() {
		return kunnr;
	}
	public void setKunnr(String kunnr) {
		this.kunnr = kunnr;
	}

	public String getVkorg() {
		return vkorg;
	}
	public void setVkorg(String vkorg) {
		this.vkorg = vkorg;
	}
	public String getVtweg() {
		return vtweg;
	}
	public void setVtweg(String vtweg) {
		this.vtweg = vtweg;
	}
	public String getSpart() {
		return spart;
	}
	public void setSpart(String spart) {
		this.spart = spart;
	}
	public Integer getNumber() {
		return number;
	}
	public void setNumber(Integer number) {
		this.number = number;
	}
	
	
}
