package com.myshipment.model;
/*
 * @Ranjeet Kumar
 */
public class StuffingReportTotalData {

	private String totPcs;
	private String totVol;
	private String totQty;
	private String totKgs;
	public String getTotPcs() {
		return totPcs;
	}
	public void setTotPcs(String totPcs) {
		this.totPcs = totPcs;
	}
	public String getTotVol() {
		return totVol;
	}
	public void setTotVol(String totVol) {
		this.totVol = totVol;
	}
	public String getTotQty() {
		return totQty;
	}
	public void setTotQty(String totQty) {
		this.totQty = totQty;
	}
	public String getTotKgs() {
		return totKgs;
	}
	public void setTotKgs(String totKgs) {
		this.totKgs = totKgs;
	}
	
	
}
