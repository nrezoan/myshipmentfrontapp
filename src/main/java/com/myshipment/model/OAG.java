package com.myshipment.model;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.myshipment.util.LocalDateParseDeserializer;
import com.myshipment.util.LocalDateTimeParseDeserializer;

public class OAG {

	private String vcFlightNo;

	private String vcAirlineCode;
	private String vcAirlineName;
	private String vcAircraftType;
	private String vcWeightClass;
	private String vcTailNumber;
	private String vcFlightStatus;
	private String vcFlightStatusType;

	private String chSvcType;
	private String chSchedInfoPresent;

	private String vcRelativeTime;
	private String vcRelativeTimeStatus;

	private String nuSeqNo;
	private String nuNoLegs;

	private String vcAirborneAltitude;
	private String vcAirborneSpeed;
	private String vcAirporneLatitude;
	private String vcAirporneLongitude;

	/**
	 * DEPARTURE AIRPORT
	 **/
	private String vcDepAirportCode;
	private String vcDepAirportName;
	private String vcDepAirportCityName;
	private String vcDepAirportCountryId;
	private String vcDepStatus;

	// ACTUAL
	@JsonSerialize(using = ToStringSerializer.class)
	@JsonDeserialize(using = LocalDateParseDeserializer.class)
	private LocalDate dtDepRtActDate;
	private String vcDepRtActTime;
	private String vcDepRtActType;

	// ESTIMATED
	@JsonSerialize(using = ToStringSerializer.class)
	@JsonDeserialize(using = LocalDateParseDeserializer.class)
	private LocalDate dtDepRtEstDate;
	private String vcDepRtEstTime;
	private String vcDepRtEstType;

	// SCHEDULED
	@JsonSerialize(using = ToStringSerializer.class)
	@JsonDeserialize(using = LocalDateParseDeserializer.class)
	private LocalDate dtDepRtSchDate;
	private String vcDepRtSchTime;
	private String vcDepRtSchType;

	// ACTUAL
	@JsonSerialize(using = ToStringSerializer.class)
	@JsonDeserialize(using = LocalDateParseDeserializer.class)
	private LocalDate dtDepGtActDate;
	private String vcDepGtActTime;
	private String vcDepGtActType;

	// ESTIMATED
	@JsonSerialize(using = ToStringSerializer.class)
	@JsonDeserialize(using = LocalDateParseDeserializer.class)
	private LocalDate dtDepGtEstDate;
	private String vcDepGtEstTime;
	private String vcDepGtEstType;

	// SCHEDULED
	@JsonSerialize(using = ToStringSerializer.class)
	@JsonDeserialize(using = LocalDateParseDeserializer.class)
	private LocalDate dtDepGtSchDate;
	private String vcDepGtSchTime;
	private String vcDepGtSchType;

	/**
	 * DEPARTURE AIRPORT
	 **/
	private String vcArrAirportCode;
	private String vcArrAirportName;
	private String vcArrAirportCityName;
	private String vcArrAirportCountryId;
	private String vcArrStatus;

	// ACTUAL
	@JsonSerialize(using = ToStringSerializer.class)
	@JsonDeserialize(using = LocalDateParseDeserializer.class)
	private LocalDate dtArrRtActDate;
	private String vcArrRtActTime;
	private String vcArrRtActType;

	// ESTIMATED
	@JsonSerialize(using = ToStringSerializer.class)
	@JsonDeserialize(using = LocalDateParseDeserializer.class)
	private LocalDate dtArrRtEstDate;
	private String vcArrRtEstTime;
	private String vcArrRtEstType;

	// SCHEDULED
	@JsonSerialize(using = ToStringSerializer.class)
	@JsonDeserialize(using = LocalDateParseDeserializer.class)
	private LocalDate dtArrRtSchDate;
	private String vcArrRtSchTime;
	private String vcArrRtSchType;

	// ACTUAL
	@JsonSerialize(using = ToStringSerializer.class)
	@JsonDeserialize(using = LocalDateParseDeserializer.class)
	private LocalDate dtArrGtActDate;
	private String vcArrGtActTime;
	private String vcArrGtActType;

	// ESTIMATED
	@JsonSerialize(using = ToStringSerializer.class)
	@JsonDeserialize(using = LocalDateParseDeserializer.class)
	private LocalDate dtArrGtEstDate;
	private String vcArrGtEstTime;
	private String vcArrGtEstType;

	// SCHEDULED
	@JsonSerialize(using = ToStringSerializer.class)
	@JsonDeserialize(using = LocalDateParseDeserializer.class)
	private LocalDate dtArrGtSchDate;
	private String vcArrGtSchTime;
	private String vcArrGtSchType;

	private String vcDepAirportTerminal;
	private String vcArrAirportTerminal;

	private String vcRoute;

	private String vcResultStatus;
	private String vcResultCode;
	private String vcResultMessage;
	@JsonSerialize(using = ToStringSerializer.class)
	@JsonDeserialize(using = LocalDateParseDeserializer.class)
	private LocalDate dtQueryDate;
	private String vcQueryTime;

	private Long nuApiCalled;

	private String systemMessage;

	@JsonSerialize(using = ToStringSerializer.class)
	@JsonDeserialize(using = LocalDateParseDeserializer.class)
	private LocalDate dtFlightOrigDate;

	@JsonSerialize(using = ToStringSerializer.class)
	@JsonDeserialize(using = LocalDateParseDeserializer.class)
	private LocalDate dtDepRunDate;

	@JsonSerialize(using = ToStringSerializer.class)
	@JsonDeserialize(using = LocalDateParseDeserializer.class)
	private LocalDate dtDepGateDate;

	@JsonSerialize(using = ToStringSerializer.class)
	@JsonDeserialize(using = LocalDateParseDeserializer.class)
	private LocalDate dtArrRunDate;

	@JsonSerialize(using = ToStringSerializer.class)
	@JsonDeserialize(using = LocalDateParseDeserializer.class)
	private LocalDate dtArrGateDate;

	@JsonSerialize(using = ToStringSerializer.class)
	@JsonDeserialize(using = LocalDateTimeParseDeserializer.class)
	private LocalDateTime dtUpdatedAt;

	@JsonSerialize(using = ToStringSerializer.class)
	@JsonDeserialize(using = LocalDateParseDeserializer.class)
	private LocalDate vcQueryDate;

	private int leg;

	
	
	public String getVcFlightNo() {
		return vcFlightNo;
	}

	public void setVcFlightNo(String vcFlightNo) {
		this.vcFlightNo = vcFlightNo;
	}

	public String getVcAirlineCode() {
		return vcAirlineCode;
	}

	public void setVcAirlineCode(String vcAirlineCode) {
		this.vcAirlineCode = vcAirlineCode;
	}

	public String getVcAirlineName() {
		return vcAirlineName;
	}

	public void setVcAirlineName(String vcAirlineName) {
		this.vcAirlineName = vcAirlineName;
	}

	public String getVcAircraftType() {
		return vcAircraftType;
	}

	public void setVcAircraftType(String vcAircraftType) {
		this.vcAircraftType = vcAircraftType;
	}

	public String getVcWeightClass() {
		return vcWeightClass;
	}

	public void setVcWeightClass(String vcWeightClass) {
		this.vcWeightClass = vcWeightClass;
	}

	public String getVcTailNumber() {
		return vcTailNumber;
	}

	public void setVcTailNumber(String vcTailNumber) {
		this.vcTailNumber = vcTailNumber;
	}

	public String getVcFlightStatus() {
		return vcFlightStatus;
	}

	public void setVcFlightStatus(String vcFlightStatus) {
		this.vcFlightStatus = vcFlightStatus;
	}

	public String getVcFlightStatusType() {
		return vcFlightStatusType;
	}

	public void setVcFlightStatusType(String vcFlightStatusType) {
		this.vcFlightStatusType = vcFlightStatusType;
	}

	public String getChSvcType() {
		return chSvcType;
	}

	public void setChSvcType(String chSvcType) {
		this.chSvcType = chSvcType;
	}

	public String getChSchedInfoPresent() {
		return chSchedInfoPresent;
	}

	public void setChSchedInfoPresent(String chSchedInfoPresent) {
		this.chSchedInfoPresent = chSchedInfoPresent;
	}

	public String getVcRelativeTime() {
		return vcRelativeTime;
	}

	public void setVcRelativeTime(String vcRelativeTime) {
		this.vcRelativeTime = vcRelativeTime;
	}

	public String getVcRelativeTimeStatus() {
		return vcRelativeTimeStatus;
	}

	public void setVcRelativeTimeStatus(String vcRelativeTimeStatus) {
		this.vcRelativeTimeStatus = vcRelativeTimeStatus;
	}

	public String getNuSeqNo() {
		return nuSeqNo;
	}

	public void setNuSeqNo(String nuSeqNo) {
		this.nuSeqNo = nuSeqNo;
	}

	public String getNuNoLegs() {
		return nuNoLegs;
	}

	public void setNuNoLegs(String nuNoLegs) {
		this.nuNoLegs = nuNoLegs;
	}

	public String getVcAirborneAltitude() {
		return vcAirborneAltitude;
	}

	public void setVcAirborneAltitude(String vcAirborneAltitude) {
		this.vcAirborneAltitude = vcAirborneAltitude;
	}

	public String getVcAirborneSpeed() {
		return vcAirborneSpeed;
	}

	public void setVcAirborneSpeed(String vcAirborneSpeed) {
		this.vcAirborneSpeed = vcAirborneSpeed;
	}

	public String getVcAirporneLatitude() {
		return vcAirporneLatitude;
	}

	public void setVcAirporneLatitude(String vcAirporneLatitude) {
		this.vcAirporneLatitude = vcAirporneLatitude;
	}

	public String getVcAirporneLongitude() {
		return vcAirporneLongitude;
	}

	public void setVcAirporneLongitude(String vcAirporneLongitude) {
		this.vcAirporneLongitude = vcAirporneLongitude;
	}

	public String getVcDepAirportCode() {
		return vcDepAirportCode;
	}

	public void setVcDepAirportCode(String vcDepAirportCode) {
		this.vcDepAirportCode = vcDepAirportCode;
	}

	public String getVcDepAirportName() {
		return vcDepAirportName;
	}

	public void setVcDepAirportName(String vcDepAirportName) {
		this.vcDepAirportName = vcDepAirportName;
	}

	public String getVcDepAirportCityName() {
		return vcDepAirportCityName;
	}

	public void setVcDepAirportCityName(String vcDepAirportCityName) {
		this.vcDepAirportCityName = vcDepAirportCityName;
	}

	public String getVcDepAirportCountryId() {
		return vcDepAirportCountryId;
	}

	public void setVcDepAirportCountryId(String vcDepAirportCountryId) {
		this.vcDepAirportCountryId = vcDepAirportCountryId;
	}

	public String getVcDepStatus() {
		return vcDepStatus;
	}

	public void setVcDepStatus(String vcDepStatus) {
		this.vcDepStatus = vcDepStatus;
	}

	public LocalDate getDtDepRtActDate() {
		return dtDepRtActDate;
	}

	public void setDtDepRtActDate(LocalDate dtDepRtActDate) {
		this.dtDepRtActDate = dtDepRtActDate;
	}

	public String getVcDepRtActTime() {
		return vcDepRtActTime;
	}

	public void setVcDepRtActTime(String vcDepRtActTime) {
		this.vcDepRtActTime = vcDepRtActTime;
	}

	public String getVcDepRtActType() {
		return vcDepRtActType;
	}

	public void setVcDepRtActType(String vcDepRtActType) {
		this.vcDepRtActType = vcDepRtActType;
	}

	public LocalDate getDtDepRtEstDate() {
		return dtDepRtEstDate;
	}

	public void setDtDepRtEstDate(LocalDate dtDepRtEstDate) {
		this.dtDepRtEstDate = dtDepRtEstDate;
	}

	public String getVcDepRtEstTime() {
		return vcDepRtEstTime;
	}

	public void setVcDepRtEstTime(String vcDepRtEstTime) {
		this.vcDepRtEstTime = vcDepRtEstTime;
	}

	public String getVcDepRtEstType() {
		return vcDepRtEstType;
	}

	public void setVcDepRtEstType(String vcDepRtEstType) {
		this.vcDepRtEstType = vcDepRtEstType;
	}

	public LocalDate getDtDepRtSchDate() {
		return dtDepRtSchDate;
	}

	public void setDtDepRtSchDate(LocalDate dtDepRtSchDate) {
		this.dtDepRtSchDate = dtDepRtSchDate;
	}

	public String getVcDepRtSchTime() {
		return vcDepRtSchTime;
	}

	public void setVcDepRtSchTime(String vcDepRtSchTime) {
		this.vcDepRtSchTime = vcDepRtSchTime;
	}

	public String getVcDepRtSchType() {
		return vcDepRtSchType;
	}

	public void setVcDepRtSchType(String vcDepRtSchType) {
		this.vcDepRtSchType = vcDepRtSchType;
	}

	public LocalDate getDtDepGtActDate() {
		return dtDepGtActDate;
	}

	public void setDtDepGtActDate(LocalDate dtDepGtActDate) {
		this.dtDepGtActDate = dtDepGtActDate;
	}

	public String getVcDepGtActTime() {
		return vcDepGtActTime;
	}

	public void setVcDepGtActTime(String vcDepGtActTime) {
		this.vcDepGtActTime = vcDepGtActTime;
	}

	public String getVcDepGtActType() {
		return vcDepGtActType;
	}

	public void setVcDepGtActType(String vcDepGtActType) {
		this.vcDepGtActType = vcDepGtActType;
	}

	public LocalDate getDtDepGtEstDate() {
		return dtDepGtEstDate;
	}

	public void setDtDepGtEstDate(LocalDate dtDepGtEstDate) {
		this.dtDepGtEstDate = dtDepGtEstDate;
	}

	public String getVcDepGtEstTime() {
		return vcDepGtEstTime;
	}

	public void setVcDepGtEstTime(String vcDepGtEstTime) {
		this.vcDepGtEstTime = vcDepGtEstTime;
	}

	public String getVcDepGtEstType() {
		return vcDepGtEstType;
	}

	public void setVcDepGtEstType(String vcDepGtEstType) {
		this.vcDepGtEstType = vcDepGtEstType;
	}

	public LocalDate getDtDepGtSchDate() {
		return dtDepGtSchDate;
	}

	public void setDtDepGtSchDate(LocalDate dtDepGtSchDate) {
		this.dtDepGtSchDate = dtDepGtSchDate;
	}

	public String getVcDepGtSchTime() {
		return vcDepGtSchTime;
	}

	public void setVcDepGtSchTime(String vcDepGtSchTime) {
		this.vcDepGtSchTime = vcDepGtSchTime;
	}

	public String getVcDepGtSchType() {
		return vcDepGtSchType;
	}

	public void setVcDepGtSchType(String vcDepGtSchType) {
		this.vcDepGtSchType = vcDepGtSchType;
	}

	public String getVcArrAirportCode() {
		return vcArrAirportCode;
	}

	public void setVcArrAirportCode(String vcArrAirportCode) {
		this.vcArrAirportCode = vcArrAirportCode;
	}

	public String getVcArrAirportName() {
		return vcArrAirportName;
	}

	public void setVcArrAirportName(String vcArrAirportName) {
		this.vcArrAirportName = vcArrAirportName;
	}

	public String getVcArrAirportCityName() {
		return vcArrAirportCityName;
	}

	public void setVcArrAirportCityName(String vcArrAirportCityName) {
		this.vcArrAirportCityName = vcArrAirportCityName;
	}

	public String getVcArrAirportCountryId() {
		return vcArrAirportCountryId;
	}

	public void setVcArrAirportCountryId(String vcArrAirportCountryId) {
		this.vcArrAirportCountryId = vcArrAirportCountryId;
	}

	public String getVcArrStatus() {
		return vcArrStatus;
	}

	public void setVcArrStatus(String vcArrStatus) {
		this.vcArrStatus = vcArrStatus;
	}

	public LocalDate getDtArrRtActDate() {
		return dtArrRtActDate;
	}

	public void setDtArrRtActDate(LocalDate dtArrRtActDate) {
		this.dtArrRtActDate = dtArrRtActDate;
	}

	public String getVcArrRtActTime() {
		return vcArrRtActTime;
	}

	public void setVcArrRtActTime(String vcArrRtActTime) {
		this.vcArrRtActTime = vcArrRtActTime;
	}

	public String getVcArrRtActType() {
		return vcArrRtActType;
	}

	public void setVcArrRtActType(String vcArrRtActType) {
		this.vcArrRtActType = vcArrRtActType;
	}

	public LocalDate getDtArrRtEstDate() {
		return dtArrRtEstDate;
	}

	public void setDtArrRtEstDate(LocalDate dtArrRtEstDate) {
		this.dtArrRtEstDate = dtArrRtEstDate;
	}

	public String getVcArrRtEstTime() {
		return vcArrRtEstTime;
	}

	public void setVcArrRtEstTime(String vcArrRtEstTime) {
		this.vcArrRtEstTime = vcArrRtEstTime;
	}

	public String getVcArrRtEstType() {
		return vcArrRtEstType;
	}

	public void setVcArrRtEstType(String vcArrRtEstType) {
		this.vcArrRtEstType = vcArrRtEstType;
	}

	public LocalDate getDtArrRtSchDate() {
		return dtArrRtSchDate;
	}

	public void setDtArrRtSchDate(LocalDate dtArrRtSchDate) {
		this.dtArrRtSchDate = dtArrRtSchDate;
	}

	public String getVcArrRtSchTime() {
		return vcArrRtSchTime;
	}

	public void setVcArrRtSchTime(String vcArrRtSchTime) {
		this.vcArrRtSchTime = vcArrRtSchTime;
	}

	public String getVcArrRtSchType() {
		return vcArrRtSchType;
	}

	public void setVcArrRtSchType(String vcArrRtSchType) {
		this.vcArrRtSchType = vcArrRtSchType;
	}

	public LocalDate getDtArrGtActDate() {
		return dtArrGtActDate;
	}

	public void setDtArrGtActDate(LocalDate dtArrGtActDate) {
		this.dtArrGtActDate = dtArrGtActDate;
	}

	public String getVcArrGtActTime() {
		return vcArrGtActTime;
	}

	public void setVcArrGtActTime(String vcArrGtActTime) {
		this.vcArrGtActTime = vcArrGtActTime;
	}

	public String getVcArrGtActType() {
		return vcArrGtActType;
	}

	public void setVcArrGtActType(String vcArrGtActType) {
		this.vcArrGtActType = vcArrGtActType;
	}

	public LocalDate getDtArrGtEstDate() {
		return dtArrGtEstDate;
	}

	public void setDtArrGtEstDate(LocalDate dtArrGtEstDate) {
		this.dtArrGtEstDate = dtArrGtEstDate;
	}

	public String getVcArrGtEstTime() {
		return vcArrGtEstTime;
	}

	public void setVcArrGtEstTime(String vcArrGtEstTime) {
		this.vcArrGtEstTime = vcArrGtEstTime;
	}

	public String getVcArrGtEstType() {
		return vcArrGtEstType;
	}

	public void setVcArrGtEstType(String vcArrGtEstType) {
		this.vcArrGtEstType = vcArrGtEstType;
	}

	public LocalDate getDtArrGtSchDate() {
		return dtArrGtSchDate;
	}

	public void setDtArrGtSchDate(LocalDate dtArrGtSchDate) {
		this.dtArrGtSchDate = dtArrGtSchDate;
	}

	public String getVcArrGtSchTime() {
		return vcArrGtSchTime;
	}

	public void setVcArrGtSchTime(String vcArrGtSchTime) {
		this.vcArrGtSchTime = vcArrGtSchTime;
	}

	public String getVcArrGtSchType() {
		return vcArrGtSchType;
	}

	public void setVcArrGtSchType(String vcArrGtSchType) {
		this.vcArrGtSchType = vcArrGtSchType;
	}

	public String getVcDepAirportTerminal() {
		return vcDepAirportTerminal;
	}

	public void setVcDepAirportTerminal(String vcDepAirportTerminal) {
		this.vcDepAirportTerminal = vcDepAirportTerminal;
	}

	public String getVcArrAirportTerminal() {
		return vcArrAirportTerminal;
	}

	public void setVcArrAirportTerminal(String vcArrAirportTerminal) {
		this.vcArrAirportTerminal = vcArrAirportTerminal;
	}

	public String getVcRoute() {
		return vcRoute;
	}

	public void setVcRoute(String vcRoute) {
		this.vcRoute = vcRoute;
	}

	public String getVcResultStatus() {
		return vcResultStatus;
	}

	public void setVcResultStatus(String vcResultStatus) {
		this.vcResultStatus = vcResultStatus;
	}

	public String getVcResultCode() {
		return vcResultCode;
	}

	public void setVcResultCode(String vcResultCode) {
		this.vcResultCode = vcResultCode;
	}

	public String getVcResultMessage() {
		return vcResultMessage;
	}

	public void setVcResultMessage(String vcResultMessage) {
		this.vcResultMessage = vcResultMessage;
	}

	public LocalDate getDtQueryDate() {
		return dtQueryDate;
	}

	public void setDtQueryDate(LocalDate dtQueryDate) {
		this.dtQueryDate = dtQueryDate;
	}

	public String getVcQueryTime() {
		return vcQueryTime;
	}

	public void setVcQueryTime(String vcQueryTime) {
		this.vcQueryTime = vcQueryTime;
	}

	public Long getNuApiCalled() {
		return nuApiCalled;
	}

	public void setNuApiCalled(Long nuApiCalled) {
		this.nuApiCalled = nuApiCalled;
	}

	public String getSystemMessage() {
		return systemMessage;
	}

	public void setSystemMessage(String systemMessage) {
		this.systemMessage = systemMessage;
	}

	public LocalDate getDtFlightOrigDate() {
		return dtFlightOrigDate;
	}

	public void setDtFlightOrigDate(LocalDate dtFlightOrigDate) {
		this.dtFlightOrigDate = dtFlightOrigDate;
	}

	public LocalDate getDtDepRunDate() {
		return dtDepRunDate;
	}

	public void setDtDepRunDate(LocalDate dtDepRunDate) {
		this.dtDepRunDate = dtDepRunDate;
	}

	public LocalDate getDtDepGateDate() {
		return dtDepGateDate;
	}

	public void setDtDepGateDate(LocalDate dtDepGateDate) {
		this.dtDepGateDate = dtDepGateDate;
	}

	public LocalDate getDtArrRunDate() {
		return dtArrRunDate;
	}

	public void setDtArrRunDate(LocalDate dtArrRunDate) {
		this.dtArrRunDate = dtArrRunDate;
	}

	public LocalDate getDtArrGateDate() {
		return dtArrGateDate;
	}

	public void setDtArrGateDate(LocalDate dtArrGateDate) {
		this.dtArrGateDate = dtArrGateDate;
	}

	public LocalDateTime getDtUpdatedAt() {
		return dtUpdatedAt;
	}

	public void setDtUpdatedAt(LocalDateTime dtUpdatedAt) {
		this.dtUpdatedAt = dtUpdatedAt;
	}

	public LocalDate getVcQueryDate() {
		return vcQueryDate;
	}

	public void setVcQueryDate(LocalDate vcQueryDate) {
		this.vcQueryDate = vcQueryDate;
	}

	public int getLeg() {
		return leg;
	}

	public void setLeg(int leg) {
		this.leg = leg;
	}
}
