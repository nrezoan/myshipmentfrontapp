package com.myshipment.model;

import java.util.Date;

/*
 * @ Gufranur Rahman
 * @ Nusrat Momtahana
 */

public class StuffingDetail {
	
	
	private int vbeLn;
	private String zzPoNumber;
	private String zzStyleNo;
	private String zzSkuNo;
	private String zzColour;
	private String vhIlm;
	private String wgbEZ60;
	private String vhIlmKu;
	private String zzMode;
	private int zzTotalNoPcs;
	private int vePos;
	private Double zzVolume;
	private int zzQuantity;
	private Double zzGrWt;
	private String bezEi;
	private String belAd;
	private Date dptBg;
	private String knoTa;
	private String knoTz;
	private int posNr;
	private Date zzStuffingDate;
	private Date zzStuffingTime;

	
	
	public String getZzSkuNo() {
		return zzSkuNo;
	}

	public void setZzSkuNo(String zzSkuNo) {
		this.zzSkuNo = zzSkuNo;
	}

	public int getVbeLn() {
		return vbeLn;
	}

	public void setVbeLn(int vbeLn) {
		this.vbeLn = vbeLn;
	}

	public String getZzPoNumber() {
		return zzPoNumber;
	}

	public void setZzPoNumber(String zzPoNumber) {
		this.zzPoNumber = zzPoNumber;
	}

	public String getZzStyleNo() {
		return zzStyleNo;
	}

	public void setZzStyleNo(String zzStyleNo) {
		this.zzStyleNo = zzStyleNo;
	}

	public String getZzColour() {
		return zzColour;
	}

	public void setZzColour(String zzColour) {
		this.zzColour = zzColour;
	}

	public String getVhIlm() {
		return vhIlm;
	}

	public void setVhIlm(String vhIlm) {
		this.vhIlm = vhIlm;
	}

	public String getWgbEZ60() {
		return wgbEZ60;
	}

	public void setWgbEZ60(String wgbEZ60) {
		this.wgbEZ60 = wgbEZ60;
	}

	public String getVhIlmKu() {
		return vhIlmKu;
	}

	public void setVhIlmKu(String vhIlmKu) {
		this.vhIlmKu = vhIlmKu;
	}

	public String getZzMode() {
		return zzMode;
	}

	public void setZzMode(String zzMode) {
		this.zzMode = zzMode;
	}

	public int getZzTotalNoPcs() {
		return zzTotalNoPcs;
	}

	public void setZzTotalNoPcs(int zzTotalNoPcs) {
		this.zzTotalNoPcs = zzTotalNoPcs;
	}

	public int getVePos() {
		return vePos;
	}

	public void setVePos(int vePos) {
		this.vePos = vePos;
	}

	public Double getZzVolume() {
		return zzVolume;
	}

	public void setZzVolume(Double zzVolume) {
		this.zzVolume = zzVolume;
	}

	public int getZzQuantity() {
		return zzQuantity;
	}

	public void setZzQuantity(int zzQuantity) {
		this.zzQuantity = zzQuantity;
	}

	public Double getZzGrWt() {
		return zzGrWt;
	}

	public void setZzGrWt(Double zzGrWt) {
		this.zzGrWt = zzGrWt;
	}

	public String getBezEi() {
		return bezEi;
	}

	public void setBezEi(String bezEi) {
		this.bezEi = bezEi;
	}

	public String getBelAd() {
		return belAd;
	}

	public void setBelAd(String belAd) {
		this.belAd = belAd;
	}

	public Date getDptBg() {
		return dptBg;
	}

	public void setDptBg(Date dptBg) {
		this.dptBg = dptBg;
	}

	public String getKnoTa() {
		return knoTa;
	}

	public void setKnoTa(String knoTa) {
		this.knoTa = knoTa;
	}

	public String getKnoTz() {
		return knoTz;
	}

	public void setKnoTz(String knoTz) {
		this.knoTz = knoTz;
	}

	public int getPosNr() {
		return posNr;
	}

	public void setPosNr(int posNr) {
		this.posNr = posNr;
	}

	public Date getZzStuffingDate() {
		return zzStuffingDate;
	}

	public void setZzStuffingDate(Date zzStuffingDate) {
		this.zzStuffingDate = zzStuffingDate;
	}

	public Date getZzStuffingTime() {
		return zzStuffingTime;
	}

	public void setZzStuffingTime(Date zzStuffingTime) {
		this.zzStuffingTime = zzStuffingTime;
	}
	
	
}
