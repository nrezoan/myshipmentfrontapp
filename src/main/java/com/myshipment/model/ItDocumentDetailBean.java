package com.myshipment.model;

import java.io.Serializable;
import java.util.Date;
/*
 * @Ranjeet Kumar
 */
public class ItDocumentDetailBean implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	private String material_no;	
	private String material_desc;	
	private Integer car_qty;	
	private String po_no;	
	private String style_no;	
	private String sku_no;	
	private String article_no;	
	private String indent_no;	
	private String hs_code;	
	private String color_unit;	
	private String color;	
	private String size1;	
	private double length1;	
	private double width;	
	private double height;	
	private double cbm_per_cartoon;	
	private Integer pcs_per_cartoon;	
	private Integer tot_no_of_pcs;	
	private double volume;	
	private double gr_weight_per_cartoon;	
	private double gr_weight;	
	private String unit_of_weight;	
	private double net_wt_per_cartoon;	
	private double net_weight;	
	private double volume_weight;	
	private String product_code;	
	private String reference_1;	
	private Date reference_1_date;	
	private String reference_2;	
	private Date reference_2_date;	
	private String reference_3;	
	private Date reference_3_date;	
	private String reference_4;	
	private Date reference_4_date;	
	private String reference_5;	
	private Date reference_5_date;		
	private Date rec_creation_date;		
	private Date rec_update_date;
	public String getMaterial_no() {
		return material_no;
	}
	public void setMaterial_no(String material_no) {
		this.material_no = material_no;
	}
	public String getMaterial_desc() {
		return material_desc;
	}
	public void setMaterial_desc(String material_desc) {
		this.material_desc = material_desc;
	}
	public Integer getCar_qty() {
		return car_qty;
	}
	public void setCar_qty(Integer car_qty) {
		this.car_qty = car_qty;
	}
	public String getPo_no() {
		return po_no;
	}
	public void setPo_no(String po_no) {
		this.po_no = po_no;
	}
	public String getStyle_no() {
		return style_no;
	}
	public void setStyle_no(String style_no) {
		this.style_no = style_no;
	}
	public String getSku_no() {
		return sku_no;
	}
	public void setSku_no(String sku_no) {
		this.sku_no = sku_no;
	}
	public String getArticle_no() {
		return article_no;
	}
	public void setArticle_no(String article_no) {
		this.article_no = article_no;
	}
	public String getIndent_no() {
		return indent_no;
	}
	public void setIndent_no(String indent_no) {
		this.indent_no = indent_no;
	}
	public String getHs_code() {
		return hs_code;
	}
	public void setHs_code(String hs_code) {
		this.hs_code = hs_code;
	}
	public String getColor_unit() {
		return color_unit;
	}
	public void setColor_unit(String color_unit) {
		this.color_unit = color_unit;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}

	public double getWidth() {
		return width;
	}
	public void setWidth(double width) {
		this.width = width;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	public double getCbm_per_cartoon() {
		return cbm_per_cartoon;
	}
	public void setCbm_per_cartoon(double cbm_per_cartoon) {
		this.cbm_per_cartoon = cbm_per_cartoon;
	}
	public Integer getPcs_per_cartoon() {
		return pcs_per_cartoon;
	}
	public void setPcs_per_cartoon(Integer pcs_per_cartoon) {
		this.pcs_per_cartoon = pcs_per_cartoon;
	}
	public Integer getTot_no_of_pcs() {
		return tot_no_of_pcs;
	}
	public void setTot_no_of_pcs(Integer tot_no_of_pcs) {
		this.tot_no_of_pcs = tot_no_of_pcs;
	}
	public double getVolume() {
		return volume;
	}
	public void setVolume(double volume) {
		this.volume = volume;
	}
	public double getGr_weight_per_cartoon() {
		return gr_weight_per_cartoon;
	}
	public void setGr_weight_per_cartoon(double gr_weight_per_cartoon) {
		this.gr_weight_per_cartoon = gr_weight_per_cartoon;
	}
	public double getGr_weight() {
		return gr_weight;
	}
	public void setGr_weight(double gr_weight) {
		this.gr_weight = gr_weight;
	}
	public String getUnit_of_weight() {
		return unit_of_weight;
	}
	public void setUnit_of_weight(String unit_of_weight) {
		this.unit_of_weight = unit_of_weight;
	}
	public double getNet_wt_per_cartoon() {
		return net_wt_per_cartoon;
	}
	public void setNet_wt_per_cartoon(double net_wt_per_cartoon) {
		this.net_wt_per_cartoon = net_wt_per_cartoon;
	}
	public double getNet_weight() {
		return net_weight;
	}
	public void setNet_weight(double net_weight) {
		this.net_weight = net_weight;
	}
	public double getVolume_weight() {
		return volume_weight;
	}
	public void setVolume_weight(double volume_weight) {
		this.volume_weight = volume_weight;
	}
	public String getProduct_code() {
		return product_code;
	}
	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}
	public String getReference_1() {
		return reference_1;
	}
	public void setReference_1(String reference_1) {
		this.reference_1 = reference_1;
	}
	public Date getReference_1_date() {
		return reference_1_date;
	}
	public void setReference_1_date(Date reference_1_date) {
		this.reference_1_date = reference_1_date;
	}
	public String getReference_2() {
		return reference_2;
	}
	public void setReference_2(String reference_2) {
		this.reference_2 = reference_2;
	}
	public Date getReference_2_date() {
		return reference_2_date;
	}
	public void setReference_2_date(Date reference_2_date) {
		this.reference_2_date = reference_2_date;
	}
	public String getReference_3() {
		return reference_3;
	}
	public void setReference_3(String reference_3) {
		this.reference_3 = reference_3;
	}
	public Date getReference_3_date() {
		return reference_3_date;
	}
	public void setReference_3_date(Date reference_3_date) {
		this.reference_3_date = reference_3_date;
	}
	public String getReference_4() {
		return reference_4;
	}
	public void setReference_4(String reference_4) {
		this.reference_4 = reference_4;
	}
	public Date getReference_4_date() {
		return reference_4_date;
	}
	public void setReference_4_date(Date reference_4_date) {
		this.reference_4_date = reference_4_date;
	}
	public String getReference_5() {
		return reference_5;
	}
	public void setReference_5(String reference_5) {
		this.reference_5 = reference_5;
	}
	public Date getReference_5_date() {
		return reference_5_date;
	}
	public void setReference_5_date(Date reference_5_date) {
		this.reference_5_date = reference_5_date;
	}
	public Date getRec_creation_date() {
		return rec_creation_date;
	}
	public void setRec_creation_date(Date rec_creation_date) {
		this.rec_creation_date = rec_creation_date;
	}
	public Date getRec_update_date() {
		return rec_update_date;
	}
	public void setRec_update_date(Date rec_update_date) {
		this.rec_update_date = rec_update_date;
	}
	public String getSize1() {
		return size1;
	}
	public void setSize1(String size1) {
		this.size1 = size1;
	}
	public double getLength1() {
		return length1;
	}
	public void setLength1(double length1) {
		this.length1 = length1;
	}
	
	
}
