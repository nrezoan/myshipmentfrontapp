package com.myshipment.model;

public class ZemailParams {
	private String hblNumber;
	private String customerNumber;
	private String buyer;
	private String salesOrg;
	private String distChannel;
	private String division;
	public String getHblNumber() {
		return hblNumber;
	}
	public void setHblNumber(String hblNumber) {
		this.hblNumber = hblNumber;
	}
	public String getCustomerNumber() {
		return customerNumber;
	}
	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}
	public String getBuyer() {
		return buyer;
	}
	public void setBuyer(String buyer) {
		this.buyer = buyer;
	}
	public String getSalesOrg() {
		return salesOrg;
	}
	public void setSalesOrg(String salesOrg) {
		this.salesOrg = salesOrg;
	}
	public String getDistChannel() {
		return distChannel;
	}
	public void setDistChannel(String distChannel) {
		this.distChannel = distChannel;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	
	
	

}
