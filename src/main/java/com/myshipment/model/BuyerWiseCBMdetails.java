package com.myshipment.model;

import java.util.List;

public class BuyerWiseCBMdetails {
	private List<BuyerWiseCBMJson> lstBuyerWiseCBMJson;
	private String message;

	public List<BuyerWiseCBMJson> getLstBuyerWiseCBMJson() {
		return lstBuyerWiseCBMJson;
	}

	public void setLstBuyerWiseCBMJson(List<BuyerWiseCBMJson> lstBuyerWiseCBMJson) {
		this.lstBuyerWiseCBMJson = lstBuyerWiseCBMJson;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
