package com.myshipment.model;

/*
 * @ Gufranur Rahman
 */

public class NegoBillLandingI {
	
	private String vbeln;
	private String kunNr;
	private String parVw;
	private String vgbel;
	private String vrkme;
	private String vbelv;
	private String posnr;
	private String venum;
	private String adrnr;
	private String vhilm;
	private String vhilm_ku;
	private String wgbez60;
	private String vemng;
	private String ntvol;
	private String zzModeShipment;
	private String ntGew;
	private String matKl;
	private String vgPos;
	private String kwmeng;
	private String zzgr_wt;
	private String zzVolume;
	private String zzLenght;
	private String zzWidth;
	private String zzHeight;
	private String zzTotalNoPcs;
	private String zzQuantityUom;
	private String zzHsCode;
	
	
	public String getZzHsCode() {
		return zzHsCode;
	}
	public void setZzHsCode(String zzHsCode) {
		this.zzHsCode = zzHsCode;
	}
	public String getVbeln() {
		return vbeln;
	}
	public void setVbeln(String vbeln) {
		this.vbeln = vbeln;
	}
	public String getKunNr() {
		return kunNr;
	}
	public void setKunNr(String kunNr) {
		this.kunNr = kunNr;
	}
	public String getParVw() {
		return parVw;
	}
	public void setParVw(String parVw) {
		this.parVw = parVw;
	}
	public String getVgbel() {
		return vgbel;
	}
	public void setVgbel(String vgbel) {
		this.vgbel = vgbel;
	}
	public String getVrkme() {
		return vrkme;
	}
	public void setVrkme(String vrkme) {
		this.vrkme = vrkme;
	}
	public String getVbelv() {
		return vbelv;
	}
	public void setVbelv(String vbelv) {
		this.vbelv = vbelv;
	}
	public String getPosnr() {
		return posnr;
	}
	public void setPosnr(String posnr) {
		this.posnr = posnr;
	}
	public String getVenum() {
		return venum;
	}
	public void setVenum(String venum) {
		this.venum = venum;
	}
	public String getAdrnr() {
		return adrnr;
	}
	public void setAdrnr(String adrnr) {
		this.adrnr = adrnr;
	}
	public String getVhilm() {
		return vhilm;
	}
	public void setVhilm(String vhilm) {
		this.vhilm = vhilm;
	}
	public String getVhilm_ku() {
		return vhilm_ku;
	}
	public void setVhilm_ku(String vhilm_ku) {
		this.vhilm_ku = vhilm_ku;
	}
	public String getWgbez60() {
		return wgbez60;
	}
	public void setWgbez60(String wgbez60) {
		this.wgbez60 = wgbez60;
	}
	public String getVemng() {
		return vemng;
	}
	public void setVemng(String vemng) {
		this.vemng = vemng;
	}
	public String getNtvol() {
		return ntvol;
	}
	public void setNtvol(String ntvol) {
		this.ntvol = ntvol;
	}
	public String getZzModeShipment() {
		return zzModeShipment;
	}
	public void setZzModeShipment(String zzModeShipment) {
		this.zzModeShipment = zzModeShipment;
	}
	public String getNtGew() {
		return ntGew;
	}
	public void setNtGew(String ntGew) {
		this.ntGew = ntGew;
	}
	public String getMatKl() {
		return matKl;
	}
	public void setMatKl(String matKl) {
		this.matKl = matKl;
	}
	public String getVgPos() {
		return vgPos;
	}
	public void setVgPos(String vgPos) {
		this.vgPos = vgPos;
	}
	public String getKwmeng() {
		return kwmeng;
	}
	public void setKwmeng(String kwmeng) {
		this.kwmeng = kwmeng;
	}
	public String getZzgr_wt() {
		return zzgr_wt;
	}
	public void setZzgr_wt(String zzgr_wt) {
		this.zzgr_wt = zzgr_wt;
	}
	public String getZzVolume() {
		return zzVolume;
	}
	public void setZzVolume(String zzVolume) {
		this.zzVolume = zzVolume;
	}
	public String getZzLenght() {
		return zzLenght;
	}
	public void setZzLenght(String zzLenght) {
		this.zzLenght = zzLenght;
	}
	public String getZzWidth() {
		return zzWidth;
	}
	public void setZzWidth(String zzWidth) {
		this.zzWidth = zzWidth;
	}
	public String getZzHeight() {
		return zzHeight;
	}
	public void setZzHeight(String zzHeight) {
		this.zzHeight = zzHeight;
	}
	public String getZzTotalNoPcs() {
		return zzTotalNoPcs;
	}
	public void setZzTotalNoPcs(String zzTotalNoPcs) {
		this.zzTotalNoPcs = zzTotalNoPcs;
	}
	public String getZzQuantityUom() {
		return zzQuantityUom;
	}
	public void setZzQuantityUom(String zzQuantityUom) {
		this.zzQuantityUom = zzQuantityUom;
	}
	
	
}

	
	
	
	
	