package com.myshipment.model;

import java.math.BigDecimal;


public class ZsdSexInvDetailInd {
	private String vText;
	private String sacCode;
	private String kbetr;	
	private String waers;
	private String conv;
	private String tdText;	
	private BigDecimal tKwert;
	private BigDecimal ntKwert;	
	private String vhilm;
	private String wgbez60;	
	private String type;	
	private Integer index;
	public String getvText() {
		return vText;
	}
	public void setvText(String vText) {
		this.vText = vText;
	}
	public String getSacCode() {
		return sacCode;
	}
	public void setSacCode(String sacCode) {
		this.sacCode = sacCode;
	}
	public String getKbetr() {
		return kbetr;
	}
	public void setKbetr(String kbetr) {
		this.kbetr = kbetr;
	}

	public String getConv() {
		return conv;
	}
	public void setConv(String conv) {
		this.conv = conv;
	}
	public String getTdText() {
		return tdText;
	}
	public void setTdText(String tdText) {
		this.tdText = tdText;
	}
	public BigDecimal gettKwert() {
		return tKwert;
	}
	public void settKwert(BigDecimal tKwert) {
		this.tKwert = tKwert;
	}
	public BigDecimal getNtKwert() {
		return ntKwert;
	}
	public void setNtKwert(BigDecimal ntKwert) {
		this.ntKwert = ntKwert;
	}
	public String getVhilm() {
		return vhilm;
	}
	public void setVhilm(String vhilm) {
		this.vhilm = vhilm;
	}
	public String getWgbez60() {
		return wgbez60;
	}
	public void setWgbez60(String wgbez60) {
		this.wgbez60 = wgbez60;
	}
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

	public String getWaers() {
		return waers;
	}
	public void setWaers(String waers) {
		this.waers = waers;
	}
	public Integer getIndex() {
		return index;
	}
	public void setIndex(Integer index) {
		this.index = index;
	}	
	
	
}