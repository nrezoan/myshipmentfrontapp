package com.myshipment.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DailySaleReportParams {
	private String mawb;
	private String gha;
	private String booked;
	private String origin;
	private String destination;
	private BigDecimal totalPieces;
	private BigDecimal weight;
	private BigDecimal cwt;
	private BigDecimal cbm;
	private String typeOfShipment;
	private Date grDate;
	private Date preAlertDate;
	private Date eta;
	private Date etd;
	private Integer diff;
	private Date ataPort;
	private Date whDate;
	private String typeOfDelivery;
	private Integer puTime;
	private Date slotReqDt;
	private Date slotRecDt;
	private String cargoStatus;
	private String exception;
	private String remarks;
	private Integer weekendHours;
	private Integer preNoticeReq;
	private Integer dlvDate;
	private Integer arrToDlvDate;
	private Integer arrToTruckDep;
	private String truckType;
	private String truckBookingDate;
	private String truckCancelDate;
	private String licensePlate;
	private String driverDetails;
	private Date ghaToSovAta;
	private Date ghaToSovAtd;
	private Integer week;
	private Integer year;
	private String tsp;
	private String freightService;
	private Date atd;
	private Date ata;
	private Date cutOffDate;
	private Date etaDeadline;
	private String portToPort;
	private String portToDc;
	private Integer dayDifference;

	private List<DailySalesReportHawbParams> hawbList = new ArrayList<>();

	public List<DailySalesReportHawbParams> getHawbList() {
		return hawbList;
	}

	public void setHawbList(List<DailySalesReportHawbParams> hawbList) {
		this.hawbList = hawbList;
	}

	public String getMawb() {
		return mawb;
	}

	public void setMawb(String mawb) {
		this.mawb = mawb;
	}

	public String getGha() {
		return gha;
	}

	public void setGha(String gha) {
		this.gha = gha;
	}

	public String getBooked() {
		return booked;
	}

	public void setBooked(String booked) {
		this.booked = booked;
	}

	public String getOrigin() {
		return origin;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public BigDecimal getTotalPieces() {
		return totalPieces;
	}

	public void setTotalPieces(BigDecimal totalPieces) {
		this.totalPieces = totalPieces;
	}

	public BigDecimal getWeight() {
		return weight;
	}

	public void setWeight(BigDecimal weight) {
		this.weight = weight;
	}

	public BigDecimal getCwt() {
		return cwt;
	}

	public void setCwt(BigDecimal cwt) {
		this.cwt = cwt;
	}

	public BigDecimal getCbm() {
		return cbm;
	}

	public void setCbm(BigDecimal cbm) {
		this.cbm = cbm;
	}

	public String getTypeOfShipment() {
		return typeOfShipment;
	}

	public void setTypeOfShipment(String typeOfShipment) {
		this.typeOfShipment = typeOfShipment;
	}

	public Date getGrDate() {
		return grDate;
	}

	public void setGrDate(Date grDate) {
		this.grDate = grDate;
	}

	public Date getPreAlertDate() {
		return preAlertDate;
	}

	public void setPreAlertDate(Date preAlertDate) {
		this.preAlertDate = preAlertDate;
	}

	public Date getEta() {
		return eta;
	}

	public void setEta(Date eta) {
		this.eta = eta;
	}

	public Date getEtd() {
		return etd;
	}

	public void setEtd(Date etd) {
		this.etd = etd;
	}

	public Integer getDiff() {
		return diff;
	}

	public void setDiff(Integer diff) {
		this.diff = diff;
	}

	public Date getAtaPort() {
		return ataPort;
	}

	public void setAtaPort(Date ataPort) {
		this.ataPort = ataPort;
	}

	public Date getWhDate() {
		return whDate;
	}

	public void setWhDate(Date whDate) {
		this.whDate = whDate;
	}

	public String getTypeOfDelivery() {
		return typeOfDelivery;
	}

	public void setTypeOfDelivery(String typeOfDelivery) {
		this.typeOfDelivery = typeOfDelivery;
	}

	public Integer getPuTime() {
		return puTime;
	}

	public void setPuTime(Integer puTime) {
		this.puTime = puTime;
	}

	public Date getSlotReqDt() {
		return slotReqDt;
	}

	public void setSlotReqDt(Date slotReqDt) {
		this.slotReqDt = slotReqDt;
	}

	public Date getSlotRecDt() {
		return slotRecDt;
	}

	public void setSlotRecDt(Date slotRecDt) {
		this.slotRecDt = slotRecDt;
	}

	public String getCargoStatus() {
		return cargoStatus;
	}

	public void setCargoStatus(String cargoStatus) {
		this.cargoStatus = cargoStatus;
	}

	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Integer getWeekendHours() {
		return weekendHours;
	}

	public void setWeekendHours(Integer weekendHours) {
		this.weekendHours = weekendHours;
	}

	public Integer getPreNoticeReq() {
		return preNoticeReq;
	}

	public void setPreNoticeReq(Integer preNoticeReq) {
		this.preNoticeReq = preNoticeReq;
	}

	public Integer getDlvDate() {
		return dlvDate;
	}

	public void setDlvDate(Integer dlvDate) {
		this.dlvDate = dlvDate;
	}

	public Integer getArrToDlvDate() {
		return arrToDlvDate;
	}

	public void setArrToDlvDate(Integer arrToDlvDate) {
		this.arrToDlvDate = arrToDlvDate;
	}

	public Integer getArrToTruckDep() {
		return arrToTruckDep;
	}

	public void setArrToTruckDep(Integer arrToTruckDep) {
		this.arrToTruckDep = arrToTruckDep;
	}

	public String getTruckType() {
		return truckType;
	}

	public void setTruckType(String truckType) {
		this.truckType = truckType;
	}

	public String getTruckBookingDate() {
		return truckBookingDate;
	}

	public void setTruckBookingDate(String truckBookingDate) {
		this.truckBookingDate = truckBookingDate;
	}

	public String getTruckCancelDate() {
		return truckCancelDate;
	}

	public void setTruckCancelDate(String truckCancelDate) {
		this.truckCancelDate = truckCancelDate;
	}

	public String getLicensePlate() {
		return licensePlate;
	}

	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public String getDriverDetails() {
		return driverDetails;
	}

	public void setDriverDetails(String driverDetails) {
		this.driverDetails = driverDetails;
	}

	public Date getGhaToSovAta() {
		return ghaToSovAta;
	}

	public void setGhaToSovAta(Date ghaToSovAta) {
		this.ghaToSovAta = ghaToSovAta;
	}

	public Date getGhaToSovAtd() {
		return ghaToSovAtd;
	}

	public void setGhaToSovAtd(Date ghaToSovAtd) {
		this.ghaToSovAtd = ghaToSovAtd;
	}

	public Integer getWeek() {
		return week;
	}

	public void setWeek(Integer week) {
		this.week = week;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getTsp() {
		return tsp;
	}

	public void setTsp(String tsp) {
		this.tsp = tsp;
	}

	public String getFreightService() {
		return freightService;
	}

	public void setFreightService(String freightService) {
		this.freightService = freightService;
	}

	public Date getAtd() {
		return atd;
	}

	public void setAtd(Date atd) {
		this.atd = atd;
	}

	public Date getAta() {
		return ata;
	}

	public void setAta(Date ata) {
		this.ata = ata;
	}

	public Date getCutOffDate() {
		return cutOffDate;
	}

	public void setCutOffDate(Date cutOffDate) {
		this.cutOffDate = cutOffDate;
	}

	public Date getEtaDeadline() {
		return etaDeadline;
	}

	public void setEtaDeadline(Date etaDeadline) {
		this.etaDeadline = etaDeadline;
	}

	public String getPortToPort() {
		return portToPort;
	}

	public void setPortToPort(String portToPort) {
		this.portToPort = portToPort;
	}

	public String getPortToDc() {
		return portToDc;
	}

	public void setPortToDc(String portToDc) {
		this.portToDc = portToDc;
	}

	public Integer getDayDifference() {
		return dayDifference;
	}

	public void setDayDifference(Integer dayDifference) {
		this.dayDifference = dayDifference;
	}

}
