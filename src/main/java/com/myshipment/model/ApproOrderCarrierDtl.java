package com.myshipment.model;

public class ApproOrderCarrierDtl {
	private static final Long serialVersionUID=-97756635555234242l;
	private Long app_carr_id;
	private Long app_id;       
	private Double no_of_items_approved;       
	private String carrier_name; 
	private String caarrier_schedule;
	private String vc_po_no; 
	private String carr_ass_comment;
	public Long getApp_carr_id() {
		return app_carr_id;
	}
	public void setApp_carr_id(Long app_carr_id) {
		this.app_carr_id = app_carr_id;
	}
	public Long getApp_id() {
		return app_id;
	}
	public void setApp_id(Long app_id) {
		this.app_id = app_id;
	}
	public Double getNo_of_items_approved() {
		return no_of_items_approved;
	}
	public void setNo_of_items_approved(Double no_of_items_approved) {
		this.no_of_items_approved = no_of_items_approved;
	}
	public String getCarrier_name() {
		return carrier_name;
	}
	public void setCarrier_name(String carrier_name) {
		this.carrier_name = carrier_name;
	}
	public String getCaarrier_schedule() {
		return caarrier_schedule;
	}
	public void setCaarrier_schedule(String caarrier_schedule) {
		this.caarrier_schedule = caarrier_schedule;
	}
	public String getVc_po_no() {
		return vc_po_no;
	}
	public void setVc_po_no(String vc_po_no) {
		this.vc_po_no = vc_po_no;
	}
	public String getCarr_ass_comment() {
		return carr_ass_comment;
	}
	public void setCarr_ass_comment(String carr_ass_comment) {
		this.carr_ass_comment = carr_ass_comment;
	}
	@Override
	public String toString() {
		return "ApproOrderCarrierDtl [app_carr_id=" + app_carr_id + ", app_id=" + app_id + ", no_of_items_approved="
				+ no_of_items_approved + ", carrier_name=" + carrier_name + ", caarrier_schedule=" + caarrier_schedule
				+ ", vc_po_no=" + vc_po_no + ", carr_ass_comment=" + carr_ass_comment + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((app_carr_id == null) ? 0 : app_carr_id.hashCode());
		result = prime * result + ((app_id == null) ? 0 : app_id.hashCode());
		result = prime * result + ((caarrier_schedule == null) ? 0 : caarrier_schedule.hashCode());
		result = prime * result + ((carr_ass_comment == null) ? 0 : carr_ass_comment.hashCode());
		result = prime * result + ((carrier_name == null) ? 0 : carrier_name.hashCode());
		result = prime * result + ((no_of_items_approved == null) ? 0 : no_of_items_approved.hashCode());
		result = prime * result + ((vc_po_no == null) ? 0 : vc_po_no.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ApproOrderCarrierDtl other = (ApproOrderCarrierDtl) obj;
		if (app_carr_id == null) {
			if (other.app_carr_id != null)
				return false;
		} else if (!app_carr_id.equals(other.app_carr_id))
			return false;
		if (app_id == null) {
			if (other.app_id != null)
				return false;
		} else if (!app_id.equals(other.app_id))
			return false;
		if (caarrier_schedule == null) {
			if (other.caarrier_schedule != null)
				return false;
		} else if (!caarrier_schedule.equals(other.caarrier_schedule))
			return false;
		if (carr_ass_comment == null) {
			if (other.carr_ass_comment != null)
				return false;
		} else if (!carr_ass_comment.equals(other.carr_ass_comment))
			return false;
		if (carrier_name == null) {
			if (other.carrier_name != null)
				return false;
		} else if (!carrier_name.equals(other.carrier_name))
			return false;
		if (no_of_items_approved == null) {
			if (other.no_of_items_approved != null)
				return false;
		} else if (!no_of_items_approved.equals(other.no_of_items_approved))
			return false;
		if (vc_po_no == null) {
			if (other.vc_po_no != null)
				return false;
		} else if (!vc_po_no.equals(other.vc_po_no))
			return false;
		return true;
	}
	

}
