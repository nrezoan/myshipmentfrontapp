package com.myshipment.model;

import java.util.List;
/*
 * @Ranjeet Kumar
 */
public class DirectBookingUpdateJsonData {
 
	private String hblnumber;
	private String zsalesdocument;
	private List<BapiReturn3> bapiReturn3;
	
	public String getHblnumber() {
		return hblnumber;
	}
	public void setHblnumber(String hblnumber) {
		this.hblnumber = hblnumber;
	}
	public String getZsalesdocument() {
		return zsalesdocument;
	}
	public void setZsalesdocument(String zsalesdocument) {
		this.zsalesdocument = zsalesdocument;
	}
	public List<BapiReturn3> getBapiReturn3() {
		return bapiReturn3;
	}
	public void setBapiReturn3(List<BapiReturn3> bapiReturn3) {
		this.bapiReturn3 = bapiReturn3;
	}
	
	
}
