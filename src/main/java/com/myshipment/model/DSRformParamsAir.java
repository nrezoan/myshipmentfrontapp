package com.myshipment.model;

import java.util.Date;
import java.util.List;

public class DSRformParamsAir {
	private List<MyshipmentTrackHeaderBean> myshipmentTrackHeader;
	private List<MyshipmentTrackItemBean> myshipmentTrackItem;
	private List<MyshipmentTrackScheduleBean> myshipmentTrackSchedule;
	private String hawb_2;
	private String rsas_invoice;
	private String costCenter;
	private String mawb_2;
	private Date pre_Alert_dt;
	private String stillage;
	private Date die_after_dt;
	private String shipment_mode;
	private String part_reference;
	private String das;
	private String airFreight_cost;
	private String cost_born;
	private String project_name;
	private String supplier_name;
	private String reason_for_shipment;
	private String scroll_dt;
	private String doc_dispatch_dt;
	private String spc_dt;
	private String doc_dt;
	private String dwell_time_target;
	private Date eta_updated;
	private Date etd_updated;
	private Date atd_updated;
	private Date ata_updated;
	private Date atd_origin;
	private Date atd_trship;
	private Date ata_trship;
	private Date eta_airlines;
	private Integer lead_time;
	private Integer dwell_target;
	private String cargo_status;
	

	public String getCargo_status() {
		return cargo_status;
	}

	public void setCargo_status(String cargo_status) {
		this.cargo_status = cargo_status;
	}

	public Integer getDwell_target() {
		return dwell_target;
	}

	public void setDwell_target(Integer dwell_target) {
		this.dwell_target = dwell_target;
	}

	public String getHawb_2() {
		return hawb_2;
	}

	public void setHawb_2(String hawb_2) {
		this.hawb_2 = hawb_2;
	}

	public String getRsas_invoice() {
		return rsas_invoice;
	}

	public void setRsas_invoice(String rsas_invoice) {
		this.rsas_invoice = rsas_invoice;
	}

	public String getCostCenter() {
		return costCenter;
	}

	public void setCostCenter(String costCenter) {
		this.costCenter = costCenter;
	}

	public String getMawb_2() {
		return mawb_2;
	}

	public void setMawb_2(String mawb_2) {
		this.mawb_2 = mawb_2;
	}

	public Date getPre_Alert_dt() {
		return pre_Alert_dt;
	}

	public void setPre_Alert_dt(Date pre_Alert_dt) {
		this.pre_Alert_dt = pre_Alert_dt;
	}

	public String getStillage() {
		return stillage;
	}

	public void setStillage(String stillage) {
		this.stillage = stillage;
	}

	public Date getDie_after_dt() {
		return die_after_dt;
	}

	public void setDie_after_dt(Date die_after_dt) {
		this.die_after_dt = die_after_dt;
	}

	public String getShipment_mode() {
		return shipment_mode;
	}

	public void setShipment_mode(String shipment_mode) {
		this.shipment_mode = shipment_mode;
	}

	public String getPart_reference() {
		return part_reference;
	}

	public void setPart_reference(String part_reference) {
		this.part_reference = part_reference;
	}

	public String getDas() {
		return das;
	}

	public void setDas(String das) {
		this.das = das;
	}

	public String getAirFreight_cost() {
		return airFreight_cost;
	}

	public void setAirFreight_cost(String airFreight_cost) {
		this.airFreight_cost = airFreight_cost;
	}

	public String getCost_born() {
		return cost_born;
	}

	public void setCost_born(String cost_born) {
		this.cost_born = cost_born;
	}

	public String getProject_name() {
		return project_name;
	}

	public void setProject_name(String project_name) {
		this.project_name = project_name;
	}

	public String getSupplier_name() {
		return supplier_name;
	}

	public void setSupplier_name(String supplier_name) {
		this.supplier_name = supplier_name;
	}

	public String getReason_for_shipment() {
		return reason_for_shipment;
	}

	public void setReason_for_shipment(String reason_for_shipment) {
		this.reason_for_shipment = reason_for_shipment;
	}

	public String getScroll_dt() {
		return scroll_dt;
	}

	public void setScroll_dt(String scroll_dt) {
		this.scroll_dt = scroll_dt;
	}

	public String getDoc_dispatch_dt() {
		return doc_dispatch_dt;
	}

	public void setDoc_dispatch_dt(String doc_dispatch_dt) {
		this.doc_dispatch_dt = doc_dispatch_dt;
	}

	public String getSpc_dt() {
		return spc_dt;
	}

	public void setSpc_dt(String spc_dt) {
		this.spc_dt = spc_dt;
	}

	public String getDoc_dt() {
		return doc_dt;
	}

	public void setDoc_dt(String doc_dt) {
		this.doc_dt = doc_dt;
	}

	public String getDwell_time_target() {
		return dwell_time_target;
	}

	public void setDwell_time_target(String dwell_time_target) {
		this.dwell_time_target = dwell_time_target;
	}

	public Date getEta_updated() {
		return eta_updated;
	}

	public void setEta_updated(Date eta_updated) {
		this.eta_updated = eta_updated;
	}

	public Date getEtd_updated() {
		return etd_updated;
	}

	public void setEtd_updated(Date etd_updated) {
		this.etd_updated = etd_updated;
	}

	public Date getAtd_updated() {
		return atd_updated;
	}

	public void setAtd_updated(Date atd_updated) {
		this.atd_updated = atd_updated;
	}

	public Date getAta_updated() {
		return ata_updated;
	}

	public void setAta_updated(Date ata_updated) {
		this.ata_updated = ata_updated;
	}

	public Date getAtd_origin() {
		return atd_origin;
	}

	public void setAtd_origin(Date atd_origin) {
		this.atd_origin = atd_origin;
	}

	public Date getAtd_trship() {
		return atd_trship;
	}

	public void setAtd_trship(Date atd_trship) {
		this.atd_trship = atd_trship;
	}

	public Date getAta_trship() {
		return ata_trship;
	}

	public void setAta_trship(Date ata_trship) {
		this.ata_trship = ata_trship;
	}

	public Date getEta_airlines() {
		return eta_airlines;
	}

	public void setEta_airlines(Date eta_airlines) {
		this.eta_airlines = eta_airlines;
	}

	public Integer getLead_time() {
		return lead_time;
	}

	public void setLead_time(Integer lead_time) {
		this.lead_time = lead_time;
	}

	public List<MyshipmentTrackHeaderBean> getMyshipmentTrackHeader() {
		return myshipmentTrackHeader;
	}

	public void setMyshipmentTrackHeader(List<MyshipmentTrackHeaderBean> myshipmentTrackHeader) {
		this.myshipmentTrackHeader = myshipmentTrackHeader;
	}

	public List<MyshipmentTrackItemBean> getMyshipmentTrackItem() {
		return myshipmentTrackItem;
	}

	public void setMyshipmentTrackItem(List<MyshipmentTrackItemBean> myshipmentTrackItem) {
		this.myshipmentTrackItem = myshipmentTrackItem;
	}

	public List<MyshipmentTrackScheduleBean> getMyshipmentTrackSchedule() {
		return myshipmentTrackSchedule;
	}

	public void setMyshipmentTrackSchedule(List<MyshipmentTrackScheduleBean> myshipmentTrackSchedule) {
		this.myshipmentTrackSchedule = myshipmentTrackSchedule;
	}

}
