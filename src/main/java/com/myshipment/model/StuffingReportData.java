package com.myshipment.model;

import java.util.List;



public class StuffingReportData {

	private BapiRet1 bapiReturn1;
	private List<StuffingHeader> headeritem; 
	private List<StuffingDetail> bapiItem; 
	private List<NegoBillLanding> itAddress; 
	private String totPcs;
	private String totVol;
	private String totQty;
	private String totKgs;
	
	public BapiRet1 getBapiReturn1() {
		return bapiReturn1;
	}
	public void setBapiReturn1(BapiRet1 bapiReturn1) {
		this.bapiReturn1 = bapiReturn1;
	}
	public List<StuffingHeader> getHeaderitem() {
		return headeritem;
	}
	public void setHeaderitem(List<StuffingHeader> headeritem) {
		this.headeritem = headeritem;
	}
	public List<StuffingDetail> getBapiItem() {
		return bapiItem;
	}
	public void setBapiItem(List<StuffingDetail> bapiItem) {
		this.bapiItem = bapiItem;
	}
	public List<NegoBillLanding> getItAddress() {
		return itAddress;
	}
	public void setItAddress(List<NegoBillLanding> itAddress) {
		this.itAddress = itAddress;
	}
	public String getTotPcs() {
		return totPcs;
	}
	public void setTotPcs(String totPcs) {
		this.totPcs = totPcs;
	}
	public String getTotVol() {
		return totVol;
	}
	public void setTotVol(String totVol) {
		this.totVol = totVol;
	}
	public String getTotQty() {
		return totQty;
	}
	public void setTotQty(String totQty) {
		this.totQty = totQty;
	}
	public String getTotKgs() {
		return totKgs;
	}
	public void setTotKgs(String totKgs) {
		this.totKgs = totKgs;
	}
	
		
			
}
