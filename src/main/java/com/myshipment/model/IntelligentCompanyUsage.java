/**
*
*@author Ahmad Naquib
*/
package com.myshipment.model;

public class IntelligentCompanyUsage {

	private String totalHit;
	private String distributionChannel;
	private String division;
	private String company;
	
	public String getTotalHit() {
		return totalHit;
	}
	public void setTotalHit(String totalHit) {
		this.totalHit = totalHit;
	}
	public String getDistributionChannel() {
		return distributionChannel;
	}
	public void setDistributionChannel(String distributionChannel) {
		this.distributionChannel = distributionChannel;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	
}
