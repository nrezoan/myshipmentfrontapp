package com.myshipment.model;

import java.util.Set;
/*
 * @Ranjeet Kumar
 */
public class PoBookingPacklistColorBean {

	private Integer colorId;	
	private String colorName;	
	private Integer pcsPerCtn;	
	private Integer totalPcs;	
	private Integer plId;	
	private String remarks;
	private PoBookingPacklistDetailBean packingDetail;
	private Set<PoBookingPacklistSizeBean> packlistSizes;
	
	public Integer getColorId() {
		return colorId;
	}
	public void setColorId(Integer colorId) {
		this.colorId = colorId;
	}
	public String getColorName() {
		return colorName;
	}
	public void setColorName(String colorName) {
		this.colorName = colorName;
	}
	public Integer getPcsPerCtn() {
		return pcsPerCtn;
	}
	public void setPcsPerCtn(Integer pcsPerCtn) {
		this.pcsPerCtn = pcsPerCtn;
	}
	public Integer getTotalPcs() {
		return totalPcs;
	}
	public void setTotalPcs(Integer totalPcs) {
		this.totalPcs = totalPcs;
	}
	public Integer getPlId() {
		return plId;
	}
	public void setPlId(Integer plId) {
		this.plId = plId;
	}
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public PoBookingPacklistDetailBean getPackingDetail() {
		return packingDetail;
	}
	public void setPackingDetail(PoBookingPacklistDetailBean packingDetail) {
		this.packingDetail = packingDetail;
	}
	public Set<PoBookingPacklistSizeBean> getPacklistSizes() {
		return packlistSizes;
	}
	public void setPacklistSizes(Set<PoBookingPacklistSizeBean> packlistSizes) {
		this.packlistSizes = packlistSizes;
	}
	
	
}
