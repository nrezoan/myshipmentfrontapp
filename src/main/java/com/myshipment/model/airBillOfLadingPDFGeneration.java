package com.myshipment.model;

import java.util.Date;

public class airBillOfLadingPDFGeneration {
	private String shipperName1;
	private String shipperName2;
	private String shipperName3;
	private String shipperName4;
	private String shipperCity1;
	private String shipperCountry;
	//A/c
	
	private String shipper2Name1;
	private String shipper2Name2;
	private String shipper2Name3;
	private String shipper2Name4;
	private String shipper2City1;
	private String shipper2Country;
	//consignee
	private String consigneeName1;
	private String consigneeName2;
	private String consigneeName3;
	private String consigneeName4;
	private String consigneeCity1;
	private String consigneeCountry;
	//A/C
	private String consignee2Name1;
	private String consignee2Name2;
	private String consignee2Name3;
	private String consignee2Name4;
	private String consignee2City1;
	private String consignee2Country;
	//Notify
	private String notifyName1;
	private String notifyName2;
	private String notifyName3;
	private String notifyName4;
	private String notifyCity1;
	private String notifyCountry;
	private String airportOfDep;
	private String airportOfDes;
	private String requestedRouting;
	private String iata;
	private String masterBil;
	private String flightNo;
	private String flightDt;
	//table
	private int noOfPcs;
	private String gw;
	private String kg;
	private String cw;
	private String rate;
	private String total;
	//
	private String biilNo;
	//airway
	private String addressName1;
	private String addressName2;
	private String addressName3;
	private String addressName4;
	private String addressCity1;
	private String addressCountry;
	private String streeNo;
	private String accountInfo;
	private String description;
	private String invoice;
	private String type;
	private String no;
	private String expNo;
	private String scbNo;
	private int dimnlength;
	private int dimnHeight;
	private int dimnWidth;
	private int dimn1;
	private String dimn2;
	private String date1;
	private String date2;
	private String date3;
	private String date4;
	//private Date date4;
	private String excuted;
	private String at;
	private String shippingMark;
	private String companyCode;
	private String date;
	private String hsCode;
	
	
	
	
	public String getStreeNo() {
		return streeNo;
	}
	public void setStreeNo(String streeNo) {
		this.streeNo = streeNo;
	}
	public String getHsCode() {
		return hsCode;
	}
	public void setHsCode(String hsCode) {
		this.hsCode = hsCode;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	public String getShippingMark() {
		return shippingMark;
	}
	public void setShippingMark(String shippingMark) {
		this.shippingMark = shippingMark;
	}
	public String getShipperName1() {
		return shipperName1;
	}
	public String getShipperName2() {
		return shipperName2;
	}
	public String getShipperName3() {
		return shipperName3;
	}
	public String getShipperName4() {
		return shipperName4;
	}
	public String getShipperCity1() {
		return shipperCity1;
	}
	public String getShipperCountry() {
		return shipperCountry;
	}
	public String getShipper2Name1() {
		return shipper2Name1;
	}
	public String getShipper2Name2() {
		return shipper2Name2;
	}
	public String getShipper2Name3() {
		return shipper2Name3;
	}
	public String getShipper2Name4() {
		return shipper2Name4;
	}
	public String getShipper2City1() {
		return shipper2City1;
	}
	public String getShipper2Country() {
		return shipper2Country;
	}
	public String getConsigneeName1() {
		return consigneeName1;
	}
	public String getConsigneeName2() {
		return consigneeName2;
	}
	public String getConsigneeName3() {
		return consigneeName3;
	}
	public String getConsigneeName4() {
		return consigneeName4;
	}
	public String getConsigneeCity1() {
		return consigneeCity1;
	}
	public String getConsigneeCountry() {
		return consigneeCountry;
	}
	public String getConsignee2Name1() {
		return consignee2Name1;
	}
	public String getConsignee2Name2() {
		return consignee2Name2;
	}
	public String getConsignee2Name3() {
		return consignee2Name3;
	}
	public String getConsignee2Name4() {
		return consignee2Name4;
	}
	public String getConsignee2City1() {
		return consignee2City1;
	}
	public String getConsignee2Country() {
		return consignee2Country;
	}
	public String getNotifyName1() {
		return notifyName1;
	}
	public String getNotifyName2() {
		return notifyName2;
	}
	public String getNotifyName3() {
		return notifyName3;
	}
	public String getNotifyName4() {
		return notifyName4;
	}
	public String getNotifyCity1() {
		return notifyCity1;
	}
	public String getNotifyCountry() {
		return notifyCountry;
	}
	public String getAirportOfDep() {
		return airportOfDep;
	}
	public String getAirportOfDes() {
		return airportOfDes;
	}
	public String getRequestedRouting() {
		return requestedRouting;
	}
	public String getIata() {
		return iata;
	}
	public String getMasterBil() {
		return masterBil;
	}
	public String getFlightNo() {
		return flightNo;
	}
	public String getFlightDt() {
		return flightDt;
	}
	public int getNoOfPcs() {
		return noOfPcs;
	}
	public String getGw() {
		return gw;
	}
	public String getKg() {
		return kg;
	}
	public String getCw() {
		return cw;
	}
	public String getRate() {
		return rate;
	}
	public String getTotal() {
		return total;
	}
	public String getBiilNo() {
		return biilNo;
	}
	public String getAddressName1() {
		return addressName1;
	}
	public String getAddressName2() {
		return addressName2;
	}
	public String getAddressName3() {
		return addressName3;
	}
	public String getAddressName4() {
		return addressName4;
	}
	public String getAddressCity1() {
		return addressCity1;
	}
	public String getAddressCountry() {
		return addressCountry;
	}
	public String getAccountInfo() {
		return accountInfo;
	}
	public String getDescription() {
		return description;
	}
	public String getInvoice() {
		return invoice;
	}
	public String getType() {
		return type;
	}
	public String getNo() {
		return no;
	}
	public String getExpNo() {
		return expNo;
	}
	public String getScbNo() {
		return scbNo;
	}
	public int getDimnlength() {
		return dimnlength;
	}
	public int getDimnHeight() {
		return dimnHeight;
	}
	public int getDimnWidth() {
		return dimnWidth;
	}
	public int getDimn1() {
		return dimn1;
	}
	public String getDimn2() {
		return dimn2;
	}
	public String getDate1() {
		return date1;
	}
	public String getDate2() {
		return date2;
	}
	public String getDate3() {
		return date3;
	}
	public String getDate4() {
		return date4;
	}
	public String getExcuted() {
		return excuted;
	}
	public String getAt() {
		return at;
	}
	public void setShipperName1(String shipperName1) {
		this.shipperName1 = shipperName1;
	}
	public void setShipperName2(String shipperName2) {
		this.shipperName2 = shipperName2;
	}
	public void setShipperName3(String shipperName3) {
		this.shipperName3 = shipperName3;
	}
	public void setShipperName4(String shipperName4) {
		this.shipperName4 = shipperName4;
	}
	public void setShipperCity1(String shipperCity1) {
		this.shipperCity1 = shipperCity1;
	}
	public void setShipperCountry(String shipperCountry) {
		this.shipperCountry = shipperCountry;
	}
	public void setShipper2Name1(String shipper2Name1) {
		this.shipper2Name1 = shipper2Name1;
	}
	public void setShipper2Name2(String shipper2Name2) {
		this.shipper2Name2 = shipper2Name2;
	}
	public void setShipper2Name3(String shipper2Name3) {
		this.shipper2Name3 = shipper2Name3;
	}
	public void setShipper2Name4(String shipper2Name4) {
		this.shipper2Name4 = shipper2Name4;
	}
	public void setShipper2City1(String shipper2City1) {
		this.shipper2City1 = shipper2City1;
	}
	public void setShipper2Country(String shipper2Country) {
		this.shipper2Country = shipper2Country;
	}
	public void setConsigneeName1(String consigneeName1) {
		this.consigneeName1 = consigneeName1;
	}
	public void setConsigneeName2(String consigneeName2) {
		this.consigneeName2 = consigneeName2;
	}
	public void setConsigneeName3(String consigneeName3) {
		this.consigneeName3 = consigneeName3;
	}
	public void setConsigneeName4(String consigneeName4) {
		this.consigneeName4 = consigneeName4;
	}
	public void setConsigneeCity1(String consigneeCity1) {
		this.consigneeCity1 = consigneeCity1;
	}
	public void setConsigneeCountry(String consigneeCountry) {
		this.consigneeCountry = consigneeCountry;
	}
	public void setConsignee2Name1(String consignee2Name1) {
		this.consignee2Name1 = consignee2Name1;
	}
	public void setConsignee2Name2(String consignee2Name2) {
		this.consignee2Name2 = consignee2Name2;
	}
	public void setConsignee2Name3(String consignee2Name3) {
		this.consignee2Name3 = consignee2Name3;
	}
	public void setConsignee2Name4(String consignee2Name4) {
		this.consignee2Name4 = consignee2Name4;
	}
	public void setConsignee2City1(String consignee2City1) {
		this.consignee2City1 = consignee2City1;
	}
	public void setConsignee2Country(String consignee2Country) {
		this.consignee2Country = consignee2Country;
	}
	public void setNotifyName1(String notifyName1) {
		this.notifyName1 = notifyName1;
	}
	public void setNotifyName2(String notifyName2) {
		this.notifyName2 = notifyName2;
	}
	public void setNotifyName3(String notifyName3) {
		this.notifyName3 = notifyName3;
	}
	public void setNotifyName4(String notifyName4) {
		this.notifyName4 = notifyName4;
	}
	public void setNotifyCity1(String notifyCity1) {
		this.notifyCity1 = notifyCity1;
	}
	public void setNotifyCountry(String notifyCountry) {
		this.notifyCountry = notifyCountry;
	}
	public void setAirportOfDep(String airportOfDep) {
		this.airportOfDep = airportOfDep;
	}
	public void setAirportOfDes(String airportOfDes) {
		this.airportOfDes = airportOfDes;
	}
	public void setRequestedRouting(String requestedRouting) {
		this.requestedRouting = requestedRouting;
	}
	public void setIata(String iata) {
		this.iata = iata;
	}
	public void setMasterBil(String masterBil) {
		this.masterBil = masterBil;
	}
	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}
	public void setFlightDt(String flightDt) {
		this.flightDt = flightDt;
	}
	public void setNoOfPcs(int noOfPcs) {
		this.noOfPcs = noOfPcs;
	}
	public void setGw(String gw) {
		this.gw = gw;
	}
	public void setKg(String kg) {
		this.kg = kg;
	}
	public void setCw(String cw) {
		this.cw = cw;
	}
	public void setRate(String rate) {
		this.rate = rate;
	}
	public void setTotal(String total) {
		this.total = total;
	}
	public void setBiilNo(String biilNo) {
		this.biilNo = biilNo;
	}
	public void setAddressName1(String addressName1) {
		this.addressName1 = addressName1;
	}
	public void setAddressName2(String addressName2) {
		this.addressName2 = addressName2;
	}
	public void setAddressName3(String addressName3) {
		this.addressName3 = addressName3;
	}
	public void setAddressName4(String addressName4) {
		this.addressName4 = addressName4;
	}
	public void setAddressCity1(String addressCity1) {
		this.addressCity1 = addressCity1;
	}
	public void setAddressCountry(String addressCountry) {
		this.addressCountry = addressCountry;
	}
	public void setAccountInfo(String accountInfo) {
		this.accountInfo = accountInfo;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public void setInvoice(String invoice) {
		this.invoice = invoice;
	}
	public void setType(String type) {
		this.type = type;
	}
	public void setNo(String no) {
		this.no = no;
	}
	public void setExpNo(String expNo) {
		this.expNo = expNo;
	}
	public void setScbNo(String scbNo) {
		this.scbNo = scbNo;
	}
	public void setDimnlength(int dimnlength) {
		this.dimnlength = dimnlength;
	}
	public void setDimnHeight(int dimnHeight) {
		this.dimnHeight = dimnHeight;
	}
	public void setDimnWidth(int dimnWidth) {
		this.dimnWidth = dimnWidth;
	}
	public void setDimn1(int dimn1) {
		this.dimn1 = dimn1;
	}
	public void setDimn2(String dimn2) {
		this.dimn2 = dimn2;
	}
	public void setDate1(String date1) {
		this.date1 = date1;
	}
	public void setDate2(String date2) {
		this.date2 = date2;
	}
	public void setDate3(String date3) {
		this.date3 = date3;
	}
	public void setDate4(String date4) {
		this.date4 = date4;
	}
	public void setExcuted(String excuted) {
		this.excuted = excuted;
	}
	public void setAt(String at) {
		this.at = at;
	}
	
	
	
	
	

}
