package com.myshipment.model;

import java.util.Date;
/*
 * @ Hamid
 */
public class MyshipmentTrackScheduleBean {
	
	//@Parameter("SO_NO")
	private String so_no;
	//@Parameter("BL_NO")
	private String bl_no;
	//@Parameter("LEG_NO")
	private String leg_no;
	//@Parameter("CAR_TYPE")
	private String car_type;
	//@Parameter("CAR_NAME")
	private String car_name;
	//@Parameter("CAR_NO")
	private String car_no;
	//@Parameter("POL")
	private String pol;
	//@Parameter("POD")
	private String pod;
	//@Parameter("ETD")
	private String etd;
	private Date etd_openTracking;
	//@Parameter("ETA")
	private String eta;
	private Date eta_openTracking;
	
	public String getSo_no() {
		return so_no;
	}
	public void setSo_no(String so_no) {
		this.so_no = so_no;
	}
	public String getBl_no() {
		return bl_no;
	}
	public void setBl_no(String bl_no) {
		this.bl_no = bl_no;
	}
	public String getLeg_no() {
		return leg_no;
	}
	public void setLeg_no(String leg_no) {
		this.leg_no = leg_no;
	}
	public String getCar_type() {
		return car_type;
	}
	public void setCar_type(String car_type) {
		this.car_type = car_type;
	}
	public String getCar_name() {
		return car_name;
	}
	public void setCar_name(String car_name) {
		this.car_name = car_name;
	}
	public String getCar_no() {
		return car_no;
	}
	public void setCar_no(String car_no) {
		this.car_no = car_no;
	}
	public String getPol() {
		return pol;
	}
	public void setPol(String pol) {
		this.pol = pol;
	}
	public String getPod() {
		return pod;
	}
	public void setPod(String pod) {
		this.pod = pod;
	}
	public String getEtd() {
		return etd;
	}
	public void setEtd(String etd) {
		this.etd = etd;
	}
	public String getEta() {
		return eta;
	}
	public void setEta(String eta) {
		this.eta = eta;
	}
	public Date getEtd_openTracking() {
		return etd_openTracking;
	}
	public void setEtd_openTracking(Date etd_openTracking) {
		this.etd_openTracking = etd_openTracking;
	}
	public Date getEta_openTracking() {
		return eta_openTracking;
	}
	public void setEta_openTracking(Date eta_openTracking) {
		this.eta_openTracking = eta_openTracking;
	}	

	
}

