package com.myshipment.model;

public class SODashboardShipperParams {
	private String salesOrg;
	private DashboardShipperParams dashboardShipperParams;
	public String getSalesOrg() {
		return salesOrg;
	}
	public void setSalesOrg(String salesOrg) {
		this.salesOrg = salesOrg;
	}
	public DashboardShipperParams getDashboardShipperParams() {
		return dashboardShipperParams;
	}
	public void setDashboardShipperParams(DashboardShipperParams dashboardShipperParams) {
		this.dashboardShipperParams = dashboardShipperParams;
	}
	

}
