package com.myshipment.model;

public class DashboardBuyerParams {
	private String buyerNo;
	public String getBuyerNo() {
		return buyerNo;
	}
	public void setBuyerNo(String buyerNo) {
		this.buyerNo = buyerNo;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getDistChannel() {
		return distChannel;
	}
	public void setDistChannel(String distChannel) {
		this.distChannel = distChannel;
	}
	public String getBookingDateFrom() {
		return bookingDateFrom;
	}
	public void setBookingDateFrom(String bookingDateFrom) {
		this.bookingDateFrom = bookingDateFrom;
	}
	public String getBookingDateTo() {
		return bookingDateTo;
	}
	public void setBookingDateTo(String bookingDateTo) {
		this.bookingDateTo = bookingDateTo;
	}
	private String division;
	private String distChannel;
	private String bookingDateFrom;
	private String bookingDateTo;
}
