package com.myshipment.model;

/*
 * @Ranjeet Kumar
 */


public class ReportParams {

	
	private String hblNumber;
	private String saleOrg;
	private String division;
	private String distChannel;
	private String customer;
	private String invType;
	public String getHblNumber() {
		return hblNumber;
	}
	public void setHblNumber(String hblNumber) {
		this.hblNumber = hblNumber;
	}
	public String getSaleOrg() {
		return saleOrg;
	}
	public void setSaleOrg(String saleOrg) {
		this.saleOrg = saleOrg;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getDistChannel() {
		return distChannel;
	}
	public void setDistChannel(String distChannel) {
		this.distChannel = distChannel;
	}
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getInvType() {
		return invType;
	}
	public void setInvType(String invType) {
		this.invType = invType;
	}
	
	
}
