package com.myshipment.model;

public class BLStatusDetailsParamBuyer {
	private String status;
	private DashboardBuyerParams dashboardBuyerParams;
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public DashboardBuyerParams getDashboardBuyerParams() {
		return dashboardBuyerParams;
	}
	public void setDashboardBuyerParams(DashboardBuyerParams dashboardBuyerParams) {
		this.dashboardBuyerParams = dashboardBuyerParams;
	}
	

}
