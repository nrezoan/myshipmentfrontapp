package com.myshipment.model;

/*
 * @ Gufranur Rahman
 */

public class ShippingReport {
	
	private String vkOrg;
	private String vkBur;
	private String adrNr;
	private String addrNumber;
	private String name1;
	private String name2;
	private String name3;
	private String name4;
	private String city1;
	private String country;
	private String telNumber;
	private String faxNumber;
	private String land1;
	private String telefTo;
	private String smtpAddr;
	private String kunNr;
	private String vbeLn;
	private String posNr;
	private String parVn;
	private String portLoad;
	private String portDest;
	private String portFinalDest;
	private String portOfDel;
	private String streetNo;
	
	

	public String getStreetNo() {
		return streetNo;
	}

	public void setStreetNo(String streetNo) {
		this.streetNo = streetNo;
	}
	
	
	public String getVkOrg() {
		return vkOrg;
	}

	public void setVkOrg(String vkOrg) {
		this.vkOrg = vkOrg;
	}

	public String getVkBur() {
		return vkBur;
	}

	public void setVkBur(String vkBur) {
		this.vkBur = vkBur;
	}

	public String getAdrNr() {
		return adrNr;
	}

	public void setAdrNr(String adrNr) {
		this.adrNr = adrNr;
	}

	public String getAddrNumber() {
		return addrNumber;
	}

	public void setAddrNumber(String addrNumber) {
		this.addrNumber = addrNumber;
	}

	public String getName1() {
		return name1;
	}

	public void setName1(String name1) {
		this.name1 = name1;
	}

	public String getName2() {
		return name2;
	}

	public void setName2(String name2) {
		this.name2 = name2;
	}

	public String getName3() {
		return name3;
	}

	public void setName3(String name3) {
		this.name3 = name3;
	}

	public String getName4() {
		return name4;
	}

	public void setName4(String name4) {
		this.name4 = name4;
	}

	public String getCity1() {
		return city1;
	}

	public void setCity1(String city1) {
		this.city1 = city1;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getTelNumber() {
		return telNumber;
	}

	public void setTelNumber(String telNumber) {
		this.telNumber = telNumber;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getLand1() {
		return land1;
	}

	public void setLand1(String land1) {
		this.land1 = land1;
	}

	public String getTelefTo() {
		return telefTo;
	}

	public void setTelefTo(String telefTo) {
		this.telefTo = telefTo;
	}

	public String getSmtpAddr() {
		return smtpAddr;
	}

	public void setSmtpAddr(String smtpAddr) {
		this.smtpAddr = smtpAddr;
	}

	public String getKunNr() {
		return kunNr;
	}

	public void setKunNr(String kunNr) {
		this.kunNr = kunNr;
	}

	public String getVbeLn() {
		return vbeLn;
	}

	public void setVbeLn(String vbeLn) {
		this.vbeLn = vbeLn;
	}

	public String getPosNr() {
		return posNr;
	}

	public void setPosNr(String posNr) {
		this.posNr = posNr;
	}

	public String getParVn() {
		return parVn;
	}

	public void setParVn(String parVn) {
		this.parVn = parVn;
	}

	public String getPortLoad() {
		return portLoad;
	}

	public void setPortLoad(String portLoad) {
		this.portLoad = portLoad;
	}

	public String getPortDest() {
		return portDest;
	}

	public void setPortDest(String portDest) {
		this.portDest = portDest;
	}

	public String getPortFinalDest() {
		return portFinalDest;
	}

	public void setPortFinalDest(String portFinalDest) {
		this.portFinalDest = portFinalDest;
	}

	public String getPortOfDel() {
		return portOfDel;
	}

	public void setPortOfDel(String portOfDel) {
		this.portOfDel = portOfDel;
	}
	
	
}
	







	
	