package com.myshipment.model;

public class ShipperWiseShipmentCount {
	private String shipperNo;
	private String shipperName;
	private long shipmentCount;
	public String getShipperNo() {
		return shipperNo;
	}
	public void setShipperNo(String shipperNo) {
		this.shipperNo = shipperNo;
	}
	public String getShipperName() {
		return shipperName;
	}
	public void setShipperName(String shipperName) {
		this.shipperName = shipperName;
	}
	public long getShipmentCount() {
		return shipmentCount;
	}
	public void setShipmentCount(long shipmentCount) {
		this.shipmentCount = shipmentCount;
	}
	

}
