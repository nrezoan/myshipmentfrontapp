package com.myshipment.model;

import java.math.BigDecimal;

public class freightInvoicePdfGeneration {
	private String payerName1;
	private String payerName2;
	private String payerName3;
	private String payerName4;
	private String payerCity1;
	private String payerCountry;
	private String invoiceNo;
	private String soNo;
	private String currency;
	private String invoiceDate;
	private String shipper;
	private String consignee;
	private String agent;
	private String incoTerm;
	private String paymentMode;
	private String origin;
	private String destination;
	private String carrier;
	private String mvsl;
	private String mvslVoyage;
	private String fvsl;
	private String fvslVoyage;
	private String mbl;
	private String hbl;
	private String commIvoiceNo;
	private String commInvoiceDt;
	private BigDecimal totalQty;
	private String eta;
	private String etd;
	private String volume;
	private BigDecimal gw;
	private String coLoader;
	private String description;
	private String rate;
	private String sac_code;
	private String sac_code2;
	private BigDecimal conversion;
	private String conversion2;
	private String uom;
	private String description2;
	private String rate2;
	private String conversionI2;
	private String uom2;
	private BigDecimal total;
	private BigDecimal total2;
	private BigDecimal taxable;
	private BigDecimal nontaxable;
	private BigDecimal taxable2;
	private BigDecimal nontaxable2;
	private BigDecimal subTotal;
	private BigDecimal subTotal2;
	private BigDecimal taxTotal;
	private BigDecimal totalAmount;
	private String amountWords;
	private String actNo;
	private String swiftID;
	private String bankName1;
	private String bankName2;
	private String bankCity1;
	private String bankCode;
	private String bankCountry;
	private String preparedBy;
	private String serviceTaxNo;
	private String panNo;
	private String beneficiary;
	private String company;
	private String compnayAdd1;
	private String companyAdd2;
	private String companyAdd3;
	private String companyCity;
	private String telNo;
	private String fax;
	private String webAddress;
	private String companyCode;
	private String payerNo;
	private String shippmentNo;
	private String containerName;
	private String containerSize;
	private String containerName2;
	private String containerSize2;
	private int containerNumber;
	private String cgst1;
	private String cgst2;
	private BigDecimal cgst3;
	private String ifscCode;
	private String gstNo;
	private String shippingLine;
	private String tax1;
	private String tax2;
	private String tax3;
	private String flightNo;
	private BigDecimal chWt;
	private String containerName3;
	private String containerSize3;
	private String description3;
	private BigDecimal conversion3;
	private String rate3;
	private String uom3;
	private BigDecimal total3;
	private int igstRate;
	private BigDecimal igst;
	private int cgstRate;
	private BigDecimal cgst;
	private int sgstRate;
	private BigDecimal sgst;
	private int igstRate2;
	private int sgstRate2;
	private int cgstRate2;
	private BigDecimal totalAmount1;
	private String odnNumber;
	private String plos;
	private String gstn;
	
	
	
	public int getIgstRate2() {
		return igstRate2;
	}

	public void setIgstRate2(int igstRate2) {
		this.igstRate2 = igstRate2;
	}

	public int getSgstRate2() {
		return sgstRate2;
	}

	public void setSgstRate2(int sgstRate2) {
		this.sgstRate2 = sgstRate2;
	}

	public int getCgstRate2() {
		return cgstRate2;
	}

	public void setCgstRate2(int cgstRate2) {
		this.cgstRate2 = cgstRate2;
	}

	public String getPlos() {
		return plos;
	}

	public void setPlos(String plos) {
		this.plos = plos;
	}

	public String getGstn() {
		return gstn;
	}

	public void setGstn(String gstn) {
		this.gstn = gstn;
	}

	public String getOdnNumber() {
		return odnNumber;
	}

	public void setOdnNumber(String odnNumber) {
		this.odnNumber = odnNumber;
	}

	public int getIgstRate() {
		return igstRate;
	}

	public void setIgstRate(int igstRate) {
		this.igstRate = igstRate;
	}

	public BigDecimal getIgst() {
		return igst;
	}

	public void setIgst(BigDecimal igst) {
		this.igst = igst;
	}

	public int getCgstRate() {
		return cgstRate;
	}

	public void setCgstRate(int cgstRate) {
		this.cgstRate = cgstRate;
	}

	public BigDecimal getCgst() {
		return cgst;
	}

	public void setCgst(BigDecimal cgst) {
		this.cgst = cgst;
	}

	public int getSgstRate() {
		return sgstRate;
	}

	public void setSgstRate(int sgstRate) {
		this.sgstRate = sgstRate;
	}

	public BigDecimal getSgst() {
		return sgst;
	}

	public void setSgst(BigDecimal sgst) {
		this.sgst = sgst;
	}

	public BigDecimal getTotalAmount1() {
		return totalAmount1;
	}

	public void setTotalAmount1(BigDecimal totalAmount1) {
		this.totalAmount1 = totalAmount1;
	}

	public String getCompanyCity() {
		return companyCity;
	}

	public void setCompanyCity(String companyCity) {
		this.companyCity = companyCity;
	}

	public String getContainerName3() {
		return containerName3;
	}

	public String getContainerSize3() {
		return containerSize3;
	}

	public String getDescription3() {
		return description3;
	}

	public BigDecimal getConversion3() {
		return conversion3;
	}

	public String getRate3() {
		return rate3;
	}

	public String getUom3() {
		return uom3;
	}

	public BigDecimal getTotal3() {
		return total3;
	}

	public void setContainerName3(String containerName3) {
		this.containerName3 = containerName3;
	}

	public void setContainerSize3(String containerSize3) {
		this.containerSize3 = containerSize3;
	}

	public void setDescription3(String description3) {
		this.description3 = description3;
	}

	public void setConversion3(BigDecimal conversion3) {
		this.conversion3 = conversion3;
	}

	public void setRate3(String rate3) {
		this.rate3 = rate3;
	}

	public void setUom3(String uom3) {
		this.uom3 = uom3;
	}

	public void setTotal3(BigDecimal total3) {
		this.total3 = total3;
	}

	public String getContainerName2() {
		return containerName2;
	}

	public String getContainerSize2() {
		return containerSize2;
	}

	public void setContainerName2(String containerName2) {
		this.containerName2 = containerName2;
	}

	public void setContainerSize2(String containerSize2) {
		this.containerSize2 = containerSize2;
	}

	public String getDescription2() {
		return description2;
	}

	public String getRate2() {
		return rate2;
	}

	public String getConversionI2() {
		return conversionI2;
	}

	public String getUom2() {
		return uom2;
	}

	public BigDecimal getTotal2() {
		return total2;
	}

	public void setTotal2(BigDecimal total2) {
		this.total2 = total2;
	}

	public BigDecimal getTaxable2() {
		return taxable2;
	}

	public BigDecimal getNontaxable2() {
		return nontaxable2;
	}

	public String getConversion2() {
		return conversion2;
	}

	public void setConversion2(String conversion2) {
		this.conversion2 = conversion2;
	}

	public void setDescription2(String description2) {
		this.description2 = description2;
	}

	public void setRate2(String rate2) {
		this.rate2 = rate2;
	}

	public void setConversionI2(String conversionI2) {
		this.conversionI2 = conversionI2;
	}

	public void setUom2(String uom2) {
		this.uom2 = uom2;
	}

	public void setTaxable2(BigDecimal taxable2) {
		this.taxable2 = taxable2;
	}

	public void setNontaxable2(BigDecimal nontaxable2) {
		this.nontaxable2 = nontaxable2;
	}

	public BigDecimal getChWt() {
		return chWt;
	}

	public void setChWt(BigDecimal chWt) {
		this.chWt = chWt;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getTax1() {
		return tax1;
	}

	public String getTax2() {
		return tax2;
	}

	public String getTax3() {
		return tax3;
	}

	public void setTax1(String tax1) {
		this.tax1 = tax1;
	}

	public void setTax2(String tax2) {
		this.tax2 = tax2;
	}

	public void setTax3(String tax3) {
		this.tax3 = tax3;
	}

	public String getShippingLine() {
		return shippingLine;
	}

	public void setShippingLine(String shippingLine) {
		this.shippingLine = shippingLine;
	}

	public String getGstNo() {
		return gstNo;
	}

	public void setGstNo(String gstNo) {
		this.gstNo = gstNo;
	}

	public String getIfscCode() {
		return ifscCode;
	}

	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}

	public String getCgst1() {
		return cgst1;
	}

	public String getCgst2() {
		return cgst2;
	}

	public BigDecimal getCgst3() {
		return cgst3;
	}

	public void setCgst1(String cgst1) {
		this.cgst1 = cgst1;
	}

	public void setCgst2(String cgst2) {
		this.cgst2 = cgst2;
	}

	public void setCgst3(BigDecimal cgst3) {
		this.cgst3 = cgst3;
	}

	public String getContainerName() {
		return containerName;
	}

	public String getContainerSize() {
		return containerSize;
	}

	public int getContainerNumber() {
		return containerNumber;
	}

	public void setContainerName(String containerName) {
		this.containerName = containerName;
	}

	public void setContainerSize(String containerSize) {
		this.containerSize = containerSize;
	}

	public void setContainerNumber(int containerNumber) {
		this.containerNumber = containerNumber;
	}

	public String getPayerCountry() {
		return payerCountry;
	}

	public String getInvoiceNo() {
		return invoiceNo;
	}

	public String getSoNo() {
		return soNo;
	}

	public String getCompanyAdd3() {
		return companyAdd3;
	}

	public void setCompanyAdd3(String companyAdd3) {
		this.companyAdd3 = companyAdd3;
	}

	public String getCurrency() {
		return currency;
	}

	public BigDecimal getSubTotal2() {
		return subTotal2;
	}

	public void setSubTotal2(BigDecimal subTotal2) {
		this.subTotal2 = subTotal2;
	}

	public String getInvoiceDate() {
		return invoiceDate;
	}

	public String getShipper() {
		return shipper;
	}

	public String getConversionI() {
		return conversionI;
	}

	public void setConversionI(String conversionI) {
		this.conversionI = conversionI;
	}

	private String conversionI;

	public String getConsignee() {
		return consignee;
	}

	public String getAgent() {
		return agent;
	}

	public String getIncoTerm() {
		return incoTerm;
	}

	public String getPaymentMode() {
		return paymentMode;
	}

	public String getOrigin() {
		return origin;
	}

	public String getDestination() {
		return destination;
	}

	public String getCarrier() {
		return carrier;
	}

	public String getMvsl() {
		return mvsl;
	}

	public String getMvslVoyage() {
		return mvslVoyage;
	}

	public String getFvsl() {
		return fvsl;
	}

	public String getFvslVoyage() {
		return fvslVoyage;
	}

	public String getMbl() {
		return mbl;
	}

	public String getHbl() {
		return hbl;
	}

	public String getCommIvoiceNo() {
		return commIvoiceNo;
	}

	public String getCommInvoiceDt() {
		return commInvoiceDt;
	}

	public BigDecimal getTotalQty() {
		return totalQty;
	}

	public String getEta() {
		return eta;
	}

	public String getEtd() {
		return etd;
	}

	public String getVolume() {
		return volume;
	}

	public void setVolume(String volume) {
		this.volume = volume;
	}

	public BigDecimal getGw() {
		return gw;
	}

	public String getCoLoader() {
		return coLoader;
	}

	public String getDescription() {
		return description;
	}

	public String getRate() {
		return rate;
	}

	public BigDecimal getConversion() {
		return conversion;
	}

	public String getShippmentNo() {
		return shippmentNo;
	}

	public void setShippmentNo(String shippmentNo) {
		this.shippmentNo = shippmentNo;
	}

	public String getUom() {
		return uom;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public BigDecimal getTaxable() {
		return taxable;
	}

	public BigDecimal getNontaxable() {
		return nontaxable;
	}

	public BigDecimal getSubTotal() {
		return subTotal;
	}

	public BigDecimal getTaxTotal() {
		return taxTotal;
	}

	public BigDecimal getTotalAmount() {
		return totalAmount;
	}

	public String getAmountWords() {
		return amountWords;
	}

	public String getActNo() {
		return actNo;
	}

	public String getSwiftID() {
		return swiftID;
	}

	public String getBankName1() {
		return bankName1;
	}

	public String getBankName2() {
		return bankName2;
	}

	public String getBankCity1() {
		return bankCity1;
	}

	public String getBankCode() {
		return bankCode;
	}

	public String getBankCountry() {
		return bankCountry;
	}

	public String getPreparedBy() {
		return preparedBy;
	}

	public String getServiceTaxNo() {
		return serviceTaxNo;
	}

	public String getPanNo() {
		return panNo;
	}

	public String getBeneficiary() {
		return beneficiary;
	}

	public String getCompany() {
		return company;
	}

	public String getCompnayAdd1() {
		return compnayAdd1;
	}

	public String getCompanyAdd2() {
		return companyAdd2;
	}

	public String getTelNo() {
		return telNo;
	}

	public String getFax() {
		return fax;
	}

	public String getWebAddress() {
		return webAddress;
	}

	public String getPayerName1() {
		return payerName1;
	}

	public String getPayerName2() {
		return payerName2;
	}

	public String getPayerName3() {
		return payerName3;
	}

	public String getPayerName4() {
		return payerName4;
	}

	public String getPayerCity1() {
		return payerCity1;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public String getPayerNo() {
		return payerNo;
	}

	public void setPayerName1(String payerName1) {
		this.payerName1 = payerName1;
	}

	public void setPayerName2(String payerName2) {
		this.payerName2 = payerName2;
	}

	public void setPayerName3(String payerName3) {
		this.payerName3 = payerName3;
	}

	public void setPayerName4(String payerName4) {
		this.payerName4 = payerName4;
	}

	public void setPayerCity1(String payerCity1) {
		this.payerCity1 = payerCity1;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public void setPayerNo(String payerNo) {
		this.payerNo = payerNo;
	}

	public void setPayerCountry(String payerCountry) {
		this.payerCountry = payerCountry;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public void setSoNo(String soNo) {
		this.soNo = soNo;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public void setInvoiceDate(String invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public void setShipper(String shipper) {
		this.shipper = shipper;
	}

	public void setConsignee(String consignee) {
		this.consignee = consignee;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public void setIncoTerm(String incoTerm) {
		this.incoTerm = incoTerm;
	}

	public void setPaymentMode(String paymentMode) {
		this.paymentMode = paymentMode;
	}

	public void setOrigin(String origin) {
		this.origin = origin;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}

	public void setMvsl(String mvsl) {
		this.mvsl = mvsl;
	}

	public void setMvslVoyage(String mvslVoyage) {
		this.mvslVoyage = mvslVoyage;
	}

	public void setFvsl(String fvsl) {
		this.fvsl = fvsl;
	}

	public void setFvslVoyage(String fvslVoyage) {
		this.fvslVoyage = fvslVoyage;
	}

	public void setMbl(String mbl) {
		this.mbl = mbl;
	}

	public void setHbl(String hbl) {
		this.hbl = hbl;
	}

	public void setCommIvoiceNo(String commIvoiceNo) {
		this.commIvoiceNo = commIvoiceNo;
	}

	public void setCommInvoiceDt(String commInvoiceDt) {
		this.commInvoiceDt = commInvoiceDt;
	}

	public void setTotalQty(BigDecimal totalQty) {
		this.totalQty = totalQty;
	}

	public void setEta(String eta) {
		this.eta = eta;
	}

	public void setEtd(String etd) {
		this.etd = etd;
	}

	public void setGw(BigDecimal gw) {
		this.gw = gw;
	}

	public void setCoLoader(String coLoader) {
		this.coLoader = coLoader;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public void setRate(String rate) {
		this.rate = rate;
	}

	public void setConversion(BigDecimal conversion) {
		this.conversion = conversion;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public void setTaxable(BigDecimal taxable) {
		this.taxable = taxable;
	}

	public void setNontaxable(BigDecimal nontaxable) {
		this.nontaxable = nontaxable;
	}

	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}

	public void setTaxTotal(BigDecimal taxTotal) {
		this.taxTotal = taxTotal;
	}

	public void setTotalAmount(BigDecimal totalAmount) {
		this.totalAmount = totalAmount;
	}

	public void setAmountWords(String amountWords) {
		this.amountWords = amountWords;
	}

	public void setActNo(String actNo) {
		this.actNo = actNo;
	}

	public void setSwiftID(String swiftID) {
		this.swiftID = swiftID;
	}

	public void setBankName1(String bankName1) {
		this.bankName1 = bankName1;
	}

	public void setBankName2(String bankName2) {
		this.bankName2 = bankName2;
	}

	public void setBankCity1(String bankCity1) {
		this.bankCity1 = bankCity1;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	public void setBankCountry(String bankCountry) {
		this.bankCountry = bankCountry;
	}

	public void setPreparedBy(String preparedBy) {
		this.preparedBy = preparedBy;
	}

	public void setServiceTaxNo(String serviceTaxNo) {
		this.serviceTaxNo = serviceTaxNo;
	}

	public void setPanNo(String panNo) {
		this.panNo = panNo;
	}

	public void setBeneficiary(String beneficiary) {
		this.beneficiary = beneficiary;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public void setCompnayAdd1(String compnayAdd1) {
		this.compnayAdd1 = compnayAdd1;
	}

	public void setCompanyAdd2(String companyAdd2) {
		this.companyAdd2 = companyAdd2;
	}

	public void setTelNo(String telNo) {
		this.telNo = telNo;
	}

	public void setFax(String fax) {
		this.fax = fax;
	}

	public void setWebAddress(String webAddress) {
		this.webAddress = webAddress;
	}

	public String getSac_code() {
		return sac_code;
	}

	public void setSac_code(String sac_code) {
		this.sac_code = sac_code;
	}

	public String getSac_code2() {
		return sac_code2;
	}

	public void setSac_code2(String sac_code2) {
		this.sac_code2 = sac_code2;
	}

}
