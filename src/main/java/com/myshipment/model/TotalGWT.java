/**
*
*@author Ahmad Naquib
*/
package com.myshipment.model;

public class TotalGWT {

	private String id;
	private String name;
	private double totalValue;
	private double valuePercentage;
	private double totalCBMCRG;
	private double totalGWT;
	private double totalCBMCRGPercentage;
	private double totalGWTPercentage;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getTotalGWT() {
		return totalGWT;
	}
	public void setTotalGWT(double totalGWT) {
		this.totalGWT = totalGWT;
	}
	public double getTotalCBMCRG() {
		return totalCBMCRG;
	}
	public void setTotalCBMCRG(double totalCBMCRG) {
		this.totalCBMCRG = totalCBMCRG;
	}
	public double getTotalCBMCRGPercentage() {
		return totalCBMCRGPercentage;
	}
	public void setTotalCBMCRGPercentage(double totalCBMCRGPercentage) {
		this.totalCBMCRGPercentage = totalCBMCRGPercentage;
	}
	public double getTotalGWTPercentage() {
		return totalGWTPercentage;
	}
	public void setTotalGWTPercentage(double totalGWTPercentage) {
		this.totalGWTPercentage = totalGWTPercentage;
	}
	public double getTotalValue() {
		return totalValue;
	}
	public void setTotalValue(double totalValue) {
		this.totalValue = totalValue;
	}
	public double getValuePercentage() {
		return valuePercentage;
	}
	public void setValuePercentage(double valuePercentage) {
		this.valuePercentage = valuePercentage;
	}
	
}
