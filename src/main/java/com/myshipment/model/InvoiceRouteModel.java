package com.myshipment.model;

public class InvoiceRouteModel {
	private String destination;
	private String placeOfReciept;
	private String portOfLoading;
	private String portOfDischarge;
	private String placeOfDelivery;
	private  String placeOfDeliveryAddress;
	public InvoiceRouteModel() {

	}
	public InvoiceRouteModel(String destination, String placeOfReciept, String portOfLoading, String portOfDischarge,
			String placeOfDelivery, String placeOfDeliveryAddress) {
		this.destination = destination;
		this.placeOfReciept = placeOfReciept;
		this.portOfLoading = portOfLoading;
		this.portOfDischarge = portOfDischarge;
		this.placeOfDelivery = placeOfDelivery;
		this.placeOfDeliveryAddress = placeOfDeliveryAddress;
	}
	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public String getPlaceOfReciept() {
		return placeOfReciept;
	}
	public void setPlaceOfReciept(String placeOfReciept) {
		this.placeOfReciept = placeOfReciept;
	}
	public String getPortOfLoading() {
		return portOfLoading;
	}
	public void setPortOfLoading(String portOfLoading) {
		this.portOfLoading = portOfLoading;
	}
	public String getPortOfDischarge() {
		return portOfDischarge;
	}
	public void setPortOfDischarge(String portOfDischarge) {
		this.portOfDischarge = portOfDischarge;
	}
	public String getPlaceOfDelivery() {
		return placeOfDelivery;
	}
	public void setPlaceOfDelivery(String placeOfDelivery) {
		this.placeOfDelivery = placeOfDelivery;
	}
	public String getPlaceOfDeliveryAddress() {
		return placeOfDeliveryAddress;
	}
	public void setPlaceOfDeliveryAddress(String placeOfDeliveryAddress) {
		this.placeOfDeliveryAddress = placeOfDeliveryAddress;
	}
	@Override
	public String toString() {
		return "InvoiceRouteModel [destination=" + destination + ", placeOfReciept=" + placeOfReciept
				+ ", portOfLoading=" + portOfLoading + ", portOfDischarge=" + portOfDischarge + ", placeOfDelivery="
				+ placeOfDelivery + ", placeOfDeliveryAddress=" + placeOfDeliveryAddress + "]";
	}
	
}
