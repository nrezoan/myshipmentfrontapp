package com.myshipment.model;

import java.math.BigDecimal;
import java.util.Date;


public class CargoItem {

		
	private int serialNumber;	
	private String vbEln;
	private String poSnr;
	private String zzPoNumber;
	private BigDecimal zzCartonQty;
	private BigDecimal menge;
	private BigDecimal zzTotalNoPcs;
	public int getSerialNumber() {
		return serialNumber;
	}
	public void setSerialNumber(int serialNumber) {
		this.serialNumber = serialNumber;
	}
	public String getVbEln() {
		return vbEln;
	}
	public void setVbEln(String vbEln) {
		this.vbEln = vbEln;
	}
	public String getPoSnr() {
		return poSnr;
	}
	public void setPoSnr(String poSnr) {
		this.poSnr = poSnr;
	}
	public String getZzPoNumber() {
		return zzPoNumber;
	}
	public void setZzPoNumber(String zzPoNumber) {
		this.zzPoNumber = zzPoNumber;
	}
	public BigDecimal getZzCartonQty() {
		return zzCartonQty;
	}
	public void setZzCartonQty(BigDecimal zzCartonQty) {
		this.zzCartonQty = zzCartonQty;
	}
	public BigDecimal getMenge() {
		return menge;
	}
	public void setMenge(BigDecimal menge) {
		this.menge = menge;
	}
	public BigDecimal getZzTotalNoPcs() {
		return zzTotalNoPcs;
	}
	public void setZzTotalNoPcs(BigDecimal zzTotalNoPcs) {
		this.zzTotalNoPcs = zzTotalNoPcs;
	}
	
	
	
}
