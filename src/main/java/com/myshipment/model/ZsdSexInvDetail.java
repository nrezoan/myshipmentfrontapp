package com.myshipment.model;

import java.math.BigDecimal;



public class ZsdSexInvDetail {
	private String vText;	
	private String kbetr;
	private String waers;	
	private BigDecimal conv;
	private BigDecimal kpein;	
	private String kmein;
	private BigDecimal kwert;	
	private String vhilm;
	private String wgbez60;	
	private String kschl;
	private String zzContainerNo;
	private String zzContainerSize;	
	private String tdText;
	private String kStat;	
	private String flagRate;
	
	public String getvText() {
		return vText;
	}
	public void setvText(String vText) {
		this.vText = vText;
	}
	public String getKbetr() {
		return kbetr;
	}
	public void setKbetr(String kbetr) {
		this.kbetr = kbetr;
	}
	public String getWaers() {
		return waers;
	}
	public void setWaers(String waers) {
		this.waers = waers;
	}
	public BigDecimal getConv() {
		return conv;
	}
	public void setConv(BigDecimal conv) {
		this.conv = conv;
	}
	public BigDecimal getKpein() {
		return kpein;
	}
	public void setKpein(BigDecimal kpein) {
		this.kpein = kpein;
	}
	public String getKmein() {
		return kmein;
	}
	public void setKmein(String kmein) {
		this.kmein = kmein;
	}
	public BigDecimal getKwert() {
		return kwert;
	}
	public void setKwert(BigDecimal kwert) {
		this.kwert = kwert;
	}
	public String getVhilm() {
		return vhilm;
	}
	public void setVhilm(String vhilm) {
		this.vhilm = vhilm;
	}
	public String getWgbez60() {
		return wgbez60;
	}
	public void setWgbez60(String wgbez60) {
		this.wgbez60 = wgbez60;
	}
	public String getKschl() {
		return kschl;
	}
	public void setKschl(String kschl) {
		this.kschl = kschl;
	}
	public String getZzContainerNo() {
		return zzContainerNo;
	}
	public void setZzContainerNo(String zzContainerNo) {
		this.zzContainerNo = zzContainerNo;
	}
	public String getZzContainerSize() {
		return zzContainerSize;
	}
	public void setZzContainerSize(String zzContainerSize) {
		this.zzContainerSize = zzContainerSize;
	}
	public String getTdText() {
		return tdText;
	}
	public void setTdText(String tdText) {
		this.tdText = tdText;
	}
	public String getkStat() {
		return kStat;
	}
	public void setkStat(String kStat) {
		this.kStat = kStat;
	}
	public String getFlagRate() {
		return flagRate;
	}
	public void setFlagRate(String flagRate) {
		this.flagRate = flagRate;
	}
	
	
}