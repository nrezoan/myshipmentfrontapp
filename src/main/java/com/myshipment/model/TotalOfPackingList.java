package com.myshipment.model;

public class TotalOfPackingList {

	private long totalCtns;
	private double volums;
	private double netWgt;
	private double grossWgt;
	public long getTotalCtns() {
		return totalCtns;
	}
	public void setTotalCtns(long totalCtns) {
		this.totalCtns = totalCtns;
	}
	public double getVolums() {
		return volums;
	}
	public void setVolums(double volums) {
		this.volums = volums;
	}
	public double getNetWgt() {
		return netWgt;
	}
	public void setNetWgt(double netWgt) {
		this.netWgt = netWgt;
	}
	public double getGrossWgt() {
		return grossWgt;
	}
	public void setGrossWgt(double grossWgt) {
		this.grossWgt = grossWgt;
	}
	
}
