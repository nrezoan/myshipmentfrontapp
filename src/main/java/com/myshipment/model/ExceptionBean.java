package com.myshipment.model;

import java.util.List;
/*
 * @Ranjeet Kumar
 */


public class ExceptionBean {

	private int delayed=0;
	private int ontime=0;
	private int advance=0;
	private int notAvailable=0;
	public int getDelayed() {
		return delayed;
	}
	public void setDelayed(int delayed) {
		this.delayed = delayed;
	}
	public int getOntime() {
		return ontime;
	}
	public void setOntime(int ontime) {
		this.ontime = ontime;
	}
	public int getAdvance() {
		return advance;
	}
	public void setAdvance(int advance) {
		this.advance = advance;
	}
	public int getNotAvailable() {
		return notAvailable;
	}
	public void setNotAvailable(int notAvailable) {
		this.notAvailable = notAvailable;
	}
	
	
			
			
}
