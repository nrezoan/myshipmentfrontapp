/**
*
*@author Ahmad Naquib
*/
package com.myshipment.model;

import java.sql.Timestamp;

public class PreAlertHBL {

	private String hblNumber;
	private String supplierId;
	private String buyerId;
	private String agentId;
	private String filePathPreAlertDoc;
	private String fileMBL;
	private String isPreAlertDoc;
	private String isMBL;
	private String crmId;
	private String downloaded;
	private String mblNumber;
	private Timestamp updatedAt;
	
	private String distributionChannel;
	private String division;
	private String salesOrg;
	
	
	
	public Timestamp getUpdatedAt() {
		return updatedAt;
	}
	public void setUpdatedAt(Timestamp updatedAt) {
		this.updatedAt = updatedAt;
	}
	public String getHblNumber() {
		return hblNumber;
	}
	public void setHblNumber(String hblNumber) {
		this.hblNumber = hblNumber;
	}
	public String getSupplierId() {
		return supplierId;
	}
	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}
	public String getBuyerId() {
		return buyerId;
	}
	public void setBuyerId(String buyerId) {
		this.buyerId = buyerId;
	}
	public String getAgentId() {
		return agentId;
	}
	public void setAgentId(String agentId) {
		this.agentId = agentId;
	}
	public String getFilePathPreAlertDoc() {
		return filePathPreAlertDoc;
	}
	public void setFilePathPreAlertDoc(String filePathPreAlertDoc) {
		this.filePathPreAlertDoc = filePathPreAlertDoc;
	}
	public String getIsPreAlertDoc() {
		return isPreAlertDoc;
	}
	public void setIsPreAlertDoc(String isPreAlertDoc) {
		this.isPreAlertDoc = isPreAlertDoc;
	}
	public String getFileMBL() {
		return fileMBL;
	}
	public void setFileMBL(String fileMBL) {
		this.fileMBL = fileMBL;
	}
	public String getIsMBL() {
		return isMBL;
	}
	public void setIsMBL(String isMBL) {
		this.isMBL = isMBL;
	}
	public String getCrmId() {
		return crmId;
	}
	public void setCrmId(String crmId) {
		this.crmId = crmId;
	}
	public String getDistributionChannel() {
		return distributionChannel;
	}
	public void setDistributionChannel(String distributionChannel) {
		this.distributionChannel = distributionChannel;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getSalesOrg() {
		return salesOrg;
	}
	public void setSalesOrg(String salesOrg) {
		this.salesOrg = salesOrg;
	}
	public String getDownloaded() {
		return downloaded;
	}
	public void setDownloaded(String downloaded) {
		this.downloaded = downloaded;
	}
	public String getMblNumber() {
		return mblNumber;
	}
	public void setMblNumber(String mblNumber) {
		this.mblNumber = mblNumber;
	}
	
}
