package com.myshipment.model;

import java.util.List;
import java.util.Map;
import java.util.Set;
/*
 * @ Ranjeet Kumar
 */
public class HeaderAndLineitemData {

	private List<POFileDTO> lineItems;
	private Map<String, HeaderPOData> headerItem;
	private Set<String> uniquePoNo;
	
	public List<POFileDTO> getLineItems() {
		return lineItems;
	}
	public void setLineItems(List<POFileDTO> lineItems) {
		this.lineItems = lineItems;
	}
	public Map<String, HeaderPOData> getHeaderItem() {
		return headerItem;
	}
	public void setHeaderItem(Map<String, HeaderPOData> headerItem) {
		this.headerItem = headerItem;
	}
	public Set<String> getUniquePoNo() {
		return uniquePoNo;
	}
	public void setUniquePoNo(Set<String> uniquePoNo) {
		this.uniquePoNo = uniquePoNo;
	}
	
	

}
