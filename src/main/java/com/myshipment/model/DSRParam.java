package com.myshipment.model;

import java.util.Date;

public class DSRParam {
	private Date searchDate;

	public Date getSearchDate() {
		return searchDate;
	}

	public void setSearchDate(Date searchDate) {
		this.searchDate = searchDate;
	}
	

}
