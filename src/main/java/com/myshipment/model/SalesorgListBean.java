package com.myshipment.model;
/*
 * @ Ranjeet Kumar
 */


public class SalesorgListBean {
	
	private String salesOrg;	
	private String salesOrgText;	
	private String distrChan;	
	private String distrChanText;	
	private String division;	
	private String divisionText;	
	private String salesGrp;	
	private String salesgrpText;	
	private String salesOff;	
	private String salesOffText;
	public String getSalesOrg() {
		return salesOrg;
	}
	public void setSalesOrg(String salesOrg) {
		this.salesOrg = salesOrg;
	}
	public String getSalesOrgText() {
		return salesOrgText;
	}
	public void setSalesOrgText(String salesOrgText) {
		this.salesOrgText = salesOrgText;
	}
	public String getDistrChan() {
		return distrChan;
	}
	public void setDistrChan(String distrChan) {
		this.distrChan = distrChan;
	}
	public String getDistrChanText() {
		return distrChanText;
	}
	public void setDistrChanText(String distrChanText) {
		this.distrChanText = distrChanText;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getDivisionText() {
		return divisionText;
	}
	public void setDivisionText(String divisionText) {
		this.divisionText = divisionText;
	}
	public String getSalesGrp() {
		return salesGrp;
	}
	public void setSalesGrp(String salesGrp) {
		this.salesGrp = salesGrp;
	}
	public String getSalesgrpText() {
		return salesgrpText;
	}
	public void setSalesgrpText(String salesgrpText) {
		this.salesgrpText = salesgrpText;
	}
	public String getSalesOff() {
		return salesOff;
	}
	public void setSalesOff(String salesOff) {
		this.salesOff = salesOff;
	}
	public String getSalesOffText() {
		return salesOffText;
	}
	public void setSalesOffText(String salesOffText) {
		this.salesOffText = salesOffText;
	}
	
	
}
