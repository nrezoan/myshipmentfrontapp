package com.myshipment.model;

import java.util.List;
/*
 * @Ranjeet Kumar
 */
public class CommercialInvoiceDataForPage {

	private ZbapiPacListHDTO zbapiPacListHDTO;
	private List<ZbapiPacListIDTO> ZbapiPacListIDTOLst;
	
	public ZbapiPacListHDTO getZbapiPacListHDTO() {
		return zbapiPacListHDTO;
	}
	public void setZbapiPacListHDTO(ZbapiPacListHDTO zbapiPacListHDTO) {
		this.zbapiPacListHDTO = zbapiPacListHDTO;
	}
	public List<ZbapiPacListIDTO> getZbapiPacListIDTOLst() {
		return ZbapiPacListIDTOLst;
	}
	public void setZbapiPacListIDTOLst(List<ZbapiPacListIDTO> zbapiPacListIDTOLst) {
		ZbapiPacListIDTOLst = zbapiPacListIDTOLst;
	}
	
	
}
