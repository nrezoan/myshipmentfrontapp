package com.myshipment.model;

import java.util.Date;

public class inspectionBookingModel {
	private Long bookingNo;
	private String salesOrg;
	private String distChannel;
	private String division;
	private String supplierCode;
	private String buyerCode;
	private String inspectorCode;
	private Date inspectionDate;
	private String poNumber;
	private Date deliveryDate;
	private int orderQty;
	private String shipmentMode;
	private String remarks;
	private Date entryTime;
	private String sapNo;
	private int inspectionQty;
	private String location;
	private String adv_order;
	private String packMethod;
	private String customerName;
	private Date cargoDeliveryDate;
	private String scota_file_submission;
	private Date scota_file_submission_date;
	private String inspection_booking_status;

	public Long getBookingNo() {
		return bookingNo;
	}

	public void setBookingNo(Long bookingNo) {
		this.bookingNo = bookingNo;
	}

	public String getSalesOrg() {
		return salesOrg;
	}

	public void setSalesOrg(String salesOrg) {
		this.salesOrg = salesOrg;
	}

	public String getDistChannel() {
		return distChannel;
	}

	public void setDistChannel(String distChannel) {
		this.distChannel = distChannel;
	}

	public String getDivision() {
		return division;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getSupplierCode() {
		return supplierCode;
	}

	public void setSupplierCode(String supplierCode) {
		this.supplierCode = supplierCode;
	}

	public String getBuyerCode() {
		return buyerCode;
	}

	public void setBuyerCode(String buyerCode) {
		this.buyerCode = buyerCode;
	}

	public String getInspectorCode() {
		return inspectorCode;
	}

	public void setInspectorCode(String inspectorCode) {
		this.inspectorCode = inspectorCode;
	}

	public Date getInspectionDate() {
		return inspectionDate;
	}

	public void setInspectionDate(Date inspectionDate) {
		this.inspectionDate = inspectionDate;
	}

	public String getPoNumber() {
		return poNumber;
	}

	public void setPoNumber(String poNumber) {
		this.poNumber = poNumber;
	}

	public Date getDeliveryDate() {
		return deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	public int getOrderQty() {
		return orderQty;
	}

	public void setOrderQty(int orderQty) {
		this.orderQty = orderQty;
	}

	public String getShipmentMode() {
		return shipmentMode;
	}

	public void setShipmentMode(String shipmentMode) {
		this.shipmentMode = shipmentMode;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public Date getEntryTime() {
		return entryTime;
	}

	public void setEntryTime(Date entryTime) {
		this.entryTime = entryTime;
	}

	public String getSapNo() {
		return sapNo;
	}

	public void setSapNo(String sapNo) {
		this.sapNo = sapNo;
	}

	public int getInspectionQty() {
		return inspectionQty;
	}

	public void setInspectionQty(int inspectionQty) {
		this.inspectionQty = inspectionQty;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getAdv_order() {
		return adv_order;
	}

	public void setAdv_order(String adv_order) {
		this.adv_order = adv_order;
	}

	public String getPackMethod() {
		return packMethod;
	}

	public void setPackMethod(String packMethod) {
		this.packMethod = packMethod;
	}

	public String getCustomerName() {
		return customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public Date getCargoDeliveryDate() {
		return cargoDeliveryDate;
	}

	public void setCargoDeliveryDate(Date cargoDeliveryDate) {
		this.cargoDeliveryDate = cargoDeliveryDate;
	}

	public String getScota_file_submission() {
		return scota_file_submission;
	}

	public void setScota_file_submission(String scota_file_submission) {
		this.scota_file_submission = scota_file_submission;
	}

	public Date getScota_file_submission_date() {
		return scota_file_submission_date;
	}

	public void setScota_file_submission_date(Date scota_file_submission_date) {
		this.scota_file_submission_date = scota_file_submission_date;
	}

	public String getInspection_booking_status() {
		return inspection_booking_status;
	}

	public void setInspection_booking_status(String inspection_booking_status) {
		this.inspection_booking_status = inspection_booking_status;
	}

}