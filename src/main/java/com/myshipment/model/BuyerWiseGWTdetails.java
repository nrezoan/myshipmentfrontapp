package com.myshipment.model;

import java.util.List;

public class BuyerWiseGWTdetails {
	private List<BuyerWiseGWTJson> lstBuyerWiseGWTJson;
	private String message;
	public List<BuyerWiseGWTJson> getLstBuyerWiseGWTJson() {
		return lstBuyerWiseGWTJson;
	}
	public void setLstBuyerWiseGWTJson(List<BuyerWiseGWTJson> lstBuyerWiseGWTJson) {
		this.lstBuyerWiseGWTJson = lstBuyerWiseGWTJson;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String messsage) {
		this.message = messsage;
	}
	

}
