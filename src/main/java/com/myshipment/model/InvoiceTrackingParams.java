package com.myshipment.model;

public class InvoiceTrackingParams {
	private String hblNumber;
	private String fromDate;
	private String toDate;
	private String saleOrg;
	private String division;
	private String distChannel;
	private String statusType;
	private String customer;
	private String invType;
	private String blType;
	private String billStatus;

	public String getBillStatus() {
		return billStatus;
	}

	public void setBillStatus(String billStatus) {
		this.billStatus = billStatus;
	}

	public String getHblNumber() {
		return hblNumber;
	}

	public String getFromDate() {
		return fromDate;
	}

	public String getToDate() {
		return toDate;
	}

	public String getSaleOrg() {
		return saleOrg;
	}

	public String getDivision() {
		return division;
	}

	public String getDistChannel() {
		return distChannel;
	}

	public String getStatusType() {
		return statusType;
	}

	public String getCustomer() {
		return customer;
	}

	public String getInvType() {
		return invType;
	}

	public String getBlType() {
		return blType;
	}

	public void setHblNumber(String hblNumber) {
		this.hblNumber = hblNumber;
	}

	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}

	public void setToDate(String toDate) {
		this.toDate = toDate;
	}

	public void setSaleOrg(String saleOrg) {
		this.saleOrg = saleOrg;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public void setDistChannel(String distChannel) {
		this.distChannel = distChannel;
	}

	public void setStatusType(String statusType) {
		this.statusType = statusType;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	public void setInvType(String invType) {
		this.invType = invType;
	}

	public void setBlType(String blType) {
		this.blType = blType;
	}

}
