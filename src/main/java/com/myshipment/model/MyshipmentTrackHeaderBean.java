package com.myshipment.model;

import java.math.BigDecimal;
import java.util.Date;
/*
 * @Hamid
 */
public class MyshipmentTrackHeaderBean {
	//@Parameter("CUST_NO")
	private String cust_no;
	//@Parameter("COMP_CODE")
	private String comp_code;
	//@Parameter("SALES_ORG")
	private String sales_org;
	//@Parameter("SALES_ORG_DESC")
	private String sales_org_desc;
	//@Parameter("DIST_CH")
	private String dist_ch;
	//@Parameter("DIVISION")
	private String division;
	//@Parameter("ORDER_TYPE")
	private String order_type;
	//@Parameter("SO_NO")
	private String so_no;
	//@Parameter("SO_DT")
	private String so_dt;
	private Date so_dt_openTracking;
	//@Parameter("BL_NO")
	private String bl_no;
	//@Parameter("BL_DT")
	private String bl_bt;
	private Date bl_bt_openTracking;
	//@Parameter("SHIPPER")
	private String shipper;
	//@Parameter("SHIPPER_NO")
	private String shipper_no;
	//@Parameter("BUYER")
	private String buyer;
	//@Parameter("BUYER_NO")
	private String buyer_no;
	//@Parameter("AGENT")
	private String agent;
	//@Parameter("AGENT_NO")
	private String agent_no;
	//@Parameter("CARRIER")
	private String carrier;
	//@Parameter("COLOADER")
	private String coloader;
	//@Parameter("FRT_MODE")
	private String frt_mode;
	//@Parameter("FRT_MODE_DS")
	private String frt_mode_ds;
	//@Parameter("INCO1")
	private String inco1;
	//@Parameter("INCO2")
	private String inco2;
	//@Parameter("COMM_INV")
	private String comm_inv;
	//@Parameter("COMM_INV_DT")
	private String comm_inv_dt;
	private Date comm_inv_dt_openTracking;
	//@Parameter("LC_TYPE")
	private String lc_type;
	//@Parameter("LC_NO")
	private String lc_no;
	//@Parameter("LC_DT")
	private String lc_dt;
	private Date lc_dt_openTracking;
	//@Parameter("EXP_NO")
	private String exp_no;
	//@Parameter("EXP_DT")
	private String exp_dt;
	private Date exp_dt_openTracking;
	//@Parameter("SHIP_BL_NO")
	private String ship_bl_no;
	//@Parameter("SHIP_BL_DT")
	private String ship_bl_dt;
	private Date ship_bl_dt_openTracking;
	//@Parameter("EXP_CH_DT")
	private String exp_ch_dt;
	private Date exp_ch_dt_openTracking;
	//@Parameter("ORIG_CN")
	private String orig_cn;	
	//@Parameter("DEST_CN")
	private String dest_cn;
	//@Parameter("POR")
	private String por;
	//@Parameter("POL")
	private String pol;
	//@Parameter("POD")
	private String pod;
	//@Parameter("LOAD_DT")
	private String load_dt;
	private Date load_dt_openTracking;
	//@Parameter("DEP_DT")
	private String dep_dt;
	private Date dep_dt_openTracking;
	//@Parameter("ETA_DT")
	private String eta_dt;
	private Date eta_dt_openTracking;
	//@Parameter("ATA_DT")
	private String ata_dt;
	private Date ata_dt_openTracking;
	//@Parameter("PLOD")
	private String plod;
	//@Parameter("CARGO_TYPE")
	private String cargo_type;
	//@Parameter("MANIFEST")
	private String manifest;
	//@Parameter("MANF_DT")
	private String manf_dt;
	private Date manf_dt_openTracking;
	//@Parameter("NOC_DT")
	private String noc_dt;
	private Date noc_dt_openTracking;
	//@Parameter("SHIP_REL_DT")
	private String ship_rel_dt;
	private Date ship_rel_dt_openTracking;
	//@Parameter("UNSTF_DT")
	private String unstf_dt;
	private Date unstf_dt_openTracking;
	//@Parameter("TOT_QTY")
	private BigDecimal tot_qty;
	//@Parameter("NET_WT")
	private BigDecimal net_wt;
	//@Parameter("GRS_WT")
	private BigDecimal grs_wt;
	//@Parameter("VOLUME")
	private BigDecimal volume;
	//@Parameter("CRG_WT")
	private BigDecimal crg_wt;
	//@Parameter("TOT_PCS")
	private BigDecimal tot_pcs;
	//@Parameter("DOCR_DT")
	private String docr_dt;
	private Date docr_dt_openTracking;
	//@Parameter("BL_REL_DT")
	private String bl_rel_dt;
	private Date bl_rel_dt_openTracking;
	//@Parameter("COUR_NO")
	private String cour_no;
	//@Parameter("COUR_DT")
	private String cour_dt;
	private Date cour_dt_openTracking;
	//@Parameter("MBL_NO")
	private String mbl_no;
	//@Parameter("MBL_DT")
	private String mbl_dt;
	private Date mbl_dt_openTracking;
	//@Parameter("MBL2_NO")
	private String mbl2_no;
	//@Parameter("MBL2_DT")
	private String mbl2_dt;
	private Date mbl2_dt_openTracking;
	//@Parameter("MBL_TYPE")
	private String mbl_type;
	//@Parameter("SHIP_MODE")
	private String ship_mode;
	//@Parameter("CONT_MODE")
	private String cont_mode;
	//@Parameter("ROT_NO")
	private String rot_no;	
	//@Parameter("GR_DT")
	private String gr_dt;
	private Date gr_dt_openTracking;
	//@Parameter("TRUCK_NO")
	private String truck_no;
	//@Parameter("SHIPMENT_NO")
	private String shipment_no;
	//@Parameter("POD_DT")
	private String pod_dt;
	private Date pod_dt_openTracking;
	//@Parameter("STUFF_DT")
	private String stuff_dt;
	private Date stuff_dt_openTracking;
	//@Parameter("STATUS")
	private String status;
	
	public String getCust_no() {
		return cust_no;
	}
	public void setCust_no(String cust_no) {
		this.cust_no = cust_no;
	}
	public String getComp_code() {
		return comp_code;
	}
	public void setComp_code(String comp_code) {
		this.comp_code = comp_code;
	}
	public String getSales_org() {
		return sales_org;
	}
	public void setSales_org(String sales_org) {
		this.sales_org = sales_org;
	}
	public String getSales_org_desc() {
		return sales_org_desc;
	}
	public void setSales_org_desc(String sales_org_desc) {
		this.sales_org_desc = sales_org_desc;
	}
	public String getDist_ch() {
		return dist_ch;
	}
	public void setDist_ch(String dist_ch) {
		this.dist_ch = dist_ch;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getOrder_type() {
		return order_type;
	}
	public void setOrder_type(String order_type) {
		this.order_type = order_type;
	}
	public String getSo_no() {
		return so_no;
	}
	public void setSo_no(String so_no) {
		this.so_no = so_no;
	}
	public String getSo_dt() {
		return so_dt;
	}
	public void setSo_dt(String so_dt) {
		this.so_dt = so_dt;
	}
	public String getBl_no() {
		return bl_no;
	}
	public void setBl_no(String bl_no) {
		this.bl_no = bl_no;
	}
	public String getBl_bt() {
		return bl_bt;
	}
	public void setBl_bt(String bl_bt) {
		this.bl_bt = bl_bt;
	}
	public String getShipper() {
		return shipper;
	}
	public void setShipper(String shipper) {
		this.shipper = shipper;
	}
	public String getShipper_no() {
		return shipper_no;
	}
	public void setShipper_no(String shipper_no) {
		this.shipper_no = shipper_no;
	}
	public String getBuyer() {
		return buyer;
	}
	public void setBuyer(String buyer) {
		this.buyer = buyer;
	}
	public String getBuyer_no() {
		return buyer_no;
	}
	public void setBuyer_no(String buyer_no) {
		this.buyer_no = buyer_no;
	}
	public String getAgent() {
		return agent;
	}
	public void setAgent(String agent) {
		this.agent = agent;
	}
	public String getAgent_no() {
		return agent_no;
	}
	public void setAgent_no(String agent_no) {
		this.agent_no = agent_no;
	}
	public String getCarrier() {
		return carrier;
	}
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	public String getColoader() {
		return coloader;
	}
	public void setColoader(String coloader) {
		this.coloader = coloader;
	}
	public String getFrt_mode() {
		return frt_mode;
	}
	public void setFrt_mode(String frt_mode) {
		this.frt_mode = frt_mode;
	}
	public String getFrt_mode_ds() {
		return frt_mode_ds;
	}
	public void setFrt_mode_ds(String frt_mode_ds) {
		this.frt_mode_ds = frt_mode_ds;
	}
	public String getInco1() {
		return inco1;
	}
	public void setInco1(String inco1) {
		this.inco1 = inco1;
	}
	public String getInco2() {
		return inco2;
	}
	public void setInco2(String inco2) {
		this.inco2 = inco2;
	}
	public String getComm_inv() {
		return comm_inv;
	}
	public void setComm_inv(String comm_inv) {
		this.comm_inv = comm_inv;
	}
	public String getComm_inv_dt() {
		return comm_inv_dt;
	}
	public void setComm_inv_dt(String comm_inv_dt) {
		this.comm_inv_dt = comm_inv_dt;
	}
	public String getLc_type() {
		return lc_type;
	}
	public void setLc_type(String lc_type) {
		this.lc_type = lc_type;
	}
	public String getLc_no() {
		return lc_no;
	}
	public void setLc_no(String lc_no) {
		this.lc_no = lc_no;
	}
	public String getLc_dt() {
		return lc_dt;
	}
	public void setLc_dt(String lc_dt) {
		this.lc_dt = lc_dt;
	}
	public String getExp_no() {
		return exp_no;
	}
	public void setExp_no(String exp_no) {
		this.exp_no = exp_no;
	}
	public String getExp_dt() {
		return exp_dt;
	}
	public void setExp_dt(String exp_dt) {
		this.exp_dt = exp_dt;
	}
	public String getShip_bl_no() {
		return ship_bl_no;
	}
	public void setShip_bl_no(String ship_bl_no) {
		this.ship_bl_no = ship_bl_no;
	}
	public String getShip_bl_dt() {
		return ship_bl_dt;
	}
	public void setShip_bl_dt(String ship_bl_dt) {
		this.ship_bl_dt = ship_bl_dt;
	}
	public String getExp_ch_dt() {
		return exp_ch_dt;
	}
	public void setExp_ch_dt(String exp_ch_dt) {
		this.exp_ch_dt = exp_ch_dt;
	}
	public String getOrig_cn() {
		return orig_cn;
	}
	public void setOrig_cn(String orig_cn) {
		this.orig_cn = orig_cn;
	}
	public String getDest_cn() {
		return dest_cn;
	}
	public void setDest_cn(String dest_cn) {
		this.dest_cn = dest_cn;
	}
	public String getPor() {
		return por;
	}
	public void setPor(String por) {
		this.por = por;
	}
	public String getPol() {
		return pol;
	}
	public void setPol(String pol) {
		this.pol = pol;
	}
	public String getPod() {
		return pod;
	}
	public void setPod(String pod) {
		this.pod = pod;
	}
	public String getLoad_dt() {
		return load_dt;
	}
	public void setLoad_dt(String load_dt) {
		this.load_dt = load_dt;
	}
	public String getDep_dt() {
		return dep_dt;
	}
	public void setDep_dt(String dep_dt) {
		this.dep_dt = dep_dt;
	}
	public String getEta_dt() {
		return eta_dt;
	}
	public void setEta_dt(String eta_dt) {
		this.eta_dt = eta_dt;
	}
	public String getAta_dt() {
		return ata_dt;
	}
	public void setAta_dt(String ata_dt) {
		this.ata_dt = ata_dt;
	}
	public String getPlod() {
		return plod;
	}
	public void setPlod(String plod) {
		this.plod = plod;
	}
	public String getCargo_type() {
		return cargo_type;
	}
	public void setCargo_type(String cargo_type) {
		this.cargo_type = cargo_type;
	}
	public String getManifest() {
		return manifest;
	}
	public void setManifest(String manifest) {
		this.manifest = manifest;
	}
	public String getManf_dt() {
		return manf_dt;
	}
	public void setManf_dt(String manf_dt) {
		this.manf_dt = manf_dt;
	}
	public String getNoc_dt() {
		return noc_dt;
	}
	public void setNoc_dt(String noc_dt) {
		this.noc_dt = noc_dt;
	}
	public String getShip_rel_dt() {
		return ship_rel_dt;
	}
	public void setShip_rel_dt(String ship_rel_dt) {
		this.ship_rel_dt = ship_rel_dt;
	}
	public String getUnstf_dt() {
		return unstf_dt;
	}
	public void setUnstf_dt(String unstf_dt) {
		this.unstf_dt = unstf_dt;
	}
	public BigDecimal getTot_qty() {
		return tot_qty;
	}
	public void setTot_qty(BigDecimal tot_qty) {
		this.tot_qty = tot_qty;
	}
	public BigDecimal getNet_wt() {
		return net_wt;
	}
	public void setNet_wt(BigDecimal net_wt) {
		this.net_wt = net_wt;
	}
	public BigDecimal getGrs_wt() {
		return grs_wt;
	}
	public void setGrs_wt(BigDecimal grs_wt) {
		this.grs_wt = grs_wt;
	}
	public BigDecimal getVolume() {
		return volume;
	}
	public void setVolume(BigDecimal volume) {
		this.volume = volume;
	}
	public BigDecimal getCrg_wt() {
		return crg_wt;
	}
	public void setCrg_wt(BigDecimal crg_wt) {
		this.crg_wt = crg_wt;
	}
	public BigDecimal getTot_pcs() {
		return tot_pcs;
	}
	public void setTot_pcs(BigDecimal tot_pcs) {
		this.tot_pcs = tot_pcs;
	}
	public String getDocr_dt() {
		return docr_dt;
	}
	public void setDocr_dt(String docr_dt) {
		this.docr_dt = docr_dt;
	}
	public String getBl_rel_dt() {
		return bl_rel_dt;
	}
	public void setBl_rel_dt(String bl_rel_dt) {
		this.bl_rel_dt = bl_rel_dt;
	}
	public String getCour_no() {
		return cour_no;
	}
	public void setCour_no(String cour_no) {
		this.cour_no = cour_no;
	}
	public String getCour_dt() {
		return cour_dt;
	}
	public void setCour_dt(String cour_dt) {
		this.cour_dt = cour_dt;
	}
	public String getMbl_no() {
		return mbl_no;
	}
	public void setMbl_no(String mbl_no) {
		this.mbl_no = mbl_no;
	}
	public String getMbl_dt() {
		return mbl_dt;
	}
	public void setMbl_dt(String mbl_dt) {
		this.mbl_dt = mbl_dt;
	}
	public String getMbl2_no() {
		return mbl2_no;
	}
	public void setMbl2_no(String mbl2_no) {
		this.mbl2_no = mbl2_no;
	}
	public String getMbl2_dt() {
		return mbl2_dt;
	}
	public void setMbl2_dt(String mbl2_dt) {
		this.mbl2_dt = mbl2_dt;
	}
	public String getMbl_type() {
		return mbl_type;
	}
	public void setMbl_type(String mbl_type) {
		this.mbl_type = mbl_type;
	}
	public String getShip_mode() {
		return ship_mode;
	}
	public void setShip_mode(String ship_mode) {
		this.ship_mode = ship_mode;
	}
	public String getCont_mode() {
		return cont_mode;
	}
	public void setCont_mode(String cont_mode) {
		this.cont_mode = cont_mode;
	}
	public String getRot_no() {
		return rot_no;
	}
	public void setRot_no(String rot_no) {
		this.rot_no = rot_no;
	}
	public String getGr_dt() {
		return gr_dt;
	}
	public void setGr_dt(String gr_dt) {
		this.gr_dt = gr_dt;
	}
	public String getTruck_no() {
		return truck_no;
	}
	public void setTruck_no(String truck_no) {
		this.truck_no = truck_no;
	}
	public String getShipment_no() {
		return shipment_no;
	}
	public void setShipment_no(String shipment_no) {
		this.shipment_no = shipment_no;
	}
	public String getPod_dt() {
		return pod_dt;
	}
	public void setPod_dt(String pod_dt) {
		this.pod_dt = pod_dt;
	}
	public String getStuff_dt() {
		return stuff_dt;
	}
	public void setStuff_dt(String stuff_dt) {
		this.stuff_dt = stuff_dt;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Date getSo_dt_openTracking() {
		return so_dt_openTracking;
	}
	public void setSo_dt_openTracking(Date so_dt_openTracking) {
		this.so_dt_openTracking = so_dt_openTracking;
	}
	public Date getBl_bt_openTracking() {
		return bl_bt_openTracking;
	}
	public void setBl_bt_openTracking(Date bl_bt_openTracking) {
		this.bl_bt_openTracking = bl_bt_openTracking;
	}
	public Date getComm_inv_dt_openTracking() {
		return comm_inv_dt_openTracking;
	}
	public void setComm_inv_dt_openTracking(Date comm_inv_dt_openTracking) {
		this.comm_inv_dt_openTracking = comm_inv_dt_openTracking;
	}
	public Date getLc_dt_openTracking() {
		return lc_dt_openTracking;
	}
	public void setLc_dt_openTracking(Date lc_dt_openTracking) {
		this.lc_dt_openTracking = lc_dt_openTracking;
	}
	public Date getExp_dt_openTracking() {
		return exp_dt_openTracking;
	}
	public void setExp_dt_openTracking(Date exp_dt_openTracking) {
		this.exp_dt_openTracking = exp_dt_openTracking;
	}
	public Date getShip_bl_dt_openTracking() {
		return ship_bl_dt_openTracking;
	}
	public void setShip_bl_dt_openTracking(Date ship_bl_dt_openTracking) {
		this.ship_bl_dt_openTracking = ship_bl_dt_openTracking;
	}
	public Date getExp_ch_dt_openTracking() {
		return exp_ch_dt_openTracking;
	}
	public void setExp_ch_dt_openTracking(Date exp_ch_dt_openTracking) {
		this.exp_ch_dt_openTracking = exp_ch_dt_openTracking;
	}
	public Date getLoad_dt_openTracking() {
		return load_dt_openTracking;
	}
	public void setLoad_dt_openTracking(Date load_dt_openTracking) {
		this.load_dt_openTracking = load_dt_openTracking;
	}	
	public Date getEta_dt_openTracking() {
		return eta_dt_openTracking;
	}
	public void setEta_dt_openTracking(Date eta_dt_openTracking) {
		this.eta_dt_openTracking = eta_dt_openTracking;
	}
	public Date getAta_dt_openTracking() {
		return ata_dt_openTracking;
	}
	public void setAta_dt_openTracking(Date ata_dt_openTracking) {
		this.ata_dt_openTracking = ata_dt_openTracking;
	}
	public Date getManf_dt_openTracking() {
		return manf_dt_openTracking;
	}
	public void setManf_dt_openTracking(Date manf_dt_openTracking) {
		this.manf_dt_openTracking = manf_dt_openTracking;
	}
	public Date getNoc_dt_openTracking() {
		return noc_dt_openTracking;
	}
	public void setNoc_dt_openTracking(Date noc_dt_openTracking) {
		this.noc_dt_openTracking = noc_dt_openTracking;
	}
	public Date getShip_rel_dt_openTracking() {
		return ship_rel_dt_openTracking;
	}
	public void setShip_rel_dt_openTracking(Date ship_rel_dt_openTracking) {
		this.ship_rel_dt_openTracking = ship_rel_dt_openTracking;
	}
	public Date getUnstf_dt_openTracking() {
		return unstf_dt_openTracking;
	}
	public void setUnstf_dt_openTracking(Date unstf_dt_openTracking) {
		this.unstf_dt_openTracking = unstf_dt_openTracking;
	}
	public Date getDocr_dt_openTracking() {
		return docr_dt_openTracking;
	}
	public void setDocr_dt_openTracking(Date docr_dt_openTracking) {
		this.docr_dt_openTracking = docr_dt_openTracking;
	}
	public Date getBl_rel_dt_openTracking() {
		return bl_rel_dt_openTracking;
	}
	public void setBl_rel_dt_openTracking(Date bl_rel_dt_openTracking) {
		this.bl_rel_dt_openTracking = bl_rel_dt_openTracking;
	}
	public Date getCour_dt_openTracking() {
		return cour_dt_openTracking;
	}
	public void setCour_dt_openTracking(Date cour_dt_openTracking) {
		this.cour_dt_openTracking = cour_dt_openTracking;
	}
	public Date getMbl_dt_openTracking() {
		return mbl_dt_openTracking;
	}
	public void setMbl_dt_openTracking(Date mbl_dt_openTracking) {
		this.mbl_dt_openTracking = mbl_dt_openTracking;
	}
	public Date getMbl2_dt_openTracking() {
		return mbl2_dt_openTracking;
	}
	public void setMbl2_dt_openTracking(Date mbl2_dt_openTracking) {
		this.mbl2_dt_openTracking = mbl2_dt_openTracking;
	}
	public Date getGr_dt_openTracking() {
		return gr_dt_openTracking;
	}
	public void setGr_dt_openTracking(Date gr_dt_openTracking) {
		this.gr_dt_openTracking = gr_dt_openTracking;
	}
	public Date getPod_dt_openTracking() {
		return pod_dt_openTracking;
	}
	public void setPod_dt_openTracking(Date pod_dt_openTracking) {
		this.pod_dt_openTracking = pod_dt_openTracking;
	}
	public Date getStuff_dt_openTracking() {
		return stuff_dt_openTracking;
	}
	public void setStuff_dt_openTracking(Date stuff_dt_openTracking) {
		this.stuff_dt_openTracking = stuff_dt_openTracking;
	}
	public Date getDep_dt_openTracking() {
		return dep_dt_openTracking;
	}
	public void setDep_dt_openTracking(Date dep_dt_openTracking) {
		this.dep_dt_openTracking = dep_dt_openTracking;
	}
	
}
