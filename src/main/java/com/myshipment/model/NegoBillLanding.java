package com.myshipment.model;

import java.math.BigDecimal;
import java.util.Date;

/*
 * @ Gufranur Rahman
 */

public class NegoBillLanding {

	private String kunNr;
	private String adrNr;
	private String addNumber;
	private String vkOrg;
	private String nameRg;
	private String nameWe;
	private String nameZa;
	private String name1;
	private String name2;
	private String name3;
	private String name4;
	private String city1;
	private String country;
	private String landX;
	private String zzMblMabwNO;
	private String zzHblHawbNo;
	private String zzCountryOrig;
	private String zzPortName;
	private String zzAirlineName1;
	private String cntry;
	private String zzAirlineNo1;
	private String zzAirline;
	private String zzAirlineNo2;
	private String zzContractNo;
	private String zzCommInvNo;
	private String zzCommInvDt;
	private String zzLcpotNo;
	private String zzLcpottNoType;
	private String zzLcdt;
	private String zzExpNo;
	private String zzExpDt;
	private String zzSoquantity;
	private String zzTrans1Etd;
	private String zzEta;
	private BigDecimal zzGrossWeigth;
	private String zzTotalVolWt;
	private String waDat_Ist;
	private String vhilm;
	private String sealN1;
	private String ntVol;
	private String ntGew;
	private String vbEln;
	private String zzPlaceReceipt;
	private String zzPort;
	private String zzPortOfLoading;
	private String zzPortOfDest;
	private String zzPlaceDelivery;
	private String zzTransport1;
	private String zzTerms_Shipment;
	private String desc_Z018;
	private String desc_Z112;
	private String desc_Z020;
	private String desc_Z023;
	private String maTkl;
	private String wgbEZ60;
	private String vkBur;
	private String zzMode_Shipment;
	private String lGobe;
	private String zzFreightMode;
	private String zzFreightModes;
	private String bk_Name1;
	private String bk_Name2;
	private String bk_Name3;
	private String bk_Name4;
	private String bk_City1;
	private String post_Code1;
	private String acNmber;
	private String swiftId;
	private String zzTot_Chrg_Wt;
	private String zzDepartureDt;
	private String senderName;
	private String preparedBy;
	private String email;
	private String telNO;
	private String faxNo;
	private String words;
	private Date zzLoadingDt;
	private Date zzHblHawBdt;
	private String zzShipping_Bl_No;
	private Date zzShipping_Bl_Dt;
	private String zzModeOfContainR;

	public String getKunNr() {
		return kunNr;
	}

	public void setKunNr(String kunNr) {
		this.kunNr = kunNr;
	}

	public String getAdrNr() {
		return adrNr;
	}

	public void setAdrNr(String adrNr) {
		this.adrNr = adrNr;
	}

	public String getAddNumber() {
		return addNumber;
	}

	public void setAddNumber(String addNumber) {
		this.addNumber = addNumber;
	}

	public String getVkOrg() {
		return vkOrg;
	}

	public void setVkOrg(String vkOrg) {
		this.vkOrg = vkOrg;
	}

	public String getNameRg() {
		return nameRg;
	}

	public void setNameRg(String nameRg) {
		this.nameRg = nameRg;
	}

	public String getNameWe() {
		return nameWe;
	}

	public void setNameWe(String nameWe) {
		this.nameWe = nameWe;
	}

	public String getNameZa() {
		return nameZa;
	}

	public void setNameZa(String nameZa) {
		this.nameZa = nameZa;
	}

	public String getName1() {
		return name1;
	}

	public void setName1(String name1) {
		this.name1 = name1;
	}

	public String getName2() {
		return name2;
	}

	public void setName2(String name2) {
		this.name2 = name2;
	}

	public String getName3() {
		return name3;
	}

	public void setName3(String name3) {
		this.name3 = name3;
	}

	public String getName4() {
		return name4;
	}

	public void setName4(String name4) {
		this.name4 = name4;
	}

	public String getCity1() {
		return city1;
	}

	public void setCity1(String city1) {
		this.city1 = city1;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getLandX() {
		return landX;
	}

	public void setLandX(String landX) {
		this.landX = landX;
	}

	public String getZzMblMabwNO() {
		return zzMblMabwNO;
	}

	public void setZzMblMabwNO(String zzMblMabwNO) {
		this.zzMblMabwNO = zzMblMabwNO;
	}

	public String getZzHblHawbNo() {
		return zzHblHawbNo;
	}

	public void setZzHblHawbNo(String zzHblHawbNo) {
		this.zzHblHawbNo = zzHblHawbNo;
	}

	public String getZzCountryOrig() {
		return zzCountryOrig;
	}

	public void setZzCountryOrig(String zzCountryOrig) {
		this.zzCountryOrig = zzCountryOrig;
	}

	public String getZzPortName() {
		return zzPortName;
	}

	public void setZzPortName(String zzPortName) {
		this.zzPortName = zzPortName;
	}

	public String getZzAirlineName1() {
		return zzAirlineName1;
	}

	public void setZzAirlineName1(String zzAirlineName1) {
		this.zzAirlineName1 = zzAirlineName1;
	}

	public String getCntry() {
		return cntry;
	}

	public void setCntry(String cntry) {
		this.cntry = cntry;
	}

	public String getZzAirlineNo1() {
		return zzAirlineNo1;
	}

	public void setZzAirlineNo1(String zzAirlineNo1) {
		this.zzAirlineNo1 = zzAirlineNo1;
	}

	public String getZzAirline() {
		return zzAirline;
	}

	public void setZzAirline(String zzAirline) {
		this.zzAirline = zzAirline;
	}

	public String getZzAirlineNo2() {
		return zzAirlineNo2;
	}

	public void setZzAirlineNo2(String zzAirlineNo2) {
		this.zzAirlineNo2 = zzAirlineNo2;
	}

	public String getZzContractNo() {
		return zzContractNo;
	}

	public void setZzContractNo(String zzContractNo) {
		this.zzContractNo = zzContractNo;
	}

	public String getZzCommInvNo() {
		return zzCommInvNo;
	}

	public void setZzCommInvNo(String zzCommInvNo) {
		this.zzCommInvNo = zzCommInvNo;
	}

	public String getZzCommInvDt() {
		return zzCommInvDt;
	}

	public void setZzCommInvDt(String zzCommInvDt) {
		this.zzCommInvDt = zzCommInvDt;
	}

	public String getZzLcpotNo() {
		return zzLcpotNo;
	}

	public void setZzLcpotNo(String zzLcpotNo) {
		this.zzLcpotNo = zzLcpotNo;
	}

	public String getZzLcpottNoType() {
		return zzLcpottNoType;
	}

	public void setZzLcpottNoType(String zzLcpottNoType) {
		this.zzLcpottNoType = zzLcpottNoType;
	}

	public String getZzLcdt() {
		return zzLcdt;
	}

	public void setZzLcdt(String zzLcdt) {
		this.zzLcdt = zzLcdt;
	}

	public String getZzExpNo() {
		return zzExpNo;
	}

	public void setZzExpNo(String zzExpNo) {
		this.zzExpNo = zzExpNo;
	}

	public String getZzExpDt() {
		return zzExpDt;
	}

	public void setZzExpDt(String zzExpDt) {
		this.zzExpDt = zzExpDt;
	}

	public String getZzSoquantity() {
		return zzSoquantity;
	}

	public void setZzSoquantity(String zzSoquantity) {
		this.zzSoquantity = zzSoquantity;
	}

	public String getZzTrans1Etd() {
		return zzTrans1Etd;
	}

	public void setZzTrans1Etd(String zzTrans1Etd) {
		this.zzTrans1Etd = zzTrans1Etd;
	}

	public String getZzEta() {
		return zzEta;
	}

	public void setZzEta(String zzEta) {
		this.zzEta = zzEta;
	}

	
	public BigDecimal getZzGrossWeigth() {
		return zzGrossWeigth;
	}

	public void setZzGrossWeigth(BigDecimal zzGrossWeigth) {
		this.zzGrossWeigth = zzGrossWeigth;
	}

	public String getZzTotalVolWt() {
		return zzTotalVolWt;
	}

	public void setZzTotalVolWt(String zzTotalVolWt) {
		this.zzTotalVolWt = zzTotalVolWt;
	}

	public String getWaDat_Ist() {
		return waDat_Ist;
	}

	public void setWaDat_Ist(String waDat_Ist) {
		this.waDat_Ist = waDat_Ist;
	}

	public String getVhilm() {
		return vhilm;
	}

	public void setVhilm(String vhilm) {
		this.vhilm = vhilm;
	}

	public String getSealN1() {
		return sealN1;
	}

	public void setSealN1(String sealN1) {
		this.sealN1 = sealN1;
	}

	public String getNtVol() {
		return ntVol;
	}

	public void setNtVol(String ntVol) {
		this.ntVol = ntVol;
	}

	public String getNtGew() {
		return ntGew;
	}

	public void setNtGew(String ntGew) {
		this.ntGew = ntGew;
	}

	public String getVbEln() {
		return vbEln;
	}

	public void setVbEln(String vbEln) {
		this.vbEln = vbEln;
	}

	public String getZzPlaceReceipt() {
		return zzPlaceReceipt;
	}

	public void setZzPlaceReceipt(String zzPlaceReceipt) {
		this.zzPlaceReceipt = zzPlaceReceipt;
	}

	public String getZzPort() {
		return zzPort;
	}

	public void setZzPort(String zzPort) {
		this.zzPort = zzPort;
	}

	public String getZzPortOfLoading() {
		return zzPortOfLoading;
	}

	public void setZzPortOfLoading(String zzPortOfLoading) {
		this.zzPortOfLoading = zzPortOfLoading;
	}

	public String getZzPortOfDest() {
		return zzPortOfDest;
	}

	public void setZzPortOfDest(String zzPortOfDest) {
		this.zzPortOfDest = zzPortOfDest;
	}

	public String getZzPlaceDelivery() {
		return zzPlaceDelivery;
	}

	public void setZzPlaceDelivery(String zzPlaceDelivery) {
		this.zzPlaceDelivery = zzPlaceDelivery;
	}

	public String getZzTransport1() {
		return zzTransport1;
	}

	public void setZzTransport1(String zzTransport1) {
		this.zzTransport1 = zzTransport1;
	}

	public String getZzTerms_Shipment() {
		return zzTerms_Shipment;
	}

	public void setZzTerms_Shipment(String zzTerms_Shipment) {
		this.zzTerms_Shipment = zzTerms_Shipment;
	}

	public String getDesc_Z018() {
		return desc_Z018;
	}

	public void setDesc_Z018(String desc_Z018) {
		this.desc_Z018 = desc_Z018;
	}

	public String getDesc_Z112() {
		return desc_Z112;
	}

	public void setDesc_Z112(String desc_Z112) {
		this.desc_Z112 = desc_Z112;
	}

	public String getDesc_Z020() {
		return desc_Z020;
	}

	public void setDesc_Z020(String desc_Z020) {
		this.desc_Z020 = desc_Z020;
	}

	public String getDesc_Z023() {
		return desc_Z023;
	}

	public void setDesc_Z023(String desc_Z023) {
		this.desc_Z023 = desc_Z023;
	}

	public String getMaTkl() {
		return maTkl;
	}

	public void setMaTkl(String maTkl) {
		this.maTkl = maTkl;
	}

	public String getWgbEZ60() {
		return wgbEZ60;
	}

	public void setWgbEZ60(String wgbEZ60) {
		this.wgbEZ60 = wgbEZ60;
	}

	public String getVkBur() {
		return vkBur;
	}

	public void setVkBur(String vkBur) {
		this.vkBur = vkBur;
	}

	public String getZzMode_Shipment() {
		return zzMode_Shipment;
	}

	public void setZzMode_Shipment(String zzMode_Shipment) {
		this.zzMode_Shipment = zzMode_Shipment;
	}

	public String getlGobe() {
		return lGobe;
	}

	public void setlGobe(String lGobe) {
		this.lGobe = lGobe;
	}

	public String getZzFreightMode() {
		return zzFreightMode;
	}

	public void setZzFreightMode(String zzFreightMode) {
		this.zzFreightMode = zzFreightMode;
	}

	public String getZzFreightModes() {
		return zzFreightModes;
	}

	public void setZzFreightModes(String zzFreightModes) {
		this.zzFreightModes = zzFreightModes;
	}

	public String getBk_Name1() {
		return bk_Name1;
	}

	public void setBk_Name1(String bk_Name1) {
		this.bk_Name1 = bk_Name1;
	}

	public String getBk_Name2() {
		return bk_Name2;
	}

	public void setBk_Name2(String bk_Name2) {
		this.bk_Name2 = bk_Name2;
	}

	public String getBk_Name3() {
		return bk_Name3;
	}

	public void setBk_Name3(String bk_Name3) {
		this.bk_Name3 = bk_Name3;
	}

	public String getBk_Name4() {
		return bk_Name4;
	}

	public void setBk_Name4(String bk_Name4) {
		this.bk_Name4 = bk_Name4;
	}

	public String getBk_City1() {
		return bk_City1;
	}

	public void setBk_City1(String bk_City1) {
		this.bk_City1 = bk_City1;
	}

	public String getPost_Code1() {
		return post_Code1;
	}

	public void setPost_Code1(String post_Code1) {
		this.post_Code1 = post_Code1;
	}

	public String getAcNmber() {
		return acNmber;
	}

	public void setAcNmber(String acNmber) {
		this.acNmber = acNmber;
	}

	public String getSwiftId() {
		return swiftId;
	}

	public void setSwiftId(String swiftId) {
		this.swiftId = swiftId;
	}

	public String getZzTot_Chrg_Wt() {
		return zzTot_Chrg_Wt;
	}

	public void setZzTot_Chrg_Wt(String zzTot_Chrg_Wt) {
		this.zzTot_Chrg_Wt = zzTot_Chrg_Wt;
	}

	public String getZzDepartureDt() {
		return zzDepartureDt;
	}

	public void setZzDepartureDt(String zzDepartureDt) {
		this.zzDepartureDt = zzDepartureDt;
	}

	public String getSenderName() {
		return senderName;
	}

	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}

	public String getPreparedBy() {
		return preparedBy;
	}

	public void setPreparedBy(String preparedBy) {
		this.preparedBy = preparedBy;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelNO() {
		return telNO;
	}

	public void setTelNO(String telNO) {
		this.telNO = telNO;
	}

	public String getFaxNo() {
		return faxNo;
	}

	public void setFaxNo(String faxNo) {
		this.faxNo = faxNo;
	}

	public String getWords() {
		return words;
	}

	public void setWords(String words) {
		this.words = words;
	}

	public Date getZzLoadingDt() {
		return zzLoadingDt;
	}

	public void setZzLoadingDt(Date zzLoadingDt) {
		this.zzLoadingDt = zzLoadingDt;
	}

	public Date getZzHblHawBdt() {
		return zzHblHawBdt;
	}

	public void setZzHblHawBdt(Date zzHblHawBdt) {
		this.zzHblHawBdt = zzHblHawBdt;
	}

	public String getZzShipping_Bl_No() {
		return zzShipping_Bl_No;
	}

	public void setZzShipping_Bl_No(String zzShipping_Bl_No) {
		this.zzShipping_Bl_No = zzShipping_Bl_No;
	}

	public Date getZzShipping_Bl_Dt() {
		return zzShipping_Bl_Dt;
	}

	public void setZzShipping_Bl_Dt(Date zzShipping_Bl_Dt) {
		this.zzShipping_Bl_Dt = zzShipping_Bl_Dt;
	}

	public String getZzModeOfContainR() {
		return zzModeOfContainR;
	}

	public void setZzModeOfContainR(String zzModeOfContainR) {
		this.zzModeOfContainR = zzModeOfContainR;
	}
	
	
	
}
