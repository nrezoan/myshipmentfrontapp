package com.myshipment.model;
/*
 * @Mohammad Salahuddin
 */
public class PackListParams {

	private String hblNo;

	public String getHblNo() {
		return hblNo;
	}

	public void setHblNo(String hblNo) {
		this.hblNo = hblNo;
	}
	
	
}
