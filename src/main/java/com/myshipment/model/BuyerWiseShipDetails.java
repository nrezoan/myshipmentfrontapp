package com.myshipment.model;

import java.util.List;

public class BuyerWiseShipDetails {
	private List<BuyerWiseShipmentJson> lstBuyerWiseShipmentJson;
	private String message;
	public List<BuyerWiseShipmentJson> getLstBuyerWiseShipmentJson() {
		return lstBuyerWiseShipmentJson;
	}
	public void setLstBuyerWiseShipmentJson(List<BuyerWiseShipmentJson> lstBuyerWiseShipmentJson) {
		this.lstBuyerWiseShipmentJson = lstBuyerWiseShipmentJson;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	

}
