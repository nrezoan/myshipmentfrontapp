package com.myshipment.model;
/*
 * @author Nusrat Momtahana
 *
 */
public class StuffingReportJasper {
	private String shipperName;
	private String buyerName;
	private String hbl;
	private String bookingDt;
	private String comInvNo;
	private String comInvDt;
	private String contractNo;
	private String contractDt;
	private String pol;
	private String pod;
	private String cargoDt;
	private String etd;
	private String eta;
	private String tos;
	private String fvsl;
	private String mvsl;
	private String stuffingDt;
	private String dischPort;
	private String transPort;
	private String deliveryDt;
	private String orderNo;
	private String skuNo;
	private String styleNo;
	private String color;
	private String containerNo;
	private String containerSize;
	private String containerSeal;
	private String freightMode;
	private int cartonQty;
	private Double cbm;
	private int noOfPcs;
	private Double grWt;
	private String companyCode;
	private String address1;
	private String address2;
	private String address3;
	private String address4;
	private String city;
	private String country;

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getAddress4() {
		return address4;
	}

	public void setAddress4(String address4) {
		this.address4 = address4;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getShipperName() {
		return shipperName;
	}

	public void setShipperName(String shipperName) {
		this.shipperName = shipperName;
	}

	public String getBuyerName() {
		return buyerName;
	}

	public void setBuyerName(String buyerName) {
		this.buyerName = buyerName;
	}

	public String getHbl() {
		return hbl;
	}

	public void setHbl(String hbl) {
		this.hbl = hbl;
	}

	public String getBookingDt() {
		return bookingDt;
	}

	public void setBookingDt(String bookingDt) {
		this.bookingDt = bookingDt;
	}

	public String getComInvNo() {
		return comInvNo;
	}

	public void setComInvNo(String comInvNo) {
		this.comInvNo = comInvNo;
	}

	public String getComInvDt() {
		return comInvDt;
	}

	public void setComInvDt(String comInvDt) {
		this.comInvDt = comInvDt;
	}

	public String getContractNo() {
		return contractNo;
	}

	public void setContractNo(String contractNo) {
		this.contractNo = contractNo;
	}

	public String getContractDt() {
		return contractDt;
	}

	public void setContractDt(String contractDt) {
		this.contractDt = contractDt;
	}

	public String getPol() {
		return pol;
	}

	public void setPol(String pol) {
		this.pol = pol;
	}

	public String getPod() {
		return pod;
	}

	public void setPod(String pod) {
		this.pod = pod;
	}

	public String getCargoDt() {
		return cargoDt;
	}

	public void setCargoDt(String cargoDt) {
		this.cargoDt = cargoDt;
	}

	public String getEtd() {
		return etd;
	}

	public void setEtd(String etd) {
		this.etd = etd;
	}

	public String getEta() {
		return eta;
	}

	public void setEta(String eta) {
		this.eta = eta;
	}

	public String getTos() {
		return tos;
	}

	public void setTos(String tos) {
		this.tos = tos;
	}

	public String getFvsl() {
		return fvsl;
	}

	public void setFvsl(String fvsl) {
		this.fvsl = fvsl;
	}

	public String getMvsl() {
		return mvsl;
	}

	public void setMvsl(String mvsl) {
		this.mvsl = mvsl;
	}

	public String getStuffingDt() {
		return stuffingDt;
	}

	public void setStuffingDt(String stuffingDt) {
		this.stuffingDt = stuffingDt;
	}

	public String getDischPort() {
		return dischPort;
	}

	public void setDischPort(String dischPort) {
		this.dischPort = dischPort;
	}

	public String getTransPort() {
		return transPort;
	}

	public void setTransPort(String transPort) {
		this.transPort = transPort;
	}

	public String getDeliveryDt() {
		return deliveryDt;
	}

	public void setDeliveryDt(String deliveryDt) {
		this.deliveryDt = deliveryDt;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getSkuNo() {
		return skuNo;
	}

	public void setSkuNo(String skuNo) {
		this.skuNo = skuNo;
	}

	public String getStyleNo() {
		return styleNo;
	}

	public void setStyleNo(String styleNo) {
		this.styleNo = styleNo;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getContainerNo() {
		return containerNo;
	}

	public void setContainerNo(String containerNo) {
		this.containerNo = containerNo;
	}

	public String getContainerSize() {
		return containerSize;
	}

	public void setContainerSize(String containerSize) {
		this.containerSize = containerSize;
	}

	public String getContainerSeal() {
		return containerSeal;
	}

	public void setContainerSeal(String containerSeal) {
		this.containerSeal = containerSeal;
	}

	public String getFreightMode() {
		return freightMode;
	}

	public void setFreightMode(String freightMode) {
		this.freightMode = freightMode;
	}

	public int getCartonQty() {
		return cartonQty;
	}

	public void setCartonQty(int cartonQty) {
		this.cartonQty = cartonQty;
	}

	public Double getCbm() {
		return cbm;
	}

	public void setCbm(Double cbm) {
		this.cbm = cbm;
	}

	public int getNoOfPcs() {
		return noOfPcs;
	}

	public void setNoOfPcs(int noOfPcs) {
		this.noOfPcs = noOfPcs;
	}

	public Double getGrWt() {
		return grWt;
	}

	public void setGrWt(Double grWt) {
		this.grWt = grWt;
	}

}
