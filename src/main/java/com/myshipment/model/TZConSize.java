package com.myshipment.model;


public class TZConSize {
	private String vbeln;
	private String zsize;
	private int num;


	public String getVbeln() {
		return vbeln;
	}

	public void setVbeln(String vbeln) {
		this.vbeln = vbeln;
	}

	public String getZsize() {
		return zsize;
	}

	public void setZsize(String zsize) {
		this.zsize = zsize;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	
	
	
}