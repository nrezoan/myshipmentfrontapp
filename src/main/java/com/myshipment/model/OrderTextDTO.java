package com.myshipment.model;
/*
 * @Ranjeet Kumar
 */
public class OrderTextDTO {

	private String soNumber;	
	private String itemNumber;	
	private String textId;	
	private String textId2;	
	private String textId3;	
	private String textId4;	
	private String line;	
	private String line2;	
	private String line3;	
	private String line4;
	public String getSoNumber() {
		return soNumber;
	}
	public void setSoNumber(String soNumber) {
		this.soNumber = soNumber;
	}
	public String getItemNumber() {
		return itemNumber;
	}
	public void setItemNumber(String itemNumber) {
		this.itemNumber = itemNumber;
	}
	public String getTextId() {
		return textId;
	}
	public void setTextId(String textId) {
		this.textId = textId;
	}
	public String getTextId2() {
		return textId2;
	}
	public void setTextId2(String textId2) {
		this.textId2 = textId2;
	}
	public String getTextId3() {
		return textId3;
	}
	public void setTextId3(String textId3) {
		this.textId3 = textId3;
	}
	public String getTextId4() {
		return textId4;
	}
	public void setTextId4(String textId4) {
		this.textId4 = textId4;
	}
	public String getLine() {
		return line;
	}
	public void setLine(String line) {
		this.line = line;
	}
	public String getLine2() {
		return line2;
	}
	public void setLine2(String line2) {
		this.line2 = line2;
	}
	public String getLine3() {
		return line3;
	}
	public void setLine3(String line3) {
		this.line3 = line3;
	}
	public String getLine4() {
		return line4;
	}
	public void setLine4(String line4) {
		this.line4 = line4;
	}
	
	
}
