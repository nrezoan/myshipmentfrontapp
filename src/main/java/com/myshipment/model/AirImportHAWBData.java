package com.myshipment.model;

import java.util.List;
/*
 * @Ranjeet Kumar
 */
public class AirImportHAWBData {

	private List<MyshipmentTrackHeaderBean> myshipmentTrackHeader;		
	private List<MyshipmentTrackItemBean> myshipmentTrackItem;		
	private List<MyshipmentTrackScheduleBean> myshipmentTrackSchedule;
	
	public List<MyshipmentTrackHeaderBean> getMyshipmentTrackHeader() {
		return myshipmentTrackHeader;
	}
	public void setMyshipmentTrackHeader(
			List<MyshipmentTrackHeaderBean> myshipmentTrackHeader) {
		this.myshipmentTrackHeader = myshipmentTrackHeader;
	}
	public List<MyshipmentTrackItemBean> getMyshipmentTrackItem() {
		return myshipmentTrackItem;
	}
	public void setMyshipmentTrackItem(
			List<MyshipmentTrackItemBean> myshipmentTrackItem) {
		this.myshipmentTrackItem = myshipmentTrackItem;
	}
	public List<MyshipmentTrackScheduleBean> getMyshipmentTrackSchedule() {
		return myshipmentTrackSchedule;
	}
	public void setMyshipmentTrackSchedule(
			List<MyshipmentTrackScheduleBean> myshipmentTrackSchedule) {
		this.myshipmentTrackSchedule = myshipmentTrackSchedule;
	}
	
	
	
}
