package com.myshipment.model;
/*
 * @ Gufranur Rahman
 */


public class FrieghtModeBean {
	
	private String zzfreightMode;	
	private String zzfreightModes;
	public String getZzfreightMode() {
		return zzfreightMode;
	}
	public void setZzfreightMode(String zzfreightMode) {
		this.zzfreightMode = zzfreightMode;
	}
	public String getZzfreightModes() {
		return zzfreightModes;
	}
	public void setZzfreightModes(String zzfreightModes) {
		this.zzfreightModes = zzfreightModes;
	}
	
	
}
