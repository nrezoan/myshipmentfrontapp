package com.myshipment.model;

public class LineItemObjectModel {
	private String poNo;
	private String totalPieces;
	private String totalGrossWeight;
	private String cartonQuantity;
	private String cartonLength;
	private String cartonHeight;
	private String cartonWidth;
	private String hSCode;
	public LineItemObjectModel() {

	}
	public LineItemObjectModel(String poNo, String totalPieces, String totalGrossWeight, String cartonQuantity,
			String cartonLength, String cartonHeight, String cartonWidth, String hSCode) {
		super();
		this.poNo = poNo;
		this.totalPieces = totalPieces;
		this.totalGrossWeight = totalGrossWeight;
		this.cartonQuantity = cartonQuantity;
		this.cartonLength = cartonLength;
		this.cartonHeight = cartonHeight;
		this.cartonWidth = cartonWidth;
		this.hSCode = hSCode;
	}
	public String getPoNo() {
		return poNo;
	}
	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}
	public String getTotalPieces() {
		return totalPieces;
	}
	public void setTotalPieces(String totalPieces) {
		this.totalPieces = totalPieces;
	}
	public String getTotalGrossWeight() {
		return totalGrossWeight;
	}
	public void setTotalGrossWeight(String totalGrossWeight) {
		this.totalGrossWeight = totalGrossWeight;
	}
	public String getCartonQuantity() {
		return cartonQuantity;
	}
	public void setCartonQuantity(String cartonQuantity) {
		this.cartonQuantity = cartonQuantity;
	}
	public String getCartonLength() {
		return cartonLength;
	}
	public void setCartonLength(String cartonLength) {
		this.cartonLength = cartonLength;
	}
	public String getCartonHeight() {
		return cartonHeight;
	}
	public void setCartonHeight(String cartonHeight) {
		this.cartonHeight = cartonHeight;
	}
	public String getCartonWidth() {
		return cartonWidth;
	}
	public void setCartonWidth(String cartonWidth) {
		this.cartonWidth = cartonWidth;
	}
	public String gethSCode() {
		return hSCode;
	}
	public void sethSCode(String hSCode) {
		this.hSCode = hSCode;
	}
	@Override
	public String toString() {
		return "LineItemObject [poNo=" + poNo + ", totalPieces=" + totalPieces + ", totalGrossWeight="
				+ totalGrossWeight + ", cartonQuantity=" + cartonQuantity + ", cartonLength=" + cartonLength
				+ ", cartonHeight=" + cartonHeight + ", cartonWidth=" + cartonWidth + ", hSCode=" + hSCode + "]";
	}

}
