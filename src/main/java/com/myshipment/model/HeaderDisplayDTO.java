package com.myshipment.model;

import java.util.Date;
/*
 * @Ranjeet Kumar
 */
public class HeaderDisplayDTO {

	private String zzfreightmode;	
	private String salesOrg;	
	private String distChannel;	
	private String division;	
	private String customerNumber;	
	private String hblNo;	
	private String partnerName;	
	private String partnerAddr1;	
	private String partnerAddr2;	
	private String partnerAddr3;	
	private String partner;	
	private String partnerNo;
	private String partnerFunction;
	private String addrGroup;
	private String addrNumber;
	private String salesOffice;
	private String fvslOrAirlineName1;
	private String voyageOrFlightNo1;
	private String mvslOrAirlineName2;
	private String voyageOrFlightNo2;
	private String mvsl2OrAirlineName3;
	private String voyageOrFlightNo3;
	private String mvsl3OrAirlineName4;
	private String voyageOrFlightNo4;
	private String zzcomminvno;
	private String zzcomminvdt;
	private String zzexpno;
	private String zzexpdt;
	private String zzlcpottnotype;
	private String zzlcpottno;
	private String zzlcdt;
	private String zzlcexpdt;
	private String zzplacereceipt;
	private String zzcrgohandoverdt;
	private String zzportofloading;
	private String zzloadingdt;
	private String zzdeparturedt;
	private String zzportofdest;
	private String zzplacedelivery;
	private String zzhblinitial;
	private String zzshpmntvldtydt;
	public String getZzfreightmode() {
		return zzfreightmode;
	}
	public void setZzfreightmode(String zzfreightmode) {
		this.zzfreightmode = zzfreightmode;
	}
	public String getSalesOrg() {
		return salesOrg;
	}
	public void setSalesOrg(String salesOrg) {
		this.salesOrg = salesOrg;
	}
	public String getDistChannel() {
		return distChannel;
	}
	public void setDistChannel(String distChannel) {
		this.distChannel = distChannel;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getCustomerNumber() {
		return customerNumber;
	}
	public void setCustomerNumber(String customerNumber) {
		this.customerNumber = customerNumber;
	}
	public String getHblNo() {
		return hblNo;
	}
	public void setHblNo(String hblNo) {
		this.hblNo = hblNo;
	}
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	public String getPartnerAddr1() {
		return partnerAddr1;
	}
	public void setPartnerAddr1(String partnerAddr1) {
		this.partnerAddr1 = partnerAddr1;
	}
	public String getPartnerAddr2() {
		return partnerAddr2;
	}
	public void setPartnerAddr2(String partnerAddr2) {
		this.partnerAddr2 = partnerAddr2;
	}
	public String getPartnerAddr3() {
		return partnerAddr3;
	}
	public void setPartnerAddr3(String partnerAddr3) {
		this.partnerAddr3 = partnerAddr3;
	}
	public String getPartner() {
		return partner;
	}
	public void setPartner(String partner) {
		this.partner = partner;
	}
	public String getPartnerNo() {
		return partnerNo;
	}
	public void setPartnerNo(String partnerNo) {
		this.partnerNo = partnerNo;
	}
	public String getPartnerFunction() {
		return partnerFunction;
	}
	public void setPartnerFunction(String partnerFunction) {
		this.partnerFunction = partnerFunction;
	}
	public String getAddrGroup() {
		return addrGroup;
	}
	public void setAddrGroup(String addrGroup) {
		this.addrGroup = addrGroup;
	}
	public String getAddrNumber() {
		return addrNumber;
	}
	public void setAddrNumber(String addrNumber) {
		this.addrNumber = addrNumber;
	}
	public String getSalesOffice() {
		return salesOffice;
	}
	public void setSalesOffice(String salesOffice) {
		this.salesOffice = salesOffice;
	}
	public String getFvslOrAirlineName1() {
		return fvslOrAirlineName1;
	}
	public void setFvslOrAirlineName1(String fvslOrAirlineName1) {
		this.fvslOrAirlineName1 = fvslOrAirlineName1;
	}
	public String getVoyageOrFlightNo1() {
		return voyageOrFlightNo1;
	}
	public void setVoyageOrFlightNo1(String voyageOrFlightNo1) {
		this.voyageOrFlightNo1 = voyageOrFlightNo1;
	}
	public String getMvslOrAirlineName2() {
		return mvslOrAirlineName2;
	}
	public void setMvslOrAirlineName2(String mvslOrAirlineName2) {
		this.mvslOrAirlineName2 = mvslOrAirlineName2;
	}
	public String getVoyageOrFlightNo2() {
		return voyageOrFlightNo2;
	}
	public void setVoyageOrFlightNo2(String voyageOrFlightNo2) {
		this.voyageOrFlightNo2 = voyageOrFlightNo2;
	}
	public String getMvsl2OrAirlineName3() {
		return mvsl2OrAirlineName3;
	}
	public void setMvsl2OrAirlineName3(String mvsl2OrAirlineName3) {
		this.mvsl2OrAirlineName3 = mvsl2OrAirlineName3;
	}
	public String getVoyageOrFlightNo3() {
		return voyageOrFlightNo3;
	}
	public void setVoyageOrFlightNo3(String voyageOrFlightNo3) {
		this.voyageOrFlightNo3 = voyageOrFlightNo3;
	}
	public String getMvsl3OrAirlineName4() {
		return mvsl3OrAirlineName4;
	}
	public void setMvsl3OrAirlineName4(String mvsl3OrAirlineName4) {
		this.mvsl3OrAirlineName4 = mvsl3OrAirlineName4;
	}
	public String getVoyageOrFlightNo4() {
		return voyageOrFlightNo4;
	}
	public void setVoyageOrFlightNo4(String voyageOrFlightNo4) {
		this.voyageOrFlightNo4 = voyageOrFlightNo4;
	}
	public String getZzcomminvno() {
		return zzcomminvno;
	}
	public void setZzcomminvno(String zzcomminvno) {
		this.zzcomminvno = zzcomminvno;
	}
	public String getZzcomminvdt() {
		return zzcomminvdt;
	}
	public void setZzcomminvdt(String zzcomminvdt) {
		this.zzcomminvdt = zzcomminvdt;
	}
	public String getZzexpno() {
		return zzexpno;
	}
	public void setZzexpno(String zzexpno) {
		this.zzexpno = zzexpno;
	}
	public String getZzexpdt() {
		return zzexpdt;
	}
	public void setZzexpdt(String zzexpdt) {
		this.zzexpdt = zzexpdt;
	}
	public String getZzlcpottnotype() {
		return zzlcpottnotype;
	}
	public void setZzlcpottnotype(String zzlcpottnotype) {
		this.zzlcpottnotype = zzlcpottnotype;
	}
	public String getZzlcpottno() {
		return zzlcpottno;
	}
	public void setZzlcpottno(String zzlcpottno) {
		this.zzlcpottno = zzlcpottno;
	}
	public String getZzlcdt() {
		return zzlcdt;
	}
	public void setZzlcdt(String zzlcdt) {
		this.zzlcdt = zzlcdt;
	}
	public String getZzlcexpdt() {
		return zzlcexpdt;
	}
	public void setZzlcexpdt(String zzlcexpdt) {
		this.zzlcexpdt = zzlcexpdt;
	}
	public String getZzplacereceipt() {
		return zzplacereceipt;
	}
	public void setZzplacereceipt(String zzplacereceipt) {
		this.zzplacereceipt = zzplacereceipt;
	}
	public String getZzcrgohandoverdt() {
		return zzcrgohandoverdt;
	}
	public void setZzcrgohandoverdt(String zzcrgohandoverdt) {
		this.zzcrgohandoverdt = zzcrgohandoverdt;
	}
	public String getZzportofloading() {
		return zzportofloading;
	}
	public void setZzportofloading(String zzportofloading) {
		this.zzportofloading = zzportofloading;
	}
	public String getZzloadingdt() {
		return zzloadingdt;
	}
	public void setZzloadingdt(String zzloadingdt) {
		this.zzloadingdt = zzloadingdt;
	}
	public String getZzdeparturedt() {
		return zzdeparturedt;
	}
	public void setZzdeparturedt(String zzdeparturedt) {
		this.zzdeparturedt = zzdeparturedt;
	}
	public String getZzportofdest() {
		return zzportofdest;
	}
	public void setZzportofdest(String zzportofdest) {
		this.zzportofdest = zzportofdest;
	}
	public String getZzplacedelivery() {
		return zzplacedelivery;
	}
	public void setZzplacedelivery(String zzplacedelivery) {
		this.zzplacedelivery = zzplacedelivery;
	}
	public String getZzhblinitial() {
		return zzhblinitial;
	}
	public void setZzhblinitial(String zzhblinitial) {
		this.zzhblinitial = zzhblinitial;
	}
	public String getZzshpmntvldtydt() {
		return zzshpmntvldtydt;
	}
	public void setZzshpmntvldtydt(String zzshpmntvldtydt) {
		this.zzshpmntvldtydt = zzshpmntvldtydt;
	}
	
	
}
