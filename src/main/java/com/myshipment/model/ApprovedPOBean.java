package com.myshipment.model;

import java.util.List;
/*
 * @Ranjeet Kumar
 */
public class ApprovedPOBean {

	private List<SegPurchaseOrderMdl> segPurOrder;
	private List<ApprovedPurchaseOrder> approvedPO;
	private List<UpdateEvent> updateEvent;
	
	public List<SegPurchaseOrderMdl> getSegPurOrder() {
		return segPurOrder;
	}
	public void setSegPurOrder(List<SegPurchaseOrderMdl> segPurOrder) {
		this.segPurOrder = segPurOrder;
	}
	public List<ApprovedPurchaseOrder> getApprovedPO() {
		return approvedPO;
	}
	public void setApprovedPO(List<ApprovedPurchaseOrder> approvedPO) {
		this.approvedPO = approvedPO;
	}
	public List<UpdateEvent> getUpdateEvent() {
		return updateEvent;
	}
	public void setUpdateEvent(List<UpdateEvent> updateEvent) {
		this.updateEvent = updateEvent;
	}
	
	
	
}
