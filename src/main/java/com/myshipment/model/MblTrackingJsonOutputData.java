package com.myshipment.model;

import java.io.Serializable;
import java.util.List;
/*
 * @Ranjeet Kumar
 */
public class MblTrackingJsonOutputData implements Serializable{
	
	private static final long serialVersionUID = 1L;

	private List<MblTrackingResultBean> trackingData;

	public List<MblTrackingResultBean> getTrackingData() {
		return trackingData;
	}

	public void setTrackingData(List<MblTrackingResultBean> trackingData) {
		this.trackingData = trackingData;
	}

	
	
}
