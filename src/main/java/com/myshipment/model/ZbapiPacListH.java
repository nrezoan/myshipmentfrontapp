package com.myshipment.model;

import java.math.BigDecimal;
import java.util.Date;


/*
 * @Ranjeet Kumar
 */

public class ZbapiPacListH {

	
	private String vbeln;
	
	private String zzcomminvno;

	private Date zzcomminvdt;
	
	private String kunnr;
	
	private String nameWe;
	
	private String nameAg;

	private String nameZy;
	
	private String nameZo;

	private String nameZs;
	
	private String nameZb;

	private String nameZa;
	
	private String nameRg;
	
	private String name1;
	
	private String name2;

	private String name3;

	private String name4;
	
	private String city1;
	
	private String country;

	private String landx;

	private String zzmblmawbno;
	
	private Date zzmblmawbdt;
	
	private String zhblhawno;

	private Date zzhblhawbdt;
	
	private String zzexpno;
	
	private Date zzexpdt;
	
	private String zzlcpottno;
	
	private Date zzlcdt;
	
	//private Integer zzsoquantity;
	private BigDecimal zzsoquantity;
	
	//private Integer zzsoquanuom;//data type?
	private String zzsoquanuom;
	
	private String zzcntryOrig;

	private String zzportoffinaldst;

	private String vkorg;

	private Date audat;

	private String zzcntryDest;

	private String spart;

	private String zzplacereceipt;
	
	private Date zzlcexpdt;
	
	private String zzportofloading;
	
	private String zzportloading;
	
	private String zzportofdest;

	private String zzportdest;
	
	private String zzplacedelivery;
	
	private String zzplacedel;
	
	private String zzlcpottnotype;
	
	private String zzhblhawbno;
	
	private String zzmode_shipment;
	
	private String inco1;

	private String desc_z005;
	
	private String desc_z023;
	
	private String desc_z026;
	
	private String desc_z027;
	
	private String desc_z028;
	
	private String desc_z029;
	
	private String desc_z030;
	
	private String desc_z031;

	private String desc_z032;
	
	private String desc_z033;

	private String desc_z112;
	
	private String desc_z247;
	
	private String desc_z248;
	
	private String zztot;
	
	private String inco2;
	
	private String zzplofdeltext;
	
	private String zzunit_cost_unit;
	
	private String ztot_qty;
	
	private String ztot_set;
	
	public String getVbeln() {
		return vbeln;
	}
	public void setVbeln(String vbeln) {
		this.vbeln = vbeln;
	}
	public String getZzcomminvno() {
		return zzcomminvno;
	}
	public void setZzcomminvno(String zzcomminvno) {
		this.zzcomminvno = zzcomminvno;
	}
	public Date getZzcomminvdt() {
		return zzcomminvdt;
	}
	public void setZzcomminvdt(Date zzcomminvdt) {
		this.zzcomminvdt = zzcomminvdt;
	}
	public String getKunnr() {
		return kunnr;
	}
	public void setKunnr(String kunnr) {
		this.kunnr = kunnr;
	}
	public String getNameWe() {
		return nameWe;
	}
	public void setNameWe(String nameWe) {
		this.nameWe = nameWe;
	}
	public String getNameAg() {
		return nameAg;
	}
	public void setNameAg(String nameAg) {
		this.nameAg = nameAg;
	}
	public String getNameZy() {
		return nameZy;
	}
	public void setNameZy(String nameZy) {
		this.nameZy = nameZy;
	}
	public String getNameZo() {
		return nameZo;
	}
	public void setNameZo(String nameZo) {
		this.nameZo = nameZo;
	}
	public String getNameZs() {
		return nameZs;
	}
	public void setNameZs(String nameZs) {
		this.nameZs = nameZs;
	}
	public String getNameZb() {
		return nameZb;
	}
	public void setNameZb(String nameZb) {
		this.nameZb = nameZb;
	}
	public String getNameZa() {
		return nameZa;
	}
	public void setNameZa(String nameZa) {
		this.nameZa = nameZa;
	}
	public String getNameRg() {
		return nameRg;
	}
	public void setNameRg(String nameRg) {
		this.nameRg = nameRg;
	}
	public String getName1() {
		return name1;
	}
	public void setName1(String name1) {
		this.name1 = name1;
	}
	public String getName2() {
		return name2;
	}
	public void setName2(String name2) {
		this.name2 = name2;
	}
	public String getName3() {
		return name3;
	}
	public void setName3(String name3) {
		this.name3 = name3;
	}
	public String getName4() {
		return name4;
	}
	public void setName4(String name4) {
		this.name4 = name4;
	}
	public String getCity1() {
		return city1;
	}
	public void setCity1(String city1) {
		this.city1 = city1;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getLandx() {
		return landx;
	}
	public void setLandx(String landx) {
		this.landx = landx;
	}
	public String getZzmblmawbno() {
		return zzmblmawbno;
	}
	public void setZzmblmawbno(String zzmblmawbno) {
		this.zzmblmawbno = zzmblmawbno;
	}
	public Date getZzmblmawbdt() {
		return zzmblmawbdt;
	}
	public void setZzmblmawbdt(Date zzmblmawbdt) {
		this.zzmblmawbdt = zzmblmawbdt;
	}
	public String getZhblhawno() {
		return zhblhawno;
	}
	public void setZhblhawno(String zhblhawno) {
		this.zhblhawno = zhblhawno;
	}
	public Date getZzhblhawbdt() {
		return zzhblhawbdt;
	}
	public void setZzhblhawbdt(Date zzhblhawbdt) {
		this.zzhblhawbdt = zzhblhawbdt;
	}
	public String getZzexpno() {
		return zzexpno;
	}
	public void setZzexpno(String zzexpno) {
		this.zzexpno = zzexpno;
	}
	public Date getZzexpdt() {
		return zzexpdt;
	}
	public void setZzexpdt(Date zzexpdt) {
		this.zzexpdt = zzexpdt;
	}
	public String getZzlcpottno() {
		return zzlcpottno;
	}
	public void setZzlcpottno(String zzlcpottno) {
		this.zzlcpottno = zzlcpottno;
	}
	public Date getZzlcdt() {
		return zzlcdt;
	}
	public void setZzlcdt(Date zzlcdt) {
		this.zzlcdt = zzlcdt;
	}
/*	public Integer getZzsoquantity() {
		return zzsoquantity;
	}
	public void setZzsoquantity(Integer zzsoquantity) {
		this.zzsoquantity = zzsoquantity;
	}*/
/*	public Integer getZzsoquanuom() {
		return zzsoquanuom;
	}
	public void setZzsoquanuom(Integer zzsoquanuom) {
		this.zzsoquanuom = zzsoquanuom;
	}*/
	public String getZzcntryOrig() {
		return zzcntryOrig;
	}
	public void setZzcntryOrig(String zzcntryOrig) {
		this.zzcntryOrig = zzcntryOrig;
	}
	public String getZzportoffinaldst() {
		return zzportoffinaldst;
	}
	public void setZzportoffinaldst(String zzportoffinaldst) {
		this.zzportoffinaldst = zzportoffinaldst;
	}
	public String getVkorg() {
		return vkorg;
	}
	public void setVkorg(String vkorg) {
		this.vkorg = vkorg;
	}
	public Date getAudat() {
		return audat;
	}
	public void setAudat(Date audat) {
		this.audat = audat;
	}
	public String getZzcntryDest() {
		return zzcntryDest;
	}
	public void setZzcntryDest(String zzcntryDest) {
		this.zzcntryDest = zzcntryDest;
	}
	public BigDecimal getZzsoquantity() {
		return zzsoquantity;
	}
	public void setZzsoquantity(BigDecimal zzsoquantity) {
		this.zzsoquantity = zzsoquantity;
	}
	public String getZzsoquanuom() {
		return zzsoquanuom;
	}
	public void setZzsoquanuom(String zzsoquanuom) {
		this.zzsoquanuom = zzsoquanuom;
	}
	public String getSpart() {
		return spart;
	}
	public void setSpart(String spart) {
		this.spart = spart;
	}
	public String getZzplacereceipt() {
		return zzplacereceipt;
	}
	public void setZzplacereceipt(String zzplacereceipt) {
		this.zzplacereceipt = zzplacereceipt;
	}
	public Date getZzlcexpdt() {
		return zzlcexpdt;
	}
	public void setZzlcexpdt(Date zzlcexpdt) {
		this.zzlcexpdt = zzlcexpdt;
	}
	public String getZzportofloading() {
		return zzportofloading;
	}
	public void setZzportofloading(String zzportofloading) {
		this.zzportofloading = zzportofloading;
	}
	public String getZzportloading() {
		return zzportloading;
	}
	public void setZzportloading(String zzportloading) {
		this.zzportloading = zzportloading;
	}
	public String getZzportofdest() {
		return zzportofdest;
	}
	public void setZzportofdest(String zzportofdest) {
		this.zzportofdest = zzportofdest;
	}
	public String getZzportdest() {
		return zzportdest;
	}
	public void setZzportdest(String zzportdest) {
		this.zzportdest = zzportdest;
	}
	public String getZzplacedelivery() {
		return zzplacedelivery;
	}
	public void setZzplacedelivery(String zzplacedelivery) {
		this.zzplacedelivery = zzplacedelivery;
	}
	public String getZzplacedel() {
		return zzplacedel;
	}
	public void setZzplacedel(String zzplacedel) {
		this.zzplacedel = zzplacedel;
	}
	public String getZzlcpottnotype() {
		return zzlcpottnotype;
	}
	public void setZzlcpottnotype(String zzlcpottnotype) {
		this.zzlcpottnotype = zzlcpottnotype;
	}
	public String getZzhblhawbno() {
		return zzhblhawbno;
	}
	public void setZzhblhawbno(String zzhblhawbno) {
		this.zzhblhawbno = zzhblhawbno;
	}
	public String getZzmode_shipment() {
		return zzmode_shipment;
	}
	public void setZzmode_shipment(String zzmode_shipment) {
		this.zzmode_shipment = zzmode_shipment;
	}
	public String getInco1() {
		return inco1;
	}
	public void setInco1(String inco1) {
		this.inco1 = inco1;
	}
	public String getDesc_z005() {
		return desc_z005;
	}
	public void setDesc_z005(String desc_z005) {
		this.desc_z005 = desc_z005;
	}
	public String getDesc_z023() {
		return desc_z023;
	}
	public void setDesc_z023(String desc_z023) {
		this.desc_z023 = desc_z023;
	}
	public String getDesc_z026() {
		return desc_z026;
	}
	public void setDesc_z026(String desc_z026) {
		this.desc_z026 = desc_z026;
	}
	public String getDesc_z027() {
		return desc_z027;
	}
	public void setDesc_z027(String desc_z027) {
		this.desc_z027 = desc_z027;
	}
	public String getDesc_z028() {
		return desc_z028;
	}
	public void setDesc_z028(String desc_z028) {
		this.desc_z028 = desc_z028;
	}
	public String getDesc_z029() {
		return desc_z029;
	}
	public void setDesc_z029(String desc_z029) {
		this.desc_z029 = desc_z029;
	}
	public String getDesc_z030() {
		return desc_z030;
	}
	public void setDesc_z030(String desc_z030) {
		this.desc_z030 = desc_z030;
	}
	public String getDesc_z031() {
		return desc_z031;
	}
	public void setDesc_z031(String desc_z031) {
		this.desc_z031 = desc_z031;
	}
	public String getDesc_z032() {
		return desc_z032;
	}
	public void setDesc_z032(String desc_z032) {
		this.desc_z032 = desc_z032;
	}
	public String getDesc_z033() {
		return desc_z033;
	}
	public void setDesc_z033(String desc_z033) {
		this.desc_z033 = desc_z033;
	}
	public String getDesc_z112() {
		return desc_z112;
	}
	public void setDesc_z112(String desc_z112) {
		this.desc_z112 = desc_z112;
	}
	public String getDesc_z247() {
		return desc_z247;
	}
	public void setDesc_z247(String desc_z247) {
		this.desc_z247 = desc_z247;
	}
	public String getDesc_z248() {
		return desc_z248;
	}
	public void setDesc_z248(String desc_z248) {
		this.desc_z248 = desc_z248;
	}
	public String getZztot() {
		return zztot;
	}
	public void setZztot(String zztot) {
		this.zztot = zztot;
	}
	public String getInco2() {
		return inco2;
	}
	public void setInco2(String inco2) {
		this.inco2 = inco2;
	}
	public String getZzplofdeltext() {
		return zzplofdeltext;
	}
	public void setZzplofdeltext(String zzplofdeltext) {
		this.zzplofdeltext = zzplofdeltext;
	}
	public String getZzunit_cost_unit() {
		return zzunit_cost_unit;
	}
	public void setZzunit_cost_unit(String zzunit_cost_unit) {
		this.zzunit_cost_unit = zzunit_cost_unit;
	}
	public String getZtot_qty() {
		return ztot_qty;
	}
	public void setZtot_qty(String ztot_qty) {
		this.ztot_qty = ztot_qty;
	}
	public String getZtot_set() {
		return ztot_set;
	}
	public void setZtot_set(String ztot_set) {
		this.ztot_set = ztot_set;
	}
	
	
}
