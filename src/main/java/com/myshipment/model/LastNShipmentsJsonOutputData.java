package com.myshipment.model;

import java.util.List;
/*
 * @Ranjeet Kumar
 */
public class LastNShipmentsJsonOutputData {

	private List<ItHeaderBean> itHeaderBeanLst;
	private BapiReturn1 bapiReturn1;
	
	public List<ItHeaderBean> getItHeaderBeanLst() {
		return itHeaderBeanLst;
	}
	public void setItHeaderBeanLst(List<ItHeaderBean> itHeaderBeanLst) {
		this.itHeaderBeanLst = itHeaderBeanLst;
	}
	public BapiReturn1 getBapiReturn1() {
		return bapiReturn1;
	}
	public void setBapiReturn1(BapiReturn1 bapiReturn1) {
		this.bapiReturn1 = bapiReturn1;
	}
	
	
}
