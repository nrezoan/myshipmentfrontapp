package com.myshipment.model;

import java.util.List;

public class PackingListItems {
	private String cartonNumbers;
	private String itemReference;
	private long noOfCartons;
	private String colors;
	private List<Integer> sizes;
	private int pcs;
	private Integer pcsPerCarton;
	private long quantityInPcs;
	public String getCartonNumbers() {
		return cartonNumbers;
	}
	public void setCartonNumbers(String cartonNumbers) {
		this.cartonNumbers = cartonNumbers;
	}
	public String getItemReference() {
		return itemReference;
	}
	public void setItemReference(String itemReference) {
		this.itemReference = itemReference;
	}
	public long getNoOfCartons() {
		return noOfCartons;
	}
	public void setNoOfCartons(long noOfCartons) {
		this.noOfCartons = noOfCartons;
	}
	public String getColors() {
		return colors;
	}
	public void setColors(String colors) {
		this.colors = colors;
	}
	public List<Integer> getSizes() {
		return sizes;
	}
	public void setSizes(List<Integer> sizes) {
		this.sizes = sizes;
	}
	public int getPcs() {
		return pcs;
	}
	public void setPcs(int pcs) {
		this.pcs = pcs;
	}
	public Integer getPcsPerCarton() {
		return pcsPerCarton;
	}
	public void setPcsPerCarton(Integer pcsPerCarton) {
		this.pcsPerCarton = pcsPerCarton;
	}
	public long getQuantityInPcs() {
		return quantityInPcs;
	}
	public void setQuantityInPcs(long quantityInPcs) {
		this.quantityInPcs = quantityInPcs;
	}
	

}
