package com.myshipment.model;
/*
 * @Mohammad Salahuddin
 */
public class VesselTrackingParam {

	private String hbl_no;
	private int mmso;
	private String vessel_name;
	
	public String getHbl_no() {
		return hbl_no;
	}
	public void setHbl_no(String hbl_no) {
		this.hbl_no = hbl_no;
	}
	public int getMmso() {
		return mmso;
	}
	public void setMmso(int mmso) {
		this.mmso = mmso;
	}
	public String getVessel_name() {
		return vessel_name;
	}
	public void setVessel_name(String vessel_name) {
		this.vessel_name = vessel_name;
	}
	
	
	
	
	
}
