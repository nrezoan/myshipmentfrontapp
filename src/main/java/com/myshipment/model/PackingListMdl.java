package com.myshipment.model;

import java.util.List;

public class PackingListMdl {
	private static final Long serialVersionUID=-43453907877l;
	private long plId;
	private String hblNumber;
	private String zSalesDocument;
	private String ctnSrlNo;
	private long noOfCtnls;
	private List<PackListColor> packListColor;
	public long getPlId() {
		return plId;
	}
	public void setPlId(long plId) {
		this.plId = plId;
	}
	
	public String getHblNumber() {
		return hblNumber;
	}
	public void setHblNumber(String hblNumber) {
		this.hblNumber = hblNumber;
	}
	public String getzSalesDocument() {
		return zSalesDocument;
	}
	public void setzSalesDocument(String zSalesDocument) {
		this.zSalesDocument = zSalesDocument;
	}
	public String getCtnSrlNo() {
		return ctnSrlNo;
	}
	public void setCtnSrlNo(String ctnSrlNo) {
		this.ctnSrlNo = ctnSrlNo;
	}
	public long getNoOfCtnls() {
		return noOfCtnls;
	}
	public void setNoOfCtnls(long noOfCtnls) {
		this.noOfCtnls = noOfCtnls;
	}
	public List<PackListColor> getPackListColor() {
		return packListColor;
	}
	public void setPackListColor(List<PackListColor> packListColor) {
		this.packListColor = packListColor;
	}
	
	

}
