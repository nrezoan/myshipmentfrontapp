package com.myshipment.model;

import java.util.Comparator;

public class TopFiveShipmentBuyer {
	private String portOfOrigin;
	private Double totalShipment;
	private Double totalCBM;
	private Double totalGWT;
	/* Hamid */
	private String portOfOriginDesc;	
	
	public String getPortOfOriginDesc() {
		return portOfOriginDesc;
	}
	public void setPortOfOriginDesc(String portOfOriginDesc) {
		this.portOfOriginDesc = portOfOriginDesc;
	}
	public String getPortOfOrigin() {
		return portOfOrigin;
	}
	public void setPortOfOrigin(String portOfOrigin) {
		this.portOfOrigin = portOfOrigin;
	}
	public Double getTotalShipment() {
		return totalShipment;
	}
	public void setTotalShipment(Double totalShipment) {
		this.totalShipment = totalShipment;
	}
	public Double getTotalCBM() {
		return totalCBM;
	}
	public void setTotalCBM(Double totalCBM) {
		this.totalCBM = totalCBM;
	}
	public Double getTotalGWT() {
		return totalGWT;
	}
	public void setTotalGWT(Double totalGWT) {
		this.totalGWT = totalGWT;
	}
	public static Comparator<TopFiveShipmentBuyer> TopFiveShipJsonDataComparatorByPOD=new Comparator<TopFiveShipmentBuyer>() {
		
		@Override
		public int compare(TopFiveShipmentBuyer o1, TopFiveShipmentBuyer o2) {
			return o1.portOfOrigin.compareTo(o2.portOfOrigin);
			
		}
	};

public static Comparator<TopFiveShipmentBuyer> TopFiveShipJsonDataComparatorByTotShip=new Comparator<TopFiveShipmentBuyer>() {
		
		@Override
		public int compare(TopFiveShipmentBuyer o1, TopFiveShipmentBuyer o2) {
			 Double d1=o1.totalShipment;
			 Double d2=o2.totalShipment;
			 return d1.compareTo(d2);
			
		}
	};
	
	
public static Comparator<TopFiveShipmentBuyer> TopFiveShipJsonDataComparatorByTotCBM=new Comparator<TopFiveShipmentBuyer>() {
		
		@Override
		public int compare(TopFiveShipmentBuyer o1, TopFiveShipmentBuyer o2) {
			 Double d1=o1.totalCBM;
			 Double d2=o2.totalCBM;
			 return d1.compareTo(d2);
			
		}
	};
public static Comparator<TopFiveShipmentBuyer> TopFiveShipJsonDataComparatorByTotGWT=new Comparator<TopFiveShipmentBuyer>() {
		
		@Override
		public int compare(TopFiveShipmentBuyer o1, TopFiveShipmentBuyer o2) {
			 Double d1=o1.totalGWT;
			 Double d2=o2.totalGWT;
			 return d1.compareTo(d2);
			
		}
	};

@Override
public String toString() {
	return "TopFiveShipmentBuyer [portOfOrigin=" + portOfOrigin + ", totalShipment=" + totalShipment + ", totalCBM="
			+ totalCBM + ", totalGWT=" + totalGWT + "]";
}

	

}
