package com.myshipment.model;

import java.util.Date;

public class CRMLogger {
	private String shipper;
	private String buyer;
	private String division;
	private String distChannel;
	private String salesOrg;
	private String hblNumber;
	private String dataType;
	private String lineItemNo;
	private String prev;
	private String updated;
	private String fieldName;
	private Date updatedDate;
	private Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getShipper() {
		return shipper;
	}

	public void setShipper(String shipper) {
		this.shipper = shipper;
	}

	public String getBuyer() {
		return buyer;
	}

	public void setBuyer(String buyer) {
		this.buyer = buyer;
	}

	public String getDivision() {
		return division;
	}

	public String getSalesOrg() {
		return salesOrg;
	}

	public void setSalesOrg(String salesOrg) {
		this.salesOrg = salesOrg;
	}

	public void setDivision(String division) {
		this.division = division;
	}

	public String getDistChannel() {
		return distChannel;
	}

	public void setDistChannel(String distChannel) {
		this.distChannel = distChannel;
	}

	public String getHblNumber() {
		return hblNumber;
	}

	public void setHblNumber(String hblNumber) {
		this.hblNumber = hblNumber;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getLineItemNo() {
		return lineItemNo;
	}

	public void setLineItemNo(String lineItemNo) {
		this.lineItemNo = lineItemNo;
	}

	public String getPrev() {
		return prev;
	}

	public void setPrev(String prev) {
		this.prev = prev;
	}

	public String getUpdated() {
		return updated;
	}

	public void setUpdated(String updated) {
		this.updated = updated;
	}

	public String getFieldName() {
		return fieldName;
	}

	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}
