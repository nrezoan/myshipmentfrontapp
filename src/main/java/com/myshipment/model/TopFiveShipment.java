package com.myshipment.model;

import java.util.List;

public class TopFiveShipment {
	private List<TopFiveShipmentJsonData> topFiveShipmentJsonData;
	private String message;
	public List<TopFiveShipmentJsonData> getTopFiveShipmentJsonData() {
		return topFiveShipmentJsonData;
	}
	public void setTopFiveShipmentJsonData(List<TopFiveShipmentJsonData> topFiveShipmentJsonData) {
		this.topFiveShipmentJsonData = topFiveShipmentJsonData;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	

}
