/**
*
*@author Ahmad Naquib
*/
package com.myshipment.model;

public class ContainerTypeInformation {

	private String typeGroup;
	private String typeDescription;
	private String typeSize;
	
	public String getTypeGroup() {
		return typeGroup;
	}
	public void setTypeGroup(String typeGroup) {
		this.typeGroup = typeGroup;
	}
	public String getTypeDescription() {
		return typeDescription;
	}
	public void setTypeDescription(String typeDescription) {
		this.typeDescription = typeDescription;
	}
	public String getTypeSize() {
		return typeSize;
	}
	public void setTypeSize(String typeSize) {
		this.typeSize = typeSize;
	}
	
	
}
