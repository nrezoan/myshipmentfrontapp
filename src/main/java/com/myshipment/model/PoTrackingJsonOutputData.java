package com.myshipment.model;

import java.util.List;

/*
 * @Ranjeet Kumar
 */
public class PoTrackingJsonOutputData {

	private List<PoTrackingResultBean> poTrackingResultData;
	private List<PoTrackingDetailResultBean> poTrackingDetailResultData;
	

	public List<PoTrackingResultBean> getPoTrackingResultData() {
		return poTrackingResultData;
	}

	public void setPoTrackingResultData(
			List<PoTrackingResultBean> poTrackingResultData) {
		this.poTrackingResultData = poTrackingResultData;
	}

	public List<PoTrackingDetailResultBean> getPoTrackingDetailResultData() {
		return poTrackingDetailResultData;
	}

	public void setPoTrackingDetailResultData(List<PoTrackingDetailResultBean> poTrackingDetailResultData) {
		this.poTrackingDetailResultData = poTrackingDetailResultData;
	}
	
	
}
