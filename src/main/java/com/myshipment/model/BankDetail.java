package com.myshipment.model;



public class BankDetail {

	private String sno;	
    private String vkorg;
    private String vtweg;
    private String spart;
    private String bankName1;
    private String bankName2;
    private String bankName3;
    private String bankName4;
    private String bankCity1;
    private String portCode1;
    private String currency;
    private String country;
    private String acNumber;
    private String swiftid;
    private String ifscCode;
	public String getSno() {
		return sno;
	}
	public void setSno(String sno) {
		this.sno = sno;
	}
	public String getVkorg() {
		return vkorg;
	}
	public void setVkorg(String vkorg) {
		this.vkorg = vkorg;
	}
	public String getVtweg() {
		return vtweg;
	}
	public void setVtweg(String vtweg) {
		this.vtweg = vtweg;
	}
	public String getSpart() {
		return spart;
	}
	public void setSpart(String spart) {
		this.spart = spart;
	}
	public String getBankName1() {
		return bankName1;
	}
	public void setBankName1(String bankName1) {
		this.bankName1 = bankName1;
	}
	public String getBankName2() {
		return bankName2;
	}
	public void setBankName2(String bankName2) {
		this.bankName2 = bankName2;
	}
	public String getBankName3() {
		return bankName3;
	}
	public void setBankName3(String bankName3) {
		this.bankName3 = bankName3;
	}
	public String getBankName4() {
		return bankName4;
	}
	public void setBankName4(String bankName4) {
		this.bankName4 = bankName4;
	}
	public String getBankCity1() {
		return bankCity1;
	}
	public void setBankCity1(String bankCity1) {
		this.bankCity1 = bankCity1;
	}
	public String getPortCode1() {
		return portCode1;
	}
	public void setPortCode1(String portCode1) {
		this.portCode1 = portCode1;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getAcNumber() {
		return acNumber;
	}
	public void setAcNumber(String acNumber) {
		this.acNumber = acNumber;
	}
	public String getSwiftid() {
		return swiftid;
	}
	public void setSwiftid(String swiftid) {
		this.swiftid = swiftid;
	}
	public String getIfscCode() {
		return ifscCode;
	}
	public void setIfscCode(String ifscCode) {
		this.ifscCode = ifscCode;
	}
	
    
    
    
}
