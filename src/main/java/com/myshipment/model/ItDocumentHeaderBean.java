package com.myshipment.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/*
 * @Ranjeet Kumar
 */
public class ItDocumentHeaderBean implements Serializable{

	
	private static final long serialVersionUID = 1L;
	
	private String company_code;	
	private String sales_org;	
	private String dist_channel;	
	private String division;
	private String document_no;
	private String bl_no;	
	//private Date booking_date;
	private String booking_date;
	//private Date bl_date;	
	private String bl_date;	
	private String shipper_no;	
	private String shipper_name;	
	private String buyer_no;	
	private String buyer_name;	
	private String agent_no;	
	private String agent_name;	
	private String carrier_no;	
	private String carrier_name;	
	private String pol_code;	
	private String pol_name;	
	private String pod_code;	
	private String pod_name;	
	private String place_of_discharge;	
	private String discharge_port_name;	
	private String comm_invoice_no;	
	//private Date comm_invoice_date;	
	private String comm_invoice_date;
	private String lc_tt_po_no;	
	//private Date lc_tt_po_date;	
	private String lc_tt_po_date;
	private String freight_mode;	
	private Integer tot_qty;	
	private Integer tot_pcs;	
	private Double net_wt;	
	private Double gross_wt;	
	private Double tot_volume;	
	private Double charge_wt;	
	//private Date doc_rcv_date;	
	private String doc_rcv_date;
	//private Date bl_release_date;	
	private String bl_release_date;	
	private String courier_no;	
	//private Date courier_date;
	private String courier_date;
	//private Date noc_date;
	private String noc_date;
	//private Date gr_date;	
	private String gr_date;
	//private Date shipment_date;
	private String shipment_date;
	//private Date inv_date;	
	private String inv_date;
	//private Date rec_creation_date;
	private String rec_creation_date;
	//private Date rec_update_date;
	private String rec_update_date;
	private List<String> containers;
	private ItScheduleDetailBean itScheduleDetailBean;
	
	public String getCompany_code() {
		return company_code;
	}
	public void setCompany_code(String company_code) {
		this.company_code = company_code;
	}
	public String getSales_org() {
		return sales_org;
	}
	public void setSales_org(String sales_org) {
		this.sales_org = sales_org;
	}
	public String getDist_channel() {
		return dist_channel;
	}
	public void setDist_channel(String dist_channel) {
		this.dist_channel = dist_channel;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getDocument_no() {
		return document_no;
	}
	public void setDocument_no(String document_no) {
		this.document_no = document_no;
	}
	public String getBl_no() {
		return bl_no;
	}
	public void setBl_no(String bl_no) {
		this.bl_no = bl_no;
	}
/*	public Date getBooking_date() {
		return booking_date;
	}
	public void setBooking_date(Date booking_date) {
		this.booking_date = booking_date;
	}
	public Date getBl_date() {
		return bl_date;
	}
	public void setBl_date(Date bl_date) {
		this.bl_date = bl_date;
	}*/
	public String getShipper_no() {
		return shipper_no;
	}
	public void setShipper_no(String shipper_no) {
		this.shipper_no = shipper_no;
	}
	public String getShipper_name() {
		return shipper_name;
	}
	public void setShipper_name(String shipper_name) {
		this.shipper_name = shipper_name;
	}
	public String getBuyer_no() {
		return buyer_no;
	}
	public void setBuyer_no(String buyer_no) {
		this.buyer_no = buyer_no;
	}
	public String getBuyer_name() {
		return buyer_name;
	}
	public void setBuyer_name(String buyer_name) {
		this.buyer_name = buyer_name;
	}
	public String getAgent_no() {
		return agent_no;
	}
	public void setAgent_no(String agent_no) {
		this.agent_no = agent_no;
	}
	public String getAgent_name() {
		return agent_name;
	}
	public void setAgent_name(String agent_name) {
		this.agent_name = agent_name;
	}
	public String getCarrier_no() {
		return carrier_no;
	}
	public void setCarrier_no(String carrier_no) {
		this.carrier_no = carrier_no;
	}
	public String getCarrier_name() {
		return carrier_name;
	}
	public void setCarrier_name(String carrier_name) {
		this.carrier_name = carrier_name;
	}
	public String getPol_code() {
		return pol_code;
	}
	public void setPol_code(String pol_code) {
		this.pol_code = pol_code;
	}
	public String getPol_name() {
		return pol_name;
	}
	public void setPol_name(String pol_name) {
		this.pol_name = pol_name;
	}
	public String getPod_code() {
		return pod_code;
	}
	public void setPod_code(String pod_code) {
		this.pod_code = pod_code;
	}
	public String getPod_name() {
		return pod_name;
	}
	public void setPod_name(String pod_name) {
		this.pod_name = pod_name;
	}
	public String getPlace_of_discharge() {
		return place_of_discharge;
	}
	public void setPlace_of_discharge(String place_of_discharge) {
		this.place_of_discharge = place_of_discharge;
	}
	public String getDischarge_port_name() {
		return discharge_port_name;
	}
	public void setDischarge_port_name(String discharge_port_name) {
		this.discharge_port_name = discharge_port_name;
	}
	public String getComm_invoice_no() {
		return comm_invoice_no;
	}
	public void setComm_invoice_no(String comm_invoice_no) {
		this.comm_invoice_no = comm_invoice_no;
	}
/*	public Date getComm_invoice_date() {
		return comm_invoice_date;
	}
	public void setComm_invoice_date(Date comm_invoice_date) {
		this.comm_invoice_date = comm_invoice_date;
	}*/
	public String getLc_tt_po_no() {
		return lc_tt_po_no;
	}
	public void setLc_tt_po_no(String lc_tt_po_no) {
		this.lc_tt_po_no = lc_tt_po_no;
	}
/*	public Date getLc_tt_po_date() {
		return lc_tt_po_date;
	}
	public void setLc_tt_po_date(Date lc_tt_po_date) {
		this.lc_tt_po_date = lc_tt_po_date;
	}*/
	public String getFreight_mode() {
		return freight_mode;
	}
	public void setFreight_mode(String freight_mode) {
		this.freight_mode = freight_mode;
	}
	public Integer getTot_qty() {
		return tot_qty;
	}
	public void setTot_qty(Integer tot_qty) {
		this.tot_qty = tot_qty;
	}
	public Integer getTot_pcs() {
		return tot_pcs;
	}
	public void setTot_pcs(Integer tot_pcs) {
		this.tot_pcs = tot_pcs;
	}
	public Double getNet_wt() {
		return net_wt;
	}
	public void setNet_wt(Double net_wt) {
		this.net_wt = net_wt;
	}
	public Double getGross_wt() {
		return gross_wt;
	}
	public void setGross_wt(Double gross_wt) {
		this.gross_wt = gross_wt;
	}
	public Double getTot_volume() {
		return tot_volume;
	}
	public void setTot_volume(Double tot_volume) {
		this.tot_volume = tot_volume;
	}
	public Double getCharge_wt() {
		return charge_wt;
	}
	public void setCharge_wt(Double charge_wt) {
		this.charge_wt = charge_wt;
	}
/*	public Date getDoc_rcv_date() {
		return doc_rcv_date;
	}
	public void setDoc_rcv_date(Date doc_rcv_date) {
		this.doc_rcv_date = doc_rcv_date;
	}
	public Date getBl_release_date() {
		return bl_release_date;
	}
	public void setBl_release_date(Date bl_release_date) {
		this.bl_release_date = bl_release_date;
	}*/
	public String getCourier_no() {
		return courier_no;
	}
	public void setCourier_no(String courier_no) {
		this.courier_no = courier_no;
	}
/*	public Date getCourier_date() {
		return courier_date;
	}
	public void setCourier_date(Date courier_date) {
		this.courier_date = courier_date;
	}
	public Date getNoc_date() {
		return noc_date;
	}
	public void setNoc_date(Date noc_date) {
		this.noc_date = noc_date;
	}
	public Date getGr_date() {
		return gr_date;
	}
	public void setGr_date(Date gr_date) {
		this.gr_date = gr_date;
	}
	public Date getShipment_date() {
		return shipment_date;
	}
	public void setShipment_date(Date shipment_date) {
		this.shipment_date = shipment_date;
	}
	public Date getInv_date() {
		return inv_date;
	}
	public void setInv_date(Date inv_date) {
		this.inv_date = inv_date;
	}
	public Date getRec_creation_date() {
		return rec_creation_date;
	}
	public void setRec_creation_date(Date rec_creation_date) {
		this.rec_creation_date = rec_creation_date;
	}
	public Date getRec_update_date() {
		return rec_update_date;
	}
	public void setRec_update_date(Date rec_update_date) {
		this.rec_update_date = rec_update_date;
	}
*/
	public String getBooking_date() {
		return booking_date;
	}
	public void setBooking_date(String booking_date) {
		this.booking_date = booking_date;
	}
	public String getBl_date() {
		return bl_date;
	}
	public void setBl_date(String bl_date) {
		this.bl_date = bl_date;
	}
	public String getComm_invoice_date() {
		return comm_invoice_date;
	}
	public void setComm_invoice_date(String comm_invoice_date) {
		this.comm_invoice_date = comm_invoice_date;
	}
	public String getLc_tt_po_date() {
		return lc_tt_po_date;
	}
	public void setLc_tt_po_date(String lc_tt_po_date) {
		this.lc_tt_po_date = lc_tt_po_date;
	}
	public String getDoc_rcv_date() {
		return doc_rcv_date;
	}
	public void setDoc_rcv_date(String doc_rcv_date) {
		this.doc_rcv_date = doc_rcv_date;
	}
	public String getBl_release_date() {
		return bl_release_date;
	}
	public void setBl_release_date(String bl_release_date) {
		this.bl_release_date = bl_release_date;
	}
	public String getCourier_date() {
		return courier_date;
	}
	public void setCourier_date(String courier_date) {
		this.courier_date = courier_date;
	}
	public String getNoc_date() {
		return noc_date;
	}
	public void setNoc_date(String noc_date) {
		this.noc_date = noc_date;
	}
	public String getGr_date() {
		return gr_date;
	}
	public void setGr_date(String gr_date) {
		this.gr_date = gr_date;
	}
	public String getShipment_date() {
		return shipment_date;
	}
	public void setShipment_date(String shipment_date) {
		this.shipment_date = shipment_date;
	}
	public String getInv_date() {
		return inv_date;
	}
	public void setInv_date(String inv_date) {
		this.inv_date = inv_date;
	}
	public String getRec_creation_date() {
		return rec_creation_date;
	}
	public void setRec_creation_date(String rec_creation_date) {
		this.rec_creation_date = rec_creation_date;
	}
	public String getRec_update_date() {
		return rec_update_date;
	}
	public void setRec_update_date(String rec_update_date) {
		this.rec_update_date = rec_update_date;
	}
	public List<String> getContainers() {
		return containers;
	}
	public void setContainers(List<String> containers) {
		this.containers = containers;
	}
	public ItScheduleDetailBean getItScheduleDetailBean() {
		return itScheduleDetailBean;
	}
	public void setItScheduleDetailBean(ItScheduleDetailBean itScheduleDetailBean) {
		this.itScheduleDetailBean = itScheduleDetailBean;
	}
	
	
}
