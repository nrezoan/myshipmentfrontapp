/**
*
*@author Ahmad Naquib
*/
package com.myshipment.model;

public class IntelligentCompanyPercentageUsage {

	private String companyName;
	private String companyCode;
	private String division;
	private String distributionChannel;
	private String totalHit;
	private double totalHitPercentage;
	
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public String getDistributionChannel() {
		return distributionChannel;
	}
	public void setDistributionChannel(String distributionChannel) {
		this.distributionChannel = distributionChannel;
	}
	public String getTotalHit() {
		return totalHit;
	}
	public void setTotalHit(String totalHit) {
		this.totalHit = totalHit;
	}
	public double getTotalHitPercentage() {
		return totalHitPercentage;
	}
	public void setTotalHitPercentage(double totalHitPercentage) {
		this.totalHitPercentage = totalHitPercentage;
	}
	
}
