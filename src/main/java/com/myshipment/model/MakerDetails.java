package com.myshipment.model;

/**
 * @author virendra
 *
 */
public class MakerDetails {
	private static final long serialVersionUID=32431241324L;
	private String makerName;
	private String makerAddress;
	private String makerCountry;
	private String makerCity;
	
	public String getMakerCity() {
		return makerCity;
	}
	public void setMakerCity(String makerCity) {
		this.makerCity = makerCity;
	}
	public String getMakerName() {
		return makerName;
	}
	public void setMakerName(String makerName) {
		this.makerName = makerName;
	}
	public String getMakerAddress() {
		return makerAddress;
	}
	public void setMakerAddress(String makerAddress) {
		this.makerAddress = makerAddress;
	}
	public String getMakerCountry() {
		return makerCountry;
	}
	public void setMakerCountry(String makerCountry) {
		this.makerCountry = makerCountry;
	}
	

}
