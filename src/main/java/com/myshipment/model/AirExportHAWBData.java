package com.myshipment.model;

import java.util.List;

public class AirExportHAWBData {

	private List<MyshipmentTrackHeaderBean> myshipmentTrackHeader;
	private List<MyshipmentTrackItemBean> myshipmentTrackItem;
	private List<MyshipmentTrackScheduleBean> myshipmentTrackSchedule;
	private List<OAG> flightDetails;
	private OAG inAirFlightDetails;

	public List<MyshipmentTrackHeaderBean> getMyshipmentTrackHeader() {
		return myshipmentTrackHeader;
	}

	public void setMyshipmentTrackHeader(List<MyshipmentTrackHeaderBean> myshipmentTrackHeader) {
		this.myshipmentTrackHeader = myshipmentTrackHeader;
	}

	public List<MyshipmentTrackItemBean> getMyshipmentTrackItem() {
		return myshipmentTrackItem;
	}

	public void setMyshipmentTrackItem(List<MyshipmentTrackItemBean> myshipmentTrackItem) {
		this.myshipmentTrackItem = myshipmentTrackItem;
	}

	public List<MyshipmentTrackScheduleBean> getMyshipmentTrackSchedule() {
		return myshipmentTrackSchedule;
	}

	public void setMyshipmentTrackSchedule(List<MyshipmentTrackScheduleBean> myshipmentTrackSchedule) {
		this.myshipmentTrackSchedule = myshipmentTrackSchedule;
	}

	public List<OAG> getFlightDetails() {
		return flightDetails;
	}

	public void setFlightDetails(List<OAG> flightDetails) {
		this.flightDetails = flightDetails;
	}

	public OAG getInAirFlightDetails() {
		return inAirFlightDetails;
	}

	public void setInAirFlightDetails(OAG inAirFlightDetails) {
		this.inAirFlightDetails = inAirFlightDetails;
	}

}
