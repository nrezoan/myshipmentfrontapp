package com.myshipment.model;

import java.util.List;

public class SupplierWiseShipmentDetailJson {
 private List<SupplierWiseShipmentJson> lstSupplierWiseShipmentJson;
 private String message;
public List<SupplierWiseShipmentJson> getLstSupplierWiseShipmentJson() {
	return lstSupplierWiseShipmentJson;
}
public void setLstSupplierWiseShipmentJson(List<SupplierWiseShipmentJson> lstSupplierWiseShipmentJson) {
	this.lstSupplierWiseShipmentJson = lstSupplierWiseShipmentJson;
}
public String getMessage() {
	return message;
}
public void setMessage(String message) {
	this.message = message;
}
}
