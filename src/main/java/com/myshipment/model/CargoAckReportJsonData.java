package com.myshipment.model;

import java.util.Date;
import java.util.List;



public class CargoAckReportJsonData {

	private BapiRet BapiRet;	
	private Date postDate;		
	private List<CargoItem> itItemList;
	private List<CargoItemHeader> itHeaderList;
	private List<ShippingReport> itAddList;
	
	public BapiRet getBapiRet() {
		return BapiRet;
	}
	public void setBapiRet(BapiRet bapiRet) {
		BapiRet = bapiRet;
	}
	public Date getPostDate() {
		return postDate;
	}
	public void setPostDate(Date postDate) {
		this.postDate = postDate;
	}
	public List<CargoItem> getItItemList() {
		return itItemList;
	}
	public void setItItemList(List<CargoItem> itItemList) {
		this.itItemList = itItemList;
	}
	public List<CargoItemHeader> getItHeaderList() {
		return itHeaderList;
	}
	public void setItHeaderList(List<CargoItemHeader> itHeaderList) {
		this.itHeaderList = itHeaderList;
	}
	public List<ShippingReport> getItAddList() {
		return itAddList;
	}
	public void setItAddList(List<ShippingReport> itAddList) {
		this.itAddList = itAddList;
	}
			
			
}
