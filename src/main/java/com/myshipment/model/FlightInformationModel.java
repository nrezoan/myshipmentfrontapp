package com.myshipment.model;
import org.joda.time.LocalDate;

public class FlightInformationModel {
	private String dep;
	private String arr;
	private String depCode;
	private String arrCode;
	private String depLat;
	private String depLong;
	private String arrLong;
	private String arrLat;
	private String depCity;
	private String arrCity;
	private String flightNo;
	private String flightLat;
	private String flightLong;
	private String depAirport;
	private String arrAirport;
	private LocalDate arrRTDate;
	private String arrRTTime;
	private LocalDate depRTDate;
	private String depRTTime;
	private LocalDate arrGTDate;
	private String arrGTTime;
	private LocalDate depGTDate;
	private String depGTTime;
	private String airlines;
	private String flightStatus;
	private String remainingTime;
	private String aircraft;
	private String tailNumber;
	private String updateTime;

	public String getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}

	public String getDep() {
		return dep;
	}

	public void setDep(String dep) {
		this.dep = dep;
	}

	public String getArr() {
		return arr;
	}

	public void setArr(String arr) {
		this.arr = arr;
	}

	public String getDepCode() {
		return depCode;
	}

	public void setDepCode(String depCode) {
		this.depCode = depCode;
	}

	public String getArrCode() {
		return arrCode;
	}

	public void setArrCode(String arrCode) {
		this.arrCode = arrCode;
	}

	public String getDepLat() {
		return depLat;
	}

	public void setDepLat(String depLat) {
		this.depLat = depLat;
	}

	public String getDepLong() {
		return depLong;
	}

	public void setDepLong(String depLong) {
		this.depLong = depLong;
	}

	public String getArrLong() {
		return arrLong;
	}

	public void setArrLong(String arrLong) {
		this.arrLong = arrLong;
	}

	public String getArrLat() {
		return arrLat;
	}

	public void setArrLat(String arrLat) {
		this.arrLat = arrLat;
	}

	public String getDepCity() {
		return depCity;
	}

	public void setDepCity(String depCity) {
		this.depCity = depCity;
	}

	public String getArrCity() {
		return arrCity;
	}

	public void setArrCity(String arrCity) {
		this.arrCity = arrCity;
	}

	public String getFlightNo() {
		return flightNo;
	}

	public void setFlightNo(String flightNo) {
		this.flightNo = flightNo;
	}

	public String getFlightLat() {
		return flightLat;
	}

	public void setFlightLat(String flightLat) {
		this.flightLat = flightLat;
	}

	public String getFlightLong() {
		return flightLong;
	}

	public void setFlightLong(String flightLong) {
		this.flightLong = flightLong;
	}

	public String getDepAirport() {
		return depAirport;
	}

	public void setDepAirport(String depAirport) {
		this.depAirport = depAirport;
	}

	public String getArrAirport() {
		return arrAirport;
	}

	public void setArrAirport(String arrAirport) {
		this.arrAirport = arrAirport;
	}

	public LocalDate getArrRTDate() {
		return arrRTDate;
	}

	public void setArrRTDate(LocalDate arrRTDate) {
		this.arrRTDate = arrRTDate;
	}

	public String getArrRTTime() {
		return arrRTTime;
	}

	public void setArrRTTime(String arrRTTime) {
		this.arrRTTime = arrRTTime;
	}

	public LocalDate getDepRTDate() {
		return depRTDate;
	}

	public void setDepRTDate(LocalDate depRTDate) {
		this.depRTDate = depRTDate;
	}

	public String getDepRTTime() {
		return depRTTime;
	}

	public void setDepRTTime(String depRTTime) {
		this.depRTTime = depRTTime;
	}

	public LocalDate getArrGTDate() {
		return arrGTDate;
	}

	public void setArrGTDate(LocalDate arrGTDate) {
		this.arrGTDate = arrGTDate;
	}

	public String getArrGTTime() {
		return arrGTTime;
	}

	public void setArrGTTime(String arrGTTime) {
		this.arrGTTime = arrGTTime;
	}

	public LocalDate getDepGTDate() {
		return depGTDate;
	}

	public void setDepGTDate(LocalDate depGTDate) {
		this.depGTDate = depGTDate;
	}

	public String getDepGTTime() {
		return depGTTime;
	}

	public void setDepGTTime(String depGTTime) {
		this.depGTTime = depGTTime;
	}

	public String getAirlines() {
		return airlines;
	}

	public void setAirlines(String airlines) {
		this.airlines = airlines;
	}

	public String getFlightStatus() {
		return flightStatus;
	}

	public void setFlightStatus(String flightStatus) {
		this.flightStatus = flightStatus;
	}

	public String getRemainingTime() {
		return remainingTime;
	}

	public void setRemainingTime(String remainingTime) {
		this.remainingTime = remainingTime;
	}

	public String getAircraft() {
		return aircraft;
	}

	public void setAircraft(String aircraft) {
		this.aircraft = aircraft;
	}

	public String getTailNumber() {
		return tailNumber;
	}

	public void setTailNumber(String tailNumber) {
		this.tailNumber = tailNumber;
	}

}
