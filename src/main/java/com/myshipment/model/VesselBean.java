package com.myshipment.model;


/*
 * @Mohammad Salahuddin
 */
public class VesselBean {

	private int imo;
	private int mmsi;
	private String vessel_name;
	private String country_flag;
	private String vessel_type;
	public int getImo() {
		return imo;
	}
	public void setImo(int imo) {
		this.imo = imo;
	}
	public int getMmsi() {
		return mmsi;
	}
	public void setMmsi(int mmsi) {
		this.mmsi = mmsi;
	}
	public String getVessel_name() {
		return vessel_name;
	}
	public void setVessel_name(String vessel_name) {
		this.vessel_name = vessel_name;
	}
	public String getCountry_flag() {
		return country_flag;
	}
	public void setCountry_flag(String country_flag) {
		this.country_flag = country_flag;
	}
	public String getVessel_type() {
		return vessel_type;
	}
	public void setVessel_type(String vessel_type) {
		this.vessel_type = vessel_type;
	}
	
	
		
	
}
