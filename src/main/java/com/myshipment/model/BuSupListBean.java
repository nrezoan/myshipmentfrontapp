package com.myshipment.model;
/*
 * @ Gufranur Rahman
 */


public class BuSupListBean {
	
	private String zzCust;
	private String zzCustName;
	private String zzParvw;
	private String zzCustAddress;
	public String getZzCust() {
		return zzCust;
	}
	public void setZzCust(String zzCust) {
		this.zzCust = zzCust;
	}
	public String getZzCustName() {
		return zzCustName;
	}
	public void setZzCustName(String zzCustName) {
		this.zzCustName = zzCustName;
	}
	public String getZzParvw() {
		return zzParvw;
	}
	public void setZzParvw(String zzParvw) {
		this.zzParvw = zzParvw;
	}
	public String getZzCustAddress() {
		return zzCustAddress;
	}
	public void setZzCustAddress(String zzCustAddress) {
		this.zzCustAddress = zzCustAddress;
	}

	
	
	
}
