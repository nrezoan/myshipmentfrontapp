package com.myshipment.model;

public class PackListSize {
	
	private long sizeId;
	private String name;
	private long noOfPcs;
	private long colorId;
	public long getSizeId() {
		return sizeId;
	}
	public void setSizeId(long sizeId) {
		this.sizeId = sizeId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public long getNoOfPcs() {
		return noOfPcs;
	}
	public void setNoOfPcs(long noOfPcs) {
		this.noOfPcs = noOfPcs;
	}
	public long getColorId() {
		return colorId;
	}
	public void setColorId(long colorId) {
		this.colorId = colorId;
	}
	

}
