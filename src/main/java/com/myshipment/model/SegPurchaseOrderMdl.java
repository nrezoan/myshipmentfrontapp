package com.myshipment.model;

public class SegPurchaseOrderMdl {
	private static final Long serialVersionUID=23432456l;
	private Long seg_id;
	private Long po_id;
	private String vc_po_no = "";
	private String nu_client_code = "";
	private String vc_product_no = "";
	private String vc_sku_no = "";
	private String vc_style_no = "";
	private String vc_article_no = "";
	private String vc_color = "";
	private String vc_size = "";
	private String vc_pol = "";
	private String vc_pod = "";
	private double nu_no_pcs_ctns;
	private String vc_commodity = "";	
	private String dt_etd = "";
	private double vc_tot_pcs;
	private double vc_quan;
	private String vc_qua_uom = "";
	private double nu_length;
	private double nu_width;
	private double nu_hieght;
	private String vc_in_hcm = "";
	private String vc_cbm_sea = "";
	private String vc_volume = "";
	private String vc_gw_car = "";
	private String vc_gr_wt = "";
	private String vc_nw_car = "";
	private String vc_nt_wt = "";
	private String vc_ref_field1 = "";
	private String vc_ref_field2 = "";
	private String vc_ref_field3 = "";
	private String vc_ref_field4 = "";
	private String vc_ref_field5 = "";
	private String vc_hs_code = "";
	private String vc_qc_dt = "";
	private String vc_rel_dt = "";
	private String sales_org = "";
	private String sap_quotation = "";
	private String vc_buyer = "";
	private String vc_buy_house = "";
	private String vc_division = "";
	private String file_upload_date = "";
	private String file_name = "";
	private String carrier_id="";
	private String carrier_schedule="";
	private PurchaseOrderMdl purchaseOrder;
	public Long getSeg_id() {
		return seg_id;
	}
	public void setSeg_id(Long seg_id) {
		this.seg_id = seg_id;
	}
	public PurchaseOrderMdl getPurchaseOrder() {
		return purchaseOrder;
	}
	public void setPurchaseOrder(PurchaseOrderMdl purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}
	public Long getPo_id() {
		return po_id;
	}
	public void setPo_id(Long po_id) {
		this.po_id = po_id;
	}
	public String getVc_po_no() {
		return vc_po_no;
	}
	public void setVc_po_no(String vc_po_no) {
		this.vc_po_no = vc_po_no;
	}
	public String getNu_client_code() {
		return nu_client_code;
	}
	public void setNu_client_code(String nu_client_code) {
		this.nu_client_code = nu_client_code;
	}
	public String getVc_product_no() {
		return vc_product_no;
	}
	public void setVc_product_no(String vc_product_no) {
		this.vc_product_no = vc_product_no;
	}
	public String getVc_sku_no() {
		return vc_sku_no;
	}
	public void setVc_sku_no(String vc_sku_no) {
		this.vc_sku_no = vc_sku_no;
	}
	public String getVc_style_no() {
		return vc_style_no;
	}
	public void setVc_style_no(String vc_style_no) {
		this.vc_style_no = vc_style_no;
	}
	public String getVc_article_no() {
		return vc_article_no;
	}
	public void setVc_article_no(String vc_article_no) {
		this.vc_article_no = vc_article_no;
	}
	public String getVc_color() {
		return vc_color;
	}
	public void setVc_color(String vc_color) {
		this.vc_color = vc_color;
	}
	public String getVc_size() {
		return vc_size;
	}
	public void setVc_size(String vc_size) {
		this.vc_size = vc_size;
	}
	public String getVc_pol() {
		return vc_pol;
	}
	public void setVc_pol(String vc_pol) {
		this.vc_pol = vc_pol;
	}
	public String getVc_pod() {
		return vc_pod;
	}
	public void setVc_pod(String vc_pod) {
		this.vc_pod = vc_pod;
	}
	public double getNu_no_pcs_ctns() {
		return nu_no_pcs_ctns;
	}
	public void setNu_no_pcs_ctns(double nu_no_pcs_ctns) {
		this.nu_no_pcs_ctns = nu_no_pcs_ctns;
	}
	public String getVc_commodity() {
		return vc_commodity;
	}
	public void setVc_commodity(String vc_commodity) {
		this.vc_commodity = vc_commodity;
	}
	public String getDt_etd() {
		return dt_etd;
	}
	public void setDt_etd(String dt_etd) {
		this.dt_etd = dt_etd;
	}
	public double getVc_tot_pcs() {
		return vc_tot_pcs;
	}
	public void setVc_tot_pcs(double vc_tot_pcs) {
		this.vc_tot_pcs = vc_tot_pcs;
	}
	public double getVc_quan() {
		return vc_quan;
	}
	public void setVc_quan(double vc_quan) {
		this.vc_quan = vc_quan;
	}
	public String getVc_qua_uom() {
		return vc_qua_uom;
	}
	public void setVc_qua_uom(String vc_qua_uom) {
		this.vc_qua_uom = vc_qua_uom;
	}
	public double getNu_length() {
		return nu_length;
	}
	public void setNu_length(double nu_length) {
		this.nu_length = nu_length;
	}
	public double getNu_width() {
		return nu_width;
	}
	public void setNu_width(double nu_width) {
		this.nu_width = nu_width;
	}
	public double getNu_hieght() {
		return nu_hieght;
	}
	public void setNu_hieght(double nu_height) {
		this.nu_hieght = nu_height;
	}
	public String getVc_in_hcm() {
		return vc_in_hcm;
	}
	public void setVc_in_hcm(String vc_in_hcm) {
		this.vc_in_hcm = vc_in_hcm;
	}
	public String getVc_cbm_sea() {
		return vc_cbm_sea;
	}
	public void setVc_cbm_sea(String vc_cbm_sea) {
		this.vc_cbm_sea = vc_cbm_sea;
	}
	public String getVc_volume() {
		return vc_volume;
	}
	public void setVc_volume(String vc_volume) {
		this.vc_volume = vc_volume;
	}
	public String getVc_gw_car() {
		return vc_gw_car;
	}
	public void setVc_gw_car(String vc_gw_car) {
		this.vc_gw_car = vc_gw_car;
	}
	public String getVc_gr_wt() {
		return vc_gr_wt;
	}
	public void setVc_gr_wt(String vc_gr_wt) {
		this.vc_gr_wt = vc_gr_wt;
	}
	public String getVc_nw_car() {
		return vc_nw_car;
	}
	public void setVc_nw_car(String vc_nw_car) {
		this.vc_nw_car = vc_nw_car;
	}
	public String getVc_nt_wt() {
		return vc_nt_wt;
	}
	public void setVc_nt_wt(String vc_nt_wt) {
		this.vc_nt_wt = vc_nt_wt;
	}
	public String getVc_ref_field1() {
		return vc_ref_field1;
	}
	public void setVc_ref_field1(String vc_ref_field1) {
		this.vc_ref_field1 = vc_ref_field1;
	}
	public String getVc_ref_field2() {
		return vc_ref_field2;
	}
	public void setVc_ref_field2(String vc_ref_field2) {
		this.vc_ref_field2 = vc_ref_field2;
	}
	public String getVc_ref_field3() {
		return vc_ref_field3;
	}
	public void setVc_ref_field3(String vc_ref_field3) {
		this.vc_ref_field3 = vc_ref_field3;
	}
	public String getVc_ref_field4() {
		return vc_ref_field4;
	}
	public void setVc_ref_field4(String vc_ref_field4) {
		this.vc_ref_field4 = vc_ref_field4;
	}
	public String getVc_ref_field5() {
		return vc_ref_field5;
	}
	public void setVc_ref_field5(String vc_ref_field5) {
		this.vc_ref_field5 = vc_ref_field5;
	}
	public String getVc_hs_code() {
		return vc_hs_code;
	}
	public void setVc_hs_code(String vc_hs_code) {
		this.vc_hs_code = vc_hs_code;
	}
	public String getVc_qc_dt() {
		return vc_qc_dt;
	}
	public void setVc_qc_dt(String vc_qc_dt) {
		this.vc_qc_dt = vc_qc_dt;
	}
	public String getVc_rel_dt() {
		return vc_rel_dt;
	}
	public void setVc_rel_dt(String vc_rel_dt) {
		this.vc_rel_dt = vc_rel_dt;
	}
	
	public String getSales_org() {
		return sales_org;
	}
	public void setSales_org(String sales_org) {
		this.sales_org = sales_org;
	}
	public String getSap_quotation() {
		return sap_quotation;
	}
	public void setSap_quotation(String sap_quotation) {
		this.sap_quotation = sap_quotation;
	}
	public String getVc_buyer() {
		return vc_buyer;
	}
	public void setVc_buyer(String vc_buyer) {
		this.vc_buyer = vc_buyer;
	}
	public String getVc_buy_house() {
		return vc_buy_house;
	}
	public void setVc_buy_house(String vc_buy_house) {
		this.vc_buy_house = vc_buy_house;
	}
	public String getVc_division() {
		return vc_division;
	}
	public void setVc_division(String vc_division) {
		this.vc_division = vc_division;
	}
	public String getFile_upload_date() {
		return file_upload_date;
	}
	public void setFile_upload_date(String file_upload_date) {
		this.file_upload_date = file_upload_date;
	}
	public String getFile_name() {
		return file_name;
	}
	public void setFile_name(String file_name) {
		this.file_name = file_name;
	}
	public String getCarrier_id() {
		return carrier_id;
	}
	public void setCarrier_id(String carrier_id) {
		this.carrier_id = carrier_id;
	}
	public String getCarrier_schedule() {
		return carrier_schedule;
	}
	public void setCarrier_schedule(String carrier_schedule) {
		this.carrier_schedule = carrier_schedule;
	}
	
}
