package com.myshipment.model;

public class ShipDetailsBuyerParams {
	private String shipperNo;
	private DashboardBuyerParams dashboardBuyerParams;
	public String getShipperNo() {
		return shipperNo;
	}
	public void setShipperNo(String shipperNo) {
		this.shipperNo = shipperNo;
	}
	public DashboardBuyerParams getDashboardBuyerParams() {
		return dashboardBuyerParams;
	}
	public void setDashboardBuyerParams(DashboardBuyerParams dashboardBuyerParams) {
		this.dashboardBuyerParams = dashboardBuyerParams;
	}
	

}
