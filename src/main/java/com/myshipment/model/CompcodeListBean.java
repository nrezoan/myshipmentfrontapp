package com.myshipment.model;
/*
 * @ Ranjeet Kumar
 */
public class CompcodeListBean {

	private String compCode;
	private String compName;
	
	public String getCompCode() {
		return compCode;
	}
	public void setCompCode(String compCode) {
		this.compCode = compCode;
	}
	public String getCompName() {
		return compName;
	}
	public void setCompName(String compName) {
		this.compName = compName;
	}
	
	
}
