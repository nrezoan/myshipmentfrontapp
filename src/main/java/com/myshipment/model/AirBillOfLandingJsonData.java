package com.myshipment.model;

import java.util.List;
/*
 * @Ranjeet Kumar
 */


public class AirBillOfLandingJsonData {

	private BapiRet1 bapiReturn1;
	private List<NegoBillLanding> itHeader; 
	private List<NegoBillLanding> itCountryOfOrig; 
	private List<NegoBillLanding> itCompCountry; 
	private List<NegoBillLanding> itShipper; 
	private List<NegoBillLanding> itShipperCountry; 
	private List<NegoBillLanding> itShipperBank; 
	
	private List<NegoBillLanding> itShbCountry; 
	private List<NegoBillLanding> itNotify; 
	private List<NegoBillLanding> itNotifyCountry; 
	private List<NegoBillLanding> itDelevery; 
	private List<NegoBillLanding> itDeleveryCountry; 
	private List<NegoBillLanding> itConsignee; 
	
	private List<NegoBillLanding> itConsigneeCountry; 
	private List<NegoBillLanding> itConsigneeBank; 
	private List<NegoBillLanding> itCbCountry; 
	private List<NegoBillLanding> itPor; 
	private List<NegoBillLanding> itPod; 
	private List<NegoBillLanding> itPol; 
	
	private List<NegoBillLanding> itPolDel; 
	private List<NegoBillLanding> itTvBur; 
	private List<NegoBillLanding> itPlaceOfIssue; 
	private List<NegoBillLanding> itLPlaceOfIssue; 
	private List<NegoBillLanding> itTsp; 
	
	private List<NegoBillLandingI> itVbpa; 	
	private List<NegoBillLandingI> itLips;
	private List<NegoBillLandingI> itVbap; 	
	private List<NegoBillLandingI> itVepo;
	private List<NegoBillLandingI> itItems;
	
	private List<ShippingReport> itAddress;
	
	private String totalAmount;	
	private String totalCbm;
	private String totalQuantity;
	
	public BapiRet1 getBapiReturn1() {
		return bapiReturn1;
	}
	public void setBapiReturn1(BapiRet1 bapiReturn1) {
		this.bapiReturn1 = bapiReturn1;
	}
	public List<NegoBillLanding> getItHeader() {
		return itHeader;
	}
	public void setItHeader(List<NegoBillLanding> itHeader) {
		this.itHeader = itHeader;
	}
	public List<NegoBillLanding> getItCountryOfOrig() {
		return itCountryOfOrig;
	}
	public void setItCountryOfOrig(List<NegoBillLanding> itCountryOfOrig) {
		this.itCountryOfOrig = itCountryOfOrig;
	}
	public List<NegoBillLanding> getItCompCountry() {
		return itCompCountry;
	}
	public void setItCompCountry(List<NegoBillLanding> itCompCountry) {
		this.itCompCountry = itCompCountry;
	}
	public List<NegoBillLanding> getItShipper() {
		return itShipper;
	}
	public void setItShipper(List<NegoBillLanding> itShipper) {
		this.itShipper = itShipper;
	}
	public List<NegoBillLanding> getItShipperCountry() {
		return itShipperCountry;
	}
	public void setItShipperCountry(List<NegoBillLanding> itShipperCountry) {
		this.itShipperCountry = itShipperCountry;
	}
	public List<NegoBillLanding> getItShipperBank() {
		return itShipperBank;
	}
	public void setItShipperBank(List<NegoBillLanding> itShipperBank) {
		this.itShipperBank = itShipperBank;
	}
	public List<NegoBillLanding> getItShbCountry() {
		return itShbCountry;
	}
	public void setItShbCountry(List<NegoBillLanding> itShbCountry) {
		this.itShbCountry = itShbCountry;
	}
	public List<NegoBillLanding> getItNotify() {
		return itNotify;
	}
	public void setItNotify(List<NegoBillLanding> itNotify) {
		this.itNotify = itNotify;
	}
	public List<NegoBillLanding> getItNotifyCountry() {
		return itNotifyCountry;
	}
	public void setItNotifyCountry(List<NegoBillLanding> itNotifyCountry) {
		this.itNotifyCountry = itNotifyCountry;
	}
	public List<NegoBillLanding> getItDelevery() {
		return itDelevery;
	}
	public void setItDelevery(List<NegoBillLanding> itDelevery) {
		this.itDelevery = itDelevery;
	}
	public List<NegoBillLanding> getItDeleveryCountry() {
		return itDeleveryCountry;
	}
	public void setItDeleveryCountry(List<NegoBillLanding> itDeleveryCountry) {
		this.itDeleveryCountry = itDeleveryCountry;
	}
	public List<NegoBillLanding> getItConsignee() {
		return itConsignee;
	}
	public void setItConsignee(List<NegoBillLanding> itConsignee) {
		this.itConsignee = itConsignee;
	}
	public List<NegoBillLanding> getItConsigneeCountry() {
		return itConsigneeCountry;
	}
	public void setItConsigneeCountry(List<NegoBillLanding> itConsigneeCountry) {
		this.itConsigneeCountry = itConsigneeCountry;
	}
	public List<NegoBillLanding> getItConsigneeBank() {
		return itConsigneeBank;
	}
	public void setItConsigneeBank(List<NegoBillLanding> itConsigneeBank) {
		this.itConsigneeBank = itConsigneeBank;
	}
	public List<NegoBillLanding> getItCbCountry() {
		return itCbCountry;
	}
	public void setItCbCountry(List<NegoBillLanding> itCbCountry) {
		this.itCbCountry = itCbCountry;
	}
	public List<NegoBillLanding> getItPor() {
		return itPor;
	}
	public void setItPor(List<NegoBillLanding> itPor) {
		this.itPor = itPor;
	}
	public List<NegoBillLanding> getItPod() {
		return itPod;
	}
	public void setItPod(List<NegoBillLanding> itPod) {
		this.itPod = itPod;
	}
	public List<NegoBillLanding> getItPol() {
		return itPol;
	}
	public void setItPol(List<NegoBillLanding> itPol) {
		this.itPol = itPol;
	}
	public List<NegoBillLanding> getItPolDel() {
		return itPolDel;
	}
	public void setItPolDel(List<NegoBillLanding> itPolDel) {
		this.itPolDel = itPolDel;
	}
	public List<NegoBillLanding> getItTvBur() {
		return itTvBur;
	}
	public void setItTvBur(List<NegoBillLanding> itTvBur) {
		this.itTvBur = itTvBur;
	}
	public List<NegoBillLanding> getItPlaceOfIssue() {
		return itPlaceOfIssue;
	}
	public void setItPlaceOfIssue(List<NegoBillLanding> itPlaceOfIssue) {
		this.itPlaceOfIssue = itPlaceOfIssue;
	}
	public List<NegoBillLanding> getItLPlaceOfIssue() {
		return itLPlaceOfIssue;
	}
	public void setItLPlaceOfIssue(List<NegoBillLanding> itLPlaceOfIssue) {
		this.itLPlaceOfIssue = itLPlaceOfIssue;
	}
	public List<NegoBillLanding> getItTsp() {
		return itTsp;
	}
	public void setItTsp(List<NegoBillLanding> itTsp) {
		this.itTsp = itTsp;
	}
	public List<NegoBillLandingI> getItVbpa() {
		return itVbpa;
	}
	public void setItVbpa(List<NegoBillLandingI> itVbpa) {
		this.itVbpa = itVbpa;
	}
	public List<NegoBillLandingI> getItLips() {
		return itLips;
	}
	public void setItLips(List<NegoBillLandingI> itLips) {
		this.itLips = itLips;
	}
	public List<NegoBillLandingI> getItVbap() {
		return itVbap;
	}
	public void setItVbap(List<NegoBillLandingI> itVbap) {
		this.itVbap = itVbap;
	}
	public List<NegoBillLandingI> getItVepo() {
		return itVepo;
	}
	public void setItVepo(List<NegoBillLandingI> itVepo) {
		this.itVepo = itVepo;
	}
	public List<NegoBillLandingI> getItItems() {
		return itItems;
	}
	public void setItItems(List<NegoBillLandingI> itItems) {
		this.itItems = itItems;
	}
	public List<ShippingReport> getItAddress() {
		return itAddress;
	}
	public void setItAddress(List<ShippingReport> itAddress) {
		this.itAddress = itAddress;
	}
	public String getTotalAmount() {
		return totalAmount;
	}
	public void setTotalAmount(String totalAmount) {
		this.totalAmount = totalAmount;
	}
	public String getTotalCbm() {
		return totalCbm;
	}
	public void setTotalCbm(String totalCbm) {
		this.totalCbm = totalCbm;
	}
	public String getTotalQuantity() {
		return totalQuantity;
	}
	public void setTotalQuantity(String totalQuantity) {
		this.totalQuantity = totalQuantity;
	}
	
	

			
			
			
}
