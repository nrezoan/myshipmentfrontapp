package com.myshipment.model;

public class InsPODetails {
	private String order_no;
	private String sap_no;
	private String inspector_code;
	private String shipment_mode;
	private String quantity;
	private String delivery_date;
	private String adv_order;
	private String customer_name;
	private String flag;
	private String entry_date;

	public String getEntry_date() {
		return entry_date;
	}

	public void setEntry_date(String entry_date) {
		this.entry_date = entry_date;
	}

	public String getOrder_no() {
		return order_no;
	}

	public void setOrder_no(String order_no) {
		this.order_no = order_no;
	}

	public String getSap_no() {
		return sap_no;
	}

	public void setSap_no(String sap_no) {
		this.sap_no = sap_no;
	}

	public String getInspector_code() {
		return inspector_code;
	}

	public void setInspector_code(String inspector_code) {
		this.inspector_code = inspector_code;
	}

	public String getShipment_mode() {
		return shipment_mode;
	}

	public void setShipment_mode(String shipment_mode) {
		this.shipment_mode = shipment_mode;
	}

	public String getQuantity() {
		return quantity;
	}

	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}

	public String getDelivery_date() {
		return delivery_date;
	}

	public void setDelivery_date(String delivery_date) {
		this.delivery_date = delivery_date;
	}

	public String getAdvOrder() {
		return adv_order;
	}

	public void setAdvOrder(String adv_order) {
		this.adv_order = adv_order;
	}

	public String getCustomerName() {
		return customer_name;
	}

	public void setCustomerName(String customer_name) {
		this.customer_name = customer_name;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

}
