package com.myshipment.model;

public class BapiReturn1 {

	private static final Long serialVersionUID=-756754747564577l;
	private String type;
	private String code;
	private String message;
	private String logNo;
	private String logMsgNo;
	private String messageV1;
	private String messageV2;
	private String messageV3;
	private String messageV4;
	
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getLogNo() {
		return logNo;
	}
	public void setLogNo(String logNo) {
		this.logNo = logNo;
	}
	public String getLogMsgNo() {
		return logMsgNo;
	}
	public void setLogMsgNo(String logMsgNo) {
		this.logMsgNo = logMsgNo;
	}
	public String getMessageV1() {
		return messageV1;
	}
	public void setMessageV1(String messageV1) {
		this.messageV1 = messageV1;
	}
	public String getMessageV2() {
		return messageV2;
	}
	public void setMessageV2(String messageV2) {
		this.messageV2 = messageV2;
	}
	public String getMessageV3() {
		return messageV3;
	}
	public void setMessageV3(String messageV3) {
		this.messageV3 = messageV3;
	}
	public String getMessageV4() {
		return messageV4;
	}
	public void setMessageV4(String messageV4) {
		this.messageV4 = messageV4;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((code == null) ? 0 : code.hashCode());
		result = prime * result + ((logMsgNo == null) ? 0 : logMsgNo.hashCode());
		result = prime * result + ((logNo == null) ? 0 : logNo.hashCode());
		result = prime * result + ((message == null) ? 0 : message.hashCode());
		result = prime * result + ((messageV1 == null) ? 0 : messageV1.hashCode());
		result = prime * result + ((messageV2 == null) ? 0 : messageV2.hashCode());
		result = prime * result + ((messageV3 == null) ? 0 : messageV3.hashCode());
		result = prime * result + ((messageV4 == null) ? 0 : messageV4.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BapiReturn1 other = (BapiReturn1) obj;
		if (code == null) {
			if (other.code != null)
				return false;
		} else if (!code.equals(other.code))
			return false;
		if (logMsgNo == null) {
			if (other.logMsgNo != null)
				return false;
		} else if (!logMsgNo.equals(other.logMsgNo))
			return false;
		if (logNo == null) {
			if (other.logNo != null)
				return false;
		} else if (!logNo.equals(other.logNo))
			return false;
		if (message == null) {
			if (other.message != null)
				return false;
		} else if (!message.equals(other.message))
			return false;
		if (messageV1 == null) {
			if (other.messageV1 != null)
				return false;
		} else if (!messageV1.equals(other.messageV1))
			return false;
		if (messageV2 == null) {
			if (other.messageV2 != null)
				return false;
		} else if (!messageV2.equals(other.messageV2))
			return false;
		if (messageV3 == null) {
			if (other.messageV3 != null)
				return false;
		} else if (!messageV3.equals(other.messageV3))
			return false;
		if (messageV4 == null) {
			if (other.messageV4 != null)
				return false;
		} else if (!messageV4.equals(other.messageV4))
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "BapiReturn1 [type=" + type + ", code=" + code + ", message=" + message + ", logNo=" + logNo
				+ ", logMsgNo=" + logMsgNo + ", messageV1=" + messageV1 + ", messageV2=" + messageV2 + ", messageV3="
				+ messageV3 + ", messageV4=" + messageV4 + "]";
	}
	
	
	
}
