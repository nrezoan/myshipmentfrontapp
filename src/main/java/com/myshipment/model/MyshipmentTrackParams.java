package com.myshipment.model;

import java.util.Date;

/*
 * @Hamid
 */
public class MyshipmentTrackParams {
	
	private String customer;	
	private String salesOrg;	
	private String distChan;	
	private String division;	
	//private String erdat1;
	//private String erdat2;
	private Date erdat1;
	private Date erdat2;
	private String action;
	private String doc_no;	
	
	public String getCustomer() {
		return customer;
	}
	public void setCustomer(String customer) {
		this.customer = customer;
	}
	public String getSalesOrg() {
		return salesOrg;
	}
	public void setSalesOrg(String salesOrg) {
		this.salesOrg = salesOrg;
	}
	public String getDistChan() {
		return distChan;
	}
	public void setDistChan(String distChan) {
		this.distChan = distChan;
	}
	public String getDivision() {
		return division;
	}
	public void setDivision(String division) {
		this.division = division;
	}
	public Date getErdat1() {
		return erdat1;
	}
	public void setErdat1(Date erdat1) {
		this.erdat1 = erdat1;
	}
	public Date getErdat2() {
		return erdat2;
	}
	public void setErdat2(Date erdat2) {
		this.erdat2 = erdat2;
	}
	public String getAction() {
		return action;
	}
	public void setAction(String action) {
		this.action = action;
	}
	public String getDoc_no() {
		return doc_no;
	}
	public void setDoc_no(String doc_no) {
		this.doc_no = doc_no;
	}
	
}
