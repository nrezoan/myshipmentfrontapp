package com.myshipment.model;
/*
 * @Ranjeet Kumar
 */
public class PartnerDTO {

	private String soNumber;	
	private String itemNumber;	
	private String partnRole;	
	private String partnNumb;
	private String partnerName;	
	private String partnerAddr1;	
	private String partnerAddr2;	
	private String partnerAddr3;	
	private String partnerCity;	
	private String partnerCountry;
	private String partnerCountryName;
	
	public String getSoNumber() {
		return soNumber;
	}
	public void setSoNumber(String soNumber) {
		this.soNumber = soNumber;
	}
	public String getItemNumber() {
		return itemNumber;
	}
	public void setItemNumber(String itemNumber) {
		this.itemNumber = itemNumber;
	}
	public String getPartnRole() {
		return partnRole;
	}
	public void setPartnRole(String partnRole) {
		this.partnRole = partnRole;
	}
	public String getPartnNumb() {
		return partnNumb;
	}
	public void setPartnNumb(String partnNumb) {
		this.partnNumb = partnNumb;
	}
	public String getPartnerName() {
		return partnerName;
	}
	public void setPartnerName(String partnerName) {
		this.partnerName = partnerName;
	}
	public String getPartnerAddr1() {
		return partnerAddr1;
	}
	public void setPartnerAddr1(String partnerAddr1) {
		this.partnerAddr1 = partnerAddr1;
	}
	public String getPartnerAddr2() {
		return partnerAddr2;
	}
	public void setPartnerAddr2(String partnerAddr2) {
		this.partnerAddr2 = partnerAddr2;
	}
	public String getPartnerAddr3() {
		return partnerAddr3;
	}
	public void setPartnerAddr3(String partnerAddr3) {
		this.partnerAddr3 = partnerAddr3;
	}
	public String getPartnerCity() {
		return partnerCity;
	}
	public void setPartnerCity(String partnerCity) {
		this.partnerCity = partnerCity;
	}
	public String getPartnerCountry() {
		return partnerCountry;
	}
	public void setPartnerCountry(String partnerCountry) {
		this.partnerCountry = partnerCountry;
	}
	public String getPartnerCountryName() {
		return partnerCountryName;
	}
	public void setPartnerCountryName(String partnerCountryName) {
		this.partnerCountryName = partnerCountryName;
	}	
	
}
