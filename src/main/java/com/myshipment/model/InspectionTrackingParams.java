package com.myshipment.model;
/*
 * @Ranjeet Kumar
 */
public class InspectionTrackingParams {

	
	private String fromDate;
	private String toDate;
	private String inspectionDate;
	
/*	private String division;
	private String statusType;
	private String expStatusType;	
	private String shipper_no;
	private String buyer_no;*/

	public String getFromDate() {
		return fromDate;
	}
	public void setFromDate(String fromDate) {
		this.fromDate = fromDate;
	}
	public String getToDate() {
		return toDate;
	}
	public void setToDate(String toDate) {
		this.toDate = toDate;
	}
	public String getInspectionDate() {
		return inspectionDate;
	}
	public void setInspectionDate(String inspectionDate) {
		this.inspectionDate = inspectionDate;
	}
	
	
}
