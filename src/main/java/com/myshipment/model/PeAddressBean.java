package com.myshipment.model;

public class PeAddressBean {

	private static final long serialVersionUID=42554647788l;
	private String formOfAd;
	private String firstName;
	private String name;
	private String name3;
	private String name4;
	private String dateBirth;
	private String street;
	private String postalCode;
	private String city;
	private String region;
	private String country;
	private String countrniso;
	private String countraiso;
	private String internate;
	private String faxNumber;
	private String telephone;
	private String telephone2;
	private String langu;
	private String languIso;
	private String currency;
	private String currencyIso;
	private String countryIso;
	private String onlyChangeComaddress;

	public String getFormOfAd() {
		return formOfAd;
	}

	public void setFormOfAd(String formOfAd) {
		this.formOfAd = formOfAd;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName3() {
		return name3;
	}

	public void setName3(String name3) {
		this.name3 = name3;
	}

	public String getName4() {
		return name4;
	}

	public void setName4(String name4) {
		this.name4 = name4;
	}

	public String getDateBirth() {
		return dateBirth;
	}

	public void setDateBirth(String dateBirth) {
		this.dateBirth = dateBirth;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCountrniso() {
		return countrniso;
	}

	public void setCountrniso(String countrniso) {
		this.countrniso = countrniso;
	}

	public String getCountraiso() {
		return countraiso;
	}

	public void setCountraiso(String countraiso) {
		this.countraiso = countraiso;
	}

	public String getInternate() {
		return internate;
	}

	public void setInternate(String internate) {
		this.internate = internate;
	}

	public String getFaxNumber() {
		return faxNumber;
	}

	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public String getTelephone2() {
		return telephone2;
	}

	public void setTelephone2(String telephone2) {
		this.telephone2 = telephone2;
	}

	public String getLangu() {
		return langu;
	}

	public void setLangu(String langu) {
		this.langu = langu;
	}

	public String getLanguIso() {
		return languIso;
	}

	public void setLanguIso(String languIso) {
		this.languIso = languIso;
	}

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public String getCurrencyIso() {
		return currencyIso;
	}

	public void setCurrencyIso(String currencyIso) {
		this.currencyIso = currencyIso;
	}

	public String getCountryIso() {
		return countryIso;
	}

	public void setCountryIso(String countryIso) {
		this.countryIso = countryIso;
	}

	public String getOnlyChangeComaddress() {
		return onlyChangeComaddress;
	}

	public void setOnlyChangeComaddress(String onlyChangeComaddress) {
		this.onlyChangeComaddress = onlyChangeComaddress;
	}

}
