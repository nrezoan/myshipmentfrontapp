package com.myshipment.model;

public class ShipDetailsByPodParamsBuyer {
	private String poo;
	private DashboardBuyerParams dashboardBuyerParams;
	
	public String getPoo() {
		return poo;
	}
	public void setPoo(String poo) {
		this.poo = poo;
	}
	public DashboardBuyerParams getDashboardBuyerParams() {
		return dashboardBuyerParams;
	}
	public void setDashboardBuyerParams(DashboardBuyerParams dashboardBuyerParams) {
		this.dashboardBuyerParams = dashboardBuyerParams;
	}

}
