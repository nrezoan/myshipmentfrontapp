package com.myshipment.model;


public class Invoice {
	private String vbeln;
	private String knumv;
	private String knumh;
	private String kopos;

	public String getVbeln() {
		return vbeln;
	}

	public void setVbeln(String vbeln) {
		this.vbeln = vbeln;
	}

	public String getKnumv() {
		return knumv;
	}

	public void setKnumv(String knumv) {
		this.knumv = knumv;
	}

	public String getKnumh() {
		return knumh;
	}

	public void setKnumh(String knumh) {
		this.knumh = knumh;
	}

	public String getKopos() {
		return kopos;
	}

	public void setKopos(String kopos) {
		this.kopos = kopos;
	}
	
}