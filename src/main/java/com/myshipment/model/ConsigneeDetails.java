package com.myshipment.model;

public class ConsigneeDetails {
	private static final long serialVersionUID=2414324l;
	
private String consigneeName;
private String consigneeAddress;
private String consigneeCountry;
public String getConsigneeName() {
	return consigneeName;
}
public void setConsigneeName(String consigneeName) {
	this.consigneeName = consigneeName;
}
public String getConsigneeAddress() {
	return consigneeAddress;
}
public void setConsigneeAddress(String consigneeAddress) {
	this.consigneeAddress = consigneeAddress;
}
public String getConsigneeCountry() {
	return consigneeCountry;
}
public void setConsigneeCountry(String consigneeCountry) {
	this.consigneeCountry = consigneeCountry;
}
public String getConsigneeCity() {
	return consigneeCity;
}
public void setConsigneeCity(String consigneeCity) {
	this.consigneeCity = consigneeCity;
}
private String consigneeCity;

}
