package com.myshipment.model;

/*
 * @Ranjeet Kumar
 */
public class SalesOrderJsonOutputData {

	private String salesOrder;

	public String getSalesOrder() {
		return salesOrder;
	}

	public void setSalesOrder(String salesOrder) {
		this.salesOrder = salesOrder;
	}
	
	
}
