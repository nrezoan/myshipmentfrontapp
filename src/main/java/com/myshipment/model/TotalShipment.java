/**
*
*@author Ahmad Naquib
*/
package com.myshipment.model;

public class TotalShipment {

	private String id;
	private String name;
	private double shipment;
	private double shipmentPercentage;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getShipment() {
		return shipment;
	}
	public void setShipment(double shipment) {
		this.shipment = shipment;
	}
	public double getShipmentPercentage() {
		return shipmentPercentage;
	}
	public void setShipmentPercentage(double shipmentPercentage) {
		this.shipmentPercentage = shipmentPercentage;
	}
	
	
	
}
