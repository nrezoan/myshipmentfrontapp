package com.myshipment.model;

import java.util.List;

public class OAGResponse {
	
	private List<OAG> oagList;

	public List<OAG> getOagList() {
		return oagList;
	}

	public void setOagList(List<OAG> oagList) {
		this.oagList = oagList;
	}

}
